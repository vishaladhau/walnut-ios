//
//  WLTHomeScreenAnimator.h
//  walnut
//
//  Created by Abhinav Singh on 19/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WLTGroupCollectionViewCell;

@interface WLTHomeScreenAnimator : NSObject <UIViewControllerAnimatedTransitioning> {
    
    UIEdgeInsets backgroundImageAlphaInsets;
    WLTGroupCollectionViewCell *groupListCell;
}

- (instancetype)initWithGroupCollectionCell:(WLTGroupCollectionViewCell*)cell;

@end
