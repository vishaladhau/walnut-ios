//
//  WLTContact.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 07/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTContact.h"

@implementation WLTContact

- (instancetype)initWithName:(NSString*)name andNumber:(NSString*)number {
    
    self = [super init];
    if (self) {
        
        self.fullName = name;
        
        WLTMobileNumber *mob = [[WLTMobileNumber alloc] init];
        mob.number = number;
        
        self.mobile = mob;
    }
    
    return self;
}

-(NSArray*)searchableValues {
    
    NSMutableArray *values = [NSMutableArray new];
    if (self.fullName.length) {
        [values addObject:self.fullName];
    }
    
    [values addObject:self.mobile.number];
    
    return values;
}

-(NSString*)name {
    return self.fullName;
}

-(NSString*)completeNumber {
    return self.mobile.number;
}

-(BOOL)isEqual:(WLTContact*)object {
    
    BOOL equal = NO;
    if ([self.mobile.number isEqualToString:object.mobile.number]) {
        equal = YES;
    }
    
    return equal;
}

@end
