//
//  WLTPartialSettle.m
//  walnut
//
//  Created by Abhinav Singh on 22/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTPartialSettle.h"
#import "WLTCombinedGroupPayments.h"

@implementation WLTPartialSettle

- (instancetype)initWithCombinedGroup:(WLTCombinedGroupPayments *)grpData {
    
    self = [super initWithCombinedGroup:grpData];
    if (self) {
        
        self.currentAmount = (self.totalGetAmount - self.totalPayAmount);
    }
    
    return self;
}

-(void)currentAmountChanged {
    
    CGFloat remaning = (self.totalPayAmount + self.currentAmount);
    for ( WLTPartialGroup *payGrp in self.getGroups ) {
        
        CGFloat canGet = 0;
        
        if (remaning > 0) {
            if (payGrp.acctualAmount <= remaning) {
                canGet = payGrp.acctualAmount;
            }else {
                canGet = remaning;
            }
        }
        
        payGrp.currentAmount = canGet;
        remaning -= canGet;
        
        if (payGrp == [self.getGroups lastObject]) {
            if (remaning > 0) {
                
                payGrp.currentAmount += remaning;
                remaning = 0;
            }
        }
    }
}

-(NSArray*)fixedGroups {
    return self.payGroups;
}

-(NSArray*)changableGroups {
    return self.getGroups;
}

@end
