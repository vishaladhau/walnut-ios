//
//  WLTDotsLoadingView.m
//  walnut
//
//  Created by Abhinav Singh on 27/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTDotsLoadingView.h"

@implementation WLTDotsLoadingView

-(void)awakeFromNib {
    
    [super awakeFromNib];
    [self initialSetup];
}

-(void)initialSetup {
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    indicator.color = [UIColor cadetColor];
    indicator.hidesWhenStopped = YES;
    [self addSubview:indicator];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(indicator);
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[indicator]-0-|" options:0 metrics:nil views:dict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[indicator]" options:0 metrics:nil views:dict]];
    
    activityView = indicator;
}

-(void)startAnimating {
    
    self.hidden = NO;
    [activityView startAnimating];
}

-(void)stopAnimating {
    
    self.hidden = YES;
    [activityView stopAnimating];
}

-(CGSize)intrinsicContentSize {
    return [activityView intrinsicContentSize];
}


@end
