//
//  WLTFlowConstants.h
//  walnut
//
//  Created by Harshad on 18/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#ifndef WLTFlowConstants_h
#define WLTFlowConstants_h

typedef NS_ENUM(NSInteger, WLTEntryFlowType) {
    WLTEntryFlowTypeSettings = 0,
    WLTEntryFlowTypePay,
    WLTEntryFlowTypeReceive
};

#endif /* WLTFlowConstants_h */
