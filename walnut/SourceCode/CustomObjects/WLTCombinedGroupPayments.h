//
//  WLTCombinedGroupPayments.h
//  walnut
//
//  Created by Abhinav Singh on 02/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WLTGroup.h"

@class WLTTransactionUser;
@class WLTGroup;

@interface WLTGroupPaymentData : NSObject {
    
}

@property(nonatomic, assign) CGFloat amount;
@property(nonatomic, strong) WLTGroup *group;
@property(nonatomic, assign) UserTransactionState transactionState;

-(NSComparisonResult)amountCompare:(WLTCombinedGroupPayments*)comTrans;

@end

@interface WLTCombinedGroupPayments : NSObject

@property(nonatomic, assign) UserTransactionState transactionState;
@property(nonatomic, assign) CGFloat amount;
@property(readonly, strong) id <User> user;

@property(nonatomic, strong) NSMutableArray *groupsData;

-(void)refreshCombinedData;
-(void)reloadGroupsData;

-(instancetype)initWithUser:(id<User>)user;
-(instancetype)initWithGETGroup:(WLTCombinedGroupPayments*)otherGrp;

-(void)addUserData:(WLTTransactionUser*)user forGroup:(WLTGroup*)group;
-(NSComparisonResult)amountCompare:(WLTCombinedGroupPayments*)comTrans;

@end
