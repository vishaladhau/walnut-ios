//
//  WLTHomeScreenAnimator.m
//  walnut
//
//  Created by Abhinav Singh on 19/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTHomeScreenAnimator.h"
#import "WLTGroupCollectionViewCell.h"
#import "WLTGroupDetailViewController.h"
#import "WLTGroupListViewController.h"

@implementation WLTHomeScreenAnimator

- (instancetype)initWithGroupCollectionCell:(WLTGroupCollectionViewCell*)cell {
    
    if (!cell) {
        return nil;
    }
    
    self = [super init];
    if (self) {
        
        backgroundImageAlphaInsets = GroupCellImageAlphaInsets;
        groupListCell = cell;
    }
    
    return self;
}

-(NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext {
    return 0.3;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    UIViewController* toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController* fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    if ([fromViewController isKindOfClass:[WLTGroupListViewController class]] && [toViewController isKindOfClass:[WLTGroupDetailViewController class]]) {
        
        [self forwardAnimationForContext:transitionContext];
    }
    else if ([toViewController isKindOfClass:[WLTGroupListViewController class]] && [fromViewController isKindOfClass:[WLTGroupDetailViewController class]]) {
        
        [self backwardAnimationForContext:transitionContext];
    }
}

-(void)backwardAnimationForContext:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    CGFloat navBarHeight = 64;
    
    UIView *container = [transitionContext containerView];
    
    UIViewController* toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UITabBar *tab = toViewController.tabBarController.tabBar;
    tab.hidden = NO;
    UIImage *image = [tab snapShotWithoutAddSubview];
    tab.hidden = YES;
    CGRect imageRect = CGRectMake(0, container.height, tab.frame.size.width, tab.frame.size.height);
    
    UIImageView *tabbarImage = [[UIImageView alloc] initWithFrame:imageRect];
    tabbarImage.image = image;
    [container addSubview:tabbarImage];
    
    toViewController.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    [container addSubview:toViewController.view];
    [toViewController.view layoutIfNeeded];
    
    UIViewController* fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    [fromViewController.view removeFromSuperview];
    
    WLTGroupDetailViewController *grpDetail = (WLTGroupDetailViewController*)fromViewController;
    
    CGRect overWind = [container convertRect:groupListCell.frame fromView:groupListCell.superview];
    
    overWind = CGRectMake((overWind.origin.x + backgroundImageAlphaInsets.left),
                          (overWind.origin.y + backgroundImageAlphaInsets.top),
                          (overWind.size.width - (backgroundImageAlphaInsets.left+backgroundImageAlphaInsets.right)) ,
                          (overWind.size.height - (backgroundImageAlphaInsets.top+backgroundImageAlphaInsets.bottom)));
    
    UIView *mainAnimationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, navBarHeight+grpDetail->headerHeight.constant)];
    mainAnimationView.layer.masksToBounds = YES;
    mainAnimationView.layer.cornerRadius = 0;
    mainAnimationView.backgroundColor = groupListCell->backgroundImageView.tintColor;
    [container addSubview:mainAnimationView];
    
    UIImageView * (^ screenShotImageViewForView)(UIView *view) = ^UIImageView * (UIView *view){
        
        UIImageView *newImageView = nil;
        if (!view.hidden) {
            
            UIImage *image = [view snapShotWithoutAddSubview];
            
            newImageView = [[UIImageView alloc] initWithFrame:[mainAnimationView convertRect:view.frame fromView:view.superview]];
            newImageView.autoresizingMask = UIViewAutoresizingNone;
            newImageView.image = image;
            newImageView.contentMode = UIViewContentModeScaleAspectFit;
            [mainAnimationView addSubview:newImageView];
        }
        
        return newImageView;
    };
    
    UIImageView *titleImageView = screenShotImageViewForView(grpDetail->titleView->titleLabel);
    
    UIImageView *membersImageView = screenShotImageViewForView(grpDetail->titleView->subTitleLabel);
    
    UIImageView *settingItemImageView = screenShotImageViewForView(grpDetail->settingsBarButton);
    
    UIImageView *headerImageView = screenShotImageViewForView(grpDetail->headerView);
    
    UIImageView *tableImageView = nil;
    if (!grpDetail->transactionsBackView.hidden) {
        
        tableImageView = screenShotImageViewForView(grpDetail->transactionsBackView);
        [container addSubview:tableImageView];
    }
    
    UIImageView *amountImageView = screenShotImageViewForView(groupListCell->oweOrGetLabel);
    CGRect amountImageFrame = groupListCell->oweOrGetLabel.frame;
    amountImageFrame.origin.x = (groupListCell->oweOrGetLabel.frame.origin.x - backgroundImageAlphaInsets.left);;
    amountImageFrame.origin.y -= backgroundImageAlphaInsets.top;
    amountImageView.frame = amountImageFrame;
    amountImageView.alpha = 0;
    
    UIImageView *addButtonImageView = screenShotImageViewForView(groupListCell->addButton);
    addButtonImageView.alpha = 0;
    
    CGRect addButtonFrame = groupListCell->addButton.frame;
    addButtonFrame.origin.x = (groupListCell->addButton.frame.origin.x - backgroundImageAlphaInsets.right);
    addButtonFrame.origin.y -= backgroundImageAlphaInsets.top;
    addButtonImageView.frame = addButtonFrame;
    
    [toViewController.navigationController setNavigationBarHidden:YES animated:NO];
    [container bringSubviewToFront:tabbarImage];
    
    CGFloat duration = [self transitionDuration:transitionContext];
    
    CABasicAnimation *animation =  [CABasicAnimation animationWithKeyPath:@"cornerRadius"];
    animation.duration = duration;
    animation.fromValue = @(mainAnimationView.layer.cornerRadius);
    animation.toValue = [NSNumber numberWithInt:3];
    [mainAnimationView.layer addAnimation:animation forKey:@"CornerChangeAnimation"];
    
    [UIView animateWithDuration:duration animations:^{
        
        mainAnimationView.frame = overWind;
        
        headerImageView.alpha = 0;
        
        tabbarImage.originY = (container.height-49);
        
        CGRect titleFrame = groupListCell->titleLabel.frame;
        titleFrame.origin.x -= backgroundImageAlphaInsets.left;
        titleFrame.origin.y -= backgroundImageAlphaInsets.top;
        titleImageView.frame = titleFrame;
        
        CGRect subTitleFrame = groupListCell->memCountLabel.frame;
        subTitleFrame.origin.x -= backgroundImageAlphaInsets.left;
        subTitleFrame.origin.y -= backgroundImageAlphaInsets.top;
        membersImageView.frame = subTitleFrame;
        
        addButtonImageView.alpha = 1;
        amountImageView.alpha = 1;
        
        settingItemImageView.originX = mainAnimationView.width;
        
        tableImageView.alpha = 0;
        tableImageView.originY = (overWind.origin.y+overWind.size.height);
    } completion:^(BOOL finished) {
        
        tab.hidden = NO;
        
        [tabbarImage removeFromSuperview];
        [mainAnimationView removeFromSuperview];
        [tableImageView removeFromSuperview];
        
        groupListCell.hidden = NO;
        
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
    }];
    
    groupListCell.hidden = YES;
}

-(void)forwardAnimationForContext:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    CGFloat navBarHeight = 64;
    CGFloat tabBarHeight = 0;
    
    UIView *container = [transitionContext containerView];//[[[UIApplication sharedApplication] delegate] window];//
    
    UIViewController* toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    toViewController.hidesBottomBarWhenPushed = YES;
    toViewController.view.frame = CGRectMake(0, navBarHeight, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-(navBarHeight+tabBarHeight));
    [toViewController.view layoutIfNeeded];
    
    [toViewController.navigationController setNavigationBarHidden:NO animated:NO];
    
    WLTGroupDetailViewController *grpDetail = (WLTGroupDetailViewController*)toViewController;
    
    CGRect overWind = [container convertRect:groupListCell.frame fromView:groupListCell.superview];
    
    UIView *mainAnimationView = [[UIView alloc] initWithFrame:CGRectMake((overWind.origin.x + backgroundImageAlphaInsets.left),
                                                                         (overWind.origin.y + backgroundImageAlphaInsets.top),
                                                                         (overWind.size.width - (backgroundImageAlphaInsets.left + backgroundImageAlphaInsets.right)),
                                                                         (overWind.size.height - (backgroundImageAlphaInsets.top + backgroundImageAlphaInsets.bottom)))];
    mainAnimationView.layer.masksToBounds = YES;
    mainAnimationView.layer.cornerRadius = DefaultCornerRadius;
    mainAnimationView.backgroundColor = groupListCell->backgroundImageView.tintColor;
    [container addSubview:mainAnimationView];
    
    UIImageView * (^ screenShotImageViewForView)(UIView *view) = ^UIImageView * (UIView *view){
        
        UIImageView *newImageView = nil;
        if (!view.hidden) {
            
            UIImage *image = [view snapShotWithoutAddSubview];
            
            newImageView = [[UIImageView alloc] initWithFrame:[view.superview convertRect:view.frame toView:mainAnimationView]];
            newImageView.autoresizingMask = UIViewAutoresizingNone;
            newImageView.image = image;
            newImageView.contentMode = UIViewContentModeScaleAspectFit;
            [mainAnimationView addSubview:newImageView];
        }
        
        return newImageView;
    };
    
    UIImageView *titleImageView = screenShotImageViewForView(groupListCell->titleLabel);
    
    UIImageView *membersImageView = screenShotImageViewForView(groupListCell->memCountLabel);
    UIImageView *amountImageView = screenShotImageViewForView(groupListCell->oweOrGetLabel);
    amountImageView.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleRightMargin);
    
    UIImageView *addButtonImageView = screenShotImageViewForView(groupListCell->addButton);
    addButtonImageView.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin);
    
    UIImageView *tableImageView = screenShotImageViewForView(grpDetail->transactionsBackView);
    tableImageView.alpha = 0;
    tableImageView.frame = CGRectMake(0, mainAnimationView.maxY, tableImageView.width, tableImageView.height);
    [container addSubview:tableImageView];
    
    UIImageView *grpDetailHeader = screenShotImageViewForView(grpDetail->headerView);
    grpDetailHeader.alpha = 0;
    grpDetailHeader.frame = CGRectMake(0, mainAnimationView.height-grpDetailHeader.height, grpDetailHeader.width, grpDetailHeader.height);
    grpDetailHeader.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    
    UIImageView *settingItemImageView = screenShotImageViewForView(grpDetail->settingsBarButton);
    CGRect finalSettingFrame = [container convertRect:grpDetail->settingsBarButton.frame fromView:grpDetail->settingsBarButton.superview];
    settingItemImageView.frame = CGRectMake(mainAnimationView.width, finalSettingFrame.origin.y, finalSettingFrame.size.width, finalSettingFrame.size.height);
    
    
    CGFloat newHeight = (grpDetail->headerHeight.constant + navBarHeight);
    
    CGRect titleOverWindow = [container convertRect:grpDetail->titleView->titleLabel.frame fromView:grpDetail->titleView];
    CGRect memCountOverWindow = [container convertRect:grpDetail->titleView->subTitleLabel.frame fromView:grpDetail->titleView];
    
    [toViewController.navigationController setNavigationBarHidden:YES animated:NO];
    
    CGFloat duration = [self transitionDuration:transitionContext];
    
    CABasicAnimation *animation =  [CABasicAnimation animationWithKeyPath:@"cornerRadius"];
    animation.duration = duration;
    animation.fromValue = @(mainAnimationView.layer.cornerRadius);
    animation.toValue = [NSNumber numberWithInt:0];
    [mainAnimationView.layer addAnimation:animation forKey:@"CornerChangeAnimation"];
    
    [UIView animateWithDuration:duration animations:^{
        
        mainAnimationView.frame = CGRectMake(0, 0, container.width, newHeight);
        
        grpDetailHeader.alpha = 1;
        
        membersImageView.frame = memCountOverWindow;
        titleImageView.frame = titleOverWindow;
        tableImageView.originY = newHeight;
        
        settingItemImageView.frame = finalSettingFrame;
        
        amountImageView.alpha = 0;
        addButtonImageView.alpha = 0;
        
        tableImageView.alpha = 1;
    } completion:^(BOOL finished) {
        
        [toViewController.navigationController setNavigationBarHidden:NO animated:NO];
        
        [container addSubview:toViewController.view];
        
        [tableImageView removeFromSuperview];
        [mainAnimationView removeFromSuperview];
        
        groupListCell.hidden = NO;
        
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
    }];
    
    groupListCell.hidden = YES;
}

@end
