//
//  WLTMobileNumber.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 07/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WLTMobileNumber : NSObject

@property(nonatomic, strong) NSString *number;
@property(nonatomic, strong) NSString *label;

@end
