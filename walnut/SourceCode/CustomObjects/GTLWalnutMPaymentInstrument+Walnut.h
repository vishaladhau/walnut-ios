//
//  GTLWalnutMPaymentInstrument+Walnut.h
//  walnut
//
//  Created by Abhinav Singh on 09/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "GTLWalnutMPaymentInstrument.h"

typedef NS_ENUM(NSInteger, InstrumentType) {
    
    InstrumentTypeBank = 0,
    InstrumentTypeCreditCard,
    InstrumentTypeDebitCard,
};

@interface GTLWalnutMPaymentInstrument (Walnut)

@end
