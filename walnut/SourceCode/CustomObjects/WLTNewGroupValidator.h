//
//  WLTNewGroupValidator.h
//  walnut
//
//  Created by Abhinav Singh on 06/09/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, NewGroupValidatorAction) {
    
    NewGroupValidatorActionCreateNewGroup = 0,
    NewGroupValidatorActionRedirect,
    NewGroupValidatorActionCancel,
};

typedef void (^ NewGroupValidationBlock)(NewGroupValidatorAction action, id userInfo);

@interface WLTNewGroupValidator : NSObject

+(void)validateForMembers:(NSArray*)members
                isPrivate:(BOOL)makePrivate
                 isDirect:(BOOL)isDirectGroup
                presenter:(UIViewController*)controller
               completion:(NewGroupValidationBlock)completion;

+(BOOL)isStatusDirectWithMembers:(NSArray*)members;

@end
