//
//  WLTNewGroupValidator.m
//  walnut
//
//  Created by Abhinav Singh on 06/09/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTNewGroupValidator.h"
#import "WLTContact.h"
#import "WLTDatabaseManager.h"
#import "WLTGroup.h"

@implementation WLTNewGroupValidator

+(BOOL)isStatusDirectWithMembers:(NSArray*)members {
    
    NSString *currentUserMobileNumber = CURRENT_USER_MOB;
    
    BOOL isDirectGroup = NO;
    if (members.count == 1) {
        
        WLTContact *first = [members firstObject];
        if (![first.mobile.number isEqualToString:currentUserMobileNumber]) {
            isDirectGroup = YES;
        }
    }else if (members.count == 2) {
        
        isDirectGroup = NO;
        for ( WLTContact *con in members ) {
            if ([con.mobile.number isEqualToString:currentUserMobileNumber]) {
                isDirectGroup = YES;
                break;
            }
        }
    }
    
    return isDirectGroup;
}

+(void)validateForMembers:(NSArray*)members
                isPrivate:(BOOL)makePrivate
                 isDirect:(BOOL)isDirectGroup
                presenter:(UIViewController*)controller
               completion:(NewGroupValidationBlock)completion{
    
    NSString *currentUserM = CURRENT_USER_MOB;
    
    NSMutableArray *allMemsMobile = [NSMutableArray new];
    for( WLTContact *cont in members ) {
        [allMemsMobile addObject:cont.mobile.number];
    }
    
    if ( ![allMemsMobile containsObject:currentUserM] ) {
        [allMemsMobile addObject:currentUserM];
    }
    
    NSPredicate *predicatePrivacy = [NSPredicate predicateWithFormat:@"isPrivate == %@", @(makePrivate)];
    NSPredicate *predicateMemCount = [NSPredicate predicateWithFormat:@"SELF.members.@count = %@", @(allMemsMobile.count)];
    
    NSPredicate *predicateFinal = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicatePrivacy, predicateMemCount]];
    
    NSArray *prevSaved = [[WLTDatabaseManager sharedManager] allObjectsOfClass:@"WLTGroup" withPredicate:predicateFinal];
    
    NSMutableArray *oldGrpsWithSameMembers = [NSMutableArray new];
    
    for ( WLTGroup *grp in prevSaved ) {
        
        BOOL isSameGrp = YES;
        for ( WLTUser *mem in grp.members ) {
            
            NSString *comStr = mem.mobileString;
            if ( ![allMemsMobile containsObject:comStr] ) {
                isSameGrp = NO;
                break;
            }
        }
        
        if (isSameGrp) {
            [oldGrpsWithSameMembers addObject:grp];
        }
    }
    
    if (oldGrpsWithSameMembers.count >= 1) {
        
        if (isDirectGroup && (oldGrpsWithSameMembers.count == 1)) {
            
            //Redirect User to old Group without asking :)
            WLTGroup *group = [oldGrpsWithSameMembers firstObject];
            completion(NewGroupValidatorActionRedirect, group);
        }else {
            
            //Show Action Sheet to select old grp or create new;
            NSString *title = @"A group with these members already exists:";
            
            BOOL haveCurrentUser = NO;
            NSMutableArray *allMemName = [NSMutableArray new];
            for ( WLTContact *cont in members ) {
                
                if ( !haveCurrentUser && [cont.mobile.number isEqualToString:CURRENT_USER_MOB] ) {
                    haveCurrentUser = YES;
                }
                [allMemName addObject:cont.fullName];
            }
            
            if (!haveCurrentUser) {
                [allMemName insertObject:@"You" atIndex:0];
            }
            
            NSString *message = [allMemName componentsJoinedByString:@", "];
            
            UIAlertController *controller = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleActionSheet];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
            [controller addAction:cancelAction];
            
            for ( WLTGroup *grpOld in oldGrpsWithSameMembers ) {
                
                UIAlertAction *detailsAction = [UIAlertAction actionWithTitle:grpOld.dName style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    completion(NewGroupValidatorActionRedirect, grpOld);
                }];
                
                [controller addAction:detailsAction];
            }
            
            if (!isDirectGroup) {
                
                UIAlertAction *createNew = [UIAlertAction actionWithTitle:@"Create New" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * _Nonnull action) {
                                                                      
                                                                      completion(NewGroupValidatorActionCreateNewGroup, nil);
                                                                  }];
                
                [controller addAction:createNew];
            }
            
            [controller presentViewController:controller animated:YES completion:nil];
        }
    }
    else {
        
        completion(NewGroupValidatorActionCreateNewGroup, nil);
    }
}

@end
