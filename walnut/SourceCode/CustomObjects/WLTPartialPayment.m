//
//  WLTPartialPayment.m
//  walnut
//
//  Created by Abhinav Singh on 21/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTPartialPayment.h"
#import "WLTCombinedGroupPayments.h"

@implementation WLTPartialPayment

- (instancetype)initWithCombinedGroup:(WLTCombinedGroupPayments *)grpData {
    
    self = [super initWithCombinedGroup:grpData];
    if (self) {
        
        if (self.payGroups.count || self.getGroups.count) {
            self.currentAmount = (self.totalPayAmount - self.totalGetAmount);
        }else {
            self.currentAmount = grpData.amount;
        }
    }
    
    return self;
}

-(void)currentAmountChanged {
    
    CGFloat remaning = (self.totalGetAmount + self.currentAmount);
    for ( WLTPartialGroup *payGrp in self.payGroups ) {
        
        CGFloat canPay = 0;
        
        if (remaning > 0) {
            if (payGrp.acctualAmount <= remaning) {
                canPay = payGrp.acctualAmount;
            }else {
                canPay = remaning;
            }
        }
        
        payGrp.currentAmount = canPay;
        remaning -= canPay;
        
        if (payGrp == [self.payGroups lastObject]) {
            if (remaning > 0) {
                payGrp.currentAmount += remaning;
                remaning = 0;
            }
        }
    }
}

-(NSArray*)fixedGroups {
    return self.getGroups;
}

-(NSArray*)changableGroups {
    return self.payGroups;
}

@end
