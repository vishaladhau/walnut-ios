//
//  WLTContact.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 07/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WLTMobileNumber.h"

@interface WLTContact : NSObject <User>{
    
}

@property(nonatomic, strong) NSString *fullName;
@property(nonatomic, strong) WLTMobileNumber *mobile;

-(instancetype)initWithName:(NSString*)name andNumber:(NSString*)number;

@end
