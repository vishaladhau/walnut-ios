//
//  WLTCombinedGroupPayments.m
//  walnut
//
//  Created by Abhinav Singh on 02/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTCombinedGroupPayments.h"
#import "WLTTransactionUser.h"
#import "WLTGroup.h"
#import "WLTContact.h"

@implementation WLTGroupPaymentData

-(NSComparisonResult)amountCompare:(WLTCombinedGroupPayments*)comTrans {
    
    return [@(self.amount) compare:@(comTrans.amount)];
}

@end

@implementation WLTCombinedGroupPayments

-(instancetype)initWithUser:(id<User>)user {
    
    self = [super init];
    if (self) {
        
        WLTContact *cont = [[WLTContact alloc] initWithName:[user name] andNumber:[user completeNumber]];
        _user = cont;
        
        _groupsData = [NSMutableArray new];
    }
    
    return self;
}

-(instancetype)initWithGETGroup:(WLTCombinedGroupPayments*)otherGrp {
    
    self = [self initWithUser:otherGrp.user];
    if (self) {
        
        self.amount = otherGrp.amount;
        self.transactionState = UserTransactionStateGet;
        
        CGFloat remaning = self.amount;
        for ( WLTGroupPaymentData *dataGrp in otherGrp.groupsData ) {
            
            if (dataGrp.transactionState == UserTransactionStateGet) {
                
                WLTGroupPaymentData *newData = [[WLTGroupPaymentData alloc] init];
                newData.group = dataGrp.group;
                newData.transactionState = UserTransactionStateGet;
                [self.groupsData addObject:newData];
                
                CGFloat canPay = 0;
                if (remaning > 0) {
                    
                    if (remaning >= newData.amount) {
                        canPay = newData.amount;
                    }else {
                        canPay = remaning;
                    }
                    
                    remaning -= canPay;
                }
                
                newData.amount = canPay;
            }
        }
    }
    
    return self;
}


-(void)addUserData:(WLTTransactionUser*)user forGroup:(WLTGroup*)group {
    
    [self addUser:user forGroup:group forType:[group userTransactionState]];
}

-(void)addUser:(WLTTransactionUser*)user forGroup:(WLTGroup*)group forType:(UserTransactionState)type{
    
    WLTGroupPaymentData *data = [[WLTGroupPaymentData alloc] init];
    data.group = group;
    data.amount = user.amount.floatValue;
    data.transactionState = type;
    
    [self.groupsData addObject:data];
}

-(void)refreshCombinedData {
    
    CGFloat amount = 0;
    for ( WLTGroupPaymentData *data in self.groupsData ) {
        
        if (data.transactionState == UserTransactionStateGet) {
            amount += data.amount;
        }
        else if (data.transactionState == UserTransactionStateOwe) {
            amount -= data.amount;
        }
    }
    
    if (amount < 0) {
        
        amount = (-1 * amount);
        _transactionState = UserTransactionStateOwe;
    }else if (amount > 0) {
        _transactionState = UserTransactionStateGet;
    }else {
        _transactionState = UserTransactionStateNone;
    }
    
    [self.groupsData sortUsingSelector:@selector(amountCompare:)];
    _amount = amount;
}

-(void)reloadGroupsData {
    
    NSString *grpUserMob = [self.user completeNumber];
    for ( WLTGroupPaymentData *data in self.groupsData ) {
        
        BOOL foundUser = NO;
        
        for ( WLTTransactionUser *users in data.group.userGetFrom ) {
            
            if ([users.mobileString isEqualToString:grpUserMob]) {
                data.amount = users.amount.floatValue;
                data.transactionState = UserTransactionStateGet;
                
                foundUser = YES;
                break;
            }
        }
        
        if (!foundUser) {
            for ( WLTTransactionUser *users in data.group.userPayTo ) {
                
                if ([users.mobileString isEqualToString:grpUserMob]) {
                    data.amount = users.amount.floatValue;
                    data.transactionState = UserTransactionStateOwe;
                    
                    foundUser = YES;
                    break;
                }
            }
        }
        
        if (!foundUser) {
            
            data.amount = 0;
            data.transactionState = UserTransactionStateNone;
        }
    }
    
    [self refreshCombinedData];
}

-(NSComparisonResult)amountCompare:(WLTCombinedGroupPayments*)comTrans {
    
    return [@(comTrans.amount) compare:@(self.amount)];
}

@end
