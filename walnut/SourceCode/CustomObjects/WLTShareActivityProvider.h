//
//  WLTShareActivityProvider.h
//  walnut
//
//  Created by Abhinav Singh on 06/09/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, WLTShareActivityType) {
    
    WLTShareActivityTypeDefault = 0,
    WLTShareActivityTypeTransactionPaid,
    WLTShareActivityTypeTransactionReceived,
    WLTShareActivityTypeTransactionRemind,
};

@interface WLTSharingHelper : NSObject

+(NSArray*)activityItemsForType:(WLTShareActivityType)type;


@end

@interface WLTShareActivityProvider : UIActivityItemProvider

+(WLTShareActivityProvider*)activityWithType:(WLTShareActivityType)type;
+(NSString*)shareTextForActivityType:(WLTShareActivityType)type;

@end
