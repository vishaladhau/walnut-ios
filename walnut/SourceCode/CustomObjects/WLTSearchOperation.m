//
//  WLTSearchOperation.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 07/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTSearchOperation.h"

@implementation WLTSearchOperation

-(instancetype)initWithDataArray:(NSArray*)toSearch andSearchString:(NSString*)search {
    
    self = [super init];
    if (self) {
        
        dataArray = toSearch;
        searchString = search;
    }
    
    return self;
}

-(void)main {
    
    if (![self isCancelled]) {
        
        if (!searchString.length) {
            return;
        }
        
        [self startProcess];
    }
    
    [super main];
}

-(void)startProcess {
    
    NSMutableArray *resultsArray = [NSMutableArray new];
    for (id <WLTSearchableObject> obj in dataArray) {
        
        if ([self isCancelled]) {
            break;
        }
        
        NSArray *allSearchables = [obj searchableValues];
        for ( NSString *strSearch in allSearchables ) {
            
            if ([self isSearchTermContainedBy:strSearch]) {
                [resultsArray addObject:obj];
                break;
            }
        }
    }
    
    if (![self isCancelled]) {
        [self.delegate searchCompleted:self forString:searchString withResults:resultsArray];
    }
}

-(BOOL)isSearchTermContainedBy:(NSString*)string {
    
    if (string.length) {
        
        if ([string rangeOfString:searchString options:NSCaseInsensitiveSearch].location != NSNotFound) {
            return YES;
        }
    }
    
    return NO;
}

-(void)start {
    
    [super start];
}

@end
