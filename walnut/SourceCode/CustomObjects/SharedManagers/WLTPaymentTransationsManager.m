//
//  WLTPaymentTransationsManager.m
//  walnut
//
//  Created by Abhinav Singh on 14/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTPaymentTransationsManager.h"
#import "WLTDatabaseManager.h"
#import "WLTPaymentTransaction.h"
#import "GTLWalnutMPaymentTransaction.h"
#import "GTLQueryWalnut.h"
#import "WLTNetworkManager.h"

@interface WLTPaymentTransationsManager ()

@property(nonatomic, weak) GTLServiceTicket *requestTicket;

@end

@implementation WLTPaymentTransationsManager

+(WLTPaymentTransationsManager*)sharedManager {
    
    static __strong WLTPaymentTransationsManager *manager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
    
    return manager;
}

-(instancetype)init {
    
    self = [super init];
    if (self) {
        
        NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"updatedIds.data"];
        
        allUnreadTransactionIDs = [NSKeyedUnarchiver unarchiveObjectWithFile:storeURL.path];
        if (!allUnreadTransactionIDs) {
            allUnreadTransactionIDs = [NSMutableArray new];
        }
        
        callBacks = [NSMutableArray new];
    }
    
    return self;
}

-(void)userMobileChanged:(NSNotification*)notify {
    
    [self logoutUser];
}

-(void)logoutUser {
    
    [self markAllTransactionsAsViewed];
    
    if (self.requestTicket) {
        [self.requestTicket cancelTicket];
    }
    [self completeAllRefreshBlocksWithError:[NSError walnutErrorDefault]];
}

-(NSURL *)applicationDocumentsDirectory {
    
    NSURL *documents = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    return documents;
}

-(void)saveUnreadIds {
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"updatedIds.data"];
    [NSKeyedArchiver archiveRootObject:allUnreadTransactionIDs toFile:storeURL.path];
}

-(NSInteger)unreadMessagesCount {
    
    return allUnreadTransactionIDs.count;
}

-(void)markTransactionIDAsViewed:(NSString*)transId {
    
    if ([allUnreadTransactionIDs containsObject:transId]) {
        
        [allUnreadTransactionIDs removeObject:transId];
        [[NSNotificationCenter defaultCenter] postNotificationName:PaymentTransactionsCountChanged object:@(allUnreadTransactionIDs.count)];
    }
}

-(void)markAllTransactionsAsViewed {
    
    [allUnreadTransactionIDs removeAllObjects];
}

-(NSArray <WLTPaymentTransaction *> *)updateCoreDataTransactionFromGTLObjects:(NSArray <GTLWalnutMPaymentTransaction *>*)trans {
    
    NSArray *coreDataObjs = nil;
    if (trans.count) {
        
        NSString *currentMob = CURRENT_USER_MOB;
        WLTDatabaseManager *manager = [WLTDatabaseManager sharedManager];
        
        NSArray *allIds = [trans valueForKeyPath:@"transactionUuid"];
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"transactionId IN %@", allIds];
        NSArray *allTrans = [manager allObjectsOfClass:@"WLTPaymentTransaction" withPredicate:pred];
        
        NSMutableDictionary *dictMapping = [NSMutableDictionary new];
        for ( WLTPaymentTransaction *trans in allTrans ) {
            dictMapping[trans.transactionId] = trans;
        }
        
        NSMutableArray *updatedNow = [NSMutableArray new];
        NSMutableArray *toRemove = [NSMutableArray new];
        
        for ( GTLWalnutMPaymentTransaction *tranObj in trans ) {
            
            if (!tranObj.deleted.boolValue) {
                
                PaymentType type = [WLTPaymentTransaction paymentTypeFromString:tranObj.type];
                if ( (type == PaymentTypePush) || (type == PaymentTypePull) || (type == PaymentTypeRequestToMe) || (type == PaymentTypeRequestFromMe)) {
                    
                    PaymentStatus status = [WLTPaymentTransaction paymentStatusFromString:tranObj.status];
                    if ( ((type == PaymentTypeRequestToMe) || (type == PaymentTypeRequestFromMe)) && (status == PaymentStatusUnknown)) {
                        status = PaymentStatusRequestInitiated;
                    }
                    
                    BOOL isRelavent = NO;
                    if([tranObj.senderPhone isEqualToString:currentMob]) {
                        isRelavent = YES;
                    }
                    else if([tranObj.receiverPhone isEqualToString:currentMob]) {
                        isRelavent = YES;
                    }
                    
                    if ( (status > PaymentStatusPullFailed) && isRelavent) {
                        
                        WLTPaymentTransaction *newTrans = dictMapping[tranObj.transactionUuid];
                        if (!newTrans) {
                            newTrans = [manager objectOfClassString:@"WLTPaymentTransaction"];
                        }
                        
                        [newTrans updateValuesWithTransaction:tranObj];
                        
                        if([tranObj.senderPhone isEqualToString:currentMob]) {
                            newTrans.isPaidNumber = @(YES);
                        }
                        else if([tranObj.receiverPhone isEqualToString:currentMob]) {
                            newTrans.isPaidNumber = @(NO);
                        }
                        
                        [updatedNow addObject:newTrans];
                    }
                    else {
                        WLTPaymentTransaction *newTrans = dictMapping[tranObj.transactionUuid];
                        if (newTrans) {
                            [toRemove addObject:newTrans];
                        }
                    }
                }
            }
            else {
                
                WLTPaymentTransaction *newTrans = dictMapping[tranObj.transactionUuid];
                if (newTrans) {
                    [toRemove addObject:newTrans];
                }
            }
        }
        
        for ( WLTPaymentTransaction *oldTrans in toRemove ) {
            
            [oldTrans.managedObjectContext deleteObject:oldTrans];
        }
        
        coreDataObjs = updatedNow;
        [manager saveDataBase];
    }
    
    return coreDataObjs;
}

-(void)completeAllRefreshBlocksWithError:(NSError*)error {
    
    for ( TransactionsRefreshBlock blocks in callBacks ) {
        if (error) {
            blocks(NO, error);
        }else {
            blocks(YES, nil);
        }
    }
    
    [callBacks removeAllObjects];
}

-(void)fetchAllPaymentTransactionsCompletion:(TransactionsRefreshBlock)block{
    
    if (block) {
        [callBacks addObject:block];
    }
    
    if (!self.requestTicket) {
        
        __block WLTAppSettings *settings = [WLTDatabaseManager sharedManager].appSettings;
        __weak typeof(self) weakSelf = self;
        
        long long dateStamp = settings.lastPayTransactinsSyncTime.doubleValue;
        
        BOOL refetched = [[NSUserDefaults standardUserDefaults] boolForKey:@"TransactionsReFetched_v1_7"];
        if (!refetched) {
            dateStamp = 0;
        }
        
//#warning -
//        dateStamp = 0;
		
        GTLQueryWalnut *wlnut = [GTLQueryWalnut queryForPaymentTransactionGetWithDeviceUuid:settings.deviceIdentifier lastSyncTime:dateStamp];
        self.requestTicket = [[WLTNetworkManager sharedManager].walnutService executeQuery:wlnut completionHandler:^(GTLServiceTicket *ticket, GTLWalnutMPaymentTransactions *object, NSError *error) {
            
            BOOL isError = YES;
            if (object && [object isKindOfClass:[GTLWalnutMPaymentTransactions class]]) {
                
                isError = NO;
                
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"TransactionsReFetched_v1_7"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                settings.lastPayTransactinsSyncTime = object.lastSyncTime;
                [weakSelf handleNewPaymentObjects:object.transactions isFirstTime:NO];
            }
            
            if (isError) {
                if (!error) {
                    error = [NSError walnutErrorDefault];
                }
            }
            
            [weakSelf completeAllRefreshBlocksWithError:error];
            weakSelf.requestTicket = nil;
        }];
    }
}

-(void)handleNewPaymentObjects:(NSArray*)newObjects isFirstTime:(BOOL)firstTime{
    
    NSArray *allUpdated = [self updateCoreDataTransactionFromGTLObjects:newObjects];
    
    if (!firstTime) {
        
        BOOL changed = NO;
        for ( WLTPaymentTransaction *trans in allUpdated ) {
            
            //Show unread badge count only for pending transactions i.e you have to pay or you have to add card.
            if ([trans isPaymentRequired] || [trans isActionRequired]) {
                
                if (![allUnreadTransactionIDs containsObject:trans.transactionId]) {
                    
                    [allUnreadTransactionIDs addObject:trans.transactionId];
                    changed = YES;
                }
            }
        }
        
        if (changed) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:PaymentTransactionsCountChanged object:@(allUnreadTransactionIDs.count)];
        }
    }
    
    if (allUpdated.count) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:PaymentTransactionsChanged object:nil];
    }
}

-(void)coreDataPaymentTransactionForPredicate:(NSPredicate*)predicate completion:(TransactionFetchBlock)block {
    
    NSArray *allObjs = [[WLTDatabaseManager sharedManager] allObjectsOfClass:@"WLTPaymentTransaction" withPredicate:predicate];
    if(allObjs.count) {
        
        block([allObjs firstObject], nil);
    }else {
        
        [self fetchAllPaymentTransactionsCompletion:^(BOOL success, NSError *error) {
            
            WLTPaymentTransaction *toSend = nil;
            if (success) {
                
                NSArray *allObjs2 = [[WLTDatabaseManager sharedManager] allObjectsOfClass:@"WLTPaymentTransaction" withPredicate:predicate];
                if (allObjs2.count) {
                    toSend = [allObjs2 firstObject];
                }
            }
            
            if (!toSend && !error) {
                error = [NSError walnutErrorDefault];
            }
            
            block(toSend, error);
        }];
    }
}

@end
