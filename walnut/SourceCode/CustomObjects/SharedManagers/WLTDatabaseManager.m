//
//  WLTDatabaseManager.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 04/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTDatabaseManager.h"
#import "WLTLoggedInUser.h"
#import "WLTGroup.h"
#import "WLTUser.h"
#import "GTLWalnut.h"
#import "WLTPaymentConfig.h"
#import "WLTSplitComment.h"
#import "WLTPaymentTransationsManager.h"
#import "WLTNetworkManager.h"
#import "WLTInstrumentsManager.h"
#import "WLTGroupsManager.h"
#import "WLTContact.h"
#import "WLTTransactionUser.h"

static NSInteger MaximumRecentContacts = 5;

@interface WLTDatabaseManager () {
    
}

@property(nonatomic, strong) NSManagedObjectContext *objectContext;
@property(nonatomic, strong) NSManagedObjectContext *mainContext;
@property(nonatomic, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property(nonatomic, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

@implementation WLTDatabaseManager

#pragma mark Testing

- (void)logCountOfObjectsOfClass:(Class)cls {
    
    NSString *name = NSStringFromClass(cls);
    NSLog(@"%@:%@",name, @([self allObjectsOfClass:name].count));
}

- (void)logAllKindOfCoreDataObjectsCount {
    
    [self logCountOfObjectsOfClass:[WLTUser class]];
    [self logCountOfObjectsOfClass:[WLTLoggedInUser class]];
    [self logCountOfObjectsOfClass:[WLTGroup class]];
    [self logCountOfObjectsOfClass:[WLTAppSettings class]];
    
    [self logCountOfObjectsOfClass:NSClassFromString(@"WLTPaymentConfig")];
    [self logCountOfObjectsOfClass:NSClassFromString(@"WLTPaymentGroup")];
    [self logCountOfObjectsOfClass:NSClassFromString(@"WLTPaymentInstrument")];
    [self logCountOfObjectsOfClass:NSClassFromString(@"WLTPaymentTransaction")];
    [self logCountOfObjectsOfClass:NSClassFromString(@"WLTPaymentUser")];
    [self logCountOfObjectsOfClass:NSClassFromString(@"WLTSplitComment")];
    [self logCountOfObjectsOfClass:NSClassFromString(@"WLTTransactionUser")];
    [self logCountOfObjectsOfClass:NSClassFromString(@"WLTTransaction")];
    [self logCountOfObjectsOfClass:NSClassFromString(@"WLTOtherSettlement")];
}

#pragma mark -

+(WLTDatabaseManager*)sharedManager {
    
    static __strong WLTDatabaseManager *manager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
    
    return manager;
}

-(instancetype)init {
    
    self = [super init];
    if (self) {
        
//        [self logAllKindOfCoreDataObjectsCount];
        
        NSArray *allBlockedBins = [[NSUserDefaults standardUserDefaults] objectForKey:@"BlockedBins"];
        _blockedBins = allBlockedBins;
        
        NSArray *allSett = [self allObjectsOfClass:@"WLTAppSettings"];
        _appSettings = [allSett firstObject];
        if (!_appSettings) {
            _appSettings = [self objectOfClassString:@"WLTAppSettings"];
        }
        
        NSArray *allUsers = [self allObjectsOfClass:@"WLTLoggedInUser"];
        _currentUser = [allUsers firstObject];
        
        _paymentConfig = [[self allObjectsOfClass:@"WLTPaymentConfig"] firstObject];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveMainDataBase) name:UIApplicationWillTerminateNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveMainDataBase) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveMainDataBase) name:UIApplicationWillResignActiveNotification object:nil];
        
        [self deleteAllFailedComments];
    }
    
    return self;
}

-(void)deleteAllFailedComments {
    
    NSPredicate *statePred = [NSPredicate predicateWithFormat:@"stateNum != %@", @(WLTSplitCommentStatePosted)];
    NSArray *allFailed = [self allObjectsOfClass:@"WLTSplitComment" withPredicate:statePred];
    for ( WLTSplitComment *cmnt in allFailed ) {
        [self.objectContext deleteObject:cmnt];
    }
    
    [self saveDataBase];
}

-(void)updatePaymentConfigration:(GTLWalnutMPaymentConfig*)config {
    
    if (!self.paymentConfig) {
        
        WLTPaymentConfig *dbConfig = [[self allObjectsOfClass:@"WLTPaymentConfig"] firstObject];
        if (!dbConfig) {
            dbConfig = [self objectOfClassString:@"WLTPaymentConfig"];
        }
        
        _paymentConfig = dbConfig;
    }
    
    BOOL oldStatus = self.paymentConfig.p2pPaymentFlag.boolValue;
    BOOL oldSendReceiveStatus = self.paymentConfig.sendRequestEnabled.boolValue;
    
    self.paymentConfig.aliasId = config.aliasId;
    self.paymentConfig.cardPaymentFlag = config.cardPaymentFlag;
    self.paymentConfig.cardRegPgmId = config.cardRegPgmId;
    self.paymentConfig.flag = config.flag;
    self.paymentConfig.identifierProperty = config.identifierProperty;
    
    self.paymentConfig.p2pMaxLimit = config.p2pLimit;
    if (!self.paymentConfig.p2pMaxLimit) {
        self.paymentConfig.p2pMaxLimit = @(5000);
    }
    self.paymentConfig.p2pMinLimit = config.minP2pAmount;
    if (!self.paymentConfig.p2pMinLimit) {
        self.paymentConfig.p2pMinLimit = @(1);
    }
    
    self.paymentConfig.suspensionMessage = config.suspendedMessage;
    if (!self.paymentConfig.suspensionMessage.length) {
        self.paymentConfig.suspensionMessage = @"Please try sometime later.";
    }
    
    self.paymentConfig.txnPgmId = config.txnPgmId;
    self.paymentConfig.url = config.url;
    self.paymentConfig.userId = config.userId;
    self.paymentConfig.useUat = config.useUat;
    self.paymentConfig.termsCondUrl = config.tncUrl;
    
    if (![config.isSuspended isKindOfClass:[NSNull class]]) {
        self.paymentConfig.isSuspended = config.isSuspended;
    }else {
        self.paymentConfig.isSuspended = @(NO);
    }
    
//    self.paymentConfig.isSuspended = @(YES);
    
    BOOL haveBins = NO;
    NSMutableArray *allBlockedBins = [NSMutableArray new];
    if ([config.blockedBins isKindOfClass:[NSArray class]]) {
        
        for ( id objs in config.blockedBins ) {
            
            if ([objs isKindOfClass:[NSString class]]) {
                
                [allBlockedBins addObject:objs];
            }
            else if ([objs isKindOfClass:[GTLWalnutMPaymentBlockedBin class]]) {
                
                NSString *newValue = ((GTLWalnutMPaymentBlockedBin*)objs).value;
                [allBlockedBins addObject:newValue];
            }
        }
        
        if (allBlockedBins) {
            
            [[NSUserDefaults standardUserDefaults] setObject:allBlockedBins forKey:@"BlockedBins"];
            _blockedBins = allBlockedBins;
            haveBins = YES;
        }
    }
    
    if (!haveBins) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"BlockedBins"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    _blockedBins = allBlockedBins;
    
    BOOL weHaveBasicPaymentNeeds = NO;
    
    if ( self.paymentConfig.url.length && self.paymentConfig.aliasId.length && self.paymentConfig.userId.length) {
        weHaveBasicPaymentNeeds = YES;
    }
    
    self.paymentConfig.p2pPaymentFlag = @(NO);
    if (config.p2pPaymentFlag.boolValue && weHaveBasicPaymentNeeds ) {
        self.paymentConfig.p2pPaymentFlag = @(YES);
    }
    
    self.paymentConfig.sendRequestEnabled = @(NO);
    if (config.requestMoneyFlag.boolValue && weHaveBasicPaymentNeeds ) {
        self.paymentConfig.sendRequestEnabled = @(YES);
    }
    
    [self saveDataBase];
    
    if ( oldStatus != self.paymentConfig.p2pPaymentFlag.boolValue ) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NotifyPaymentsStatusChanged object:nil];
    }
    
    if ( oldSendReceiveStatus != self.paymentConfig.sendRequestEnabled.boolValue ) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NotifySendReceiveMoneyStatusChanged object:nil];
    }
}

-(BOOL)doesCurrentUserHaveMultipleGroupTransactionsWithUser:(id<User>)otherUser {
    
    BOOL haveMultiple = NO;
    
    NSString *otherUserNum = [otherUser completeNumber];
    
    NSPredicate *oweUserPred = [NSPredicate predicateWithFormat:@"ANY userGetFrom.mobileString == %@", otherUserNum];
    NSPredicate *getUserPred = [NSPredicate predicateWithFormat:@"ANY userPayTo.mobileString == %@", otherUserNum];
    NSPredicate *comUserPred = [NSCompoundPredicate orPredicateWithSubpredicates:@[oweUserPred, getUserPred]];
    
    NSArray *allGroups = [[WLTDatabaseManager sharedManager] allObjectsOfClass:@"WLTGroup" withPredicate:comUserPred];
    
    if (allGroups.count > 1) {
        haveMultiple = YES;
    }
    
    return haveMultiple;
}

#pragma mark - 

-(void)deleteTransactionFromDataBase:(WLTTransaction*)trans {
    
    if (trans.owner) {
        [self.objectContext deleteObject:trans.owner];
    }
    
    if (trans.addedBy) {
        [self.objectContext deleteObject:trans.addedBy];
    }
    
    if (trans.receiver) {
        [self.objectContext deleteObject:trans.receiver];
    }
    
    for ( WLTTransactionUser *splitU in trans.splits ) {
        
        [self.objectContext deleteObject:splitU];
    }
    [self.objectContext deleteObject:trans];
    
    [self saveDataBase];
}

-(void)deleteAllObjects {
    
    NSArray *allgrps = [self allObjectsOfClass:@"WLTGroup"];
    for (NSManagedObject *grp in allgrps) {
        [self.objectContext deleteObject:grp];
    }
    
    NSArray *allpays = [self allObjectsOfClass:@"WLTPaymentConfig"];
    for (NSManagedObject *pys in allpays) {
        [self.objectContext deleteObject:pys];
    }
    
    NSArray *allUsrs = [self allObjectsOfClass:@"WLTUser"];
    for (NSManagedObject *usrs in allUsrs) {
        [self.objectContext deleteObject:usrs];
    }
    
    NSArray *allTUsrs = [self allObjectsOfClass:@"WLTTransactionUser"];
    for (NSManagedObject *usrs in allTUsrs) {
        [self.objectContext deleteObject:usrs];
    }
    
    NSArray *allOtheSett = [self allObjectsOfClass:@"WLTOtherSettlement"];
    for (NSManagedObject *obj in allOtheSett) {
        [self.objectContext deleteObject:obj];
    }
    
    NSArray *allTrans = [self allObjectsOfClass:@"WLTPaymentTransaction"];
    for (NSManagedObject *usrs in allTrans) {
        [self.objectContext deleteObject:usrs];
    }
    
    NSArray *allInstr = [self allObjectsOfClass:@"WLTPaymentInstrument"];
    for (NSManagedObject *usrs in allInstr) {
        [self.objectContext deleteObject:usrs];
    }
    
    [self saveMainDataBase];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NotifyPaymentsStatusChanged object:nil];
}

-(void)logOut {
    
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    [[WLTNetworkManager sharedManager] logoutCurrrentUser];
    [[WLTPaymentTransationsManager sharedManager] logoutUser];
    [[WLTInstrumentsManager sharedManager] logoutUser];
    [[WLTGroupsManager sharedManager] logoutUser];
    
    if (self.appSettings) {
        [self.objectContext deleteObject:self.appSettings];
        _appSettings = nil;
    }
    _appSettings = [self objectOfClassString:@"WLTAppSettings"];
    
    if (self.paymentConfig) {
        [self.objectContext deleteObject:self.paymentConfig];
        _paymentConfig = nil;
    }
    
    if (self.currentUser) {
        [self.objectContext deleteObject:self.currentUser];
        self.currentUser = nil;
    }
    
    [self deleteAllObjects];
}

-(void)deleteGroups:(NSArray*)groups {
    
    for ( WLTGroup *group in groups) {
        
        NSString *grpID = group.groupUUID;
        [self.objectContext deleteObject:group];
        [[NSNotificationCenter defaultCenter] postNotificationName:NotifyGroupDeleted object:grpID];
    }
    
    [self saveDataBase];
}

-(void)removeObjects:(NSSet*)objectsSet fromContext:(NSManagedObjectContext*)context {
    
    NSArray *objectsArray = objectsSet.allObjects;
    for ( NSManagedObject *obj in objectsArray ) {
        [context deleteObject:obj];
    }
}

-(void)addGroups:(NSArray*)grpArray completion:(SuccessBlock)block {
    
    NSArray *allSavedCats = [self allObjectMatchingProperty:@"groupUUID" inArray:grpArray arrayKey:@"uuid" ofClass:NSStringFromClass([WLTGroup class])];
    NSMutableDictionary *dictMapping = [NSMutableDictionary new];
    
    for ( WLTGroup *grp in allSavedCats ) {
        dictMapping[grp.groupUUID] = grp;
    }
    
    for ( GTLWalnutMGroup *grps in grpArray ) {
        
        WLTGroup *saved = dictMapping[grps.uuid];
        if (!grps.deleted.boolValue) {
            
            if (!saved) {
                saved = [self objectOfClassString:@"WLTGroup"];
            }
            [saved updateGroupFromWLTGroup:grps];
        }
        else if (saved) {
            
            NSString *grpID = saved.groupUUID;
            [self.objectContext deleteObject:saved];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:NotifyGroupDeleted object:grpID];
        }
    }
    
    [self saveDataBase];
    
    if (block) {
        block(YES);
    }
}

-(void)setCurrentUser:(WLTLoggedInUser *)currentUser {
    
    if (_currentUser) {
        [self.objectContext deleteObject:_currentUser];
    }
    
    _currentUser = currentUser;
    [self saveMainDataBase];
}

#pragma mark - Inserting Database Objects

-(id)objectOfClassString:(NSString*)classStr{
    
    return [self objectOfClassString:classStr andContext:self.objectContext];
}

-(id)objectOfClassString:(NSString*)classStr andContext:(NSManagedObjectContext*)context{
    
    if (!context) {
        return nil;
    }
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:classStr inManagedObjectContext:context];
    return [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
}

#pragma mark - Retriving Database Objects

-(NSArray *)allObjectMatchingProperty:(NSString*)prop inArray:(NSArray *)dataArray arrayKey:(NSString *)distinctKey ofClass:(NSString *)entity{
    
    NSArray *distinct = [dataArray valueForKeyPath:[NSString stringWithFormat:@"@distinctUnionOfObjects.%@", distinctKey]];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%@ CONTAINS %K", distinct, prop];
    [fetchRequest setPredicate:predicate];
    
    return [self.objectContext executeFetchRequest:fetchRequest error:nil];;
}

-(NSArray*)allObjectsOfClass:(NSString*)className {
    return [self allObjectsOfClass:className withPredicate:nil andSortDescriptors:nil];
}

-(NSArray*)allObjectsOfClass:(NSString*)className withPredicate:(NSPredicate*)predicate {
    return [self allObjectsOfClass:className withPredicate:predicate andSortDescriptors:nil];
}

-(NSArray*)allObjectsOfClass:(NSString*)className andSortDescriptors:(NSArray*)sortDescris {
    return [self allObjectsOfClass:className withPredicate:nil andSortDescriptors:sortDescris];
}

- (NSArray*)allObjectsOfClass:(NSString*)className withPredicate:(NSPredicate*)predicate andSortDescriptors:(NSArray*)sortDescri{
    return [self allObjectsOfClass:className withPredicate:predicate andSortDescriptors:sortDescri andLimit:0];
}

-(NSArray*)allObjectsOfClass:(NSString*)className withPredicate:(NSPredicate*)predicate andSortDescriptors:(NSArray*)sortDescri andLimit:(NSInteger)limit{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    if (limit > 0) {
        [fetchRequest setFetchLimit:limit];
    }
    NSEntityDescription *entity = [NSEntityDescription entityForName:className inManagedObjectContext:self.objectContext];
    [fetchRequest setEntity:entity];
    
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    if (sortDescri) {
        [fetchRequest setSortDescriptors:sortDescri];
    }
    
    return [self resultsForFetchRequest:fetchRequest];
}

-(NSArray*)resultsForFetchRequest:(NSFetchRequest*)request {
    
    NSError *error = nil;
    return [self.objectContext executeFetchRequest:request error:&error];
}

-(NSArray*)distinctObjectsOfClass:(NSString*)classNa forProperties:(NSArray*)props {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:classNa];
    fetchRequest.propertiesToFetch = props;
    fetchRequest.resultType = NSDictionaryResultType;
    
    fetchRequest.returnsDistinctResults = YES;
    
    return [self resultsForFetchRequest:fetchRequest];
}

#pragma mark - Counting

-(NSInteger)countForObjectsOfClass:(NSString*)className {
    return [self countForObjectsOfClass:className withPredicate:nil];
}

-(NSInteger)countForObjectsOfClass:(NSString*)className withPredicate:(NSPredicate*)predicate{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:className inManagedObjectContext:self.objectContext];
    [fetchRequest setEntity:entity];
    
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSError *error = nil;
    return [self.objectContext countForFetchRequest:fetchRequest error:&error];
}

#pragma mark - Core Data

-(void)saveDataBase {
    
    NSError *error = nil;
    [self.objectContext save:&error];
    if (error) {
        NSLog(@"Error Saving Background Context %@", error);
    }
}

-(void)removeAllScrapObjects {
    
}

-(void)saveMainDataBase {
    
    [self removeAllScrapObjects];
    [self saveDataBase];
    
    [[WLTPaymentTransationsManager sharedManager] saveUnreadIds];
    
    NSError *error = nil;
    [self.mainContext save:&error];
    if (error) {
        NSLog(@"Error Saving Main Context %@", error);
    }
}

-(NSURL *)applicationDocumentsDirectory {
    
    NSURL *documents = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    return documents;
}

-(NSManagedObjectContext *)objectContext {
    
    if (!_objectContext) {
        _objectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [_objectContext setParentContext:self.mainContext];
    }
    return _objectContext;
}

-(NSManagedObjectContext *)mainContext {
    
    if (!_mainContext) {
        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
        if (coordinator != nil) {
            _mainContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
            [_mainContext setPersistentStoreCoordinator:coordinator];
        }
    }
    return _mainContext;
}

-(NSManagedObjectModel *)managedObjectModel {
    
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"walnut" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

-(NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    if (!_persistentStoreCoordinator) {
        
        NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"walnut"];
        NSError *error = nil;
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        // If the expected store doesn't exist, copy the default store.
        if (![fileManager fileExistsAtPath:[storeURL path]]) {
            NSURL *defaultStoreURL = [[NSBundle mainBundle] URLForResource:@"walnut" withExtension:@"sqlite"];
            if (defaultStoreURL) {
                NSError *error = nil;
                [fileManager copyItemAtURL:defaultStoreURL toURL:storeURL error:&error];
            }
        }
        
        NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES};
        _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        
        if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil
                                                                 URL:storeURL
                                                             options:options error:&error]) {
            
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Recent List

-(NSArray*)recentContactsList {
    
    NSURL *docsUrl = [self applicationDocumentsDirectory];
    NSArray *recent = [NSKeyedUnarchiver unarchiveObjectWithFile:[docsUrl URLByAppendingPathComponent:@"recentContacts.plist"].path];
    return recent;
}

-(void)saveContactToRecentList:(WLTContact*)cont {
    
    NSURL *docsUrl = [self applicationDocumentsDirectory];
    
    NSString *recentContPath = [docsUrl URLByAppendingPathComponent:@"recentContacts.plist"].path;
    NSMutableArray *recent = [NSKeyedUnarchiver unarchiveObjectWithFile:recentContPath];
    if (!recent) {
        recent = [NSMutableArray new];
    }
    
    NSString *monNum = [cont completeNumber];
    if ( monNum.length ) {
        
        NSDictionary *oldCont = nil;
        for ( NSDictionary *prevDict in recent ) {
            if ([monNum isEqualToString:prevDict[@"mobileString"]]) {
                oldCont = prevDict;
                break;
            }
        }
        
        if (oldCont) {
            
            [recent removeObject:oldCont];
            [recent insertObject:oldCont atIndex:0];
        }else {
            
            NSMutableDictionary *dict = [NSMutableDictionary new];
            dict[@"mobileString"] = monNum;
            dict[@"name"] = [cont fullName];
            
            [recent insertObject:dict atIndex:0];
        }
    }
    
    if (recent.count > MaximumRecentContacts) {
        
        [recent removeObjectsInRange:NSMakeRange(MaximumRecentContacts, (recent.count - MaximumRecentContacts))];
    }
    
    [NSKeyedArchiver archiveRootObject:recent toFile:recentContPath];
}

@end
