//
//  WLTInstrumentsManager.h
//  walnut
//
//  Created by Abhinav Singh on 05/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString * const InstrumentTypeBank;
FOUNDATION_EXPORT NSString * const InstrumentTypeCards;
FOUNDATION_EXPORT NSString *const NotifyCardChanged;

#import "WLTFlowConstants.h"

@class GTLWalnutMPaymentInstrument;

typedef void (^ CardsFetchBlock)(NSArray *cards, NSError *error);

typedef NS_ENUM(NSInteger, PaymentInstrumentType) {
    
    PaymentInstrumentTypeBoth = 0,
    PaymentInstrumentTypePay,
    PaymentInstrumentTypeReceive,
};

typedef NS_ENUM(NSInteger, IntrumentManagerState) {
    
    IntrumentManagerStateInitial = 0,
    IntrumentManagerStateDownloading,
};

@interface WLTInstrumentsCallback : NSObject {
    
}

@property(nonatomic, strong) DataCompletionBlock callBack;
@property(nonatomic, assign) PaymentInstrumentType type;

@end

@interface WLTInstrumentsManager : NSObject {
    
}

@property(readonly, strong) GTLWalnutMPaymentInstrument *defaultReceiveInstrument;
@property(readonly, assign) IntrumentManagerState state;

+(WLTInstrumentsManager*)sharedManager;
-(void)logoutUser;

-(void)validCardsForTransactionType:(PaymentInstrumentType)type
                    invalidateCache:(BOOL)cached
                     withCompletion:(DataCompletionBlock)block;

-(void)addInstrumentToWalnut:(GTLWalnutMPaymentInstrument*)instr
             withCompletion:(DataCompletionBlock)block;

-(void)removeInstrumentFromWalnut:(GTLWalnutMPaymentInstrument*)instr
              withCompletion:(DataCompletionBlock)block;

-(void)showNewDefaultPopUpForIntrument:(GTLWalnutMPaymentInstrument*)instrument flowType:(WLTEntryFlowType)flowType;
-(void)showMakeNewDefaultPopUpForIntrument:(GTLWalnutMPaymentInstrument*)instrument completion:(DataCompletionBlock)block;

-(BOOL)sendingInstrumentsContainsCards;
-(GTLWalnutMPaymentInstrument*)defaultReceivingCard;

@end
