//
//  WLTNetworkManager.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 08/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTLWalnut.h"
#import "GTLServiceWalnut.h"
#import "Reachability.h"

@class WLTLoginTempUser;
@class WLTGroup;

FOUNDATION_EXPORT NSString *const WalnutClientID;
FOUNDATION_EXPORT NSString *const WalnutAPIKey;

@interface WLTNetworkManager : NSObject {
    
}

@property(readonly, strong) GTLServiceWalnut *walnutService;
@property(readonly, strong) Reachability *internetReachability;

-(void)refreshAuthState;
-(void)logoutCurrrentUser;

+(WLTNetworkManager*)sharedManager;
+(NSString*)generateUniqueIdentifier;

-(void)setUPUserProfile:(GTLWalnutMUserProfile*)profile forUser:(WLTLoginTempUser*)userTemp withCompletion:(DataCompletionBlock)block;
-(void)getUserProfilesWithCompletion:(DataCompletionBlock)block;

-(void)postPushNotificationAgain;

-(void)getOTPForUser:(WLTLoginTempUser*)user completion:(DataCompletionBlock)block;
-(void)validateOTP:(NSString*)otp forUser:(WLTLoginTempUser*)user completion:(DataCompletionBlock)block;

@end
