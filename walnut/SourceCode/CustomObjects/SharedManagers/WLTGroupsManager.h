//
//  WLTGroupsManager.h
//  walnut
//
//  Created by Abhinav Singh on 01/08/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTLWalnut.h"
#import "GTLServiceWalnut.h"

FOUNDATION_EXPORT NSString *const RefreshedGroupsList;

typedef void (^ MultiGroupsDetailBlock)(NSArray *changedGrps, NSError *error);
typedef void (^ GroupDetailsBlock)(WLTGroup *group, NSError *error);

@interface WLTGroupsManager : NSObject {
    
    NSArray *validTransactionTypes;
}

-(void)logoutUser;

+(WLTGroupsManager*)sharedManager;

//Fetch group_list and then group_details.
-(void)fetchAllChangedGroups:(MultiGroupsDetailBlock)block;

//Fetch group_list and then details of groups only in parameter grpIdsArr.
-(void)fetchEveryChangeOfGroupsIDS:(NSArray*)grpIdsArr completion:(MultiGroupsDetailBlock)block;

//Fetch group_list and then details of group.
-(void)fetchEveryChangeOfGroup:(WLTGroup*)grp completion:(GroupDetailsBlock)block;

//Fetch only details of group.
-(void)fetchDetailsOfGroup:(WLTGroup*)grp completion:(GroupDetailsBlock)block;

//fetch only group_list.
-(void)fetchChangedGroupsList:(MultiGroupsDetailBlock)block;

@end
