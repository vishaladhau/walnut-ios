//
//  WLTNetworkManager.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 08/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTNetworkManager.h"
#import "WLTRootViewController.h"
#import "SSKeychain.h"
#import "WLTDatabaseManager.h"
#import "WLTGroup.h"
#import "WLTLoggedInUser.h"
#import "WLTTransactionUser.h"
#import "WLTTransaction.h"
#import "WLTColorPalette.h"
#import "WLTProfilesSelectionView.h"
#import "UIView+Loading.h"
#import "WLTSplitComment.h"
#import "WLTOtherSettlement.h"
#import "WLTTargetSettings.h"
#import "WLTLoginTempUser.h"

typedef NS_ENUM(NSInteger, AuthorizationState) {
    
    AuthorizationStateUnAuthorized = 0,
    AuthorizationStateAuthorizing,
    AuthorizationStateAuthorized,
};

@interface WLTAuthCallback : NSObject{
    
}

@property(nonatomic, weak) NSMutableURLRequest *request;
@property(nonatomic, weak) id delegate;
@property(nonatomic, assign) SEL selector;

@end

@implementation WLTAuthCallback

@end

@interface WLTNetworkManager () <GTMFetcherAuthorizationProtocol>{
    
    NSMutableArray *requestAuthCallBacks;
}

@property(nonatomic, assign) WLTLoginTempUser *tempUser;
@property(nonatomic, assign) AuthorizationState authState;

@end

@implementation WLTNetworkManager

@dynamic userEmail;
@dynamic canAuthorize;

+(NSString *)generateUniqueIdentifier {
    
    NSString *uuid = [[[NSUUID UUID] UUIDString] lowercaseString];
    return uuid;
}

+(WLTNetworkManager*)sharedManager {
    
    static __strong WLTNetworkManager *manager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
    
    return manager;
}

- (instancetype)init {
    
    self = [super init];
    if (self) {
        
        requestAuthCallBacks = [NSMutableArray new];
        
        _walnutService = [[GTLServiceWalnut alloc] init];
        _walnutService.APIKey = WalnutAPIKey;
        _walnutService.retryEnabled = YES;
        
        _walnutService.authorizer = self;
        
        [self refreshAuthState];
        
        _internetReachability = [Reachability reachabilityForInternetConnection];
        [_internetReachability startNotifier];
    }
    
    return self;
}

-(void)getUserProfilesWithCompletion:(DataCompletionBlock)block {
    
    GTLQueryWalnut *users = [GTLQueryWalnut queryForUserGet];
    
    [self.walnutService executeQuery:users completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
        
        if (!error) {
            
            if (block) {
                block(object);
            }
        }else {
            
            if (block) {
                block(error);
            }
        }
    }];
}

-(void)postPushNotificationAgain {
    
    __weak WLTNetworkManager *weakSelf = self;
    
    NSString *currentIdentifier = [WLTDatabaseManager sharedManager].appSettings.deviceIdentifier;
    
    if (currentIdentifier.length) {
        
        [self getUserProfilesWithCompletion:^(GTLWalnutMUserProfiles *data) {
            
            if ([data isKindOfClass:[GTLWalnutMUserProfiles class]]) {
                if(data.userProfiles.count > 0) {
                    
                    for ( GTLWalnutMUserProfile *prfl in data.userProfiles ) {
                        
                        if ([prfl.deviceUuid isEqualToString:currentIdentifier]) {
                            
                            [weakSelf setUPUserProfile:prfl forUser:nil withCompletion:nil];
                            break;
                        }
                    }
                }
            }
        }];
    }
}

-(void)setUPUserProfile:(GTLWalnutMUserProfile*)profile forUser:(WLTLoginTempUser*)userTemp withCompletion:(DataCompletionBlock)block {
    
    self.tempUser = userTemp;
//#warning -
//    profile.gcmRegistrationId =@"cRBa66IDWsU:APA91bHjnnd_f_O5M8H86DsUkGimLnkgn9ozrHPhObyRhtnIicqS8IerS29xSnLaBPLQeI4pBE_BAt47R1OHpCslSpFbtZqkIc3mvkn7YqQbWhI084zKUI1qzSwetePUDugl7PY6UlEL";// [WLTDatabaseManager sharedManager].appSettings.pushToken;
    profile.gcmRegistrationId = [WLTDatabaseManager sharedManager].appSettings.pushToken;
    profile.platform = @"ios";
    
    __weak typeof(self) weakSelf = self;
    
    GTLQueryWalnut *userProfile = [GTLQueryWalnut queryForUserProfileSetWithObject:profile];
    [self.walnutService executeQuery:userProfile completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
        
        weakSelf.tempUser = nil;
        
        if (!error) {
            
            if (block) {
                block(profile);
            }
        }else {
            
            if (block) {
                block(error);
            }
        }
    }];
}

//-(void)sendOTPToNumber:(NSString*)number name:(NSString*)userName completion:(DataCompletionBlock)completion {
//    
//    GTLQueryWalnut *grpDetail = [GTLQueryWalnut queryForOtpGetWithName:@"" mobileNumber:number];
//    
//    [self.walnutService executeQuery:grpDetail completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
//        
//        if (!error) {
//            
//            if (completion) {
//                completion(@(1));
//            }
//        }else {
//            if (completion) {
//                completion(error);
//            }
//        }
//    }];
//}
//
//-(void)verifyOTP:(NSString*)otp forNumber:(NSString*)number name:(NSString*)userName completion:(DataCompletionBlock)completion {
//    
//    GTLQueryWalnut *verify = [GTLQueryWalnut queryForOtpValidateWithOtp:otp
//                                                              mobileNumber:number
//                                                                deviceUuid:[WLTDatabaseManager sharedManager].appSettings.deviceIdentifier
//                                                                      name:userName];
//    
//    [self.walnutService executeQuery:verify completionHandler:^(GTLServiceTicket *ticket, GTLWalnutMMobileNumber *object, NSError *error) {
//        
//        if ( !error ) {
//            if (![object isKindOfClass:[GTLWalnutMMobileNumber class]]) {
//                error = [NSObject walnutErrorDefault];
//            }else if (!object.valid.boolValue) {
//                error = [NSObject walnutErrorWithMessage:@"Please enter a valid OTP!"];
//            }
//        }
//        
//        if (error) {
//            if (completion) {
//                completion(error);
//            }
//        }else {
//            
//            [[WLTDatabaseManager sharedManager] updatePaymentConfigration:object.paymentConfig];
//            
//            if (completion) {
//                completion(@(1));
//            }
//        }
//    }];
//}

-(void)fetchHelloAuth {
    
    GTLQueryWalnut *walN = [GTLQueryWalnut queryForHelloauth];
    [self.walnutService executeQuery:walN completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
        
    }];
}

-(void)logoutCurrrentUser {
    
    self.authState = AuthorizationStateUnAuthorized;
    [self completeAllRequestCallBacksWithError:[NSObject walnutErrorDefault]];
    
    GTLServiceWalnut *walnutService = [[GTLServiceWalnut alloc] init];
    walnutService.APIKey = WalnutAPIKey;
    walnutService.retryEnabled = YES;
    walnutService.authorizer = self;
    
    _walnutService = walnutService;
}

#pragma mark - Payments

-(void)getPaymentInstruments:(DataCompletionBlock)block {
    
    WLTAppSettings *settings = [WLTDatabaseManager sharedManager].appSettings;
    long long dateStamp = 0;//[settings.lastPayInstrumentsSyncTime timeStamp];
    
    GTLQueryWalnut *wlnut = [GTLQueryWalnut queryForPaymentInstrumentListWithDeviceUuid:settings.deviceIdentifier lastSyncTime:dateStamp];
    [[WLTNetworkManager sharedManager].walnutService executeQuery:wlnut completionHandler:^(GTLServiceTicket *ticket, GTLWalnutMPaymentInstruments *object, NSError *error) {
        
        if (!object || ![object isKindOfClass:[GTLWalnutMPaymentInstruments class]]) {
            if (!error) {
                error = [NSObject walnutErrorDefault];
            }
        }
        
        if (block) {
            if (error) {
                block(error);
            }else {
                block(object);
            }
        }
    }];
}

#pragma mark - Auth

-(void)refreshAuthState {
    
    if ([WLTDatabaseManager sharedManager].currentUser.wltAuthToken.length) {
        self.authState = AuthorizationStateAuthorized;
    }else {
        self.authState = AuthorizationStateUnAuthorized;
    }
}

//mobile_number (required)
//otp (required)
//device_uuid (required)
//name (required, as provided during registration)
//email (if provided during registration)

-(void)validateOTP:(NSString*)otp forUser:(WLTLoginTempUser*)user completion:(DataCompletionBlock)block{
    
    __weak typeof(self) weakSelf = self;
    
    NSString *emailAdd = user.emailAddress;
    if (!emailAdd) {
        emailAdd = @"";
    }
    
    self.authState = AuthorizationStateAuthorizing;
    
    NSURL *validateOTPURL = [NSURL URLWithString:ValidateOTPUrl];
    NSMutableURLRequest *validateOTPRequest = [[NSMutableURLRequest alloc] initWithURL:validateOTPURL];
    validateOTPRequest.HTTPMethod = @"POST";
    
    NSDictionary *validateOTPRequestDict = @{@"mobile_number":[user.mobileNumber stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                                             @"otp":otp,
                                             @"name":user.userName,
                                             @"email":emailAdd,
                                             @"device_uuid":user.deviceUUID,
                                             };
    
    NSData *bodyData = [NSJSONSerialization dataWithJSONObject:validateOTPRequestDict options:0 error:nil];
    
    validateOTPRequest.HTTPBody = bodyData;
    [validateOTPRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [NSURLConnection sendAsynchronousRequest:validateOTPRequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        
        NSDictionary *jsonDict = nil;
        if (!connectionError) {
            
            jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSString *token = jsonDict[@"a_token"];
            if (!token.length) {
                jsonDict = nil;
            }
        }
        
        if (!jsonDict) {
            
            if (!connectionError) {
                connectionError = [NSError walnutErrorDefault];
            }
            
            [weakSelf completeAllRequestCallBacksWithError:connectionError];
            
            if (block) {
                block(connectionError);
            }
        }else {
            
            [weakSelf completeAllRequestCallBacksWithError:nil];
            
            if (block) {
                block(jsonDict);
            }
        }
    }];
}

-(void)getOTPForUser:(WLTLoginTempUser*)user
            completion:(DataCompletionBlock)block {
    
    NSURL *registerURL = [NSURL URLWithString:RegisterAPIUrl];
    NSMutableURLRequest *registerRequest = [[NSMutableURLRequest alloc] initWithURL:registerURL];
    registerRequest.HTTPMethod = @"POST";
    NSString *emailAdd = user.emailAddress;
    if (!emailAdd) {
        emailAdd = @"";
    }
    
    NSDictionary *regRequestDict = @{@"mobile_number":user.mobileNumber,
                                     @"name":user.userName,
                                     @"voice":@(!user.throughSMS),
                                     @"source":@"iOS",
                                     @"email":emailAdd
                                     };
    
    NSData *bodyData = [NSJSONSerialization dataWithJSONObject:regRequestDict options:0 error:nil];
    
    registerRequest.HTTPBody = bodyData;
    [registerRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [NSURLConnection sendAsynchronousRequest:registerRequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        
        NSDictionary *jsonDict = nil;
        if (!connectionError) {
            jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        }
        
        if (!jsonDict) {
            
            if (!connectionError) {
                connectionError = [NSError walnutErrorDefault];
            }
            
            if (block) {
                block(connectionError);
            }
        }else {
            
            if (block) {
                block(jsonDict);
            }
        }
    }];
}

#pragma mark - Authenticating Requests

-(BOOL)canAuthorize {
    
    return YES;
}

-(void)addAuthorizationHeaderToRequest:(NSMutableURLRequest*)request {
    
    NSString *token = [WLTDatabaseManager sharedManager].currentUser.wltAuthToken;
    NSString *mobile = [WLTDatabaseManager sharedManager].currentUser.mobileString;
    
    if ( token.length && mobile.length ) {
        
        NSString *tok = [NSString stringWithFormat:@"%@:%@", mobile, token];
        [request setValue:tok forHTTPHeaderField:@"WAuthorization"];
    }
}

-(void)authorizeRequest:(NSMutableURLRequest *)request delegate:(id)delegate didFinishSelector:(SEL)sel {
    
    WLTAuthCallback *callBack = [[WLTAuthCallback alloc] init];
    callBack.request = request;
    callBack.delegate = delegate;
    callBack.selector = sel;
    
    if(self.tempUser.authToken.length && self.tempUser.mobileNumber.length) {
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:request.HTTPBody options:0 error:nil];
        NSString *method = dict[@"method"];
        
        if ([method isEqualToString:@"walnut.user_profile.set"]) {
            
            NSString *tok = [NSString stringWithFormat:@"%@:%@", self.tempUser.mobileNumber, self.tempUser.authToken];
            [request setValue:tok forHTTPHeaderField:@"WAuthorization"];
            
            [self completeCallBack:callBack withErorr:nil];
            return;
        }
    }
    
    if (self.authState == AuthorizationStateAuthorized) {
        
        [self addAuthorizationHeaderToRequest:request];
        [self completeCallBack:callBack withErorr:nil];
    }else if (self.authState == AuthorizationStateAuthorizing) {
        
        [requestAuthCallBacks addObject:callBack];
    }else {
        
        [self completeCallBack:callBack withErorr:[NSObject walnutErrorDefault]];
    }
}

-(void)stopAuthorization {
    
    [requestAuthCallBacks removeAllObjects];
}

-(void)stopAuthorizationForRequest:(NSURLRequest *)request {
    
    WLTAuthCallback *toComplete = nil;
    for ( WLTAuthCallback *calBacks in requestAuthCallBacks ) {
        if (calBacks.request == request) {
            toComplete =calBacks;
            break;
        }
    }
    
    [requestAuthCallBacks removeObject:toComplete];
}

-(BOOL)isAuthorizingRequest:(NSURLRequest *)request {
    
    BOOL authorizing = NO;
    for ( WLTAuthCallback *calBacks in requestAuthCallBacks ) {
        
        if (calBacks.request == request) {
            authorizing = YES;
            break;
        }
    }
    
    return authorizing;
}

- (BOOL)isAuthorizedRequest:(NSURLRequest *)request {
    
    if ([request valueForHTTPHeaderField:@"WAuthorization"].length) {
        return YES;
    }
    
    return NO;
}

#pragma mark CallBacks 

-(void)completeAllRequestCallBacksWithError:(NSError*)error {
    
    if (self.authState != AuthorizationStateAuthorized) {
        if (!error) {
            error = [NSObject walnutErrorDefault];
        }
    }
    
    for ( WLTAuthCallback *callBack in requestAuthCallBacks.objectEnumerator ) {
        
        if (!error) {
            [self addAuthorizationHeaderToRequest:callBack.request];
        }
        [self completeCallBack:callBack withErorr:error];
    }
    [requestAuthCallBacks removeAllObjects];
}

-(void)completeCallBack:(WLTAuthCallback*)callBack withErorr:(NSError*)error{
    
    NSMutableURLRequest *req = callBack.request;
    WLTNetworkManager *man = self;
    
    NSMethodSignature *sig = [callBack.delegate methodSignatureForSelector:callBack.selector];
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:sig];
    
    [invocation setSelector:callBack.selector];
    [invocation setTarget:callBack.delegate];
    [invocation setArgument:&man atIndex:2];
    [invocation setArgument:&req atIndex:3];
    [invocation setArgument:&error atIndex:4];
    
    [invocation invoke];
}

@end