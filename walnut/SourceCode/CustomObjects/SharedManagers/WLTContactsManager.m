//
//  WLTContactsManager.m
//  walnut
//
//  Created by Abhinav Singh on 24/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTContactsManager.h"
#import "WLTMobileNumber.h"
#import "WLTContact.h"
#import "WLTDatabaseManager.h"

NSString *const NotifyContactListChanged = @"NotifyContactListChanged";

@import AddressBookUI;

@interface WLTContactsManager () {
    
}

@property(nonatomic, assign) ContactsManagerState managerState;
@property(nonatomic, strong) NSMutableDictionary *mappingDictionary;

@property(nonatomic, strong) NSMutableArray *numberCallBackArray;
@property(nonatomic, strong) NSMutableArray *allContactsCallBackArray;

@property(nonatomic, strong) NSArray *allContacts;

@end

@implementation WLTContactsManager

void addressBookChanged(ABAddressBookRef addressBook, CFDictionaryRef info, void *context) {
    
    [[WLTContactsManager sharedManager] refreshContactsList];
}

+(WLTContactsManager*)sharedManager {
    
    static __strong WLTContactsManager *manager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
    
    return manager;
}

-(instancetype)init {
    
    self = [super init];
    if (self) {
        
        self.mappingDictionary = [NSMutableDictionary new];
		self.numberCallBackArray = [NSMutableArray new];
		self.allContactsCallBackArray = [NSMutableArray new];
        
		self.managerState = ContactsManagerStateInitial;
		
        ABAddressBookRef ntificationaddressbook = ABAddressBookCreate();
        ABAddressBookRegisterExternalChangeCallback(ntificationaddressbook, addressBookChanged, (__bridge void *)(self));
    }
    
    return self;
}

-(void)refreshContactsList {
	
	if (self.managerState != ContactsManagerStateFetching) {
		
		__weak typeof(self) weakSelf = self;
		[self checkPermissionsAndFetchAllUsersCompletion:^(id data) {
			
			[weakSelf completeAllCallBacks];
			
			if (data && [data isKindOfClass:[NSArray class]]) {
                
				[[NSNotificationCenter defaultCenter] postNotificationName:NotifyContactListChanged object:nil];
			}
		}];
	}
}

-(void)fetchAllContacts {
	
	ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, nil);
	// Get all contacts from the address book
	NSArray *allPeople = (__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBook);
	
	NSMutableArray *allContactsArray = [NSMutableArray new];
	NSMutableDictionary *tempDuplicateMapping = [NSMutableDictionary new];
	
	for (id person in allPeople) {
		
		NSMutableArray *tempContacts = [NSMutableArray new];
		
		ABMultiValueRef multi = ABRecordCopyValue((__bridge ABRecordRef)person, kABPersonPhoneProperty);
		CFIndex tCount = ABMultiValueGetCount(multi);
		
		for ( CFIndex j = 0; j < tCount ; j++ ) {
			
			NSString* phone = (__bridge_transfer NSString*)ABMultiValueCopyValueAtIndex(multi, j);
			if (phone.length) {
				
				CFStringRef locLabel = ABMultiValueCopyLabelAtIndex(multi, j);
				CFStringRef phoneNumberLocalizedLabel = ABAddressBookCopyLocalizedLabel(locLabel);
				
                NSString *completeStr = phone.mobileNumberWithCountryCode;
                if (completeStr.length && !tempDuplicateMapping[completeStr]) {
                    
                    if ([completeStr isValidIndianMobile]) {
                        
                        WLTMobileNumber *mobileNum = [[WLTMobileNumber alloc] init];
                        mobileNum.label = (__bridge_transfer NSString*)phoneNumberLocalizedLabel;
                        mobileNum.number = completeStr;
                        
                        [tempContacts addObject:mobileNum];
                    }
                }
			}
		}
		
		if (tempContacts.count) {
			
			NSString *fullName = nil;
			
			NSString *firstName = (__bridge_transfer NSString*)ABRecordCopyValue((__bridge ABRecordRef)(person), kABPersonFirstNameProperty);
			NSString *lastName = (__bridge_transfer NSString*)ABRecordCopyValue((__bridge ABRecordRef)(person), kABPersonLastNameProperty);
			
			if (firstName.length) {
				fullName = firstName;
			}
			
			if (lastName) {
				if (fullName.length) {
					fullName = [fullName stringByAppendingFormat:@" %@", lastName];
				}else {
					fullName = lastName;
				}
			}
			
			for ( WLTMobileNumber *mobileNum in tempContacts ) {
				
				NSString *keyStr = mobileNum.number;
				
				self.mappingDictionary[keyStr] = fullName;
				tempDuplicateMapping[keyStr] = fullName;
                
                WLTContact *contact = [[WLTContact alloc] initWithName:fullName andNumber:nil];
                contact.mobile = mobileNum;
                
                [allContactsArray addObject:contact];
			}
		}
	}
	
	self.allContacts = allContactsArray;
}

- (void)checkPermissionsAndFetchAllUsersCompletion:(DataCompletionBlock)block{
	
    // Request authorization to Address Book
    __weak WLTContactsManager *weakSelf = self;
	
	self.managerState = ContactsManagerStateFetching;
	
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
	
    if ( status == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
			
            if (granted) {
				
                // First time access has been granted, add the contact
                [weakSelf fetchAllContacts];
                block(weakSelf.allContacts);
            } else {
                // User denied access
                // Display an alert telling user the contact could not be added
                block(nil);
            }
        });
    }
    else if ( status == kABAuthorizationStatusAuthorized ) {
		
        // The user has previously given access, add the contact
       	[weakSelf fetchAllContacts];
        block(weakSelf.allContacts);
    }
    else {
        
        // The user has previously denied access
        // Send an alert telling user to change privacy setting in settings app
        block(nil);
    }
}

-(void)completeAllCallBacks {
	
	self.managerState = ContactsManagerStateFetched;
	
	for ( ContactsFetchCompletion block in self.allContactsCallBackArray ) {
		block([self.allContacts mutableCopy]);
	}
	[self.allContactsCallBackArray removeAllObjects];
	
	for ( NSDictionary *dict in self.numberCallBackArray ) {
		
		DataCompletionBlock block = dict[@"blockValue"];
		if (block) {
			
			NSString *name = self.mappingDictionary[dict[@"number"]];
			if (!name) {
    			name = @"";
			}
			block(name);
		}
	}
	
	[self.numberCallBackArray removeAllObjects];
}

-(void)fetchAllContacts:(ContactsFetchCompletion)completion {
	
	if ( self.managerState == ContactsManagerStateFetching ) {
		
		[self.allContactsCallBackArray addObject:completion];
	}else if ( self.managerState == ContactsManagerStateFetched ) {
		
		completion([self.allContacts mutableCopy]);
	}else if (self.managerState == ContactsManagerStateInitial) {
		
		[self.allContactsCallBackArray addObject:completion];
		[self refreshContactsList];
	}
}

-(void)fetchNameForMobileNumber:(NSString*)completeString completion:(DataCompletionBlock)block {
	
    if (completeString.length) {
        if (self.managerState == ContactsManagerStateFetching) {
            
            [self.numberCallBackArray addObject:@{@"blockValue":block, @"number":completeString}];
        }else if ( self.managerState == ContactsManagerStateFetched ) {
            
            NSString *name = self.mappingDictionary[completeString];
            block(name);
        }else if (self.managerState == ContactsManagerStateInitial) {
            
            [self.numberCallBackArray addObject:@{@"blockValue":block, @"number":completeString}];
            [self refreshContactsList];
        }
    }else {
        
        block(nil);
    }
}

-(void)displayNameForUser:(id<User>)user completion:(ContactNameFetchBlock)block {
    
    if (block) {
        
        NSString *completeNum = [user completeNumber];
        if ([completeNum isEqualToString:CURRENT_USER_MOB]) {
            
            block(@"You");
        }
        else {
            
            NSString *userName = [user name];
            [self fetchNameForMobileNumber:completeNum completion:^(NSString *data) {
                
                if (data.length) {
                    block(data);
                }else if (userName.length) {
                    block(userName);
                }else {
                    block(nil);
                }
            }];
        }
    }
}

-(void)displayNameForUser:(id<User>)user defaultIsMob:(BOOL)mobile completion:(ContactNameFetchBlock)block {
    
    if (block) {
        
        [self displayNameForUser:user completion:^(NSString *name) {
            
            if (!name) {
                
                NSString *completeNum = [user completeNumber];
                block(completeNum);
            }else{
                
                block(name);
            }
        }];
    }
}

-(UserInContactsStatus)userExistenceInContacts:(id<User>)usr {
    
    UserInContactsStatus status = UserInContactsStatusUnknown;
    if (self.managerState == ContactsManagerStateFetched) {
        
        NSString *name = self.mappingDictionary[[usr completeNumber]];
        if (name.length) {
            
            status = UserInContactsStatusIn;
        }else {
            
            status = UserInContactsStatusOut;
        }
    }
    
    return status;
}

@end
