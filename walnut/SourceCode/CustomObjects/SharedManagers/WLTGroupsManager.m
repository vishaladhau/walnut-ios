//
//  WLTGroupsManager.m
//  walnut
//
//  Created by Abhinav Singh on 01/08/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTGroupsManager.h"
#import "WLTAppSettings.h"
#import "WLTDatabaseManager.h"
#import "WLTNetworkManager.h"
#import "WLTGroup.h"
#import "WLTTransaction.h"
#import "WLTTransactionUser.h"
#import "WLTOtherSettlement.h"
#import "WLTSplitComment.h"

NSString *const RefreshedGroupsList = @"RefreshedGroupsList";

@implementation WLTGroupsManager

+(WLTGroupsManager*)sharedManager {
    
    static __strong WLTGroupsManager *manager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
    
    return manager;
}

- (instancetype)init {
    
    self = [super init];
    if (self) {
        
        NSArray *array = [[NSUserDefaults standardUserDefaults] objectForKey:RefreshedGroupsList];
        if (!array) {
            
            NSMutableArray *newArray = [NSMutableArray new];
            [[NSUserDefaults standardUserDefaults] setObject:newArray forKey:RefreshedGroupsList];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        validTransactionTypes = @[TransactionObjectTypeSplit, TransactionObjectTypeSplitSettleIncomplete,
                                  TransactionObjectServerComment, TransactionObjectSplitComment,
                                  TransactionObjectSplitSettle, TransactionObjectGroupSettled];
    }
    
    return self;
}

-(void)logoutUser {
    
    NSMutableArray *newArray = [NSMutableArray new];
    [[NSUserDefaults standardUserDefaults] setObject:newArray forKey:RefreshedGroupsList];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FetchedAllGroups"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)fetchChangedGroupsList:(MultiGroupsDetailBlock)block {
    
    WLTAppSettings *settings = [WLTDatabaseManager sharedManager].appSettings;
    
    long long dateStamp = [settings.lastGroupListSyncTime timeStamp];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"FetchedAllGroups"]) {
        dateStamp = 0;
    }
    
    GTLQueryWalnut *walN = [GTLQueryWalnut queryForGroupsGetWithDeviceUuid:settings.deviceIdentifier lastSyncTime:dateStamp];
    
    [[WLTNetworkManager sharedManager].walnutService executeQuery:walN completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
        
        if (!error) {
            
            GTLWalnutMGroups *groups = object;
            settings.lastGroupListSyncTime = [NSDate dateFromWalnutTimestamp:groups.lastSyncTime.longLongValue];
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FetchedAllGroups"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            if (groups.groups.count) {
                
                NSMutableArray *changedGroups = [NSMutableArray new];
                NSMutableArray *deletedGroups = [NSMutableArray new];
                
                NSArray *allSavedCats = [[WLTDatabaseManager sharedManager] allObjectMatchingProperty:@"groupUUID" inArray:groups.groups arrayKey:@"uuid" ofClass:NSStringFromClass([WLTGroup class])];
                NSMutableDictionary *dictMapping = [NSMutableDictionary new];
                
                for ( WLTGroup *grp in allSavedCats ) {
                    dictMapping[grp.groupUUID] = grp;
                }
                
                for ( GTLWalnutMGroup *grps in groups.groups ) {
                    
                    WLTGroup *saved = dictMapping[grps.uuid];
                    if (!grps.deleted.boolValue) {
                        
                        if (!saved) {
                            saved = [[WLTDatabaseManager sharedManager] objectOfClassString:@"WLTGroup"];
                        }
                        [saved updateGroupFromWLTGroup:grps];
                        
                        [changedGroups addObject:saved];
                    }
                    else if (saved) {
                        
                        [deletedGroups addObject:saved];
                    }
                }
                
                if (deletedGroups.count) {
                    [[WLTDatabaseManager sharedManager] deleteGroups:deletedGroups];
                }else {
                    [[WLTDatabaseManager sharedManager] saveDataBase];
                }
                
                if (block) {
                    block(changedGroups, nil);
                }
            }
            else {
                
                if (block) {
                    block(nil, nil);
                }
            }
        }else {
            
            if (block) {
                block(nil, error);
            }
        }
    }];
}

-(void)fetchAllChangedGroups:(MultiGroupsDetailBlock)block {
    
    void (^ startFetchingDetails)() = ^void (){
        
        NSArray *allGrps = [[WLTDatabaseManager sharedManager] allObjectsOfClass:@"WLTGroup"];
        if (allGrps.count) {
            
            __block NSMutableArray *remaningToFetch = [NSMutableArray new];
            [remaningToFetch addObjectsFromArray:allGrps];
            
            for ( WLTGroup *grp in remaningToFetch ) {
                
                [self fetchDetailsOfGroup:grp completion:^(WLTGroup *grp2, NSError *error) {
                    
                    [remaningToFetch removeObject:grp];
                    
                    if (remaningToFetch.count == 0) {
                        if (block) {
                            block(allGrps, nil);
                        }
                    }
                }];
            }
        }else {
            if (block) {
                block(nil, nil);
            }
        }
    };
    
    [self fetchChangedGroupsList:^(NSArray *groups, NSError *error) {
        
        if (!error) {
            
            startFetchingDetails();
        }else {
            
            if (block) {
                block(nil, error);
            }
        }
    }];
}

-(void)fetchEveryChangeOfGroup:(WLTGroup*)grp completion:(GroupDetailsBlock)block {
    
    [self fetchChangedGroupsList:^(NSArray *grps, NSError *error) {
        
        if (!error) {
            
            [self fetchDetailsOfGroup:grp completion:^(WLTGroup *cGrp, NSError *cError) {
                
                if (block) {
                    block(cGrp, cError);
                }
            }];
        }else {
            
            if (block) {
                block(nil, error);
            }
        }
    }];
}

-(void)fetchEveryChangeOfGroupsIDS:(NSArray*)grpIdsArr completion:(MultiGroupsDetailBlock)block {
    
    [self fetchChangedGroupsList:^(NSArray *cGrps, NSError *error) {
        
        if (!error) {
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"groupUUID IN %@", grpIdsArr];
            NSArray *allGrpsToFetch = [[WLTDatabaseManager sharedManager] allObjectsOfClass:@"WLTGroup" withPredicate:predicate];
            
            if (allGrpsToFetch.count) {
                
                __block NSMutableArray *copyArray = [allGrpsToFetch mutableCopy];
                for ( WLTGroup *grps in copyArray ) {
                    
                    [self fetchDetailsOfGroup:grps completion:^(WLTGroup *ccGrp, NSError *ccError) {
                        
                        [copyArray removeObject:grps];
                        if(copyArray.count == 0) {
                            
                            if (block) {
                                block(allGrpsToFetch, nil);
                            }
                        }
                    }];
                }
            }
            else {
                
                if (block) {
                    block(nil, nil);
                }
            }
        }else {
            
            if (block) {
                block(nil, error);
            }
        }
    }];
}

-(void)fetchDetailsOfGroup:(WLTGroup*)grp completion:(GroupDetailsBlock)block {
    
    if (!grp.groupUUID.length) {
        block(nil, [NSObject walnutErrorDefault]);
        return;
    }
    
    long long dateStamp = [grp.lastSyncTime doubleValue];
//#warning -
//    dateStamp = 0;
    
    NSMutableArray *refreshedGrps = [[NSUserDefaults standardUserDefaults] objectForKey:RefreshedGroupsList];
    if (![refreshedGrps containsObject:grp.groupUUID]) {
        dateStamp = 0;
    }
    
    GTLQueryWalnut *grpDetail = [GTLQueryWalnut queryForGroupGetWithDeviceUuid:[WLTDatabaseManager sharedManager].appSettings.deviceIdentifier groupUuid:grp.groupUUID lastSyncTime:dateStamp];
    [[WLTNetworkManager sharedManager].walnutService executeQuery:grpDetail completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
        
        if (!error) {
            
            if (![grp isDeleted] && grp.groupUUID.length) {
                
                NSMutableArray *refreshedGrps2 = [[NSUserDefaults standardUserDefaults] objectForKey:RefreshedGroupsList];
                if (![refreshedGrps2 containsObject:grp.groupUUID]) {
                    
                    NSMutableArray *newArr = [refreshedGrps2 mutableCopy];
                    [newArr addObject:grp.groupUUID];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:newArr forKey:RefreshedGroupsList];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
            }
            
            WLTDatabaseManager *dbManager = [WLTDatabaseManager sharedManager];
            
            GTLWalnutMGroupInfo *groupInfo = object;
            
            grp.lastSyncTime = groupInfo.lastSyncTime;
            grp.currentUserContr = groupInfo.callerBalances.youContributed;
            grp.currentUserShare = groupInfo.callerBalances.yourShare;
            grp.groupTotal = groupInfo.groupTotal;
            grp.currentUserSent = groupInfo.callerBalances.youSettled ?: @0;
            grp.currentUserReceived = groupInfo.callerBalances.youReceived ?: @0;
            grp.currentUserActContr = @(((groupInfo.callerBalances.youContributed.floatValue + groupInfo.callerBalances.youSettled.floatValue) - groupInfo.callerBalances.youReceived.floatValue));
            
            grp.currentUserOwe = @(0);
            grp.currentUserGet = @(0);
            
            if (groupInfo.callerBalances.youOwe.integerValue > 0) {
                
                grp.currentUserOwe = groupInfo.callerBalances.youOwe;
            }else if (groupInfo.callerBalances.youOwe.integerValue < 0) {
                
                grp.currentUserGet = @(-1 * (groupInfo.callerBalances.youOwe.floatValue));
            }
            
            NSMutableDictionary *grpMemMapping = [NSMutableDictionary new];
            for ( WLTUser *grpMem in grp.members) {
                grpMemMapping[grpMem.mobileString] = grpMem.name;
            }
            
            WLTTransactionUser * (^ transUserForMobileNumber)(NSString *mobNum, NSString *name) = ^WLTTransactionUser * (NSString *mobNum, NSString *name){
                WLTTransactionUser *retUser = [dbManager objectOfClassString:@"WLTTransactionUser"];
                if (!name.length) {
                    name = grpMemMapping[mobNum];
                }
                
                retUser.name = name;
                retUser.mobileString = mobNum;
                return retUser;
            };
            
            WLTUser * (^ userForMobileNumber)(NSString *mobNum, NSString *name) = ^WLTUser * (NSString *mobNum, NSString *name){
                
                WLTUser *retUser = [dbManager objectOfClassString:@"WLTUser"];
                if (!name.length) {
                    name = grpMemMapping[mobNum];
                }
                
                retUser.name = name;
                retUser.mobileString = mobNum;
                
                return retUser;
            };
            
            NSString *currentUserMobile = CURRENT_USER_MOB;
            
            NSMutableArray *toRemoveObjs = [NSMutableArray new];
            [toRemoveObjs addObjectsFromArray:grp.userGetFrom.allObjects];
            [toRemoveObjs addObjectsFromArray:grp.userPayTo.allObjects];
            [toRemoveObjs addObjectsFromArray:grp.otherSettlements.allObjects];
            
            [grp removeUserGetFrom:grp.userGetFrom];
            [grp removeUserPayTo:grp.userPayTo];
            [grp removeOtherSettlements:grp.otherSettlements];
            
            for ( GTLWalnutMBalance *bal in groupInfo.magicBalances ) {
                if ([bal.mFrom.mobileNumber isEqualToString:currentUserMobile]) {
                    
                    WLTTransactionUser *user = transUserForMobileNumber(bal.mTo.mobileNumber, bal.mTo.name);
                    user.amount = bal.amount;
                    
                    [grp addUserPayToObject:user];
                }
                else if ([bal.mTo.mobileNumber isEqualToString:currentUserMobile]) {
                    
                    WLTTransactionUser *user = transUserForMobileNumber(bal.mFrom.mobileNumber, bal.mFrom.name);
                    user.amount = bal.amount;
                    
                    [grp addUserGetFromObject:user];
                }else {
                    
                    WLTOtherSettlement *other = [dbManager objectOfClassString:@"WLTOtherSettlement"];
                    if (bal.mFrom) {
                        other.fromUser = userForMobileNumber(bal.mFrom.mobileNumber, bal.mFrom.name);
                    }
                    if (bal.mFrom) {
                        other.toUser = userForMobileNumber(bal.mTo.mobileNumber, bal.mTo.name);
                    }
                    other.amount = bal.amount;
                    
                    [grp addOtherSettlementsObject:other];
                }
            }
            
            NSMutableArray *toRemoveTransID = [NSMutableArray new];
            
            NSMutableDictionary *mappingDic = nil;
            NSArray *allIds = [groupInfo.transactions valueForKeyPath:@"objUuid"];
            if (allIds.count) {
                
                NSArray *existingTrans = [dbManager allObjectsOfClass:@"WLTTransaction" withPredicate:[NSPredicate predicateWithFormat:@"objID IN %@", allIds]];
                mappingDic = [NSMutableDictionary new];
                for ( WLTTransaction *tr in existingTrans ) {
                    
                    mappingDic[tr.objID] = tr;
                }
            }
            
            NSInteger unreadCount = 0;
            double lastTransTime = 0;
            
            for ( GTLWalnutMTransaction *trans in groupInfo.transactions) {
                
                if (!trans.deleted.boolValue) {
                    
                    if ([validTransactionTypes containsObject:trans.objType]) {
                        
                        WLTTransaction *transaction = mappingDic[trans.objUuid];
                        if (!transaction) {
                            
                            if ([trans.objType isEqualToString:TransactionObjectSplitComment]) {
                                
                                WLTSplitComment *stransaction = [dbManager objectOfClassString:@"WLTSplitComment"];
                                transaction = stransaction;
                            }
                            else {
                                
                                transaction = [dbManager objectOfClassString:@"WLTTransaction"];
                            }
                        }
                        
                        if ([transaction isKindOfClass:[WLTSplitComment class]]) {
                            
                            [(WLTSplitComment*)transaction setStateNum:@(WLTSplitCommentStatePosted)];
                        }
                        
                        transaction.isWpaySettled = trans.isWpaySettled;
                        transaction.amount = trans.amount;
                        transaction.notes = trans.notes;
                        transaction.txnID = trans.txnUuid;
                        transaction.type = trans.objType;
                        transaction.objID = trans.objUuid;
                        transaction.receiverBank = trans.receiverBank;
                        transaction.senderBank = trans.senderBank;
                        transaction.isCrossGroupSettlement = trans.isCrossGroupSettlement;
                        
                        if (trans.txnDate.doubleValue > 0) {
                            transaction.transactionDate = [NSDate dateWithTimeIntervalSince1970:trans.txnDate.doubleValue];
                        }else {
                            transaction.transactionDate = nil;
                        }
                        
                        if (trans.creationDate.doubleValue > 0) {
                            transaction.createdDate = [NSDate dateWithTimeIntervalSince1970:trans.creationDate.doubleValue];
                        }else {
                            transaction.createdDate = nil;
                        }
                        
                        if (trans.updatedAt.doubleValue > 0) {
                            transaction.updatedDate = [NSDate dateWithTimeIntervalSince1970:trans.updatedAt.doubleValue];
                        }else {
                            transaction.updatedDate = nil;
                        }
                        
                        double sortTime = 0;
                        
                        if([transaction.type isEqualToString:TransactionObjectSplitSettle]) {
                            
                            transaction.sortingDate = transaction.transactionDate;
                            sortTime = [trans.txnDate doubleValue];
                        }
                        else if (transaction.createdDate) {
                            if (trans.updatedAt.doubleValue > trans.creationDate.doubleValue) {
                                
                                transaction.sortingDate = transaction.updatedDate;
                                sortTime = [trans.updatedAt doubleValue];
                            }else {
                                
                                transaction.sortingDate = transaction.createdDate;
                                sortTime = [trans.creationDate doubleValue];
                            }
                        }
                        else if(transaction.transactionDate){
                            
                            transaction.sortingDate = transaction.transactionDate;
                            sortTime = [trans.txnDate doubleValue];
                        }
                        
                        if (grp.lastDisplayTime && (sortTime > grp.lastDisplayTime.doubleValue)) {
                            
                            unreadCount += 1;
                        }
                        
                        if (lastTransTime < sortTime) {
                            lastTransTime = sortTime;
                        }
                        
                        [transaction removeSplits:transaction.splits];
                        
                        for ( GTLWalnutMSplit *split in trans.splits ) {
                            
                            WLTTransactionUser *usr = transUserForMobileNumber(split.mobileNumber, nil);
                            usr.amount = split.amount;
                            
                            [transaction addSplitsObject:usr];
                        }
                        
                        transaction.placeName = trans.placeName;
                        if (!transaction.placeName) {
                            transaction.placeName = trans.posname;
                        }
                        
                        if (trans.owner) {
                            transaction.owner = userForMobileNumber(trans.owner.mobileNumber, trans.owner.name);
                        }
                        else {
                            transaction.owner = nil;
                        }
                        
                        if (trans.receiver) {
                            transaction.receiver = userForMobileNumber(trans.receiver.mobileNumber, trans.receiver.name);
                        }
                        else {
                            transaction.receiver = nil;
                        }
                        
                        if (trans.addedBy) {
                            transaction.addedBy = userForMobileNumber(trans.addedBy.mobileNumber, trans.addedBy.name);
                        }else {
                            transaction.addedBy = nil;
                        }
                        
                        [grp addTransactionsObject:transaction];
                    }
                }
                else {
                    
                    [toRemoveTransID addObject:trans.txnUuid];
                }
            }
            
            if (!grp.lastDisplayTime.doubleValue) {
                
                grp.lastDisplayTime = @(lastTransTime);
                unreadCount = 0;
            }
			
			NSInteger newUnreadCount = (unreadCount + grp.unreadMessagesCount.integerValue);
            if (grp.unreadMessagesCount.integerValue != newUnreadCount) {
                
                grp.unreadMessagesCount = @(newUnreadCount);
                [[NSNotificationCenter defaultCenter] postNotificationName:NotifyGroupUnreadCountChanged object:grp];
            }
            
            if (toRemoveTransID.count) {
                
                NSMutableArray *toremoveTrans = [NSMutableArray new];
                for ( NSString *ids  in toRemoveTransID ) {
                    for ( WLTTransaction *ttrans in grp.transactions) {
                        if ([ttrans.txnID isEqualToString:ids]) {
                            [toremoveTrans addObject:ttrans];
                        }
                    }
                }
                
                if (toremoveTrans.count) {
                    
                    [toRemoveObjs addObjectsFromArray:toremoveTrans];
                    [grp removeTransactions:[NSSet setWithArray:toremoveTrans]];
                }
            }
            
            if (toRemoveObjs.count) {
                for ( NSManagedObject *obj in toRemoveObjs ) {
                    
                    if (![obj isDeleted]) {
                        [obj.managedObjectContext deleteObject:obj];
                    }
                }
            }
            
            double newExpectedGroupSortTime = (lastTransTime*1000000);
            if (grp.sortingTime.doubleValue < newExpectedGroupSortTime) {
                grp.sortingTime = @(newExpectedGroupSortTime);
            }
            
            [dbManager saveDataBase];
            
            if (block) {
                block(grp, nil);
            }
        }
        else {
            
            if (block) {
                block(nil, error);
            }
        }
    }];
}

@end
