//
//  WLTInstrumentsManager.m
//  walnut
//
//  Created by Abhinav Singh on 05/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTInstrumentsManager.h"
#import "WLTNetworkManager.h"
#import "WLTDatabaseManager.h"
#import "WLTPaymentInstrument.h"
#import "WLTRootViewController.h"
#import "WLTViewController.h"
#import "WLTPaymentTransaction.h"

NSString * const InstrumentTypeBank = @"bank_account";
NSString * const InstrumentTypeCards = @"cards";
NSString * const InstrumentSubTypeCredit = @"Credit";

static const NSTimeInterval CardsCachingTime = (60*50);//50 Mins

NSString *const NotifyCardChanged = @"NotifyCardChanged";

@implementation WLTInstrumentsCallback

@end

@interface WLTInstrumentsManager () {
    
    NSMutableArray *callBacksArray;
}

@property(nonatomic, strong) NSString *justAddedCardID;

@property(nonatomic, strong) NSArray *allInstruments;
@property(nonatomic, strong) NSArray *payInstruments;
@property(nonatomic, strong) NSArray *receiveInstruments;

@property(nonatomic, strong) NSDate *lastUpdatedDate;

@end

@implementation WLTInstrumentsManager

+ (BOOL)instrumentIsVisaDebit:(GTLWalnutMPaymentInstrument *)instrument {
    return ([instrument.type caseInsensitiveCompare:InstrumentTypeCards] == NSOrderedSame)
        && ([instrument.subtype caseInsensitiveCompare:@"debit"] == NSOrderedSame)
        && ([instrument.network caseInsensitiveCompare:@"visa"] == NSOrderedSame);
}

+(WLTInstrumentsManager*)sharedManager {
    
    static __strong WLTInstrumentsManager *manager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        manager = [[self alloc] init];
    });
    
    return manager;
}

-(instancetype)init {
    
    self = [super init];
    if (self) {
        
        callBacksArray = [NSMutableArray new];
        
        NSMutableArray *allInstruments = [NSMutableArray new];
        NSMutableArray *payInstruments = [NSMutableArray new];
        NSMutableArray *receiveInstruments = [NSMutableArray new];
        
        _state = IntrumentManagerStateInitial;
        
        NSArray *allSaved = [[WLTDatabaseManager sharedManager] allObjectsOfClass:@"WLTPaymentInstrument"];
        for( WLTPaymentInstrument *intrSaved in allSaved ) {
            
            NSData *strJson = [intrSaved.jsonString dataUsingEncoding:NSUTF8StringEncoding];
            NSMutableDictionary *dictJ = [[NSJSONSerialization JSONObjectWithData:strJson options:0 error:nil] mutableCopy];
            
            GTLWalnutMPaymentInstrument *instr = [GTLWalnutMPaymentInstrument objectWithJSON:dictJ];
            if (instr.enabledForMoneysend.boolValue) {
                [payInstruments addObject:instr];
            }
            if (instr.enabledForMoneyreceive.boolValue) {
                [receiveInstruments addObject:instr];
            }
            
            [allInstruments addObject:instr];
            if (instr.receiveDefault.boolValue) {
                _defaultReceiveInstrument = instr;
            }
        }
        
        _allInstruments = allInstruments;
        _payInstruments = payInstruments;
        _receiveInstruments = receiveInstruments;
        
        _lastUpdatedDate = nil;
    }
    
    return self;
}

-(void)userMobileChanged:(NSNotification*)notify {
    
    [self logoutUser];
    [self completeAllCallBacksWithData:[NSObject walnutErrorDefault]];
}

-(void)logoutUser {
	
	_defaultReceiveInstrument = nil;
	
    _allInstruments = nil;
    _payInstruments = nil;
    _payInstruments = nil;
    
	_lastUpdatedDate = nil;
	
	_state = IntrumentManagerStateInitial;
}

-(void)showNewDefaultPopUpForIntrument:(GTLWalnutMPaymentInstrument*)instrument flowType:(WLTEntryFlowType)flowType {
    
   // Change the alert here!
    if (_receiveInstruments.count == 1 && [[self class] instrumentIsVisaDebit:instrument]) {
        if (flowType == WLTEntryFlowTypePay) {
            UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Congratulations on your payment!"
                                                                                message:[NSString stringWithFormat:@"You can also send and receive money from friends in %@ ****%@\n\nNo more Bank accounts and IFSC details", instrument.bank, instrument.last4Digits]
                                                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Got it"
                                                                   style:UIAlertActionStyleCancel
                                                                 handler:nil];
            [controller addAction:cancelAction];
            
            UIViewController *cont = [self.rootController visibleTopViewController];
            
            [cont presentViewController:controller animated:YES completion:nil];
        } else if (flowType == WLTEntryFlowTypeReceive) {
            UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Congratulations!"
                                                                                message:[NSString stringWithFormat:@"You just received money directly in your debit card\n\nNo more Bank account and IFSC details\n\nYou will receive money in %@ ****%@", instrument.bank, instrument.last4Digits]
                                                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Got it"
                                                                   style:UIAlertActionStyleCancel
                                                                 handler:nil];
            [controller addAction:cancelAction];
            
            UIViewController *cont = [self.rootController visibleTopViewController];
            
            [cont presentViewController:controller animated:YES completion:nil];
            
        }
    } else if ([[self class] instrumentIsVisaDebit:instrument]) {
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"You have added a new debit card %@ ****%@", instrument.bank, instrument.last4Digits]
                                                                            message:@"Do you wish to receive money sent by friends in this?"
                                                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No"
                                                               style:UIAlertActionStyleCancel
                                                             handler:nil];
        UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                instrument.receiveDefault = @(YES);
                [[WLTInstrumentsManager sharedManager] addInstrumentToWalnut:instrument withCompletion:^(id data) {
                }];
        }];
        [controller addAction:yesAction];
        [controller addAction:cancelAction];
        
        UIViewController *cont = [self.rootController visibleTopViewController];
        
        [cont presentViewController:controller animated:YES completion:nil];
    }

}

-(void)showMakeNewDefaultPopUpForIntrument:(GTLWalnutMPaymentInstrument*)instrument completion:(DataCompletionBlock)block{
    
    if (instrument.enabledForMoneysend.boolValue && instrument.enabledForMoneyreceive.boolValue && !instrument.receiveDefault.boolValue) {
        
        __weak typeof(self) weakSelf = self;
        
        __weak UIViewController *topCont = [self.rootController visibleTopViewController];
        
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"WalnutPay"
                                                                            message:[NSString stringWithFormat:@"Now your friends can send you money without sharing your bank account details.\n\nYou wish to receive money in %@ %@", instrument.bank, instrument.last4Digits]
                                                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No, thanks"
                                                               style:UIAlertActionStyleDestructive
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 
                                                                 if (block) {
                                                                     block(@(0));
                                                                 }
                                                             }];
        [controller addAction:cancelAction];
        
        UIAlertAction *settleAction = [UIAlertAction actionWithTitle:@"Yes"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 
                                                                 instrument.receiveDefault = @(YES);
                                                                 if ([topCont isKindOfClass:[WLTViewController class]]) {
                                                                     [(WLTViewController*)topCont startLoading];
                                                                 }
                                                                 [weakSelf addInstrumentToWalnut:instrument withCompletion:^(id data) {
                                                                     
                                                                     if ([data isKindOfClass:[NSError class]]) {
                                                                         
                                                                         if (block) {
                                                                             block(data);
                                                                         }
                                                                     }else {
                                                                         
                                                                         [[NSNotificationCenter defaultCenter] postNotificationName:NotifyCardChanged object:instrument];
                                                                         
                                                                         if (block) {
                                                                             block(@(1));
                                                                         }
                                                                     }
                                                                     
                                                                     if ([topCont isKindOfClass:[WLTViewController class]]) {
                                                                         [(WLTViewController*)topCont endViewLoading];
                                                                     }
                                                                 }];
                                                             }];
        
        [controller addAction:settleAction];
        
        [topCont presentViewController:controller animated:YES completion:nil];
    }else {
        if (block) {
            block(@(0));
        }
    }
}

-(void)checkAndShowAlertPopupsForNewInstruments:(NSArray*)allIntruments {
    
    GTLWalnutMPaymentInstrument *prevDefault = self.defaultReceiveInstrument;
    _defaultReceiveInstrument = nil;
    
    NSMutableArray *payInstruments = [NSMutableArray new];
    NSMutableArray *receiveInstruments = [NSMutableArray new];
    
    BOOL newDefaultInstr = NO;
    
    NSMutableArray *newInstruments = [NSMutableArray new];
    
    for ( GTLWalnutMPaymentInstrument *inst in allIntruments ) {
        
        if (inst.enabledForMoneysend.boolValue) {
            [payInstruments addObject:inst];
        }
        if (inst.enabledForMoneyreceive.boolValue) {
            [receiveInstruments addObject:inst];
        }
        
        if (inst.receiveDefault.boolValue) {
            
            if (!prevDefault || ![prevDefault.instrumentUuid isEqualToString:inst.instrumentUuid]) {
                
                newDefaultInstr = YES;
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"WalnutPay" message:[NSString stringWithFormat:@"Now your friends can send you money without sharing your bank account details.\nYou will receive money in %@ %@", inst.bank, inst.last4Digits]
                                                                   delegate:nil cancelButtonTitle:@"Okay"
                                                          otherButtonTitles:nil, nil];
                    [alert show];
                });
            }
            
            _defaultReceiveInstrument = inst;
        }
        
        BOOL isNew = YES;
        for ( GTLWalnutMPaymentInstrument *prevInst in self.allInstruments) {
            if ([inst.instrumentUuid isEqualToString:prevInst.instrumentUuid]) {
                isNew = NO;
                break;
            }
        }
        
        if (isNew) {
            [newInstruments addObject:inst];
        }
    }
    
    GTLWalnutMPaymentInstrument *toAsk = nil;
    
    if (!newDefaultInstr && newInstruments.count) {
        
        for ( GTLWalnutMPaymentInstrument *newInst in newInstruments) {
            
            if (newInst.enabledForMoneyreceive.boolValue && newInst.enabledForMoneysend.boolValue) {
                if (!newInst.receiveDefault.boolValue) {
                    toAsk = newInst;
                    break;
                }
            }
        }
    }
    
    _payInstruments = payInstruments;
    _receiveInstruments = receiveInstruments;
    _allInstruments = allIntruments;
}

-(void)saveCurrentCardsToDatabase {
    
    NSArray *allSaved = [[WLTDatabaseManager sharedManager] allObjectsOfClass:@"WLTPaymentInstrument"];
    NSMutableDictionary *mapping = [NSMutableDictionary new];
    for ( WLTPaymentInstrument *instr in allSaved ) {
        mapping[instr.instrumentID] = instr;
    }
    
    WLTDatabaseManager *manager = [WLTDatabaseManager sharedManager];
    
    for ( GTLWalnutMPaymentInstrument *instrCurr in self.allInstruments ) {
        
        WLTPaymentInstrument *instrDB = mapping[instrCurr.instrumentUuid];
        if(!instrDB) {
            instrDB = [manager objectOfClassString:@"WLTPaymentInstrument"];
        }
        
        instrDB.jsonString = instrCurr.JSONString;
        instrDB.instrumentID = instrCurr.instrumentUuid;
        
        mapping[instrCurr.instrumentUuid] = nil;
    }
    
    for ( WLTPaymentInstrument *instrDB in mapping.allValues ) {
        [instrDB.managedObjectContext deleteObject:instrDB];
    }
    
    [manager saveDataBase];
}

-(void)removeInstrumentFromWalnut:(GTLWalnutMPaymentInstrument*)instrument withCompletion:(DataCompletionBlock)block {
    
    GTLWalnutMPaymentInstrumentDelete *deleteObj = [[GTLWalnutMPaymentInstrumentDelete alloc] init];
    deleteObj.deviceUuid = instrument.deviceUuid;
    deleteObj.paymentInstrumentUuid = instrument.instrumentUuid;
    
    GTLQueryWalnut *walN = [GTLQueryWalnut queryForPaymentInstrumentDeleteWithObject:deleteObj];
    
    __weak WLTInstrumentsManager *weakSelf = self;
    
    [[WLTNetworkManager sharedManager].walnutService executeQuery:walN completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
        
        if (!error) {
            
            [weakSelf cardRemoved:instrument];
            
            [weakSelf validCardsForTransactionType:PaymentInstrumentTypeBoth
                                   invalidateCache:YES
                                    withCompletion:block];
        }else {
            
            if (block) {
                block(error);
            }
        }
    }];
}

-(void)addCard:(GTLWalnutMPaymentInstrument*)instrument withCompletion:(DataCompletionBlock)block {
    
    __weak typeof(self) weakSelf = self;
    GTLQueryWalnut *excuteQuery = [GTLQueryWalnut queryForPaymentInstrumentAddWithObject:instrument];
    [[WLTNetworkManager sharedManager].walnutService executeQuery:excuteQuery completionHandler:^(GTLServiceTicket *ticket, GTLWalnutMPaymentInstrument *object, NSError *error) {
        
        if (!error) {
            
            if ([object.subtype isEqualToString:InstrumentSubTypeCredit]) {
                object.enabledForMoneyreceive = @(NO);
            }
            else if ([object.type isEqualToString:InstrumentTypeBank]) {
                object.fastFundEnabled = @(YES);
            }
            
            [weakSelf newCardAdded:object];
            
            if (block) {
                block(@(YES));
            }
        }else {
            
            if (block) {
                block(error);
            }
        }
    }];
}

-(void)addInstrumentToWalnut:(GTLWalnutMPaymentInstrument*)instr withCompletion:(DataCompletionBlock)block{
    
    void (^ addInstrument)(GTLWalnutMPaymentInstrument *newCard) = ^void (GTLWalnutMPaymentInstrument *newCard){
        
        __weak WLTInstrumentsManager *weakSelf = self;
        
        [self addCard:newCard withCompletion:^(id data) {
            if (![data isKindOfClass:[NSError class]]) {
                
                [weakSelf validCardsForTransactionType:PaymentInstrumentTypeBoth invalidateCache:YES withCompletion:^(id data) {
                    
                    GTLWalnutMPaymentInstrument *justAdded = newCard;
                    if ([data isKindOfClass:[NSArray class]]) {
                        
                        for ( GTLWalnutMPaymentInstrument *instr in data ) {
                            
                            if ([instr.instrumentUuid isEqualToString:newCard.instrumentUuid]) {
                                justAdded = instr;
                                break;
                            }
                        }
                    }
                    
                    if (block) {
                        block(data);
                    }
                }];
            }else {
                
                if (block) {
                    block(data);
                }
            }
        }];
    };
    
    NSMutableArray *toUnMarkReceiveDefault = [NSMutableArray new];
    
    if (instr.receiveDefault.boolValue) {
        for ( GTLWalnutMPaymentInstrument *iteInst in self.receiveInstruments ) {
            
            if (![iteInst.instrumentUuid isEqualToString:instr.instrumentUuid]) {
                
                if (iteInst.receiveDefault.boolValue) {
                   
                    GTLWalnutMPaymentInstrument *iteCopy = [iteInst copy];
                    iteCopy.receiveDefault = @(NO);
                    [toUnMarkReceiveDefault addObject:iteCopy];
                }
            }
        }
    }
    
    if (toUnMarkReceiveDefault.count) {
        
        __block NSMutableArray *allCards = [NSMutableArray new];
        [allCards addObjectsFromArray:toUnMarkReceiveDefault];
        
        for ( GTLWalnutMPaymentInstrument *toUnMark in allCards ) {
            
            [self addCard:toUnMark withCompletion:^(id data) {
                
                NSError *error = nil;
                if ([data isKindOfClass:[NSError class]]) {
                    
                    error = data;
                    if (block) {
                        block(error);
                    }
                }else {
                    [allCards removeObject:toUnMark];
                }
                
                if (allCards.count == 0 && !error) {
                    addInstrument(instr);
                }
            }];
        }
    }
    else {
        
        addInstrument(instr);
    }
}

-(void)refreshCardsListFromServerCompletion:(DataCompletionBlock)block {
    
    _state = IntrumentManagerStateDownloading;
    
    WLTAppSettings *settings = [WLTDatabaseManager sharedManager].appSettings;
    long long dateStamp = 0;//[settings.lastPayInstrumentsSyncTime timeStamp];
    
    __weak WLTInstrumentsManager *weakSelf = self;
    
    GTLQueryWalnut *wlnut = [GTLQueryWalnut queryForPaymentInstrumentListWithDeviceUuid:settings.deviceIdentifier lastSyncTime:dateStamp];
    [[WLTNetworkManager sharedManager].walnutService executeQuery:wlnut completionHandler:^(GTLServiceTicket *ticket, GTLWalnutMPaymentInstruments *object, NSError *error) {
        
        if (!error) {
            
            _allInstruments = nil;
            _receiveInstruments = nil;
            _payInstruments = nil;
            
            NSMutableArray *allInstruments = [NSMutableArray new];
            NSMutableArray *payInstruments = [NSMutableArray new];
            NSMutableArray *receiveInstruments = [NSMutableArray new];
            
            if (object && [object isKindOfClass:[GTLWalnutMPaymentInstruments class]]) {
                
                for ( GTLWalnutMPaymentInstrument *instr in object.instruments ) {
                    if (instr.isActive.boolValue) {
                        
                        if ([instr.subtype isEqualToString:InstrumentSubTypeCredit]) {
                            
                            instr.enabledForMoneyreceive = @(NO);
                        }
                        else if ([instr.type isEqualToString:InstrumentTypeBank]) {
                            
                            instr.fastFundEnabled = @(YES);
                        }
                        
                        if (instr.enabledForMoneysend.boolValue) {
                            [payInstruments addObject:instr];
                        }
                        if (instr.enabledForMoneyreceive.boolValue) {
                            [receiveInstruments addObject:instr];
                        }
                        
                        [allInstruments addObject:instr];
                    }
                }
                
                [weakSelf saveCurrentCardsToDatabase];
                
                _allInstruments = allInstruments;
                _receiveInstruments = receiveInstruments;
                _payInstruments = payInstruments;
            }
            else {
                
                error = [NSObject walnutErrorDefault];
            }
            
            if (error) {
                if (block) {
                    block(error);
                }
            }else {
                if (block) {
                    block(@(YES));
                }
            }
        }
        else {
            if (block) {
                block(error);
            }
        }
    }];
}

-(void)validCardsForTransactionType:(PaymentInstrumentType)type
                    invalidateCache:(BOOL)invalidate
                     withCompletion:(DataCompletionBlock)block {
    
    if (invalidate) {
        
        self.lastUpdatedDate = nil;
    }
    
    if ( !self.lastUpdatedDate || ([self.lastUpdatedDate timeIntervalSinceNow] > CardsCachingTime) ) {
        
        WLTInstrumentsCallback *callBack = [WLTInstrumentsCallback new];
        callBack.callBack = block;
        callBack.type = type;
        
        [callBacksArray addObject:callBack];
        
        if (self.state != IntrumentManagerStateDownloading) {
            
            __weak WLTInstrumentsManager *weakSelf = self;
            [self refreshCardsListFromServerCompletion:^(id data) {
                
                NSError *error = nil;
                if ([data isKindOfClass:[NSError class]]) {
                    error = data;
                }
                
                _state = IntrumentManagerStateInitial;
                if (!error) {
                    
                    weakSelf.lastUpdatedDate = [NSDate date];
                    [weakSelf completeAllCallBacksWithData:data];
                }else {
                    
                    //Send saved intruments in database.
                    weakSelf.lastUpdatedDate = nil;
                    if (weakSelf.allInstruments.count) {
                        [weakSelf completeAllCallBacksWithData:@(YES)];
                    }else {
                        [weakSelf completeAllCallBacksWithData:error];
                    }
                }
            }];
        }
    }
    else {
        
        if (type == PaymentInstrumentTypePay) {
            if (block) {
                block(self.payInstruments);
            }
        }
        else if (type == PaymentInstrumentTypeReceive) {
            if (block) {
                block(self.receiveInstruments);
            }
        }else {
            if (block) {
                block(self.allInstruments);
            }
        }
    }
}

-(void)completeAllCallBacksWithData:(id)data {
    
    if([data isKindOfClass:[NSError class]]) {
        
        for ( WLTInstrumentsCallback *callBack in callBacksArray ) {
            
            if (callBack.callBack) {
                callBack.callBack(data);
            }
        }
    }else {
        
        for ( WLTInstrumentsCallback *callBack in callBacksArray ) {
            
            NSArray *dataArr = nil;
            switch (callBack.type) {
                    
                case PaymentInstrumentTypePay: {
                    dataArr = self.payInstruments;
                }
                    break;
                case PaymentInstrumentTypeReceive: {
                    dataArr = self.receiveInstruments;
                }
                    break;
                default: {
                    dataArr = self.allInstruments;
                }
                    break;
            }
            
            if (callBack.callBack) {
                callBack.callBack(dataArr);
            }
        }
    }
    
    [callBacksArray removeAllObjects];
}

- (BOOL)sendingInstrumentsContainsCards {
    BOOL hasCards = NO;
    for (GTLWalnutMPaymentInstrument *instrument in _allInstruments) {
        if ([instrument.type caseInsensitiveCompare:InstrumentTypeCards] == NSOrderedSame) {
            hasCards = YES;
            break;
        }
    }
    
    return hasCards;
}

-(GTLWalnutMPaymentInstrument*)defaultReceivingCard {
    
    GTLWalnutMPaymentInstrument *tinstrument = nil;
    for (GTLWalnutMPaymentInstrument *instrument in _allInstruments) {
        if (instrument.receiveDefault.boolValue) {
            tinstrument = instrument;
        }
    }
    
    return tinstrument;
}

-(void)newCardAdded:(GTLWalnutMPaymentInstrument*)instrument {
    
    if (instrument.receiveDefault.boolValue) {
        
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"statusNum == %@", @(PaymentStatusPushPending)];
        NSPredicate *predPaid = [NSPredicate predicateWithFormat:@"isPaidNumber == %@", @(NO)];
        NSPredicate *combined = [NSCompoundPredicate andPredicateWithSubpredicates:@[predPaid, pred]];
        
        NSArray *allObjs = [[WLTDatabaseManager sharedManager] allObjectsOfClass:@"WLTPaymentTransaction" withPredicate:combined];
        
        BOOL changed = NO;
        for ( WLTPaymentTransaction *object in allObjs) {
            
            if ( (object.subStatus <= PaymentSubStatusReceiverInstrumentUUIDNotFound) ) {
                if (object.subStatus != PaymentSubStatusWaiting) {
                    
                    changed = YES;
                    object.subStatusNum = @(PaymentSubStatusWaiting);
                }
            }
        }
        
        if(changed) {
            
            [[WLTDatabaseManager sharedManager] saveDataBase];
            [[NSNotificationCenter defaultCenter] postNotificationName:PaymentTransactionsChanged object:nil];
        }
    }
}

-(void)cardRemoved:(GTLWalnutMPaymentInstrument*)instrument {
    
    if (instrument.receiveDefault.boolValue) {
        
        NSPredicate *predStatus = [NSPredicate predicateWithFormat:@"statusNum == %@", @(PaymentStatusPushPending)];
        NSPredicate *predPaid = [NSPredicate predicateWithFormat:@"isPaidNumber == %@", @(NO)];
        
        NSPredicate *combined = [NSCompoundPredicate andPredicateWithSubpredicates:@[predStatus, predPaid]];
        NSArray *allObjs = [[WLTDatabaseManager sharedManager] allObjectsOfClass:@"WLTPaymentTransaction" withPredicate:combined];
        
        if (allObjs.count) {
            
            for ( WLTPaymentTransaction *object in allObjs) {
                
                object.subStatusNum = @(PaymentSubStatusReceiverNotHaveDefaultInstrument);
            }
        }
        
        [[WLTDatabaseManager sharedManager] saveDataBase];
        [[NSNotificationCenter defaultCenter] postNotificationName:PaymentTransactionsChanged object:nil];
    }
}

@end
