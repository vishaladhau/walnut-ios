//
//  WLTPaymentTransationsManager.h
//  walnut
//
//  Created by Abhinav Singh on 14/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GTLWalnutMPaymentTransaction;
@class WLTPaymentTransaction;

typedef void (^ TransactionsRefreshBlock)(BOOL success, NSError *error);
typedef void (^ TransactionFetchBlock)(WLTPaymentTransaction *object, NSError *error);

@interface WLTPaymentTransationsManager : NSObject {
    
    NSMutableArray *callBacks;
    NSMutableArray *allUnreadTransactionIDs;
}

+(WLTPaymentTransationsManager*)sharedManager;

-(void)logoutUser;

-(NSArray <WLTPaymentTransaction *> *)updateCoreDataTransactionFromGTLObjects:(NSArray <GTLWalnutMPaymentTransaction *>*)trans;

-(void)fetchAllPaymentTransactionsCompletion:(TransactionsRefreshBlock)block;
-(void)coreDataPaymentTransactionForPredicate:(NSPredicate*)predicate completion:(TransactionFetchBlock)block;
-(void)handleNewPaymentObjects:(NSArray*)newObjects isFirstTime:(BOOL)firstTime;

-(void)markTransactionIDAsViewed:(NSString*)transId;
-(void)markAllTransactionsAsViewed;
-(NSInteger)unreadMessagesCount;
-(void)saveUnreadIds;

@end
