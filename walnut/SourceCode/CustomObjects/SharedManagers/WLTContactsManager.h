//
//  WLTContactsManager.h
//  walnut
//
//  Created by Abhinav Singh on 24/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, UserInContactsStatus) {
    
    UserInContactsStatusUnknown = 0,
    UserInContactsStatusIn,
    UserInContactsStatusOut,
};

typedef NS_ENUM(NSInteger, ContactsManagerState) {
	
	ContactsManagerStateInitial = 0,
	ContactsManagerStateFetching,
	ContactsManagerStateFetched
};

FOUNDATION_EXPORT NSString * const NotifyContactListChanged;

typedef void (^ ContactsFetchCompletion)(NSMutableArray *data);
typedef void (^ ContactNameFetchBlock)(NSString *name);

@interface WLTContactsManager : NSObject {
    
}

+(WLTContactsManager*)sharedManager;
-(void)refreshContactsList;

-(UserInContactsStatus)userExistenceInContacts:(id<User>)usr;

-(void)fetchAllContacts:(ContactsFetchCompletion)completion;

//-(void)displayNameOrMobileForUser:(id<User>)user completion:(ContactNameFetchBlock)block;
-(void)displayNameForUser:(id<User>)user defaultIsMob:(BOOL)mobile completion:(ContactNameFetchBlock)block;
-(void)displayNameForUser:(id<User>)user completion:(ContactNameFetchBlock)block;

@end
