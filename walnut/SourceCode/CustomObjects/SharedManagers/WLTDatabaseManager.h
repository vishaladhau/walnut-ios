//
//  WLTDatabaseManager.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 04/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WLTAppSettings.h"
#import "WLTLoggedInUser.h"
#import "WLTPaymentConfig.h"

@class GTLWalnutMPaymentConfig;
@class WLTContact;
@class WLTTransaction;

#define CURRENT_USER_MOB [WLTDatabaseManager sharedManager].currentUser.mobileString

@interface WLTDatabaseManager : NSObject

@property(readonly, strong) NSArray *blockedBins;
@property(readonly, strong) WLTAppSettings *appSettings;
@property(readonly, strong) WLTPaymentConfig *paymentConfig;
@property(nonatomic, strong) WLTLoggedInUser *currentUser;

+(WLTDatabaseManager*)sharedManager;

-(void)logOut;
-(void)updatePaymentConfigration:(GTLWalnutMPaymentConfig*)config;
-(id)objectOfClassString:(NSString*)classStr;

-(NSArray*)allObjectsOfClass:(NSString*)className;
-(NSArray*)allObjectsOfClass:(NSString*)className andSortDescriptors:(NSArray*)sortDescri;
-(NSArray*)allObjectsOfClass:(NSString*)className withPredicate:(NSPredicate*)predicate;
-(NSArray*)allObjectsOfClass:(NSString*)className withPredicate:(NSPredicate*)predicate andSortDescriptors:(NSArray*)sortDescri;
-(NSArray*)allObjectMatchingProperty:(NSString*)prop inArray:(NSArray *)dataArray arrayKey:(NSString *)distinctKey ofClass:(NSString *)entity;

-(BOOL)doesCurrentUserHaveMultipleGroupTransactionsWithUser:(id<User>)otherUser;
-(void)deleteTransactionFromDataBase:(WLTTransaction*)trans;
-(void)deleteAllObjects;
-(void)removeObjects:(NSSet*)objectsSet fromContext:(NSManagedObjectContext*)context;

-(NSInteger)countForObjectsOfClass:(NSString*)className withPredicate:(NSPredicate*)predicate;
-(NSArray*)distinctObjectsOfClass:(NSString*)classN forProperties:(NSArray*)props;

-(NSArray*)recentContactsList;
-(void)saveContactToRecentList:(WLTContact*)cont;

-(void)saveDataBase;
-(void)saveMainDataBase;

-(void)deleteGroups:(NSArray*)groups;
-(void)addGroups:(NSArray*)grpArray completion:(SuccessBlock)block;

@end

static inline BOOL PaymentsEnabled() {
    
    if ([WLTDatabaseManager sharedManager].paymentConfig) {
        
        WLTPaymentConfig *config = [WLTDatabaseManager sharedManager].paymentConfig;
        if (config.p2pPaymentFlag.boolValue) {
            return YES;
        }
    }
    
    return NO;
}

static inline BOOL SendReceivePaymentsEnabled() {
    
    if ([WLTDatabaseManager sharedManager].paymentConfig) {
        
        WLTPaymentConfig *config = [WLTDatabaseManager sharedManager].paymentConfig;
        
        if ( config.sendRequestEnabled.boolValue ) {
            return YES;
        }
    }
    
    return NO;
}