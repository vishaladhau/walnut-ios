//
//  WLTPartial.h
//  walnut
//
//  Created by Abhinav Singh on 22/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WLTGroup;

@interface WLTPartialGroup : NSObject <NSCopying>{
    
}

@property(nonatomic, assign) CGFloat currentAmount;

@property(readonly, assign) CGFloat acctualAmount;
@property(readonly, strong) WLTGroup *group;

@end


@interface WLTPartial : NSObject <NSCopying>

-(instancetype)initWithCombinedGroup:(WLTCombinedGroupPayments*)grpData;

@property(nonatomic, assign) CGFloat currentAmount;

@property(readonly, assign) CGFloat totalGetAmount;
@property(readonly, assign) CGFloat totalPayAmount;

@property(readonly, strong) id <User> user;

@property(readonly, strong) NSArray *payGroups;
@property(readonly, strong) NSArray *getGroups;

-(NSArray*)fixedGroups;
-(NSArray*)changableGroups;

@end
