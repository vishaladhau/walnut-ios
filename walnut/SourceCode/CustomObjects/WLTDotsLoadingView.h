//
//  WLTDotsLoadingView.h
//  walnut
//
//  Created by Abhinav Singh on 27/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLTDotsLoadingView : UIView {
    
    __weak UIActivityIndicatorView *activityView;
}

-(void)startAnimating;
-(void)stopAnimating;

@end
