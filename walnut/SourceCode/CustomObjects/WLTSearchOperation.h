//
//  WLTSearchOperation.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 07/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol WLTSearchableObject <NSObject>
-(NSArray*)searchableValues;
@end

@class WLTSearchOperation;

@protocol WLTSearchDelegate <NSObject>

-(void)searchCompleted:(WLTSearchOperation*)search forString:(NSString*)string
            withResults:(NSMutableArray*)results;

@end

@interface WLTSearchOperation : NSOperation {
    
    NSString *searchString;
    NSArray *dataArray;
}

@property(nonatomic, strong) NSString *identifier;
@property(nonatomic, weak) id <WLTSearchDelegate> delegate;

-(instancetype)initWithDataArray:(NSArray*)toSearch andSearchString:(NSString*)search;
-(BOOL)isSearchTermContainedBy:(NSString*)string;

@end
