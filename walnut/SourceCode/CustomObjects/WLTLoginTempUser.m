//
//  WLTLoginTempUser.m
//  walnut
//
//  Created by Abhinav Singh on 03/08/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTLoginTempUser.h"

@implementation WLTLoginTempUser

- (instancetype)init {
    
    self = [super init];
    if (self) {
        
        _throughSMS = YES;
    }
    
    return self;
}

@end
