//
//  WLTShareActivityProvider.m
//  walnut
//
//  Created by Abhinav Singh on 06/09/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTShareActivityProvider.h"

@implementation WLTSharingHelper

+(NSArray*)activityItemsForType:(WLTShareActivityType)type {
    
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    WLTShareActivityProvider *provider = [WLTShareActivityProvider activityWithType:type];
    [sharingItems addObject:provider];
    
    if (type == WLTShareActivityTypeDefault) {
        [sharingItems addObject:[NSURL URLWithString:@"http://wnut.in/wnutshare"]];
    }
    else if (type == WLTShareActivityTypeTransactionPaid) {
        [sharingItems addObject:[NSURL URLWithString:@"http://wnut.in/wnutPay"]];
    }
    else if (type == WLTShareActivityTypeTransactionReceived) {
        [sharingItems addObject:[NSURL URLWithString:@"http://wnut.in/wnutPay"]];
    }
    [sharingItems addObject:[UIImage imageNamed:@"ShareImage"]];
    
    return sharingItems;
}

@end

@implementation WLTShareActivityProvider {
    
    NSString *sharingText;
}

#pragma mark - Initialization

+(NSString*)shareTextForActivityType:(WLTShareActivityType)type {
    
    NSString *shareText = nil;
    if (type == WLTShareActivityTypeDefault) {
        
        shareText = @"Go get #WalnutPay. The easiest way to split expenses and send money to your friends.";
    }
    else if (type == WLTShareActivityTypeTransactionPaid) {
        shareText = @"Just sent money to my friend with #WalnutPay – Super fast and easy!";
    }
    else if (type == WLTShareActivityTypeTransactionReceived) {
        shareText = @"Just received money from my friend using #WalnutPay - Super fast and easy!";
    }
    else if (type == WLTShareActivityTypeTransactionRemind) {
        shareText = @"I have sent you money on WalnutPay. To receive it, get Walnut App now! http://wnut.in/wnutPay";
    }
    
    return shareText;
}

+(WLTShareActivityProvider*)activityWithType:(WLTShareActivityType)type {
    
    NSString *shareText = [self shareTextForActivityType:type];
    return [[WLTShareActivityProvider alloc] initWithPlaceholderItem:shareText];
}

- (instancetype)initWithPlaceholderItem:(id)content {
    
    self = [super initWithPlaceholderItem:content];
    if (self) {
        
        sharingText = content;
    }
    
    return self;
}

#pragma mark - UIActivityItemSource

- (id)activityViewController:(UIActivityViewController *)activityViewController itemForActivityType:(NSString *)activityType {
    
    NSString *toPost = sharingText;
    if ([activityType isEqualToString:UIActivityTypePostToTwitter]) {
        toPost = [toPost stringByAppendingString:@" @getwalnutapp"];
    }
    
    return toPost;
}

@end
