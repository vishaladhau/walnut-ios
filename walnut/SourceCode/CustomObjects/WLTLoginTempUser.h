//
//  WLTLoginTempUser.h
//  walnut
//
//  Created by Abhinav Singh on 03/08/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GTLWalnutMPaymentConfig;

@interface WLTLoginTempUser : NSObject {
    
}

@property(nonatomic, strong) NSString *mobileNumber;
@property(nonatomic, strong) NSString *userName;
@property(nonatomic, strong) NSString *emailAddress;
@property(nonatomic, strong) NSString *deviceUUID;
@property(nonatomic, strong) NSString *authToken;
@property(nonatomic, assign) BOOL throughSMS;

@property(nonatomic, strong) GTLWalnutMPaymentConfig *paymentConfig;

@end
