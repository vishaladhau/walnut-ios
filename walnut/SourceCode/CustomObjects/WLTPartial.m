//
//  WLTPartial.m
//  walnut
//
//  Created by Abhinav Singh on 22/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTPartial.h"
#import "WLTCombinedGroupPayments.h"

@implementation WLTPartialGroup

-(id)copyWithZone:(NSZone *)zone {
    
    WLTPartialGroup *newBatch = [[[self class] allocWithZone:zone] initByCopying:self];
    return newBatch;
}

-(instancetype)initByCopying:(WLTPartialGroup*)other {
    
    self = [super init];
    if (self) {
        
        _currentAmount = other.currentAmount;
        _acctualAmount = other.acctualAmount;
        
        _group = other.group;
    }
    
    return self;
}

-(instancetype)initWithGroup:(WLTGroupPaymentData*)data {
    
    self = [super init];
    if (self) {
        
        _currentAmount = -1;
        _acctualAmount = data.amount;
        
        _group = data.group;
    }
    
    return self;
}

@end

@implementation WLTPartial

-(id)copyWithZone:(NSZone *)zone {
    
    WLTPartial *newBatch = [[[self class] allocWithZone:zone] initByCopying:self];
    return newBatch;
}

-(instancetype)initByCopying:(WLTPartial*)other {
    
    self = [super init];
    if (self) {
        
        NSArray *getGrps = [other.getGroups copy];
        NSArray *payGrps = [other.payGroups copy];
        
        _payGroups = payGrps;
        _getGroups = getGrps;
        
        _totalGetAmount = other.totalGetAmount;
        _totalPayAmount = other.totalPayAmount;
        
        _currentAmount = other.currentAmount;
        _user = other.user;
    }
    
    return self;
}

-(instancetype)initWithCombinedGroup:(WLTCombinedGroupPayments*)grpData {
    
    self = [super init];
    if (self) {
        
        _user = grpData.user;
        
        NSMutableArray *getGrps = [NSMutableArray new];
        NSMutableArray *payGrps = [NSMutableArray new];
        
        CGFloat getAmt = 0;
        CGFloat payAmt = 0;
        
        for ( WLTGroupPaymentData *dataGrp in grpData.groupsData ) {
            
            WLTPartialGroup *payGrp = [[WLTPartialGroup alloc] initWithGroup:dataGrp];
            if (dataGrp.transactionState == UserTransactionStateGet) {
                
                getAmt += payGrp.acctualAmount;
                [getGrps addObject:payGrp];
            }
            else if ( dataGrp.transactionState == UserTransactionStateOwe ) {
                
                payAmt += payGrp.acctualAmount;
                [payGrps addObject:payGrp];
            }
        }
        
        _payGroups = payGrps;
        _getGroups = getGrps;
        
        _totalPayAmount = payAmt;
        _totalGetAmount = getAmt;
        
        _currentAmount = -1;
        
        NSArray *fixed = [self fixedGroups];
        for ( WLTPartialGroup *grp in fixed ) {
            grp.currentAmount = grp.acctualAmount;
        }
    }
    
    return self;
}

-(void)setCurrentAmount:(CGFloat)currentAmount {
    
    if ( (_currentAmount != currentAmount) && (currentAmount >= 0)) {
        
        _currentAmount = currentAmount;
        [self currentAmountChanged];
    }
}

#pragma mark - Expected Overrides

-(void)currentAmountChanged {
    
}

-(NSArray*)fixedGroups {
    return nil;
}

-(NSArray*)changableGroups {
    return nil;
}

@end
