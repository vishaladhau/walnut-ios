//
//  WLTGroup+CoreDataProperties.m
//  walnut
//
//  Created by Harshad on 25/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WLTGroup+CoreDataProperties.h"

@implementation WLTGroup (CoreDataProperties)

@dynamic aName;
@dynamic colourID;
@dynamic createdTime;
@dynamic currentUserContr;
@dynamic currentUserGet;
@dynamic currentUserOwe;
@dynamic currentUserShare;
@dynamic deviceUUID;
@dynamic dName;
@dynamic groupTotal;
@dynamic groupUUID;
@dynamic isPrivate;
@dynamic isSilent;
@dynamic lastDisplayTime;
@dynamic lastSyncTime;
@dynamic lastUpdatedTime;
@dynamic sortingTime;
@dynamic unreadMessagesCount;
@dynamic currentUserSent;
@dynamic currentUserReceived;
@dynamic members;
@dynamic otherSettlements;
@dynamic owner;
@dynamic transactions;
@dynamic userGetFrom;
@dynamic userPayTo;
@dynamic currentUserActContr;

@end
