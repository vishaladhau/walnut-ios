//
//  WLTLoggedInUser+CoreDataProperties.h
//  walnut
//
//  Created by Abhinav Singh on 02/08/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WLTLoggedInUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLTLoggedInUser (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *gmailName;
@property (nullable, nonatomic, retain) NSNumber *totalToGet;
@property (nullable, nonatomic, retain) NSNumber *totalToPay;
@property (nullable, nonatomic, retain) NSString *wltAuthToken;

@end

NS_ASSUME_NONNULL_END
