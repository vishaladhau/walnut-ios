//
//  WLTTransactionUser.h
//  walnut
//
//  Created by Abhinav Singh on 25/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class WLTGroup, GTLWalnutMSplit, GTLWalnutMBalance, GTLWalnutMSplitUser;

typedef NS_ENUM(NSInteger, TransactionType) {
    
    TransactionTypePayment = 0,
    TransactionTypeNone,
};

NS_ASSUME_NONNULL_BEGIN

@interface WLTTransactionUser : NSManagedObject <User>

+(WLTTransactionUser*)userFromGTLSplit:(GTLWalnutMSplit*)split;
+(WLTTransactionUser*)userFromGTLBalance:(GTLWalnutMBalance*)balance
                                 andUser:(GTLWalnutMSplitUser*)user;


@end

NS_ASSUME_NONNULL_END

#import "WLTTransactionUser+CoreDataProperties.h"
