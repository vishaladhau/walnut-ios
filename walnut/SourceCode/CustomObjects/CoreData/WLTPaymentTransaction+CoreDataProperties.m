//
//  WLTPaymentTransaction+CoreDataProperties.m
//  walnut
//
//  Created by Abhinav Singh on 16/09/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WLTPaymentTransaction+CoreDataProperties.h"

@implementation WLTPaymentTransaction (CoreDataProperties)

@dynamic amount;
@dynamic deviceUuid;
@dynamic initiationDate;
@dynamic isPaidNumber;
@dynamic paynimoReferenceId;
@dynamic paynimoTransactionId;
@dynamic receiverBankName;
@dynamic receiverLast4Digits;
@dynamic receiverMessage;
@dynamic senderBankName;
@dynamic senderLast4Digits;
@dynamic statementUuid;
@dynamic statuMessage;
@dynamic statusNum;
@dynamic subStatusMessage;
@dynamic subStatusNum;
@dynamic transactionId;
@dynamic typeNum;
@dynamic updateDate;
@dynamic userMessage;
@dynamic walnutTransactionUuid;
@dynamic completionDate;
@dynamic sortingDate;
@dynamic groups;
@dynamic receiver;
@dynamic sender;

@end
