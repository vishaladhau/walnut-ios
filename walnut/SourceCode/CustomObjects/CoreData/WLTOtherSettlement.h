//
//  WLTOtherSettlement.h
//  walnut
//
//  Created by Abhinav Singh on 02/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class WLTUser;

NS_ASSUME_NONNULL_BEGIN

@interface WLTOtherSettlement : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "WLTOtherSettlement+CoreDataProperties.h"
