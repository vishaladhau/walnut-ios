//
//  WLTGroup+CoreDataProperties.h
//  walnut
//
//  Created by Harshad on 25/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WLTGroup.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLTGroup (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *aName;
@property (nullable, nonatomic, retain) NSString *colourID;
@property (nullable, nonatomic, retain) NSNumber *createdTime;
@property (nullable, nonatomic, retain) NSNumber *currentUserContr;
@property (nullable, nonatomic, retain) NSNumber *currentUserActContr;
@property (nullable, nonatomic, retain) NSNumber *currentUserGet;
@property (nullable, nonatomic, retain) NSNumber *currentUserOwe;
@property (nullable, nonatomic, retain) NSNumber *currentUserShare;
@property (nullable, nonatomic, retain) NSString *deviceUUID;
@property (nullable, nonatomic, retain) NSString *dName;
@property (nullable, nonatomic, retain) NSNumber *groupTotal;
@property (nullable, nonatomic, retain) NSString *groupUUID;
@property (nullable, nonatomic, retain) NSNumber *isPrivate;
@property (nullable, nonatomic, retain) NSNumber *isSilent;
@property (nullable, nonatomic, retain) NSNumber *lastDisplayTime;
@property (nullable, nonatomic, retain) NSNumber *lastSyncTime;
@property (nullable, nonatomic, retain) NSNumber *lastUpdatedTime;
@property (nullable, nonatomic, retain) NSNumber *sortingTime;
@property (nullable, nonatomic, retain) NSNumber *unreadMessagesCount;
@property (nullable, nonatomic, retain) NSNumber *currentUserSent;
@property (nullable, nonatomic, retain) NSNumber *currentUserReceived;
@property (nullable, nonatomic, retain) NSSet<WLTUser *> *members;
@property (nullable, nonatomic, retain) NSSet<WLTOtherSettlement *> *otherSettlements;
@property (nullable, nonatomic, retain) WLTUser *owner;
@property (nullable, nonatomic, retain) NSSet<WLTTransaction *> *transactions;
@property (nullable, nonatomic, retain) NSSet<WLTTransactionUser *> *userGetFrom;
@property (nullable, nonatomic, retain) NSSet<WLTTransactionUser *> *userPayTo;

@end

@interface WLTGroup (CoreDataGeneratedAccessors)

- (void)addMembersObject:(WLTUser *)value;
- (void)removeMembersObject:(WLTUser *)value;
- (void)addMembers:(NSSet<WLTUser *> *)values;
- (void)removeMembers:(NSSet<WLTUser *> *)values;

- (void)addOtherSettlementsObject:(WLTOtherSettlement *)value;
- (void)removeOtherSettlementsObject:(WLTOtherSettlement *)value;
- (void)addOtherSettlements:(NSSet<WLTOtherSettlement *> *)values;
- (void)removeOtherSettlements:(NSSet<WLTOtherSettlement *> *)values;

- (void)addTransactionsObject:(WLTTransaction *)value;
- (void)removeTransactionsObject:(WLTTransaction *)value;
- (void)addTransactions:(NSSet<WLTTransaction *> *)values;
- (void)removeTransactions:(NSSet<WLTTransaction *> *)values;

- (void)addUserGetFromObject:(WLTTransactionUser *)value;
- (void)removeUserGetFromObject:(WLTTransactionUser *)value;
- (void)addUserGetFrom:(NSSet<WLTTransactionUser *> *)values;
- (void)removeUserGetFrom:(NSSet<WLTTransactionUser *> *)values;

- (void)addUserPayToObject:(WLTTransactionUser *)value;
- (void)removeUserPayToObject:(WLTTransactionUser *)value;
- (void)addUserPayTo:(NSSet<WLTTransactionUser *> *)values;
- (void)removeUserPayTo:(NSSet<WLTTransactionUser *> *)values;

@end

NS_ASSUME_NONNULL_END
