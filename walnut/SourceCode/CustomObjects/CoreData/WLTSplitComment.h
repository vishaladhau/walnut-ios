//
//  WLTSplitComment.h
//  walnut
//
//  Created by Abhinav Singh on 25/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WLTTransaction.h"

typedef NS_ENUM(NSInteger, WLTSplitCommentState) {
    
    WLTSplitCommentStatePosting = 0,
    WLTSplitCommentStatePosted,
    WLTSplitCommentStateFailed
};

NS_ASSUME_NONNULL_BEGIN

@interface WLTSplitComment : WLTTransaction

// Insert code here to declare functionality of your managed object subclass
-(WLTSplitCommentState)state;

@end

NS_ASSUME_NONNULL_END

#import "WLTSplitComment+CoreDataProperties.h"
