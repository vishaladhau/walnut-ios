//
//  WLTLoggedInUser+CoreDataProperties.m
//  walnut
//
//  Created by Abhinav Singh on 02/08/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WLTLoggedInUser+CoreDataProperties.h"

@implementation WLTLoggedInUser (CoreDataProperties)

@dynamic gmailName;
@dynamic totalToGet;
@dynamic totalToPay;
@dynamic wltAuthToken;

@end
