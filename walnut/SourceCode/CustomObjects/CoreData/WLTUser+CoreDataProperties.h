//
//  WLTUser+CoreDataProperties.h
//  walnut
//
//  Created by Abhinav Singh on 14/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WLTUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLTUser (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *email;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *mobileString;

@end

NS_ASSUME_NONNULL_END
