//
//  WLTGroup.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 04/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class GTLWalnutMGroup;

NS_ASSUME_NONNULL_BEGIN

FOUNDATION_EXPORT NSString * const NotifyGroupColorChanged;
FOUNDATION_EXPORT NSString * const NotifyGroupDeleted;
FOUNDATION_EXPORT NSString * const NotifyGroupChanged;
FOUNDATION_EXPORT NSString * const NotifyGroupUnreadCountChanged;

@class WLTUser, WLTTransactionUser, WLTTransaction, WLTUser, WLTOtherSettlement;

@interface WLTGroup : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
-(UserTransactionState)userTransactionState;
-(void)updateGroupFromWLTGroup:(GTLWalnutMGroup*)grps;
-(void)updateMembersAndNameWLTGroup:(GTLWalnutMGroup*)grps;

@end

NS_ASSUME_NONNULL_END

#import "WLTGroup+CoreDataProperties.h"
