//
//  WLTPaymentConfig+CoreDataProperties.h
//  walnut
//
//  Created by Abhinav Singh on 09/09/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WLTPaymentConfig.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLTPaymentConfig (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *aliasId;
@property (nullable, nonatomic, retain) NSNumber *cardPaymentFlag;
@property (nullable, nonatomic, retain) NSString *cardRegPgmId;
@property (nullable, nonatomic, retain) NSNumber *flag;
@property (nullable, nonatomic, retain) NSString *identifierProperty;
@property (nullable, nonatomic, retain) NSNumber *isSuspended;
@property (nullable, nonatomic, retain) NSNumber *p2pMaxLimit;
@property (nullable, nonatomic, retain) NSNumber *p2pMinLimit;
@property (nullable, nonatomic, retain) NSNumber *p2pPaymentFlag;
@property (nullable, nonatomic, retain) NSNumber *sendRequestEnabled;
@property (nullable, nonatomic, retain) NSString *termsCondUrl;
@property (nullable, nonatomic, retain) NSString *txnPgmId;
@property (nullable, nonatomic, retain) NSString *url;
@property (nullable, nonatomic, retain) NSString *userId;
@property (nullable, nonatomic, retain) NSNumber *useUat;
@property (nullable, nonatomic, retain) NSString *suspensionMessage;

@end

NS_ASSUME_NONNULL_END
