//
//  WLTTransaction+CoreDataProperties.m
//  walnut
//
//  Created by Abhinav Singh on 13/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WLTTransaction+CoreDataProperties.h"

@implementation WLTTransaction (CoreDataProperties)

@dynamic amount;
@dynamic createdDate;
@dynamic isWpaySettled;
@dynamic notes;
@dynamic objID;
@dynamic placeName;
@dynamic sortingDate;
@dynamic transactionDate;
@dynamic txnID;
@dynamic type;
@dynamic updatedDate;
@dynamic senderBank;
@dynamic receiverBank;
@dynamic isCrossGroupSettlement;
@dynamic addedBy;
@dynamic owner;
@dynamic receiver;
@dynamic splits;

@end
