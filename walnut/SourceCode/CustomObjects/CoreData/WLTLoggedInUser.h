//
//  WLTLoggedInUser.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 04/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLTLoggedInUser : WLTUser


// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "WLTLoggedInUser+CoreDataProperties.h"
