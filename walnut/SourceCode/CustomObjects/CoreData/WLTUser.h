//
//  WLTUser.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 04/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@class WLTGroup;

@interface WLTUser : NSManagedObject <User>

// Insert code here to declare functionality of your managed object subclass

+(WLTUser*)userWithMobile:(NSString*)mob andName:(NSString*)name;

@end

NS_ASSUME_NONNULL_END

#import "WLTUser+CoreDataProperties.h"
