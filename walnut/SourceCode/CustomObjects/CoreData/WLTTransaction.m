//
//  WLTTransaction.m
//  walnut
//
//  Created by Abhinav Singh on 25/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTTransaction.h"
#import "WLTUser.h"

NSString * const TransactionObjectTypeSplitSettleIncomplete = @"split_settlement_incomplete";
NSString * const TransactionObjectTypeSplitSettleComplete = @"split_settlement_complete";
NSString * const TransactionObjectTypeSplit = @"split_transaction";
NSString * const TransactionObjectServerComment = @"server_comment";
NSString * const TransactionObjectSplitComment = @"split_comment";
NSString * const TransactionObjectSplitSettle = @"split_settlement";
NSString * const TransactionObjectGroupSettled = @"group_settled";

@implementation WLTTransaction

// Insert code here to add functionality to your managed object subclass

@end
