//
//  WLTGroup.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 04/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTGroup.h"
#import "WLTUser.h"
#import "GTLWalnutMGroup.h"
#import "WLTColorPalette.h"
#import "WLTDatabaseManager.h"
#import "WLTContactsManager.h"
#import "GTLWalnutMSplitUser.h"

NSString *const NotifyGroupChanged = @"NotifyGroupChanged";
NSString *const NotifyGroupColorChanged = @"NotifyGroupColorChanged";
NSString *const NotifyGroupDeleted = @"NotifyGroupDeleted";
NSString *const NotifyGroupUnreadCountChanged = @"NotifyGroupUnreadCountChanged";

@implementation WLTGroup

-(UserTransactionState)userTransactionState {
    
    UserTransactionState state = UserTransactionStateNone;
    if (self.currentUserGet.floatValue > 0) {
        state = UserTransactionStateGet;
    }else if (self.currentUserOwe.floatValue > 0) {
        state = UserTransactionStateOwe;
    }
    
    return state;
}

-(void)updateMembersAndNameWLTGroup:(GTLWalnutMGroup*)grps{
    
    self.aName = grps.name;
    
    NSMutableArray *toRemoveUsers = [NSMutableArray new];
    if (self.owner) {
        [toRemoveUsers addObject:self.owner];
    }
    [toRemoveUsers addObjectsFromArray:self.members.allObjects];
    
    WLTDatabaseManager *dbManager = [WLTDatabaseManager sharedManager];
    
    WLTUser * (^ userForMobileNumber)(NSString *mobNum, NSString *name) = ^WLTUser * (NSString *mobNum, NSString *name){
        
        WLTUser *retUser = nil;
        for ( WLTUser *usr in toRemoveUsers ) {
            
            if ([usr.mobileString isEqualToString:mobNum]) {
                retUser = usr;
                break;
            }
        }
        
        if (!retUser) {
            
            retUser = [dbManager objectOfClassString:@"WLTUser"];
            retUser.name = name;
            retUser.mobileString = mobNum;
        }else {
            
            [toRemoveUsers removeObject:retUser];
        }
        
        return retUser;
    };
    
    if (grps.owner) {
        
        self.owner = userForMobileNumber(grps.owner.mobileNumber, grps.owner.name);
    }
    
    [self removeMembers:self.members];
    for ( GTLWalnutMSplitUser *mem in grps.members ) {
        
        WLTUser *newMem = userForMobileNumber(mem.mobileNumber, mem.name);
        [self addMembersObject:newMem];
    }
    
    if (self.members.count == 2) {
        
        self.dName = @"Direct";
        
        for (WLTUser *mem in self.members) {
            if (![mem.mobileString isEqualToString:CURRENT_USER_MOB]) {
                
                __weak WLTGroup *weakGrp = self;
                __weak typeof(self) weakSelf = self;
                __weak WLTDatabaseManager *weakDb = dbManager;
                
                [[WLTContactsManager sharedManager] displayNameForUser:mem defaultIsMob:YES completion:^(NSString *data) {
                    
                    if (weakGrp && weakSelf) {
                        if (!data.length) {
                            data = @"Direct";
                        }
                        
                        weakGrp.dName = data;
                        [weakDb saveDataBase];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:NotifyGroupChanged object:weakGrp];
                    }
                }];
                
                break;
            }
        }
    }
    else {
        
        self.dName = self.aName;
        if (!self.dName) {
            self.dName = @"Direct";
        }
    }
    
    for (WLTUser *usersToRem in toRemoveUsers) {
        [self.managedObjectContext deleteObject:usersToRem];
    }
}

-(void)updateGroupFromWLTGroup:(GTLWalnutMGroup*)grps{
    
    if (!self.colourID.length) {
        [[WLTColorPalette sharedPalette] colorForGroup:self];
    }
    
    self.deviceUUID = grps.deviceUuid;
    self.groupUUID = grps.uuid;
    
    if (grps.privateProperty.boolValue) {
        self.isPrivate = @(YES);
    }else{
        self.isPrivate = @(NO);
    }
    
    self.createdTime = grps.creationDate;
    self.lastUpdatedTime = grps.lastUpdatedAt;
    
    if (self.createdTime.doubleValue > self.lastUpdatedTime.doubleValue) {
        self.sortingTime = self.createdTime;
    }
    else {
        self.sortingTime = self.lastUpdatedTime;
    }
    
    [self updateMembersAndNameWLTGroup:grps];
}

@end
