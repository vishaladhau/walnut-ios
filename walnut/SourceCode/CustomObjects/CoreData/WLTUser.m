//
//  WLTUser.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 04/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTUser.h"
#import "WLTDatabaseManager.h"

@implementation WLTUser

+(WLTUser*)userWithMobile:(NSString*)mobStr andName:(NSString*)name {
    
    WLTUser *user = [[WLTDatabaseManager sharedManager] objectOfClassString:@"WLTUser"];
    user.name = name;
    user.mobileString = mobStr;
    
    return user;
}

//-(NSString*)name {
//    return self.name;
//}

-(NSString*)completeNumber {
    return self.mobileString;
}

@end
