//
//  WLTPaymentInstrument+CoreDataProperties.m
//  walnut
//
//  Created by Abhinav Singh on 01/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WLTPaymentInstrument+CoreDataProperties.h"

@implementation WLTPaymentInstrument (CoreDataProperties)

@dynamic instrumentID;
@dynamic jsonString;

@end
