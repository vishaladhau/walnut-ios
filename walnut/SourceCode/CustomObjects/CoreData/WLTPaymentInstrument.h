//
//  WLTPaymentInstrument.h
//  
//
//  Created by Abhinav Singh on 28/05/16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface WLTPaymentInstrument : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "WLTPaymentInstrument+CoreDataProperties.h"
