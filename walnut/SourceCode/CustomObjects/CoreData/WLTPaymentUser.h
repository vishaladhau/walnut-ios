//
//  WLTPaymentUser.h
//  
//
//  Created by Abhinav Singh on 28/05/16.
//
//

#import <Foundation/Foundation.h>
#import "WLTUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLTPaymentUser : WLTUser

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "WLTPaymentUser+CoreDataProperties.h"
