//
//  WLTPaymentConfig.h
//  walnut
//
//  Created by Abhinav Singh on 11/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface WLTPaymentConfig : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "WLTPaymentConfig+CoreDataProperties.h"
