//
//  WLTTransaction.h
//  walnut
//
//  Created by Abhinav Singh on 25/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

FOUNDATION_EXPORT NSString * const TransactionObjectTypeSplit;
FOUNDATION_EXPORT NSString * const TransactionObjectServerComment;
FOUNDATION_EXPORT NSString * const TransactionObjectSplitComment;
FOUNDATION_EXPORT NSString * const TransactionObjectSplitSettle;
FOUNDATION_EXPORT NSString * const TransactionObjectGroupSettled;
FOUNDATION_EXPORT NSString * const TransactionObjectTypeSplitSettleIncomplete;
FOUNDATION_EXPORT NSString * const TransactionObjectTypeSplitSettleComplete;

@class WLTCategory, WLTUser, WLTGroup, WLTTransactionUser;

NS_ASSUME_NONNULL_BEGIN

@interface WLTTransaction : NSManagedObject



@end

NS_ASSUME_NONNULL_END

#import "WLTTransaction+CoreDataProperties.h"
