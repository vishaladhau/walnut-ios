//
//  WLTPaymentConfig+CoreDataProperties.m
//  walnut
//
//  Created by Abhinav Singh on 09/09/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WLTPaymentConfig+CoreDataProperties.h"

@implementation WLTPaymentConfig (CoreDataProperties)

@dynamic aliasId;
@dynamic cardPaymentFlag;
@dynamic cardRegPgmId;
@dynamic flag;
@dynamic identifierProperty;
@dynamic isSuspended;
@dynamic p2pMaxLimit;
@dynamic p2pMinLimit;
@dynamic p2pPaymentFlag;
@dynamic sendRequestEnabled;
@dynamic termsCondUrl;
@dynamic txnPgmId;
@dynamic url;
@dynamic userId;
@dynamic useUat;
@dynamic suspensionMessage;

@end
