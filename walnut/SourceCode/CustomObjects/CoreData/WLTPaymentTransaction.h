//
//  WLTPaymentTransaction.h
//  
//
//  Created by Abhinav Singh on 28/05/16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

typedef NS_ENUM(NSInteger, PaymentStatus) {
    
    PaymentStatusUnknown = 0,
    PaymentStatusPullInitiated,
    PaymentStatusPullFailed,
    PaymentStatusRequestInitiated,
    PaymentStatusPullSuccess,
    PaymentStatusPushPending,
    PaymentStatusPushSuccess,
    PaymentStatusPushFailed,
    PaymentStatusPushReversed,
};

typedef NS_ENUM(NSInteger, PaymentSubStatus) {
    
    PaymentSubStatusUnknown = 0,
    PaymentSubStatusReceiverNotOnWalnut,
    PaymentSubStatusSuccess,//Amount successfully debited from sender account.
    PaymentSubStatusReceiverNotHaveDefaultInstrument,
    PaymentSubStatusWaiting,//Local subStatus.
    PaymentSubStatusRetryAddInstrument,
    PaymentSubStatusRetry,
    PaymentSubStatusReceiverInstrumentUUIDNotFound,
    PaymentSubStatusReceiverReceivedMoney,
    PaymentSubStatusMoneyReceivedSuccessfully,
    PaymentSubStatusMoneyRevertInitiated,
    PaymentSubStatusMoneyRevertDueToNoAccept,
};

typedef NS_ENUM(NSInteger, PaymentType) {
    
    PaymentTypeUnknown = 0,
    PaymentTypePush,
    PaymentTypePull,
    PaymentTypeRequestToMe,
    PaymentTypeRequestFromMe,
};


NS_ASSUME_NONNULL_BEGIN

FOUNDATION_EXPORT NSString * const PaymentTransactionsChanged;
FOUNDATION_EXPORT NSString * const PaymentTransactionsCountChanged;

@class GTLWalnutMPaymentTransaction;
@class WLTPaymentUser;
@class WLTPaymentGroup;

@interface WLTPaymentTransaction : NSManagedObject

@property(nonatomic, assign) BOOL isPaid;
@property(nonatomic, assign) PaymentSubStatus subStatus;
@property(nonatomic, assign) PaymentStatus status;
@property(nonatomic, assign) PaymentType type;

-(void)updateValuesWithTransaction:(GTLWalnutMPaymentTransaction*)trans;

+(NSString*)stringForStatus:(PaymentStatus)status;
+(PaymentStatus)paymentStatusFromString:(NSString*)statusString;

+(PaymentType)paymentTypeFromString:(NSString*)typeString;
+(NSString*)stringForPaymentType:(PaymentType)type;

-(BOOL)isTransactionPullPush;
-(BOOL)isActionRequired;
-(BOOL)isPaymentRequired;

@end

NS_ASSUME_NONNULL_END

#import "WLTPaymentTransaction+CoreDataProperties.h"
