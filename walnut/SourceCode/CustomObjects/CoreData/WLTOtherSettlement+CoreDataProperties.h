//
//  WLTOtherSettlement+CoreDataProperties.h
//  walnut
//
//  Created by Abhinav Singh on 02/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WLTOtherSettlement.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLTOtherSettlement (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *amount;
@property (nullable, nonatomic, retain) WLTUser *fromUser;
@property (nullable, nonatomic, retain) WLTUser *toUser;

@end

NS_ASSUME_NONNULL_END
