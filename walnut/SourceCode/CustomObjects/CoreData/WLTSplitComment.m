//
//  WLTSplitComment.m
//  walnut
//
//  Created by Abhinav Singh on 25/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTSplitComment.h"

@implementation WLTSplitComment

-(WLTSplitCommentState)state {
    return self.stateNum.integerValue;
}

@end
