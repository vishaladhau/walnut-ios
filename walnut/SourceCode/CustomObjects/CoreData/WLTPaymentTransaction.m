//
//  WLTPaymentTransaction.m
//
//  Created by Abhinav Singh on 28/05/16.
//

#import "WLTPaymentTransaction.h"
#import "GTLWalnutMPaymentTransaction.h"
#import "WLTDatabaseManager.h"
#import "WLTPaymentUser.h"
#import "WLTPaymentGroup.h"
#import "GTLWalnutMPaymentTransactionGroup.h"
#import "WLTGroup.h"

NSString * const PaymentTransactionsChanged = @"PaymentTransactionsChanged";
NSString * const PaymentTransactionsCountChanged = @"PaymentTransactionsCountChanged";

@implementation WLTPaymentTransaction

@synthesize status;
@synthesize subStatus;
@synthesize isPaid;
@synthesize type;

+(PaymentStatus)paymentStatusFromString:(NSString*)statusString {
    
    PaymentStatus retStatus = PaymentStatusUnknown;
    for (PaymentStatus status = PaymentStatusUnknown; status <= PaymentStatusPushReversed; status++) {
        
        if ([[self stringForStatus:status] isEqualToString:statusString]) {
            retStatus = status;
            break;
        }
    }
    
    return retStatus;
}

+(PaymentSubStatus)paymentSubStatusFromString:(NSString*)statusString {
    
    PaymentSubStatus retStatus = PaymentSubStatusUnknown;
    for (PaymentSubStatus status = PaymentSubStatusUnknown; status <= PaymentSubStatusMoneyRevertDueToNoAccept; status++) {
        
        if ([[self stringForSubStatus:status] isEqualToString:statusString]) {
            retStatus = status;
            break;
        }
    }
    
    return retStatus;
}

+(NSString*)stringForSubStatus:(PaymentSubStatus)substatus {
    
    NSString *string = nil;
    switch (substatus) {
        case PaymentSubStatusReceiverNotOnWalnut:
            string = @"receiver_not_on_walnut";
            break;
        case PaymentSubStatusMoneyRevertInitiated:
            string = @"money_revert_initiated";
            break;
        case PaymentSubStatusReceiverReceivedMoney:
            string = @"receiver_received_money";
            break;
        case PaymentSubStatusMoneyRevertDueToNoAccept:
            string = @"money_revert_due_to_no_accept";
            break;
        case PaymentSubStatusMoneyReceivedSuccessfully:
            string = @"money_received_successfully";
            break;
        case PaymentSubStatusReceiverInstrumentUUIDNotFound:
            string = @"receiver_instrument_uuid_not_found";
            break;
        case PaymentSubStatusReceiverNotHaveDefaultInstrument:
            string = @"receiver_no_default_instrument";
            break;
        case PaymentSubStatusRetry:
            string = @"walnut_retry";
            break;
        case PaymentSubStatusRetryAddInstrument:
            string = @"walnut_retry_add_instrument";
            break;
        case PaymentSubStatusSuccess:
            string = @"success";
            break;
        default:
            string = @"unknown";
            break;
    }
    
    return string;
}

+(NSString*)stringForStatus:(PaymentStatus)status {
    
    NSString *string = nil;
    switch (status) {
        case PaymentStatusPushPending:
            string = @"pending";
            break;
        case PaymentStatusPushSuccess:
            string = @"success";
            break;
        case PaymentStatusPushFailed:
            string = @"push_fail";
            break;
        case PaymentStatusPushReversed:
            string = @"reversed";
            break;
        case PaymentStatusPullFailed:
            string = @"pull_fail";
            break;
        case PaymentStatusPullInitiated:
            string = @"pull_initiated";
            break;
        case PaymentStatusPullSuccess:
            string = @"pull_success";
            break;
        default:
            string = @"unknown";
            break;
    }
    
    return string;
}

+(NSString*)stringForPaymentType:(PaymentType)type {
    
    NSString *string = nil;
    switch (type) {
        case PaymentTypePush:
            string = @"push";
            break;
        case PaymentTypePull:
            string = @"pull";
            break;
        case PaymentTypeRequestToMe:
            string = @"request_to_me";
            break;
        case PaymentTypeRequestFromMe:
            string = @"request_from_me";
            break;
        default:
            string = @"unknown";
            break;
    }
    
    return string;
}

+(PaymentType)paymentTypeFromString:(NSString*)typeString {
    
    PaymentType retType = PaymentTypeUnknown;
    for ( PaymentType type = PaymentTypeUnknown; type <= PaymentTypeRequestFromMe; type++ ) {
        
        if ([[self stringForPaymentType:type] isEqualToString:typeString]) {
            retType = type;
            break;
        }
    }
    
    return retType;
}

-(BOOL)isPaymentRequired {
    
    BOOL required = NO;
    if (self.type == PaymentTypeRequestToMe){
        required = YES;
    }
    return required;
}

-(BOOL)isActionRequired {
    
    BOOL required = NO;
    if ([self isTransactionPullPush]){
        
        if ( (self.status == PaymentStatusPushPending) && !self.isPaid) {
            
            switch (self.subStatus) {
                    
                case PaymentSubStatusReceiverNotHaveDefaultInstrument:
                case PaymentSubStatusRetryAddInstrument:
                case PaymentSubStatusRetry:
                case PaymentSubStatusReceiverInstrumentUUIDNotFound:
                    required = YES;
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    return required;
}

-(BOOL)isTransactionPullPush {
    
    BOOL isPullPush = YES;
    if ((self.type == PaymentTypeRequestToMe) || (self.type == PaymentTypeRequestFromMe)) {
        
        isPullPush = NO;
    }
    
    return isPullPush;
}

-(void)updateValuesWithTransaction:(GTLWalnutMPaymentTransaction*)trans {
    
    self.amount = trans.amount;
    
    PaymentSubStatus subS = [WLTPaymentTransaction paymentSubStatusFromString:trans.subStatus];
    if ( (self.subStatus == PaymentSubStatusWaiting) ) {
        if (subS > PaymentSubStatusWaiting) {
            self.subStatusNum = @(subS);
        }
    }else {
        
        self.subStatusNum = @(subS);
    }
    
    self.typeNum = @([WLTPaymentTransaction paymentTypeFromString:trans.type]);
    
    PaymentStatus stat = [WLTPaymentTransaction paymentStatusFromString:trans.status];
    if ( ((self.type == PaymentTypeRequestToMe) || (self.type == PaymentTypeRequestFromMe)) && (stat == PaymentStatusUnknown)) {
        stat = PaymentStatusRequestInitiated;
    }
    self.statusNum = @(stat);
    
    self.statuMessage = nil;
    self.subStatusMessage = nil;
    
    if (trans.statusMessageJson.length) {
        
        NSData *data = [trans.statusMessageJson dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        if (dict[@"message"]) {
            
            self.statuMessage = dict[@"message"];
            self.subStatusMessage = dict[@"sub_text"];
        }
    }
    
    if (!self.statuMessage.length) {
        self.statuMessage = trans.statusMessage;
    }
    self.userMessage = trans.userMessage;
    self.receiverMessage = trans.replyMessage;
    
    self.initiationDate = [NSDate dateFromWalnutTimestamp:trans.initiationDate.longLongValue];
    self.updateDate = [NSDate dateFromWalnutTimestamp:trans.updateDate.longLongValue];
    if (trans.completionDate.longLongValue > 0) {
        self.completionDate = [NSDate dateFromWalnutTimestamp:trans.completionDate.longLongValue];
    }else {
        self.completionDate = nil;
    }
    
    if (self.completionDate) {
        self.sortingDate = self.completionDate;
    }else {
        self.sortingDate = self.initiationDate;
    }
    
    self.senderBankName = trans.senderBank;
    self.senderLast4Digits = trans.senderLast4Digits;
    
    self.receiverBankName = trans.receiverBank;
    self.receiverLast4Digits = trans.receiverLast4Digits;
    
    self.transactionId = trans.transactionUuid;
    self.paynimoReferenceId = trans.paynimoReferenceId;
    self.paynimoTransactionId = trans.paynimoTransactionId;
    self.statementUuid = trans.statementUuid;
    self.walnutTransactionUuid = trans.walnutTransactionUuid;
    self.deviceUuid = trans.deviceUuid;
    
    WLTDatabaseManager *manager = [WLTDatabaseManager sharedManager];
    
    if (!self.receiver) {
        self.receiver = [manager objectOfClassString:@"WLTPaymentUser"];
    }
    self.receiver.name = trans.receiverName;
    self.receiver.email = trans.receiverEmail;
    self.receiver.mobileString = trans.receiverPhone;
    
    if (!self.sender) {
        self.sender = [manager objectOfClassString:@"WLTPaymentUser"];
    }
    self.sender.name = trans.senderName;
    self.sender.mobileString = trans.senderPhone;
    
    NSMutableDictionary *mapping = [NSMutableDictionary new];
    for ( WLTPaymentGroup *grp in self.groups ) {
        mapping[grp.groupUUID] = grp;
    }
    
    NSArray *allIds = [trans.groupDetails valueForKeyPath:@"groupUuid"];
    if (allIds.count) {
        
        NSMutableDictionary *nameMapping = [NSMutableDictionary new];
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"groupUUID IN %@", allIds];
        NSArray *allSavedGrps = [[WLTDatabaseManager sharedManager] allObjectsOfClass:@"WLTGroup" withPredicate:pred];
        
        for ( WLTGroup *grpSaved in allSavedGrps ) {
            nameMapping[grpSaved.groupUUID] = grpSaved.dName;
        }
        
        [self removeGroups:self.groups];
        
        NSString *currentUserMob = CURRENT_USER_MOB;
        
        for ( GTLWalnutMPaymentTransactionGroup *grpNew in trans.groupDetails ) {
            
            WLTPaymentGroup *grp = mapping[grpNew.groupUuid];
            if (!grp) {
                grp = [[WLTDatabaseManager sharedManager] objectOfClassString:@"WLTPaymentGroup"];
                grp.groupUUID = grpNew.groupUuid;
            }
            
            NSString *newName = nameMapping[grp.groupUUID];
            if (newName.length) {
                grp.name = newName;
            }
            grp.amount = grpNew.amount;
            grp.splitTransactionID = grpNew.splitTransactionUuid;
            
            if ([grpNew.senderPhone isEqualToString:currentUserMob]) {
                grp.isPaidAmount = @(YES);
            }else {
                grp.isPaidAmount = @(NO);
            }
            
            mapping[grpNew.groupUuid] = nil;
            
            [self addGroupsObject:grp];
        }
    }
    
    for ( WLTPaymentGroup *toRem in mapping.allValues ) {
        [self.managedObjectContext deleteObject:toRem];
    }
}

-(PaymentSubStatus)subStatus {
    return self.subStatusNum.integerValue;
}

-(PaymentStatus)status {
    return self.statusNum.integerValue;
}

-(PaymentType)type {
    return self.typeNum.integerValue;
}

-(BOOL)isPaid {
    return self.isPaidNumber.boolValue;
}

@end
