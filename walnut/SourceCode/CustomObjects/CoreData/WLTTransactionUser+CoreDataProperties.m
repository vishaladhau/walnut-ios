//
//  WLTTransactionUser+CoreDataProperties.m
//  walnut
//
//  Created by Abhinav Singh on 14/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WLTTransactionUser+CoreDataProperties.h"

@implementation WLTTransactionUser (CoreDataProperties)

@dynamic amount;
@dynamic name;
@dynamic mobileString;

@end
