//
//  WLTPaymentTransaction+CoreDataProperties.h
//  walnut
//
//  Created by Abhinav Singh on 16/09/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WLTPaymentTransaction.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLTPaymentTransaction (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *amount;
@property (nullable, nonatomic, retain) NSString *deviceUuid;
@property (nullable, nonatomic, retain) NSDate *initiationDate;
@property (nullable, nonatomic, retain) NSNumber *isPaidNumber;
@property (nullable, nonatomic, retain) NSString *paynimoReferenceId;
@property (nullable, nonatomic, retain) NSString *paynimoTransactionId;
@property (nullable, nonatomic, retain) NSString *receiverBankName;
@property (nullable, nonatomic, retain) NSString *receiverLast4Digits;
@property (nullable, nonatomic, retain) NSString *receiverMessage;
@property (nullable, nonatomic, retain) NSString *senderBankName;
@property (nullable, nonatomic, retain) NSString *senderLast4Digits;
@property (nullable, nonatomic, retain) NSString *statementUuid;
@property (nullable, nonatomic, retain) NSString *statuMessage;
@property (nullable, nonatomic, retain) NSNumber *statusNum;
@property (nullable, nonatomic, retain) NSString *subStatusMessage;
@property (nullable, nonatomic, retain) NSNumber *subStatusNum;
@property (nullable, nonatomic, retain) NSString *transactionId;
@property (nullable, nonatomic, retain) NSNumber *typeNum;
@property (nullable, nonatomic, retain) NSDate *updateDate;
@property (nullable, nonatomic, retain) NSString *userMessage;
@property (nullable, nonatomic, retain) NSString *walnutTransactionUuid;
@property (nullable, nonatomic, retain) NSDate *completionDate;
@property (nullable, nonatomic, retain) NSDate *sortingDate;
@property (nullable, nonatomic, retain) NSSet<WLTPaymentGroup *> *groups;
@property (nullable, nonatomic, retain) WLTPaymentUser *receiver;
@property (nullable, nonatomic, retain) WLTPaymentUser *sender;

@end

@interface WLTPaymentTransaction (CoreDataGeneratedAccessors)

- (void)addGroupsObject:(WLTPaymentGroup *)value;
- (void)removeGroupsObject:(WLTPaymentGroup *)value;
- (void)addGroups:(NSSet<WLTPaymentGroup *> *)values;
- (void)removeGroups:(NSSet<WLTPaymentGroup *> *)values;

@end

NS_ASSUME_NONNULL_END
