//
//  WLTPaymentInstrument+CoreDataProperties.h
//  walnut
//
//  Created by Abhinav Singh on 01/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WLTPaymentInstrument.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLTPaymentInstrument (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *instrumentID;
@property (nullable, nonatomic, retain) NSString *jsonString;

@end

NS_ASSUME_NONNULL_END
