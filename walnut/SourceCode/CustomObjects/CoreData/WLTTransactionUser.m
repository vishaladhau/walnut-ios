//
//  WLTTransactionUser.m
//  walnut
//
//  Created by Abhinav Singh on 25/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTTransactionUser.h"
#import "GTLWalnutMSplit.h"
#import "WLTDatabaseManager.h"
#import "GTLWalnutMBalance.h"
#import "GTLWalnutMSplitUser.h"

@implementation WLTTransactionUser

//-(NSString*)name {
//    return self.name;
//}

-(NSString*)completeNumber {
    return self.mobileString;
}

+(WLTTransactionUser*)userFromGTLBalance:(GTLWalnutMBalance*)balance andUser:(GTLWalnutMSplitUser*)user{
    
    WLTTransactionUser *usr = [[WLTDatabaseManager sharedManager] objectOfClassString:@"WLTTransactionUser"];
    usr.amount = balance.amount;
    usr.name = user.name;
    usr.mobileString = user.mobileNumber;
    
    return usr;
}

+(WLTTransactionUser*)userFromGTLSplit:(GTLWalnutMSplit*)split {
    
    WLTTransactionUser *usr = [[WLTDatabaseManager sharedManager] objectOfClassString:@"WLTTransactionUser"];
    usr.amount = split.amount;
    usr.mobileString = split.mobileNumber;
    
    return usr;
}

@end
