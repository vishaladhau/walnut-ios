//
//  WLTTransaction+CoreDataProperties.h
//  walnut
//
//  Created by Abhinav Singh on 13/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WLTTransaction.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLTTransaction (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *amount;
@property (nullable, nonatomic, retain) NSDate *createdDate;
@property (nullable, nonatomic, retain) NSNumber *isWpaySettled;
@property (nullable, nonatomic, retain) NSString *notes;
@property (nullable, nonatomic, retain) NSString *objID;
@property (nullable, nonatomic, retain) NSString *placeName;
@property (nullable, nonatomic, retain) NSDate *sortingDate;
@property (nullable, nonatomic, retain) NSDate *transactionDate;
@property (nullable, nonatomic, retain) NSString *txnID;
@property (nullable, nonatomic, retain) NSString *type;
@property (nullable, nonatomic, retain) NSDate *updatedDate;
@property (nullable, nonatomic, retain) NSString *senderBank;
@property (nullable, nonatomic, retain) NSString *receiverBank;
@property (nullable, nonatomic, retain) NSNumber *isCrossGroupSettlement;
@property (nullable, nonatomic, retain) WLTUser *addedBy;
@property (nullable, nonatomic, retain) WLTUser *owner;
@property (nullable, nonatomic, retain) WLTUser *receiver;
@property (nullable, nonatomic, retain) NSSet<WLTTransactionUser *> *splits;

@end

@interface WLTTransaction (CoreDataGeneratedAccessors)

- (void)addSplitsObject:(WLTTransactionUser *)value;
- (void)removeSplitsObject:(WLTTransactionUser *)value;
- (void)addSplits:(NSSet<WLTTransactionUser *> *)values;
- (void)removeSplits:(NSSet<WLTTransactionUser *> *)values;

@end

NS_ASSUME_NONNULL_END
