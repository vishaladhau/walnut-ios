//
//  WLTAppSettings+CoreDataProperties.h
//  walnut
//
//  Created by Abhinav Singh on 01/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WLTAppSettings.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLTAppSettings (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *deviceIdentifier;
@property (nullable, nonatomic, retain) NSDate *lastGroupListSyncTime;
@property (nullable, nonatomic, retain) NSNumber *lastPayInstrumentsSyncTime;
@property (nullable, nonatomic, retain) NSNumber *lastPayTransactinsSyncTime;
@property (nullable, nonatomic, retain) NSNumber *mobileVerified;
@property (nullable, nonatomic, retain) NSString *pushToken;

@end

NS_ASSUME_NONNULL_END
