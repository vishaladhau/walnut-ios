//
//  WLTPaymentGroup.h
//  walnut
//
//  Created by Abhinav Singh on 07/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface WLTPaymentGroup : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "WLTPaymentGroup+CoreDataProperties.h"
