//
//  WLTPaymentGroup+CoreDataProperties.m
//  walnut
//
//  Created by Abhinav Singh on 10/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WLTPaymentGroup+CoreDataProperties.h"

@implementation WLTPaymentGroup (CoreDataProperties)

@dynamic amount;
@dynamic groupUUID;
@dynamic isPaidAmount;
@dynamic name;
@dynamic splitTransactionID;

@end
