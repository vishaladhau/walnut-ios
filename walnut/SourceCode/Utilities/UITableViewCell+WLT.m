//
//  UITableViewCell+WLT.m
//  walnut
//
//  Created by Abhinav Singh on 12/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "UITableViewCell+WLT.h"

@implementation UITableViewCell (WLT)

-(UIEdgeInsets)layoutMargins {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

@end
