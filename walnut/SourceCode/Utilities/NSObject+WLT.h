//
//  NSObject+WLT.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 08/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+WLT.h"

FOUNDATION_EXPORT NSString *const WalnutErrorDomain;
FOUNDATION_EXPORT NSString *const NotifyGmailSessionExpired;
FOUNDATION_EXPORT NSString *const NotifyPaymentsStatusChanged;

@protocol User <NSObject>

-(NSString*)name;
-(NSString*)completeNumber;

@end

@class WLTRootViewController;

@interface NSObject (WLT)

-(WLTRootViewController*)rootController;
-(void)showAlertForError:(NSError*)error;
-(id)nullCheck;

+(NSError*)walnutErrorDefault;
+(NSError*)walnutErrorWithMessage:(NSString*)message;
+(NSError*)walnutErrorWithTitle:(NSString*)title andMessage:(NSString*)message;

+(BOOL)checkAndShowToastIfNotGroupMember:(NSError*)error;
+(BOOL)isNotAGroupMemberError:(NSError*)error;

@end
