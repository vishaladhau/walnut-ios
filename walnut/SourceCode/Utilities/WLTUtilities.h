//
//  WLTUtilities.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 03/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#define DegreeToRadian(degree) (degree/57.2957795)
#define RadianToDegree(radian) (radian*57.2957795f)

@class WLTLoginTempUser;

static NSInteger IndianMobileLength = 10;
static NSInteger MaximumAmountHandled = 999999;

static CGFloat HomeScreensNavbarHeight = 100;

static NSString *NotifyUserMobileChanged = @"NotifyUserMobileChanged";
static NSString *NotifyUserInfoChanged = @"NotifyUserInfoChanged";
static NSString *NotifySendReceiveMoneyStatusChanged = @"NotifySendReceiveMoneyStatusChanged";

#warning - Check Version Number Backend/Andriod Team Before Going Live
static NSString *AppVersionString = @"212";

typedef void (^ CompletionBlock)();
typedef void (^ DataCompletionBlock)(id data);
typedef void (^ SuccessBlock)(BOOL success);
typedef void (^ LoginBlock)(WLTLoginTempUser *loggedinUser);

#import "UIFont+WLT.h"
#import "UIColor+WLT.h"
#import "UIView+Frames.h"
#import "UIViewController+Helper.h"
#import "NSNumber+WLT.h"
#import "UIView+WLT.h"
#import "UINavigationController+WLT.h"

#import "NSObject+WLT.h"
#import "UITableViewCell+WLT.h"
#import "NSString+WLT.h"
#import "NSDate+WLT.h"
#import "UILabel+User.h"
#import "NSString+Card.h"
#import "NSObject+Analytics.h"
