//
//  UIView+WLT.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 05/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

static CGFloat DefaultCornerRadius = 3;

#define GroupCellImageAlphaInsets UIEdgeInsetsMake( 2, 4, 6, 4)

typedef NS_ENUM(NSInteger, BorderLinePosition) {
    
    BorderLinePositionTop = 1 << 0,
    BorderLinePositionBottom = 1 << 1,
    BorderLinePositionLeft = 1 << 2,
    BorderLinePositionRight = 1 << 3,
    BorderLineHorizantalCenter = 1 << 4,
    BorderLineVerticalCenter = 1 << 5,
};

@interface UIView (WLT)

+(instancetype)initWithDefaultXib;

-(void)addBlurBackgroundDark:(BOOL)isDark;
+(UIView*)lightBlurView;
+(UIView*)darkBlurView;

-(NSArray*)addBorderLineAtPosition:(BorderLinePosition)position;
-(NSArray*)addBorderLineAtPosition:(BorderLinePosition)position ofMagnitude:(CGFloat)magn
                          andColor:(UIColor*)color andOffset:(CGFloat)offset;

-(UIImage*)snapShotWithoutAddSubview;
-(UIImage *)snapshotOfRect:(CGRect)rect;

+(UIImage*)snapshotWithImage:(UIImage*)image backGroundColor:(UIColor*)color andSize:(CGSize)size;

@end
