//
//  NSObject+Analytics.h
//  walnut
//
//  Created by Abhinav Singh on 31/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *GA_ACTION_PROFILE_RESTORED = @"ProfileRestored";

static NSString *GA_ACTION_OTP_VALIDATE_TYPE = @"OtpValidateType";
static NSString *GA_LABEL_OTP_VALIDATED_SILENTLY_LATE_SMS = @"OtpValidatedSilentlyLateSms";
static NSString *GA_LABEL_OTP_VALIDATED_BY_REQUEST_OTP = @"OtpValidatedByRequestOtp";

//static NSString *GA_TIMING_OTP_SMS = @"OtpSmsTime";
//static NSString *GA_TIMING_VERIFICATION = @"VerificationTime";

static NSString *GA_ACTION_PRIVATE_GROUP_ADDED = @"PrivateGroupAdded";
static NSString *GA_ACTION_GROUP_ADDED = @"GroupAdded";
static NSString *GA_ACTION_GROUP_MODIFIED = @"GroupModified";
static NSString *GA_ACTION_GROUP_DELETED = @"GroupDeleted";
static NSString *GA_ACTION_GROUP_LEFT = @"GroupLeft";

static NSString *GA_ACTION_SPLIT_ADDED = @"SplitAdded";
//static NSString *GA_ACTION_SPLIT_EDITED = @"SplitEdited";
static NSString *GA_ACTION_SPLIT_DELETED = @"SplitDeleted";
static NSString *GA_ACTION_SPLIT_STATUS = @"SplitStatus";
static NSString *GA_LABEL_SPLIT_STATUS_EQUALLY = @"Equally";
static NSString *GA_LABEL_SPLIT_STATUS_UNEQUALLY = @"Unequally";


static NSString *GA_ACTION_COMMENT_ADDED = @"CommentAdded";
//static NSString *GA_ACTION_COMMENT_DELETED = @"CommentDeleted";

static NSString *GA_ACTION_SETTLEMENT_ADDED = @"SettlementAdded";

static NSString *GA_ACTION_SEND_REMINDER = @"SendReminder";

static NSString *GA_ACTION_INSIGHTS_SHARE_SELECTED = @"InsightsShareSelected";

//Payments
static NSString *GA_ACTION_CARD_ADDED = @"CardAdded";
static NSString *GA_ACTION_WPAY_CLICKED = @"WPayClicked";
static NSString *GA_ACTION_2FA_PAGE_LOAD_TIME = @"2FAPageLoadTime";
static NSString *GA_ACTION_PAYNIMO_API_TIME = @"PaynimoApiTime";
static NSString *GA_ACTION_PAYMENT_FAILED = @"PaymentFailed";
static NSString *GA_ACTION_PAYMENT_CANCELLED = @"PaymentCancelled";
static NSString *GA_ACTION_PAYMENT_SUCCESS = @"PaymentSuccess";
static NSString *GA_ACTION_PAYMENT_INITIATED = @"PaymentInitiated";

@interface NSObject (Analytics)

-(void)trackEvent:(NSString*)eventName
            label:(NSString*)label
         andValue:(NSNumber*)value;

-(void)trackEvent:(NSString*)eventName
         andValue:(NSNumber*)value;

@end
