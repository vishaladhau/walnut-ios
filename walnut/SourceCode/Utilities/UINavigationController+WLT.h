//
//  UINavigationController+WLT.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 07/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (WLT)

-(void)applyDefaultStyle;
-(void)applyTranslusentStyle;
-(void)applyTransparentStyle;

-(NSArray*)allControllersUpTo:(UIViewController*)controller;
-(NSArray*)allControllersUpTo:(UIViewController*)controller exclude:(BOOL)exclude;

-(UIViewController*)rootViewController;

@end
