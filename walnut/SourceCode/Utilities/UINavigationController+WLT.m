//
//  UINavigationController+WLT.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 07/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "UINavigationController+WLT.h"
#import "WLTColorPalette.h"

@implementation UINavigationController (WLT)

-(void)applyDefaultStyle{
    
    UINavigationBar *bar = self.navigationBar;
    
    [bar setBackgroundImage:[UIImage new]
             forBarPosition:UIBarPositionAny
                 barMetrics:UIBarMetricsDefault];
    
    [bar setShadowImage:[UIImage new]];
    
    [bar setTitleTextAttributes:@{NSFontAttributeName:[UIFont appTitleFont], NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [bar setTintColor:[UIColor whiteColor]];
    [bar setTranslucent:NO];
    [bar setOpaque:YES];
}

-(void)applyTranslusentStyle{
    
    UINavigationBar *bar = self.navigationBar;
    [bar setBarStyle:UIBarStyleDefault];
    
    [bar setTitleTextAttributes:@{NSFontAttributeName:[UIFont appTitleFont], NSForegroundColorAttributeName:[UIColor rhinoColor]}];
    
    [bar setTintColor:[UIColor rhinoColor]];
    [bar setTranslucent:YES];
    [bar setOpaque:NO];
}

-(void)applyTransparentStyle{
    
    UINavigationBar *bar = self.navigationBar;
    
    [bar setBackgroundImage:[UIImage new]
             forBarPosition:UIBarPositionAny
                 barMetrics:UIBarMetricsDefault];
    [bar setBackgroundColor:[UIColor lightBackgroundColor]];
    
    [bar setShadowImage:[UIImage new]];
    
    [bar setTitleTextAttributes:@{NSFontAttributeName:[UIFont appTitleFont], NSForegroundColorAttributeName:[UIColor whiteColor]}];
}

+(void)applyOpaqueStyleToObject:(UINavigationBar*)bar {
    
    [bar setTintColor:[UIColor whiteColor]];
    
    [bar setBackgroundImage:[[UIImage alloc] init]
             forBarPosition:UIBarPositionAny
                 barMetrics:UIBarMetricsDefault];
}

-(NSArray*)allControllersUpTo:(UIViewController*)controller {
    
    return [self allControllersUpTo:controller exclude:NO];
}

-(NSArray*)allControllersUpTo:(UIViewController*)controller exclude:(BOOL)exclude{
    
    NSArray *subArray = nil;
    NSInteger index = [self.viewControllers indexOfObject:controller];
    if (index != NSNotFound) {
        
        if (exclude) {
            subArray = [self.viewControllers subarrayWithRange:NSMakeRange(0, index)];
        }else {
            subArray = [self.viewControllers subarrayWithRange:NSMakeRange(0, index+1)];
        }
    }
    
    return subArray;
}

-(UIViewController*)rootViewController {
    return [[self viewControllers] firstObject];
}

@end
