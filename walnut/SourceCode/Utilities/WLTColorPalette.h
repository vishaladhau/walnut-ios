//
//  WLTColorPalette.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 04/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WLTGroup;

@interface WLTColorPalette : NSObject {
    
}

+(WLTColorPalette*)sharedPalette;
-(UIColor*)colorForID:(NSString*)clrID;
-(NSArray*)allColorIDs;

#pragma mark - Group

-(UIColor*)colorForGroup:(WLTGroup*)grp;
-(NSString*)randomGroupColorID;

#pragma mark - Instrument

-(UIColor*)colorForInstrumentID:(NSString*)instrID;
-(NSString*)randomInstrumentColorID;

#pragma mark - Names

-(UIColor*)colorForUser:(id<User>)user;

@end
