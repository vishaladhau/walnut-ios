//
//  UIFont+WLT.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 02/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "UIFont+WLT.h"

@implementation UIFont (WLT)

+(UIFont*)sfUITextRegularOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"SFUIText-Regular" size:size];
}

+(UIFont*)sfUITextRegularItallicOfSize:(CGFloat)size {
    
    return [UIFont fontWithName:@"SFUIText-Italic" size:size];
}

+(UIFont*)sfUITextMediumOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"SFUIText-Medium" size:size];
}

+(UIFont*)sfUITextLightOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"SFUIText-Light" size:size];
}

+(UIFont*)sfUITextBoldOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"SFUIText-Bold" size:size];
}

+(UIFont*)sfUITextSemiboldOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"SFUIText-Semibold" size:size];
}

+(UIFont*)sfUITextHeavyOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"SFUIText-Heavy" size:size];
}

+(UIFont*)ocrFontOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"OCRAExtended" size:size];
}

+(UIFont*)appTitleFont {
    return [self sfUITextRegularOfSize:17];
}

+(UIFont*)appSubTitleFont {
    return [self sfUITextRegularOfSize:14];
}

+(UIFont*)appBarButtonFont {
    return [self sfUITextRegularOfSize:16];
}

@end
