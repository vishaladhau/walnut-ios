//
//  UIView+WLT.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 05/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "UIView+WLT.h"

@implementation UIView (WLT)

+(instancetype)initWithDefaultXib {
    
    NSArray *allObjects = [[UINib nibWithNibName:NSStringFromClass(self)
                                          bundle:nil]
                           instantiateWithOwner:nil
                           options:nil];
    
    UIView *thisView = [allObjects firstObject];
    return thisView;
}

-(NSArray*)addBorderLineAtPosition:(BorderLinePosition)position ofMagnitude:(CGFloat)magn
                           andColor:(UIColor*)color andOffset:(CGFloat)offset{
    
    if (!magn) {
        magn = 0.5;
    }
    if (!color) {
        color = [UIColor colorWithRed:(200/255.0f) green:(199/255.0f) blue:(205/255.0f) alpha:1];
    }
    
    NSMutableArray *lines = [NSMutableArray new];
    
    UIView * (^ defaultLineView)() = ^UIView * (){
        
        UIView *seperatorLine = [[UIView alloc] initWithFrame:CGRectZero];
        seperatorLine.translatesAutoresizingMaskIntoConstraints = NO;
        seperatorLine.backgroundColor = color;
        [self addSubview:seperatorLine];
        [lines addObject:seperatorLine];
        
        return seperatorLine;
    };
    
    UIView *selfView = self;
    
    if ((position&BorderLinePositionTop)) {
        
        UIView *viewAdded = defaultLineView();
        NSDictionary *views = NSDictionaryOfVariableBindings(selfView, viewAdded);
        
        [selfView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%f-[viewAdded]-0-|", offset] options:0 metrics:nil views:views]];
        [selfView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[viewAdded(==%f)]",offset, magn] options:0 metrics:nil views:views]];
    }
    
    if ((position&BorderLinePositionBottom)) {
        
        UIView *viewAdded = defaultLineView();
        NSDictionary *views = NSDictionaryOfVariableBindings(selfView, viewAdded);
        
        [selfView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%f-[viewAdded]-0-|", offset] options:0 metrics:nil views:views]];
        [selfView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[viewAdded(==%f)]-0-|", magn] options:0 metrics:nil views:views]];
    }
    
    if ((position&BorderLinePositionLeft)) {
        
        UIView *viewAdded = defaultLineView();
        NSDictionary *views = NSDictionaryOfVariableBindings(selfView, viewAdded);
        
        [selfView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-0-[viewAdded(==%f)]",magn] options:0 metrics:nil views:views]];
        [selfView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[viewAdded]-0-|", offset] options:0 metrics:nil views:views]];
    }
    
    if ((position&BorderLinePositionRight)) {
        
        UIView *viewAdded = defaultLineView();
        NSDictionary *views = NSDictionaryOfVariableBindings(selfView, viewAdded);
        
        [selfView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[viewAdded(==%f)]-%f-|", magn, offset] options:0 metrics:nil views:views]];
        [selfView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[viewAdded(==selfView)]-0-|",offset] options:0 metrics:nil views:views]];
    }
    
    if ((position&BorderLineHorizantalCenter)) {
        
        UIView *viewAdded = defaultLineView();
        NSDictionary *views = NSDictionaryOfVariableBindings(selfView, viewAdded);
        
        [selfView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%f-[viewAdded]-0-|", offset] options:0 metrics:nil views:views]];
        [selfView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[viewAdded(==%f)]", magn] options:0 metrics:nil views:views]];
        
        [selfView addConstraint:[NSLayoutConstraint constraintWithItem:viewAdded
                                                             attribute:NSLayoutAttributeCenterY
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:selfView
                                                             attribute:NSLayoutAttributeCenterY
                                                            multiplier:1 constant:0]];
    }
    
    return lines;
}

-(NSArray*)addBorderLineAtPosition:(BorderLinePosition)position {
    
    return [self addBorderLineAtPosition:position ofMagnitude:0 andColor:nil andOffset:0];
}

-(void)addBlurBackgroundDark:(BOOL)isDark {
    
    UIView *blurView = nil;
    if (isDark) {
        blurView = [UIView darkBlurView];
    }else {
        blurView = [UIView lightBlurView];
    }
    
    blurView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:blurView];
    self.backgroundColor = [UIColor clearColor];
    [self sendSubviewToBack:blurView];
    
    NSDictionary *dictBlur = NSDictionaryOfVariableBindings(blurView);
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[blurView]-0-|" options:0 metrics:nil views:dictBlur]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[blurView]-0-|" options:0 metrics:nil views:dictBlur]];
}

+(UIView*)lightBlurView {
    
    UIView *blurView = nil;
    if ([UIVisualEffect class]) {
        
        UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurView = visualEffectView;
    }else {
        
        blurView = [[UIView alloc] initWithFrame:CGRectZero];
        blurView.backgroundColor = [UIColor whiteColor];
    }
    
    return blurView;
}

+(UIView*)darkBlurView {
    
    UIView *blurView = nil;
    if ([UIVisualEffect class]) {
        
        UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurView = visualEffectView;
    }else {
        
        blurView = [[UIView alloc] initWithFrame:CGRectZero];
        blurView.backgroundColor = [UIColor colorWithWhite:0.22 alpha:1];
    }
    
    return blurView;
}

- (UIImage*)snapShotWithoutAddSubview {
    
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, [UIScreen mainScreen].scale);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *screengrab = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return screengrab;
}

-(UIImage *)snapshotOfRect:(CGRect)rect {
    
    CGFloat scale = [UIScreen mainScreen].scale;
    
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, scale);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    
    CGRect newRect = CGRectApplyAffineTransform(rect, CGAffineTransformScale(CGAffineTransformIdentity, scale, scale));
    
    CGImageRef cropped = CGImageCreateWithImageInRect(snapshotImage.CGImage, newRect);
    snapshotImage = [UIImage imageWithCGImage:cropped scale:scale orientation:UIImageOrientationUp];
    
    UIGraphicsEndImageContext();
    
    return snapshotImage;
}

+(UIImage*)snapshotWithImage:(UIImage*)image backGroundColor:(UIColor*)color andSize:(CGSize)size {
    
    UIImageView *back = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    [back setBackgroundColor:color];
    back.contentMode = UIViewContentModeCenter;
    back.image = image;
    
    return [back snapShotWithoutAddSubview];
}

@end
