//
//  NSObject+Analytics.m
//  walnut
//
//  Created by Abhinav Singh on 31/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "NSObject+Analytics.h"
#import <Google/Analytics.h>

@implementation NSObject (Analytics)

-(void)trackEvent:(NSString*)eventName label:(NSString*)label andValue:(NSNumber*)value {
   
    NSDictionary *dataDict = [[GAIDictionaryBuilder createEventWithCategory:nil action:eventName label:label value:value] build];
    [[[GAI sharedInstance] defaultTracker] send:dataDict];
}

-(void)trackEvent:(NSString*)eventName andValue:(NSNumber*)value {
    
    [self trackEvent:eventName label:nil andValue:value];
}

@end
