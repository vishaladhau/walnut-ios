//
//  WLTColorPalette.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 04/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTColorPalette.h"
#import "WLTGroup.h"
#import "WLTDatabaseManager.h"

@interface WLTColorPalette () {
    
    NSMutableArray *usableGroupColorIds;
    
    NSMutableDictionary *userColorMappingDict;
    NSMutableArray *usableUserColorIds;
    
    NSMutableDictionary *instrumentColorMappingDict;
    NSMutableArray *usableInstrumentColorIds;
}

@property(nonatomic, readonly) NSDictionary *palette;

@end

@implementation WLTColorPalette

+(WLTColorPalette*)sharedPalette {
    
    static __strong WLTColorPalette *palette = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        palette = [[self alloc] init];
    });
    
    return palette;
}

-(NSArray*)allColorIDs {
    
    return self.palette.allKeys;
}

-(instancetype)init {
    
    self = [super init];
    if (self) {
        
        usableGroupColorIds = [NSMutableArray new];
        usableUserColorIds = [NSMutableArray new];
        userColorMappingDict = [NSMutableDictionary new];
        
        usableInstrumentColorIds = [NSMutableArray new];
        instrumentColorMappingDict = [NSMutableDictionary new];
        
        _palette = @{@"1":[UIColor colorWithRed:1.00 green:0.52 blue:0.34 alpha:1.00],
                     @"2":[UIColor colorWithRed:0.95 green:0.71 blue:0.35 alpha:1.00],
                     @"3":[UIColor colorWithRed:0.37 green:0.60 blue:0.81 alpha:1.00],
                     @"4":[UIColor colorWithRed:0.53 green:0.40 blue:0.85 alpha:1.00],
                     @"5":[UIColor colorWithRed:0.36 green:0.71 blue:0.71 alpha:1.00],
                     @"6":[UIColor colorWithRed:0.68 green:0.48 blue:0.40 alpha:1.00],
                     @"7":[UIColor colorWithRed:0.77 green:0.58 blue:0.78 alpha:1.00],
                     @"8":[UIColor colorWithRed:0.39 green:0.45 blue:0.50 alpha:1.00]};
        
        [usableGroupColorIds addObjectsFromArray:self.palette.allKeys];
        [usableUserColorIds addObjectsFromArray:self.palette.allKeys];
        [usableInstrumentColorIds addObjectsFromArray:self.palette.allKeys];
        
        NSArray *arrGrps = [[WLTDatabaseManager sharedManager] allObjectsOfClass:@"WLTGroup"];
        NSArray *allGIds = [arrGrps valueForKeyPath:@"@distinctUnionOfObjects.colourID"];
        
        if (allGIds.count < self.palette.count) {
            [usableGroupColorIds removeObjectsInArray:allGIds];
        }
    }
    
    return self;
}

-(UIColor*)colorForID:(NSString*)clrID {
    
    return self.palette[clrID];
}

#pragma mark - Group

-(UIColor*)colorForGroup:(WLTGroup*)grp {
    
    if (!grp.colourID.length) {
        
        grp.colourID = [self randomGroupColorID];
        [usableGroupColorIds removeObject:grp.colourID];
        
        if (usableGroupColorIds.count == 0) {
            
            [usableGroupColorIds addObjectsFromArray:self.palette.allKeys];
        }
    }
    
    return self.palette[grp.colourID];
}

-(NSString*)randomGroupColorID {
    
    NSInteger index = (arc4random()%usableGroupColorIds.count);
    NSString *clrID = usableGroupColorIds[index];
    
    return clrID;
}

#pragma mark - Users Color

-(NSString*)randomUserColorID {
    
    NSInteger index = (arc4random()%usableUserColorIds.count);
    NSString *clrID = usableUserColorIds[index];
    
    return clrID;
}

-(UIColor*)colorForUser:(id<User>)user {
    
    NSString *identifier = [user completeNumber];
    if (!identifier) {
        identifier = [user name];
    }
    if (identifier) {
        NSString *saved = userColorMappingDict[identifier];
        
        if (!saved) {
            
            saved = [self randomUserColorID];
            [usableUserColorIds removeObject:saved];
            
            if (!usableUserColorIds.count) {
                [usableUserColorIds addObjectsFromArray:self.palette.allKeys];
            }
            
            userColorMappingDict[identifier] = saved;
        }
        
        return self.palette[saved];
    }
    
    return [UIColor appleGreenColor];
}

#pragma mark - Instrument

-(NSString*)randomInstrumentColorID {
    
    NSInteger index = (arc4random()%usableInstrumentColorIds.count);
    NSString *clrID = usableInstrumentColorIds[index];
    
    return clrID;
}

-(UIColor*)colorForInstrumentID:(NSString*)instrID {
    
    if (instrID) {
        
        NSString *saved = instrumentColorMappingDict[instrID];
        if (!saved) {
            
            saved = [self randomInstrumentColorID];
            [usableInstrumentColorIds removeObject:saved];
            
            if (!usableInstrumentColorIds.count) {
                [usableInstrumentColorIds addObjectsFromArray:self.palette.allKeys];
            }
            
            instrumentColorMappingDict[instrID] = saved;
        }
        
        return self.palette[saved];
    }
    
    return [UIColor darkElectricBlueColor];
}

@end
