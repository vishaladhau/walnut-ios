//
//  NSString+Card.m
//  walnut
//
//  Created by Abhinav Singh on 04/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "NSString+Card.h"

static NSString *VisaCardType = @"^4[0-9]{3}?";
static NSString *MasterCardType = @"^5[1-5][0-9]{2}$";
static NSString *AmexCardType = @"^3[47][0-9]{2}$";
static NSString *DinnersClubCardType = @"^3(?:0[0-5]|[68][0-9])[0-9]$";
static NSString *DiscoverCardType = @"^6(?:011|5[0-9]{2})$";
static NSString *MasteroCardType = @"^(5018|5020|5038|5081|5612|5893|6304|6759|6761|6762|6763|6799|0604|6390)+$";
static NSString *JCBType = @"^(35|2131|1800)+$";

@implementation NSString (Card)

-(NSString*)formattedCreditCardNumber {
    
    NSInteger completeGrps = (self.length/NumberGroupSize);
    NSInteger remaning = (self.length - (NumberGroupSize*completeGrps));
    
    NSString *withSeperators = self;
    if ( completeGrps > 0 ) {
        
        for( int i = 1; i <= completeGrps; i++ ) {
            
            if ( i == completeGrps ) {
                
                if (remaning > 0) {
                    
                    withSeperators = [withSeperators stringByReplacingCharactersInRange:NSMakeRange((NumberGroupSize*i)+((i-1)*CardNumberSeperator.length), 0) withString:CardNumberSeperator];
                }
            }else {
                
                withSeperators = [withSeperators stringByReplacingCharactersInRange:NSMakeRange((NumberGroupSize*i)+((i-1)*CardNumberSeperator.length), 0) withString:CardNumberSeperator];
            }
        }
    }
    
    return withSeperators;
}

-(NSString*)creditCardNumber {
    
    return [self stringByReplacingOccurrencesOfString:CardNumberSeperator withString:@""];
}

+(NSString*)emptyCardPlaceHolder {
    
    return [NSString stringWithFormat:@"XXXX%@XXXX%@XXXX%@XXXX", CardNumberSeperator, CardNumberSeperator, CardNumberSeperator];
}

-(NSString*)cardNumberForLast4Digits {
    
    return [NSString stringWithFormat:@"XXXX%@XXXX%@XXXX%@%@", CardNumberSeperator, CardNumberSeperator,CardNumberSeperator, self];
}

-(CreditCardBrand)cardBrand {
    
    CreditCardBrand cardType = CreditCardBrandUnknown;
    if([self length] >= 4) {
        
        cardType = CreditCardBrandInvalid;
        NSRegularExpression *regex = nil;
        NSError *error = nil;
        
        for( CreditCardBrand card = CreditCardBrandVisa; card < CreditCardBrandInvalid; card++ ) {
            
            cardType = card;
            
            switch(card) {
                case CreditCardBrandVisa:
                    regex = [NSRegularExpression regularExpressionWithPattern:VisaCardType options:0 error:&error];
                    break;
                case CreditCardBrandMasterCard:
                    regex = [NSRegularExpression regularExpressionWithPattern:MasterCardType options:0 error:&error];
                    break;
                case CreditCardBrandAmex:
                    regex = [NSRegularExpression regularExpressionWithPattern:AmexCardType options:0 error:&error];
                    break;
                case CreditCardBrandDinersClub:
                    regex = [NSRegularExpression regularExpressionWithPattern:DinnersClubCardType options:0 error:&error];
                    break;
                case CreditCardBrandDiscover:
                    regex = [NSRegularExpression regularExpressionWithPattern:DiscoverCardType options:0 error:&error];
                    break;
                case CreditCardBrandMAESTRO:
                    regex = [NSRegularExpression regularExpressionWithPattern:MasterCardType options:0 error:&error];
                    break;
                case CreditCardBrandJCB:
                    regex = [NSRegularExpression regularExpressionWithPattern:JCBType options:0 error:&error];
                    break;
                default:
                    regex =nil;
                    break;
            }
            
            NSUInteger matches = [regex numberOfMatchesInString:self options:0 range:NSMakeRange(0, 4)];
            if(matches == 1) {
                
                break;
            }
        }
    }
    
    return cardType;
}

-(BOOL)isValidCreditCard {
    
    BOOL valid = NO;
    if (self.length > 9) {
        
        NSInteger len = self.length;
        NSInteger oddDigits = 0;
        NSInteger evenDigits = 0;
        BOOL isOdd = YES;
        
        for (NSInteger i = len - 1; i >= 0; i--) {
            
            NSInteger number = [self substringWithRange:NSMakeRange(i, 1)].integerValue;
            if (isOdd) {
                oddDigits += number;
            }else{
                number = number * 2;
                if (number > 9) {
                    number = number - 9;
                }
                evenDigits += number;
            }
            isOdd = !isOdd;
        }
        
        valid = ((oddDigits + evenDigits) % 10 == 0);
    }
    
    return valid;
}

+(NSString*)displayNameForCardType:(CreditCardBrand)brand {
    
    NSString *name = nil;
    switch (brand) {
        case CreditCardBrandAmex:
            name = @"AMEX";
            break;
        case CreditCardBrandDinersClub:
            name = @"Diners Club";
            break;
        case CreditCardBrandDiscover:
            name = @"Discover";
            break;
        case CreditCardBrandJCB:
            name = @"JCB";
            break;
        case CreditCardBrandMAESTRO:
            name = @"MAESTRO";
            break;
        case CreditCardBrandMasterCard:
            name = @"Master Card";
            break;
        case CreditCardBrandVisa:
            name = @"VISA";
            break;
        case CreditCardBrandUnknown:
        case CreditCardBrandInvalid:
            name = @"";
            break;
        default:
            break;
    }
    
    return name;
}

+(CreditCardBrand)cardTypeForDisplayString:(NSString*)name {
    
    CreditCardBrand brand = CreditCardBrandUnknown;
    if ([name isEqualToString:@"VISA"]) {
        brand = CreditCardBrandVisa;
    }
    else if ([name isEqualToString:@"Master"]) {
        brand = CreditCardBrandMasterCard;
    }
    
    return brand;
}

+(NSString*)imageNameForCardType:(CreditCardBrand)brand {
    
    NSString *name = nil;
    switch (brand) {
        case CreditCardBrandAmex:
            name = @"AMEXListIcon";
            break;
        case CreditCardBrandMAESTRO:
            name = @"MaestroListIcon";
            break;
        case CreditCardBrandMasterCard:
            name = @"MasterCardListIcon";
            break;
        case CreditCardBrandVisa:
            name = @"VISAListIcon";
            break;
        case CreditCardBrandDiscover:
            name = @"DISCOVERListIcon";
            break;
        case CreditCardBrandDinersClub:
            name = @"DINERCLUBListIcon";
            break;
        case CreditCardBrandJCB:
            name = @"JCBListIcon";
            break;
        default:
            name = @"CCVCardFront";
            break;
    }
    
    return name;
}

+(NSString*)iconForCardType:(CreditCardBrand)brand {
    
    NSString *name = nil;
    switch (brand) {
        case CreditCardBrandMAESTRO:
            name = @"MaestroCardLogo";
            break;
        case CreditCardBrandMasterCard:
            name = @"MasterCardLogo";
            break;
        case CreditCardBrandVisa:
            name = @"VisaLogo";
            break;
        default:
            name = @"CCVCardFront";
            break;
    }
    
    return name;
}

@end
