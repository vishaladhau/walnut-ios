//
//  NSString+WLT.m
//  walnut
//
//  Created by Abhinav Singh on 12/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "NSString+WLT.h"

NSString *const DefaultCountryCode = @"+91";

@implementation NSString (WLT)

+(NSArray*)allCountryCodes {
    
    static __strong NSArray *codes = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"];
        NSData *data = [NSData dataWithContentsOfFile:path];
        NSArray *countries = [NSJSONSerialization JSONObjectWithData:data options:1 error:nil];
        
        codes = [countries valueForKeyPath:@"code"];
    });
    
    return codes;
}

+(NSCharacterSet*)notMobileNumberSet {
    
    static __strong NSCharacterSet *charSet = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        charSet = [[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet];
    });
    
    return charSet;
}

+(NSCharacterSet*)nonAlphaNumericCharecterSet {
    
    static __strong NSCharacterSet *charSet = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        charSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "] invertedSet];
    });
    
    return charSet;
}

+(NSString*)validCardNameForUser:(id<User>)user {
    
    NSString *uname = [user name];
    
    uname = [uname stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSArray *components = [uname componentsSeparatedByCharactersInSet:[NSString nonAlphaNumericCharecterSet]];
    components = [components filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self <> ''"]];
    NSString *newName = [components componentsJoinedByString:@""];
    if (!newName.length) {
        newName = [[user completeNumber] onlyMobileNumber];
    }
    
    return newName;
}

-(BOOL)isValidIndianMobile {
    
    BOOL valid = NO;
    NSString *string = self;
    if ( string.length >= IndianMobileLength ) {
        
        NSRange otherCharsRange = [string rangeOfCharacterFromSet:[NSString notMobileNumberSet]];
        if (otherCharsRange.location == NSNotFound) {
            
            if ([string rangeOfString:@"+91"].location == 0) {
                
                string = [string stringByReplacingOccurrencesOfString:@"+91" withString:@""];
            }
            
            NSRange plusChar = [string rangeOfString:@"+"];
            if (plusChar.location == NSNotFound) {
                
                string = [NSString stringWithFormat:@"%@", @(string.integerValue)];
                if (string.length == IndianMobileLength) {
                    valid = YES;
                }
            }
        }
    }
    
    return valid;
}

-(NSString*)stringByRemovingExtraSpacesAndNewLine {
    
    NSString *string = self;
    string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSArray *components = [string componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    components = [components filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self <> ''"]];
    
    return [components componentsJoinedByString:@" "];
}

-(NSString*)mobileNumberWithCountryCode {

    NSString* phone = self;
    NSString *completeString = nil;
    if (phone.length) {
        while (1) {
            
            NSRange toRemoveRange = [phone rangeOfCharacterFromSet:[NSString notMobileNumberSet]];
            if (toRemoveRange.location != NSNotFound) {
                phone = [phone stringByReplacingCharactersInRange:toRemoveRange withString:@""];
            }else {
                break;
            }
        }
        
        NSString *cCode = nil;
        NSString *mNum = nil;
        
        if ([phone rangeOfString:@"+"].location == 0) {
            
            NSArray *allCodes = [NSString allCountryCodes];
            
            for ( NSString *contryCode in allCodes ) {
                
                NSString *code = [NSString stringWithFormat:@"+%@", [contryCode stringByReplacingOccurrencesOfString:@" " withString:@""]];
                NSRange rangeCode = [phone rangeOfString:code];
                if ( rangeCode.location == 0 ) {
                    
                    cCode = code;
                    mNum = [phone substringFromIndex:rangeCode.length];
                    break;
                }
            }
        }else  {
            
            if ([phone rangeOfString:@"0"].location == 0) {
                phone = [phone stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
            }
            
            mNum = phone;
            cCode = DefaultCountryCode;
        }
        
        if (mNum.length) {
            
            if (!cCode.length) {
                cCode = DefaultCountryCode;
            }
        }
        
        completeString = [NSString stringWithFormat:@"%@%@", cCode, mNum];
    }
    
    return completeString;
}

-(NSString*)onlyMobileNumber {
    
    NSString *mobNum = self;
    if (mobNum.length > IndianMobileLength) {
        mobNum = [mobNum substringFromIndex:(mobNum.length-IndianMobileLength)];
    }
    
    return mobNum;
}

-(NSString*)shortForm{
    
    NSString *sForm = nil;
    NSString *finalStr = [self stringByRemovingExtraSpacesAndNewLine];
    
    NSArray *arrN = [finalStr componentsSeparatedByString:@" "];
    if (arrN.count > 0) {
        if (arrN.count == 1) {
            NSString *str = arrN[0];
            if (str.length) {
                sForm = [str substringToIndex:1];
            }
        }
        else {
            
            NSString *str = arrN[0];
            NSString *strTwo = arrN[1];
            
            if (str.length && strTwo.length) {
                sForm = [NSString stringWithFormat:@"%@%@",[str substringToIndex:1], [strTwo substringToIndex:1]];
            }
        }
    }
    
    return [sForm uppercaseString];
}

-(NSString*)firstName {
    
    NSString *fForm = nil;
    NSArray *arrN = [self componentsSeparatedByString:@" "];
    if (arrN.count > 0) {
        fForm = [arrN firstObject];
    }
    
    return fForm;
}

-(BOOL)isValidateEmailAddress{
    
    // For some obscure reason, escaping the % in the directly in the format string (using %%) does not work and crashes at runtime! But creating the
    // regex outside and inser.ting it into the control string works. Are control strings in Objective-C really standard?
    // Not perfect, but should suit 98% of the e-mail addresses
    
    static NSPredicate *emailPredicate = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    });
    
    return [emailPredicate evaluateWithObject:self];
}

-(NSMutableAttributedString*)attributtedStringAddingPrivateIconOfHeight:(CGFloat)size {
    
    NSString *imageName = @"PrivateIconMedium";
    if (size > 25) {
        imageName = @"PrivateIconBig";
    }
    else if (size <= 10) {
        imageName = @"PrivateIconSmall";
    }
    
    NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:self];
    
    NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
    UIImage *image = [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    textAttachment.image = image;
    textAttachment.bounds = CGRectMake( 0, 0, image.size.width, image.size.height );
    
    NSAttributedString *attributedAtt = [NSAttributedString attributedStringWithAttachment:textAttachment];
    [attributed appendAttributedString:attributedAtt];
    
    return attributed;
}

@end
