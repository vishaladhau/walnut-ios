//
//  UIViewController+Helper.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 03/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "UIViewController+Helper.h"
#import "WLTTransactionUser.h"
#import "WLTMakePaymentListViewController.h"
#import "WLTInstrumentsManager.h"
#import "WLTViewController.h"
#import "WLTPaymentConfig.h"
#import "WLTDatabaseManager.h"
#import "WLTRootViewController.h"
#import "WLTHomeViewController.h"
#import "WLTSettingsViewController.h"
#import "WLTCombinedGroupPaymentsViewController.h"
#import "WLTTransactionUser.h"
#import "WLTCombinedGroupPayments.h"
#import "WLTReminderTextViewController.h"
#import "WLTPartialPayment.h"
#import "WLTPartialSettle.h"
#import "WLTGroupPaymentAmountEditViewController.h"
#import "WLTPaymentTransaction.h"
#import "WLTPaymentTransactionDetailViewController.h"
#import "WLTCardsListViewController.h"
#import "WLTPaymentTransationsManager.h"
#import "WLTFlowConstants.h"
#import "WLTGroupsManager.h"
#import "WLTPaymentUser.h"
#import "WLTRequestPaymentDetailViewController.h"
#import "WLTSendMoneyRequestViewController.h"
#import "WLTFirstPaymentAlertView.h"

NSString *const EditCellIdentifierPay = @"pay_user";

NSString *const EditCellIdentifierSettle = @"settle";
NSString *const EditCellIdentifierCall = @"call_user";
NSString *const EditCellIdentifierRemind = @"remind_user";

NSString *const EditCellIdentifierRemoveMember = @"remove_user";

@implementation UIViewController (Helper)

+(instancetype)initWithDefaultXIB {
    
    UIViewController *controller = [[[self class] alloc] initWithNibName:NSStringFromClass([self class]) bundle:nil];
    return controller;
}

-(id)controllerWithIdentifier:(NSString*)identifier {
    return [self.storyboard instantiateViewControllerWithIdentifier:identifier];
}

- (void)addController:(UIViewController*)toController
            replacing:(UIViewController*)fromController
     transactionStyle:(TransactionDirection)trascation {
    
    if (!fromController) {
        trascation = TransactionDirectionNone;
    }
    
    toController.view.autoresizingMask = (UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight);
    
    if (trascation == TransactionDirectionNone) {
        
        toController.view.frame = self.view.bounds;
        [self.view addSubview:toController.view];
        [self addChildViewController:toController];
        
        [toController didMoveToParentViewController:self];
        [toController setNeedsStatusBarAppearanceUpdate];
        
        if (fromController) {
            
            [fromController.view removeFromSuperview];
            [fromController removeFromParentViewController];
        }
    }
    else if (trascation == TransactionDirectionTop) {
        
        toController.view.frame = self.view.bounds;
        toController.view.originY -= self.view.height;
        [self.view addSubview:toController.view];
        
        [self addChildViewController:toController];
        
        [UIView animateWithDuration:0.3 animations:^{
            
            fromController.view.originY = self.view.height;
            toController.view.originY = 0;
            
            [toController setNeedsStatusBarAppearanceUpdate];
        } completion:^(BOOL finished) {
            
            [toController didMoveToParentViewController:self];
            
            [fromController.view removeFromSuperview];
            [fromController removeFromParentViewController];
        }];
    }
}

-(UIBarButtonItem*)addBackBarButtonWithSelector:(SEL)selector {
    
    UIBarButtonItem *added = nil;
    if ([self.navigationController.rootViewController isEqual:self]) {
        
        added = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone
                                                target:self action:selector];
    }else {
        
        added = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton"] style:UIBarButtonItemStyleDone
                                                target:self action:selector];
    }
    self.navigationItem.leftBarButtonItem = added;
    
    return added;
}

-(UIBarButtonItem*)addImagesBackBarButtonWithSelector:(SEL)selector {
    
    UIBarButtonItem *added = nil;
    if ([self.navigationController.rootViewController isEqual:self]) {
        
        added = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"CloseButton"] style:UIBarButtonItemStyleDone
                                                target:self action:selector];
    }else {
        
        added = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton"] style:UIBarButtonItemStyleDone
                                                target:self action:selector];
    }
    
    self.navigationItem.leftBarButtonItem = added;
    return added;
}

#pragma mark - Payments

-(void)showMakePaymentScreenForTransactionID:(NSString*)transID {
    
    if ([self isKindOfClass:[WLTViewController class]]) {
        
        __weak WLTViewController *weakSelf = (WLTViewController*)self;
        
        void (^ showControllerForTransaction)(WLTPaymentTransaction *transaction) = ^void (WLTPaymentTransaction *transaction){
            
            WLTSendMoneyRequestViewController *controller = [weakSelf controllerWithIdentifier:@"WLTSendMoneyRequestViewController"];
            controller.navBarColor = weakSelf.navBarColor;
            controller.transaction = transaction;
            controller.transactionType = UserTransactionStateOwe;
            [weakSelf.navigationController pushViewController:controller animated:YES];
            
            [controller setCompletion:^(BOOL success){
                
                if (success) {
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }
            }];
        };
        
        [weakSelf startLoading];
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"transactionId == %@", transID];
        [[WLTPaymentTransationsManager sharedManager] coreDataPaymentTransactionForPredicate:pred completion:^(WLTPaymentTransaction *object, NSError *error) {
            
            if (object) {
                
                if ([WLTFirstPaymentAlertView canShow]) {
                    
                    WLTFirstPaymentAlertView *alert = [WLTFirstPaymentAlertView initWithDefaultXib];
                    [alert setProceedBlock:^(BOOL success){
                        
                        if (success) {
                            
                            showControllerForTransaction(object);
                        }
                    }];
                    [alert show];
                }else {
                    
                    showControllerForTransaction(object);
                }
            }else {
                
                [weakSelf showAlertForError:error];
            }
            
            [weakSelf endViewLoading];
        }];
    }
}

-(void)showDetailsOfPaymentTransactionID:(NSString*)transactionID {
    
    if ([self isKindOfClass:[WLTViewController class]]) {
        
        __weak WLTViewController *weakSelf = (WLTViewController*)self;
        
        [weakSelf startLoading];
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"transactionId == %@", transactionID];
        [[WLTPaymentTransationsManager sharedManager] coreDataPaymentTransactionForPredicate:pred completion:^(WLTPaymentTransaction *object, NSError *error) {
            
            if (object) {
                
                if ( (object.type == PaymentTypePush) || (object.type == PaymentTypePull)) {
                    
                    WLTPaymentTransactionDetailViewController *topCont = nil;
                    if ([weakSelf.navigationController.topViewController isKindOfClass:[WLTPaymentTransactionDetailViewController class]]) {
                        
                        topCont = (WLTPaymentTransactionDetailViewController *)weakSelf.navigationController.topViewController;
                        if (![topCont.transaction.transactionId isEqualToString:transactionID]) {
                            topCont = nil;
                        }
                    }
                    
                    if (!topCont) {
                        
                        WLTPaymentTransactionDetailViewController *controller = [weakSelf controllerWithIdentifier:@"WLTPaymentTransactionDetailViewController"];
                        controller.navBarColor = weakSelf.navBarColor;
                        controller.transaction = object;
                        [weakSelf.navigationController pushViewController:controller animated:YES];
                    }else {
                        [topCont refreshCurrentTransaction];
                    }
                }
                else if ( (object.type == PaymentTypeRequestToMe) || (object.type == PaymentTypeRequestFromMe)) {
                    
                    if (SendReceivePaymentsEnabled()) {
                        
                        WLTRequestPaymentDetailViewController *topCont = nil;
                        if ([weakSelf.navigationController.topViewController isKindOfClass:[WLTRequestPaymentDetailViewController class]]) {
                            
                            topCont = (WLTRequestPaymentDetailViewController *)weakSelf.navigationController.topViewController;
                            if (![topCont.transaction.transactionId isEqualToString:transactionID]) {
                                topCont = nil;
                            }
                        }
                        
                        if (!topCont) {
                            
                            WLTRequestPaymentDetailViewController *controller = [weakSelf controllerWithIdentifier:@"WLTRequestPaymentDetailViewController"];
                            controller.navBarColor = weakSelf.navBarColor;
                            controller.transaction = object;
                            [weakSelf.navigationController pushViewController:controller animated:YES];
                        }else {
                            
                            [topCont refreshCurrentTransaction];
                        }
                    }
                }
            }else {
                
                [weakSelf showAlertForError:error];
            }
            
            [weakSelf endViewLoading];
        }];
    }
}

-(void)showDetailsOfTransactionID:(NSString*)transactionID {
    
    if ([self isKindOfClass:[WLTViewController class]]) {
        
        __weak WLTViewController *weakSelf = (WLTViewController*)self;
        
        [weakSelf startLoading];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY groups.splitTransactionID == %@", transactionID];
        [[WLTPaymentTransationsManager sharedManager] coreDataPaymentTransactionForPredicate:predicate completion:^(WLTPaymentTransaction *object, NSError *error) {
            
            if(object) {
                
                WLTPaymentTransactionDetailViewController *controller = [self controllerWithIdentifier:@"WLTPaymentTransactionDetailViewController"];
                controller.navBarColor = weakSelf.navBarColor;
                controller.transaction = object;
                
                [weakSelf.navigationController pushViewController:controller animated:YES];
            }
            
            [weakSelf endViewLoading];
        }];
    }
}

-(void)showAddCardForTransactionID:(NSString*)transactionID {
    
    if ([self isKindOfClass:[WLTViewController class]]) {
        
        WLTCardsListViewController *instruments = [self controllerWithIdentifier:@"WLTCardsListViewController"];
        instruments.flowType = WLTEntryFlowTypeReceive;
        instruments.showReceiveAlert = YES;
        instruments.transactionIDToShow = transactionID;
        instruments.navBarColor = nil;
        [self.navigationController pushViewController:instruments animated:YES];
    }
}

-(void)showPaymentScreenForTransaction:(WLTCombinedGroupPayments*)comGrp message:(NSString*)message receiverMessage:(NSString*)recMessage andOldTrans:(NSString*)oldTranId withCompletion:(SuccessBlock)block{
    
    if ([self isKindOfClass:[WLTViewController class]]) {
        
        __weak WLTViewController *weakSelf = (WLTViewController*)self;
        
        void (^ showPaymentScreen)() = ^void (){
            
            [weakSelf startLoadingWithColor:[UIColor appleGreenColor]];
            
            [self.rootController.homeController.settingsController fetchWhatsNewWithCompletion:^(id data) {
                
                [weakSelf endViewLoading];
                
                if ( [data isKindOfClass:[NSError class]]) {
                    
                    [weakSelf showAlertForError:data];
                    if (block) {
                        block(NO);
                    }
                }
                else {
                    
                    if (!PaymentsEnabled()) {
                        //This won't happen because we don't any button which shows payment controller in case payments are disabled.
                        //But lets keep it here.
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Payments Unavaliable!" message:@"Payments are not enabled for this account"
                                                                       delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                        [alert show];
                        
                        if (block) {
                            block(NO);
                        }
                    }
                    else if([WLTDatabaseManager sharedManager].paymentConfig.isSuspended.boolValue) {
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Payments Unavaliable!" message:[WLTDatabaseManager sharedManager].paymentConfig.suspensionMessage
                                                                       delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                        [alert show];
                        
                        if (block) {
                            block(NO);
                        }
                    }
                    else {
                        
                        WLTPartialPayment *payGrp = [[WLTPartialPayment alloc] initWithCombinedGroup:comGrp];
                        
                        WLTMakePaymentListViewController *payment = [weakSelf controllerWithIdentifier:@"WLTMakePaymentListViewController"];
                        payment.userMessage = message;
                        payment.previousTransactionId = oldTranId;
                        payment.transaction = payGrp;
                        payment.receiverMessage = recMessage;
                        payment.navBarColor = [UIColor appleGreenColor];
                        
                        UINavigationController *navCont = [[UINavigationController alloc] initWithRootViewController:payment];
                        [navCont applyDefaultStyle];
                        [weakSelf presentViewController:navCont animated:YES completion:nil];
                        
                        __weak UINavigationController *weakNav = navCont;
                        
                        [payment setCompletion:^(BOOL success, GTLWalnutMPaymentTransaction *trans, GTLWalnutMPaymentInstrument *usedInstrument, BOOL newInstrument){
                            
                            void (^ dismissControllerWithSuccess)(BOOL paySuccess) = ^void (BOOL paySuccess){
                                
                                UIView *superV = [[[UIApplication sharedApplication] delegate] window];
                                
                                UIImage *image = [weakNav.view snapShotWithoutAddSubview];
                                UIImageView *imgView = [[UIImageView alloc] initWithFrame:superV.bounds];
                                imgView.image = image;
                                [superV addSubview:imgView];
                                
                                [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                                    
                                    imgView.originY = superV.height;
                                } completion:^(BOOL finished) {
                                    
                                    [imgView removeFromSuperview];
                                }];
                                
                                [weakSelf dismissViewControllerAnimated:NO completion:^{
                                    
                                    if (block) {
                                        block(paySuccess);
                                    }
                                }];
                            };
                            
                            if (success) {
                                
                                NSArray *array = nil;
                                if (trans) {
                                    array = [[WLTPaymentTransationsManager sharedManager] updateCoreDataTransactionFromGTLObjects:@[trans]];
                                }
                                
                                if (array.count) {
                                    
                                    if (oldTranId.length) {
                                        
                                        NSPredicate *pred = [NSPredicate predicateWithFormat:@"transactionId CONTAINS[cd] %@", oldTranId];
                                        NSArray *allTrans = [[WLTDatabaseManager sharedManager] allObjectsOfClass:@"WLTPaymentTransaction" withPredicate:pred];
                                        
                                        WLTPaymentTransaction *toRemove = nil;
                                        for ( WLTPaymentTransaction *trans in allTrans ) {
                                            if ([trans.transactionId rangeOfString:@"requested"].location != NSNotFound) {
                                                toRemove = trans;
                                                break;
                                            }
                                        }
                                        
                                        if (toRemove) {
                                            [toRemove.managedObjectContext deleteObject:toRemove];
                                            [[WLTDatabaseManager sharedManager] saveDataBase];
                                        }
                                    }
                                    
                                    [[NSNotificationCenter defaultCenter] postNotificationName:PaymentTransactionsChanged object:nil];
                                    
                                    WLTPaymentTransactionDetailViewController *controller = [self controllerWithIdentifier:@"WLTPaymentTransactionDetailViewController"];
                                    controller.navBarColor = [UIColor appleGreenColor];
                                    controller.transaction = [array firstObject];
                                    [weakNav setViewControllers:@[controller] animated:YES];
                                    
                                    if (newInstrument) {
                                        [[WLTInstrumentsManager sharedManager] showNewDefaultPopUpForIntrument:usedInstrument flowType:WLTEntryFlowTypePay];
                                    }
                                    if (!usedInstrument.sendDefault.boolValue) {
                                        
                                        usedInstrument.sendDefault = @(YES);
                                        [[WLTInstrumentsManager sharedManager] addInstrumentToWalnut:usedInstrument withCompletion:nil];
                                        NSString *instrumentID = usedInstrument.instrumentUuid;
                                        
                                        [[WLTInstrumentsManager sharedManager] validCardsForTransactionType:PaymentInstrumentTypePay invalidateCache:NO withCompletion:^(id data) {
                                            
                                            if ([data isKindOfClass:[NSArray class]]) {
                                                
                                                for (GTLWalnutMPaymentInstrument *instrument in data) {
                                                    
                                                    if (instrument.sendDefault.boolValue && ![instrument.instrumentUuid isEqualToString:instrumentID]) {
                                                        
                                                        instrument.sendDefault = @NO;
                                                        [[WLTInstrumentsManager sharedManager] addInstrumentToWalnut:instrument withCompletion:nil];
                                                    }
                                                }
                                            }
                                        }];
                                    }
                                    
                                    [controller setCompletion:^{
                                        
                                        dismissControllerWithSuccess(YES);
                                    }];
                                }
                                else {
                                    
                                    dismissControllerWithSuccess(YES);
                                }
                            }
                            else {
                                
                                dismissControllerWithSuccess(NO);
                            }
                        }];
                    }
                }
            }];
        };
        
        if ([WLTFirstPaymentAlertView canShow]) {
            
            WLTFirstPaymentAlertView *alert = [WLTFirstPaymentAlertView initWithDefaultXib];
            [alert setProceedBlock:^(BOOL success){
                
                if (success) {
                    
                    showPaymentScreen();
                }else {
                    if (block) {
                        block(NO);
                    }
                }
            }];
            
            [alert show];
        }else {
            
            showPaymentScreen();
        }
    }
}

#pragma mark - You Owe/Get Screens

-(void)showCombinedTransactionsForUser:(id<User>)user privacyStatus:(WLTCombinedGroupStatus)status{
    
    WLTCombinedGroupPaymentsViewController *combinedGet = [self controllerWithIdentifier:@"WLTCombinedGroupPaymentsViewController"];
    [combinedGet showDataForUser:user privacyStatus:status];
    [self.navigationController pushViewController:combinedGet animated:YES];
    
    __weak typeof(self) weakSelf = self;
    [combinedGet setCompletion:^(BOOL success){
        
        if (success) {
            [weakSelf.navigationController popToRootViewControllerAnimated:YES];
        }
    }];
}

-(void)showCombinedTransactionsForGroups:(NSArray*)grps andState:(UserTransactionState)state{
    
    if (grps.count >= 1) {
        
        WLTCombinedGroupPaymentsViewController *combinedGet = [self controllerWithIdentifier:@"WLTCombinedGroupPaymentsViewController"];
        [combinedGet showDataOfGroups:grps forState:state];
        [self.navigationController pushViewController:combinedGet animated:YES];
        
        __weak typeof(self) weakSelf = self;
        [combinedGet setCompletion:^(BOOL success){
            
            if (success) {
                [weakSelf.navigationController popToRootViewControllerAnimated:YES];
            }
        }];
    }
}

#pragma mark - Other Payment Actions

-(void)openiTunesURL {
    
    NSString *appID = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppleStoreId"];
    
    NSString *iTunesLink = [NSString stringWithFormat:@"itms://itunes.apple.com/us/app/apple-store/id%@?mt=8", appID];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}

-(void)callToNumber:(NSString*)number {
	
	NSURL *callUrl = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", number]];
	
	if ([[UIApplication sharedApplication] canOpenURL:callUrl]) {
		
		[[UIApplication sharedApplication] openURL:callUrl];
	}else {
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!"
														message:@"We are unable to make call from this device."
													   delegate:nil
											  cancelButtonTitle:@"Ok"
											  otherButtonTitles:nil, nil];
		[alert show];
	}
}

-(void)showReminderScreenForPayment:(WLTCombinedGroupPayments*)pay {
	
	WLTReminderTextViewController *remind = [self controllerWithIdentifier:@"WLTReminderTextViewController"];
	remind.transaction = pay;
	UINavigationController *navCont = [[UINavigationController alloc] initWithRootViewController:remind];
	[navCont applyTranslusentStyle];
	[self presentViewController:navCont animated:YES completion:nil];
	
	__weak typeof(self) weakSelf = self;
	[remind setCompletionBlock:^(BOOL success){
		
		[weakSelf dismissViewControllerAnimated:YES completion:nil];
	}];
}

#pragma mark - Settle

-(void)settleForCombinedTransaction:(WLTCombinedGroupPayments*)comPay completion:(SuccessBlock)completion {
    
    WLTPartialSettle *payGrp = [[WLTPartialSettle alloc] initWithCombinedGroup:comPay];
    
    WLTGroupPaymentAmountEditViewController *editCont = [self controllerWithIdentifier:@"WLTGroupPaymentAmountEditViewController"];
    editCont.settling = YES;
    editCont.navBarColor = [UIColor appleGreenColor];
    editCont.transaction = payGrp;
    
    __weak WLTGroupPaymentAmountEditViewController *weakEdit = editCont;
    __weak WLTViewController *weakSelf = (WLTViewController*)self;
    
    [editCont setCompletion:^(WLTPartial *pay){
        
        if (pay) {
            
            [weakEdit startLoading];
            
            [weakSelf settleForPartialSettle:(WLTPartialSettle*)pay withCompletion:^(BOOL success) {
                
                if (success) {
                    
                    if (completion) {
                        completion(YES);
                    }
                }
                
                [weakEdit endViewLoading];
            }];
        }
    }];
    
    [self.navigationController pushViewController:editCont animated:YES];
}

-(void)settleForPartialSettle:(WLTPartialSettle*)settle withCompletion:(SuccessBlock)completion{
    
    id <User> toGetUser = [WLTDatabaseManager sharedManager].currentUser;
    
    GTLWalnutMSplitUser *otherUser = [GTLWalnutMSplitUser new];
    otherUser.mobileNumber = [settle.user completeNumber];
    otherUser.name = [settle.user name];
    
    GTLWalnutMSplitUser *appUser = [GTLWalnutMSplitUser new];
    appUser.mobileNumber = [toGetUser completeNumber];
    appUser.name = [toGetUser name];
    
    GTLWalnutMMultiSettle *settleCom = [[GTLWalnutMMultiSettle alloc] init];
    settleCom.settleAmount = @(settle.currentAmount).twoPlaceDecimalNumber;
    settleCom.settleFrom = otherUser;
    settleCom.settleTo = appUser;
    
    NSMutableArray *allSettles = [NSMutableArray new];
    
    for ( WLTPartialGroup *grpData in settle.payGroups ) {
        
        GTLWalnutMSettle *settle = [[GTLWalnutMSettle alloc] init];
        settle.settleFrom = appUser;
        settle.settleTo = otherUser;
        settle.amount = @(grpData.acctualAmount).twoPlaceDecimalNumber;
        settle.groupUuid = grpData.group.groupUUID;
        
        [allSettles addObject:settle];
    }
    
    for ( WLTPartialGroup *grpData in settle.getGroups ) {
        
        if (grpData.currentAmount > 0) {
            
            GTLWalnutMSettle *settle = [[GTLWalnutMSettle alloc] init];
            settle.settleFrom = otherUser;
            settle.settleTo = appUser;
            settle.amount = @(grpData.currentAmount).twoPlaceDecimalNumber;
            settle.groupUuid = grpData.group.groupUUID;
            
            [allSettles addObject:settle];
        }
    }
    
    NSNumber *timeStamp = @([[NSDate date] timeStamp]);
    for ( GTLWalnutMSettle *settleGrp in allSettles ) {
        
        settleGrp.txnDate = timeStamp;
        settleGrp.txnUuid = [WLTNetworkManager generateUniqueIdentifier];
        settleGrp.objUuid = [WLTNetworkManager generateUniqueIdentifier];
    }
    
    settleCom.groupSettle = allSettles;
    
    GTLQueryWalnut *settleCall = [GTLQueryWalnut queryForSplitSettleMultiWithObject:settleCom];
    
    __weak typeof(self) weakSelf = self;
    
    [[WLTNetworkManager sharedManager].walnutService executeQuery:settleCall completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
        
        if (!error) {
            
            NSMutableArray *changedGrps = [NSMutableArray new];
            for ( GTLWalnutMSettle *settle in settleCom.groupSettle ) {
                [changedGrps addObject:settle.groupUuid];
            }
            
            [weakSelf trackEvent:GA_ACTION_SETTLEMENT_ADDED andValue:@(settle.currentAmount)];
            
            [[WLTGroupsManager sharedManager] fetchEveryChangeOfGroupsIDS:changedGrps completion:^(NSArray *grpArr, NSError *error) {
                
                for ( WLTGroup *grpChanged in grpArr ) {
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:NotifyGroupChanged object:grpChanged];
                }
                
                if (completion) {
                    completion(YES);
                }
            }];
        }
        else {
            
            [weakSelf showAlertForError:error];
            if (completion) {
                completion(NO);
            }
        }
    }];
}

#pragma mark - Support Email/SMS

-(void)sendText:(NSString*)text toNumber:(NSString*)number {
    
    if([MFMessageComposeViewController canSendText]) {
        
        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
        messageController.messageComposeDelegate = self;
        [messageController setRecipients:@[number]];
        [messageController setBody:text];
        
        [self presentViewController:messageController animated:YES completion:nil];
    }
    else {
        
        NSError *error = [NSError walnutErrorWithMessage:@"We are unable to send Text from this device."];
        [self showAlertForError:error];
    }
}

-(void)sendSupportEmailForTransaction:(WLTPaymentTransaction*)transaction{
    
    if ([MFMailComposeViewController canSendMail]) {
        
        WLTDatabaseManager *dbManager = [WLTDatabaseManager sharedManager];
        UIDevice *device = [UIDevice currentDevice];
        
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [[controller navigationBar] setTintColor: [UIColor rhinoColor]];
        
        [controller setToRecipients:@[@"help@getwalnut.com"]];
        
        NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        NSString *build = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey];
        
        NSString *deviceName = [device localizedModel];
        
        NSString *messageBody = nil;
        NSString *subjectQuery = nil;
        
        messageBody = [NSString stringWithFormat:@"[<b>App support email from :</b>%@<br>\
                       <b>App version:</b> v%@ Build %@ (%@)<br>\
                       <b>OS version:</b> %@<br>\
                       <b>Device name:</b> %@<br>", dbManager.currentUser.email, version, build, AppVersionString,
                       [device systemVersion], deviceName];
        
        if (transaction) {
            
            NSString *amount = transaction.amount.displayString;
            
            NSString *paymentTxnID = transaction.paynimoTransactionId;
            NSString *paymentRefID = transaction.paynimoReferenceId;
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
            [formatter setDateFormat:@"MM-dd-yy hh:mm:ss a"];
            
            NSString *dateString = [formatter stringFromDate:transaction.initiationDate];
            
            messageBody = [NSString stringWithFormat:@"%@\
                              <b>Device ID:</b> %@<br>\
                              <b>Txn UUID:</b> %@<br>\
                              <b>Payment Txn ID:</b> %@<br>\
                              <b>Payment Txn RefId:</b> %@<br>\
                              <b>Amount:</b> %@<br>\
                              <b>Payment Initiation Date :</b> %@]",messageBody, transaction.deviceUuid,
                              transaction.transactionId, paymentTxnID, paymentRefID, amount, dateString];
            
            subjectQuery = [NSString stringWithFormat:@"Payment query TXN_UUID: %@", transaction.transactionId];
        }else {
            
            messageBody = [NSString stringWithFormat:@"%@<b>Device ID:</b> %@<br>]",messageBody, dbManager.appSettings.deviceIdentifier];
        }
        
        if (subjectQuery.length) {
            [controller setSubject:subjectQuery];
        }
        [controller setMessageBody:messageBody isHTML:YES];
        
        if (controller){
            
            [self presentViewController:controller animated:YES completion:nil];
        }
    }
    else {
        
        NSError *error = [NSError walnutErrorWithMessage:@"We are unable to send email from this device.\nPlease make sure you have added an email account to your device settings."];
        [self showAlertForError:error];
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
