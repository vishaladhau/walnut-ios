//
//  NSNumber+WLT.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 04/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "NSNumber+WLT.h"

NSString *const NilAmountPlaceholder = @"₹0.00";

@implementation NSNumber (WLT)

NSNumberFormatter* DefaultNumberFormatter () {
    
    static __strong NSNumberFormatter *numberFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [numberFormatter setAllowsFloats:NO];
        [numberFormatter setMaximumFractionDigits:0];
    });
    
    return numberFormatter;
}

NSNumberFormatter* DefaultCurrencyFormatter () {
    
    static __strong NSNumberFormatter *currencyFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        currencyFormatter = [[NSNumberFormatter alloc] init];
        [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [currencyFormatter setCurrencySymbol:@"₹"];
        [currencyFormatter setAllowsFloats:YES];
        [currencyFormatter setMaximumFractionDigits:2];
        [currencyFormatter setGeneratesDecimalNumbers:NO];
    });
    
    return currencyFormatter;
}

NSNumberFormatter* DefaultCurrencyRoundedFormatter () {
    
    static __strong NSNumberFormatter *currencyFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        currencyFormatter = [[NSNumberFormatter alloc] init];
        [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [currencyFormatter setCurrencySymbol:@"₹"];
        [currencyFormatter setAllowsFloats:NO];
        [currencyFormatter setMaximumFractionDigits:0];
    });
    
    return currencyFormatter;
}

NSNumberFormatter* DefaultRoundedFormatter () {
    
    static __strong NSNumberFormatter *currencyFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        currencyFormatter = [[NSNumberFormatter alloc] init];
        [currencyFormatter setAllowsFloats:YES];
        [currencyFormatter setMaximumFractionDigits:2];
        [currencyFormatter setMinimumIntegerDigits:1];
    });
    
    return currencyFormatter;
}

-(NSString*)displayString {
    NSNumberFormatter *numberFormatter = DefaultNumberFormatter();
    return [numberFormatter stringFromNumber:self];
}

-(NSString*)displayCurrencyString {
    
    NSNumberFormatter *currencyFormatter = DefaultRoundedFormatter();
    NSString *string = [currencyFormatter stringFromNumber:self];
    return [@"₹" stringByAppendingString:string];
}

-(NSString*)displayRoundedCurrencyString {
    
    NSNumberFormatter *currencyFormatter = DefaultCurrencyRoundedFormatter();
    return [currencyFormatter stringFromNumber:self];
}

-(NSString*)displayRoundedString {
    
    NSNumberFormatter *currencyFormatter = DefaultRoundedFormatter();
    return [currencyFormatter stringFromNumber:self];
}

-(NSString*)fixedCurrencyString {
    
    NSNumberFormatter *currencyFormatter = DefaultCurrencyFormatter();
    return [currencyFormatter stringFromNumber:self];
}

-(NSNumber*)twoPlaceDecimalNumber {
    
    return [DefaultRoundedFormatter() numberFromString:[NSString stringWithFormat:@"%0.2f", self.floatValue]];
}

@end
