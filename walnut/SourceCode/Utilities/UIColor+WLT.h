//
//  UIColor+WLT.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 02/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (WLT)

+(UIColor*)appDefaultTintColor;

+(UIColor*)darkGrayTextColor;
+(UIColor*)mediumGrayTextColor;
+(UIColor*)lightGrayTextColor;
+(UIColor*)placeHolderTextColor;

+(UIColor*)darkBackgroundColor;
+(UIColor*)lightBackgroundColor;

//+(UIColor*)colorWithGray:(CGFloat)gray;
+(UIColor*)cadetGrayColor;
+(UIColor*)bermudaGray;

+(UIColor*)darkCellBackground;

+(UIColor*)fiordColor;
+(UIColor*)cadetColor;
+(UIColor*)lightSlateGrayColor;
+(UIColor*)lilyWhiteColor;
+(UIColor*)deepSkyBlueColor;

+(UIColor*)rhinoColor;

+(UIColor*)getColor;
+(UIColor*)oweColor;
+(UIColor*)denimColor;

+(UIColor*)catSkillWhiteColor;
+(UIColor*)appleGreenColor;
+(UIColor*)hokiColor;
+(UIColor*)charcoalColor;
+(UIColor*)aroundBlackColor;
+(UIColor*)dogerBlueColor;
+(UIColor*)chileanFireColor;

+(UIColor*)fullMoonColor;
+(UIColor*)athensGray;

+(UIColor*)limeadeColor;
+(UIColor*)flamePeaColor;
+(UIColor*)mediumCarmine;
+(UIColor*)pomegranateColor;
//+(UIColor*)mercuryColor;
+(UIColor*)concreteColor;
+(UIColor*)defaultCategoryColor;

+(UIColor*)bleuDeFranceColor;
+(UIColor*)darkElectricBlueColor;
+(UIColor*)sanJuanColor;

+ (UIColor *)walnutIconColor;

-(UIColor*)intermidiateColorFrom:(UIColor*)color withPercentage:(CGFloat)percent;

@end
