//
//  UIViewController+Helper.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 03/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

FOUNDATION_EXPORT NSString *const EditCellIdentifierPay;

FOUNDATION_EXPORT NSString *const EditCellIdentifierSettle;
FOUNDATION_EXPORT NSString *const EditCellIdentifierCall;
FOUNDATION_EXPORT NSString *const EditCellIdentifierRemind;

FOUNDATION_EXPORT NSString *const EditCellIdentifierRemoveMember;

@class WLTYouGetSettleViewController;
@class WLTTransactionUser;
@class WLTCombinedGroupPayments;
@class WLTDBMobile;
@class WLTGroup;
@class WLTPaymentTransaction;

@protocol User;

@import MessageUI;

typedef NS_ENUM(NSInteger, UserTransactionState) {
    
    UserTransactionStateUnknown = 0,
    UserTransactionStateNone = 0,
    UserTransactionStateGet,
    UserTransactionStateOwe,
};

typedef NS_ENUM(NSInteger, TransactionDirection) {
    
    TransactionDirectionNone = -1,
    TransactionDirectionTop = 0,
    TransactionDirectionBottom,
    TransactionDirectionLeft,
    TransactionDirectionRight
};

typedef NS_ENUM(NSInteger, WLTCombinedGroupStatus) {
    
    WLTCombinedGroupStatusPrivate = 0,
    WLTCombinedGroupStatusPublic,
    WLTCombinedGroupStatusBoth,
};

@interface UIViewController (Helper) <MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>

+(instancetype)initWithDefaultXIB;

-(void)sendText:(NSString*)text toNumber:(NSString*)number;
-(void)callToNumber:(NSString*)number;
-(void)openiTunesURL;

-(void)showReminderScreenForPayment:(WLTCombinedGroupPayments*)pay;

-(void)showDetailsOfPaymentTransactionID:(NSString*)transactionID;
-(void)showDetailsOfTransactionID:(NSString*)transactionID;
-(void)showAddCardForTransactionID:(NSString*)transactionID;

-(void)addController:(UIViewController*)toController
            replacing:(UIViewController*)fromController
     transactionStyle:(TransactionDirection)trascation;

-(UIBarButtonItem*)addBackBarButtonWithSelector:(SEL)selector;
-(UIBarButtonItem*)addImagesBackBarButtonWithSelector:(SEL)selector;

-(id)controllerWithIdentifier:(NSString*)identifier;

-(void)showCombinedTransactionsForUser:(id<User>)user privacyStatus:(WLTCombinedGroupStatus)status;

-(void)showCombinedTransactionsForGroups:(NSArray*)grps andState:(UserTransactionState)state;

-(void)showPaymentScreenForTransaction:(WLTCombinedGroupPayments*)comGrp message:(NSString*)message receiverMessage:(NSString*)recMessage andOldTrans:(NSString*)oldTranId withCompletion:(SuccessBlock)block;
-(void)showMakePaymentScreenForTransactionID:(NSString*)transID;

-(void)settleForCombinedTransaction:(WLTCombinedGroupPayments*)comPay completion:(SuccessBlock)completion;

-(void)sendSupportEmailForTransaction:(WLTPaymentTransaction*)transaction;

@end
