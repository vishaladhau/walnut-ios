//
//  NSString+Card.h
//  walnut
//
//  Created by Abhinav Singh on 04/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *CardNumberSeperator = @" ";
static NSInteger NumberGroupSize = 4;

static NSInteger MaxCardLengthAllowed = 16;

static NSInteger MaxCVVLengthAllowed = 4;
static NSInteger MinCVVLengthAllowed = 3;

typedef NS_ENUM(NSInteger, CreditCardBrand) {
    
    CreditCardBrandUnknown = 0,
    CreditCardBrandVisa,
    CreditCardBrandMasterCard,
    CreditCardBrandDinersClub,
    CreditCardBrandAmex,
    CreditCardBrandDiscover,
    CreditCardBrandMAESTRO,
    CreditCardBrandJCB,
    CreditCardBrandInvalid
};

@interface NSString (Card)

-(NSString*)creditCardNumber;
-(NSString*)formattedCreditCardNumber;
-(NSString*)cardNumberForLast4Digits;
-(CreditCardBrand)cardBrand;
-(BOOL)isValidCreditCard;

+(NSString*)emptyCardPlaceHolder;
+(NSString*)displayNameForCardType:(CreditCardBrand)brand;
+(CreditCardBrand)cardTypeForDisplayString:(NSString*)name;

+(NSString*)imageNameForCardType:(CreditCardBrand)brand;
+(NSString*)iconForCardType:(CreditCardBrand)brand;

@end
