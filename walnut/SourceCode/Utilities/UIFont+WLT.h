//
//  UIFont+WLT.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 02/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (WLT)

+(UIFont*)sfUITextMediumOfSize:(CGFloat)size;
+(UIFont*)sfUITextLightOfSize:(CGFloat)size;

+(UIFont*)sfUITextRegularOfSize:(CGFloat)size;
+(UIFont*)sfUITextRegularItallicOfSize:(CGFloat)size;

+(UIFont*)sfUITextBoldOfSize:(CGFloat)size;
+(UIFont*)sfUITextSemiboldOfSize:(CGFloat)size;
+(UIFont*)sfUITextHeavyOfSize:(CGFloat)size;

+(UIFont*)ocrFontOfSize:(CGFloat)size;

+(UIFont*)appTitleFont;
+(UIFont*)appSubTitleFont;

+(UIFont*)appBarButtonFont;

@end
