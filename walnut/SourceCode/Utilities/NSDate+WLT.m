//
//  NSDate+WLT.m
//  walnut
//
//  Created by Abhinav Singh on 26/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "NSDate+WLT.h"

@implementation NSDate (WLT)

NSDateFormatter* DefaultDateFormatter () {
    
    static __strong NSDateFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [formatter setDateFormat:@"MMM dd, h:mm a"];
    });
    
    return formatter;
}

NSDateFormatter* DateMonthFormatter () {
    
    static __strong NSDateFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [formatter setDateFormat:@"dd MMM"];
    });
    
    return formatter;
}

NSDateFormatter* TimeFormatter () {
    
    static __strong NSDateFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [formatter setDateFormat:@"h:mm a"];
    });
    
    return formatter;
}

+(long long)currentTimeStamp {
    return [[NSDate date] timeStamp];
}

-(long long)timeStamp {
    return (long long)([self timeIntervalSince1970]);
}

+(NSDate*)dateFromWalnutTimestamp:(long long)stamp {
    return [NSDate dateWithTimeIntervalSince1970:stamp];
}

-(NSString*)displayString {
    
//    return [self relativeDateString];
    
    NSInteger days = [self daysBetweenDate:[NSDate date]];
    if (days == 0) {
        
        return [NSString stringWithFormat:@"Today, %@", [TimeFormatter() stringFromDate:self]];
    }
    else if (days == -1) {
        
        return [NSString stringWithFormat:@"Yesterday, %@", [TimeFormatter() stringFromDate:self]];
    }
    else {
        return [DefaultDateFormatter() stringFromDate:self];
    }
}

-(NSString*)dateMonthDisplay {
    return [DateMonthFormatter() stringFromDate:self];
}

-(NSString *)relativeDateString {
    
    NSCalendarUnit units = (NSCalendarUnitSecond | NSCalendarUnitMinute | NSCalendarUnitHour | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear);;
	NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                   fromDate:self
																	 toDate:[NSDate date]
																	options:NSCalendarMatchLast];
    
	NSString *returned = nil;
	if (components.year >= 1) {

		if (components.year == 1){
			returned = @"a year ago";
		}else {
			returned = [NSString stringWithFormat:@"%ld years ago", (long)components.year];
		}
	}
	else if (components.month >= 1) {

		if (components.month == 1){
			returned = @"a month ago";
		}else {
			returned = [NSString stringWithFormat:@"%ld months ago", (long)components.month];
		}
	}
	else if (components.day >= 1){

		if (components.day == 1){
			returned = @"Yesterday";
		}
		else{
			returned = [NSString stringWithFormat:@"%ld days ago", (long)components.day];
		}
	}
	else if (components.hour > 0) {
		if (components.hour == 1){
			returned = [NSString stringWithFormat:@"%ld hour ago", (long)components.hour];
		}
		else {
			returned = [NSString stringWithFormat:@"%ld hours ago", (long)components.hour];
		}
	}
	else if (components.minute > 0) {
		if (components.minute == 1){
			returned = [NSString stringWithFormat:@"%ld min ago", (long)components.minute];
		}
		else {
			returned = [NSString stringWithFormat:@"%ld mins ago", (long)components.minute];
		}
	}
	else if (components.second > 0) {

		returned = [NSString stringWithFormat:@"%ld sec ago", (long)components.second];
	}
	else {
		returned = @"JUST NOW";
	}

	return returned;
}

-(NSInteger)daysBetweenDate:(NSDate*)fromDateTime{
    
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:self];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}
//
//-(NSInteger)gapForCalendarUnit:(NSCalendarUnit)unit fromDate:(NSDate*)date {
//    
//    NSDateComponents *components = [[NSCalendar currentCalendar] components:unit
//                                                                   fromDate:self
//                                                                     toDate:date
//                                                                    options:0];
//    NSInteger retValue = NSNotFound;
//    switch (unit) {
//        case NSCalendarUnitSecond:
//            retValue = components.second;
//            break;
//        case NSCalendarUnitMinute:
//            retValue = components.minute;
//            break;
//        case NSCalendarUnitHour:
//            retValue = components.hour;
//            break;
//        case NSCalendarUnitDay:
//            retValue = components.day;
//            break;
//        case NSCalendarUnitMonth:
//            retValue = components.month;
//            break;
//        case NSCalendarUnitYear:
//            retValue = components.year;
//            break;
//        default:
//            break;
//    }
//    
//    return retValue;
//}

-(NSDate*)midnightDate {
    
    NSCalendar *calender = [NSCalendar currentCalendar];
    
    NSUInteger components = (NSCalendarUnitYear| NSCalendarUnitMonth| NSCalendarUnitDay);
    return [calender dateFromComponents:[calender components:components fromDate:self]];
}

@end
