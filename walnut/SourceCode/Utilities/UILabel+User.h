//
//  UILabel+User.h
//  walnut
//
//  Created by Abhinav Singh on 07/03/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WLTGroup;

@interface UILabel (User)

-(void)displayUserNameOrNumberOfUser:(id<User>)user;
-(void)displayUserName:(id<User>)user;

-(void)displayGroupName:(WLTGroup *)group checkDirect:(BOOL)direct;

@end
