//
//  NSString+WLT.h
//  walnut
//
//  Created by Abhinav Singh on 12/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const DefaultCountryCode;

@interface NSString (WLT)

-(NSString*)stringByRemovingExtraSpacesAndNewLine;

-(NSString*)mobileNumberWithCountryCode;
-(NSString*)onlyMobileNumber;

-(NSString*)shortForm;
-(NSString*)firstName;
-(BOOL)isValidateEmailAddress;
-(BOOL)isValidIndianMobile;

+(NSString*)validCardNameForUser:(id<User>)user;

-(NSMutableAttributedString*)attributtedStringAddingPrivateIconOfHeight:(CGFloat)size;

@end
