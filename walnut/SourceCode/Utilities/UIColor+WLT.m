//
//  UIColor+WLT.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 02/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "UIColor+WLT.h"

@implementation UIColor (WLT)

+(UIColor*)colorWithGray:(CGFloat)gray andAlpha:(CGFloat)alpha {
    return [UIColor colorWithWhite:gray alpha:alpha];
}

+(UIColor*)darkGrayTextColor {
    return [UIColor colorWithRed:0.20 green:0.22 blue:0.24 alpha:1.00];
}

+(UIColor*)mediumGrayTextColor {
    return [self cadetColor];
}

+(UIColor*)lightGrayTextColor {
    return [self colorWithGray:0.7];
}

+(UIColor*)lightBackgroundColor {
    return [UIColor colorWithRed:0.94 green:0.94 blue:0.96 alpha:1.00];
}

+(UIColor*)darkBackgroundColor {
    return [self colorWithGray:0.45 andAlpha:1];
}

+(UIColor*)darkCellBackground {
    return [UIColor colorWithRed:0.30 green:0.33 blue:0.38 alpha:1.00];
}

+(UIColor*)appDefaultTintColor {
    return [UIColor redColor];
}

+(UIColor*)fiordColor {
    return [UIColor colorWithRed:0.30 green:0.33 blue:0.38 alpha:1.00];
}

+(UIColor*)lilyWhiteColor {
    return [UIColor colorWithRed:0.92 green:0.92 blue:0.92 alpha:1.00];
}

+(UIColor*)deepSkyBlueColor {
    return [UIColor colorWithRed:0.01 green:0.66 blue:0.96 alpha:1.00];
}

+(UIColor*)athensGray {
    
    return [UIColor colorWithRed:0.94 green:0.94 blue:0.96 alpha:1.00];
}

+(UIColor*)cadetColor {
    
    return [UIColor colorWithRed:0.33 green:0.37 blue:0.43 alpha:1.00];
}

+(UIColor*)fullMoonColor {
    return [UIColor colorWithRed:1.00 green:1.00 blue:0.79 alpha:1.00];
}

+(UIColor*)colorWithGray:(CGFloat)gray{
    return [self colorWithGray:gray andAlpha:1];
}

+(UIColor*)geraldineColor {
    return [UIColor colorWithRed:1.00 green:0.54 blue:0.51 alpha:1.00];
}

+(UIColor*)oweColor {
    return [self flamePeaColor];
}

+ (UIColor *)walnutIconColor {
    return [UIColor colorWithRed:0.00 green:0.62 blue:0.80 alpha:1.00];
}

+(UIColor*)getColor {
    return [self appleGreenColor];
}

+(UIColor*)lightSlateGrayColor {
    
    return [UIColor colorWithRed:0.45 green:0.55 blue:0.60 alpha:1.00];
}

+(UIColor*)denimColor {
    
    return [self walnutIconColor];
//    return [UIColor colorWithRed:0.08 green:0.40 blue:0.75 alpha:1.00];
}

+(UIColor*)rhinoColor {
    return [UIColor colorWithRed:0.24 green:0.27 blue:0.31 alpha:1.00];
}

+(UIColor*)placeHolderTextColor {
    return [UIColor colorWithRed:0.788 green:0.788 blue:0.808 alpha:1];
}

+(UIColor*)mediumCarmine {
    return [UIColor colorWithRed:0.73 green:0.26 blue:0.19 alpha:1];
}

+(UIColor*)flamePeaColor {
    return [UIColor colorWithRed:0.85 green:0.31 blue:0.23 alpha:1];
}

+(UIColor*)catSkillWhiteColor {
    return [UIColor colorWithRed:0.95 green:0.96 blue:0.97 alpha:1];
}

+(UIColor*)appleGreenColor {
    return [UIColor colorWithRed:0.5 green:0.72 blue:0 alpha:1];
}

+(UIColor*)hokiColor {
    return [UIColor colorWithRed:0.38 green:0.49 blue:0.55 alpha:1];
}

+(UIColor*)bermudaGray {
    return [UIColor colorWithRed:0.45 green:0.55 blue:0.61 alpha:1];
}

+(UIColor*)charcoalColor {
    return [UIColor colorWithRed:0.26 green:0.26 blue:0.26 alpha:1];
}

+(UIColor*)aroundBlackColor {
    return [UIColor colorWithRed:0.13 green:0.13 blue:0.13 alpha:1];
}

+(UIColor*)dogerBlueColor {
    return [UIColor colorWithRed:0.15 green:0.65 blue:0.98 alpha:1];
}

+(UIColor*)chileanFireColor {
    return [UIColor colorWithRed:0.98 green:0.44 blue:0 alpha:1];
}

+(UIColor*)cadetGrayColor {
    return [UIColor colorWithRed:0.58 green:0.64 blue:0.69 alpha:1];
}

+(UIColor*)limeadeColor {
    return [UIColor colorWithRed:0.44 green:0.64 blue:0 alpha:1];
}

+(UIColor*)pomegranateColor {
    return [UIColor colorWithRed:0.98 green:0.25 blue:0.15 alpha:1];
}

+(UIColor*)mercuryColor {
    return [UIColor colorWithRed:0.91 green:0.91 blue:0.91 alpha:1];
}

+(UIColor*)concreteColor {
    return [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1];
}

+(UIColor*)defaultCategoryColor {
    return [UIColor colorWithRed:0.51 green:0.47 blue:0 alpha:1];
}

+(UIColor*)bleuDeFranceColor {
    return [UIColor colorWithRed:0.25 green:0.61 blue:0.98 alpha:1.00];
}

+(UIColor*)darkElectricBlueColor {
    return [UIColor colorWithRed:0.31 green:0.40 blue:0.49 alpha:1.00];
}

+(UIColor*)sanJuanColor {
    return [UIColor colorWithRed:0.27 green:0.35 blue:0.39 alpha:1.00];
}



-(UIColor*)intermidiateColorFrom:(UIColor*)color withPercentage:(CGFloat)percent {
    
    size_t sizeStart = CGColorGetNumberOfComponents(self.CGColor);
    size_t sizeEnd = CGColorGetNumberOfComponents(color.CGColor);
    
    UIColor *retColor = nil;
    if ( (sizeStart >= 3) && (sizeEnd >= 3)) {
        
        const CGFloat *start = CGColorGetComponents(self.CGColor);
        const CGFloat *end = CGColorGetComponents(color.CGColor);
        
        CGFloat (^ finalValueBetween)(CGFloat startVal, CGFloat endVal) = ^CGFloat (CGFloat startVal, CGFloat endVal){
            
            CGFloat difval = (startVal - endVal);
            CGFloat newVal = startVal - (difval*percent);
            
            return newVal;
        };
        
        CGFloat startRed = start[0];
        CGFloat startGreen = start[1];
        CGFloat startBlue = start[2];
        
        CGFloat endRed = end[0];
        CGFloat endGreen = end[1];
        CGFloat endBlue = end[2];
        
        CGFloat newRed = finalValueBetween(startRed, endRed);
        CGFloat newGreen = finalValueBetween(startGreen, endGreen);
        CGFloat newBlue = finalValueBetween(startBlue, endBlue);
        
        CGFloat newAlpha = 1;
        
        retColor = [UIColor colorWithRed:newRed green:newGreen blue:newBlue alpha:newAlpha];
    }
    
    return retColor;
}

@end
