//
//  NSNumber+WLT.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 04/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const NilAmountPlaceholder;

@interface NSNumber (WLT)

-(NSString*)displayString;
-(NSString*)displayCurrencyString;
-(NSString*)displayRoundedCurrencyString;
-(NSString*)displayRoundedString;
-(NSString*)fixedCurrencyString;

-(NSNumber*)twoPlaceDecimalNumber;

@end
