//
//  NSObject+WLT.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 08/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "NSObject+WLT.h"
#import "WLTRootViewController.h"
#import "GTLServiceWalnut.h"
#import "LSToastMessage.h"

NSString *const WalnutErrorDomain = @"WalnutErrorDomain";
NSString *const NotifyGmailSessionExpired = @"NotifyGmailSessionExpired";
NSString *const NotifyPaymentsStatusChanged = @"NotifyPaymentsStatusChanged";

@implementation NSObject (WLT)

-(id)nullCheck {
    
    if ([self isKindOfClass:[NSNull class]]) {
        return nil;
    }
    
    return self;
}

-(WLTRootViewController*)rootController {
    
    return (WLTRootViewController*)[[UIApplication sharedApplication].delegate window].rootViewController;
}

+(NSError*)walnutErrorDefault {
    
    return [self walnutErrorWithTitle:@"Sorry!" andMessage:@"Please try sometime later."];
}

+(NSError*)walnutErrorWithMessage:(NSString*)message {
    if (!message) {
        message = @"Please try sometime later.";
    }
    return [self walnutErrorWithTitle:@"Sorry!" andMessage:message];
}

+(NSError*)walnutErrorWithTitle:(NSString*)title andMessage:(NSString*)message {
    
    NSError *error = [NSError errorWithDomain:WalnutErrorDomain code:0
                                     userInfo:@{@"title":title, @"message":message}];
    return error;
}

+(BOOL)isNotAGroupMemberError:(NSError*)error {
    
    BOOL retValue = NO;
    if ([error.domain isEqualToString:kGTLJSONRPCErrorDomain]) {
        
        if (error.code == 403) {
            
            retValue = YES;
        }
    }
    
    return retValue;
}

+(BOOL)checkAndShowToastIfNotGroupMember:(NSError*)error {
    
    BOOL retValue = NO;
    if ([self isNotAGroupMemberError:error]) {
        
        NSString *errorString = [error userInfo][@"error"];
        NSString *displayMessage = nil;
        if (errorString.length) {
            
            NSData *data = [errorString dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *object = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            displayMessage = object[@"Error"];
            if (!displayMessage) {
                displayMessage = object[@"Error:"];
            }
        }
        
        if (!displayMessage) {
            displayMessage = @"You can’t participate in this group because you are no longer a member";
        }
        
        NSAttributedString *attributed = [[NSAttributedString alloc] initWithString:displayMessage];
        [LSToastMessageHandler showMessage:attributed withID:NotAGroupMember];
        
        retValue = YES;
    }
    
    return retValue;
}

-(void)showAlertForError:(NSError*)error {
    
    if (error) {
		
        NSString *message = nil;
        NSString *title = nil;
        BOOL showAlertView = YES;
        
        if ([error.domain isEqualToString:WalnutErrorDomain]) {
            
            message = error.userInfo[@"message"];
            title = error.userInfo[@"title"];
        }
        else if ([error.domain isEqualToString:kGTLServiceErrorDomain]) {
            
        }
        else if ([error.domain isEqualToString:NSURLErrorDomain]) {
            
            if ((error.code == NSURLErrorTimedOut) || (error.code == NSURLErrorNetworkConnectionLost) || (error.code == NSURLErrorNotConnectedToInternet)) {
                
                title = @"No Network!";
                message = @"Please check your internet connection and try again later!";
            }
        }
        else if ([error.domain isEqualToString:kGTLJSONRPCErrorDomain]) {
            
            NSString *errorString = [error userInfo][@"error"];
            
            if (error.code == 401) {
                
                title = @"Invalid session!";
                message = @"Your session has expired. Please try logging in again!";
                
                [[NSNotificationCenter defaultCenter] postNotificationName:NotifyGmailSessionExpired object:nil];
            }
            else if (error.code == 403) {
                
                NSString *displayMessage = nil;
                if (errorString.length) {
                    
                    NSData *data = [errorString dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary *object = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    
                    displayMessage = object[@"Error"];
                }
                
                if (!displayMessage) {
                    displayMessage = @"You can’t participate in this group because you are no longer a member";
                }
                
                NSAttributedString *attributed = [[NSAttributedString alloc] initWithString:displayMessage];
                [LSToastMessageHandler showMessage:attributed withID:NotAGroupMember];
                
                showAlertView = NO;
            }
            else if (errorString.length) {
                
                NSData *data = [errorString dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *object = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                message = object[@"Error"];
            }
        }else if ([error.domain isEqualToString:kGTMSessionFetcherStatusDomain]) {
            
            if (error.code == 400) {
                
                title = @"Invalid session!";
                message = @"Your session has expired. Please try logging in again!";
                
                [[NSNotificationCenter defaultCenter] postNotificationName:NotifyGmailSessionExpired object:nil];
            }
        }
        
        if (!message.length) {
            message = @"Please try sometime later.";
        }
        if (!title.length) {
            title = @"Sorry!";
        }
        
        if (showAlertView) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:nil
                                                  cancelButtonTitle:@"Okay"
                                                  otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

@end
