//
//  UILabel+User.m
//  walnut
//
//  Created by Abhinav Singh on 07/03/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "UILabel+User.h"
#import "WLTContactsManager.h"
#import "WLTDatabaseManager.h"
#import "WLTColorPalette.h"
#import "WLTGroup.h"

@implementation UILabel (User)

-(void)displayUserName:(id<User>)user {
    
    self.text = @"...";
    
    __weak typeof(self) weakSelf = self;
    [[WLTContactsManager sharedManager] displayNameForUser:user defaultIsMob:NO completion:^(NSString *data) {
        
        if (weakSelf) {
            
            weakSelf.text = data;
        }
    }];
}

-(void)displayUserNameOrNumberOfUser:(id<User>)user {
    
    self.text = @"...";
    
    __weak typeof(self) weakSelf = self;
    [[WLTContactsManager sharedManager] displayNameForUser:user defaultIsMob:YES completion:^(NSString *data) {
        
        if (weakSelf) {
            
            weakSelf.text = data;
        }
    }];
}

-(void)displayGroupName:(WLTGroup *)group checkDirect:(BOOL)checkDirect{
    
    NSString *name = nil;
    if ( checkDirect && group.members.count == 2) {
        name = @"Direct";
    }else {
        name = group.dName;
    }
    
    if (group.isPrivate.boolValue) {
        
        self.attributedText = [name attributtedStringAddingPrivateIconOfHeight:self.font.fontDescriptor.pointSize];
    }else {
        self.text = name;
    }
}

@end
