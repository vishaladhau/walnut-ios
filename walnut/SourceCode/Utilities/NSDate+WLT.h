//
//  NSDate+WLT.h
//  walnut
//
//  Created by Abhinav Singh on 26/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (WLT)

+(long long)currentTimeStamp;
+(NSDate*)dateFromWalnutTimestamp:(long long)stamp;

-(long long)timeStamp;
-(NSString*)displayString;
-(NSDate*)midnightDate;

-(NSString*)relativeDateString;
-(NSString*)dateMonthDisplay;

@end
