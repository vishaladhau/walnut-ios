//
//  WLTPaymentContactsViewController.h
//  walnut
//
//  Created by Abhinav Singh on 12/08/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTContactsViewController.h"

@interface WLTPaymentContactsViewController : WLTContactsViewController{
    
}

@property(nonatomic, assign) UserTransactionState transactionState;

@end
