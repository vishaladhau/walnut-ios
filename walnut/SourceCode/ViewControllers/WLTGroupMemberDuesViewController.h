//
//  WLTGroupMemberDuesViewController.h
//  walnut
//
//  Created by Abhinav Singh on 01/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"

@class WLTGroup;

@interface WLTGroupMemberDuesViewController : WLTViewController <UITableViewDelegate, UITableViewDataSource>{
    
    __weak IBOutlet UITableView *usersTableView;
}

@property(nonatomic, strong) WLTGroup *currentGroup;

@end

