//
//  WLTCombinedGroupPaymentsViewController.m
//  walnut
//
//  Created by Abhinav Singh on 02/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTCombinedGroupPaymentsViewController.h"
#import "WLTCombinedGroupPayments.h"
#import "WLTCombinedGroupPaymentsView.h"
#import "WLTTransactionsListViewController.h"
#import "WLTTransactionUser.h"
#import "WLTGroupDetailViewController.h"
#import "WLTColorPalette.h"
#import "WLTGroupPaymentAmountEditViewController.h"
#import "WLTPartialSettle.h"
#import "WLTContactsManager.h"
#import "WLTGroupMemberDuesViewController.h"
#import "WLTRootViewController.h"

@interface WLTCombinedGroupPaymentsViewController () <WLTCombinedGroupPaymentsViewDelegate, WLTEmptyStateViewDelegate> {
    
    __weak WLTEmptyStateView *placeHolderView;
}

@property(nonatomic, strong) NSArray *allGroupsArray;
@property(nonatomic, strong) NSArray *groups;

@property(nonatomic, assign) UserTransactionState transactionState;
@property(nonatomic, assign) WLTCombinedGroupPaymentsType paymentsType;

@property(nonatomic, strong) id <User> user;
@property(nonatomic, assign) WLTCombinedGroupStatus privacyStatus;
@end

@implementation WLTCombinedGroupPaymentsViewController

- (NSString *)screenName {
    return (self.transactionState == UserTransactionStateOwe) ? @"WLTCombinedGroupPaymentsViewControllerYouOwe" : @"WLTCombinedGroupPaymentsViewControllerYouGet";
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    headerInfoLabel.font = [UIFont sfUITextRegularOfSize:16];
    amountLabel.font = [UIFont sfUITextRegularOfSize:16];
    
    headerView.backgroundColor = [UIColor whiteColor];
    [headerView addBorderLineAtPosition:BorderLinePositionBottom];
    
    theScrollView.alwaysBounceVertical = YES;
    
    WLTEmptyStateView *emptyView = [WLTEmptyStateView emptyStateViewWithStyle:WLTEmptyStateViewStyleDefault andDelegate:self];
    emptyView.userInteractionEnabled = NO;
    emptyView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:emptyView];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(emptyView);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[emptyView]-0-|" options:0 metrics:nil views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[emptyView]-0-|" options:0 metrics:nil views:dict]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCurrentGroupStates:) name:NotifyGroupChanged object:nil];
    
    placeHolderView = emptyView;
    placeHolderView.hidden = YES;
    
    [self addSubviewsForCurrentData];
}

-(void)addSubviewsForCurrentData {
    
    for ( UIView *view in theScrollView.subviews) {
        if ([view isKindOfClass:[WLTCombinedGroupPaymentsView class]]) {
            [view removeFromSuperview];
        }
    }
    
    BOOL hideHeader = YES;
    
    if (self.allGroupsArray.count) {
        
        placeHolderView.hidden = YES;
        theScrollView.hidden = NO;
        headerView.hidden = NO;
        
        CGFloat width = [[UIScreen mainScreen] bounds].size.width;
        CGFloat amount = 0;
        
        WLTCombinedGroupPaymentsView *lastView = nil;
        
        for ( WLTCombinedGroupPayments *pays in self.allGroupsArray ) {
            
            WLTCombinedGroupPaymentsView *view = [WLTCombinedGroupPaymentsView initWithDefaultXib];
            view.delegate = self;
            view.translatesAutoresizingMaskIntoConstraints = NO;
            [view showDetailsOfData:pays];
            [theScrollView addSubview:view];
            
            NSDictionary *dict = NSDictionaryOfVariableBindings(view);
            [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-0-[view(==%f)]-0-|", width] options:0 metrics:nil views:dict]];
            if (lastView) {
                
                dict = NSDictionaryOfVariableBindings(view, lastView);
                [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lastView]-0-[view]" options:0 metrics:nil views:dict]];
            }else {
                
                dict = NSDictionaryOfVariableBindings(view);
                [view addBorderLineAtPosition:BorderLinePositionTop];
                [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]" options:0 metrics:nil views:dict]];
            }
            
            lastView = view;
            
            amount += pays.amount;
        }
        
        if (lastView) {
            
            NSDictionary *dict = NSDictionaryOfVariableBindings(lastView);
            [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lastView]-0-|" options:0 metrics:nil views:dict]];
        }
        
        amountLabel.text = @(amount).displayCurrencyString;
        
        if (self.transactionState == UserTransactionStateOwe) {
            
            amountLabel.textColor = [UIColor oweColor];
            if (self.paymentsType == WLTCombinedGroupPaymentsTypeUser) {
                [[WLTContactsManager sharedManager] displayNameForUser:self.user defaultIsMob:YES completion:^(NSString *name) {
                    
                    headerInfoLabel.text = [NSString stringWithFormat:headerInfoLabel.text = @"You owe to %@", name];
                }];
            }else {
                headerInfoLabel.text = @"You owe to your friends";
            }
            self.title = @"You Owe";
            
            hideHeader = NO;
        }
        else if (self.transactionState == UserTransactionStateGet) {
            
            amountLabel.textColor = [UIColor getColor];
            if (self.paymentsType == WLTCombinedGroupPaymentsTypeUser) {
                [[WLTContactsManager sharedManager] displayNameForUser:self.user defaultIsMob:YES completion:^(NSString *name) {
                    
                    headerInfoLabel.text = [NSString stringWithFormat:@"You get from %@", name];
                }];
            }else {
                
                headerInfoLabel.text = @"You get from your friends";
            }
            
            self.title = @"You Get";
            
            hideHeader = NO;
        }else {
            
            self.title = @"Settle";
            
            headerInfoLabel.text = @"";
            amountLabel.text = @"";
        }
    }
    else {
        
        placeHolderView.hidden = NO;
        theScrollView.hidden = YES;
        headerView.hidden = YES;
        
        if (self.transactionState == UserTransactionStateOwe) {
            self.title = @"You Owe";
            [placeHolderView showTitle:@"Great!"
                           andSubtitle:@"You have cleared all your dues"
                             imageName:@"OweNothingImage"];
        } else {
            self.title = @"You Get";
            [placeHolderView showTitle:@"Great!"
                           andSubtitle:@"You have settled with your friends"
                             imageName:@"OweNothingImage"];
        }
    }
    
    if (hideHeader) {
        
        headerOriginY.constant = -56;
        [theScrollView setContentInset:UIEdgeInsetsZero];
    }else {
        
        headerOriginY.constant = 0;
        [theScrollView setContentInset:UIEdgeInsetsMake(15, 0, 15, 0)];
    }
}

-(void)showDataForUser:(id<User>)user privacyStatus:(WLTCombinedGroupStatus)privacyStatus {
    
    NSPredicate *getPred = [NSPredicate predicateWithFormat:@"currentUserGet > 0"];
    NSPredicate *payPred = [NSPredicate predicateWithFormat:@"currentUserOwe > 0"];
    NSCompoundPredicate *comPred = [NSCompoundPredicate orPredicateWithSubpredicates:@[payPred, getPred]];
    
    NSArray *allGrps = nil;
    
    if (privacyStatus == WLTCombinedGroupStatusBoth) {
        
        allGrps = [[WLTDatabaseManager sharedManager] allObjectsOfClass:@"WLTGroup" withPredicate:comPred andSortDescriptors:nil];
    }
    else {
        
        NSPredicate *privatePred = nil;
        if (privacyStatus == WLTCombinedGroupStatusPrivate) {
            privatePred = [NSPredicate predicateWithFormat:@"isPrivate == %@", @(YES)];
        }
        else{
            privatePred = [NSPredicate predicateWithFormat:@"isPrivate == %@", @(NO)];
        }
        
        NSCompoundPredicate *comPred = [NSCompoundPredicate orPredicateWithSubpredicates:@[payPred, getPred]];
        NSCompoundPredicate *final = [NSCompoundPredicate andPredicateWithSubpredicates:@[comPred, privatePred]];
        
        allGrps = [[WLTDatabaseManager sharedManager] allObjectsOfClass:@"WLTGroup" withPredicate:final andSortDescriptors:nil];
    }
    
    NSString *otherMob = [user completeNumber];
    
    WLTCombinedGroupPayments *sharedGrpPays = [[WLTCombinedGroupPayments alloc] initWithUser:user];
    WLTCombinedGroupPayments *privateGrpPays = [[WLTCombinedGroupPayments alloc] initWithUser:user];
    
    for (WLTGroup *grp in allGrps) {
        
        WLTCombinedGroupPayments *grpPays = nil;
        if (grp.isPrivate.boolValue) {
            grpPays = privateGrpPays;
        }
        else {
            grpPays = sharedGrpPays;
        }
        
        if ([grp userTransactionState] == UserTransactionStateGet) {
            
            for ( WLTTransactionUser *userTrans in grp.userGetFrom ) {
                if ([userTrans.mobileString isEqualToString:otherMob]) {
                    
                    [grpPays addUserData:userTrans forGroup:grp];
                    break;
                }
            }
        }
        else if ([grp userTransactionState] == UserTransactionStateOwe) {
            
            for ( WLTTransactionUser *userTrans in grp.userPayTo ) {
                if ([userTrans.mobileString isEqualToString:otherMob]) {
                    
                    [grpPays addUserData:userTrans forGroup:grp];
                    break;
                }
            }
        }
    }
    
    [sharedGrpPays refreshCombinedData];
    [privateGrpPays refreshCombinedData];
    
    self.user = user;
    self.paymentsType = WLTCombinedGroupPaymentsTypeUser;
    self.privacyStatus = privacyStatus;
    
    NSMutableArray *allGrpsArray = [NSMutableArray new];
    
    if (sharedGrpPays.groupsData.count) {
        
        if ((sharedGrpPays.transactionState == UserTransactionStateUnknown) || (sharedGrpPays.transactionState == UserTransactionStateNone)) {
            sharedGrpPays.transactionState = UserTransactionStateGet;
        }
        
        [allGrpsArray addObject:sharedGrpPays];
    }
    if (privateGrpPays.groupsData.count) {
        
        if ((privateGrpPays.transactionState == UserTransactionStateUnknown) || (privateGrpPays.transactionState == UserTransactionStateNone)) {
            privateGrpPays.transactionState = UserTransactionStateGet;
        }
        
        [allGrpsArray addObject:privateGrpPays];
    }
    
    if (allGrpsArray.count == 1) {
        
        WLTCombinedGroupPayments *grp = [allGrpsArray firstObject];
        self.transactionState = grp.transactionState;
    }else {
        
        self.transactionState = UserTransactionStateUnknown;
    }
    
    self.allGroupsArray = allGrpsArray;
    
    canShowHeader = YES;
    if (self.transactionState == UserTransactionStateOwe) {
        
        self.navBarColor = [UIColor oweColor];
    }else if (self.transactionState == UserTransactionStateGet) {
        
        self.navBarColor = [UIColor getColor];
    }else {
        
        self.navBarColor = [UIColor getColor];
        canShowHeader = NO;
    }
    
    if ([self isViewLoaded]) {
        
        [self addSubviewsForCurrentData];
    }
}

-(void)refreshCurrentGroupStates:(NSNotification*)notify {
    
    for ( WLTCombinedGroupPayments *grp in self.allGroupsArray ) {
        [grp reloadGroupsData];
    }
    
    [self addSubviewsForCurrentData];
}

-(void)showDataOfGroups:(NSArray*)grps forState:(UserTransactionState)state {
    
    NSMutableArray *dataArray = nil;
    if (grps.count == 1) {
        
        NSSet *splitsArray = nil;
        WLTGroup *grp = [grps firstObject];
        
        if (state == UserTransactionStateOwe) {
            
            splitsArray = grp.userPayTo;
            self.navBarColor = [UIColor oweColor];
        }else if (state == UserTransactionStateGet) {
            
            splitsArray = grp.userGetFrom;
            self.navBarColor = [UIColor getColor];
        }
        
        dataArray = [NSMutableArray new];
        for ( WLTTransactionUser *user in splitsArray ) {
            
            WLTCombinedGroupPayments *grpPays = [[WLTCombinedGroupPayments alloc] initWithUser:user];
            [grpPays addUserData:user forGroup:grp];
            [grpPays refreshCombinedData];
            
            [dataArray addObject:grpPays];
        }
    }
    else if (grps.count > 1) {
        
        dataArray = [NSMutableArray new];
        NSMutableDictionary *mappingShared = [NSMutableDictionary new];
        NSMutableDictionary *mappingPrivate = [NSMutableDictionary new];
        
        for ( WLTGroup *grp in grps ) {
            
            NSMutableDictionary *mappingDict = nil;
            if (grp.isPrivate.boolValue) {
                mappingDict = mappingPrivate;
            }else {
                mappingDict = mappingShared;
            }
            
            void (^ processUser)(WLTTransactionUser *user) = ^void (WLTTransactionUser *user){
                
                NSString *userM = user.mobileString;
                WLTCombinedGroupPayments *grpPays = mappingDict[userM];
                if (!grpPays) {
                    grpPays = [[WLTCombinedGroupPayments alloc] initWithUser:user];
                    mappingDict[userM] = grpPays;
                }
                
                [grpPays addUserData:user forGroup:grp];
            };
            
            for ( WLTTransactionUser *user in grp.userPayTo ) {
                processUser(user);
            }
            for ( WLTTransactionUser *user in grp.userGetFrom ) {
                processUser(user);
            }
        }
        
        for ( WLTCombinedGroupPayments *grpAll in mappingShared.allValues) {
            
            [grpAll refreshCombinedData];
            if (grpAll.transactionState == state) {
                [dataArray addObject:grpAll];
            }
        }
        
        for ( WLTCombinedGroupPayments *grpAll in mappingPrivate.allValues) {
            
            [grpAll refreshCombinedData];
            if (grpAll.transactionState == state) {
                [dataArray addObject:grpAll];
            }
        }
    }
    
    self.groups = grps;
    self.transactionState = state;
    self.allGroupsArray = dataArray;
    self.paymentsType = WLTCombinedGroupPaymentsTypeGroup;
    
    if (self.transactionState == UserTransactionStateOwe) {
        
        self.navBarColor = [UIColor oweColor];
    }else if (self.transactionState == UserTransactionStateGet) {
        
        self.navBarColor = [UIColor getColor];
    }
    
    if ([self isViewLoaded]) {
        
        [self addSubviewsForCurrentData];
    }
}

-(void)combinedPayCell:(WLTCombinedGroupPaymentsView*)cell selectedGroup:(WLTGroup*)grp {
    
    BOOL canContinue = YES;
    if (cell.paymentGroup.groupsData.count == 1) {
        
        for ( UIViewController *inStackCont in self.navigationController.viewControllers ) {
            if ([inStackCont isKindOfClass:[WLTGroupDetailViewController class]]) {
                
                WLTGroupDetailViewController *grpDet = (WLTGroupDetailViewController*)inStackCont;
                if ([grpDet.currentGroup.groupUUID isEqualToString:grp.groupUUID]) {
                    
                    canContinue  = NO;
                    break;
                }
            }
        }
    }
    
    if (canContinue) {
        
        WLTGroupDetailViewController *details = [self controllerWithIdentifier:@"WLTGroupDetailViewController"];
        details.hidesBottomBarWhenPushed = YES;
        details.currentGroup = grp;
        details.navBarColor = [[WLTColorPalette sharedPalette] colorForGroup:grp];
        [self.navigationController pushViewController:details animated:YES];
    }
}

-(void)reminderButtonClickedFromCell:(WLTCombinedGroupPaymentsView*)cell {
	
	[self showReminderScreenForPayment:cell.paymentGroup];
}

-(void)actionButtonClickedFromCell:(WLTCombinedGroupPaymentsView*)cell {
    
    __weak typeof(self) weakSelf = self;
    
    if ( cell.paymentGroup.transactionState == UserTransactionStateGet ) {
		
        [self settleForCombinedTransaction:cell.paymentGroup completion:^(BOOL success) {
            
            if (success) {
                
                NSMutableArray *allControllers = [NSMutableArray new];
                
                for ( UIViewController *cont in weakSelf.navigationController.viewControllers ) {
                    
                    if (![cont isKindOfClass:[WLTGroupMemberDuesViewController class]]) {
                        
                        [allControllers addObject:cont];
                        if ([cont isKindOfClass:[WLTCombinedGroupPaymentsViewController class]]) {
                            break;
                        }
                    }
                }
                
                [weakSelf.navigationController setViewControllers:allControllers animated:YES];
            }
        }];
	}
    else if ( cell.paymentGroup.transactionState == UserTransactionStateOwe ) {
        
        [weakSelf showPaymentScreenForPayGroup:cell.paymentGroup];
	}
}

-(void)showPaymentScreenForPayGroup:(WLTCombinedGroupPayments*)payGrp {
    
    __weak typeof(self) weakSelf = self;
    [self trackEvent:GA_ACTION_WPAY_CLICKED andValue:nil];
    [self showPaymentScreenForTransaction:payGrp message:nil receiverMessage:nil andOldTrans:nil withCompletion:^(BOOL success) {
        
        if (success) {
            
            for ( UIViewController *cont in weakSelf.navigationController.viewControllers ) {
                if ([cont isKindOfClass:[WLTCombinedGroupPaymentsViewController class]]) {
                    
                    NSArray *allCont = [weakSelf.navigationController allControllersUpTo:cont];
                    [weakSelf.navigationController setViewControllers:allCont animated:YES];
                    
                    break;
                }
            }
        }
    }];
}

#pragma mark - WLTEmptyStateViewDelegate

-(void)emptyStateViewReloadScreen:(WLTEmptyStateView *)view {}

-(void)emptyStateViewActionButtonTapped:(WLTEmptyStateView *)view {}

@end
