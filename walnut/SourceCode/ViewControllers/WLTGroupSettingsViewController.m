//
//  WLTGroupSettingsViewController.m
//  walnut
//
//  Created by Abhinav Singh on 25/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTGroupSettingsViewController.h"
#import "WLTGroupNameTableViewCell.h"
#import "WLTGroupTypeTableViewCell.h"
#import "WLTColorPalette.h"
#import "WLTContact.h"
#import "WLTContactsManager.h"
#import "WLTUser.h"
#import "WLTNewGroupValidator.h"
#import "WLTContactTableViewCell.h"
#import "WLTGroupContactsViewController.h"

@implementation WLTGroupSettingsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
	
	self.title = @"Edit Group";
    
    makePrivateGroup = self.toEdit.isPrivate.boolValue;
    
    [theTableView registerClass:[WLTContactTableViewCell class] forCellReuseIdentifier:@"WLTContactTableViewCell"];
    self.selectedColorID = self.toEdit.colourID;
    
    self.members = [NSMutableArray new];
    for ( WLTUser *usr in self.toEdit.members ) {
        
        WLTContact *cont = [[WLTContact alloc] initWithName:usr.name andNumber:usr.mobileString];
        [self.members addObject:cont];
    }
    
    [theTableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [theTableView reloadData];
}

-(BOOL)userCanTakeActions {
    return YES;
}

-(BOOL)userCanEditGroupInfo {
    
    BOOL canEdit = NO;
    if ([self.toEdit.owner.mobileString isEqualToString:currentUserMobileNumber]) {
        canEdit = YES;
    }
    
    return canEdit;
}

-(void)exitGroupClicked{
    
    __weak typeof(self) weakSelf = self;
    
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Please Confirm!" message:[NSString stringWithFormat:@"Are you sure to exit group %@", self.toEdit.dName] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"Exit" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        [weakSelf leaveGroup:weakSelf.toEdit];
    }];
    
    [controller addAction:cancelAction];
    [controller addAction:deleteAction];
    
    [self presentViewController:controller animated:YES completion:nil];
}

-(void)deleteGroupClicked{
    
    __weak typeof(self) weakSelf = self;
    
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Please Confirm!" message:[NSString stringWithFormat:@"Are you sure to delete group %@", self.toEdit.dName] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        [weakSelf deleteGroup:weakSelf.toEdit];
    }];
    
    [controller addAction:cancelAction];
    [controller addAction:deleteAction];
    
    [self presentViewController:controller animated:YES completion:nil];
}

-(void)leaveGroup:(WLTGroup*)grp {
    
    GTLWalnutMGroup *groupUpdated = [[GTLWalnutMGroup alloc] init];
    groupUpdated.uuid = grp.groupUUID;
    groupUpdated.deviceUuid = [WLTDatabaseManager sharedManager].appSettings.deviceIdentifier;
    groupUpdated.name = grp.aName;
    
    WLTLoggedInUser *currentuser = [WLTDatabaseManager sharedManager].currentUser;
    
    GTLWalnutMSplitUser *user = [[GTLWalnutMSplitUser alloc] init];
    user.name = currentuser.name;
    user.mobileNumber = currentuser.mobileString;
    
    groupUpdated.owner = user;
    
    NSMutableArray *allMems = [NSMutableArray new];
    for ( WLTUser *already in grp.members ) {
        
        if (![already.mobileString isEqualToString:currentuser.mobileString]) {
            
            GTLWalnutMSplitUser *users = [[GTLWalnutMSplitUser alloc] init];
            users.name = already.name;
            users.mobileNumber = already.mobileString;
            
            [allMems addObject:users];
        }
    }
    groupUpdated.members = allMems;
    
    __weak WLTGroupSettingsViewController *weakSelf = self;
    [self startLoading];
    
    GTLQueryWalnut *query = [GTLQueryWalnut queryForGroupUpdateWithObject:groupUpdated];
    
    [[WLTNetworkManager sharedManager].walnutService executeQuery:query completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
        
        if (!error) {
            
            [[WLTDatabaseManager sharedManager] deleteGroups:@[grp]];
            
            if (weakSelf.completion) {
                weakSelf.completion(YES);
            }
            
            [weakSelf trackEvent:GA_ACTION_GROUP_LEFT label:weakSelf.title andValue:@(groupUpdated.members.count)];
        }else {
            
            if ([NSObject isNotAGroupMemberError:error]) {
                
                [[WLTDatabaseManager sharedManager] deleteGroups:@[grp]];
                
                if (weakSelf.completion) {
                    weakSelf.completion(YES);
                }
                
                [weakSelf trackEvent:GA_ACTION_GROUP_LEFT label:weakSelf.title andValue:@(groupUpdated.members.count)];
            }else {
                
                [weakSelf showAlertForError:error];
            }
        }
        
        [weakSelf endViewLoading];
    }];
}

-(void)deleteGroup:(WLTGroup*)grp {
    
    GTLWalnutMGroup *groupUpdated = [[GTLWalnutMGroup alloc] init];
    groupUpdated.uuid = grp.groupUUID;
    groupUpdated.deleted = @(YES);
    groupUpdated.deviceUuid = [WLTDatabaseManager sharedManager].appSettings.deviceIdentifier;
    
    groupUpdated.name = grp.aName;
    
    WLTLoggedInUser *currentuser = [WLTDatabaseManager sharedManager].currentUser;
    
    GTLWalnutMSplitUser *user = [[GTLWalnutMSplitUser alloc] init];
    user.name = currentuser.name;
    user.mobileNumber = currentuser.mobileString;
    
    groupUpdated.owner = user;
    
    NSMutableArray *allMems = [NSMutableArray new];
    [allMems addObject:user];
    
    for ( WLTUser *already in grp.members ) {
        
        GTLWalnutMSplitUser *users = [[GTLWalnutMSplitUser alloc] init];
        users.name = already.name;
        users.mobileNumber = already.mobileString;
        
        [allMems addObject:users];
    }
    groupUpdated.members = allMems;
    
    __weak WLTGroupSettingsViewController *weakSelf = self;
    [self startLoading];
    
    GTLQueryWalnut *query = [GTLQueryWalnut queryForGroupUpdateWithObject:groupUpdated];
    
    [[WLTNetworkManager sharedManager].walnutService executeQuery:query completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
        
        if (!error) {
            
            [[WLTDatabaseManager sharedManager] deleteGroups:@[grp]];
            
            if (weakSelf.completion) {
                weakSelf.completion(YES);
            }
            
            [weakSelf trackEvent:GA_ACTION_GROUP_DELETED label:weakSelf.title andValue:@(groupUpdated.members.count)];
        }else {
            
            [weakSelf showAlertForError:error];
        }
        
        [weakSelf endViewLoading];
    }];
}

-(void)doneClicked:(UIBarButtonItem*)item {
    
    if (self.isLoading) {
        return;
    }
    
    [self.view endEditing:YES];
    
    if ([self userCanEditGroupInfo]) {
        
        NSString *tempGrpName = nil;
        BOOL isDirectGrp = [WLTNewGroupValidator isStatusDirectWithMembers:self.members];
        if (isDirectGrp) {
            
            tempGrpName = self.toEdit.aName;
            if (!tempGrpName.length) {
                tempGrpName = @"";
            }
        }else if (groupNameTableCell) {
            
            tempGrpName = [groupNameTableCell->nameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        }
        
        NSString *errorMessage = nil;
        UIAlertAction *action = nil;
        
        if (self.members.count < 2) {
            
            errorMessage = @"Please add some members to the group.";
        }
        else if (!tempGrpName.length && !isDirectGrp) {
            
            errorMessage = @"Please enter name of group.";
            action = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * _Nonnull action) {
                                                
                                                [groupNameTableCell->nameTextField becomeFirstResponder];
                                            }];
            
            [theTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        else if (!self.selectedColorID.length) {
            
            errorMessage = @"Please select colour of your group.";
        }
        
        if (errorMessage) {
            
            if (!action) {
                action = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault
                                                handler:nil];
            }
            
            
            UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Sorry!" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
            [controller addAction:action];
            
            [self presentViewController:controller animated:YES completion:nil];
        }else {
            
            WLTUser *currentuser = [WLTDatabaseManager sharedManager].currentUser;
            
            GTLWalnutMGroup *groupUpdated = [[GTLWalnutMGroup alloc] init];
            groupUpdated.privateProperty = @(makePrivateGroup);
            groupUpdated.deviceUuid = [WLTDatabaseManager sharedManager].appSettings.deviceIdentifier;
            groupUpdated.name = tempGrpName;
            
            NSMutableArray *allMems = [NSMutableArray new];
            for ( WLTContact *cont in self.members ) {
                
                if (![cont.mobile.number isEqualToString:currentuser.mobileString]) {
                    
                    GTLWalnutMSplitUser *user = [[GTLWalnutMSplitUser alloc] init];
                    user.name = cont.fullName;
                    user.mobileNumber = cont.mobile.number;
                    
                    [allMems addObject:user];
                }
            }
            
            GTLWalnutMSplitUser *user = [[GTLWalnutMSplitUser alloc] init];
            user.name = currentuser.name;
            user.mobileNumber = currentuser.mobileString;
            
            [allMems addObject:user];
            
            groupUpdated.owner = user;
            
            groupUpdated.members = allMems;
            
            __weak WLTGroupSettingsViewController *weakSelf = self;
            [self startLoading];
            
            groupUpdated.uuid = self.toEdit.groupUUID;
            
            GTLQueryWalnut *query = [GTLQueryWalnut queryForGroupUpdateWithObject:groupUpdated];
            [[WLTNetworkManager sharedManager].walnutService executeQuery:query completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
                
                if (!error) {
                    
                    weakSelf.toEdit.colourID = self.selectedColorID;
                    [weakSelf.toEdit updateMembersAndNameWLTGroup:groupUpdated];
                    [[NSNotificationCenter defaultCenter] postNotificationName:NotifyGroupChanged object:weakSelf.toEdit];
                    
                    if (weakSelf.completion) {
                        weakSelf.completion(YES);
                    }
                    [weakSelf trackEvent:GA_ACTION_GROUP_MODIFIED label:weakSelf.title andValue:@(groupUpdated.members.count)];
                }else {
                    
                    [weakSelf showAlertForError:error];
                    [weakSelf endViewLoading];
                }
            }];
        }
    }
    else {
        
        self.toEdit.colourID = self.selectedColorID;
        [[WLTDatabaseManager sharedManager] saveDataBase];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NotifyGroupChanged object:self.toEdit];
        
        if (self.completion) {
            self.completion(YES);
        }
    }
}

-(void)setUpPrivacyCell:(WLTGroupTypeTableViewCell*)tcell forRowIndex:(NSInteger)row {
    
    [tcell setUpForPrivate:makePrivateGroup];
    tcell.accessoryType = UITableViewCellAccessoryNone;
}

-(NSString*)cellIdentifierForContact:(WLTContact*)contact {
    
    NSString *cellIndentifier = @"WLTContactTableViewCell";
    if ([self userCanEditGroupInfo]) {
        cellIndentifier = @"WLTEditableContactTableViewCell";
    }
    if ([contact.mobile.number isEqualToString:self.toEdit.owner.mobileString]) {
        cellIndentifier = @"WLTAdminTableViewCell";
    }
    
    return cellIndentifier;
}

-(void)addNewMembersClicked:(LSButton*)btn {
    
    WLTGroupContactsViewController *cont = [[WLTGroupContactsViewController alloc] initWithNibName:@"WLTContactsViewController" bundle:nil];
    cont.selectedContacts = self.members;
    cont.selectedColorID = self.selectedColorID;
    cont.contactSelectionType = GroupContactSelectionTypeEdit;
    cont.navBarColor = self.navBarColor;
    [self.navigationController pushViewController:cont animated:YES];
}

#pragma mark - UITableViewDelegate & UITableViewDataSource

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GroupControllerSectionType secType = [self identifierForSectionIndex:indexPath.section];
    
    UITableViewCell *cell = nil;
    if (secType == GroupControllerSectionTypeMembers) {
        
        if ( indexPath.row >= self.members.count) {
            
            UITableViewCell *addMemCell = [tableView dequeueReusableCellWithIdentifier:@"AddMemberTableViewCell"];
            if (!addMemCell) {
                
                addMemCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddMemberTableViewCell"];
                
                LSButton *btn = [[LSButton alloc] initWithFrame:CGRectZero];
                btn.translatesAutoresizingMaskIntoConstraints = NO;
                [btn addTarget:self action:@selector(addNewMembersClicked:) forControlEvents:UIControlEventTouchUpInside];
                [btn setTitle:@"Add Members"];
                [btn applyDefaultStyle];
                [addMemCell.contentView addSubview:btn];
                
                NSDictionary *dict = NSDictionaryOfVariableBindings(btn);
                [addMemCell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[btn]-10-|" options:0 metrics:nil views:dict]];
                [addMemCell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[btn(==40)]-8-|" options:0 metrics:nil views:dict]];
            }
            
            cell = addMemCell;
        }
    }
    
    if (!cell) {
        cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    }
    
    if ([cell isKindOfClass:[WLTGroupNameTableViewCell class]]) {
        
        WLTGroupNameTableViewCell *tCell = (WLTGroupNameTableViewCell*)cell;
        if (!tCell->nameTextField.text.length) {
            tCell->nameTextField.text = self.toEdit.aName;
        }
        
        if (![self userCanEditGroupInfo]) {
            
            tCell->nameTextField.clearButtonMode = UITextFieldViewModeNever;
            tCell.userInteractionEnabled = NO;
        }else {
            
            tCell->nameTextField.clearButtonMode = UITextFieldViewModeAlways;
            tCell.userInteractionEnabled = YES;
        }
    }
    if (secType == GroupControllerSectionTypeEditAction) {
        
        cell.textLabel.font = [UIFont sfUITextRegularOfSize:16];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor flamePeaColor];
        
        if ([self userCanEditGroupInfo]) {
            
            cell.textLabel.text = @"Delete Group";
        }else {
            
            cell.textLabel.text = @"Leave Group";
        }
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BOOL actionTaken = NO;
    GroupControllerSectionType secType = [self identifierForSectionIndex:indexPath.section];
    if (secType == GroupControllerSectionTypeEditAction) {
        
        if ([self userCanEditGroupInfo]) {
            
            actionTaken = YES;
            [self deleteGroupClicked];
        }else {
            
            actionTaken = YES;
            [self exitGroupClicked];
        }
    }
    
    if (!actionTaken) {
        
        [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    }else {
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    GroupControllerSectionType secType = [self identifierForSectionIndex:section];
    NSInteger count = 0;
    if (secType == GroupControllerSectionTypeGroupType) {
        count = 1;
    }
    else if (secType == GroupControllerSectionTypeEditAction) {
        count = 1;
    }
    else if (secType == GroupControllerSectionTypeMembers) {
        
        count = self.members.count;
        if ([self userCanEditGroupInfo]) {
            
            //Add Members table cell
            count += 1;
        }
    }
    else {
        
        count = [super tableView:tableView numberOfRowsInSection:section];
    }
    
    return count;
}

@end
