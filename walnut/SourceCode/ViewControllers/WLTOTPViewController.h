//
//  WLTOTPViewController.h
//  walnut
//
//  Created by Abhinav Singh on 15/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"
#import "WLTOTPView.h"
#import "WLTLoginTempUser.h"

@interface WLTOTPViewController : WLTViewController {
    
    __weak IBOutlet WLTOTPView *otpView;
    __weak IBOutlet UILabel *infoLabel;
    
    __weak IBOutlet UILabel *timerLabel;
    __weak IBOutlet UILabel *timeDisplayLabel;
    
    __weak IBOutlet UIButton *resendSmsButton;
    __weak IBOutlet UIButton *resendCallButton;
    __weak IBOutlet NSLayoutConstraint *bottomMarginConstraint;
}

@property(nonatomic, strong) WLTLoginTempUser *userObject;
@property(nonatomic, strong) LoginBlock completion;

@end
