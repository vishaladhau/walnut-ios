//
//  WLTGroupContactsViewController.m
//  walnut
//
//  Created by Abhinav Singh on 02/09/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTGroupContactsViewController.h"
#import "WLTContact.h"
#import "WLTGroupContactTableViewCell.h"
#import "WLTCreateNewGroupViewController.h"
#import "WLTColorPalette.h"
#import "WLTNewGroupValidator.h"
#import "WLTGroupSettingsViewController.h"
#import "WLTContactsManager.h"

@interface WLTGroupContactsViewController ()

@end

@implementation WLTGroupContactsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [theTableView registerClass:[WLTGroupContactTableViewCell class] forCellReuseIdentifier:@"WLTGroupContactTableViewCell"];
    
    if (self.contactSelectionType == GroupContactSelectionTypeUnknown) {
        self.contactSelectionType = GroupContactSelectionTypeNew;
    }
    
    if (self.contactSelectionType == GroupContactSelectionTypeNew) {
        
        UIBarButtonItem *next = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleDone
                                                                target:self action:@selector(nextCicked:)];
        self.navigationItem.rightBarButtonItem = next;
    }
}

-(void)addNewContacts:(NSMutableArray*)result {
    
    if (!self.selectedContacts) {
        self.selectedContacts = [NSMutableArray new];
    }
    
    NSMutableArray *toAddObjects = [NSMutableArray new];
    NSArray *allNums = [result valueForKeyPath:@"mobile.number"];
    
    WLTContact *currentUser = nil;
    NSString *currenUserMob = [WLTDatabaseManager sharedManager].currentUser.mobileString;
    if ([allNums containsObject:currenUserMob]) {
        
        NSInteger index = [allNums indexOfObject:currenUserMob];
        currentUser = result[index];
    }
    else {
        
        WLTContact *cont = [[WLTContact alloc] initWithName:[WLTDatabaseManager sharedManager].currentUser.name andNumber:currenUserMob];
        currentUser = cont;
    }
    
    if (![self.selectedContacts containsObject:currentUser]) {
        [self.selectedContacts addObject:currentUser];
    }
    
    for ( WLTContact *cont in self.selectedContacts ) {
        
        NSInteger index = [allNums indexOfObject:cont.completeNumber];
        if (index == NSNotFound) {
            [toAddObjects addObject:cont];
        }
    }
    
    [result addObjectsFromArray:toAddObjects];
    
    [super addNewContacts:result];
    [self changeTitle];
}

-(void)changeTitle {
    
    if (self.selectedContacts.count > 1) {
        
        self.title = [NSString stringWithFormat:@"%@ Persons selected", @(self.selectedContacts.count).displayString];
    }
    else if (self.selectedContacts.count == 1) {
        
        self.title = @"1 Person selected";
    }else {
        
        self.title = @"Add People";
    }
}

-(void)viewWillAppear:(BOOL)animated {
    
    if (!self.selectedColorID) {
        
        [self showNewColorID:[[WLTColorPalette sharedPalette] randomGroupColorID]];
    }else {
        
        self.navBarColor = [[WLTColorPalette sharedPalette] colorForID:self.selectedColorID];
        theTableView.tintColor = self.navBarColor;
        
        [UIView animateWithDuration:0.25 animations:^{
            [self.navigationController.navigationBar setBarTintColor:self.navBarColor];
        }];
    }
    
    [theTableView reloadData];
    [self changeTitle];
    
    [super viewWillAppear:animated];
}

-(void)showNewColorID:(NSString*)colorID {
    
    if (!self.selectedColorID || ![self.selectedColorID isEqualToString:colorID]) {
        
        self.selectedColorID = colorID;
        self.navBarColor = [[WLTColorPalette sharedPalette] colorForID:self.selectedColorID];
        theTableView.tintColor = self.navBarColor;
        
        [UIView animateWithDuration:0.25 animations:^{
            [self.navigationController.navigationBar setBarTintColor:self.navBarColor];
        }];
    }
}

-(void)nextCicked:(UIBarButtonItem*)item {
    
    __weak typeof(self) weakSelf = self;
    
    if (self.contactSelectionType == GroupContactSelectionTypeNew) {
        
        WLTCreateNewGroupViewController *settings = [[WLTCreateNewGroupViewController alloc] init];
        settings.members = self.selectedContacts;
        settings.selectedColorID = self.selectedColorID;
        settings.navBarColor = self.navBarColor;
        [self.navigationController pushViewController:settings animated:YES];
        
        [settings setCompletion:^(BOOL success){
            
            if (success) {
                
                NSArray *allControllers = [weakSelf.navigationController allControllersUpTo:weakSelf exclude:YES];
                [weakSelf.navigationController setViewControllers:allControllers animated:YES];
            }
        }];
    }else if (self.contactSelectionType == GroupContactSelectionTypeEdit) {
        
        //        WLTGroupSettingsViewController *settings = [[WLTGroupSettingsViewController alloc] init];
        //        settings.members = self.selectedContacts;
        //        settings.toEdit = self.currentGroup;
        //        settings.navBarColor = self.navBarColor;
        //        [self.navigationController pushViewController:settings animated:YES];
        //
        //        [settings setCompletion:^(BOOL success){
        //
        //            if (success) {
        //
        //                NSArray *allControllers = [weakSelf.navigationController allControllersUpTo:weakSelf exclude:YES];
        //                [weakSelf.navigationController setViewControllers:allControllers animated:YES];
        //            }
        //        }];
    }
}

-(void)userSelectedContact:(WLTContact*)contact atIndexPath:(NSIndexPath*)indexPath{
    
    if (![contact.mobile.number isEqualToString:CURRENT_USER_MOB]) {
        
        BOOL reloadTable = NO;
        if ([self.selectedContacts containsObject:contact]) {
            
            [self.selectedContacts removeObject:contact];
        }else {
            
            [self.selectedContacts addObject:contact];
            
            if (![self.contactsArray containsObject:contact]) {
                
                reloadTable = YES;
                
                [self.contactsArray addObject:contact];
                [self reloadIndexs];
            }
        }
        
        if (reloadTable) {
            [theTableView reloadData];
        }else {
            [theTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        
        if (self.selectedContacts.count >= 1) {
            self.navigationItem.rightBarButtonItem.enabled = YES;
        }else {
            self.navigationItem.rightBarButtonItem.enabled = NO;
        }
        
        [self changeTitle];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    id <User>contact = [self contactAtIndexPath:indexPath];
    
    static NSString *cellIndentifier = @"WLTGroupContactTableViewCell";
    
    WLTGroupContactTableViewCell *userCell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    [userCell showInformation:contact];
    [userCell setAccessoryColor:self.navBarColor];
    
    if ([self.selectedContacts containsObject:contact]) {
        
        [userCell showSelectedContact:YES];
    }else {
        
        [userCell showSelectedContact:NO];
    }
    
    return userCell;
}

@end
