//
//  WLTGroupMembersViewController.m
//  walnut
//
//  Created by Abhinav Singh on 6/9/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTGroupMembersViewController.h"
#import "WLTUser.h"
#import "WLTAdminTableViewCell.h"

@interface WLTGroupMembersViewController ()

@end

@implementation WLTGroupMembersViewController

-(void)viewDidLoad {
	
    [super viewDidLoad];
	
	allMembers = self.group.members.allObjects;
	
    [self setMembersTitleForGroup:self.group checkDirect:NO];
    
	theTableView.rowHeight = UITableViewAutomaticDimension;
	theTableView.separatorInset = UIEdgeInsetsZero;
	[theTableView registerClass:[WLTContactTableViewCell class] forCellReuseIdentifier:@"WLTContactTableViewCell"];
	[theTableView registerClass:[WLTAdminTableViewCell class] forCellReuseIdentifier:@"WLTAdminTableViewCell"];
	
	UIView *footer = [[UIView alloc] initWithFrame:CGRectZero];
	theTableView.tableFooterView = footer;
}

#pragma mark - UITableViewDelegate & UITableViewDataSource

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSString *cellIndentifier = @"WLTContactTableViewCell";
	WLTUser *cont = allMembers[indexPath.row];
	if ([cont.mobileString isEqualToString:self.group.owner.mobileString]) {
		cellIndentifier = @"WLTAdminTableViewCell";
	}
	
	WLTContactTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    [cell showInformation:cont];
	
	return cell;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	return 70;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	return allMembers.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	
	return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (self.completionBlock) {
		self.completionBlock(allMembers[indexPath.row]);
	}
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
