//
//  WLTKeyValueAmountViewController.m
//  walnut
//
//  Created by Abhinav Singh on 02/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTKeyValueAmountViewController.h"
#import "WLTKeyValueTableViewCell.h"

@interface WLTKeyValueAmountViewController ()

@end

@implementation WLTKeyValueAmountViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectZero];
    [theTableView registerNib:[UINib nibWithNibName:@"WLTKeyValueTableViewCell" bundle:nil] forCellReuseIdentifier:@"WLTKeyValueTableViewCell"];
    theTableView.backgroundColor = self.view.backgroundColor;
    theTableView.tableFooterView = footer;
    theTableView.separatorInset = UIEdgeInsetsZero;
}

#pragma mark - UITableViewDelegate & UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    WLTKeyValueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WLTKeyValueTableViewCell"];
    cell->keyLabel.font = [UIFont sfUITextMediumOfSize:16];
    cell->keyLabel.textColor = [UIColor cadetColor];
    
    cell->valueLabel.font = [UIFont sfUITextRegularOfSize:18];
    cell->valueLabel.textColor = [UIColor getColor];
    
    
    NSDictionary *dict = self.dataArray[indexPath.row];
    [cell setKey:dict[@"key"] forValue:dict[@"value"]];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    
    return 44;
}

//-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
//    
//    return 50;
//}


@end
