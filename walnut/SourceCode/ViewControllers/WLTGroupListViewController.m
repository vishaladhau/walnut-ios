//
//  WLTGroupListViewController.m
//  walnut
//
//  Created by Abhinav Singh on 12/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTGroupListViewController.h"
#import "WLTGroupCollectionViewCell.h"
#import "WLTGroupDetailViewController.h"
#import "WLTHomeViewController.h"
#import "WLTExpenseViewController.h"
#import "WLTGroupSettingsViewController.h"
#import "WLTColorPalette.h"
#import "WLTHomeScreenAnimator.h"
#import "WLTHomeHeaderButton.h"
#import "WLTCombinedGroupPayments.h"
#import "WLTCombinedGroupPaymentsViewController.h"
#import "WLTTransactionUser.h"
#import "WLTContact.h"
#import "WLTContactsManager.h"
#import "WLTCreateNewGroupViewController.h"
#import "WLTGroupsManager.h"
#import "WLTContactsViewController.h"
#import "WLTGroupContactsViewController.h"

CGFloat cellHGap = 10;
CGFloat cellVGap = 10;
CGFloat cellHeight = 158;

@interface WLTGroupListViewController ()<WLTEmptyStateViewDelegate, WLTGroupCollectionViewCellDelegate, UINavigationControllerDelegate>{
    
    UIEdgeInsets groupCellInsets;
    
    __weak IBOutlet WLTHomeHeaderButton *oweButton;
    __weak IBOutlet WLTHomeHeaderButton *getButton;
    
    NSNumber *totalOwe;
    NSNumber *totalGet;
    
    WLTHomeScreenAnimator *currentAnimator;
}

@property(nonatomic, strong) NSMutableArray *changedGroupIDs;

@end

@implementation WLTGroupListViewController

-(UITabBarItem *)tabBarItem {
    
    if (!tabItem) {
        
        UITabBarItem *item = [[UITabBarItem alloc] initWithTitle:@"Groups" image:[[UIImage imageNamed:@"SpendsIconTabUnSelected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"SpendsIconTabSelected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        tabItem = item;
    }
    
    return tabItem;
}

-(BOOL)needNavigationBar {
    
    return NO;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.changedGroupIDs = [NSMutableArray new];
    
    groupCellInsets = GroupCellImageAlphaInsets;
	fakeNavbarHeight.constant = HomeScreensNavbarHeight;
	
    [self setEdgesForExtendedLayout:UIRectEdgeAll];
    
    [fakeNavigationBarView addBlurBackgroundDark:NO];
    [fakeNavigationBarView addBorderLineAtPosition:BorderLinePositionBottom];
    
    WLTEmptyStateView *emptyView = [WLTEmptyStateView emptyStateViewWithStyle:WLTEmptyStateViewStyleWithAction andDelegate:self];
    emptyView.translatesAutoresizingMaskIntoConstraints = NO;
    [emptyView showTitle:@"Let’s create a group!" andSubtitle:@"Create your first group and split restaurant bills, groceries, house rent, utilities, cab fares, movie tickets etc." imageName:@"NoGroupsImage"];
    [self.view addSubview:emptyView];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(emptyView);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[emptyView]-0-|" options:0 metrics:nil views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[emptyView]-0-|" options:0 metrics:nil views:dict]];
    
    errorView = emptyView;
    
    [errorView setActionButtonTitle:@"Add New Group"];
    
    [self fetchDBGroups];
    [self refreshGroupData:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(groupChanged:) name:NotifyGroupChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(groupDeleted:) name:NotifyGroupDeleted object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteAndRefetchNewGroups:) name:NotifyUserMobileChanged object:nil];
    
    UIRefreshControl *control = [[UIRefreshControl alloc] initWithFrame:CGRectZero];
    control.tintColor = self.navigationController.navigationBar.barTintColor;
    [control sizeToFit];
    [control addTarget:self action:@selector(refreshGroupData:) forControlEvents:UIControlEventValueChanged];
    [theScrollView addSubview:control];
    
    theScrollView.bounces = YES;
    [theScrollView setContentInset:UIEdgeInsetsMake(HomeScreensNavbarHeight, 0, 49, 0)];
    theScrollView.alwaysBounceVertical = YES;
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if (self.changedGroupIDs.count) {
        
        __weak typeof(self) weakSelf = self;
        [[WLTGroupsManager sharedManager] fetchEveryChangeOfGroupsIDS:self.changedGroupIDs completion:^(NSArray *changedGrps, NSError *error) {
            
            if (!error) {
                
                for ( WLTGroup *grpsChanged in changedGrps ) {
                    [weakSelf.changedGroupIDs removeObject:grpsChanged.groupUUID];
                }
                
                [weakSelf fetchDBGroups];
            }
        }];
    }
}

-(void)calculatePaymentsForGroups:(NSArray*)grps {
    
    CGFloat totalOweVal = 0;
    CGFloat totalGetVal = 0;
    
    NSMutableDictionary *mappingShared = [NSMutableDictionary new];
    NSMutableDictionary *mappingPrivate = [NSMutableDictionary new];
    
    for ( WLTGroup *grp in grps ) {
        
        NSMutableDictionary *mappingDict = nil;
        if (grp.isPrivate.boolValue) {
            mappingDict = mappingPrivate;
        }else {
            mappingDict = mappingShared;
        }
        
        void (^ processUser)(WLTTransactionUser *user) = ^void (WLTTransactionUser *user){
            
            NSString *userM = user.mobileString;
            WLTCombinedGroupPayments *grpPays = mappingDict[userM];
            if (!grpPays) {
                grpPays = [[WLTCombinedGroupPayments alloc] initWithUser:user];
                mappingDict[userM] = grpPays;
            }
            
            [grpPays addUserData:user forGroup:grp];
        };
        
        for ( WLTTransactionUser *user in grp.userPayTo ) {
            processUser(user);
        }
        for ( WLTTransactionUser *user in grp.userGetFrom ) {
            processUser(user);
        }
    }
    
    for ( WLTCombinedGroupPayments *grpAll in mappingShared.allValues) {
        [grpAll refreshCombinedData];
        if (grpAll.transactionState == UserTransactionStateGet) {
            
            totalGetVal += grpAll.amount;
        }
        else if (grpAll.transactionState == UserTransactionStateOwe) {
            
            totalOweVal += grpAll.amount;
        }
    }
    
    for ( WLTCombinedGroupPayments *grpAll in mappingPrivate.allValues) {
        [grpAll refreshCombinedData];
        if (grpAll.transactionState == UserTransactionStateGet) {
            
            totalGetVal += grpAll.amount;
        }
        else if (grpAll.transactionState == UserTransactionStateOwe) {
            
            totalOweVal += grpAll.amount;
        }
    }
    
    totalGet = @(totalGetVal);
    totalOwe = @(totalOweVal);
    
    [oweButton setValue:totalOwe withState:UserTransactionStateOwe];
    [getButton setValue:totalGet withState:UserTransactionStateGet];
}

-(void)showErrorLabel:(BOOL)show {
    
    errorView.hidden = !show;
    theScrollView.hidden = show;
}

-(WLTGroupCollectionViewCell*)cellForGroup:(WLTGroup*)grp {
    
    WLTGroupCollectionViewCell *retCell = nil;
    
    for ( WLTGroupCollectionViewCell *cell in theScrollView.subviews ) {
        
        if ([cell isKindOfClass:[WLTGroupCollectionViewCell class]]) {
            if ([cell.displayedGroup.groupUUID isEqualToString:grp.groupUUID]) {
                if(CGRectIntersectsRect(theScrollView.bounds, cell.frame)) {
                    retCell = cell;
                }
                
                break;
            }
        }
    }
    
    return retCell;
}

-(void)refreshGroupData:(UIRefreshControl*)control {
    
    if (control) {
        [control beginRefreshing];
    }else {
        [self startLoadingWithColor:nil];
    }
    
    [self showErrorLabel:NO];
    
    __weak WLTGroupListViewController *weakSelf = self;
    [[WLTGroupsManager sharedManager] fetchAllChangedGroups:^(NSArray *data, NSError *error) {
        
        [weakSelf fetchDBGroups];
        [weakSelf endViewLoading];
        [control endRefreshing];
    }];
}

-(void)deleteAndRefetchNewGroups:(NSNotification*)notify {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    [self fetchDBGroups];
    [self refreshGroupData:nil];
}

-(void)groupDeleted:(NSNotification*)notify {
    
	dispatch_async(dispatch_get_main_queue(), ^{
        
		[self fetchDBGroups];
	});
}

-(void)groupChanged:(NSNotification*)notify {
	
    WLTGroup *changed = notify.object;
    if (changed && [changed isKindOfClass:[WLTGroup class]]) {
        if ([self.changedGroupIDs containsObject:changed.groupUUID]) {
            [self.changedGroupIDs addObject:changed.groupUUID];
        }
    }
    
	dispatch_async(dispatch_get_main_queue(), ^{
        
		[self fetchDBGroups];
	});
}

-(void)fetchDBGroups {
	
	NSSortDescriptor *sortUpdated = [NSSortDescriptor sortDescriptorWithKey:@"sortingTime" ascending:NO];
    self.groupArrays = [[WLTDatabaseManager sharedManager] allObjectsOfClass:@"WLTGroup" withPredicate:nil andSortDescriptors:@[sortUpdated]];
    
	[self reloadScrollViewData];
	[self calculatePaymentsForGroups:self.groupArrays];
}

-(void)addNewGroupClicked:(id)sender {
    
    WLTGroupContactsViewController *cont = [[WLTGroupContactsViewController alloc] initWithNibName:@"WLTContactsViewController" bundle:nil];
    [self.navigationController pushViewController:cont animated:YES];
}

-(LSButton*)newCreateGroupButton {
    
    LSButton *btn = [[LSButton alloc] initWithFrame:CGRectZero];
    btn.layer.cornerRadius = DefaultCornerRadius;
    btn.translatesAutoresizingMaskIntoConstraints = NO;
    [btn addTarget:self action:@selector(addNewGroupClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitle:@"Add New Group"];
    
    LSStateColor *nrmlClr = [[LSStateColor alloc] initWithBack:[UIColor whiteColor]
                                                  contentColor:[UIColor appleGreenColor]
                                                  andBorderClr:[UIColor appleGreenColor]];
    
    LSStateColor *highClr = [[LSStateColor alloc] initWithBack:[UIColor appleGreenColor]
                                                  contentColor:[UIColor whiteColor]
                                                  andBorderClr:nil];
    
    [btn addColors:nrmlClr forState:LSButtonStateNormal];
    [btn addColors:highClr forState:LSButtonStateHighlighted];
    
    return btn;
}

-(void)reloadScrollViewData {
    
    NSMutableArray *toRemove = [NSMutableArray new];
    
    for ( WLTGroupCollectionViewCell *cell in theScrollView.subviews ) {
        
        if ([cell isKindOfClass:[WLTGroupCollectionViewCell class]]) {
            [toRemove addObject:cell];
        }
    }
    
    [theScrollView removeConstraints:theScrollView.constraints];
    
    if (self.groupArrays.count) {
        
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        
        CGFloat hGap = (cellHGap-groupCellInsets.left);
        CGFloat vGap = (cellHGap-(groupCellInsets.top+groupCellInsets.bottom));
        CGFloat grpWidth = (screenSize.width-(hGap*2));
        
        NSDictionary *metrics = @{@"hGap":@(hGap), @"vGap":@(vGap), @"width":@(grpWidth), @"height":@(cellHeight)};
        
        if (!addNewGroupButton) {
            LSButton *btn = [self newCreateGroupButton];
            [theScrollView addSubview:btn];
            addNewGroupButton = btn;
        }
        
        NSDictionary *dict = NSDictionaryOfVariableBindings(addNewGroupButton);
        [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[addNewGroupButton(==44)]", cellVGap] options:0 metrics:nil views:dict]];
        [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%f-[addNewGroupButton]-%f-|", cellHGap, cellHGap] options:0 metrics:nil views:dict]];
        
        UIView *lastAdded = nil;
        for ( WLTGroup *grps in self.groupArrays ) {
            
            WLTGroupCollectionViewCell *cellPrev = [toRemove firstObject];
            if (!cellPrev) {
                
                cellPrev = [WLTGroupCollectionViewCell initWithDefaultXib];
                [cellPrev addTarget:self action:@selector(groupSelected:) forControlEvents:UIControlEventTouchUpInside];
                cellPrev.delegate = self;
                cellPrev.translatesAutoresizingMaskIntoConstraints = NO;
                [theScrollView addSubview:cellPrev];
            }else {
                
                [toRemove removeObject:cellPrev];
            }
            
            [cellPrev showInfoOfGroup:grps];
            
            NSDictionary *dict = NSDictionaryOfVariableBindings(cellPrev);
            [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-hGap-[cellPrev(==width)]-hGap-|" options:0 metrics:metrics views:dict]];
            
            if (lastAdded) {
                dict = NSDictionaryOfVariableBindings(cellPrev, lastAdded);
                [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lastAdded]-vGap-[cellPrev(==height)]" options:0 metrics:metrics views:dict]];
            }else {
                [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[cellPrev(==height)]", (64-groupCellInsets.top)] options:0 metrics:metrics views:dict]];
            }
            
            lastAdded = cellPrev;
        }
        
        if (lastAdded) {
            dict = NSDictionaryOfVariableBindings(lastAdded);
            [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lastAdded]-vGap-|" options:0 metrics:metrics views:dict]];
        }
    }
    else {
        
        [addNewGroupButton removeFromSuperview];
        addNewGroupButton = nil;
    }
    
    [toRemove makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if (!self.groupArrays.count) {
        [self showErrorLabel:YES];
    }else {
        
        [self showErrorLabel:NO];
    }
}

-(void)showDetailsControllerForGroup:(WLTGroup*)grp {
    
    WLTGroupCollectionViewCell *cell = [self cellForGroup:grp];
    if (cell) {
        
        CGRect overMe = [self.view convertRect:cell.frame fromView:theScrollView];
        
        if (overMe.origin.y < fakeNavigationBarView.height) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                CGRect toShow = cell.frame;
                toShow.origin.y -= 10;
                
                [theScrollView scrollRectToVisible:toShow animated:NO];
            });
        }
    }
    
    WLTGroupDetailViewController *details = nil;
    if ([self.navigationController.topViewController isKindOfClass:[WLTGroupDetailViewController class]]) {
        
        WLTGroupDetailViewController *detailCont = (WLTGroupDetailViewController*)self.navigationController.topViewController;
        if ([detailCont.currentGroup.groupUUID isEqualToString:grp.groupUUID]) {
            details = detailCont;
        }
    }
    
    if (!details) {
        
        details = [self controllerWithIdentifier:@"WLTGroupDetailViewController"];
        details.hidesBottomBarWhenPushed = YES;
        details.currentGroup = grp;
        details.navBarColor = [[WLTColorPalette sharedPalette] colorForGroup:grp];
        [self.navigationController pushViewController:details animated:YES];
    }
    
    [self.tabBarController setSelectedIndex:0];
}

-(void)groupSelected:(WLTGroupCollectionViewCell*)cell {
    
    [self showDetailsControllerForGroup:cell.displayedGroup];
}

#pragma mark - WLTGroupCollectionViewCellDelegate

-(void)payButtonClickedFromCell:(WLTGroupCollectionViewCell*)cell {
    
    [self showCombinedTransactionsForGroups:@[cell.displayedGroup] andState:UserTransactionStateUnknown];
}

-(void)reminderButtonClickedFromCell:(WLTGroupCollectionViewCell*)cell {
    
    [self showCombinedTransactionsForGroups:@[cell.displayedGroup] andState:UserTransactionStateUnknown];
}

-(void)addButtonClickedFromCell:(WLTGroupCollectionViewCell*)cell {
    
    WLTExpenseViewController *cont = [self controllerWithIdentifier:@"WLTExpenseViewController"];
    cont.hidesBottomBarWhenPushed = YES;
    cont.navBarColor = [[WLTColorPalette sharedPalette] colorForGroup:cell.displayedGroup];
    cont.group = cell.displayedGroup;
    [self.navigationController pushViewController:cont animated:YES];
    
    __weak WLTGroupListViewController *weakCont = self;
    
    [cont setCompletion:^(BOOL success){
        
        if (success) {
            NSArray *allConts = [weakCont.navigationController allControllersUpTo:weakCont];
            [weakCont.navigationController setViewControllers:allConts animated:YES];
        }
    }];
}

#pragma mark

- (nullable id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                            animationControllerForOperation:(UINavigationControllerOperation)operation
                                                         fromViewController:(UIViewController *)fromVC
                                                           toViewController:(UIViewController *)toVC {
    
    if (([fromVC isEqual:self] && [toVC isKindOfClass:[WLTGroupDetailViewController class]]) || ([toVC isEqual:self] && [fromVC isKindOfClass:[WLTGroupDetailViewController class]])) {
        
        WLTGroupDetailViewController *grpDetail = nil;
        if ([toVC isKindOfClass:[WLTGroupDetailViewController class]]) {
            grpDetail = (WLTGroupDetailViewController*)toVC;
        }else {
            grpDetail = (WLTGroupDetailViewController*)fromVC;
        }
        
        currentAnimator = [[WLTHomeScreenAnimator alloc] initWithGroupCollectionCell:[self cellForGroup:grpDetail.currentGroup]];
        return currentAnimator;
    }
    
    return nil;
}

-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    if (!currentAnimator) {
        [super navigationController:navigationController willShowViewController:viewController animated:animated];
    }
    
    currentAnimator = nil;
}

#pragma mark - Actions

-(void)showSettleScreenForUserMobile:(NSString*)mobileNumber userName:(NSString*)name andGroup:(NSString*)grpID{
    
    WLTContact *cont = [[WLTContact alloc] initWithName:name andNumber:mobileNumber];
    
    __weak typeof(self) weakSelf = self;
    [[WLTContactsManager sharedManager] displayNameForUser:cont defaultIsMob:NO completion:^(NSString *data) {
        
        cont.fullName = data;
        [weakSelf showCombinedTransactionsForUser:cont privacyStatus:WLTCombinedGroupStatusBoth];
        
        [weakSelf.tabBarController setSelectedIndex:0];
    }];
}

-(void)showSettleScreen{
    
    if (self.isLoading) {
        return;
    }
    
    [self showCombinedTransactionsForGroups:self.groupArrays andState:UserTransactionStateOwe];
}

-(IBAction)oweButtonClicked:(id)sender {
    
    [self showSettleScreen];
}

-(IBAction)getButtonClicked:(id)sender {
    
    if (self.isLoading) {
        return;
    }
    
    [self showCombinedTransactionsForGroups:self.groupArrays andState:UserTransactionStateGet];
}

#pragma mark - WLTEmptyStateViewDelegate

-(void)emptyStateViewReloadScreen:(WLTEmptyStateView*)view {
    
    [self refreshGroupData:nil];
}

-(void)emptyStateViewActionButtonTapped:(WLTEmptyStateView*)view {
    
    [self addNewGroupClicked:nil];
}

@end
