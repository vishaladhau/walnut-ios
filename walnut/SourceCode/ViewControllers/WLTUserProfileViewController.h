//
//  WLTUserProfileViewController.h
//  walnut
//
//  Created by Abhinav Singh on 02/03/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "KeyboardHandlerViewController.h"
#import "WLTTextField.h"

@interface WLTUserProfileViewController : KeyboardHandlerViewController <UITableViewDataSource, UITableViewDelegate>{
    
    __weak UITableView *theTableView;
    
    __weak UITextField *nameField;
    __weak UITextField *cellNumberField;
    __weak UITextField *emailAddressTextField;
}

@property(nonatomic, strong) SuccessBlock completion;

@end
