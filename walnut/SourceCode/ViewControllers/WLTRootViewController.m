//
//  WLTRootViewController.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 02/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTRootViewController.h"
#import "WLTOnBoardingViewController.h"
#import "WLTHomeViewController.h"
#import "WLTNetworkManager.h"
#import "WLTDatabaseManager.h"
#import "WLTGroupListViewController.h"
#import "WLTContactsManager.h"
#import "WLTContact.h"
#import "WLTPaymentTransationsManager.h"
#import "WLTGroupsManager.h"
#import "WLTProfilesSelectionView.h"
#import "WLTLoginTempUser.h"
#import "WLTPaymentTransaction.h"

NSString *const RemoteNotificationTypeGroupAdded = @"group_added";
NSString *const RemoteNotificationTypeGroupModified = @"group_modified";

NSString *const RemoteNotificationTypeTransAdded = @"txn_add";
NSString *const RemoteNotificationTypeTransDeleted = @"txn_delete";

NSString *const RemoteNotificationTypeCommentAdded = @"comment_add";
NSString *const RemoteNotificationTypeGroupSetteled = @"settled";
NSString *const RemoteNotificationTypeSettelReminder = @"settle_reminder";

NSString *const RemoteNotificationTypeNewPayment = @"new_payment";
NSString *const RemoteNotificationSubTypeNoDefaultInstrument = @"receiver_no_default_instrument";
NSString *const RemoteNotificationSubTypeMoneyReceived = @"money_received_successfully";
NSString *const RemoteNotificationTypeRequestDeleted = @"delete_request";
NSString *const RemoteNotificationTypeRequestMoney = @"request_money";

static const NSTimeInterval ReminderTime = (4*60*60); //4 Hrs

@interface WLTRootViewController () {
    
}

@property(nonatomic, weak) UIViewController *currentController;

@end

@implementation WLTRootViewController

-(void)showHomeController {
    
    WLTHomeViewController *homeCont = [self controllerWithIdentifier:@"WLTHomeViewController"];
    [self addController:homeCont replacing:self.currentController transactionStyle:TransactionDirectionTop];
    
    self.homeController = homeCont;
    self.currentController = homeCont;
}

-(void)showOnboardingController {
    
    WLTOnBoardingViewController *onboard = [self controllerWithIdentifier:@"WLTOnBoardingViewController"];
    UINavigationController *navCont = [[UINavigationController alloc] initWithRootViewController:onboard];
    navCont.delegate = onboard;
    [navCont applyTransparentStyle];
    
    [self addController:navCont replacing:self.currentController transactionStyle:TransactionDirectionNone];
    
    self.currentController = navCont;
    
    __weak WLTRootViewController *wealSelf = self;
    
    [onboard setCompletionBlock:^(WLTLoginTempUser *user){
        
        if (user) {
            [wealSelf setProfileForUser:user];
        }
    }];
}

-(void)setProfileForUser:(WLTLoginTempUser*)user {
    
    __weak typeof(self) weakSelf = self;
    
    GTLWalnutMUserProfile *newProfile = [[GTLWalnutMUserProfile alloc] init];
    newProfile.deviceUuid = user.deviceUUID;
    newProfile.deviceName = [[UIDevice currentDevice] name];
    
    [[WLTNetworkManager sharedManager] setUPUserProfile:newProfile forUser:user withCompletion:^(id data) {
        
        if ([data isKindOfClass:[NSError class]]) {
            
            [weakSelf showAlertForError:data];
            
            if ([weakSelf.currentController isKindOfClass:[UINavigationController class]]) {
                [(UINavigationController*)weakSelf.currentController popToRootViewControllerAnimated:YES];
            }
        }else {
            
            WLTDatabaseManager *dbManager = [WLTDatabaseManager sharedManager];
            
            WLTLoggedInUser *loggedIn = [dbManager objectOfClassString:@"WLTLoggedInUser"];
            loggedIn.wltAuthToken = user.authToken;
            loggedIn.name = user.userName;
            loggedIn.mobileString = user.mobileNumber;
            loggedIn.email = user.emailAddress;
            
            dbManager.currentUser = loggedIn;
            
            dbManager.appSettings.deviceIdentifier = user.deviceUUID;
            [dbManager updatePaymentConfigration:user.paymentConfig];
            
            dbManager.appSettings.mobileVerified = @(YES);
            
            [dbManager saveMainDataBase];
            [[WLTNetworkManager sharedManager] refreshAuthState];
            [weakSelf showHomeController];
        }
    }];
}

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor lightBackgroundColor];
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont appBarButtonFont]} forState:UIControlStateNormal];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionExpired:) name:NotifyGmailSessionExpired object:nil];
    
    if ([WLTDatabaseManager sharedManager].currentUser.wltAuthToken.length && [WLTDatabaseManager sharedManager].appSettings.deviceIdentifier.length) {
        
        [self showHomeController];
    }else {
        
        [self showOnboardingController];
    }
}

-(void)logoutCurrentUserAndShowOnBoarding {
    
    [[WLTDatabaseManager sharedManager] logOut];
    
    WLTOnBoardingViewController *onboard = [self controllerWithIdentifier:@"WLTOnBoardingViewController"];
    UINavigationController *navCont = [[UINavigationController alloc] initWithRootViewController:onboard];
    navCont.delegate = onboard;
    [navCont applyTransparentStyle];
    [self addController:navCont replacing:self.currentController transactionStyle:TransactionDirectionNone];
    
    self.currentController = navCont;
    
    __weak WLTRootViewController *wealSelf = self;
    [onboard setCompletionBlock:^(WLTLoginTempUser *user){
        
        if (user) {
            [wealSelf setProfileForUser:user];
        }
    }];
}

-(void)sessionExpired:(NSNotificationCenter*)notify {
	
	[self logoutCurrentUserAndShowOnBoarding];
}

-(UIViewController*)visibleTopViewController {
    
    UIViewController *onTop = nil;
    UIViewController *cont = [self.homeController visibleController];
    if (cont.presentedViewController) {
        onTop = cont.presentedViewController;
    }else {
        onTop = cont;
    }
    
    return onTop;
}

-(void)handleLocalNotification:(NSDictionary*)pushDict showAlert:(BOOL)showAlert{
    
    if (showAlert) {
        
        [self handleRemoteNotification:pushDict fetchHandler:nil];
    }else {
        
        NSString *transactionUUID = [pushDict[@"transaction_uuid"] nullCheck];
        if(transactionUUID.length) {
            
            [[self.homeController visibleController] showAddCardForTransactionID:transactionUUID];
        }
    }
}

-(void)handleRemoteNotification:(NSDictionary*)pushDict fetchHandler:(void (^)(UIBackgroundFetchResult))handler{
    
    __weak WLTRootViewController *weakSelf = self;
    
    void (^ showAlertPopupReminder)(NSString *message, NSString *mobile, NSString *name, NSString *grpID) = ^void (NSString *message, NSString *mobile, NSString *name, NSString *grpID){
        
        WLTContact *cont = [[WLTContact alloc] initWithName:name andNumber:mobile];
        
        __weak typeof(self) weakSelf = self;
        [[WLTContactsManager sharedManager] displayNameForUser:cont defaultIsMob:NO completion:^(NSString *data) {
            
            cont.fullName = data;
            
            UIAlertController *controller = [UIAlertController alertControllerWithTitle:message
                                                                                message:nil
                                                                         preferredStyle:UIAlertControllerStyleAlert];
            
            if (PaymentsEnabled()) {
                
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Later"
                                                                       style:UIAlertActionStyleDefault
                                                                     handler:nil];
                [controller addAction:cancelAction];
                
                UIAlertAction *settleAction = [UIAlertAction actionWithTitle:@"Pay"
                                                                       style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                                         
                                                                         [weakSelf.homeController.groupListController showSettleScreenForUserMobile:mobile userName:name andGroup:grpID];
                                                                     }];
                
                [controller addAction:settleAction];
            }else {
                
                UIAlertAction *settleAction = [UIAlertAction actionWithTitle:@"Okay"
                                                                       style:UIAlertActionStyleDefault
                                                                     handler:nil];
                
                [controller addAction:settleAction];
            }
            
            [weakSelf presentViewController:controller animated:YES completion:nil];
        }];
    };
    
    NSString *grpID = [pushDict[@"group_uuid"] nullCheck];
    NSString *notifyMesage = [pushDict[@"notification_msg"] nullCheck];
    NSString *pushCommand = [pushDict[@"push_command"] nullCheck];
    NSString *pushSubCommand = [pushDict[@"push_subcommand"] nullCheck];
    NSString *transactionUUID = [pushDict[@"transaction_uuid"] nullCheck];
    
    UIApplicationState appState = [[UIApplication sharedApplication] applicationState];
    
//    UIApplicationStateInactive User Clicked app notification.
//    In this case redirect user directly to the relavent screen without showing any alert messages. [He Knows what he is doing]
//    In case of add card reminder show alert anyways
    
//    UIApplicationStateActive User Already Using App.
//    Refresh data and show Badge or something don't Show popups for any changes Except (Settle Reminder and Add Card Popups)
    
    void (^ refreshTransactions)() = ^void (){
        
        __weak WLTViewController *topCont = (WLTViewController*)[weakSelf visibleTopViewController];
        if ([topCont isKindOfClass:[WLTViewController class]]) {
            
            [topCont startLoading];
            [[WLTPaymentTransationsManager sharedManager] fetchAllPaymentTransactionsCompletion:^(BOOL success, NSError *error) {
                
                if (handler) {
                    handler(UIBackgroundFetchResultNewData);
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:PaymentTransactionsChanged object:nil];
                [topCont endViewLoading];
            }];
        }
    };
    
    void (^ refreshChangedGroups)() = ^void (){
        
        [[WLTGroupsManager sharedManager] fetchAllChangedGroups:^(NSArray *data, NSError *error) {
            
        }];
    };
    
    BOOL payments = PaymentsEnabled();
    
    if (payments && [pushSubCommand isEqualToString:RemoteNotificationSubTypeNoDefaultInstrument]) {
        
        refreshTransactions();
        refreshChangedGroups();
        
        //This case dosen't respect application state i.e if app is already open or in background show alert message.
        if (notifyMesage.length && transactionUUID.length) {
            
            BOOL showAlert = NO;
            if (appState == UIApplicationStateActive) {
                showAlert = YES;
            }
            
            if (showAlert) {
                
                UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"WalnutPay"
                                                                                    message:notifyMesage
                                                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *addAction = [UIAlertAction actionWithTitle:@"Add DEBIT Card"
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * _Nonnull action) {
                                                                      
                                                                      [[weakSelf.homeController visibleController] showAddCardForTransactionID:transactionUUID];
                                                                  }];
                
                UIAlertAction *details = [UIAlertAction actionWithTitle:@"View Details"
                                                                  style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * _Nonnull action) {
                                                                    
                                                                    [[weakSelf.homeController visibleController] showDetailsOfPaymentTransactionID:transactionUUID];
                                                                }];
                
                UIAlertAction *laterAction = [UIAlertAction actionWithTitle:@"Remind me later"
                                                                      style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                                        
                                                                        UILocalNotification *notify = [[UILocalNotification alloc] init];
                                                                        notify.userInfo = pushDict;
                                                                        notify.soundName = UILocalNotificationDefaultSoundName;
                                                                        notify.alertBody = notifyMesage;
                                                                        notify.alertTitle = @"Add DEBIT Card";
                                                                        notify.fireDate = [[NSDate date] dateByAddingTimeInterval:ReminderTime];
                                                                        
                                                                        [[UIApplication sharedApplication] scheduleLocalNotification:notify];
                                                                    }];
                
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No, thanks"
                                                                       style:UIAlertActionStyleDestructive
                                                                     handler:nil];
                
                
                [controller addAction:addAction];
                [controller addAction:details];
                [controller addAction:laterAction];
                [controller addAction:cancelAction];
                
                [self presentViewController:controller animated:YES completion:nil];
            }
            else {
                
                if (![self isViewLoaded]) {
                    //Hack to load view.
                    self.view.backgroundColor = [UIColor whiteColor];
                }
                
                [[weakSelf.homeController visibleController] showDetailsOfPaymentTransactionID:transactionUUID];
            }
        }
    }
    else if (transactionUUID.length && payments) {
        
        refreshTransactions();
        refreshChangedGroups();
        
        //This case dosen't respect application state i.e if app is already open or in background show alert message.
        if (notifyMesage.length && transactionUUID.length) {
            
            BOOL showAlert = NO;
            if (appState == UIApplicationStateActive) {
                showAlert = YES;
            }
            
            if ([pushCommand isEqualToString:RemoteNotificationTypeRequestMoney]) {
                
                if (showAlert) {
                    
                    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"WalnutPay"
                                                                                        message:notifyMesage
                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *addAction = [UIAlertAction actionWithTitle:@"Not Now"
                                                                        style:UIAlertActionStyleCancel
                                                                      handler:nil];
                    [controller addAction:addAction];
                    
                    UIAlertAction *detailsAction = [UIAlertAction actionWithTitle:@"View Details"
                                                                        style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                                          [[weakSelf.homeController visibleController] showDetailsOfPaymentTransactionID:transactionUUID];
                                                                      }];
                    [controller addAction:detailsAction];
                    
                    UIAlertAction *details = [UIAlertAction actionWithTitle:@"Pay"
                                                                      style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                                        
                                                                        [[weakSelf.homeController visibleController] showMakePaymentScreenForTransactionID:transactionUUID];
                                                                    }];
                    
                    [controller addAction:details];
                    [self presentViewController:controller animated:YES completion:nil];
                }
                else {
                    
                    if (![self isViewLoaded]) {
                        //Hack to load view.
                        self.view.backgroundColor = [UIColor whiteColor];
                    }
                    
                    [[weakSelf.homeController visibleController] showDetailsOfPaymentTransactionID:transactionUUID];
                }
            }
            else if (showAlert) {
                
                UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"WalnutPay"
                                                                                    message:notifyMesage
                                                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *addAction = [UIAlertAction actionWithTitle:@"Okay"
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:nil];
                [controller addAction:addAction];
                
                if (![pushCommand isEqualToString:RemoteNotificationTypeRequestDeleted]) {
                    
                    UIAlertAction *details = [UIAlertAction actionWithTitle:@"View Details"
                                                                      style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                                        
                                                                        [[weakSelf.homeController visibleController] showDetailsOfPaymentTransactionID:transactionUUID];
                                                                    }];
                    
                    [controller addAction:details];
                }
                [self presentViewController:controller animated:YES completion:nil];
            }
            else {
                
                if (![self isViewLoaded]) {
                    //Hack to load view.
                    self.view.backgroundColor = [UIColor whiteColor];
                }
                
                if (![pushCommand isEqualToString:RemoteNotificationTypeRequestDeleted]) {
                    [[weakSelf.homeController visibleController] showDetailsOfPaymentTransactionID:transactionUUID];
                }
            }
        }
    }
    else if ([pushCommand isEqualToString:RemoteNotificationTypeSettelReminder]) {
        
        // If app is in active state show Alert Message else redirect directoly to settle screen.
        BOOL canShowAlert = NO;
        if (appState == UIApplicationStateActive ) {
            canShowAlert = YES;
        }
        
        WLTViewController *topCont = (WLTViewController*)[weakSelf.homeController.groupListController.navigationController topViewController];
        [topCont startLoading];
        
        [[WLTGroupsManager sharedManager] fetchAllChangedGroups:^(NSArray *changed, NSError *error) {
            
            if (!error) {
                
                NSString *templateString = [pushDict[@"new_template"] nullCheck];
                NSData *data = [templateString dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                NSDictionary *userDict = [[[json[@"title"] nullCheck][@"parameters"] nullCheck][@"user"] nullCheck];
                NSString *name = userDict[@"default_value"];
                NSString *mobile = [[userDict[@"value"] nullCheck][@"number"] nullCheck];
                
                if (mobile.length) {
                    
                    if (canShowAlert) {
                        
                        showAlertPopupReminder(notifyMesage, mobile, name, grpID);
                    }else{
                        
                        if (PaymentsEnabled()) {
                            
                            [weakSelf.homeController.groupListController showSettleScreenForUserMobile:mobile userName:name andGroup:grpID];
                        }
                    }
                }
                if (handler) {
                    
                    handler(UIBackgroundFetchResultNewData);
                }
            }else {
                if (handler) {
                    
                    handler(UIBackgroundFetchResultFailed);
                }
            }
            
            [topCont endViewLoading];
        }];
    }
    else if (grpID.length) {
        
        BOOL canRedirect = NO;
        if (appState == UIApplicationStateInactive ) {
            canRedirect = YES;
        }
        
        if (canRedirect) {
            [self.homeController.groupListController startLoading];
        }
        
        [[WLTGroupsManager sharedManager] fetchEveryChangeOfGroupsIDS:@[grpID] completion:^(NSArray *changed, NSError *error) {
            
            if (!error) {
                
                WLTGroup *toShow = nil;
                for ( WLTGroup *grpChanged in changed ) {
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:NotifyGroupChanged object:grpChanged];
                    if ([grpChanged.groupUUID isEqualToString:grpID]) {
                        toShow = grpChanged;
                    }
                }
                
                if (toShow && canRedirect) {
                    
                    //Check if its not already visible
                    if (![weakSelf.homeController isShowingDetailsOfGroup:toShow]) {
                        [weakSelf.homeController.groupListController showDetailsControllerForGroup:toShow];
                    }
                }
                
                [weakSelf.homeController.groupListController endViewLoading];
                if (handler) {
                    handler(UIBackgroundFetchResultNewData);
                }
            }
            else {
                if (handler) {
                    handler(UIBackgroundFetchResultFailed);
                }
            }
        }];
    }
    else if ([pushCommand isEqualToString:@"ping_update"]) {
        
        BOOL showAlert = NO;
        if (appState == UIApplicationStateActive) {
            showAlert = YES;
        }
        
        if (showAlert) {
            
            UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"WalnutPay"
                                                                                message:notifyMesage
                                                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *details = [UIAlertAction actionWithTitle:@"Not Now"
                                                              style:UIAlertActionStyleCancel
                                                            handler:nil];
            
            [controller addAction:details];
            
            UIAlertAction *addAction = [UIAlertAction actionWithTitle:@"Update"
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * _Nonnull action) {
                                                                  
                                                                  [weakSelf openiTunesURL];
                                                              }];
            [controller addAction:addAction];
            
            [self presentViewController:controller animated:YES completion:nil];
        }else {
            
            [weakSelf openiTunesURL];
        }
    }
    else {
        
        if (handler) {
            handler(UIBackgroundFetchResultNoData);
        }
    }
}

@end
