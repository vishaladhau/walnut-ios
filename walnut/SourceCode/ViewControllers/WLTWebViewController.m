//
//  WLTWebViewController.m
//  walnut
//
//  Created by Abhinav Singh on 13/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTWebViewController.h"
#import "WLTEmptyStateView.h"

@interface WLTWebViewController () <WLTEmptyStateViewDelegate> {
    
    __weak UIActivityIndicatorView *activity;
    __weak WLTEmptyStateView *errorView;
}

@end

@implementation WLTWebViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self addAllSubviews];
    [self addAllConstraints];
    
    if (self.urlToLoad) {
        
        [theWebView loadRequest:[NSURLRequest requestWithURL:self.urlToLoad]];
    }
    
    UIActivityIndicatorView *loadingView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    loadingView.color = [UIColor rhinoColor];
    loadingView.hidesWhenStopped = YES;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:loadingView];
    
    activity = loadingView;
    
    [self showErrorView:NO];
}

-(void)startLoadingWithColor:(UIColor*)color {
    
    self.isLoading = YES;
    [activity startAnimating];
}

-(void)startLoading {
    
    self.isLoading = YES;
    [activity startAnimating];
}

-(void)endViewLoading {
    
    self.isLoading = NO;
    [activity stopAnimating];
}

-(BOOL)canAddEmptyStateView {
    
    return YES;
}

-(void)addAllSubviews {
    
    UIWebView *webV = [[UIWebView  alloc] initWithFrame:CGRectZero];
    webV.delegate = self;
    [webV setScalesPageToFit:NO];
    webV.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:webV];
    
    if ([self canAddEmptyStateView]) {
        
        WLTEmptyStateView *emptyV = [WLTEmptyStateView emptyStateViewWithStyle:WLTEmptyStateViewStyleDefault andDelegate:self];
        emptyV.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addSubview:emptyV];
        [errorView showTitle:@"Sorry!" andSubtitle:@"Can't load the requested page.\nplease try sometime later." imageName:nil];
        
        errorView = emptyV;
    }
    
    theWebView = webV;
}

-(void)addAllConstraints {
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(theWebView);
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[theWebView]-0-|" options:0 metrics:nil views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[theWebView]-0-|" options:0 metrics:nil views:dict]];
    
    if (errorView) {
        NSDictionary *dict = NSDictionaryOfVariableBindings(errorView);
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[errorView]-0-|" options:0 metrics:nil views:dict]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[errorView]-0-|" options:0 metrics:nil views:dict]];
    }
}

-(void)setUrlToLoad:(NSURL *)urlToLoad {
    
    _urlToLoad = urlToLoad;
    if ([self isViewLoaded]) {
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:_urlToLoad];
        [request setTimeoutInterval:(1*60)];
        [theWebView loadRequest:request];
    }
}

-(void)showErrorView:(BOOL)show {
    
    errorView.hidden = !show;
    theWebView.hidden = show;
}

#pragma mark - WLTEmptyStateView

-(void)emptyStateViewReloadScreen:(WLTEmptyStateView*)view {
    
    if (self.urlToLoad) {
        
        [theWebView loadRequest:[NSURLRequest requestWithURL:self.urlToLoad]];
    }
}

-(void)emptyStateViewActionButtonTapped:(WLTEmptyStateView*)view {}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    [self showErrorView:YES];
    [self endViewLoading];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [self endViewLoading];
    [webView setNeedsDisplay];
}

-(void)webViewDidStartLoad:(UIWebView *)webView {
    
    [self startLoadingWithColor:[UIColor appleGreenColor]];
}

@end
