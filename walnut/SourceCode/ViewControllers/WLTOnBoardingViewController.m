//
//  WLTOnBoardingViewController.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 02/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTOnBoardingViewController.h"
#import "LSButton.h"
#import "WLTRegisterViewController.h"

@interface WLTOnBoardingViewController () <UIScrollViewDelegate>{
	
	CGFloat screenWidth;
	NSArray *onBoardingScreens;
}

@end

@implementation WLTOnBoardingViewController

- (void)dealloc {
    
    self.completionBlock = nil;
}

-(BOOL)needNavigationBar {
    return NO;
}

- (void)viewDidLoad {
	
	[super viewDidLoad];
	
	screenWidth = [UIScreen mainScreen].bounds.size.width;
	CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
	
	theScrollView.backgroundColor = [UIColor clearColor];
	theScrollView.pagingEnabled = YES;
	theScrollView.showsHorizontalScrollIndicator = NO;
	theScrollView.delegate = self;
	
	self.view.backgroundColor = [UIColor colorWithRed:0.98 green:0.98 blue:0.99 alpha:1.00];
	
	[registerButton applyDefaultStyle];
	[registerButton setTitle:@"SIGN UP"];
	
	if([UIScreen mainScreen].bounds.size.height < 500) {
		
		topIconImageView.hidden = YES;
		topConstraint.constant = -40;
		
		screenHeight -= (105+20);
	}else {
		screenHeight -= (105+86);
	}
	
	[alreadyUserButton.titleLabel setFont:[UIFont sfUITextMediumOfSize:14]];
	[alreadyUserButton setTitleColor:[UIColor denimColor] forState:UIControlStateNormal];
	
	NSMutableArray *screens = [NSMutableArray new];
	
	UIView * (^ viewForTextAndImage)(NSString *text, UIImage *image) = ^UIView * (NSString *text, UIImage *image){
		
		UIView *content = [[UIView alloc] initWithFrame:CGRectZero];
		content.translatesAutoresizingMaskIntoConstraints = NO;
		[theScrollView addSubview:content];
		
		UIView *topGap = [[UIView alloc] initWithFrame:CGRectZero];
		topGap.translatesAutoresizingMaskIntoConstraints = NO;
		[content addSubview:topGap];
		
		UIView *bottomGap = [[UIView alloc] initWithFrame:CGRectZero];
		bottomGap.translatesAutoresizingMaskIntoConstraints = NO;
		[content addSubview:bottomGap];
		
		UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectZero];
		imageV.translatesAutoresizingMaskIntoConstraints = NO;
		imageV.image = image;
		[content addSubview:imageV];
		
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
		label.textAlignment = NSTextAlignmentCenter;
		[label setPreferredMaxLayoutWidth:(screenWidth-40)];
		label.translatesAutoresizingMaskIntoConstraints = NO;
		label.numberOfLines = 0;
		label.textColor = [UIColor cadetColor];
		label.text = text;
		label.font = [UIFont sfUITextRegularOfSize:16];
		[content addSubview:label];
		
		NSDictionary *dict = NSDictionaryOfVariableBindings(topGap, bottomGap, label, imageV);
		[content addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[topGap(==bottomGap)]-0-[imageV]-15-[label]-0-[bottomGap(==topGap)]-0-|" options:0 metrics:nil views:dict]];
		[content addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[topGap]-0-|" options:0 metrics:nil views:dict]];
		[content addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[bottomGap]-0-|" options:0 metrics:nil views:dict]];
		[content addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[label]-20-|" options:0 metrics:nil views:dict]];
		
		[content addConstraint:[NSLayoutConstraint constraintWithItem:imageV
															attribute:NSLayoutAttributeCenterX
															relatedBy:NSLayoutRelationEqual
															   toItem:content
															attribute:NSLayoutAttributeCenterX
														   multiplier:1 constant:0]];
		return content;
	};
	
	UIView *lastAdded = nil;
	for ( int i = 0; i < 2; i++ ) {
		
		UIView *content = nil;
		if (i == 0) {
			content = viewForTextAndImage( @"Split expenses with friends & roommates", [UIImage imageNamed:@"OnBoardingImageOne"] );
		}
		else if (i == 1) {
			content = viewForTextAndImage( @"Send and request money from friends", [UIImage imageNamed:@"OnBoardingImageTwo"] );
		}
		content.translatesAutoresizingMaskIntoConstraints = NO;
		[theScrollView addSubview:content];
		
		NSDictionary *dict = NSDictionaryOfVariableBindings(content);
		if (lastAdded) {
			
			dict = NSDictionaryOfVariableBindings(content, lastAdded);
			
			[theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[content(==lastAdded)]-0-|" options:0 metrics:nil views:dict]];
			[theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[lastAdded]-0-[content(==lastAdded)]" options:0 metrics:nil views:dict]];
		}else {
			
			[theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-0-[content(==%f)]-0-|", screenHeight] options:0 metrics:nil views:dict]];
			[theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-0-[content(==%f)]", screenWidth] options:0 metrics:nil views:dict]];
		}
		
		lastAdded = content;
		[screens addObject:content];
	}
	
	onBoardingScreens = screens;
	
	NSDictionary *dict = NSDictionaryOfVariableBindings(lastAdded);
	[theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[lastAdded]-0-|" options:0 metrics:nil views:dict]];
	
	pageControl.pageIndicatorTintColor = [UIColor cadetGrayColor];
	pageControl.currentPageIndicatorTintColor = [UIColor cadetColor];
	pageControl.numberOfPages = onBoardingScreens.count;
	pageControl.currentPage = 0;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	
	[self checkAndChangeNextButtonTitleForOffset:scrollView.contentOffset.x];
}

-(void)checkAndChangeNextButtonTitleForOffset:(CGFloat)newOffset {
	
	NSInteger index = (newOffset/screenWidth);
	pageControl.currentPage = index;
}

-(IBAction)signInClicked:(UIControl*)button {
	
    WLTRegisterViewController *cont = [self controllerWithIdentifier:@"WLTRegisterViewController"];
    cont.isSignIn = YES;
    cont.completion = self.completionBlock;
    [self.navigationController pushViewController:cont animated:YES];
}

-(IBAction)registerClicked:(UIControl*)button {
	
    WLTRegisterViewController *cont = [self controllerWithIdentifier:@"WLTRegisterViewController"];
    cont.completion = self.completionBlock;
    [self.navigationController pushViewController:cont animated:YES];
}

@end
