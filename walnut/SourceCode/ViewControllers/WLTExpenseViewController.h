//
//  WLTExpenseViewController.h
//  walnut
//
//  Created by Abhinav Singh on 15/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "KeyboardHandlerViewController.h"
#import "WLTGroup.h"
#import "WLTTextField.h"

@interface WLTExpenseViewController : KeyboardHandlerViewController {
    
    __weak IBOutlet UIView *titleBackView;
    __weak IBOutlet UILabel *titleHeaderLabel;
    __weak IBOutlet UITextField *titleTextField;
    
    __weak IBOutlet UIView *amountBackView;
    __weak IBOutlet UILabel *amountHeaderLabel;
    __weak IBOutlet UITextField *amountTextField;
	
	__weak IBOutlet UIView *paidByBackView;
	__weak IBOutlet UILabel *paidByHeaderLabel;
	__weak IBOutlet UITextField *paidByTextField;
	__weak IBOutlet UIButton *changeButton;
	__weak IBOutlet UILabel *changeLabel;
	
    __weak IBOutlet NSLayoutConstraint *fieldHeightConstraint;
    __weak IBOutlet NSLayoutConstraint *fieldWidthConstraint;
}

@property(nonatomic, strong) WLTGroup *group;
@property(nonatomic, strong) SuccessBlock completion;

@end
