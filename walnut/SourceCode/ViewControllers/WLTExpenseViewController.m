//
//  WLTExpenseViewController.m
//  walnut
//
//  Created by Abhinav Singh on 15/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTExpenseViewController.h"
#import "WLTColorPalette.h"
#import "WLTSplitViewController.h"
#import "WLTTransaction.h"
#import "WLTGroupMembersViewController.h"
#import "WLTContactsManager.h"

@interface WLTExpenseViewController () <UITextFieldDelegate>{
	
    UITapGestureRecognizer *keyboardTapGest;
}

@property(nonatomic, strong) WLTUser *expenseOwner;

@end

@implementation WLTExpenseViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    theScrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    theScrollView.alwaysBounceVertical = YES;
    
    fieldWidthConstraint.constant = [UIScreen mainScreen].bounds.size.width;
    fieldHeightConstraint.constant = 80;
	
	{
		[titleBackView addBorderLineAtPosition:(BorderLinePositionBottom|BorderLinePositionTop)];
		
		titleHeaderLabel.textColor = [UIColor cadetGrayColor];
		titleHeaderLabel.font = [UIFont sfUITextMediumOfSize:12];
		
		titleTextField.font = [UIFont sfUITextRegularOfSize:16];
		titleTextField.textColor = [UIColor darkGrayTextColor];
	}
	
	{
		[amountBackView addBorderLineAtPosition:BorderLinePositionBottom];
		
		amountHeaderLabel.textColor = [UIColor cadetGrayColor];
		amountHeaderLabel.font = [UIFont sfUITextMediumOfSize:12];
		
		amountTextField.font = [UIFont sfUITextRegularOfSize:16];
		amountTextField.textColor = [UIColor darkGrayTextColor];
		amountTextField.delegate = self;
	}
	
	{
		[paidByBackView addBorderLineAtPosition:BorderLinePositionBottom];
		
		paidByHeaderLabel.textColor = [UIColor cadetGrayColor];
		paidByHeaderLabel.font = [UIFont sfUITextMediumOfSize:12];
		
		paidByTextField.font = [UIFont sfUITextRegularOfSize:16];
		paidByTextField.textColor = [UIColor darkGrayTextColor];
		paidByTextField.userInteractionEnabled = NO;
		
		[changeLabel setText:@"CHANGE"];
		[changeLabel setTextColor:self.navBarColor];
		[changeLabel setFont:[UIFont sfUITextMediumOfSize:12]];
	}
	
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithTitle:@"Next"
																 style:UIBarButtonItemStyleDone
                                                                target:self
																action:@selector(nextClicked:)];
    self.navigationItem.rightBarButtonItem = doneItem;
	
	self.expenseOwner = [WLTDatabaseManager sharedManager].currentUser;
	
	[self changeOwnersText];
    [self changeTitle];
}

-(IBAction)changeOwnerClicked:(id)sender {
	
	WLTGroupMembersViewController *memCont = [self controllerWithIdentifier:@"WLTGroupMembersViewController"];
	memCont.navBarColor = self.navBarColor;
	memCont.group = self.group;
	[self.navigationController pushViewController:memCont animated:YES];
	
	__weak WLTExpenseViewController *weakSelf = self;
	[memCont setCompletionBlock:^(WLTUser *cont){
		
		if (cont) {
			
			[weakSelf.navigationController popViewControllerAnimated:YES];
			weakSelf.expenseOwner = cont;
			[weakSelf changeOwnersText];
		}
	}];
}

-(void)viewTapped:(UITapGestureRecognizer*)tapGest {
    
    [self.view endEditing:YES];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if(titleTextField.text.length == 0) {
        [titleTextField becomeFirstResponder];
    }
}

-(void)nextClicked:(UIBarButtonItem*)item {
    
    NSError *validationError = nil;
    if (!titleTextField.text.length) {
        
        validationError = [NSObject walnutErrorWithMessage:@"Please enter valid description."];
    }
    else if (amountTextField.text.intValue <= 0) {
        
        validationError = [NSObject walnutErrorWithMessage:@"Please enter valid amount."];
    }
    
    if (validationError) {
        
        [self showAlertForError:validationError];
    }else {
        
        NSMutableArray *newMemArray = [NSMutableArray new];
        NSString *currentUserMob = CURRENT_USER_MOB;
        
        for ( WLTUser *mem in self.group.members ) {
            
            WLTSplitUsers *newusr = [[WLTSplitUsers alloc] init];
            newusr.amount = @(0);
            newusr.member = mem;
            
            if ([mem.mobileString isEqualToString:currentUserMob]) {
                
                [newMemArray insertObject:newusr atIndex:0];
            } else {
                [newMemArray addObject:newusr];
            }
        }
        
        long long dateStamp = [NSDate currentTimeStamp];
        
        GTLWalnutMTransaction *trans = [[GTLWalnutMTransaction alloc] init];
        trans.objType = TransactionObjectTypeSplit;
        trans.objUuid = [WLTNetworkManager generateUniqueIdentifier];
        trans.txnUuid = [WLTNetworkManager generateUniqueIdentifier];
        trans.deviceUuid = [WLTDatabaseManager sharedManager].appSettings.deviceIdentifier;
        trans.groupUuid = self.group.groupUUID;
        trans.txnDate = @(dateStamp);
        
        trans.amount = @(amountTextField.text.doubleValue);
//        trans.notes = notesTextField.text;
        trans.placeName = titleTextField.text;
        
        GTLWalnutMSplitUser *newUs = [[GTLWalnutMSplitUser alloc] init];
        newUs.mobileNumber = self.expenseOwner.mobileString;
        newUs.name = [self.expenseOwner name];
        trans.owner = newUs;
		
		GTLWalnutMSplitUser *newUsOwner = [[GTLWalnutMSplitUser alloc] init];
		newUsOwner.mobileNumber = CURRENT_USER_MOB;
		newUsOwner.name = [[WLTDatabaseManager sharedManager].currentUser name];
		trans.addedBy = newUsOwner;
		
        WLTSplitViewController *controller = [self controllerWithIdentifier:@"WLTSplitViewController"];
        controller.transaction = trans;
        controller.allMembers = newMemArray;
        controller.group = self.group;
        controller.navBarColor = self.navBarColor;
		controller.newTransaction = YES;
        controller.completionBlock = self.completion;
        
        [self.navigationController pushViewController:controller animated:YES];
    }
}

-(void)changeOwnersText {
	
	paidByTextField.text = @"--";
	
    __weak WLTExpenseViewController *weakSelf = self;
    [[WLTContactsManager sharedManager] displayNameForUser:self.expenseOwner defaultIsMob:YES completion:^(NSString *data) {
        
        if (weakSelf && data.length) {
            paidByTextField.text = data;
        }
    }];
}

-(void)changeTitle {
    
    [self setTitle:[NSString stringWithFormat:@"Splitting with %@", self.group.dName] andSubtitle:nil];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    BOOL retValue = YES;
    if ([textField isEqual:amountTextField]) {
        if (newString.doubleValue > MaximumAmountHandled) {
            retValue = NO;
        }
    }
    
    return retValue;
}

@end
