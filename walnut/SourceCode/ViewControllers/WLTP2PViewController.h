//
//  WLTP2PViewController.h
//  walnut
//
//  Created by Abhinav Singh on 11/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"
#import "WLTContact.h"
#import "LSButton.h"
#import "WLTUserIconButton.h"

@interface WLTP2PViewController : WLTViewController {
    
    __weak IBOutlet LSButton *payButton;
    __weak IBOutlet LSButton *remindButton;
    
    __weak IBOutlet UILabel *userNameLabel;
    __weak IBOutlet WLTUserIconButton *iconButton;
}

@property(nonatomic, strong) WLTContact *contact;

@end
