//
//  WLTCreateNewGroupViewController.h
//  walnut
//
//  Created by Abhinav Singh on 11/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "KeyboardHandlerViewController.h"

@class WLTGroupNameTableViewCell;
@class WLTGroupTypeTableViewCell;
@class WLTContact;

typedef NS_ENUM(NSInteger, GroupControllerSectionType) {
    
    GroupControllerSectionTypeNone = 0,
    GroupControllerSectionTypeInfo,
    GroupControllerSectionTypeName,
    GroupControllerSectionTypeGroupType,
    GroupControllerSectionTypeEditAction,
    GroupControllerSectionTypeEditMembers,
    GroupControllerSectionTypeMembers,
};

@interface WLTCreateNewGroupViewController : KeyboardHandlerViewController <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>{
    
    BOOL makePrivateGroup;
    
    __weak WLTGroupNameTableViewCell *groupNameTableCell;
    __weak UITableView *theTableView;
    
    NSString *currentUserMobileNumber;
}

@property(nonatomic, strong) NSMutableArray *members;
@property(nonatomic, assign) NSString *selectedColorID;
@property(nonatomic, strong) SuccessBlock completion;

-(BOOL)canShowGroupName;
-(BOOL)userCanEditGroupInfo;
-(BOOL)userCanTakeActions;

-(NSInteger)sectionIndexForIdentifier:(GroupControllerSectionType)identifier;
-(GroupControllerSectionType)identifierForSectionIndex:(NSInteger)secIndex;

-(void)setUpPrivacyCell:(WLTGroupTypeTableViewCell*)cell forRowIndex:(NSInteger)row;
-(NSString*)cellIdentifierForContact:(WLTContact*)contact;

@end
