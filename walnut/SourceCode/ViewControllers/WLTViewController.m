//
//  WLTViewController.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 04/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"
#import "UIView+Loading.h"
#import "WLTGroup.h"

@implementation WLTTitleView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
		
        UILabel *titlView = [[UILabel alloc] initWithFrame:CGRectZero];
        titlView.adjustsFontSizeToFitWidth = YES;
        titlView.minimumScaleFactor = 0.6;
        titlView.translatesAutoresizingMaskIntoConstraints = NO;
        titlView.textAlignment = NSTextAlignmentCenter;
        titlView.font = [UIFont appTitleFont];
        titlView.textColor = [UIColor whiteColor];
        titlView.numberOfLines = 1;
        [self addSubview:titlView];
        
        UILabel *subtitlView = [[UILabel alloc] initWithFrame:CGRectZero];
        subtitlView.translatesAutoresizingMaskIntoConstraints = NO;
        subtitlView.textAlignment = NSTextAlignmentCenter;
        subtitlView.textColor = [UIColor whiteColor];
        subtitlView.font = [UIFont appSubTitleFont];
        subtitlView.numberOfLines = 1;
        [self addSubview:subtitlView];
        
        UIView *topGap = [[UIView alloc] initWithFrame:CGRectZero];
        topGap.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:topGap];
        
        UIView *bottomGap = [[UIView alloc] initWithFrame:CGRectZero];
        bottomGap.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:bottomGap];
        
        NSDictionary *dict = NSDictionaryOfVariableBindings(titlView, subtitlView, topGap, bottomGap);
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[topGap(==bottomGap)]-0-[titlView]-0-[subtitlView]-0-[bottomGap(==topGap)]-0-|" options:0 metrics:nil views:dict]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[topGap]-0-|" options:0 metrics:nil views:dict]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[bottomGap]-0-|" options:0 metrics:nil views:dict]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:titlView
                                                         attribute:NSLayoutAttributeLeading
                                                         relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeLeading
                                                        multiplier:1 constant:0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:titlView
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1 constant:0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:subtitlView
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1 constant:0]];
        
        
        titleLabel = titlView;
        subTitleLabel = subtitlView;
    }
    
    return self;
}

-(CGSize)intrinsicContentSize {
    return CGSizeMake(200, 44);
}

-(void)setTitle:(NSString *)title subtitle:(NSString*)sub {
    
    titleLabel.text = title;
    subTitleLabel.text = sub;
}

@end

@interface WLTViewController ()

@end


@implementation WLTViewController

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.isLoading = NO;
    
    self.view.backgroundColor = [UIColor lightBackgroundColor];
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@""
                                                                               style:UIBarButtonItemStyleDone
                                                                              target:nil
                                                                              action:nil]];
}

-(NSString *)screenName {
    //TODO: Clean up the name. And if possible cache the screenName
    return NSStringFromClass([self class]);
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    UIStatusBarStyle currentStyle = [[UIApplication sharedApplication] statusBarStyle];
    UIStatusBarStyle preferredStyle = [self preferredStatusBarStyle];
    if (currentStyle != preferredStyle) {
        [[UIApplication sharedApplication] setStatusBarStyle:preferredStyle animated:animated];
    }
    
    UIColor *currentColor = self.navigationController.navigationBar.barTintColor;
    if (self.navBarColor) {
        
        if (![currentColor isEqual:self.navBarColor]) {
            if (animated) {
                [UIView animateWithDuration:0.25 animations:^{
                    [self.navigationController.navigationBar setBarTintColor:self.navBarColor];
                }];
            }else {
                [self.navigationController.navigationBar setBarTintColor:self.navBarColor];
            }
        }
        
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont appTitleFont], NSForegroundColorAttributeName:[UIColor whiteColor]}];
        [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
        if (titleView) {
            titleView->titleLabel.textColor = [UIColor whiteColor];
        }
    }else {
        
        if (animated) {
            [UIView animateWithDuration:0.25 animations:^{
                [self.navigationController.navigationBar setBarTintColor:nil];
            }];
        }else {
            [self.navigationController.navigationBar setBarTintColor:nil];
        }
        
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont appTitleFont], NSForegroundColorAttributeName:[UIColor rhinoColor]}];
        [self.navigationController.navigationBar setTintColor:[UIColor rhinoColor]];
        if (titleView) {
            titleView->titleLabel.textColor = [UIColor rhinoColor];
        }
    }
}

-(void)startLoadingWithColor:(UIColor*)color {
    
    self.isLoading = YES;
    [self.view startLoadingWithColor:color];
}

-(void)startLoading {
    
    [self startLoadingWithColor:self.navigationController.navigationBar.barTintColor];
}

-(void)endViewLoading {
    
    self.isLoading = NO;
    [self.view endLoading];
}

-(void)setTitle:(NSString *)title andSubtitle:(NSString*)sub {
    
    if (!titleView) {
        
        WLTTitleView *titlView = [[WLTTitleView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-150, 44)];
        
        UIColor *textColor = self.navigationController.navigationBar.tintColor;
        if (!textColor) {
            textColor = [UIColor cadetColor];
        }
        titlView->titleLabel.textColor = textColor;
        self.navigationItem.titleView = titlView;
        
        titleView = titlView;
    }
    
    if (!title.length) {
        title = sub;
        sub = nil;
    }
    
    [titleView setTitle:title subtitle:sub];
}

-(void)setTitleForGroup:(WLTGroup *)grp andSubtitle:(NSString*)sub {
    
    if (grp.isPrivate.boolValue) {
        
        NSMutableAttributedString *attributed = [grp.dName attributtedStringAddingPrivateIconOfHeight:17];
//        if (sub.length) {
//            
//            NSAttributedString *attSub = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", sub] attributes:@{NSFontAttributeName:[UIFont appSubTitleFont]}];
//            [attributed appendAttributedString:attSub];
//        }
        
        [self setAttributedTitle:attributed andSubTitle:sub];
    }else {
        
        [self setTitle:grp.dName andSubtitle:sub];
    }
}

-(void)setMembersTitleForGroup:(WLTGroup *)grp checkDirect:(BOOL)checkDirect{
    
    NSString *subTitle = nil;
    if (checkDirect && (grp.members.count == 2)) {
        
        subTitle = nil;
    }else {
        
        if (grp.members.count > 0) {
            
            if (grp.members.count == 1) {
                subTitle = [NSString stringWithFormat:@"%d Member", (int)grp.members.count];
            }else {
                subTitle = [NSString stringWithFormat:@"%d Members", (int)grp.members.count];
            }
        }
    }
    
    [self setTitleForGroup:grp andSubtitle:subTitle];
}

-(void)setAttributedTitle:(NSAttributedString *)title andSubTitle:(NSString*)sub{
    
    if (!titleView) {
        
        WLTTitleView *titlView = [[WLTTitleView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-150, 44)];
        self.navigationItem.titleView = titlView;
        titleView = titlView;
    }
    
    titleView->titleLabel.attributedText = title;
    titleView->titleLabel.numberOfLines = 0;
    
    titleView->subTitleLabel.text = sub;
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    if (self.navBarColor) {
        return UIStatusBarStyleLightContent;
    }
    else {
        return UIStatusBarStyleDefault;
    }
}

-(BOOL)needNavigationBar {
    return YES;
}

#pragma mark - UINavigatoinControllerDelegate

-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    if ([viewController isKindOfClass:[WLTViewController class]]) {
        
        WLTViewController *newCont = (WLTViewController*)viewController;
        if ([newCont needNavigationBar]) {
            
            if (self.navigationController.isNavigationBarHidden) {
                [self.navigationController setNavigationBarHidden:NO animated:animated];
            }
        }else if (!self.navigationController.isNavigationBarHidden) {
            
            [self.navigationController setNavigationBarHidden:YES animated:animated];
        }
    }
}

@end
