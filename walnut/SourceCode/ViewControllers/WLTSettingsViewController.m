//
//  WLTSettingsViewController.m
//  walnut
//
//  Created by Abhinav Singh on 27/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTSettingsViewController.h"
#import "WLTUserProfileViewController.h"
#import "WLTNetworkManager.h"
#import "WLTWebViewController.h"
#import "WLTPaymentTransactionsViewController.h"
#import "WLTInstrumentsManager.h"
#import "WLTCardInfoTableViewCell.h"
#import "WLTNewBankAccountViewController.h"
#import "WLTCardsListViewController.h"
#import "WLTFirstPaymentAlertView.h"
#import "WLTRootViewController.h"
#import "WLTShareActivityProvider.h"

static CGFloat HeaderHeight = 140;
static NSTimeInterval CachingTime = (1*60*60);

typedef NS_ENUM(NSInteger, SettingsTableSectionIdentifier) {
    
    SettingsTableSectionIdentifierCards = 0,
    SettingsTableSectionIdentifierSharing,
};

typedef NS_ENUM(NSInteger,CardsRowIdentifier) {
    
    CardsRowIdentifierAll = 0,
    CardsRowIdentifierDownloading,
};

@interface WLTSettingsViewController () <UINavigationControllerDelegate>{
    
    __weak IBOutlet UIView *headerView;
    __weak IBOutlet NSLayoutConstraint *headerViewHeight;
    
    NSDateFormatter *dateFormatter;
    UIActivityIndicatorView *instrumentsActivityIndicator;
}

@property(nonatomic, strong) NSDate *lastFetchTime;
@property(nonatomic, strong) NSString *selectedSyncValue;
@property(nonatomic, strong) NSDate *selectedSyncTime;

@end

@implementation WLTSettingsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setEdgesForExtendedLayout:UIRectEdgeAll];
    editImageView.hidden = YES;
    
    headerViewHeight.constant = HeaderHeight;
    
    headerView.backgroundColor = [UIColor clearColor];
    
    UIView *blurV = [UIView lightBlurView];
    blurV.translatesAutoresizingMaskIntoConstraints = NO;
    [headerView addSubview:blurV];
    [headerView sendSubviewToBack:blurV];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(blurV);
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[blurV]-0-|" options:0 metrics:nil views:dict]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[blurV]-0-|" options:0 metrics:nil views:dict]];
    
    [headerView addBorderLineAtPosition:BorderLinePositionBottom];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userChanged:) name:NotifyUserMobileChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userChanged:) name:NotifyUserInfoChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paymentsChanged:) name:NotifyPaymentsStatusChanged object:nil];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectZero];
    
    theTableView.separatorInset = UIEdgeInsetsZero;
    theTableView.backgroundColor = [UIColor clearColor];
    theTableView.tableFooterView = footer;
    theTableView.separatorColor = [UIColor placeHolderTextColor];
    [theTableView setContentInset:UIEdgeInsetsMake(HeaderHeight, 0, 49, 0)];
    [theTableView setContentOffset:CGPointMake( 0, -HeaderHeight)];
	
    [theTableView registerNib:[UINib nibWithNibName:@"WLTCardInfoTableViewCell" bundle:nil] forCellReuseIdentifier:@"WLTCardInfoTableViewCell"];
	
    nameLabel.font = [UIFont sfUITextRegularOfSize:21];
    nameLabel.textColor = [UIColor cadetColor];
    
    editImageView.image = [editImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    editImageView.tintColor = [UIColor cadetColor];
    
    mobNumberLabel.textColor = [UIColor cadetColor];
    mobNumberLabel.font = [UIFont sfUITextRegularOfSize:12];
    
    [userIconButton displayUserIcon:[WLTDatabaseManager sharedManager].currentUser];
    nameLabel.text = [WLTDatabaseManager sharedManager].currentUser.name;
    
    [mobNumberLabel setText:CURRENT_USER_MOB];
    
    [self fetchWhatsNewWithCompletion:nil];
    __weak typeof(self) weakSelf = self;
    [[WLTInstrumentsManager sharedManager] validCardsForTransactionType:PaymentInstrumentTypeBoth invalidateCache:YES withCompletion:^(id data) {
        
        [weakSelf reloadPaymentSection];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
}

-(void)reachabilityChanged:(NSNotification*)notify {
    
    NetworkStatus networkStatus = [[WLTNetworkManager sharedManager].internetReachability currentReachabilityStatus];
    if (networkStatus != NotReachable) {
        
        __weak typeof(self) weakSelf = self;
        [[WLTInstrumentsManager sharedManager] validCardsForTransactionType:PaymentInstrumentTypeBoth invalidateCache:YES withCompletion:^(id data) {
            
            [weakSelf reloadPaymentSection];
        }];
        
        [self fetchWhatsNewWithCompletion:nil];
    }
}

-(void)paymentsChanged:(NSNotification*)notify {
    
    [theTableView reloadData];
}

-(void)reloadPaymentSection {
    
    [theTableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
    [self reloadPaymentSection];
}

-(BOOL)needNavigationBar {
    return NO;
}

-(CardsRowIdentifier)cardIndentifierForRow:(NSInteger)row {
    
    CardsRowIdentifier identifier = CardsRowIdentifierAll;
    WLTInstrumentsManager *manager = [WLTInstrumentsManager sharedManager];
    
    if (manager.state == IntrumentManagerStateDownloading) {
        
        identifier = CardsRowIdentifierDownloading;
    }else {
        
        identifier = CardsRowIdentifierAll;
    }
    
    return identifier;
}

-(UITabBarItem *)tabBarItem {
    
    if (!tabItem) {
        UITabBarItem *item = [[UITabBarItem alloc] initWithTitle:@"You" image:[[UIImage imageNamed:@"YouIconTabUnSelected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"YouIconTabSelected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        tabItem = item;
    }
    
    return tabItem;
}

-(IBAction)accountSettingsClicked:(UIButton*)btnSetting {
    
    return;
    
    __weak typeof(self) weakSelf = self;
    WLTUserProfileViewController *profile = [self controllerWithIdentifier:@"WLTUserProfileViewController"];
    profile.navBarColor = self.navBarColor;
    [profile setCompletion:^(BOOL success){
        
        if (success) {
            [weakSelf.navigationController popToRootViewControllerAnimated:YES];
        }
    }];
    
    [self.navigationController pushViewController:profile animated:YES];
}

-(void)userChanged:(NSNotification*)notify {
    
    [userIconButton displayUserIcon:[WLTDatabaseManager sharedManager].currentUser];
    nameLabel.text = [WLTDatabaseManager sharedManager].currentUser.name;
    [mobNumberLabel setText:CURRENT_USER_MOB];
    
    [theTableView reloadData];
}

-(void)fetchWhatsNewWithCompletion:(DataCompletionBlock)block {
    
    if ( !self.lastFetchTime || ([self.lastFetchTime timeIntervalSinceNow] > CachingTime) ) {
        
        WLTDatabaseManager *manager = [WLTDatabaseManager sharedManager];
        
        GTLWalnutMUserInfo *info = [[GTLWalnutMUserInfo alloc] init];
        info.registrationId = manager.appSettings.pushToken;
        info.notificationSyncTime = @(0);
        
        NSDictionary *dict = @{@"AppVersion":@(AppVersionString.integerValue)};
        NSData *data = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
        NSString *dataString = [[NSString alloc] initWithData:data encoding:4];
        
        info.attributes = dataString;
        info.deviceUuid = manager.appSettings.deviceIdentifier;
        
        GTLQueryWalnut *walN = [GTLQueryWalnut queryForUserWhatsNewWithObject:info];
        [self startLoadingWithColor:nil];
        
        __weak WLTSettingsViewController *weakSelf = self;
        [[WLTNetworkManager sharedManager].walnutService executeQuery:walN completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
            
            if (!error) {
				
				weakSelf.lastFetchTime = [NSDate date];
				
                GTLWalnutMUserNotifications *notifyList = object;
                [[WLTDatabaseManager sharedManager] updatePaymentConfigration:notifyList.paymentConfig];
                
                if (block) {
                    block(@(YES));
                }
            }
            else {
                if (block) {
                    block(error);
                }
            }
            
            [weakSelf endViewLoading];
        }];
    }else {
        
        if (block) {
            block(@(YES));
        }
    }
}

#pragma mark - UITableViewDelegate & UITableViewDataSource

-(SettingsTableSectionIdentifier)sectionIdentifierForSection:(NSInteger)section {
    
    SettingsTableSectionIdentifier identifier = SettingsTableSectionIdentifierSharing;
    if (PaymentsEnabled()) {
        
        if (section == 0) {
            identifier = SettingsTableSectionIdentifierCards;
        }else {
            identifier = SettingsTableSectionIdentifierSharing;
        }
    }else {
        identifier = SettingsTableSectionIdentifierSharing;
    }
    
    return identifier;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger count = 0;
    switch ([self sectionIdentifierForSection:section]) {
        case SettingsTableSectionIdentifierCards: {
            
            count = 1;
        }
        break;
        case SettingsTableSectionIdentifierSharing:
            count = 5;
            break;
        default:
            break;
    }
    
    return count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger count = 1;
    if (PaymentsEnabled()) {
        count += 1;
    }
    
    return count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *cell = nil;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"DefaultTableCell"];
    if (!cell) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DefaultTableCell"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.separatorInset = UIEdgeInsetsZero;
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor rhinoColor];
        cell.textLabel.font = [UIFont sfUITextRegularOfSize:14];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    switch ([self sectionIdentifierForSection:indexPath.section]) {
            
        case SettingsTableSectionIdentifierCards: {
            
            switch ([self cardIndentifierForRow:indexPath.row]) {
                case CardsRowIdentifierDownloading: {
                    cell.textLabel.text = @"Downloading Please Wait...";
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
                break;
                case CardsRowIdentifierAll:
                    cell.textLabel.text = @"Saved Cards & Banks";
                    break;
                default:
                    break;
            }
        }
        break;
        case SettingsTableSectionIdentifierSharing: {
            
            cell.accessoryType = UITableViewCellAccessoryNone;
            if (indexPath.row == 0) {
                cell.textLabel.text = @"About Us";
            }
            else if (indexPath.row == 1) {
                cell.textLabel.text = @"Rate Us";
            }
            else if (indexPath.row == 2) {
                cell.textLabel.text = @"FAQ";
            }
            else if (indexPath.row == 3) {
                cell.textLabel.text = @"Support";
            }
            else if (indexPath.row == 4) {
                cell.textLabel.text = @"Spread a good word. Help us grow. 🙏";
            }
        }
        break;
        default:
            
            cell.textLabel.text = @"";
        break;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch ([self sectionIdentifierForSection:indexPath.section]) {
            
        case SettingsTableSectionIdentifierCards: {
            
            __weak typeof(self) weakSelf = self;
            
            void (^ showCardListController)() = ^void (){
                
                CardsRowIdentifier identifier = [weakSelf cardIndentifierForRow:indexPath.row];
                if ( identifier != CardsRowIdentifierDownloading ) {
                    
                    WLTCardsListViewController *instruments = [weakSelf controllerWithIdentifier:@"WLTCardsListViewController"];
                    instruments.navBarColor = weakSelf.navBarColor;
                    [weakSelf.navigationController pushViewController:instruments animated:YES];
                }
            };
            
            if ([WLTFirstPaymentAlertView canShow]) {
                
                WLTFirstPaymentAlertView *alert = [WLTFirstPaymentAlertView initWithDefaultXib];
                [alert setProceedBlock:^(BOOL success){
                    
                    if (success) {
                        
                        showCardListController();
                    }
                }];
                [alert show];
            }else {
                
                showCardListController();
            }
        }
        break;
        case SettingsTableSectionIdentifierSharing: {
            if (indexPath.row == 0 ) {
                
                NSURL *aboutUr = [NSURL URLWithString:@"http://getwalnut.com/about-us"];
                if ([[UIApplication sharedApplication] canOpenURL:aboutUr]) {
                    
                    [[UIApplication sharedApplication] openURL:aboutUr];
                }else {
                    
                    WLTWebViewController *webCont = [[WLTWebViewController alloc] init];
                    webCont.navBarColor = self.navBarColor;
                    webCont.urlToLoad = aboutUr;
                    webCont.title = @"About Us";
                    [self.navigationController pushViewController:webCont animated:YES];
                }
            }
            else if (indexPath.row == 1 ) {
                
                [self openiTunesURL];
            }
            else if (indexPath.row == 2 ) {
                
                NSURL *faqUr = [NSURL URLWithString:@"http://www.getwalnut.com/help_app_ios"];
                if ([[UIApplication sharedApplication] canOpenURL:faqUr]) {
                    
                    [[UIApplication sharedApplication] openURL:faqUr];
                }else {
                    
                    WLTWebViewController *webCont = [[WLTWebViewController alloc] init];
                    webCont.navBarColor = self.navBarColor;
                    webCont.urlToLoad = faqUr;
                    webCont.title = @"FAQ";
                    [self.navigationController pushViewController:webCont animated:YES];
                }
            }
            else if ( indexPath.row == 3 ) {
                
                [self sendSupportEmailForTransaction:nil];
            }
            else if (indexPath.row == 4 ) {
                
                NSArray *sharingItems = [WLTSharingHelper activityItemsForType:WLTShareActivityTypeDefault];
                UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
                [activityController setExcludedActivityTypes:@[UIActivityTypeAssignToContact, UIActivityTypeCopyToPasteboard, UIActivityTypePrint,  UIActivityTypeSaveToCameraRoll, UIActivityTypePostToWeibo]];
                
                [self presentViewController:activityController animated:YES completion:nil];
            }
        }
        break;
        default:
            break;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 45;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    CGFloat height = 1;
    SettingsTableSectionIdentifier iden = [self sectionIdentifierForSection:section];
    if (iden == SettingsTableSectionIdentifierSharing) {
        height = 50;
    }
    
    return height;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    CGFloat height = 35;
    return height;
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *footer = nil;
    SettingsTableSectionIdentifier iden = [self sectionIdentifierForSection:section];
    if ( iden == SettingsTableSectionIdentifierSharing ) {
        
        footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 20)];
        footer.backgroundColor = [UIColor clearColor];
        
        footer.height = 50;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.textAlignment = NSTextAlignmentCenter;
        label.translatesAutoresizingMaskIntoConstraints = NO;
        label.font = [UIFont sfUITextMediumOfSize:12];
        label.textColor = [UIColor cadetGrayColor];
        
        NSString *plistVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        NSString *bundleVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey];
        label.text = [NSString stringWithFormat:@"v%@ B %@",plistVersion, bundleVersion];
        
        [footer addSubview:label];
        
        NSDictionary *dict = NSDictionaryOfVariableBindings(label);
        [footer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[label]-0-|" options:0 metrics:nil views:dict]];
        [footer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[label]-0-|" options:0 metrics:nil views:dict]];
    }
    
    return footer;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    SettingsTableSectionIdentifier iden = [self sectionIdentifierForSection:section];
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40)];
    header.backgroundColor = [UIColor clearColor];
    
    if ( iden != SettingsTableSectionIdentifierSharing ) {
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.translatesAutoresizingMaskIntoConstraints = NO;
        label.font = [UIFont sfUITextMediumOfSize:12];
        label.textColor = [UIColor cadetGrayColor];
        [header addSubview:label];
        
        NSDictionary *dict = NSDictionaryOfVariableBindings(label);
        [header addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[label]-0-|" options:0 metrics:nil views:dict]];
        [header addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[label]-5-|" options:0 metrics:nil views:dict]];
        
        switch ([self sectionIdentifierForSection:section]) {
                
            case SettingsTableSectionIdentifierCards:
                label.text = @"PAYMENT";
                break;
            default:
                break;
        }
    }
    
    return header;
}

@end
