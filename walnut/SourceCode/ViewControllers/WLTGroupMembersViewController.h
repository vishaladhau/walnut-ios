//
//  WLTGroupMembersViewController.h
//  walnut
//
//  Created by Abhinav Singh on 6/9/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"
#import "WLTGroup.h"

typedef void (^ MemberSelectionBlock)(WLTUser *member);

@interface WLTGroupMembersViewController : WLTViewController <UITableViewDelegate, UITableViewDataSource>{
	
	__weak IBOutlet UITableView *theTableView;
	NSArray *allMembers;
}

@property(nonatomic, strong) MemberSelectionBlock completionBlock;
@property(nonatomic, strong) WLTGroup *group;

@end
