//
//  WLTContactsViewController.h
//  walnut
//
//  Created by Abhinav Singh on 01/09/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"
#import "WLTSearchOperation.h"

FOUNDATION_EXPORT NSString *const InvalidNamePrefix;

typedef NS_ENUM(NSInteger, ContactSectionIndex) {
    
    ContactSectionIndexNone = 0,
    ContactSectionIndexContactUsers,
    ContactSectionIndexSearchResults,
    ContactSectionIndexContactNotFound,
    ContactSectionIndexContactRecent,
};

@interface WLTContactsViewController : WLTViewController < WLTSearchDelegate, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource > {
    
    __weak IBOutlet UISearchBar *theSearchBar;
    __weak IBOutlet UITableView *theTableView;
    
    NSMutableDictionary *indexMappingDict;
    NSMutableArray *allContactIndexes;
    
    BOOL showingSearchResults;
    
    NSOperationQueue *searchQueue;
    NSMutableDictionary *searchUnknownContactsMapping;
}

@property(nonatomic, strong) NSString *currentSearchText;
@property(nonatomic, strong) NSMutableArray *searchResults;

@property(nonatomic, strong) NSMutableArray *contactsArray;

-(WLTContact*)contactAtIndexPath:(NSIndexPath*)path;
-(void)reloadIndexs;
-(void)addNewContacts:(NSMutableArray*)result;
-(void)resetSearch;

@end
