//
//  WLTOTPViewController.m
//  walnut
//
//  Created by Abhinav Singh on 15/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTOTPViewController.h"
#import "WLTOTPDotView.h"
#import "WLTProfilesSelectionView.h"

static int OTPLength = 6;
static NSTimeInterval WaitingTime = 30;

@interface WLTOTPViewController () {
	
    NSDate *timmerStartDate;
    NSTimer *resendTimmer;
}

@end

@implementation WLTOTPViewController

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [otpView becomeFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)keyboardWillShow:(NSNotification*)notify {
    
    NSDictionary *userInfo = notify.userInfo;
    
    NSTimeInterval animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationOptions animationCurve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
	CGRect rectStart = [notify.userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue];
	CGRect rectEnd = [notify.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
	
	rectStart = [self.view.window convertRect:rectStart toView:self.view];
	rectEnd = [self.view.window convertRect:rectEnd toView:self.view];
	
	CGFloat gap = (self.view.height - rectEnd.origin.y);
	
	bottomMarginConstraint.constant = (gap+10);
	
    __weak WLTOTPViewController *weakSelf = self;
	
    UIViewAnimationOptions animationOptions = animationCurve | UIViewAnimationOptionBeginFromCurrentState;
    [UIView animateWithDuration:animationDuration delay:0 options:animationOptions animations:^{
        
        [weakSelf.view layoutIfNeeded];
    } completion:nil];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    bottomMarginConstraint.constant = 20;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"Enter OTP";
    
    [otpView setupForOTPCount:OTPLength andDotViewClass:[WLTOTPDotView class]];
    [otpView addTarget:self action:@selector(otpValueChanged:) forControlEvents:UIControlEventValueChanged];
	
    [timerLabel setFont:[UIFont sfUITextMediumOfSize:12]];
	timerLabel.textColor = [UIColor cadetColor];
    
    if (self.userObject.mobileNumber.length > IndianMobileLength) {
        
        [self startResendTimer];
    }
	
    otpView.backgroundColor = self.view.backgroundColor;
    
    infoLabel.textColor = [UIColor cadetColor];
    infoLabel.font = [UIFont sfUITextMediumOfSize:12];
    
    timeDisplayLabel.textColor = [UIColor cadetColor];
    timeDisplayLabel.font = [UIFont sfUITextMediumOfSize:12];
    
    [resendCallButton.titleLabel setFont:[UIFont sfUITextMediumOfSize:14]];
    [resendSmsButton setTitleColor:[UIColor appleGreenColor] forState:UIControlStateNormal];
    [resendSmsButton setTitleColor:[UIColor appleGreenColor] forState:UIControlStateDisabled];
    [resendSmsButton setTitle:@"RESEND SMS" forState:UIControlStateNormal];
    
    [resendSmsButton.titleLabel setFont:[UIFont sfUITextMediumOfSize:14]];
    [resendCallButton setTitleColor:[UIColor appleGreenColor] forState:UIControlStateNormal];
    [resendCallButton setTitleColor:[UIColor appleGreenColor] forState:UIControlStateDisabled];
    [resendCallButton setTitle:@"CALL ME INSTEAD" forState:UIControlStateNormal];
    
    resendCallButton.hidden = YES;
    resendSmsButton.hidden = YES;
    timerLabel.hidden = YES;
    
    timeDisplayLabel.hidden = NO;
}

-(void)endTimer {
    
    if (resendTimmer) {
        [resendTimmer invalidate];
        resendTimmer = nil;
    }
}

-(void)startResendTimer {
    
    if (resendTimmer) {
        [resendTimmer invalidate];
        resendTimmer = nil;
    }
    
    timerLabel.hidden = YES;
    resendSmsButton.hidden = YES;
    resendCallButton.hidden = YES;
    
    timeDisplayLabel.hidden = NO;
    
    timmerStartDate = [NSDate date];
    resendTimmer = [NSTimer scheduledTimerWithTimeInterval:(1/120.0) target:self
                                                  selector:@selector(changeResendStatus:)
                                                  userInfo:nil repeats:YES];
}

-(void)changeResendStatus:(NSTimer*)timer {
    
    if ([timer isValid]) {
        
        NSTimeInterval secondPassed = [[NSDate date] timeIntervalSinceDate:timmerStartDate];
        int remaning = (int)(WaitingTime - secondPassed);
        if (remaning > 0) {
            
            timeDisplayLabel.text = [NSString stringWithFormat:@"00:%d", remaning];
        }else {
            
            [self endTimer];
            
            timerLabel.hidden = NO;
            
            resendSmsButton.hidden = NO;
            resendCallButton.hidden = NO;
            
            timeDisplayLabel.text = nil;
            timeDisplayLabel.hidden = YES;
        }
    }
}

-(void)showInvalidOTPError:(NSError*)error {
    
    [otpView reset];
    [otpView becomeFirstResponder];
    
    [self showAlertForError:error];
}

-(IBAction)resendOTPViaCallClicked:(id)sender {
    
    self.userObject.throughSMS = NO;
    [self reFetchOTP];
}

-(IBAction)resendOTPViaSMSClicked:(id)sender {
    
    self.userObject.throughSMS = YES;
    [self reFetchOTP];
}

-(void)reFetchOTP {
    
    [self.view endEditing:YES];
    [self startLoadingWithColor:nil];
    
    __weak WLTOTPViewController *weakSelf = self;
    
    [otpView reset];
    [self endTimer];
    
    [self startLoading];
    
    [[WLTNetworkManager sharedManager] getOTPForUser:self.userObject completion:^(id data) {
        
        if (![data isKindOfClass:[NSError class]]) {
            
            [weakSelf startResendTimer];
            [otpView becomeFirstResponder];
        }else {
            [weakSelf showAlertForError:data];
        }
        
        [weakSelf endViewLoading];
    }];
}

-(void)otpValueChanged:(WLTOTPView*)otView {
    
    if (otView.enteredDigits == OTPLength) {
        
        [otView resignFirstResponder];
        [self checkMobileAndValidateOTP];
    }
}

-(void)validateOTP:(NSString*)otp {
   
    if ([self isLoading]) {
        return;
    }
    
    [self startLoadingWithColor:nil];
    
    __weak WLTOTPViewController *weakSelf = self;
    
    if (!self.userObject.deviceUUID.length) {
        self.userObject.deviceUUID = [WLTNetworkManager generateUniqueIdentifier];
    }
    
    [[WLTNetworkManager sharedManager] validateOTP:otp forUser:self.userObject completion:^(id data) {
        
        if (![data isKindOfClass:[NSError class]]) {
            
            NSString *token = data[@"a_token"];
            weakSelf.userObject.authToken = token;
            
            if (data[@"payment_config"]) {
                
                GTLWalnutMPaymentConfig *config = [GTLWalnutMPaymentConfig objectWithJSON:[data[@"payment_config"] mutableCopy]];
                weakSelf.userObject.paymentConfig = config;
            }
            
            if (weakSelf.completion) {
                weakSelf.completion(weakSelf.userObject);
            }
            
            [weakSelf endTimer];
        }else {
            
            if (weakSelf.completion) {
                weakSelf.completion(nil);
            }
            
            [weakSelf endTimer];
            [weakSelf showAlertForError:data];
        }
        
        [weakSelf endViewLoading];
    }];
}

-(void)checkMobileAndValidateOTP {
    
    [self validateOTP:otpView.text];
}

@end
