//
//  WLTWebViewController.h
//  walnut
//
//  Created by Abhinav Singh on 13/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"

@import WebKit;

@interface WLTWebViewController : WLTViewController <UIWebViewDelegate>{
    __weak UIWebView  *theWebView;
}

@property(nonatomic, strong) NSURL *urlToLoad;

//-(void)addAllSubviews;
//-(void)addAllConstraints;
//-(BOOL)canAddEmptyStateView;

@end
