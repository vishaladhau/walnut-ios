//
//  WLTPaymentContactsViewController.m
//  walnut
//
//  Created by Abhinav Singh on 12/08/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTPaymentContactsViewController.h"
#import "WLTContactsManager.h"
#import "WLTContact.h"
#import "WLTSearchOperation.h"
#import "WLTDatabaseManager.h"
#import "WLTUser.h"
#import "WLTTableHeaderView.h"
#import "WLTNumberSelectionView.h"
#import "WLTCombinedGroupPayments.h"
#import "WLTSendMoneyRequestViewController.h"
#import "WLTContactTableViewCell.h"
#import "WLTRootViewController.h"

@interface WLTPaymentContactsViewController () {
    
}

@property(nonatomic, strong) NSArray *recentUsers;
@property(nonatomic, strong) NSArray *recentDisplayedUsers;

@end

@implementation WLTPaymentContactsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if (self.transactionState == UserTransactionStateGet) {
        
        self.title = @"Select a friend to request money";
    }
    else if (self.transactionState == UserTransactionStateOwe) {
        
        self.title = @"Select a friend to send money";
    }
}

-(void)addNewContacts:(NSMutableArray*)result {
    
    NSMutableArray *addedContKeys = [NSMutableArray new];
    NSMutableArray *recentConts = [NSMutableArray new];
    
    NSString *currentUserMob = CURRENT_USER_MOB;
    [addedContKeys addObject:currentUserMob];
    
    NSArray *dataArray = [[WLTDatabaseManager sharedManager] recentContactsList];
    for ( NSDictionary *dict in dataArray ) {
        
        NSString *key = dict[@"mobileString"];
        
        WLTContact *cont = [[WLTContact alloc] initWithName:dict[@"name"] andNumber:key];
        cont.mobile.label = @"Recent";
        
        [recentConts addObject:cont];
        [addedContKeys addObject:key];
    }
    
    self.recentUsers = recentConts;
    self.recentDisplayedUsers = self.recentUsers;
    
    NSMutableArray *toRemoveObjects = [NSMutableArray new];
    NSArray *allNums = [result valueForKeyPath:@"mobile.number"];
    
    for ( WLTContact *usr in recentConts ) {
        
        NSInteger index = [allNums indexOfObject:usr.mobile.number];
        if (index != NSNotFound) {
            
            [toRemoveObjects addObject:result[index]];
        }
    }
    
    [result removeObjectsInArray:toRemoveObjects];
    
    [super addNewContacts:result];
}

-(void)resetSearch {
    
    theSearchBar.text = nil;
    self.currentSearchText = nil;
    
    showingSearchResults = NO;
    self.recentDisplayedUsers = self.recentUsers;
    
    [theTableView reloadData];
}

-(WLTContact*)contactAtIndexPath:(NSIndexPath*)path {
    
    WLTContact *cont = nil;
    ContactSectionIndex identifier = [self identifierForIndex:path.section];
    
    if (identifier == ContactSectionIndexContactRecent) {
        cont = self.recentDisplayedUsers[path.row];
    }
    else if (identifier == ContactSectionIndexContactUsers) {
        
        NSInteger index = path.section;
        if (self.recentDisplayedUsers.count) {
            index -= 1;
        }
        cont = indexMappingDict[allContactIndexes[index]][path.row];
    }
    else if (identifier == ContactSectionIndexSearchResults) {
        
        cont = self.searchResults[path.row];
    }else if (identifier == ContactSectionIndexContactNotFound) {
        
        NSString *comString = [self.currentSearchText mobileNumberWithCountryCode];
        cont = searchUnknownContactsMapping[comString];
        if (!cont) {
            
            cont = [[WLTContact alloc] initWithName:nil andNumber:comString];
            searchUnknownContactsMapping[comString] = cont;
        }
    }
    
    return cont;
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    if (showingSearchResults) {
        return nil;
    }
    
    return allContactIndexes;
}

-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    
    if (showingSearchResults) {
        return 0;
    }
    
    if (self.recentDisplayedUsers.count) {
        index += 1;
    }
    return index;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger secs = 0;
    if (showingSearchResults) {
//        secs = 1;
        if (self.recentDisplayedUsers.count) {
            secs += 1;
        }
        if (self.searchResults.count) {
            secs += 1;
        }
        if (secs == 0) {
            if (self.currentSearchText.isValidIndianMobile) {
                secs = 1;
            }
        }
    }else {
        
        secs = allContactIndexes.count;
        if (self.recentDisplayedUsers.count) {
            secs += 1;
        }
    }
    
    return secs;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger rowsCount = 0;
    ContactSectionIndex identifier = [self identifierForIndex:section];
    if (identifier == ContactSectionIndexContactUsers) {
        
        NSInteger index = section;
        if (self.recentDisplayedUsers.count) {
            index -= 1;
        }
        rowsCount = [indexMappingDict[allContactIndexes[index]] count];
    }else if (identifier == ContactSectionIndexSearchResults) {
        
        rowsCount = self.searchResults.count;
    }
    else if (identifier == ContactSectionIndexContactNotFound) {
        rowsCount = 1;
    }
    else if (identifier == ContactSectionIndexContactRecent) {
        rowsCount = self.recentDisplayedUsers.count;
    }
    
    return rowsCount;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    CGFloat height = 0;
    ContactSectionIndex identifier = [self identifierForIndex:section];
    if ( (identifier == ContactSectionIndexContactNotFound) || (identifier == ContactSectionIndexContactRecent) ) {
        height = 35;
    }
    else if ( (identifier == ContactSectionIndexContactUsers) || (identifier == ContactSectionIndexSearchResults) ) {
        
        NSInteger headerSection = -1;
        if (self.recentDisplayedUsers.count) {
            headerSection = 1;
        }
        
        if (section == headerSection) {
            height = 35;
        }
    }
    
    return height;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *vHeader = nil;
    ContactSectionIndex identifier = [self identifierForIndex:section];
    if ( (identifier == ContactSectionIndexContactUsers) || (identifier == ContactSectionIndexSearchResults) ) {
        
        NSInteger headerSection = -1;
        if (self.recentDisplayedUsers.count) {
            headerSection = 1;
        }
        
        if (section == headerSection) {
            
            WLTTableHeaderView *header = [[WLTTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
            header.label.text = @"OTHER";
            
            vHeader = header;
        }
    }
    else if ( identifier == ContactSectionIndexContactNotFound ) {
        
        WLTTableHeaderView *header = [[WLTTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
        header.label.text = @"NOT IN CONTACTS";
        
        vHeader = header;
    }
    else if ( identifier == ContactSectionIndexContactRecent ) {
        
        WLTTableHeaderView *header = [[WLTTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
        header.label.text = @"RECENT";
        
        vHeader = header;
    }
    return vHeader;
}

-(ContactSectionIndex)identifierForIndex:(NSInteger)secIndex {
    
    ContactSectionIndex identifier = ContactSectionIndexNone;
    if (secIndex == 0) {
        
        if (self.recentDisplayedUsers.count) {
            identifier = ContactSectionIndexContactRecent;
        }
        else if (showingSearchResults && self.currentSearchText.isValidIndianMobile && !self.searchResults.count && !self.recentDisplayedUsers.count) {
            
            identifier = ContactSectionIndexContactNotFound;
        }
        else if (showingSearchResults) {
            
            identifier = ContactSectionIndexSearchResults;
        }
        else  {
            
            identifier = ContactSectionIndexContactUsers;
        }
    }else {
        
        if (showingSearchResults) {
            identifier = ContactSectionIndexSearchResults;
        }
        else  {
            identifier = ContactSectionIndexContactUsers;
        }
    }
    
    return identifier;
}

-(void)userSelectedContact:(WLTContact*)contact atIndexPath:(NSIndexPath*)indexPath {
    
    [self.view endEditing:YES];
    
    [[WLTDatabaseManager sharedManager] saveContactToRecentList:contact];
    
    WLTSendMoneyRequestViewController *controller = [self.rootController controllerWithIdentifier:@"WLTSendMoneyRequestViewController"];
    controller.transactionType = self.transactionState;
    controller.requestingUser = contact;
    [self.navigationController pushViewController:controller animated:YES];
    
    __weak typeof(self) weakSelf = self;
    [controller setCompletion:^(BOOL success){
        
        if (success) {
            [weakSelf.navigationController popToRootViewControllerAnimated:NO];
        }
    }];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    [searchQueue cancelAllOperations];
    if (searchText.length) {
        
        self.currentSearchText = searchText;
        
        if (self.contactsArray.count) {
            
            WLTSearchOperation *search = [[WLTSearchOperation alloc] initWithDataArray:self.contactsArray andSearchString:searchText];
            search.identifier = @"Other";
            search.delegate = self;
            [searchQueue addOperation:search];
        }
        if (self.recentUsers.count) {
            
            WLTSearchOperation *search = [[WLTSearchOperation alloc] initWithDataArray:self.recentUsers andSearchString:searchText];
            search.identifier = @"Recent";
            search.delegate = self;
            [searchQueue addOperation:search];
        }
    }else {
        
        [self resetSearch];
    }
}

-(void)searchCompleted:(WLTSearchOperation*)search forString:(NSString*)string withResults:(NSMutableArray*)results {
    
    if ([string isEqualToString:self.currentSearchText]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if ([search.identifier isEqualToString:@"Other"]) {
                self.searchResults = results;
            }
            if ([search.identifier isEqualToString:@"Recent"]) {
                self.recentDisplayedUsers = results;
            }
            
            showingSearchResults = YES;
            [theTableView reloadData];
        });
    }
}

@end
