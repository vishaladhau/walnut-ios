//
//  WLTColorPickerViewController.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 07/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"

@interface WLTColorPickerViewController : WLTViewController <UICollectionViewDelegate, UICollectionViewDataSource> {
    
    __weak IBOutlet UICollectionView *theCollectionView;
}

@property(nonatomic, strong) NSString *selectedColorID;
@property(nonatomic, strong) DataCompletionBlock completionBlock;

@end
