//
//  WLTGroupListViewController.h
//  walnut
//
//  Created by Abhinav Singh on 12/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"
#import "LSButton.h"
#import "WLTEmptyStateView.h"

@class WLTGroupCollectionViewCell;
@class WLTHomeViewController;

@interface WLTGroupListViewController : WLTViewController {
    
    __weak IBOutlet UIView *fakeNavigationBarView;
    __weak IBOutlet NSLayoutConstraint *fakeNavbarHeight;
	
    __weak IBOutlet UIScrollView *theScrollView;
    __weak LSButton *addNewGroupButton;
    __weak WLTEmptyStateView *errorView;
    
    UITabBarItem *tabItem;
}

@property(nonatomic, strong) NSArray *groupArrays;

-(WLTGroupCollectionViewCell*)cellForGroup:(WLTGroup*)grp;

-(void)showDetailsControllerForGroup:(WLTGroup*)grp;

-(void)showSettleScreenForUserMobile:(NSString*)mobileNumber
                            userName:(NSString*)name
                            andGroup:(NSString*)grpID;

@end
