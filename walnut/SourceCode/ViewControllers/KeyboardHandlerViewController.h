//
//  KeyboardHandlerViewController.h
//
//  Created by Abhinav Singh on 08/10/15.
//

#import "WLTViewController.h"

@interface KeyboardHandlerViewController : WLTViewController {
    
    UIEdgeInsets beforeEdgeInsets;
    CGRect keyboardEndFrame;
    
    __weak IBOutlet UIScrollView *theScrollView;
    
    UIView *currentActiveField;
}

-(void)subViewBecomeActive:(UIView*)subView;

-(void)keyboardWillShow:(NSNotification*)notify;
-(void)keyboardWillHide:(NSNotification *)notification;

@end
