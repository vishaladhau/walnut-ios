//
//  WLTGroupMemberDuesViewController.m
//  walnut
//
//  Created by Abhinav Singh on 01/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTGroupMemberDuesViewController.h"
#import "WLTGroupMemberDuesTableViewCell.h"
#import "WLTCombinedGroupPayments.h"
#import "WLTTransactionsListViewController.h"
#import "WLTTransactionUser.h"
#import "WLTEmptyStateView.h"
#import "WLTTableHeaderView.h"
#import "WLTOtherSettelmentTableCell.h"
#import "WLTOtherSettlement.h"
#import "WLTRootViewController.h"

typedef NS_ENUM(NSInteger, ControllerSectionType) {
    
    ControllerSectionTypeNone = 0,
    ControllerSectionTypeUser,
    ControllerSectionTypeOthers,
};

@interface WLTGroupMemberDuesViewController () <WLTGroupMemberDuesTableViewCellDelegate, WLTEmptyStateViewDelegate> {
    
    __weak WLTEmptyStateView *noTransUsersView;
}

@property(nonatomic, strong) NSArray *transactionUsers;
@property(nonatomic, strong) NSArray *transactionOthers;

@end

@implementation WLTGroupMemberDuesViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectZero];
    [usersTableView registerClass:[WLTGroupMemberDuesTableViewCell class] forCellReuseIdentifier:NSStringFromClass([WLTGroupMemberDuesTableViewCell class])];
    [usersTableView registerClass:[WLTOtherSettelmentTableCell class] forCellReuseIdentifier:@"WLTOtherSettelmentTableCell"];
    usersTableView.backgroundColor = self.view.backgroundColor;
    usersTableView.rowHeight = UITableViewAutomaticDimension;
    usersTableView.tableFooterView = footer;
    usersTableView.separatorInset = UIEdgeInsetsZero;
    
    WLTEmptyStateView *noTransUsersV = [WLTEmptyStateView emptyStateViewWithStyle:WLTEmptyStateViewStyleDefault andDelegate:self];
    noTransUsersV.userInteractionEnabled = NO;
    [noTransUsersV showTitle:@"Great!" andSubtitle:@"You have cleared all your dues" imageName:@"OweNothingImage"];
    noTransUsersV.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:noTransUsersV];
    
    noTransUsersView = noTransUsersV;
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(noTransUsersV);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[noTransUsersV]-0-|" options:0 metrics:nil views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[noTransUsersV]-55-|" options:0 metrics:nil views:dict]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(groupChanged:) name:NotifyGroupChanged object:nil];
    
    [self refreshUserData];
}

-(void)groupChanged:(NSNotification*)notify {
    
    WLTGroup *grp = notify.object;
    if (grp && [grp.groupUUID isEqualToString:self.currentGroup.groupUUID]) {
        
        [self refreshUserData];
    }
}

#pragma mark - UITableViewDelegate & UITableViewDataSource

-(ControllerSectionType)sectionTypeForIndex:(NSInteger)index {
    
    ControllerSectionType type = ControllerSectionTypeNone;
    if (index == 0) {
        if (self.transactionUsers.count) {
            type = ControllerSectionTypeUser;
        }
        else if (self.transactionOthers.count) {
            type = ControllerSectionTypeOthers;
        }
    }
    else if (index == 1) {
        if (self.transactionOthers.count) {
            type = ControllerSectionTypeOthers;
        }
    }
    
    return type;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    
    ControllerSectionType type = [self sectionTypeForIndex:indexPath.section];
    if (type == ControllerSectionTypeUser) {
        
        NSString *reuserIdentifier = @"WLTGroupMemberDuesTableViewCell";
        
        WLTGroupMemberDuesTableViewCell *tcell = [tableView dequeueReusableCellWithIdentifier:reuserIdentifier];
        tcell.delegate = self;
        WLTCombinedGroupPayments *trans = self.transactionUsers[indexPath.row];
        [tcell showDetailsOfPayment:trans];
        
        cell = tcell;
    }
    else if (type == ControllerSectionTypeOthers) {
        
        WLTOtherSettlement *other = self.transactionOthers[indexPath.row];
        
        WLTOtherSettelmentTableCell *tcell = [tableView dequeueReusableCellWithIdentifier:@"WLTOtherSettelmentTableCell"];
        [tcell showDetailsOfOtherSettelment:other];
        
        cell = tcell;
    }
    
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger sections = 0;
    if (self.transactionUsers.count) {
        sections += 1;
    }
    if (self.transactionOthers.count) {
        sections += 1;
    }
    return sections;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger count = 0;
    
    ControllerSectionType type = [self sectionTypeForIndex:section];
    if (type == ControllerSectionTypeUser) {
        
        count = self.transactionUsers.count;
    }
    else if (type == ControllerSectionTypeOthers) {
        
        count = self.transactionOthers.count;
    }
    
    return count;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    return 70;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

-(void)groupMemberDuesCellClickedRemindButton:(WLTGroupMemberDuesTableViewCell *)cell{
    
    NSIndexPath *indexPath = [usersTableView indexPathForCell:cell];
    [self takeEditActionOnUserTableViewAtIndexPath:indexPath identifier:EditCellIdentifierRemind];
}

-(void)groupMemberDuesCellClickedActionButton:(WLTGroupMemberDuesTableViewCell*)cell {
    
    NSIndexPath *indexPath = [usersTableView indexPathForCell:cell];
    if (cell.displayedTransaction.transactionState == UserTransactionStateGet) {
        
        [self takeEditActionOnUserTableViewAtIndexPath:indexPath identifier:EditCellIdentifierSettle];
    }
    else if (cell.displayedTransaction.transactionState == UserTransactionStateOwe) {
        
        [self takeEditActionOnUserTableViewAtIndexPath:indexPath identifier:EditCellIdentifierPay];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return [WLTTableHeaderView defaultHeight];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    WLTTableHeaderView *header = [[WLTTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.width, [WLTTableHeaderView defaultHeight])];
    
    ControllerSectionType type = [self sectionTypeForIndex:section];
    if (type == ControllerSectionTypeUser) {
        
        header.label.text = @"YOUR SETTLEMENTS";
    }
    else if (type == ControllerSectionTypeOthers) {
        
        header.label.text = @"OTHER SETTLEMENTS";
    }
    
    return header;
}

#pragma mark - Actions

-(void)refreshUserData {
    
    NSArray *users = nil;
    UserTransactionState state = [self.currentGroup userTransactionState];
    if ( state == UserTransactionStateGet) {
        
        users = self.currentGroup.userGetFrom.allObjects;
        self.title = @"You Get";
    }else if ( state == UserTransactionStateOwe) {
        
        users = self.currentGroup.userPayTo.allObjects;
        self.title = @"You Owe";
    }else {
        
        self.title = @"No Dues";
    }
    
    NSMutableArray *newData = [NSMutableArray new];
    for ( WLTTransactionUser *usr in users ) {
        
        WLTCombinedGroupPayments *com = [[WLTCombinedGroupPayments  alloc] initWithUser:usr];
        [com addUserData:usr forGroup:self.currentGroup];
        [com refreshCombinedData];
        [newData addObject:com];
    }
    
    self.transactionUsers = [newData sortedArrayUsingSelector:@selector(amountCompare:)];
    self.transactionOthers = self.currentGroup.otherSettlements.allObjects;
    
    if (!self.transactionUsers.count && !self.transactionOthers.count) {
        noTransUsersView.hidden = NO;
    }else {
        noTransUsersView.hidden = YES;
    }
    
    [usersTableView reloadData];
}

-(void)takeEditActionOnUserTableViewAtIndexPath:(NSIndexPath*)path identifier:(NSString*)identifier {
    
    WLTCombinedGroupPayments *com = self.transactionUsers[path.row];
    
    if ([identifier isEqualToString:EditCellIdentifierSettle] || [identifier isEqualToString:EditCellIdentifierPay]) {
        
        BOOL showSplitScreen = NO;
        if ([[WLTDatabaseManager sharedManager] doesCurrentUserHaveMultipleGroupTransactionsWithUser:com.user]) {
            showSplitScreen = YES;
        }
        
        if (showSplitScreen) {
            
            [self showCombinedTransactionsForUser:com.user privacyStatus:WLTCombinedGroupStatusBoth];
        }else if(com.transactionState == UserTransactionStateGet) {
            
            __weak typeof(self) weakSelf = self;
            [self settleForCombinedTransaction:com completion:^(BOOL success) {
                
                if (success) {
                    
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }
            }];
        }
        else if(com.transactionState == UserTransactionStateOwe) {
            
            [self trackEvent:GA_ACTION_WPAY_CLICKED andValue:nil];
            [self showPaymentScreenForTransaction:com message:nil receiverMessage:nil andOldTrans:nil withCompletion:nil];
        }
    }
    else if ([identifier isEqualToString:EditCellIdentifierCall]) {
        
        NSString *num = [com.user completeNumber];
        [self callToNumber:num];
    }
    else if ([identifier isEqualToString:EditCellIdentifierRemind]) {
        
        [self showReminderScreenForPayment:com];
    }
}

#pragma mark - EmptyState View

-(void)emptyStateViewReloadScreen:(WLTEmptyStateView*)view {}

-(void)emptyStateViewActionButtonTapped:(WLTEmptyStateView*)view {}

@end