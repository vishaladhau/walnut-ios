//
//  WLTGroupContactsViewController.h
//  walnut
//
//  Created by Abhinav Singh on 02/09/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTContactsViewController.h"

typedef NS_ENUM(NSInteger, GroupContactSelectionType) {
    
    GroupContactSelectionTypeUnknown = 0,
    GroupContactSelectionTypeNew,
    GroupContactSelectionTypeEdit,
};

@interface WLTGroupContactsViewController : WLTContactsViewController {
    
}

@property(nonatomic, strong) NSString *selectedColorID;
@property(nonatomic, assign) GroupContactSelectionType contactSelectionType;

@property(nonatomic, strong) NSMutableArray *selectedContacts;

@end
