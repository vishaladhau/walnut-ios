//
//  WLTSplitTransactionDetailsViewController.m
//  walnut
//
//  Created by Abhinav Singh on 6/10/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTSplitTransactionDetailsViewController.h"
#import "LSButton.h"
#import "WLTContactsManager.h"
#import "WLTSplitViewController.h"
#import "WLTTransactionUser.h"
#import "WLTSplitDetailsInfoTableViewCell.h"
#import "WLTSplitMembersTableViewCell.h"
#import "WLTGroupsManager.h"

@interface WLTSplitTransactionDetailsViewController () {
	
}

@end

@implementation WLTSplitTransactionDetailsViewController

- (void)viewDidLoad {
	
    [super viewDidLoad];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(groupChanged:) name:NotifyGroupChanged object:nil];
    
    self.title = self.transaction.placeName;
    
	if ([self currentUserCanEdit]) {
		
		UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"EditIcon"]
																	  style:UIBarButtonItemStyleDone
																	 target:self
																	 action:@selector(editTransactionClicked:)];
		self.navigationItem.rightBarButtonItem = rightItem;
	}
	
    [theTableView registerNib:[UINib nibWithNibName:@"WLTSplitMembersTableViewCell" bundle:nil] forCellReuseIdentifier:@"WLTSplitMembersTableViewCell"];
    [theTableView registerNib:[UINib nibWithNibName:@"WLTSplitDetailsInfoTableViewCell" bundle:nil] forCellReuseIdentifier:@"WLTSplitDetailsInfoTableViewCell"];
    [theTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
    
	theTableView.rowHeight = UITableViewAutomaticDimension;
    theTableView.backgroundColor = self.view.backgroundColor;
	theTableView.separatorInset = UIEdgeInsetsZero;
}

-(void)editTransactionClicked:(UIBarButtonItem*)item {
	
    NSMutableDictionary *grpMemMapping = [NSMutableDictionary new];
    for ( WLTUser *usr in self.group.members ) {
        NSString *mobString = usr.mobileString;
        grpMemMapping[mobString] = usr;
    }
    
    NSMutableDictionary *splitMemMapping = [NSMutableDictionary new];
    for ( WLTTransactionUser *mem in self.transaction.splits ) {
        NSString *mobString = mem.mobileString;
        splitMemMapping[mobString] = mem;
    }
    
	NSMutableArray *newMemArray = [NSMutableArray new];
    NSMutableArray *removedMemArray = [NSMutableArray new];
    
    for ( NSString *grpUsersMob in grpMemMapping ) {
        
        WLTTransactionUser *mem = splitMemMapping[grpUsersMob];
        
        WLTSplitUsers *newusr = [[WLTSplitUsers alloc] init];
        newusr.amount = mem.amount;
        newusr.member = grpMemMapping[grpUsersMob];
        if (!newusr.amount) {
            newusr.amount = @(0);
        }
        
        if ([grpUsersMob isEqualToString:CURRENT_USER_MOB]) {
            [newMemArray insertObject:newusr atIndex:0];
        }else {
            [newMemArray addObject:newusr];
        }
        
        splitMemMapping[grpUsersMob] = nil;
    }
    
    for ( NSString *removedUsersMob in splitMemMapping ) {
        
        WLTTransactionUser *mem = splitMemMapping[removedUsersMob];
        
        WLTSplitUsers *newusr = [[WLTSplitUsers alloc] init];
        newusr.amount = mem.amount;
        newusr.member = mem;
        
        [removedMemArray addObject:newusr];
    }
	
	long long dateStamp = [self.transaction.transactionDate timeStamp];
	
	GTLWalnutMTransaction *trans = [[GTLWalnutMTransaction alloc] init];
	trans.objType = self.transaction.type;
	trans.objUuid = self.transaction.objID;
	trans.txnUuid = self.transaction.txnID;
	trans.deviceUuid = [WLTDatabaseManager sharedManager].appSettings.deviceIdentifier;
	trans.groupUuid = self.group.groupUUID;
	trans.txnDate = @(dateStamp);
	
	trans.amount = self.transaction.amount;
	trans.notes = self.transaction.notes;
	trans.placeName = self.transaction.placeName;
	
	GTLWalnutMSplitUser *newUs = [[GTLWalnutMSplitUser alloc] init];
	newUs.mobileNumber = self.transaction.owner.mobileString;
	newUs.name = [self.transaction.owner name];
	trans.owner = newUs;
	
	GTLWalnutMSplitUser *newUsOwner = [[GTLWalnutMSplitUser alloc] init];
	newUsOwner.mobileNumber = self.transaction.addedBy.mobileString;
	newUsOwner.name = self.transaction.addedBy.name;
	trans.addedBy = newUsOwner;
	
	WLTSplitViewController *controller = [self controllerWithIdentifier:@"WLTSplitViewController"];
	controller.newTransaction = NO;
	controller.transaction = trans;
	controller.allMembers = newMemArray;
    controller.removedMembers = removedMemArray;
	controller.group = self.group;
	controller.navBarColor = self.navBarColor;
	
	__weak typeof(self) weakSelf = self;
	[controller setCompletionBlock:^(BOOL success){
		if (success) {
			
			[weakSelf.navigationController popViewControllerAnimated:YES];
		}
	}];
	
	[self.navigationController pushViewController:controller animated:YES];
}

-(BOOL)currentUserCanEdit {
	
	BOOL edit = NO;
	NSString *currenUser = CURRENT_USER_MOB;
	if ([self.transaction.owner.mobileString isEqualToString:currenUser] || [self.transaction.addedBy.mobileString isEqualToString:currenUser]) {
		edit = YES;
	}
	
	return edit;
}

-(void)groupChanged:(NSNotification*)changeNotify {
	
	WLTGroup *changed = changeNotify.object;
	if ([changed.groupUUID isEqualToString:self.group.groupUUID]) {
        
		[theTableView reloadData];
	}
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    UITableViewCell *cell = nil;
    if (indexPath.section == 0) {
        
        NSString *reuserIdentifier = @"WLTSplitDetailsInfoTableViewCell";
        
        WLTSplitDetailsInfoTableViewCell *tcell = [tableView dequeueReusableCellWithIdentifier:reuserIdentifier];
        [tcell showDetailsOfTransaction:self.transaction];
        
        cell = tcell;
    }
	else if (indexPath.section == 1) {
        
        NSString *reuserIdentifier = @"WLTSplitMembersTableViewCell";
        
        WLTSplitMembersTableViewCell *tcell = [tableView dequeueReusableCellWithIdentifier:reuserIdentifier];
        
        WLTTransactionUser *user = self.transaction.splits.allObjects[indexPath.row];
        [tcell showDetailsOfTransactionUser:user];
        
        cell = tcell;
    }
    else if (indexPath.section == 2) {
        
        NSString *reuserIdentifier = @"UITableViewCell";
        UITableViewCell *tcell = [tableView dequeueReusableCellWithIdentifier:reuserIdentifier];
        tcell.textLabel.textAlignment = NSTextAlignmentCenter;
        tcell.textLabel.text = @"Delete Transaction";
        tcell.textLabel.textColor = [UIColor pomegranateColor];
        
        cell = tcell;
    }
    
	return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger count = 2;
    if ([self currentUserCanEdit]) {
        count += 1;
    }
    
	return count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	NSInteger count = 0;
    if (section == 0) {
        count = 1;
    }else if (section == 1) {
        count = self.transaction.splits.count;
    }else if ([self currentUserCanEdit] && (section == 2)) {
        count = 1;
    }
    
	return count;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
	
	return 70;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
    if ([self currentUserCanEdit] && (indexPath.section == 2)) {
        
        __weak typeof(self) weakSelf = self;
        
        UIAlertController *cont = [UIAlertController alertControllerWithTitle:@"Please Confirm!" message:@"Are you sure you want to delete this split bill?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAct = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }];
        
        UIAlertAction *deleteAct = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
            [weakSelf deleteTransaction];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }];
        
        [cont addAction:cancelAct];
        [cont addAction:deleteAct];
        
        [self presentViewController:cont animated:YES completion:nil];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        
        return 0.1;
    }
    
    return 30;
}

#pragma mark - Button Clicks

-(void)deleteTransaction{
	
	GTLWalnutMTransaction *ttrans = [[GTLWalnutMTransaction alloc] init];
	ttrans.deleted = @(YES);
	ttrans.deviceUuid = [WLTDatabaseManager sharedManager].appSettings.deviceIdentifier;
	ttrans.txnUuid = self.transaction.txnID;
	ttrans.objUuid = self.transaction.objID;
	ttrans.groupUuid = self.group.groupUUID;
	
	GTLWalnutMSplitUser *owner = [[GTLWalnutMSplitUser alloc] init];
	owner.name = self.transaction.owner.name;
	owner.mobileNumber = self.transaction.owner.mobileString;
	ttrans.owner = owner;
	
    GTLWalnutMSplitUser *addedBy = [[GTLWalnutMSplitUser alloc] init];
    addedBy.name = self.transaction.addedBy.name;
    addedBy.mobileNumber = self.transaction.addedBy.mobileString;
    ttrans.addedBy = addedBy;
    
	GTLQueryWalnut *deleteCall = [GTLQueryWalnut queryForTransactionUpdateWithObject:ttrans];
	[self startLoading];
	
	__weak WLTGroup *weakGroup = self.group;
	__weak typeof(self) weakSelf = self;
	[[WLTNetworkManager sharedManager].walnutService executeQuery:deleteCall completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
		
		if (!error) {
			
            [weakSelf.group removeTransactionsObject:weakSelf.transaction];
            [[WLTDatabaseManager sharedManager] deleteTransactionFromDataBase:weakSelf.transaction];
            
            weakSelf.transaction = nil;
            
			[[WLTGroupsManager sharedManager] fetchDetailsOfGroup:weakGroup completion:^(WLTGroup *grp, NSError *error) {
				
				if (error) {
					
					[weakSelf showAlertForError:error];
				}else {
					
					[weakSelf trackEvent:GA_ACTION_SPLIT_DELETED label:[weakSelf screenName] andValue:@(1)];
					[[NSNotificationCenter defaultCenter] postNotificationName:NotifyGroupChanged object:weakGroup];
					
					if (weakSelf.completion) {
						weakSelf.completion();
					}
				}
				
				[weakSelf endViewLoading];
			}];
		}else {
			
			[weakSelf showAlertForError:error];
			[weakSelf endViewLoading];
		}
	}];
}

@end
