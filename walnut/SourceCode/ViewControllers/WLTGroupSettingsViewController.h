//
//  WLTGroupSettingsViewController.h
//  walnut
//
//  Created by Abhinav Singh on 25/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTCreateNewGroupViewController.h"
#import "WLTGroup.h"
#import "LSButton.h"

@interface WLTGroupSettingsViewController : WLTCreateNewGroupViewController <UITableViewDelegate, UITableViewDataSource>{
    
//    BOOL shouldEnterNewName;
}

@property(nonatomic, strong) WLTGroup *toEdit;

@end
