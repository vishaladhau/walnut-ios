//
//  WLTSettingsViewController.h
//  walnut
//
//  Created by Abhinav Singh on 27/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"
#import "WLTUserIconButton.h"

@interface WLTSettingsViewController : WLTViewController <UITableViewDataSource, UITableViewDelegate>{
    
    __weak IBOutlet WLTUserIconButton *userIconButton;
    __weak IBOutlet UILabel *mobNumberLabel;
    __weak IBOutlet UILabel *nameLabel;
    
    __weak IBOutlet UITableView *theTableView;
    __weak IBOutlet UIImageView *editImageView;
    
    UITabBarItem *tabItem;
}

-(void)fetchWhatsNewWithCompletion:(DataCompletionBlock)block;

@end
