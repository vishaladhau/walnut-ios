//
//  WLTGroupPaymentAmountEditViewController.h
//  walnut
//
//  Created by Abhinav Singh on 20/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"

@class WLTPartialGroup;
@class WLTPartial;

typedef void (^ PayEditedBlock)(WLTPartial *edited);

@interface WLTGroupAmountView : UIView {
    
    __weak UIView *groupColorView;
    __weak UILabel *groupNameLabel;
    __weak UILabel *amountLabel;
}

@property(readonly, strong) WLTPartialGroup *displayed;

@end

@interface WLTGroupPaymentAmountEditViewController : WLTViewController {
    
    __weak IBOutlet UIScrollView *theScrollView;
    __weak IBOutlet UIView *headerView;
    __weak IBOutlet UILabel *headerInfoLabel;
    __weak IBOutlet UITextField *amountTextField;
    
    __weak IBOutlet NSLayoutConstraint *widthConstraint;
    
    NSMutableArray *allScrollPaySubViews;
}

@property(nonatomic, strong) PayEditedBlock completion;
@property(nonatomic, strong) WLTPartial *transaction;
@property (nonatomic, assign) BOOL settling;
@end
