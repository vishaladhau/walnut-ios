//
//  WLTKeyValueAmountViewController.h
//  walnut
//
//  Created by Abhinav Singh on 02/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"

@interface WLTKeyValueAmountViewController : WLTViewController <UITableViewDelegate, UITableViewDataSource> {
    
    __weak IBOutlet UITableView *theTableView;
}

@property(nonatomic, strong) NSArray *dataArray;

@end
