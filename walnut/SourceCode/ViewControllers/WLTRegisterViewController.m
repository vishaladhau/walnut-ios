//
//  WLTRegisterViewController.m
//  walnut
//
//  Created by Abhinav Singh on 02/08/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTRegisterViewController.h"
#import "WLTOTPViewController.h"

@interface WLTRegisterViewController () <UITextFieldDelegate>{
    
    NSCharacterSet *notNumeric;
}

@end

@implementation WLTRegisterViewController

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [mobTextField becomeFirstResponder];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    notNumeric = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    
    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:@"Next"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(nextClicked:)];
    self.navigationItem.rightBarButtonItem = nextButton;
    
    {
        mobTextBackView.backgroundColor = [UIColor whiteColor];
        [mobTextBackView addBorderLineAtPosition:BorderLinePositionTop];
        
        contCodeLabel.textColor = [UIColor cadetGrayColor];
        contCodeLabel.font = [UIFont sfUITextMediumOfSize:16];
        
        mobTextField.textColor = [UIColor cadetColor];
        mobTextField.delegate = self;
        mobTextField.font = [UIFont sfUITextMediumOfSize:16];
        mobTextField.placeholder = @"Enter phone number";
    }
    
    {
        fNameBackView.backgroundColor = [UIColor whiteColor];
        [fNameBackView addBorderLineAtPosition:(BorderLinePositionTop|BorderLinePositionBottom)];
        
        fNameTextField.textColor = [UIColor cadetColor];
        fNameTextField.font = [UIFont sfUITextMediumOfSize:16];
        fNameTextField.placeholder = @"Name";
        fNameTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    }
    
    {
        if (self.isSignIn) {
            
            self.title = @"Sign In";
            emailBackView.hidden = YES;
        }else {
            
            self.title = @"Register";
            
            emailBackView.backgroundColor = [UIColor whiteColor];
            [emailBackView addBorderLineAtPosition:BorderLinePositionBottom];
            
            emailTextField.textColor = [UIColor cadetColor];
            emailTextField.font = [UIFont sfUITextMediumOfSize:16];
            emailTextField.placeholder = @"Email address (optional)";
            emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        }
    }
}

-(IBAction)jumpToNextField:(UITextField*)field {
    
    if ([mobTextField isFirstResponder]) {
        [fNameTextField becomeFirstResponder];
    }
    else if ([fNameTextField isFirstResponder]) {
        if (!emailBackView.isHidden) {
            [emailTextField becomeFirstResponder];
        }
    }
}

-(void)nextClicked:(UIBarButtonItem*)item {
    
    if (self.isLoading) {
        return;
    }
    
    NSError *validationError = nil;
    
    NSCharacterSet *toRemoveCharSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    NSString *fname = [fNameTextField.text stringByTrimmingCharactersInSet:toRemoveCharSet];
    
    if (mobTextField.text.length != IndianMobileLength) {
        
        validationError = [NSError walnutErrorWithMessage:@"Please enter a valid mobile number"];
    }else if (!fname.length) {
        
        validationError = [NSError walnutErrorWithMessage:@"Please enter a valid first name"];
    }
    else if (emailTextField.text.length && ![emailTextField.text isValidateEmailAddress]){
        
        validationError = [NSError walnutErrorWithMessage:@"Please enter a valid email address!"];
    }
    
    if (!validationError) {
        
        [self startLoading];
        
        NSString *mob = [NSString stringWithFormat:@"+91%@", mobTextField.text];
        NSString *name = fname;
        NSString *emailAdd = emailTextField.text;
        
        WLTLoginTempUser *newUser = [[WLTLoginTempUser alloc] init];
        newUser.emailAddress = emailAdd;
        newUser.userName = name;
        newUser.mobileNumber = mob;
        
        __weak typeof(self) weakSelf = self;
        
        [[WLTNetworkManager sharedManager] getOTPForUser:newUser completion:^(id data) {
            
            if ([data isKindOfClass:[NSError class]]) {
                
                [weakSelf showAlertForError:data];
            }else {
                
                [weakSelf showOTPScreenForUser:newUser];
            }
            
            [weakSelf endViewLoading];
        }];
    }else {
        
        [self showAlertForError:validationError];
    }
}

-(void)showOTPScreenForUser:(WLTLoginTempUser*)user{
    
    __weak typeof(self) weakSelf = self;
    WLTOTPViewController *otpCont = [self controllerWithIdentifier:@"WLTOTPViewController"];
    otpCont.userObject = user;
    [otpCont setCompletion:^(WLTLoginTempUser *user){
        
        if (user) {
            if (weakSelf.completion) {
                weakSelf.completion(user);
            }
        }else {
            
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
    
    [self.navigationController pushViewController:otpCont animated:YES];
}

#pragma mark - UITextFieldDelegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL canChange = NO;
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([string rangeOfCharacterFromSet:notNumeric].location == NSNotFound) {
        if (newString.length < IndianMobileLength) {
            
            canChange = YES;
        }else if (newString.length == IndianMobileLength) {
            
            textField.text = newString;
            [fNameTextField becomeFirstResponder];
        }
    }
    
    return canChange;
}

@end
