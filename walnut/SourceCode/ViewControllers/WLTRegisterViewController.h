//
//  WLTRegisterViewController.h
//  walnut
//
//  Created by Abhinav Singh on 02/08/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"

@interface WLTRegisterViewController : WLTViewController {
    
    __weak IBOutlet UILabel *contCodeLabel;
    __weak IBOutlet UITextField *mobTextField;
    __weak IBOutlet UIView *mobTextBackView;
    
    __weak IBOutlet UITextField *fNameTextField;
    __weak IBOutlet UIView *fNameBackView;
    
    __weak IBOutlet UITextField *emailTextField;
    __weak IBOutlet UIView *emailBackView;
}

@property(nonatomic, strong) LoginBlock completion;
@property(nonatomic, assign) BOOL isSignIn;

@end
