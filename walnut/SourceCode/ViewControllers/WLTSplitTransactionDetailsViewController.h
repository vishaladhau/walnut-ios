//
//  WLTSplitTransactionDetailsViewController.h
//  walnut
//
//  Created by Abhinav Singh on 6/10/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"
#import "WLTTransaction.h"
#import "WLTGroup.h"

@interface WLTSplitTransactionDetailsViewController : WLTViewController <UITableViewDelegate, UITableViewDataSource>{
	
	__weak IBOutlet UITableView *theTableView;
}

@property(nonatomic, strong) WLTTransaction *transaction;
@property(nonatomic, strong) WLTGroup *group;

@property(nonatomic, strong) CompletionBlock completion;

@end
