//
//  WLTRootViewController.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 02/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

FOUNDATION_EXPORT NSString *const WalnutAPIKey;

@class WLTHomeViewController;

@interface WLTRootViewController : UIViewController {
    
}

@property(nonatomic, weak) WLTHomeViewController *homeController;

//-(void)logoutCurrentUserAndShowOnBoarding;

-(void)handleLocalNotification:(NSDictionary*)pushDict showAlert:(BOOL)showAlert;
-(void)handleRemoteNotification:(NSDictionary*)pushDict fetchHandler:(void (^)(UIBackgroundFetchResult))handler;

-(UIViewController*)visibleTopViewController;

@end
