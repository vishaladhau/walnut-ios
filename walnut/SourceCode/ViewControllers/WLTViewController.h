//
//  WLTViewController.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 04/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WLTDatabaseManager.h"
#import "WLTNetworkManager.h"
#import <Google/Analytics.h>

@interface WLTTitleView : UIView {
    
    __weak NSLayoutConstraint *gapConstraint;
@public
    __weak UILabel *titleLabel;
    __weak UILabel *subTitleLabel;
}

@end

@interface WLTViewController : GAITrackedViewController <UINavigationControllerDelegate>{
    
    @public
    WLTTitleView *titleView;
}

@property(nonatomic, strong) UIColor *navBarColor;
@property(nonatomic, assign) BOOL isLoading;

-(void)startLoading;
-(void)endViewLoading;
-(void)startLoadingWithColor:(UIColor*)color;

-(BOOL)needNavigationBar;

-(void)setTitle:(NSString *)title andSubtitle:(NSString*)sub;
-(void)setAttributedTitle:(NSAttributedString *)title andSubTitle:(NSString*)sub;
-(void)setTitleForGroup:(WLTGroup *)grp andSubtitle:(NSString*)sub;
-(void)setMembersTitleForGroup:(WLTGroup *)grp checkDirect:(BOOL)checkDirect;

@end
