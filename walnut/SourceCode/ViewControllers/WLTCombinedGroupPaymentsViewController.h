//
//  WLTCombinedGroupPaymentsViewController.h
//  walnut
//
//  Created by Abhinav Singh on 02/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"
#import "WLTGroup.h"


typedef NS_ENUM(NSInteger, WLTCombinedGroupPaymentsType) {
    
    WLTCombinedGroupPaymentsTypeUnknown = 0,
    WLTCombinedGroupPaymentsTypeGroup,
    WLTCombinedGroupPaymentsTypeUser,
};

@interface WLTCombinedGroupPaymentsViewController : WLTViewController {
    
    __weak IBOutlet UIScrollView *theScrollView;
    
    __weak IBOutlet UIView *headerView;
    __weak IBOutlet UILabel *amountLabel;
    __weak IBOutlet UILabel *headerInfoLabel;
    
    __weak IBOutlet NSLayoutConstraint *headerOriginY;
    
    BOOL canShowHeader;
    
}

-(void)showDataOfGroups:(NSArray*)grps forState:(UserTransactionState)state;
-(void)showDataForUser:(id<User>)user privacyStatus:(WLTCombinedGroupStatus)privacyStatus;

@property(nonatomic, strong) SuccessBlock completion;

@end
