//
//  WLTReminderTextViewController.h
//  walnut
//
//  Created by Abhinav Singh on 19/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"
#import "WLTCombinedGroupPayments.h"
#import "PlaceholderTextView.h"

@interface WLTReminderTextViewController : WLTViewController {
    
    __weak IBOutlet PlaceholderTextView *reminderTextView;
    __weak IBOutlet NSLayoutConstraint *bottomMargin;
}

@property(nonatomic, strong) WLTCombinedGroupPayments *transaction;
@property(nonatomic, strong) SuccessBlock completionBlock;

@end
