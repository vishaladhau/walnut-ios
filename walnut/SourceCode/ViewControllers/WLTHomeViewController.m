//
//  WLTHomeViewController.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 03/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTHomeViewController.h"
#import "WLTGroupCollectionViewCell.h"
#import "WLTGroup.h"
#import "WLTGroupDetailViewController.h"
#import "WLTColorPalette.h"
#import "WLTGroupSettingsViewController.h"
#import "WLTSettingsViewController.h"
#import "GTLQueryWalnut.h"
#import "WLTGroupListViewController.h"
#import "WLTHomeScreenAnimator.h"
#import "WLTPaymentTransactionsViewController.h"
#import "WLTPaymentTransationsManager.h"

@interface WLTHomeViewController () {
    
    __weak UITabBarController *tabController;
}

@end

@implementation WLTHomeViewController

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paymentStatusChanged:) name:NotifyPaymentsStatusChanged object:nil];
    
    WLTGroupListViewController *grpListCont = [self controllerWithIdentifier:@"WLTGroupListViewController"];
    [self enableInteractivePopGesture:YES forViewController:grpListCont];
    UINavigationController *grpNav = [[UINavigationController alloc] initWithRootViewController:grpListCont];
    grpNav.delegate = grpListCont;
    [grpNav applyDefaultStyle];
    
    WLTSettingsViewController *settingsCont = [self controllerWithIdentifier:@"WLTSettingsViewController"];
    [self enableInteractivePopGesture:YES forViewController:settingsCont];
    UINavigationController *settNav = [[UINavigationController alloc] initWithRootViewController:settingsCont];
    settNav.delegate = settingsCont;
    [settNav applyTranslusentStyle];
    
    self.groupListController = grpListCont;
    self.settingsController = settingsCont;
    
    [self.settingsController fetchWhatsNewWithCompletion:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor cadetColor]}
                                             forState:UIControlStateNormal];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor appleGreenColor]}
                                             forState:UIControlStateSelected];
    
    UITabBarController *tabCont = [[UITabBarController alloc] init];
    tabCont.view.autoresizingMask = (UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight);
    tabCont.tabBar.itemSpacing = 0;
    [tabCont.tabBar setBarStyle:UIBarStyleDefault];
    [tabCont.tabBar setTranslucent:YES];
    [tabCont.tabBar setSelectionIndicatorImage:nil];
    tabCont.view.frame = self.view.bounds;
    
    [self.view addSubview:tabCont.view];
    [self addChildViewController:tabCont];
    [tabCont willMoveToParentViewController:self];
    
    tabController = tabCont;
    
    [self changeTabbarControllerAnimated:NO];
}

-(void)appBecomeActive {
    
    [[WLTPaymentTransationsManager sharedManager] fetchAllPaymentTransactionsCompletion:nil];
}

-(WLTViewController*)visibleController {
    
    UINavigationController *navCont = [tabController selectedViewController];
    WLTViewController *top = (WLTViewController*)[navCont topViewController];
    if (![top isKindOfClass:[WLTViewController class]]) {
        top = nil;
    }
    
    return top;
}

-(void)changeTabbarControllerAnimated:(BOOL)animate {
    
    NSMutableArray *allcontrollers = [NSMutableArray new];
    [allcontrollers addObject:self.groupListController.navigationController];
    
    if (PaymentsEnabled()) {
        
        WLTPaymentTransactionsViewController *payTrans = [self controllerWithIdentifier:@"WLTPaymentTransactionsViewController"];
        [self enableInteractivePopGesture:YES forViewController:payTrans];
        UINavigationController *payTransNav = [[UINavigationController alloc] initWithRootViewController:payTrans];
        payTransNav.delegate = payTrans;
        [payTransNav applyTranslusentStyle];
        
        [allcontrollers addObject:payTransNav];
        _paymentListController = payTrans;
    }else {
        
        _paymentListController = nil;
    }
    
    [allcontrollers addObject:self.settingsController.navigationController];
    [tabController setViewControllers:allcontrollers animated:animate];
}

-(void)paymentStatusChanged:(NSNotification*)notify {
    
    [self changeTabbarControllerAnimated:YES];
}

-(BOOL)isShowingDetailsOfGroup:(WLTGroup*)grp {
    
    BOOL showing = NO;
    if (tabController.selectedIndex == 0) {
        
        UIViewController *topCont = self.groupListController.navigationController.topViewController;
        if([topCont isKindOfClass:[WLTGroupDetailViewController class]]) {
            
            WLTGroupDetailViewController *detailCont = (WLTGroupDetailViewController*)topCont;
            if ([detailCont.currentGroup.groupUUID isEqualToString:grp.groupUUID]) {
                showing = YES;
            }
        }
    }
    
    return showing;
}

-(BOOL)isShowingGroupList {
    
    BOOL showing = NO;
    if (tabController.selectedIndex == 0) {
        
        UIViewController *topCont = self.groupListController.navigationController.topViewController;
        if([topCont isKindOfClass:[WLTGroupListViewController class]]) {
            
            showing = YES;
        }
    }
    
    return showing;
}

-(void)enableInteractivePopGesture:(BOOL)enable forViewController:(UIViewController*)viewControllerToUpdate {
	
    if ([viewControllerToUpdate.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        
        if(enable) {
			
            viewControllerToUpdate.navigationController.interactivePopGestureRecognizer.enabled = enable;
            viewControllerToUpdate.navigationController.interactivePopGestureRecognizer.delegate = nil;
        } else {
			
            viewControllerToUpdate.navigationController.interactivePopGestureRecognizer.enabled = enable;
        }
    }
}

@end
