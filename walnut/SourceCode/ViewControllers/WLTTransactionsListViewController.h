//
//  WLTTransactionsListViewController.h
//  walnut
//
//  Created by Abhinav Singh on 02/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"

@class WLTCombinedGroupPayments;

@interface WLTTransactionsListViewController : WLTViewController {
    
    __weak IBOutlet UITableView *theTableView;
}

@property(nonatomic, strong) WLTCombinedGroupPayments *paymentGroup;

@end
