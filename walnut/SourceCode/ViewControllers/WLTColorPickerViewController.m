//
//  WLTColorPickerViewController.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 07/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTColorPickerViewController.h"
#import "WLTColorPalette.h"
#import "WLTColorCollectionViewCell.h"

@interface WLTColorPickerViewController () {
    
    CGSize cellSize;
    CGFloat spacing;
    
    NSArray *allAvailableColor;
}

@end

@implementation WLTColorPickerViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"Group Colour";
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat coloms = 3;
    if(screenWidth > 320) {
        coloms = 4;
    }
    
    spacing = 10;

    CGFloat width = (screenWidth - ((coloms+1)*spacing))/coloms;
    cellSize = CGSizeMake(width, width);

    allAvailableColor = [[WLTColorPalette sharedPalette] allColorIDs];
    theCollectionView.backgroundColor = [UIColor whiteColor];
    

    [theCollectionView registerNib:[UINib nibWithNibName:@"WLTColorCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"WLTColorCollectionViewCell"];
}

#pragma mark PPTCategoryContainerViewDelegate

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    WLTColorCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WLTColorCollectionViewCell" forIndexPath:indexPath];
    
    NSString *clrId = allAvailableColor[indexPath.row];
    cell->backgroundImageView.tintColor = [[WLTColorPalette sharedPalette] colorForID:clrId];
    
    if (self.selectedColorID.length && [self.selectedColorID isEqualToString:clrId]) {
        [cell setSelected:YES];
    }else {
        [cell setSelected:NO];
    }
    
    return cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSInteger count = allAvailableColor.count;
    return count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return cellSize;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
       insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake( spacing, spacing, spacing, spacing);
}

-(CGFloat)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return spacing;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return spacing;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *clrId = allAvailableColor[indexPath.row];
    self.selectedColorID = clrId;
    
    if (self.completionBlock) {
        self.completionBlock(clrId);
    }
}

@end
