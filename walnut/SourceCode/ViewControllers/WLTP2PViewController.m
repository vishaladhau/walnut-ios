//
//  WLTP2PViewController.m
//  walnut
//
//  Created by Abhinav Singh on 11/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTP2PViewController.h"

@interface WLTP2PViewController ()

@end

@implementation WLTP2PViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"Payments";
    
    iconButton.layer.cornerRadius = 50;
    iconButton->nameLabel.font = [UIFont sfUITextMediumOfSize:30];
    [iconButton displayUserIcon:self.contact];
    
    [userNameLabel displayUserName:self.contact];
    
    [payButton setTitle:@"Pay"];
    payButton.layer.cornerRadius = DefaultCornerRadius;
    LSStateColor *normalClr = [[LSStateColor alloc] initWithBack:[UIColor oweColor]
                                                    contentColor:[UIColor lightBackgroundColor]
                                                    andBorderClr:nil];
    
    LSStateColor *highClr = [[LSStateColor alloc] initWithBack:[UIColor clearColor]
                                                  contentColor:[UIColor oweColor]
                                                  andBorderClr:[UIColor oweColor]];
    
    [payButton addColors:highClr forState:LSButtonStateHighlighted];
    [payButton addColors:normalClr forState:LSButtonStateNormal];
    
    [remindButton setTitle:@"Remind"];
    remindButton.layer.cornerRadius = DefaultCornerRadius;
    normalClr = [[LSStateColor alloc] initWithBack:[UIColor getColor]
                                                    contentColor:[UIColor lightBackgroundColor]
                                                    andBorderClr:nil];
    
    highClr = [[LSStateColor alloc] initWithBack:[UIColor clearColor]
                                                  contentColor:[UIColor getColor]
                                                  andBorderClr:[UIColor getColor]];
    
    [remindButton addColors:highClr forState:LSButtonStateHighlighted];
    [remindButton addColors:normalClr forState:LSButtonStateNormal];
}

-(IBAction)payClicked:(id)sender {}

-(IBAction)remindClicked:(id)sender {}

@end
