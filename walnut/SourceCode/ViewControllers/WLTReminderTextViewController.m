//
//  WLTReminderTextViewController.m
//  walnut
//
//  Created by Abhinav Singh on 19/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTReminderTextViewController.h"
#import "WLTContactsManager.h"
#import "WLTDatabaseManager.h"
#import "WLTNetworkManager.h"

@interface WLTReminderTextViewController () {
    
    
}

@property(nonatomic, strong) NSString *defaultPlaceHolderString;

@end

@implementation WLTReminderTextViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"Remind";
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self setEdgesForExtendedLayout:UIRectEdgeAll];
    
    [self addBackBarButtonWithSelector:@selector(backClicked:)];
    
    self.navigationItem.leftBarButtonItem.customView.tintColor = self.navigationController.navigationBar.tintColor;
    
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                 style:UIBarButtonItemStyleDone
                                                                target:self action:@selector(doneClicked:)];
    self.navigationItem.rightBarButtonItem = doneItem;
    
    [reminderTextView setTextColor:[UIColor cadetColor]];
    [reminderTextView setFont:[UIFont sfUITextRegularOfSize:16]];
    
	__weak typeof(self) weakSelf = self;
    [[WLTContactsManager sharedManager] displayNameForUser:self.transaction.user defaultIsMob:NO completion:^(NSString *data) {
		
		if (weakSelf) {
			if (!data) {
				data = @"Friend";
			}
            
            weakSelf.defaultPlaceHolderString = [NSString stringWithFormat:@"Hey %@,\n\nLooks like you owe me %@\nLets settle? 😉", data, @(self.transaction.amount).displayCurrencyString];
            reminderTextView.text = weakSelf.defaultPlaceHolderString;
            
            if (weakSelf != nil) {
                typeof(self) sSelf = weakSelf;
                [sSelf->reminderTextView setPlaceHolderText:weakSelf.defaultPlaceHolderString];
            }
		}
    }];
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [reminderTextView becomeFirstResponder];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [reminderTextView resignFirstResponder];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)keyboardWillShow:(NSNotification*)notify {
    
    NSDictionary *userInfo = notify.userInfo;
    
    NSTimeInterval animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationOptions animationCurve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    CGRect keyboardScreenEndFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    CGRect keyboardViewEndFrame = [self.view convertRect:keyboardScreenEndFrame fromView:self.view.window];
    
    bottomMargin.constant = (keyboardViewEndFrame.size.height);
    
    UIViewAnimationOptions animationOptions = animationCurve | UIViewAnimationOptionBeginFromCurrentState;
    [UIView animateWithDuration:animationDuration delay:0 options:animationOptions animations:^{
        
        [self.view layoutIfNeeded];
    } completion:nil];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    bottomMargin.constant = 0;
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];
}

-(void)doneClicked:(UIBarButtonItem*)item {
    
    NSError *validateError = nil;
    ///User can send empty text.
//    if (!reminderTextView.text.length) {
//        validateError = [NSObject walnutErrorWithMessage:@"Please enter a valid message!"];
//    }
    
    if (!validateError) {
        
        GTLWalnutMSplitSettleReminder *reminder = [[GTLWalnutMSplitSettleReminder alloc] init];
        
        NSMutableArray *grps = [NSMutableArray new];
        for ( WLTGroupPaymentData *data in self.transaction.groupsData ) {
            [grps addObject:data.group.groupUUID];
        }
        
        reminder.deviceUuid = [WLTDatabaseManager sharedManager].appSettings.deviceIdentifier;
        reminder.groupUuids = grps;
        
        if (reminderTextView.text.length) {
            reminder.userMessage = reminderTextView.text;
        }else {
            reminder.userMessage = self.defaultPlaceHolderString;
        }
        
        GTLWalnutMSplitUser *toRemindUser = [[GTLWalnutMSplitUser alloc] init];
        toRemindUser.name = self.transaction.user.name;
        toRemindUser.mobileNumber = [self.transaction.user completeNumber];
        
        GTLWalnutMSplitUser *currentUser = [[GTLWalnutMSplitUser alloc] init];
        currentUser.name = [WLTDatabaseManager sharedManager].currentUser.name;
        currentUser.mobileNumber = CURRENT_USER_MOB;
        
        reminder.reminderTo = toRemindUser;
        reminder.reminderFrom = currentUser;
        
        __weak WLTReminderTextViewController *weakSelf = self;
        
        GTLQueryWalnut *query = [GTLQueryWalnut queryForSplitSettleReminderWithObject:reminder];
        
        [self startLoading];
        
        [[WLTNetworkManager sharedManager].walnutService executeQuery:query completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
            
            if (!error) {
                if (weakSelf.completionBlock) {
                    weakSelf.completionBlock(YES);
                }
                
                [weakSelf trackEvent:GA_ACTION_SEND_REMINDER andValue:@(weakSelf.transaction.amount)];
            }else {
                
                if ([NSObject checkAndShowToastIfNotGroupMember:error]) {
                    
                    if (weakSelf.completionBlock) {
                        weakSelf.completionBlock(NO);
                    }
                }else {
                    [weakSelf showAlertForError:error];
                }
            }
            
            [weakSelf endViewLoading];
        }];
    }
    
}

-(void)backClicked:(UIBarButtonItem*)item {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
