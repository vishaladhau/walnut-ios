//
//  WLTSplitViewController.h
//  walnut
//
//  Created by Abhinav Singh on 22/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"
#import "WLTUser.h"
#import "WLTGroup.h"
#import "KeyboardHandlerViewController.h"

@interface WLTSplitUsers : NSObject {
    
}

@property(nonatomic, strong) NSNumber *amount;
@property(nonatomic, strong) id <User> member;

@end

@interface WLTSplitViewController : KeyboardHandlerViewController {
    
    __weak IBOutlet UIView *headerView;
    __weak IBOutlet UILabel *amountLabel;
    __weak IBOutlet UILabel *totalLabel;
}

@property(nonatomic, assign) BOOL newTransaction;

@property(nonatomic, strong) GTLWalnutMTransaction *transaction;
@property(nonatomic, strong) WLTGroup *group;

@property(nonatomic, strong) NSArray *allMembers;
@property(nonatomic, strong) NSArray *removedMembers;

@property(nonatomic, strong) SuccessBlock completionBlock;

@end
