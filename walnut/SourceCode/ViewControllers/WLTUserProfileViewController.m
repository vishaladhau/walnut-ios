//
//  WLTUserProfileViewController.m
//  walnut
//
//  Created by Abhinav Singh on 02/03/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTUserProfileViewController.h"
#import "WLTLoggedInUser.h"
#import "WLTDatabaseManager.h"
#import "WLTNetworkManager.h"
#import "WLTRootViewController.h"
#import "WLTOTPViewController.h"
#import "WLTTargetSettings.h"

typedef NS_ENUM(NSInteger, ProfileTableSectionIdentifier) {
    
    ProfileTableSectionIdentifierName = 0,
    ProfileTableSectionIdentifierMobile,
    ProfileTableSectionIdentifierEmail,
    ProfileTableSectionIdentifierLogout,
};

@interface WLTUserProfileViewController () <UITextFieldDelegate>

@end

@implementation WLTUserProfileViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
	self.title = @"Account";
    
    theTableView = (UITableView*)theScrollView;
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectZero];
    theTableView.separatorInset = UIEdgeInsetsZero;
    theTableView.backgroundColor = [UIColor clearColor];
    theTableView.tableFooterView = footer;
    theTableView.separatorColor = [UIColor placeHolderTextColor];
    
    theScrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    theScrollView.bounces = YES;
    theScrollView.alwaysBounceVertical = YES;
    
//    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
//                                                                   style:UIBarButtonItemStyleDone
//                                                                  target:self action:@selector(doneClicked:)];
//    self.navigationItem.rightBarButtonItem = doneButton;
}

-(void)doneClicked:(UIBarButtonItem*)doneItem {
    
    if (self.isLoading) {
        return;
    }
    
    [nameField resignFirstResponder];
    [cellNumberField resignFirstResponder];
    
    NSError *validationError = nil;
    if (!nameField.text.length) {
        
        validationError = [NSError walnutErrorWithMessage:@"Please enter a valid name!"];
    }
    else if (cellNumberField.text.length != IndianMobileLength) {
        
        validationError = [NSError walnutErrorWithMessage:@"Please enter a valid mobile number without country code!"];
    }else if (emailAddressTextField.text.length && ![emailAddressTextField.text isValidateEmailAddress]){
        
        validationError = [NSError walnutErrorWithMessage:@"Please enter a valid email address!"];
    }
    
    if (!validationError) {
        
        BOOL mobileChanged = NO;
        BOOL haveChanges = NO;
        WLTLoggedInUser *user = [WLTDatabaseManager sharedManager].currentUser;
        if (![user.name isEqualToString:nameField.text]) {
            haveChanges = YES;
        }
        else if (![user.mobileString.onlyMobileNumber isEqualToString:cellNumberField.text]) {
            mobileChanged = YES;
            haveChanges = YES;
        }
        else {
            
            if (!user.email.length && emailAddressTextField.text.length) {
                haveChanges = YES;
            }
            else if (![user.email isEqualToString:emailAddressTextField.text]) {
                haveChanges = YES;
            }
        }
        
        if (!haveChanges) {
            
            if (self.completion) {
                self.completion(YES);
            }
        }else {
            
            NSString *cellNum = cellNumberField.text;
            NSString *name = nameField.text;
            NSString *completeNum = [NSString stringWithFormat:@"+91%@",cellNum];
            NSString *emailAddress = emailAddressTextField.text;
            
            WLTLoginTempUser *newUser = [[WLTLoginTempUser alloc] init];
            newUser.emailAddress = emailAddress;
            newUser.userName = name;
            newUser.mobileNumber = completeNum;
            
            if (mobileChanged) {
                newUser.deviceUUID = [WLTNetworkManager generateUniqueIdentifier];
            }else {
                newUser.deviceUUID = [WLTDatabaseManager sharedManager].appSettings.deviceIdentifier;
            }
            
            __weak typeof(self) weakSelf = self;
            [self startLoadingWithColor:nil];
            
            [[WLTNetworkManager sharedManager] getOTPForUser:newUser completion:^(id data) {
                
                if ([data isKindOfClass:[NSError class]]) {
                    
                    [weakSelf showAlertForError:data];
                }else {
                    
                    [weakSelf showOTPScreenForUser:newUser];
                }
                
                [weakSelf endViewLoading];
            }];
        }
    }else {
        
        [self showAlertForError:validationError];
    }
}

-(void)setProfileForUser:(WLTLoginTempUser*)user completion:(SuccessBlock)block{
    
    GTLWalnutMUserProfile *newProfile = [[GTLWalnutMUserProfile alloc] init];
    newProfile.deviceUuid = user.deviceUUID;
    newProfile.deviceName = [[UIDevice currentDevice] name];
    
    [[WLTNetworkManager sharedManager] setUPUserProfile:newProfile forUser:user withCompletion:^(id data) {
        
        if ([data isKindOfClass:[NSError class]]) {
            
            block(NO);
        }else {
            block(YES);
        }
    }];
}

-(void)showOTPScreenForUser:(WLTLoginTempUser*)user{
    
    __weak typeof(self) weakSelf = self;
    WLTOTPViewController *otpCont = [self controllerWithIdentifier:@"WLTOTPViewController"];
    otpCont.userObject = user;
    [otpCont setCompletion:^(WLTLoginTempUser *user){
        
        if (user) {
            
            NSString *currentMob = CURRENT_USER_MOB;
            if (![user.mobileNumber isEqualToString:currentMob]) {
                
                [weakSelf setProfileForUser:user completion:^(BOOL success){
                    
                    if (success) {
                        
                        WLTDatabaseManager *dbManager = [WLTDatabaseManager sharedManager];
                        [dbManager logOut];
                        
                        WLTLoggedInUser *loggedIn = [dbManager objectOfClassString:@"WLTLoggedInUser"];
                        loggedIn.wltAuthToken = user.authToken;
                        loggedIn.name = user.userName;
                        loggedIn.mobileString = user.mobileNumber;
                        loggedIn.email = user.emailAddress;
                        
                        dbManager.currentUser = loggedIn;
                        
                        dbManager.appSettings.deviceIdentifier = user.deviceUUID;
                        [dbManager updatePaymentConfigration:user.paymentConfig];
                        
                        dbManager.appSettings.mobileVerified = @(YES);
                        
                        [dbManager saveMainDataBase];
                        
                        [[WLTNetworkManager sharedManager] refreshAuthState];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:NotifyUserMobileChanged object:nil];
                    }
                    
                    if (weakSelf.completion) {
                        weakSelf.completion(success);
                    }
                }];
            }else {
                
                WLTLoggedInUser *current = [[WLTDatabaseManager sharedManager] currentUser];
                current.mobileString = user.mobileNumber;
                current.name = user.userName;
                current.email = user.emailAddress;
                current.wltAuthToken = user.authToken;
                
                [[WLTDatabaseManager sharedManager] saveMainDataBase];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:NotifyUserInfoChanged object:nil];
                
                if (weakSelf.completion) {
                    weakSelf.completion(YES);
                }
            }
        }
        else {
            
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
    
    [self.navigationController pushViewController:otpCont animated:YES];
}

-(BOOL)textField:(WLTTextField *)field canChangeTextTo:(NSString *)newTextString {
    
    BOOL retValue = YES;
    
    if ([field isEqual:cellNumberField]) {
        if (newTextString.length > IndianMobileLength) {
            retValue = NO;
        }
    }
    
    return retValue;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL retValue = YES;
    if ([textField isEqual:cellNumberField]) {
        
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if (newString.length > IndianMobileLength) {
            retValue = NO;
        }
    }
    
    return retValue;
}

#pragma mark - 

-(ProfileTableSectionIdentifier)sectionIdentifierForSection:(NSInteger)section {
    
    ProfileTableSectionIdentifier identifier = section;
    return identifier;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger count = 1;
    return count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger count = 3;
    return count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *reuseIdentifier = @"DefaultTableCell";
    ProfileTableSectionIdentifier ident = [self sectionIdentifierForSection:indexPath.section];
    
    if ( ident == ProfileTableSectionIdentifierName ) {
        reuseIdentifier = @"DefaultTableCellUserName";
    }
    else if ( ident == ProfileTableSectionIdentifierEmail ) {
        reuseIdentifier = @"DefaultTableCellEmail";
    }
    else if ( ident == ProfileTableSectionIdentifierMobile ) {
        reuseIdentifier = @"DefaultTableCellMobile";
    }
    else if ( ident == ProfileTableSectionIdentifierLogout ) {
        reuseIdentifier = @"DefaultTableCellLogout";
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (!cell) {
        
        UIView *backView = [[UIView alloc] initWithFrame:CGRectZero];
        backView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DefaultTableCell"];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        cell.separatorInset = UIEdgeInsetsZero;
        cell.selectedBackgroundView = backView;
        cell.backgroundColor = [UIColor whiteColor];
        
        WLTLoggedInUser *user = [WLTDatabaseManager sharedManager].currentUser;
        
        if ( [reuseIdentifier isEqualToString:@"DefaultTableCellUserName"] || [reuseIdentifier isEqualToString:@"DefaultTableCellMobile"] || [reuseIdentifier isEqualToString:@"DefaultTableCellEmail"] ) {
            
            UITextField *field = [[UITextField alloc] initWithFrame:CGRectZero];
            field.font = [UIFont sfUITextRegularOfSize:14];
            field.backgroundColor = [UIColor clearColor];
            field.autocapitalizationType = UITextAutocapitalizationTypeWords;
            field.autocorrectionType = UITextAutocorrectionTypeDefault;
            field.translatesAutoresizingMaskIntoConstraints = NO;
            field.textColor = [UIColor rhinoColor];
            [cell addSubview:field];
            
            NSDictionary *dict = NSDictionaryOfVariableBindings(field);
            
            [cell addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[field]-15-|" options:0 metrics:nil views:dict]];
            [cell addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[field]-0-|" options:0 metrics:nil views:dict]];
            
            if ([reuseIdentifier isEqualToString:@"DefaultTableCellUserName"]) {
                
                field.placeholder = @"Name";
                [field setText:user.name];
                
                nameField = field;
            }
            else if ([reuseIdentifier isEqualToString:@"DefaultTableCellEmail"]) {
                
                field.autocapitalizationType = UITextAutocapitalizationTypeNone;
                field.autocorrectionType = UITextAutocorrectionTypeNo;
                field.placeholder = @"Enter Email Address";
                [field setText:user.email];
                
                emailAddressTextField = field;
            }
            else if ( [reuseIdentifier isEqualToString:@"DefaultTableCellMobile"]) {
                
                UILabel *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 45)];
                leftView.textColor = [UIColor colorWithRed:0.56 green:0.56 blue:0.56 alpha:1.00];
                leftView.font = [UIFont sfUITextRegularOfSize:14];
                leftView.text = @"+91 ";
                [leftView sizeToFit];
                
                field.delegate = self;
                field.leftViewMode = UITextFieldViewModeAlways;
                field.leftView = leftView;
                field.keyboardType = UIKeyboardTypeNumberPad;
                field.backgroundColor = [UIColor clearColor];
                field.textColor = [UIColor rhinoColor];
                field.placeholder = @"XXXXXXXXXX";
                
                [field setText:user.mobileString.onlyMobileNumber];
                
                cellNumberField = field;
            }
        }
        else if([reuseIdentifier isEqualToString:@"DefaultTableCellLogout"]){
            
            cell.textLabel.textColor = [UIColor flamePeaColor];
            cell.textLabel.text = @"Logout";
            cell.textLabel.font = [UIFont sfUITextRegularOfSize:14];
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
        }else {
            
            cell.textLabel.textColor = [UIColor rhinoColor];
            cell.textLabel.text = user.email;
            cell.textLabel.font = [UIFont sfUITextRegularOfSize:14];
        }
    };
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 45;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	
	CGFloat height = 35;
	return height;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
	return 1;
}


-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	
    UIView *header = nil;
	
    ProfileTableSectionIdentifier identi = [self sectionIdentifierForSection:section];
    if ( identi != ProfileTableSectionIdentifierLogout ) {
        
        header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 20)];
        header.backgroundColor = [UIColor clearColor];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.translatesAutoresizingMaskIntoConstraints = NO;
		label.font = [UIFont sfUITextMediumOfSize:12];
		label.textColor = [UIColor cadetGrayColor];
        [header addSubview:label];
        
        NSDictionary *dict = NSDictionaryOfVariableBindings(label);
        [header addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[label]-0-|" options:0 metrics:nil views:dict]];
        [header addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[label]-5-|" options:0 metrics:nil views:dict]];
        
        switch ([self sectionIdentifierForSection:section]) {
                
            case ProfileTableSectionIdentifierName:
                label.text = @"NAME";
                break;
            case ProfileTableSectionIdentifierMobile:
                label.text = @"PHONE NUMBER";
                break;
            case ProfileTableSectionIdentifierEmail:
                label.text = @"EMAIL ADDRESS";
                break;
            default:
                break;
        }
    }
    
    return header;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    ProfileTableSectionIdentifier ident = [self sectionIdentifierForSection:indexPath.section];
//    if (ident == ProfileTableSectionIdentifierLogout) {
//        
//        __weak WLTUserProfileViewController *weakSelf = self;
//        
//        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Please Confirm!" message:@"Are you sure you want to logout from Walnut?" preferredStyle:UIAlertControllerStyleAlert];
//        
//        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
//        
//        UIAlertAction *actionlogout = [UIAlertAction actionWithTitle:@"Logout" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
//            
//            [weakSelf.rootController logoutCurrentUser];
//        }];
//        
//        [controller addAction:actionCancel];
//        [controller addAction:actionlogout];
//        
//        [self presentViewController:controller animated:YES completion:nil];
//    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
