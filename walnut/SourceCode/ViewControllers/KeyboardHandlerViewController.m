//
//  KeyboardHandlerViewController.m
//
//  Created by Abhinav Singh on 08/10/15.
//

#import "KeyboardHandlerViewController.h"

@interface KeyboardHandlerViewController ()

@end

@implementation KeyboardHandlerViewController

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)keyboardWillShow:(NSNotification*)notify {
    
    NSDictionary *userInfo = notify.userInfo;
    
    NSTimeInterval animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationOptions animationCurve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    CGRect keyboardScreenBeginFrame = [userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGRect keyboardScreenEndFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    CGRect keyboardViewBeginFrame = [theScrollView convertRect:keyboardScreenBeginFrame fromView:theScrollView.window];
    CGRect keyboardViewEndFrame = [theScrollView convertRect:keyboardScreenEndFrame fromView:theScrollView.window];
    
    keyboardEndFrame = keyboardScreenEndFrame;
    
    CGFloat originDelta = (keyboardViewBeginFrame.origin.y - keyboardViewEndFrame.origin.y);
    
    if (originDelta > 0) {
        
        beforeEdgeInsets = theScrollView.contentInset;
        UIEdgeInsets insets = beforeEdgeInsets;
        
        //Check how much its hiding ScrollView.
        CGPoint pt = [theScrollView.window convertPoint:CGPointMake(0, theScrollView.height-1) fromView:theScrollView];
        CGFloat bottomMargin = (theScrollView.window.height-pt.y);
        
        originDelta -= bottomMargin;
        
        insets.bottom = originDelta;
        
        [theScrollView setContentInset:insets];
    }
    
    UIView *active = [self activeField];
    CGPoint newOffset = [self contentOffsetForField:active];
    
    if (!CGPointEqualToPoint(CGPointZero, newOffset)) {
        
        UIViewAnimationOptions animationOptions = animationCurve | UIViewAnimationOptionBeginFromCurrentState;
        [UIView animateWithDuration:animationDuration delay:0 options:animationOptions animations:^{
            
            [theScrollView setContentOffset:newOffset];
        } completion:nil];
    }
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    [theScrollView setContentInset:beforeEdgeInsets];
}

-(UIView*)firstResponderSubviewInView:(UIView*)view {
    
    UIView *active = nil;
    if ([view isFirstResponder]) {
        active = view;
    }else {
        for ( UIView *sub in view.subviews ) {
            
            active = [self firstResponderSubviewInView:sub];
            if (active) {
                break;
            }
        }
    }
    
    return active;
}

-(UIView*)activeField {
    
    return [self firstResponderSubviewInView:theScrollView];
}

-(CGPoint)contentOffsetForField:(UIView*)field {
    
    CGPoint newOffset = CGPointZero;
    if(field) {
        
        CGRect overWindow = [theScrollView.window convertRect:field.frame fromView:field.superview];
        overWindow.size.height += 10;
        
        CGFloat maxY = (overWindow.origin.y + overWindow.size.height);
        
        if ((keyboardEndFrame.size.height > 0) && (maxY > keyboardEndFrame.origin.y)) {
            
            CGFloat howMuchToChange = (maxY - keyboardEndFrame.origin.y);
            CGFloat expected = (theScrollView.contentOffset.y + howMuchToChange);
            
            newOffset = CGPointMake(theScrollView.contentOffset.x, expected);
        }
    }
    
    return newOffset;
}

-(void)subViewBecomeActive:(UIView*)subView {
    
    CGPoint newOffset = [self contentOffsetForField:subView];
    
    if (!CGPointEqualToPoint(CGPointZero, newOffset)) {
        
        UIViewAnimationOptions animationOptions = UIViewAnimationOptionCurveEaseIn | UIViewAnimationOptionBeginFromCurrentState;
        [UIView animateWithDuration:0.25 delay:0 options:animationOptions animations:^{
            
            [theScrollView setContentOffset:newOffset];
        } completion:nil];
    }
}

@end
