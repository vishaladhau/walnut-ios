//
//  WLTGroupPaymentAmountEditViewController.m
//  walnut
//
//  Created by Abhinav Singh on 20/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTGroupPaymentAmountEditViewController.h"
#import "WLTPartial.h"
#import "WLTColorPalette.h"
#import "WLTGroup.h"
#import "WLTPaymentEditorKeyboardAccessoryView.h"
#import "WLTDatabaseManager.h"

@implementation WLTGroupAmountView

-(instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        UIView *grpColorView = [[UIView alloc] initWithFrame:CGRectZero];
        grpColorView.layer.cornerRadius = 18;
        grpColorView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:grpColorView];
        
        UILabel *grpNameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        grpNameLabel.numberOfLines = 2;
        grpNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        grpNameLabel.font = [UIFont sfUITextRegularOfSize:16];
        grpNameLabel.textColor = [UIColor cadetColor];
        [self addSubview:grpNameLabel];
        
        UILabel *amtLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        amtLabel.textAlignment = NSTextAlignmentRight;
        amtLabel.numberOfLines = 2;
        amtLabel.translatesAutoresizingMaskIntoConstraints = NO;
        amtLabel.font = [UIFont sfUITextRegularOfSize:16];
        amtLabel.textColor = [UIColor cadetColor];
        [amtLabel setPreferredMaxLayoutWidth:150];
        [amtLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        [self addSubview:amtLabel];
        
        amountLabel = amtLabel;
        groupNameLabel = grpNameLabel;
        groupColorView = grpColorView;
        
        NSDictionary *dict = NSDictionaryOfVariableBindings(groupNameLabel, amtLabel, groupColorView);
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[groupColorView(==36)]-10-[groupNameLabel]-10-[amtLabel]-10-|" options:0 metrics:nil views:dict]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[groupNameLabel]-5-|" options:0 metrics:nil views:dict]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[amtLabel]-5-|" options:0 metrics:nil views:dict]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[groupColorView(==36)]" options:0 metrics:nil views:dict]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:groupColorView
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self attribute:NSLayoutAttributeCenterY
                                                        multiplier:1 constant:0]];
    }
    
    return self;
}

-(void)showDetailsOfVariableGroup:(WLTPartialGroup*)pays {
    
    _displayed = pays;
    
    [groupNameLabel displayGroupName:pays.group checkDirect:YES];
    
    groupColorView.backgroundColor = [[WLTColorPalette sharedPalette] colorForGroup:pays.group];
    
    [self refreshAmount];
}

-(void)showDetailsOfFixedGroup:(WLTPartialGroup*)pays {
    
    _displayed = pays;
    [groupNameLabel displayGroupName:pays.group checkDirect:YES];
    
    groupColorView.backgroundColor = [[WLTColorPalette sharedPalette] colorForGroup:pays.group];
    
    NSString *complete = [NSString stringWithFormat:@"%@\nSettled", @(self.displayed.acctualAmount).displayCurrencyString];
    
    NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:complete attributes:@{NSForegroundColorAttributeName:[UIColor getColor], NSFontAttributeName:[UIFont sfUITextRegularOfSize:16]}];
    [attributed addAttributes:@{NSForegroundColorAttributeName:[UIColor mediumGrayTextColor], NSFontAttributeName:[UIFont sfUITextRegularOfSize:12]} range:[complete rangeOfString:@"Settled"]];
    
    amountLabel.attributedText = attributed;
}

-(void)refreshAmount {
    
    NSString *statusString = nil;
    if ( (self.displayed.currentAmount > 0) && (self.displayed.currentAmount < self.displayed.acctualAmount)) {
        statusString = @"Partially Settled";
    }
    else if (self.displayed.currentAmount == self.displayed.acctualAmount) {
        statusString = @"Settled";
    }
    else if (self.displayed.currentAmount > self.displayed.acctualAmount) {
        statusString = @"Settled with extra money";
    }
    else {
        statusString = @"Not Settled";
    }
    
    NSString *complete = [NSString stringWithFormat:@"%@\n%@", @(self.displayed.currentAmount).displayCurrencyString, statusString];
    
    NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:complete attributes:@{NSForegroundColorAttributeName:[UIColor darkGrayTextColor], NSFontAttributeName:[UIFont sfUITextRegularOfSize:16]}];
    [attributed addAttributes:@{NSForegroundColorAttributeName:[UIColor mediumGrayTextColor], NSFontAttributeName:[UIFont sfUITextRegularOfSize:12]} range:[complete rangeOfString:statusString]];
    
    amountLabel.attributedText = attributed;
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat amtWidth = [amountLabel intrinsicContentSize].width;
    
    [groupNameLabel setPreferredMaxLayoutWidth:(screenWidth-amtWidth-76)];
}

@end

@interface WLTGroupPaymentAmountEditViewController () <UITextFieldDelegate> {
    __weak WLTPaymentEditorKeyboardAccessoryView *_errorView;
    
}

@end

@implementation WLTGroupPaymentAmountEditViewController

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)keyboardWillShow:(NSNotification*)notify {
    
    NSDictionary *userInfo = notify.userInfo;
    
    CGRect keyboardScreenEndFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    [theScrollView setContentInset:UIEdgeInsetsMake(0, 0, keyboardScreenEndFrame.size.height, 0)];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"Edit Amount";
    
    headerView.backgroundColor = [UIColor whiteColor];
    [headerView addBorderLineAtPosition:(BorderLinePositionTop|BorderLinePositionBottom)];
    
    headerInfoLabel.textColor = [UIColor darkGrayTextColor];
    headerInfoLabel.font = [UIFont sfUITextRegularOfSize:16];
    headerInfoLabel.text = @"Payable Amount";
    
    amountTextField.textColor = [UIColor darkGrayTextColor];
    amountTextField.font = [UIFont sfUITextRegularOfSize:16];
    amountTextField.text = @(self.transaction.currentAmount).displayRoundedString;
    amountTextField.keyboardType = UIKeyboardTypeDecimalPad;
    amountTextField.delegate = self;
    if (!_settling) {
        WLTPaymentEditorKeyboardAccessoryView *errorView = [WLTPaymentEditorKeyboardAccessoryView instantiateAccessoryViewFromNib];
        [errorView addBorderLineAtPosition:(BorderLinePositionTop|BorderLinePositionBottom)];
        errorView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 55);
        amountTextField.inputAccessoryView = errorView;
        _errorView = errorView;
        _errorView.hidden = YES;
    }
    
    WLTPaymentConfig *config = [[WLTDatabaseManager sharedManager] paymentConfig];
    if (self.transaction.currentAmount < config.p2pMinLimit.doubleValue) {
        
        _errorView.titleLabel.text = [NSString stringWithFormat:@"Amount cannot be less than %@", config.p2pMinLimit.displayCurrencyString];
        _errorView.hidden = NO;
        self.navigationItem.rightBarButtonItem.enabled = NO;
    } else if (self.transaction.currentAmount > config.p2pMaxLimit.doubleValue) {
        
        _errorView.titleLabel.text = [NSString stringWithFormat:@"Amount cannot be greater than %@", config.p2pMaxLimit.displayCurrencyString];
        _errorView.hidden = NO;
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
    
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone
                                                             target:self action:@selector(doneClicked:)];
    self.navigationItem.rightBarButtonItem = right;
    
    allScrollPaySubViews = [NSMutableArray new];
    
    [self addSubviewsForCurrentData];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [amountTextField becomeFirstResponder];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *newStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self changePayableAmount:newStr.floatValue];
    
    return YES;
}

-(void)changePayableAmount:(CGFloat)amount {
    
    WLTPaymentConfig *config = [[WLTDatabaseManager sharedManager] paymentConfig];
    if (!_settling) {
        if (amount < config.p2pMinLimit.doubleValue) {
            
            _errorView.titleLabel.text = [NSString stringWithFormat:@"Amount cannot be less than %@", config.p2pMinLimit.displayCurrencyString];
            _errorView.hidden = NO;
            self.navigationItem.rightBarButtonItem.enabled = NO;
        } else if (amount > config.p2pMaxLimit.doubleValue) {
            
            _errorView.titleLabel.text = [NSString stringWithFormat:@"Amount cannot be greater than %@", config.p2pMaxLimit.displayCurrencyString];
            _errorView.hidden = NO;
            self.navigationItem.rightBarButtonItem.enabled = NO;
        } else {
            
            self.navigationItem.rightBarButtonItem.enabled = YES;
            _errorView.hidden = YES;
        }
    } else {
        self.navigationItem.rightBarButtonItem.enabled = (amount >= 0);
    }
    

    self.transaction.currentAmount = amount;
    for (WLTGroupAmountView *view in allScrollPaySubViews ) {
        [view refreshAmount];
    }
}

-(void)doneClicked:(UIBarButtonItem*)item {
    
    if (![self isLoading]) {
        
        [self.view endEditing:YES];
        
        if (self.completion) {
            self.completion(self.transaction);
        }
    }
}

-(void)addSubviewsForCurrentData {
    
    for ( UIView *view in theScrollView.subviews) {
        if ([view isKindOfClass:[WLTGroupAmountView class]]) {
            [view removeFromSuperview];
        }
    }
    
    CGFloat width = [[UIScreen mainScreen] bounds].size.width;
    CGFloat height = 53;
    
    __block WLTGroupAmountView *lastView = nil;
    
    WLTGroupAmountView * (^ addGroupView)() = ^WLTGroupAmountView * (){
        
        WLTGroupAmountView *view = [[WLTGroupAmountView alloc] initWithFrame:CGRectZero];
        view.translatesAutoresizingMaskIntoConstraints = NO;
        [view addBorderLineAtPosition:BorderLinePositionTop];
        [theScrollView addSubview:view];
        
        NSDictionary *dict = NSDictionaryOfVariableBindings(view);
        [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-0-[view(==%f)]-0-|", width] options:0 metrics:nil views:dict]];
        [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[view(==%f)]", height] options:0 metrics:nil views:dict]];
        
        if (lastView) {
            
            dict = NSDictionaryOfVariableBindings(view, lastView);
            [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lastView]-0-[view]" options:0 metrics:nil views:dict]];
        }else {
            
            dict = NSDictionaryOfVariableBindings(view);
            
            [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]" options:0 metrics:nil views:dict]];
        }
        
        lastView = view;
        
        return view;
    };
    
    NSArray *changableGrps = [self.transaction changableGroups];
    NSArray *fixedGrps = [self.transaction fixedGroups];
    
    for ( WLTPartialGroup *changed in changableGrps ) {
        
        WLTGroupAmountView *view = addGroupView();
        [view showDetailsOfVariableGroup:changed];
        
        [allScrollPaySubViews addObject:view];
    }
    
    for ( WLTPartialGroup *fixed in fixedGrps ) {
        
        WLTGroupAmountView *view = addGroupView();
        [view showDetailsOfFixedGroup:fixed];
    }
    
    if (lastView) {
        
        [lastView addBorderLineAtPosition:BorderLinePositionBottom];
        NSDictionary *dict = NSDictionaryOfVariableBindings(lastView);
        [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lastView]-0-|" options:0 metrics:nil views:dict]];
    }
}

@end
