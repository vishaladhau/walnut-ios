//
//  WLTGroupDetailViewController.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 05/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"
#import "WLTGroup.h"
#import "WLTCommentView.h"
#import "WLTEmptyStateView.h"

@class WLTGroupDetailViewController, WLTDetailsHeaderButton;

@interface WLTTransactionTableViewDelegate : NSObject <UITableViewDelegate, UITableViewDataSource>{
    
    
}

@property(nonatomic, weak) UITableView *theTableView;
@property(nonatomic, weak) WLTGroupDetailViewController *owner;

@end

@interface WLTGroupDetailViewController : WLTViewController {
    
@public
    __weak IBOutlet UIView *transactionsBackView;
    
    __weak IBOutlet UIView *headerView;
    __weak IBOutlet NSLayoutConstraint *headerHeight;
    
    __weak IBOutlet UITableView *transactionsTableView;
    __weak IBOutlet NSLayoutConstraint *bottomConstraint;
    
    __weak IBOutlet WLTCommentView *commentView;
    
    __weak UIButton *settingsBarButton;
    
    WLTTransactionTableViewDelegate *transactionTableDelegate;
    
    BOOL showingComments;
    
    __weak WLTEmptyStateView *noTransactionsView;
    __weak WLTEmptyStateView *noTansUsersView;
    
    BOOL somethingChanged;
    BOOL viewDidAppeared;
    
    __weak WLTDetailsHeaderButton *yourShareView;
    __weak WLTDetailsHeaderButton *yourContView;
    __weak WLTDetailsHeaderButton *yourDuesView;
}

@property(nonatomic, weak) IBOutlet UIButton *unreadCountView;
@property(nonatomic, strong) WLTGroup *currentGroup;

@end

