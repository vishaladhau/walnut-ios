//
//  WLTOnBoardingViewController.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 02/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"

@class LSButton;

@interface WLTOnBoardingViewController : WLTViewController {
    
    __weak IBOutlet LSButton *registerButton;
    __weak IBOutlet UIButton *alreadyUserButton;
	
	__weak IBOutlet UIScrollView *theScrollView;
	
    __weak IBOutlet UIImageView *topIconImageView;
    __weak IBOutlet NSLayoutConstraint *topConstraint;

	__weak IBOutlet UIPageControl *pageControl;
}

@property(nonatomic, strong) LoginBlock completionBlock;

@end
