//
//  WLTCardsListViewController.h
//  walnut
//
//  Created by Abhinav Singh on 08/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"
#import "WLTInstrumentsManager.h"
#import "WLTFlowConstants.h"

@class WLTCardsScrollView;
@class LSButton;

@interface WLTCardsListViewController : WLTViewController {
    
    __weak IBOutlet UIScrollView *backScrollView;
    __weak IBOutlet WLTCardsScrollView *theScrollView;
    __weak IBOutlet NSLayoutConstraint *scrollViewWidth;
    
    __weak IBOutlet UIView *bottomBackView;
    __weak IBOutlet NSLayoutConstraint *bottomBackViewHeight;
    
    __weak IBOutlet UIView *newCardEntryBottomView;
    __weak IBOutlet UIButton *addNewBankButton;
    __weak IBOutlet UILabel *orLabel;
    
    __weak IBOutlet UIView *oldCardEntryBottomView;
    
    __weak IBOutlet UIView *receiveUsingThisBackView;
    __weak IBOutlet UIButton *receiveUsingThisButton;
    
    __weak IBOutlet UIView *removeCardBackView;
    __weak IBOutlet UIButton *removeCardButton;
    
    __weak IBOutlet UIView *makeDefaultBackView;
    __weak IBOutlet UISwitch *makeDefaultSwitch;
    __weak IBOutlet UILabel *makeDefaultLabel;
    
    __weak IBOutlet UIView *dontSupportBackView;
    __weak IBOutlet UILabel *dontSupportLabel;
    __weak IBOutlet UIImageView *dontSupportImageView;
    
    __weak IBOutlet NSLayoutConstraint *addBankBackViewTopConstarint;
    __weak IBOutlet NSLayoutConstraint *addBankBackViewHeight;
    __weak IBOutlet UIView *addBankBackView;
    __weak IBOutlet LSButton *addBankButton;
    
    __weak IBOutlet UILabel *fastFundInfoLabel;
    __weak IBOutlet UIImageView *fastFundInfoImageView;
}

@property(nonatomic, strong) NSString *transactionIDToShow;
@property (nonatomic, assign) WLTEntryFlowType flowType;
@property (nonatomic, assign) BOOL showReceiveAlert;

@end
