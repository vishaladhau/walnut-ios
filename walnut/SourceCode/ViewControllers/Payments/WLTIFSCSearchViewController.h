//
//  WLTIFSCSearchViewController.h
//  walnut
//
//  Created by Abhinav Singh on 17/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"

typedef void (^ SearchCompletion)(GTLWalnutMIfscSearchObject *selected);

@interface WLTIFSCSearchViewController : WLTViewController {
    
    __weak IBOutlet UITableView *theTableView;
    __weak IBOutlet UISearchBar *theSearchBar;
}

@property(nonatomic, strong) SearchCompletion completion;

@end
