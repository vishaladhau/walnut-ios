//
//  WLTPaymentTransactionsViewController.m
//  walnut
//
//  Created by Abhinav Singh on 25/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTPaymentTransactionsViewController.h"
#import "LSButton.h"
#import "WLTEmptyStateView.h"
#import "WLTPaymentTransactionsTableViewCell.h"
#import "WLTPaymentTransaction.h"
#import "WLTGroup.h"
#import "WLTDatabaseManager.h"
#import "WLTPaymentUser.h"
#import "WLTPaymentTransactionDetailViewController.h"
#import "WLTPaymentTransationsManager.h"
#import "WLTRequestSendMoneyTableViewCell.h"
#import "GTLWalnutMPaymentTransaction.h"
#import "WLTSendRequestMoneyButton.h"
#import "WLTPaymentContactsViewController.h"
#import "WLTRequestPaymentDetailViewController.h"
#import "WLTFirstPaymentAlertView.h"
#import "WLTSendMoneyRequestViewController.h"
#import "LSTRefreshHeaderView.h"

@interface WLTPaymentTransactionsViewController () <WLTPaymentTransTableCellDelegate, WLTEmptyStateViewDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource>{
    
    CGFloat expandedSegmentHeight;
    CGFloat collapsedSegmentHeight;
    
    __weak IBOutlet WLTSendRequestMoneyButton *sendButton;
    __weak IBOutlet WLTSendRequestMoneyButton *receiveButton;
    
    __weak IBOutlet UISegmentedControl *theSegmentControl;
    __weak IBOutlet UITableView *theTableView;
    
    __weak IBOutlet UIView *blurBackView;
	__weak IBOutlet UIView *topContentBackView;
    __weak IBOutlet UIView *segmentHeaderBackView;
    
    __weak IBOutlet NSLayoutConstraint *topHeaderViewHeight;
    __weak IBOutlet NSLayoutConstraint *segmentBackViewHeight;
    
    __weak LSTRefreshHeaderView *refreshControl;
    __weak WLTEmptyStateView *emptyStateView;
	
	__weak IBOutlet UIView *seperatorView;
	
    NSInteger unreadCount;
}

@property(nonatomic, strong) NSArray *allTransactions;
@property(nonatomic, strong) NSArray *paidTransactions;
@property(nonatomic, strong) NSArray *receivedTransactions;

@property(nonatomic, strong) NSArray *displayedTransactions;

@end

@implementation WLTPaymentTransactionsViewController

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeUnreadBadgeCount:) name:PaymentTransactionsCountChanged object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendReceiveStatusChanged:) name:NotifySendReceiveMoneyStatusChanged object:nil];
    }
    
    return self;
}

-(void)dealloc {
    
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [[WLTPaymentTransationsManager sharedManager] markAllTransactionsAsViewed];
    tabItem.badgeValue = nil;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
	seperatorView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.05];
	[self setEdgesForExtendedLayout:UIRectEdgeAll];
    
    self.allTransactions = [NSMutableArray new];
    self.paidTransactions = [NSMutableArray new];
    self.receivedTransactions = [NSMutableArray new];
    
    expandedSegmentHeight = 64;
    collapsedSegmentHeight = 44;
    
	[blurBackView addBlurBackgroundDark:NO];
    topContentBackView.backgroundColor = [UIColor clearColor];
    [topContentBackView addBorderLineAtPosition:BorderLinePositionBottom];
    
    segmentHeaderBackView.backgroundColor = [UIColor clearColor];
    [segmentHeaderBackView addBorderLineAtPosition:BorderLinePositionBottom];
    theSegmentControl.tintColor = [UIColor appleGreenColor];
    
    
    [sendButton setTitle:@"Send Money"];
    [sendButton setImage:[UIImage imageNamed:@"SendMoneyIcon"] andColor:[UIColor oweColor]];
    
    [receiveButton setTitle:@"Request Money"];
    [receiveButton setImage:[UIImage imageNamed:@"ReceiveMoneyIcon"] andColor:[UIColor getColor]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchAllTransactionsFromDB) name:PaymentTransactionsChanged object:nil];
    
    WLTEmptyStateView *emptyState = [WLTEmptyStateView emptyStateViewWithStyle:WLTEmptyStateViewStyleDefault andDelegate:self];
    [emptyState showTitle:@"" andSubtitle:@"WalnutPay – the cool new way to pay your friends. Free money transfers direct to bank account. No more sharing account & IFSC. No more wallets" imageName:@"NoTransactionsImage"];
    emptyState.userInteractionEnabled = NO;
    emptyState.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:emptyState];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(emptyState, segmentHeaderBackView);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[emptyState]-0-|" options:0 metrics:nil views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[segmentHeaderBackView]-0-[emptyState]-0-|" options:0 metrics:nil views:dict]];
    
    emptyStateView = emptyState;
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectZero];
    theTableView.tableFooterView = footer;
    theTableView.separatorInset = UIEdgeInsetsZero;
    theTableView.rowHeight = UITableViewAutomaticDimension;
    theTableView.backgroundColor = self.view.backgroundColor;
    [theTableView registerNib:[UINib nibWithNibName:@"WLTPaymentTransactionsTableViewCell" bundle:nil] forCellReuseIdentifier:@"WLTPaymentTransactionsTableViewCell"];
    [theTableView registerNib:[UINib nibWithNibName:@"WLTRequestSendMoneyTableViewCell" bundle:nil] forCellReuseIdentifier:@"WLTRequestSendMoneyTableViewCell"];
    
    __weak typeof(self) weakSelf = self;
    LSTRefreshHeaderView *control = [[LSTRefreshHeaderView alloc] initWithHeight:70 andPosition:RefreshControlPositionTop];
    [control setStateChangeBlock:^(RefreshControlState state){
        
        if(state == RefreshControlStateStarted) {
			
            [weakSelf refreshTransactionsData];
        }
    }];
    
    refreshControl = control;
    [theTableView addSubview:control];
	
    [self changeSendReceiveButtonsView:NO];
    
    [self fetchAllTransactionsFromDB];
    [self refreshTransactionsData];
}

-(void)changeSendReceiveButtonsView:(BOOL)animate {
    
    if (SendReceivePaymentsEnabled()) {
        
        segmentBackViewHeight.constant = collapsedSegmentHeight;
        topHeaderViewHeight.constant = HomeScreensNavbarHeight;
    }else {
        
        segmentBackViewHeight.constant = expandedSegmentHeight;
        topHeaderViewHeight.constant = 0;
    }
    
    [theTableView setContentInset:UIEdgeInsetsMake( topHeaderViewHeight.constant + segmentBackViewHeight.constant + 10, 0, 49, 0)];
    if (animate) {
        
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

-(void)sendReceiveStatusChanged:(NSNotification*)notify {
    
    [self changeSendReceiveButtonsView:YES];
}

-(BOOL)needNavigationBar {
    
    return NO;
}

-(void)changeEmptyViewState {
    
    emptyStateView.hidden = YES;
    if (![self isLoading]) {
        
        if (!self.displayedTransactions.count) {
            
            emptyStateView.hidden = NO;
        }else {
            
            emptyStateView.hidden = YES;
        }
    }
}

-(void)changeUnreadBadgeCount:(NSNotification*)notify {
    
    if( (self.tabBarController.selectedViewController == self.navigationController) && ([self.navigationController.topViewController isEqual:self])) {
        
        [[WLTPaymentTransationsManager sharedManager] markAllTransactionsAsViewed];
        tabItem.badgeValue = nil;
    }else {
        
        NSNumber *count = notify.object;
        if (count.intValue > 0) {
            
            tabItem.badgeValue = count.displayString;
        }else {
            
            tabItem.badgeValue = nil;
        }
    }
}

-(void)refreshTransactionsData{
    
    __weak WLTPaymentTransactionsViewController *weakSelf = self;
    if ((refreshControl.state != RefreshControlStateStarted)) {
        
        if (!self.displayedTransactions.count) {
            
            [self startLoading];
        }
    }
    
    [self changeEmptyViewState];
    [[WLTPaymentTransationsManager sharedManager] fetchAllPaymentTransactionsCompletion:^(BOOL success, NSError *error) {
        
        [weakSelf endViewLoading];
        if (error && (refreshControl.state == RefreshControlStateStarted)) {
            
            [weakSelf showAlertForError:error];
        }else {
            
            [weakSelf fetchAllTransactionsFromDB];
        }
        
        [refreshControl endRefreshing];
    }];
}

-(UITabBarItem *)tabBarItem {
    
    if (!tabItem) {
        
        UITabBarItem *item = [[UITabBarItem alloc] initWithTitle:@"Transactions" image:[[UIImage imageNamed:@"WalnutPayIconTabUnSelected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"WalnutPayIconTabSelected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        tabItem = item;
        NSInteger unread = [[WLTPaymentTransationsManager sharedManager] unreadMessagesCount];
        if ( unread > 0 ) {
            tabItem.badgeValue = @(unread).displayString;
        }
    }
    
    return tabItem;
}

-(void)fetchAllTransactionsFromDB {
    
    NSMutableArray *paidTrans = [NSMutableArray new];
    NSMutableArray *receiveTrans = [NSMutableArray new];
    
    WLTDatabaseManager *manager = [WLTDatabaseManager sharedManager];
    
    NSSortDescriptor *sorting = [NSSortDescriptor sortDescriptorWithKey:@"sortingDate" ascending:NO];
    self.allTransactions = [manager allObjectsOfClass:@"WLTPaymentTransaction" andSortDescriptors:@[sorting]];
    
    for (WLTPaymentTransaction *trans in self.allTransactions) {
        
        if ( (trans.type == PaymentTypePull) || (trans.type == PaymentTypePush)) {
            if (trans.isPaid) {
                [paidTrans addObject:trans];
            }else{
                [receiveTrans addObject:trans];
            }
        }
    }
    
    self.paidTransactions = paidTrans;
    self.receivedTransactions = receiveTrans;
    
    [self reloadTableData];
}

-(void)reloadTableData {
    
    switch (theSegmentControl.selectedSegmentIndex) {
        case 1: {
            self.displayedTransactions = self.receivedTransactions;
            break;
        }
        case 2: {
            self.displayedTransactions = self.paidTransactions;
            break;
        }
        default: {
            self.displayedTransactions = self.allTransactions;
            break;
        }
    }
    
    [self changeEmptyViewState];
    [theTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(IBAction)segmentChanged:(UISegmentedControl*)sender {
    
    [self reloadTableData];
}

#pragma mark - WLTPaymentTransTableCellDelegate

-(void)actionButtonClickedFromCell:(WLTPaymentTransactionsTableViewCell *)cell {
    
    void (^ showControllerForTransaction)() = ^void (){
        
        NSIndexPath *index = [theTableView indexPathForCell:cell];
        if(index.row != NSNotFound) {
            
            WLTPaymentTransaction *transaction = self.displayedTransactions[index.row];
            if ( transaction.type != PaymentTypeRequestToMe ){
                
                [self redirectToTheDetailsOfTransaction:transaction];
            }else {
                
                if (SendReceivePaymentsEnabled()) {
                    
                    __weak typeof(self) weakSelf = self;
                    WLTSendMoneyRequestViewController *controller = [weakSelf controllerWithIdentifier:@"WLTSendMoneyRequestViewController"];
                    controller.transaction = transaction;
                    controller.transactionType = UserTransactionStateOwe;
                    [weakSelf.navigationController pushViewController:controller animated:YES];
                    
                    [controller setCompletion:^(BOOL success){
                        
                        if (success) {
                            
                            [weakSelf.navigationController popToRootViewControllerAnimated:YES];
                        }
                    }];
                }
            }
        }
    };
    
    if ([WLTFirstPaymentAlertView canShow]) {
        
        WLTFirstPaymentAlertView *alert = [WLTFirstPaymentAlertView initWithDefaultXib];
        [alert setProceedBlock:^(BOOL success){
            
            if (success) {
                
                showControllerForTransaction();
            }
        }];
        [alert show];
    }else {
        
        showControllerForTransaction();
    }
}

#pragma mark - UITableViewDelegate & UITableViewDatasource

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0.1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    WLTPaymentTransaction *transaction = self.displayedTransactions[indexPath.row];
    
    WLTPaymentTransactionsTableViewCell *tcell = [tableView dequeueReusableCellWithIdentifier:@"WLTPaymentTransactionsTableViewCell"];
    tcell.delegate = self;
    [tcell showDetailsOfTransactionFromListPage:transaction];
    
    return tcell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.displayedTransactions.count;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 90;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    void (^ showControllerForTransaction)() = ^void (){
        
        WLTPaymentTransaction *transaction = self.displayedTransactions[indexPath.row];
        [self redirectToTheDetailsOfTransaction:transaction];
    };
    
    if ([WLTFirstPaymentAlertView canShow]) {
        
        WLTFirstPaymentAlertView *alert = [WLTFirstPaymentAlertView initWithDefaultXib];
        [alert setProceedBlock:^(BOOL success){
            
            if (success) {
                
                showControllerForTransaction();
            }
        }];
        [alert show];
    }else {
        
        showControllerForTransaction();
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)redirectToTheDetailsOfTransaction:(WLTPaymentTransaction*)transaction {
    
    if ( (transaction.type == PaymentTypePull) || (transaction.type == PaymentTypePush)) {
        
        WLTPaymentTransactionDetailViewController *controller = [self controllerWithIdentifier:@"WLTPaymentTransactionDetailViewController"];
        controller.transaction = transaction;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if ( (transaction.type == PaymentTypeRequestToMe) || (transaction.type == PaymentTypeRequestFromMe)) {
        
        if (SendReceivePaymentsEnabled()) {
            
            WLTRequestPaymentDetailViewController *controller = [self controllerWithIdentifier:@"WLTRequestPaymentDetailViewController"];
            controller.transaction = transaction;
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}

#pragma mark - Empty States

-(void)emptyStateViewReloadScreen:(WLTEmptyStateView *)view {
    
    [self refreshTransactionsData];
}

-(void)emptyStateViewActionButtonTapped:(WLTEmptyStateView *)view {}

#pragma mark - 

-(IBAction)sendMoneyClicked:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    void (^ showController)() = ^void (){
        
        WLTPaymentContactsViewController *controller = [[WLTPaymentContactsViewController alloc] initWithNibName:@"WLTContactsViewController" bundle:nil];
        controller.transactionState = UserTransactionStateOwe;
        [weakSelf.navigationController pushViewController:controller animated:YES];
    };
    
    if ([WLTFirstPaymentAlertView canShow]) {
        
        WLTFirstPaymentAlertView *alert = [WLTFirstPaymentAlertView initWithDefaultXib];
        [alert setProceedBlock:^(BOOL success){
            
            if (success) {
                
                showController();
            }
        }];
        [alert show];
    }else {
        
        showController();
    }
}

-(IBAction)receiveMoneyClicked:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    void (^ showController)() = ^void (){
        
        WLTPaymentContactsViewController *controller = [[WLTPaymentContactsViewController alloc] initWithNibName:@"WLTContactsViewController" bundle:nil];
        controller.transactionState = UserTransactionStateGet;
        [weakSelf.navigationController pushViewController:controller animated:YES];
    };
    
    if ([WLTFirstPaymentAlertView canShow]) {
        
        WLTFirstPaymentAlertView *alert = [WLTFirstPaymentAlertView initWithDefaultXib];
        [alert setProceedBlock:^(BOOL success){
            
            if (success) {
                
                showController();
            }
        }];
        [alert show];
    }else {
        
        showController();
    }
}

@end
