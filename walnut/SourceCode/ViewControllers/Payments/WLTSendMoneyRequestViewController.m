//
//  WLTSendMoneyRequestViewController.m
//  walnut
//
//  Created by Abhinav Singh on 13/08/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTSendMoneyRequestViewController.h"
#import "WLTPaymentTransationsManager.h"
#import "WLTPaymentTransaction.h"
#import "WLTContactsManager.h"
#import "LSButton.h"
#import "WLTPaymentEditorKeyboardAccessoryView.h"
#import "WLTCombinedGroupPayments.h"
#import "WLTPaymentTransaction.h"
#import "WLTPaymentUser.h"
#import "WLTContact.h"
#import "WLTUserIconButton.h"
#import "PlaceholderTextView.h"

@interface WLTSendMoneyRequestViewController () <UITextFieldDelegate>{
    
    __weak IBOutlet NSLayoutConstraint *widthConstraint;
    __weak IBOutlet NSLayoutConstraint *topConstraint;
    __weak IBOutlet UIView *amountBackView;
    __weak IBOutlet UILabel *amountLabel;
    __weak IBOutlet UITextField *amountField;
    
    __weak IBOutlet PlaceholderTextView *theTextView;
    __weak IBOutlet UIView *notesBackView;
    
    __weak UIView *sendMoneyButtonView;
    __weak WLTPaymentEditorKeyboardAccessoryView *sendMoneyErrorView;
    
    CGFloat minValueAllowed;
    CGFloat maxValueAllowed;
	BOOL passedMinimumValueOnce;
}

@property(nonatomic, strong) NSString *requestingUserName;

@end

@implementation WLTSendMoneyRequestViewController

-(UIView*)keyboardAccessoryView {

    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 55)];
    [backView addBorderLineAtPosition:(BorderLinePositionTop|BorderLinePositionBottom)];
    backView.backgroundColor = [UIColor whiteColor];
    
    LSButton *button = [[LSButton alloc] initWithFrame:CGRectMake(5, 5, backView.width-10, 40)];
    [button addTarget:self action:@selector(postReceiveRequest:) forControlEvents:UIControlEventTouchUpInside];
    [button applyDefaultStyle];
    [backView addSubview:button];
    
    if(self.transactionType == UserTransactionStateGet) {
        
        [button setTitle:@"REQUEST MONEY"];
    }
    else if(self.transactionType == UserTransactionStateOwe) {
        
        [button setTitle:@"SEND MONEY"];
    }
    
    sendMoneyButtonView = button;
    
    WLTPaymentEditorKeyboardAccessoryView *errorView = [WLTPaymentEditorKeyboardAccessoryView instantiateAccessoryViewFromNib];
    errorView.translatesAutoresizingMaskIntoConstraints = NO;
    [backView addSubview:errorView];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(errorView);
    [backView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-2-[errorView]-2-|" options:0 metrics:nil views:dict]];
    [backView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[errorView]-2-|" options:0 metrics:nil views:dict]];
    
    sendMoneyErrorView = errorView;
    
    return backView;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [amountField becomeFirstResponder];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
	
	passedMinimumValueOnce = NO;
	
    widthConstraint.constant = [UIScreen mainScreen].bounds.size.width;
    
    topConstraint.constant = 64;
    if (![self.navigationController.navigationBar isTranslucent]) {
        topConstraint.constant = 0;
        [amountBackView addBorderLineAtPosition:BorderLinePositionTop];
    }
    
    [self setEdgesForExtendedLayout:UIRectEdgeAll];
	minValueAllowed = [WLTDatabaseManager sharedManager].paymentConfig.p2pMinLimit.floatValue;
    maxValueAllowed = [WLTDatabaseManager sharedManager].paymentConfig.p2pMaxLimit.floatValue;
    
    self.navigationItem.leftBarButtonItem.customView.tintColor = self.navigationController.navigationBar.tintColor;
    
    if (!self.requestingUser && self.transaction) {
        self.requestingUser = self.transaction.receiver;
    }
    
    __weak typeof(self) weakSelf = self;
    [[WLTContactsManager sharedManager] displayNameForUser:self.requestingUser defaultIsMob:YES
                                                completion:^(NSString *name) {
                                                    
                                                    if(weakSelf.transactionType == UserTransactionStateGet) {
                                                        
                                                        weakSelf.title = [NSString stringWithFormat:@"Request %@ to pay", name];
                                                    }
                                                    else if(weakSelf.transactionType == UserTransactionStateOwe) {
                                                        
                                                        weakSelf.title = [NSString stringWithFormat:@"Send money to %@", name];
                                                    }
                                                    weakSelf.requestingUserName = name;
                                                }];
    
    UIView *keyView = [self keyboardAccessoryView];
    
    {
        [amountBackView addBorderLineAtPosition:BorderLinePositionBottom];
        
        amountLabel.textColor = [UIColor cadetGrayColor];
        amountLabel.font = [UIFont sfUITextMediumOfSize:12];
        
        if (self.transaction.amount.floatValue > 0 ) {
            amountField.text = self.transaction.amount.stringValue;
        }else {
            amountField.text = @"";
        }
        
        amountField.font = [UIFont sfUITextRegularOfSize:16];
        amountField.textColor = [UIColor darkGrayTextColor];
        amountField.delegate = self;
        
        amountField.inputAccessoryView = keyView;
    }
    
    sendMoneyButtonView.hidden = NO;
    sendMoneyErrorView.hidden = YES;
    
    NSString *otherUserText = nil;
    if (self.transaction) {
        if ( self.transaction.type == PaymentTypeRequestToMe ) {
            otherUserText = self.transaction.userMessage;
        }
    }
    
    if (otherUserText.length) {
        
        otherUserCommentBackImageView.image = [otherUserCommentBackImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        otherUserCommentBackImageView.tintColor = [UIColor lightBackgroundColor];
        
        [otherUserCommentBackView setBackgroundColor:[UIColor clearColor]];
        
        [otherUserCommentLabel setPreferredMaxLayoutWidth:([UIScreen mainScreen].bounds.size.width-95)];
        otherUserCommentLabel.textColor = [UIColor cadetColor];
        otherUserCommentLabel.font = [UIFont sfUITextRegularOfSize:14];
        otherUserCommentLabel.numberOfLines = 0;
        
        otherUserCommentLabel.text = [NSString stringWithFormat:@"--\n%@", otherUserText];
        [otherUserButton displayUserIcon:self.transaction.receiver];
        
        [[WLTContactsManager sharedManager] displayNameForUser:self.transaction.receiver defaultIsMob:YES completion:^(NSString *name) {
            
            NSString *complete = [NSString stringWithFormat:@"%@\n\n%@",name, otherUserText];
            NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:complete];
            [attributed addAttributes:@{NSFontAttributeName:[UIFont sfUITextRegularOfSize:10], NSForegroundColorAttributeName:[UIColor denimColor]} range:[complete rangeOfString:name]];
            [attributed addAttributes:@{NSFontAttributeName:[UIFont sfUITextRegularOfSize:3]} range:NSMakeRange([complete rangeOfString:name].length, 2)];
            
            otherUserCommentLabel.attributedText = attributed;
        }];
        
        CGFloat height = ([otherUserCommentLabel intrinsicContentSize].height + 30);
        if (height < 60) {
            height = 60;
        }
        
        otherUserViewHeight.constant = height;
        commentBackViewYGap.constant = height;
        
        bottomViewHeightConstraint.constant = (height+80);
    }else {
        
        commentBackViewYGap.constant = 0;
        bottomViewHeightConstraint.constant = 80;
        [otherUserCommentBackView removeFromSuperview];
    }
    
    {
        currentUserCommentBackImageView.image = [currentUserCommentBackImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        currentUserCommentBackImageView.tintColor = [UIColor lightBackgroundColor];
        
        [commentBackView setBackgroundColor:[UIColor clearColor]];
        
        [currentUserButton displayUserIcon:[WLTDatabaseManager sharedManager].currentUser];
        
        NSAttributedString *attributed = [[NSAttributedString alloc] initWithString:@"Add a message" attributes:@{NSFontAttributeName:[UIFont sfUITextMediumOfSize:12], NSForegroundColorAttributeName:[UIColor cadetGrayColor]}];
        theTextView.placeHolderText = attributed;
        
        [theTextView setBackgroundColor:[UIColor clearColor]];
        [theTextView->theTextView setBackgroundColor:[UIColor clearColor]];
        theTextView->theTextView.textColor = [UIColor cadetColor];
        theTextView->theTextView.font = [UIFont sfUITextRegularOfSize:14];
        theTextView->theTextView.inputAccessoryView = keyView;
        
        [notesBackView addBorderLineAtPosition:BorderLinePositionBottom];
        [notesBackView setBackgroundColor:[UIColor whiteColor]];
    }
}

-(void)startPaymentFlow {
    
    if ([self isLoading]) {
        return;
    }
    
    if (self.transaction) {
        
        WLTContact *cont = [[WLTContact alloc] initWithName:self.transaction.receiver.name andNumber:self.transaction.receiver.mobileString];
        
        [[WLTDatabaseManager sharedManager] saveContactToRecentList:cont];
        
        __weak typeof(self) weakSelf = self;
        WLTCombinedGroupPayments *comGrp = [[WLTCombinedGroupPayments alloc] initWithUser:cont];
        comGrp.amount = amountField.text.doubleValue;
        
        NSString *oldId = self.transaction.transactionId;
        oldId = [oldId stringByReplacingOccurrencesOfString:@"requested_" withString:@""];
        
        [self showPaymentScreenForTransaction:comGrp message:self.transaction.userMessage receiverMessage:theTextView.text andOldTrans:oldId withCompletion:^(BOOL success) {
            
            if (weakSelf.completion) {
                weakSelf.completion(success);
            }
        }];
    }else if(self.requestingUser){
        
        __weak typeof(self) weakSelf = self;
        WLTCombinedGroupPayments *comGrp = [[WLTCombinedGroupPayments alloc] initWithUser:self.requestingUser];
        comGrp.amount = amountField.text.doubleValue;
        
        [self showPaymentScreenForTransaction:comGrp message:theTextView.text receiverMessage:nil andOldTrans:nil withCompletion:^(BOOL success) {
            
            if (weakSelf.completion) {
                weakSelf.completion(success);
            }
        }];
    }
}

-(void)startReceiveFlow {
    
    if ([self isLoading]) {
        return;
    }
    
    WLTLoggedInUser *loggedInUser = [WLTDatabaseManager sharedManager].currentUser;
    
    NSString *loggedInUserMobile = loggedInUser.mobileString;
    NSString *otherUserMobile = [self.requestingUser completeNumber];
    NSString *otherUserName = [self.requestingUser name];
    if (!otherUserName.length) {
        otherUserName = @"";
    }
    
    long long dateStamp = [NSDate currentTimeStamp];
    
    GTLWalnutMPaymentTransaction *trans = [[GTLWalnutMPaymentTransaction alloc] init];
    trans.completionDate = @(0);
    trans.amount = @(amountField.text.doubleValue).twoPlaceDecimalNumber;
    trans.deviceUuid = [WLTDatabaseManager sharedManager].appSettings.deviceIdentifier;
    trans.flags = @(0);
    trans.fastFundEnabled = @(NO);
    trans.initiationDate = @(dateStamp);
    trans.userMessage = theTextView.text;
    trans.instrumentUuid = @"DUMMY";
    
    NSString *newUUID = [WLTNetworkManager generateUniqueIdentifier];
    trans.type = [WLTPaymentTransaction stringForPaymentType:PaymentTypeRequestFromMe];
    trans.transactionUuid = [NSString stringWithFormat:@"request_%@", newUUID];
    
    trans.senderName = otherUserName;
    trans.senderPhone = otherUserMobile;
    
    trans.receiverName = loggedInUser.name;
    trans.receiverPhone = loggedInUserMobile;
    
    trans.subStatus = @"unknown";
    trans.status = @"";
    
    [self startLoadingWithColor:[UIColor appleGreenColor]];
    
    __weak typeof(self) weakSelf = self;
    
    GTLQueryWalnut *walN = [GTLQueryWalnut queryForPaymentTransactionAddWithObject:trans];
    [[WLTNetworkManager sharedManager].walnutService executeQuery:walN completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
        
        [weakSelf endViewLoading];
        
        if (error) {
            
            [weakSelf showAlertForError:error];
        }else {
            
            GTLWalnutMPaymentTransactions *allTransactions = (GTLWalnutMPaymentTransactions*)object;
            [[WLTPaymentTransationsManager sharedManager] handleNewPaymentObjects:allTransactions.transactions isFirstTime:NO];
            
            [weakSelf.view endEditing:YES];
            
            if (weakSelf.completion) {
                weakSelf.completion(YES);
            }
        }
    }];
}

-(void)postReceiveRequest:(LSButton*)item {
    
    NSError *error = nil;
    
    if (amountField.text.doubleValue < minValueAllowed) {
        error = [NSError walnutErrorWithMessage:[NSString stringWithFormat:@"Amount cannot be less than %@", @(minValueAllowed).displayCurrencyString]];
    }
	else if (amountField.text.doubleValue > maxValueAllowed) {
		error = [NSError walnutErrorWithMessage:[NSString stringWithFormat:@"Amount cannot be greater than %@", @(maxValueAllowed).displayCurrencyString]];
	}
	
    if (!error) {
        
        if (self.transactionType == UserTransactionStateGet) {
            
            [self startReceiveFlow];
        }
        else if (self.transactionType == UserTransactionStateOwe) {
            
            [self startPaymentFlow];
        }
    }else {
        
        [self showAlertForError:error];
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    BOOL retValue = YES;
	CGFloat stringValue = newString.doubleValue;
    if ( stringValue > maxValueAllowed ) {
        
        sendMoneyErrorView.hidden = NO;
        sendMoneyErrorView.titleLabel.text = [NSString stringWithFormat:@"Amount cannot be greater than %@", @(maxValueAllowed).displayCurrencyString];
    }else if (stringValue < minValueAllowed) {
		
		if (passedMinimumValueOnce) {
			sendMoneyErrorView.hidden = NO;
			sendMoneyErrorView.titleLabel.text = [NSString stringWithFormat:@"Amount cannot be less than %@", @(minValueAllowed).displayCurrencyString];
		}
    }else {
		
        sendMoneyErrorView.hidden = YES;
    }
	
	if (stringValue >= minValueAllowed) {
		passedMinimumValueOnce = YES;
	}
	
    return retValue;
}

@end
