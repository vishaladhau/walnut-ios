//
//  WLTMakePaymentListViewController.h
//  walnut
//
//  Created by Abhinav Singh on 02/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"

@class WLTPartialPayment;

typedef void (^ PaymentSuccessBlock)(BOOL success, GTLWalnutMPaymentTransaction *trans, GTLWalnutMPaymentInstrument *usedInstrument, BOOL newInstrument);

@interface WLTMakePaymentListViewController : WLTViewController {}

@property(nonatomic, strong) PaymentSuccessBlock completion;
@property(nonatomic, strong) WLTPartialPayment *transaction;
@property(nonatomic, strong) NSString *previousTransactionId;

@property(nonatomic, strong) NSString *userMessage;
@property(nonatomic, strong) NSString *receiverMessage;

@end
