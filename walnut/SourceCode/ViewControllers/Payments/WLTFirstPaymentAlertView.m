//
//  WLTFirstPaymentAlertView.m
//  walnut
//
//  Created by Harshad on 19/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTFirstPaymentAlertView.h"
#import "WLTDatabaseManager.h"

NSString *const WLTDefaultsFirstPaymentAlertKey = @"WLTDefaultsFirstPaymentAlertKey";

@implementation WLTFirstPaymentAlertView

+(BOOL)canShow {
    
    BOOL acceptedTerms = [[NSUserDefaults standardUserDefaults] boolForKey:WLTDefaultsFirstPaymentAlertKey];
    return !acceptedTerms;
}

-(IBAction)touchAlertCancel:(id)sender {
    
    if (self.proceedBlock) {
        self.proceedBlock(NO);
    }
    
    [self dismiss];
}

-(IBAction)touchAlertProceed:(id)sender {
    
    if (self.proceedBlock) {
        self.proceedBlock(YES);
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:WLTDefaultsFirstPaymentAlertKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self dismiss];
}

-(IBAction)touchAlertTermsAndConditions:(id)sender {
    
    NSString *urlString = [WLTDatabaseManager sharedManager].paymentConfig.termsCondUrl;
    if (!urlString.length) {
        urlString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"IciciBankTermsConditions"];
    }
    NSURL *url = [NSURL URLWithString:urlString];
    
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

@end
