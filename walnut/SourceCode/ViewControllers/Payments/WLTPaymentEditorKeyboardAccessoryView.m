//
//  WLTPaymentEditorKeyboardAccessoryView.m
//  walnut
//
//  Created by Harshad on 20/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTPaymentEditorKeyboardAccessoryView.h"

@implementation WLTPaymentEditorKeyboardAccessoryView

+ (instancetype)instantiateAccessoryViewFromNib {
    UINib *nib = [UINib nibWithNibName:@"WLTAmountRangeAlertView" bundle:nil];
    return [[nib instantiateWithOwner:nil options:nil] firstObject];
}

@end
