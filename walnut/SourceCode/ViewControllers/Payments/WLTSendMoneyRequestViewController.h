//
//  WLTSendMoneyRequestViewController.h
//  walnut
//
//  Created by Abhinav Singh on 13/08/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "KeyboardHandlerViewController.h"

@class WLTUserIconButton;

@interface WLTSendMoneyRequestViewController : KeyboardHandlerViewController {
    
    __weak IBOutlet NSLayoutConstraint *bottomViewHeightConstraint;
    
    __weak IBOutlet UIView *commentBackView;
    __weak IBOutlet NSLayoutConstraint *commentBackViewYGap;
    __weak IBOutlet WLTUserIconButton *currentUserButton;
    __weak IBOutlet UIImageView *currentUserCommentBackImageView;
    
    __weak IBOutlet WLTUserIconButton *otherUserButton;
    __weak IBOutlet UIView *otherUserCommentBackView;
    __weak IBOutlet UIImageView *otherUserCommentBackImageView;
    __weak IBOutlet UILabel *otherUserCommentLabel;
    __weak IBOutlet NSLayoutConstraint *otherUserViewHeight;
}

@property(nonatomic, assign) UserTransactionState transactionType;
@property(nonatomic, strong) WLTPaymentTransaction *transaction;

@property(nonatomic, strong) SuccessBlock completion;
@property(nonatomic, strong) id <User> requestingUser;

@end
