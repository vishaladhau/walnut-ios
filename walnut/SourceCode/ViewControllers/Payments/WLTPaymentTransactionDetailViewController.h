//
//  WLTPaymentTransactionDetailViewController.h
//  walnut
//
//  Created by Abhinav Singh on 23/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"

@class WLTPaymentTransaction;

@interface WLTPaymentTransactionDetailViewController : WLTViewController {
    
    __weak IBOutlet UITableView *theTableView;
}

@property(nonatomic, strong) CompletionBlock completion;
@property(nonatomic, strong) WLTPaymentTransaction *transaction;
-(void)refreshCurrentTransaction;

@end
