//
//  WLTMakePaymentViewController.h
//  walnut
//
//  Created by Abhinav Singh on 26/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"
#import "WLTWebViewController.h"

typedef void (^ TransSuccessBlock)(BOOL success, GTLWalnutMPaymentTransaction *trans);

@interface WLTMakePaymentViewController : WLTViewController <UIWebViewDelegate>{
    
    __weak IBOutlet UIView *errorBackView;
    __weak IBOutlet UILabel *erorrLabel;
    __weak IBOutlet UIButton *retryButon;
    __weak IBOutlet UIImageView *erorrImageView;
    
    __weak IBOutlet UIWebView *theWebView;
}

@property(nonatomic, strong) GTLWalnutMPaymentInstrument *instrument;
@property(nonatomic, strong) GTLWalnutMPaymentTransaction *transaction;

@property(nonatomic, strong) NSString *verifyCode;

@property(nonatomic, strong) TransSuccessBlock completion;

@end
