//
//  WLTRequestPaymentDetailViewController.h
//  walnut
//
//  Created by Abhinav Singh on 14/08/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"

@class WLTPaymentTransaction;

@interface WLTRequestPaymentDetailViewController : WLTViewController {
    
    __weak IBOutlet UITableView *theTableView;
}

@property(nonatomic, strong) WLTPaymentTransaction *transaction;
@property(nonatomic, strong) CompletionBlock completion;

-(void)refreshCurrentTransaction;

@end
