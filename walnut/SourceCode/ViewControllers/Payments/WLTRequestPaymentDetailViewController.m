//
//  WLTRequestPaymentDetailViewController.m
//  walnut
//
//  Created by Abhinav Singh on 14/08/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTRequestPaymentDetailViewController.h"
#import "WLTPaymentUser.h"
#import "WLTPaymentTransaction.h"
#import "WLTContactsManager.h"
#import "WLTPaymentTransactionsTableViewCell.h"
#import "WLTPaymentTransationsManager.h"
#import "WLTPaymentTransactionsTableViewCell.h"
#import "WLTTableHeaderView.h"
#import "WLTCombinedGroupPayments.h"
#import "WLTDatabaseManager.h"
#import "WLTContact.h"
#import "LSButton.h"
#import "WLTDatabaseManager.h"
#import "WLTSendMoneyRequestViewController.h"
#import "WLTFirstPaymentAlertView.h"

@interface WLTRequestPaymentDetailViewController () {
    
    
}

@property(nonatomic, strong) NSString *otherUserName;

@end

@implementation WLTRequestPaymentDetailViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    __weak typeof(self) weakSelf = self;
    
    WLTPaymentUser *otherUser = nil;
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"TrashIcon"] style:UIBarButtonItemStyleDone
                                                                 target:self action:@selector(removeTransactionClicked:)];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    if (self.transaction.isPaid) {
        
        otherUser = self.transaction.receiver;
    }else {
        otherUser = self.transaction.sender;
    }
    
    [[WLTContactsManager sharedManager] displayNameForUser:otherUser defaultIsMob:YES completion:^(NSString *data) {
        
        if (weakSelf) {
            
            if (weakSelf.transaction.isPaid) {
                [weakSelf setTitle:[NSString stringWithFormat:@"Request from %@", data]];
            }else {
                [weakSelf setTitle:[NSString stringWithFormat:@"Requested %@", data]];
            }
            
            weakSelf.otherUserName = data;
        }
    }];
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectZero];
    theTableView.tableFooterView = footer;
    theTableView.separatorInset = UIEdgeInsetsZero;
    theTableView.rowHeight = UITableViewAutomaticDimension;
    theTableView.backgroundColor = self.view.backgroundColor;
    
    [theTableView registerNib:[UINib nibWithNibName:@"WLTPaymentTransactionsTableViewCell" bundle:nil] forCellReuseIdentifier:@"WLTPaymentTransactionsTableViewCell"];
    [theTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"DefaultTableCell"];
    
    if ( self.presentingViewController && [[self.navigationController rootViewController] isEqual:self] ) {
        
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                       style:UIBarButtonItemStyleDone
                                                                      target:self action:@selector(doneClicked:)];
        self.navigationItem.rightBarButtonItem = doneButton;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paymentsChanged:) name:PaymentTransactionsChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unreadBadgeCountChanged:) name:PaymentTransactionsCountChanged object:nil];
}

-(void)unreadBadgeCountChanged:(NSNotification*)notify {
    
    if([self.navigationController.topViewController isEqual:self]) {
        
        [[WLTPaymentTransationsManager sharedManager] markTransactionIDAsViewed:self.transaction.transactionId];
    }
}

-(void)removeTransactionClicked:(UIBarButtonItem*)item {
    
    __weak typeof(self) weakSelf = self;
    void (^ deleteTransaction)(WLTPaymentTransaction *transaction) = ^void (WLTPaymentTransaction *transaction){
        
        if ([weakSelf isLoading]) {
            return;
        }
        
        WLTPaymentTransaction *toRemove = weakSelf.transaction;
        GTLWalnutMPaymentTransaction *toRemoveTransBacked = [[GTLWalnutMPaymentTransaction alloc] init];
        toRemoveTransBacked.amount = toRemove.amount;
        toRemoveTransBacked.instrumentUuid = @"DUMMY";
        toRemoveTransBacked.type = [WLTPaymentTransaction stringForPaymentType:toRemove.type];
        toRemoveTransBacked.deleted = @(YES);
        toRemoveTransBacked.deviceUuid = toRemove.deviceUuid;
        toRemoveTransBacked.initiationDate = @([toRemove.initiationDate timeStamp]);
        toRemoveTransBacked.userMessage = toRemove.userMessage;
        toRemoveTransBacked.receiverName = [toRemove.receiver name];
        toRemoveTransBacked.status = @"";
        toRemoveTransBacked.receiverPhone = [toRemove.receiver completeNumber];
        toRemoveTransBacked.senderName = [toRemove.sender name];
        toRemoveTransBacked.senderPhone = [toRemove.sender completeNumber];
        toRemoveTransBacked.transactionUuid = toRemove.transactionId;
        
        [weakSelf startLoading];
        
        GTLQueryWalnut *query = [GTLQueryWalnut queryForPaymentTransactionAddWithObject:toRemoveTransBacked];
        [[WLTNetworkManager sharedManager].walnutService executeQuery:query completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
            
            if (!error) {
                
                [toRemove.managedObjectContext deleteObject:toRemove];
                [[WLTDatabaseManager sharedManager] saveDataBase];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:PaymentTransactionsChanged object:nil];
                
                if (weakSelf.completion) {
                    weakSelf.completion();
                }else {
                    
                    [weakSelf.navigationController popToRootViewControllerAnimated:YES];
                }
            }else {
                
                [weakSelf showAlertForError:error];
            }
            
            [weakSelf endViewLoading];
        }];
    };
    
    NSString *title = nil;
    
    if (weakSelf.transaction.isPaid) {
        title = [NSString stringWithFormat:@"Are you sure to delete request sent from %@", self.otherUserName];
    }else {
        title = [NSString stringWithFormat:@"Are you sure to delete request sent to %@", self.otherUserName];
    }
    
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Please Confirm!" message:title preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        deleteTransaction(weakSelf.transaction);
    }];
    
    [controller addAction:cancelAction];
    [controller addAction:deleteAction];
    
    [self presentViewController:controller animated:YES completion:nil];
    
    
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[WLTPaymentTransationsManager sharedManager] markTransactionIDAsViewed:self.transaction.transactionId];
    [self refreshCurrentTransaction];
}

-(void)doneClicked:(UIBarButtonItem*)item {
    
    if (self.completion) {
        self.completion();
    }
}

-(void)payAmountClicked:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    void (^ showControllerForTransaction)(WLTPaymentTransaction *transaction) = ^void (WLTPaymentTransaction *transaction){
        
        WLTSendMoneyRequestViewController *controller = [weakSelf controllerWithIdentifier:@"WLTSendMoneyRequestViewController"];
        controller.transaction = transaction;
        controller.transactionType = UserTransactionStateOwe;
        [weakSelf.navigationController pushViewController:controller animated:YES];
        
        [controller setCompletion:^(BOOL success){
            
            if (success) {
                
                [weakSelf.navigationController popToRootViewControllerAnimated:YES];
            }
        }];
    };
    
    if ([WLTFirstPaymentAlertView canShow]) {
        
        WLTFirstPaymentAlertView *alert = [WLTFirstPaymentAlertView initWithDefaultXib];
        [alert setProceedBlock:^(BOOL success){
            
            if (success) {
                
                showControllerForTransaction(weakSelf.transaction);
            }
        }];
        [alert show];
    }else {
        
        showControllerForTransaction(weakSelf.transaction);
    }
}

-(void)refreshCurrentTransaction {
    
    __weak typeof(self) weakSelf = self;
    [self startLoading];
    [[WLTPaymentTransationsManager sharedManager] fetchAllPaymentTransactionsCompletion:^(BOOL success, NSError *error) {
        
        if ([weakSelf.transaction isDeleted] || !weakSelf.transaction.transactionId.length) {
            
            if (weakSelf.completion) {
                weakSelf.completion();
            }else {
                
                [weakSelf.navigationController popToRootViewControllerAnimated:YES];
            }
        }
        
        [weakSelf endViewLoading];
    }];
}

#pragma mark - Payments Changed

-(void)paymentsChanged:(NSNotification*)notify {
    
    [theTableView reloadData];
}

#pragma mark - UITableViewDelegate & UITableViewDatasource

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *tcell = nil;
    if (indexPath.section == 0) {
        
        if(indexPath.row == 0) {
            
            WLTPaymentTransactionsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WLTPaymentTransactionsTableViewCell"];
            [cell showDetailsOfTransaction:self.transaction];
            
            tcell = cell;
        }
        else if (indexPath.row == 1) {
            
            UITableViewCell *cell = [theTableView dequeueReusableCellWithIdentifier:@"payTableCell"];
            if (!cell) {
                
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"payTableCell"];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                LSButton *button = [[LSButton alloc] initWithFrame:CGRectZero];
                [button addTarget:self action:@selector(payAmountClicked:) forControlEvents:UIControlEventTouchUpInside];
                [button setTitle:@"PAY NOW"];
                [button applyDefaultStyle];
                button.translatesAutoresizingMaskIntoConstraints = NO;
                [cell.contentView addSubview:button];
                
                NSDictionary *dict = NSDictionaryOfVariableBindings(button);
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[button]-10-|" options:0 metrics:nil views:dict]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[button(==40)]-5-|" options:0 metrics:nil views:dict]];
            }
            
            tcell = cell;
        }
    }
    else if (indexPath.section == 1) {
        
        tcell =  [theTableView dequeueReusableCellWithIdentifier:@"DefaultTableCell"];
        tcell.textLabel.font = [UIFont sfUITextRegularOfSize:16];
        tcell.textLabel.textColor = [UIColor cadetColor];
        
        tcell.textLabel.text = @"Email Us";
        tcell.imageView.image = nil;
    }
    
    return tcell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger sectionCnt = 2;
    return sectionCnt;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger rowCnt = 1;
    if ( (section == 0) && self.transaction.isPaid) {
        if ( (self.transaction.type == PaymentTypeRequestFromMe) || (self.transaction.type == PaymentTypeRequestToMe)) {
            rowCnt += 1;
        }
    }
    
    return rowCnt;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 90;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    CGFloat height = 0.1;
    if (section == 1) {
        height = 50;
    }
    
    return height;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *vHeader = nil;
    if (section == 1) {
        
        WLTTableHeaderView *header = [[WLTTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
        header.label.text = @"NEED HELP?";
        
        vHeader = header;
    }
    
    return vHeader;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    if (indexPath.section == 1) {
        
        [self sendSupportEmailForTransaction:self.transaction];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
