//
//  WLTMakePaymentViewController.m
//  walnut
//
//  Created by Abhinav Singh on 26/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTMakePaymentViewController.h"
#import "WLTPaymentConfig.h"
#import "WLTGroup.h"
#import "WLTPaymentTransaction.h"
#import "WLTGroupsManager.h"
#import "WLTContactsManager.h"
#import "LSToastMessage.h"

static NSString *PaynimoErrorDomain = @"PaynimoErrorDomain";
static const NSInteger InvalidIssuerErrorCode = 1000;

static NSString *ShowHideJavaScript = @"(function () {\
var hasNonHiddenFields = '0';\
var inputs = document.getElementsByTagName('input');\
for(var i = 0; i < inputs.length; i++) {\
if(inputs[i].type.toLowerCase() !== 'hidden') {\
hasNonHiddenFields = '1';\
}\
}\
return hasNonHiddenFields;\
})();";

static NSString *RedirectURLString = @"https://storage.googleapis.com/walnut_payment/payments.html";

@interface WLTMakePaymentViewController () {
    
    NSTimer *slowNetworkDetectorTimer;
    
    NSMutableDictionary *paymentDictionary;
    NSDate *webPageLoadStartTime;
    __weak UIViewController *presentedBackController;
}

@end

@implementation WLTMakePaymentViewController

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    NSString *name = self.transaction.receiverName;
    if (!name.length) {
        name = self.transaction.receiverPhone;
    }
    
    [self setTitle:[NSString stringWithFormat:@"Paying %@ %@", name, self.transaction.amount.displayCurrencyString]];
    
    [retryButon.titleLabel setFont:[UIFont sfUITextMediumOfSize:14]];
    retryButon.tintColor = [UIColor appleGreenColor];
    
    theWebView.hidden = YES;
    
    [self addBackBarButtonWithSelector:@selector(backClicked:)];
    [self makePaymentAPICall];
}

-(IBAction)retryClicked:(UIButton*)retryButton {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)checkAndRemoveBackAlert {
    
    if (presentedBackController) {
        [presentedBackController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)showErrorStateForError:(NSError*)error {
    
    if (error) {
        
        [self endSlowNetworkTimer];
        
        errorBackView.hidden = NO;
        
        NSString *title = nil;;
        NSString *info = nil;
        
        if ([error.domain isEqualToString:PaynimoErrorDomain]) {
            if (error.code == InvalidIssuerErrorCode) {
				
				info = error.userInfo[@"message"];
            }
        }
        else if ([error.domain isEqualToString:WalnutErrorDomain]) {
            
            info = error.userInfo[@"message"];
            title = error.userInfo[@"title"];
        }
        else if ([error.domain isEqualToString:NSURLErrorDomain]) {
            
            if ((error.code == NSURLErrorTimedOut) || (error.code == NSURLErrorNetworkConnectionLost) || (error.code == NSURLErrorNotConnectedToInternet)) {
                
                title = @"No Network!";
                info = @"Please check your internet connection and try again later!";
            }
        }
        if ([error.domain isEqualToString:kGTLJSONRPCErrorDomain]) {
            
            NSString *errorString = [error userInfo][@"error"];
            if (errorString.length) {
                
                NSData *data = [errorString dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *object = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                info = object[@"Error"];
            }
        }
        
        if (!title) {
            title = @"Oops";
        }
        if (!info) {
            info = @"Please try again later!";
        }
        
        NSString *complete = [NSString stringWithFormat:@"%@\n%@", title, info];
        
        NSMutableAttributedString *attributted = [[NSMutableAttributedString alloc] initWithString:complete attributes:@{NSForegroundColorAttributeName:[UIColor cadetColor], NSFontAttributeName:[UIFont sfUITextMediumOfSize:17]}];
        [attributted addAttributes:@{NSFontAttributeName:[UIFont sfUITextRegularOfSize:14]} range:[complete rangeOfString:info]];
        
        erorrLabel.attributedText = attributted;
    }
    else {
        errorBackView.hidden = YES;
    }
}

-(void)updateTransaction:(GTLWalnutMPaymentTransaction*)trans {
    
    __weak WLTMakePaymentViewController *weakSelf = self;
    
    [self startLoading];
    GTLQueryWalnut *query = [GTLQueryWalnut queryForPaymentTransactionAddWithObject:trans];
    [[WLTNetworkManager sharedManager].walnutService executeQuery:query completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
        
        if (!error) {
            
            NSMutableArray *allChangedIds = [NSMutableArray new];
            
            GTLWalnutMPaymentTransaction *added = trans;
            GTLWalnutMPaymentTransactions *allTransactions = (GTLWalnutMPaymentTransactions*)object;
            
            for ( GTLWalnutMPaymentTransaction *trns in allTransactions.transactions ) {
                
                for ( GTLWalnutMPaymentTransactionGroup *grpChanged in trns.groupDetails ) {
                    [allChangedIds addObject:grpChanged.groupUuid];
                }
                if ([trans.transactionUuid isEqualToString:trns.transactionUuid]) {
                    added = trns;
                }
            }
            
            [weakSelf trackEvent:GA_ACTION_PAYMENT_SUCCESS andValue:added.amount];
            
            [[WLTGroupsManager sharedManager] fetchEveryChangeOfGroupsIDS:allChangedIds completion:^(NSArray *grpArr, NSError *error) {
                
                for ( WLTGroup *grpChanged in grpArr ) {
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:NotifyGroupChanged object:grpChanged];
                }
                
                PaymentStatus status =  [WLTPaymentTransaction paymentStatusFromString:trans.status];
                if ( status >= PaymentStatusPullSuccess ) {
                    
                    [weakSelf checkAndRemoveBackAlert];
                    
                    if (weakSelf.completion) {
                        weakSelf.completion(YES, added);
                    }
                }else {
                    
                    [weakSelf checkAndRemoveBackAlert];
                    
                    if (weakSelf.completion) {
                        weakSelf.completion(NO, added);
                    }
                }
                
                [weakSelf endViewLoading];
            }];
        }else {
            
            [weakSelf endViewLoading];
            
            [weakSelf trackEvent:GA_ACTION_PAYMENT_FAILED andValue:weakSelf.transaction.amount];
            [weakSelf showErrorStateForError:error];
        }
    }];
}

-(void)backClicked:(UIBarButtonItem*)item {
    
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Please Confrim!" message:@"Are you sure you want to cancel this transaction?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:nil];
    
    __weak WLTMakePaymentViewController *weakSelf = self;
    
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        [weakSelf trackEvent:GA_ACTION_PAYMENT_CANCELLED andValue:weakSelf.transaction.amount];
        
        if (weakSelf.completion) {
            weakSelf.completion(NO, self.transaction);
        }
    }];
    
    [controller addAction:cancel];
    [controller addAction:done];
    
    [self presentViewController:controller animated:YES completion:nil];
    
    presentedBackController = controller;
}

-(void)addPaymentTransactionToWalnut:(NSDictionary*)jsonDict {
    
    NSDictionary *payMethod = jsonDict[@"paymentMethod"];
    NSDictionary *payMethodTrans = payMethod[@"paymentTransaction"];
    NSDictionary *payMethodTransSender = payMethodTrans[@"sender"];
    
    NSString *message = payMethodTrans[@"statusMessage"];
    
    if ([message isEqualToString:@"success"]) {
        
        NSString *dateTime = payMethodTrans[@"dateTime"];
        NSString *paynimoStatusCode = payMethodTrans[@"statusCode"];
        NSString *paynimoTransID = payMethodTrans[@"identifier"];
        
        NSString *paynimoSendTxnID = payMethodTransSender[@"paynimoSendTxnID"];
        
        NSDateFormatter *dateForm = [[NSDateFormatter alloc] init];
        [dateForm setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
        NSDate *transDate = [dateForm dateFromString:dateTime];
        long long dateStamp = [transDate timeStamp];
        
        GTLWalnutMPaymentTransaction *trans = [self.transaction copy];
        
        trans.completionDate = @(dateStamp);
        
        trans.status = [WLTPaymentTransaction stringForStatus:PaymentStatusPullSuccess];
        trans.paynimoReferenceId = paynimoSendTxnID;
        trans.paynimoStatus = paynimoStatusCode;
        trans.paynimoTransactionId = paynimoTransID;
        
        NSData *data = [NSJSONSerialization dataWithJSONObject:jsonDict
                                                       options:0 error:nil];
        
        trans.paynimoResponse = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        [self updateTransaction:trans];
    }else {
        
        NSError *error = nil;
        NSString *errorMessage =  [[[jsonDict[@"paymentMethod"] nullCheck][@"paymentTransaction"] nullCheck][@"errorMessage"] nullCheck];
		NSRange range = [errorMessage rangeOfString:@"-"];
		NSString *acctualMessage = nil;
		if (range.location != NSNotFound) {
			
			acctualMessage = [errorMessage substringFromIndex:(range.location+1)];
			error = [NSError errorWithDomain:PaynimoErrorDomain code:InvalidIssuerErrorCode userInfo:@{@"message":acctualMessage}];
		}
		else {
            error = [NSObject walnutErrorDefault];
        }
        
        [self trackEvent:GA_ACTION_PAYMENT_FAILED andValue:self.transaction.amount];
        [self showErrorStateForError:error];
    }
}

-(void)loadWebViewFromParameters:(NSDictionary*)params {
    
    NSDictionary *aCS = params[@"paymentMethod"][@"aCS"];
    NSString *stringUrl = aCS[@"bankAcsUrl"];
    
    NSError *connectionError = nil;
    
    if (!stringUrl || ![stringUrl isKindOfClass:[NSString class]] || !stringUrl.length) {
        
        NSDictionary *errorDict = params[@"paymentMethod"][@"error"];
        NSString *string = errorDict[@"desc"];
        
        if (string.length) {
            
            NSArray *allCompo = [string componentsSeparatedByString:@"|"];
            NSString *errorStr = [allCompo lastObject];
            if (errorStr.length) {
                
                connectionError = [NSObject walnutErrorWithMessage:errorStr];
            }
        }
        
        if (!connectionError) {
            
            connectionError = [NSObject walnutErrorDefault];
        }
    }
    
    if (!connectionError) {
        
        webPageLoadStartTime = [NSDate date];
        
        NSString *method = [aCS[@"bankAcsHttpMethod"] uppercaseString];
        if (!method.length) {
            method = @"POST";
        }
        
        NSArray *paramsArray = aCS[@"bankAcsParams"];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:aCS[@"bankAcsUrl"]]];
        [request setHTTPMethod:method];
        
        NSMutableString *paramsString = [@"" mutableCopy];
        
        for ( NSDictionary *keyDict in paramsArray ) {
            
            NSArray *allKeys = keyDict.allKeys;
            for ( NSString *key in allKeys ) {
                
                [paramsString appendString:[NSString stringWithFormat:@"%@=%@&", key, keyDict[key]]];
            }
        }
        
        if (paramsString.length) {
            
            [paramsString replaceCharactersInRange:NSMakeRange((paramsString.length-1), 1) withString:@""];
        }
        
        NSData *data = [paramsString dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:data];
        [theWebView loadRequest:request];
    }else {
        
        [self showErrorStateForError:connectionError];
    }
}

-(void)showSlowNetworkToast:(NSTimer*)timer {
    
    if ([timer isValid]) {
        
        NSString *complete = @"Hang on, Network seems slow";
        NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:complete attributes:@{NSFontAttributeName:[UIFont sfUITextRegularOfSize:14], NSForegroundColorAttributeName:[UIColor whiteColor]}];
        [attributed addAttributes:@{NSFontAttributeName:[UIFont sfUITextMediumOfSize:14]} range:[complete rangeOfString:@"Hang on,"]];
        [LSToastMessageHandler showMessage:attributed withID:SlowNetwork];
        
        [timer invalidate];
        slowNetworkDetectorTimer = nil;
    }
}

-(void)endSlowNetworkTimer {
    
    if ([slowNetworkDetectorTimer isValid]) {
        [slowNetworkDetectorTimer invalidate];
    }
    slowNetworkDetectorTimer = nil;
}

-(void)makePaymentAPICall {
    
    if ([slowNetworkDetectorTimer isValid]) {
        [slowNetworkDetectorTimer invalidate];
    }
    
    slowNetworkDetectorTimer = nil;
    slowNetworkDetectorTimer = [NSTimer scheduledTimerWithTimeInterval:8 target:self
                                                  selector:@selector(showSlowNetworkToast:)
                                                  userInfo:nil repeats:NO];
    
    errorBackView.hidden = YES;
    theWebView.hidden = YES;
	
    [self trackEvent:GA_ACTION_PAYMENT_INITIATED andValue:nil];
    
    WLTLoggedInUser *user = [WLTDatabaseManager sharedManager].currentUser;
    WLTDatabaseManager *manager = [WLTDatabaseManager sharedManager];
    WLTPaymentConfig *dbConfig = [[manager allObjectsOfClass:@"WLTPaymentConfig"] firstObject];
    
    NSString *custAliasID = dbConfig.aliasId;
    NSString *paynimoCustID = dbConfig.userId;
    
    NSString *cardRegPGMID = dbConfig.cardRegPgmId;
    NSString *txnPGMID = dbConfig.txnPgmId;
    
    NSString *merchantIdent = dbConfig.identifierProperty;
    NSString *requestType = @"TCD";
    
    NSString *custName = user.name;
    
    NSString *custEmailID = user.email;
    
    NSDateFormatter *todaysDateFormat = [[NSDateFormatter alloc] init];
    [todaysDateFormat setDateFormat:@"dd-MM-yyyy"];
    NSString *todaysDate = [todaysDateFormat stringFromDate:[NSDate date]];
    
    NSString *cvvNumber = self.verifyCode;
    NSNumber *amount = self.transaction.amount;
    NSString *transIdentifier = self.transaction.transactionUuid;
    
    NSMutableDictionary *parameters = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"DefaultJsonParameters" ofType:@"data"]];
    
    NSMutableDictionary *cartItem = [parameters[@"cart"][@"item"] firstObject];
    cartItem[@"amount"] = amount;
    
    NSMutableDictionary *consumer = parameters[@"consumer"];
    NSMutableDictionary *consAlias = [consumer[@"consumerAlias"] firstObject];
    consAlias[@"custAliasId"] = custAliasID;
    
    consumer[@"emailID"] = custEmailID;
    consumer[@"identifier"] = custAliasID;
    
    consumer[@"name"] = custName;
    consumer[@"paynimoCustRegId"] = paynimoCustID;
    
    NSMutableDictionary *merchant = parameters[@"merchant"];
    merchant[@"cardRegPGMID"] = cardRegPGMID;
    merchant[@"identifier"] = merchantIdent;
    merchant[@"txnPGMID"] = txnPGMID;
    merchant[@"responseEndpointURL"] = RedirectURLString;
    
    NSMutableDictionary *instrument = parameters[@"payment"][@"instrument"];
    instrument[@"token"] = self.instrument.cardTokenNumber;
    instrument[@"verificationCode"] = cvvNumber;
    
    NSMutableDictionary *transaction = parameters[@"transaction"];
    transaction[@"action"] = @"Create";
    transaction[@"amount"] = amount;
    transaction[@"forced3DSCall"] = @"Y";
    transaction[@"isRegistration"] = @"N";
    transaction[@"identifier"] = transIdentifier;
    transaction[@"dateTime"] = todaysDate;
    transaction[@"requestType"] = requestType;
    transaction[@"reference"] = transIdentifier;
    transaction[@"requestType"] = @"TCD";
    transaction[@"smsSending"] = @"N";
    transaction[@"subType"] = @"001";
    transaction[@"type"] = @"004";
    transaction[@"token"] = @"";
    
    NSMutableDictionary *paySender = transaction[@"payment"][@"sender"];
    paySender[@"paynimoSenderAliasToken"] = custAliasID;
    paySender[@"paynimoSenderName"] = custName;
    paySender[@"paynimoSenderInstrumentToken"] = self.instrument.cardTokenNumber;
    
    paymentDictionary = parameters;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.paynimo.com/api/paynimoP2PV2.req"]];
    [request setHTTPMethod:@"POST"];
    
    NSData *bodyData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    [request setHTTPBody:bodyData];
    
    __weak WLTMakePaymentViewController *weakSelf = self;
    
    [self startLoading];
    
    __block NSDate *startTime = [NSDate date];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        
        NSDictionary *jsonDict = nil;
        
        [weakSelf trackEvent:GA_ACTION_PAYNIMO_API_TIME andValue:@(-[startTime timeIntervalSinceNow])];
        if (!connectionError) {
            jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        }
        
        [weakSelf endViewLoading];
        
        if (connectionError) {
            
            [weakSelf showErrorStateForError:connectionError];
        }else {
            
            [weakSelf loadWebViewFromParameters:jsonDict];
        }
    }];
}

-(void)makeAddToPaynimoAPICallForParameters:(NSDictionary*)dict {
    
    NSString *txthdntpslmrctcd = dict[@"txthdntpslmrctcd"];
    NSString *txthdnMsg = dict[@"txthdnMsg"];
    
    if (txthdnMsg.length && txthdntpslmrctcd.length) {
        
        NSMutableDictionary *sentDict = [paymentDictionary mutableCopy];
        NSMutableDictionary *merchant = sentDict[@"merchant"];
        merchant[@"cardRegPGMID"] = txthdntpslmrctcd;
        merchant[@"txnPGMID"] = txthdntpslmrctcd;
        merchant[@"identifier"] = txthdntpslmrctcd;
        
        NSMutableDictionary *instrument = sentDict[@"payment"][@"instrument"];
        NSMutableDictionary *instruction = sentDict[@"payment"][@"instruction"];
        NSMutableDictionary *method = sentDict[@"payment"][@"method"];
        
        instruction[@"action"] = @"";
        
        instrument[@"type"] = @"";
        instrument[@"verificationCode"] = @"";
        
        method[@"token"] = @"";
        method[@"type"] = @"";
        
        NSMutableDictionary *transaction = sentDict[@"transaction"];
        
        transaction[@"description"] = txthdnMsg;
        transaction[@"isRegistration"] = @"Y";
        transaction[@"requestType"] = @"TAR";
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.paynimo.com/api/paynimoP2PV2.req"]];
        [request setHTTPMethod:@"POST"];
        
        NSData *bodyData = [NSJSONSerialization dataWithJSONObject:sentDict options:0 error:nil];
        [request setHTTPBody:bodyData];
        
        [self startLoading];
        __weak typeof(self) weakSelf = self;
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
            
            NSDictionary *jsonDict = nil;
            
            if (!connectionError) {
                
                jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            }
            
            [weakSelf endViewLoading];
            
            if (connectionError) {
                
                [weakSelf showErrorStateForError:connectionError];
            }else {
                
                [weakSelf addPaymentTransactionToWalnut:jsonDict];
            }
        }];
    }
    else {
        
        [self showErrorStateForError:[NSError walnutErrorDefault]];
    }
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    BOOL canLoad = YES;
    
    NSLog(@"---------------------------");
    NSLog(@"Request:%@", request);
    NSLog(@"---------------------------");
    
    if ([request.URL.absoluteString rangeOfString:RedirectURLString].location == 0) {
        
        canLoad = NO;
        
        NSURLComponents *components = [NSURLComponents componentsWithURL:request.URL resolvingAgainstBaseURL:NO];
        NSArray *params = [components queryItems];
        
        NSMutableDictionary *dict = [NSMutableDictionary new];
        for ( NSURLQueryItem *item in params ) {
            dict[item.name] = item.value;
        }
        
        [self makeAddToPaynimoAPICallForParameters:dict];
    }
    
    return canLoad;
}


-(void)webViewDidStartLoad:(UIWebView *)webView {
    
    [self startLoading];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    
    NSString *canShow = [webView stringByEvaluatingJavaScriptFromString:ShowHideJavaScript];
    webView.hidden = !canShow.boolValue;
    
    if (!webView.hidden) {
        [self endSlowNetworkTimer];
    }
    
    if (webPageLoadStartTime && !webView.hidden) {
        
        [self trackEvent:GA_ACTION_2FA_PAGE_LOAD_TIME andValue:@(-[webPageLoadStartTime timeIntervalSinceNow])];
        webPageLoadStartTime = nil;
    }
    
    [self endViewLoading];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    if ([error.domain isEqualToString:@"WebKitErrorDomain"] && (error.code == 102)) {
        //ignore Frame load interrupted error
    }else {
        
        [self endViewLoading];
        [self showErrorStateForError:error];
        [self trackEvent:GA_ACTION_PAYMENT_FAILED andValue:nil];
    }
}

@end
