//
//  WLTMakePaymentListViewController.m
//  walnut
//
//  Created by Abhinav Singh on 02/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTMakePaymentListViewController.h"
#import "WLTEmptyStateView.h"
#import "WLTContactsManager.h"
#import "WLTMakePaymentViewController.h"
#import "WLTCardView.h"
#import "LSButton.h"
#import "WLTCVVEnterView.H"
#import "WLTCVVDotView.h"
#import "WLTInstrumentsManager.h"
#import "WLTPartialPayment.h"
#import "WLTGroupPaymentAmountEditViewController.h"
#import "WLTGroup.h"
#import "WLTPaymentTransaction.h"
#import "WLTCardEntryAccessoryView.h"
#import "WLTEmptyCardView.h"
#import "LSToastMessage.h"
#import "WLTCardsScrollView.h"
#import "WLTDatabaseManager.h"
#import "WLTGroupsManager.h"
#import "WLTPaymentEditorKeyboardAccessoryView.h"

CGFloat PayButtonSize = 60;

@interface WLTMakePaymentListViewController () <WLTCardsScrollViewDelegate, UITextFieldDelegate, UIScrollViewDelegate, WLTCardEntryAccessoryViewDelegate>{
    
    NSString *justAddedCardID;
    
    __weak IBOutlet UIView *headerView;
    __weak IBOutlet UITextField *amountField;
    __weak IBOutlet NSLayoutConstraint *amountFieldWidth;
    
    __weak IBOutlet UILabel *rupeeIconLabel;
    
    __weak IBOutlet UIButton *editButton;
    __weak IBOutlet NSLayoutConstraint *editButtonWidth;
    
    __weak WLTCardEntryAccessoryView *entryView;
    __weak NSLayoutConstraint *bottomMargin;
    
    __weak IBOutlet WLTCardsScrollView *theScrollView;
    
    NSString *toPayUserName;
    __weak UILabel *titleLabel;
    
    CreditCardBrand lastDetectedBrand;
    
    WLTPaymentEditorKeyboardAccessoryView *amoutErrorView;
}

@property(nonatomic, strong) NSMutableArray *addedInstrumentIDs;

@property(nonatomic, assign) CGFloat appliedDiscount;

@property(nonatomic, strong) NSArray *allInstruments;
@property(nonatomic, strong) NSString *displayedUserName;
@property(nonatomic, assign) BOOL isP2PPaymemt;
@end

@implementation WLTMakePaymentListViewController

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self makeEntryViewFirstResponderForCurrentSelection];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

-(void)keyboardWillShow:(NSNotification*)notify {
    
    NSDictionary *userInfo = notify.userInfo;
    
    NSTimeInterval animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationOptions animationCurve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    CGRect keyboardScreenEndFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    CGRect keyboardViewEndFrame = [self.view convertRect:keyboardScreenEndFrame fromView:self.view.window];
    
    CGFloat newBottom = -(keyboardViewEndFrame.size.height);
    
    
    if (newBottom > bottomMargin.constant) {
        
        bottomMargin.constant = newBottom;
        UIViewAnimationOptions animationOptions = animationCurve | UIViewAnimationOptionBeginFromCurrentState;
        [UIView animateWithDuration:animationDuration delay:0 options:animationOptions animations:^{
            
            [self.view layoutIfNeeded];
        } completion:nil];
    }else {
        
        bottomMargin.constant = newBottom;
        [self.view layoutIfNeeded];
    }
}

-(void)keyboardWillHide:(NSNotification *)notification {
    
    bottomMargin.constant = 0;
}

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    if (self.transaction.getGroups.count || self.transaction.payGroups.count) {
        _isP2PPaymemt = NO;
    }else {
        _isP2PPaymemt = YES;
    }
    
    self.addedInstrumentIDs = [NSMutableArray new];
    
    lastDetectedBrand = CreditCardBrandInvalid;
    theScrollView.delegate = self;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    WLTCardEntryAccessoryView *inputView = [WLTCardEntryAccessoryView initWithDefaultXib];
    inputView.flowType = WLTEntryFlowTypePay;
    inputView.delegate = self;
    inputView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:inputView];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(inputView);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[inputView]-0-|" options:0 metrics:nil views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[inputView(==45)]" options:0 metrics:nil views:dict]];
    
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:inputView
                                                                  attribute:NSLayoutAttributeBottom
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self.view
                                                                  attribute:NSLayoutAttributeBottom
                                                                 multiplier:1 constant:0];
    [self.view addConstraint:constraint];
    [inputView layoutIfNeeded];
    
    bottomMargin = constraint;
    entryView = inputView;
    
    headerView.backgroundColor = [UIColor appleGreenColor];
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-40, 44)];
    
    UILabel *titlView = [[UILabel alloc] initWithFrame:CGRectZero];
    titlView.translatesAutoresizingMaskIntoConstraints = NO;
    titlView.adjustsFontSizeToFitWidth = YES;
    titlView.minimumScaleFactor = 0.3;
    titlView.textAlignment = NSTextAlignmentLeft;
    titlView.textColor = [UIColor whiteColor];
    titlView.numberOfLines = 0;
    [backView addSubview:titlView];
    
    self.navigationItem.titleView = backView;
    
    dict = NSDictionaryOfVariableBindings(titlView);
    [backView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[titlView]-0-|" options:0 metrics:nil views:dict]];
    [backView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-3-[titlView]-3-|" options:0 metrics:nil views:dict]];
    
    titleLabel = titlView;
    
    {
        amountField.textColor = [UIColor whiteColor];
        amountField.userInteractionEnabled = NO;
        amountField.font = [UIFont sfUITextLightOfSize:32];
        
        if (!self.isP2PPaymemt) {
            
            [editButton setImage:[UIImage imageNamed:@"EditIcon"] forState:UIControlStateNormal];
            editButtonWidth.constant = 35;
            
            editButton.tintColor = [UIColor whiteColor];
            editButton.layer.cornerRadius = 3;
            editButton.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];
        }else {
            
            editButton.hidden = YES;
        }
        
        rupeeIconLabel.textColor = amountField.textColor;
        rupeeIconLabel.font = amountField.font;
        rupeeIconLabel.text = @"₹";
    }
    
    __weak typeof(self) weakSelf = self;
    [[WLTContactsManager sharedManager] displayNameForUser:self.transaction.user defaultIsMob:YES completion:^(NSString *data) {
        
        if (weakSelf) {
            
            toPayUserName = data;
            if (weakSelf.isP2PPaymemt) {
                [weakSelf changePaymentAmountCanShowAlert:NO];
            }else {
                [weakSelf changePaymentAmountCanShowAlert:YES];
            }
        }
    }];
    
    [self addImagesBackBarButtonWithSelector:@selector(backClicked:)];
    
    [self startLoadingWithColor:[UIColor appleGreenColor]];
    [[WLTContactsManager sharedManager] displayNameForUser:self.transaction.user defaultIsMob:YES completion:^(NSString *data) {
        
        weakSelf.displayedUserName = data;
        [weakSelf endViewLoading];
    }];
    
    [self refreshCardsList];
}

-(void)changePaymentAmountCanShowAlert:(BOOL)alert {
    
    if (alert) {
        
        NSNumber *maxLimit = [WLTDatabaseManager sharedManager].paymentConfig.p2pMaxLimit;
        NSNumber *minLimit = [WLTDatabaseManager sharedManager].paymentConfig.p2pMinLimit;
        
        if (self.transaction.currentAmount > maxLimit.floatValue) {
            
            NSString *amountStr = maxLimit.displayCurrencyString;
            NSString *complete = [NSString stringWithFormat:@"Amount cannot be greater than %@", amountStr];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!" message:complete delegate:nil
                                                  cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
            
            self.transaction.currentAmount = maxLimit.floatValue;
        }else if (self.transaction.currentAmount < minLimit.floatValue) {
            
            NSString *amountStr = minLimit.displayCurrencyString;
            NSString *complete = [NSString stringWithFormat:@"Amount cannot be less than %@", amountStr];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!" message:complete delegate:nil
                                                  cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
            
            self.transaction.currentAmount = minLimit.floatValue;
        }
    }
    
    if (!self.isP2PPaymemt) {
        
        NSString *amountStr = @(self.transaction.currentAmount).displayRoundedString;
        
        NSString *complete = [NSString stringWithFormat:@"Paying %@", toPayUserName];
        NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:complete attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],
                                                                                                                        NSFontAttributeName:[UIFont sfUITextBoldOfSize:14]}];
        [attributed setAttributes:@{NSFontAttributeName:[UIFont sfUITextRegularOfSize:14]} range:[complete rangeOfString:@"Paying"]];
        
        titleLabel.attributedText = attributed;
        
        amountField.text = amountStr;
        
        CGFloat width = [amountField intrinsicContentSize].width;
        amountFieldWidth.constant = width;
        [amountField.superview layoutIfNeeded];
    }else {
        
        NSString *complete = [NSString stringWithFormat:@"Sending money to %@", toPayUserName];
        NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:complete attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],
                                                                                                                        NSFontAttributeName:[UIFont sfUITextBoldOfSize:14]}];
        [attributed setAttributes:@{NSFontAttributeName:[UIFont sfUITextRegularOfSize:14]} range:[complete rangeOfString:@"Sending money to"]];
        
        titleLabel.attributedText = attributed;
        
        if (self.transaction.currentAmount > 0) {
            
            NSString *amountStr = @(self.transaction.currentAmount).displayRoundedString;
            amountField.text = amountStr;
        }
        
        CGFloat width = ([[UIScreen mainScreen] bounds].size.width - (20+40+editButtonWidth.constant+20));
        if (amountFieldWidth.constant != width) {
            
            amountFieldWidth.constant = width;
            [amountField.superview layoutIfNeeded];
        }
    }
}

-(IBAction)editAmountClicked:(id)sender {
    
    if (self.isP2PPaymemt) {
        
        NSNumber *maxLimit = [WLTDatabaseManager sharedManager].paymentConfig.p2pMaxLimit;
        NSNumber *minLimit = [WLTDatabaseManager sharedManager].paymentConfig.p2pMinLimit;
        
        if (self.transaction.currentAmount > maxLimit.floatValue) {
            
            NSString *amountStr = maxLimit.displayCurrencyString;
            NSString *complete = [NSString stringWithFormat:@"Amount cannot be greater than %@", amountStr];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!" message:complete delegate:nil
                                                  cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }else if (self.transaction.currentAmount < minLimit.floatValue) {
            
            NSString *amountStr = minLimit.displayCurrencyString;
            NSString *complete = [NSString stringWithFormat:@"Amount cannot be less than %@", amountStr];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!" message:complete delegate:nil
                                                  cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }else {
            
            [amountField resignFirstResponder];
        }
    }else {
        
        WLTGroupPaymentAmountEditViewController *editCont = [self controllerWithIdentifier:@"WLTGroupPaymentAmountEditViewController"];
        editCont.navBarColor = self.navBarColor;
        editCont.transaction = [self.transaction copy];
        
        __weak typeof(self) weakSelf = self;
        [editCont setCompletion:^(WLTPartial *pay){
            
            if (pay) {
                
                weakSelf.transaction = (WLTPartialPayment*)pay;
                [weakSelf changePaymentAmountCanShowAlert:YES];
            }
            
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }];
        
        [self.navigationController pushViewController:editCont animated:YES];
    }
}

-(void)refreshCardsList{
    
    __weak WLTMakePaymentListViewController *weakSelf = self;
    [self startLoadingWithColor:[UIColor appleGreenColor]];
    
    [[WLTInstrumentsManager sharedManager] validCardsForTransactionType:PaymentInstrumentTypePay
                                                        invalidateCache:NO withCompletion:^(id data) {
        
        if ([data isKindOfClass:[NSError class]]) {
            
            [weakSelf showAlertForError:data];
        }else {
            
            [weakSelf changeDisplayedDataForInstrumentsArray:data];
        }
        
        [weakSelf endViewLoading];
    }];
}

-(void)backClicked:(UIButton*)btn {
    
    if (self.completion) {
        
        GTLWalnutMPaymentInstrument *justAdded = nil;
        if (self.addedInstrumentIDs.count) {
            for ( GTLWalnutMPaymentInstrument *instr in self.allInstruments ) {
                if ([self.addedInstrumentIDs containsObject:instr.instrumentUuid]) {
                    justAdded = instr;
                    break;
                }
            }
        }
        
        if (justAdded) {
            self.completion(NO, nil, justAdded, YES);
        }else {
            self.completion(NO, nil, nil, NO);
        }
    }
}

-(void)changeDisplayedDataForInstrumentsArray:(NSArray*)instruments{
    
    self.allInstruments = [instruments sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"sendDefault" ascending:NO]]];
    [theScrollView addSubviewsForCards:self.allInstruments];
    
    if (justAddedCardID.length) {
        
        BOOL itsOkToUse = NO;
        for ( GTLWalnutMPaymentInstrument *instr in self.allInstruments ) {
            if ([instr.instrumentUuid isEqualToString:justAddedCardID]) {
                if (instr.enabledForMoneysend.boolValue) {
                    itsOkToUse = YES;
                    break;
                }
            }
        }
        
        if (!itsOkToUse) {
            
            __weak typeof(self) weakSelf = self;
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Sorry!" message:@"We cannot make paymemts from this card.Please try adding another VISA/MASTER Debit card." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"Add a debit card" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [weakSelf changeRespondersForCurrentSelection];
            }];
            [alert addAction:action];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            [entryView resetNewCardText];
            [theScrollView selectCardOfID:nil animated:NO];
        }else {
            
            [theScrollView selectCardOfID:justAddedCardID animated:NO];
        }
        
        justAddedCardID = nil;
        [self changeRespondersForCurrentSelection];
    }else if (self.allInstruments.count) {
        
        GTLWalnutMPaymentInstrument *instr = [self.allInstruments firstObject];
        [theScrollView selectCardOfID:instr.instrumentUuid animated:NO];
        [self changeRespondersForCurrentSelection];
    }
}

-(void)makeEntryViewFirstResponderForCurrentSelection {
    
    if (![amountField isFirstResponder]) {
        
        if (theScrollView.selectedCardView) {
            
            [entryView->cvvEntryView becomeFirstResponder];
        }else {
            
            [entryView->cardNumberField becomeFirstResponder];
        }
    }
}

#pragma mark Payments API

-(void)continueMakingPaymentsForTransaction:(GTLWalnutMPaymentTransaction*)trans
                              andInstrument:(GTLWalnutMPaymentInstrument*)instrument
                                    withCVV:(NSString*)cvv{
    
    [entryView resetEnteredText];
    
    __weak WLTMakePaymentListViewController *weakSelf = self;
    
    WLTMakePaymentViewController *cont = [self controllerWithIdentifier:@"WLTMakePaymentViewController"];
    cont.transaction = trans;
    cont.instrument = instrument;
    cont.verifyCode = cvv;
    cont.navBarColor = [UIColor appleGreenColor];
    
    [cont setCompletion:^(BOOL success, GTLWalnutMPaymentTransaction *paidTrans){
        if (success) {
            if (weakSelf.completion) {
                
                BOOL newInstr = NO;
                if ([weakSelf.addedInstrumentIDs containsObject:instrument.instrumentUuid]) {
                    newInstr = YES;
                }
                
                weakSelf.completion(YES, paidTrans, instrument, newInstr);
            }
        }else {
            
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
    
    [self.navigationController pushViewController:cont animated:YES];
}

-(void)initiateTransactionForInstrument:(GTLWalnutMPaymentInstrument *)instrument withCVV:(NSString*)cvv{
    
    WLTLoggedInUser *loggedInUser = [WLTDatabaseManager sharedManager].currentUser;
    
    NSString *loggedInUserMobile = loggedInUser.mobileString;
    NSString *otherUserMobile = [self.transaction.user completeNumber];
    
    long long dateStamp = [NSDate currentTimeStamp];
    
    GTLWalnutMPaymentTransaction *trans = [[GTLWalnutMPaymentTransaction alloc] init];
    trans.userMessage = self.userMessage;
    trans.completionDate = @(0);
    trans.amount = @(self.transaction.currentAmount).twoPlaceDecimalNumber;
    trans.fastFundEnabled = instrument.fastFundEnabled;
    trans.deviceUuid = [WLTDatabaseManager sharedManager].appSettings.deviceIdentifier;
    trans.initiationDate = @(dateStamp);
    trans.instrumentUuid = instrument.instrumentUuid;
    trans.senderLast4Digits = instrument.last4Digits;
    
    trans.receiverName = self.displayedUserName;
    trans.receiverPhone = otherUserMobile;
    trans.replyMessage = self.receiverMessage;
    
    trans.senderBank = instrument.bank;
    trans.senderName = loggedInUser.name;
    trans.senderPhone = loggedInUserMobile;
    
    trans.status = [WLTPaymentTransaction stringForStatus:PaymentStatusPullInitiated];
    trans.subStatus = @"unknown";
    trans.type = [WLTPaymentTransaction stringForPaymentType:PaymentTypePull];
    trans.transactionUuid = [WLTNetworkManager generateUniqueIdentifier];
    
    if (self.previousTransactionId.length) {
        trans.requestReferenceId = self.previousTransactionId;
    }
    
    trans.flags = @(0);
    
    GTLWalnutMPaymentTransactionGroup * (^ newPayGroupForPartialData)(WLTPartialGroup *data) = ^GTLWalnutMPaymentTransactionGroup * (WLTPartialGroup *data){
        
        GTLWalnutMPaymentTransactionGroup *newGrp = [[GTLWalnutMPaymentTransactionGroup alloc] init];
        newGrp.splitTransactionUuid = [WLTNetworkManager generateUniqueIdentifier];
        newGrp.groupUuid = data.group.groupUUID;
        newGrp.amount = @(data.currentAmount).twoPlaceDecimalNumber;
        
        return newGrp;
    };
    
    NSMutableArray *allGrps = [NSMutableArray new];
    for ( WLTPartialGroup *data in self.transaction.payGroups ) {
        
        if (data.currentAmount > 0) {
            
            GTLWalnutMPaymentTransactionGroup *newPayGrp = newPayGroupForPartialData(data);
            
            newPayGrp.senderPhone = loggedInUserMobile;
            newPayGrp.receiverPhone = otherUserMobile;
            
            [allGrps addObject:newPayGrp];
        }
    }
    for ( WLTPartialGroup *data in self.transaction.getGroups ) {
        
        GTLWalnutMPaymentTransactionGroup *newGetGrp = newPayGroupForPartialData(data);
        
        newGetGrp.senderPhone = otherUserMobile;
        newGetGrp.receiverPhone = loggedInUserMobile;
        
        [allGrps addObject:newGetGrp];
    }
    
    trans.groupDetails = allGrps;
    
    [self startLoadingWithColor:[UIColor appleGreenColor]];
    
    __weak WLTMakePaymentListViewController *weakSelf = self;
    
    GTLQueryWalnut *walN = [GTLQueryWalnut queryForPaymentTransactionAddWithObject:trans];
    [[WLTNetworkManager sharedManager].walnutService executeQuery:walN completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
        
        [weakSelf endViewLoading];
        
        if (error) {
            
            [weakSelf showAlertForError:error];
        }else {
            
            GTLWalnutMPaymentTransactions *allTransactions = (GTLWalnutMPaymentTransactions*)object;
            
            if ( [allTransactions isKindOfClass:[GTLWalnutMPaymentTransactions class]] && (allTransactions.transactions.count > 0) ) {
                
                GTLWalnutMPaymentTransaction *transNow = [allTransactions.transactions firstObject];
                transNow.groupDetails = allGrps;
                [weakSelf continueMakingPaymentsForTransaction:transNow andInstrument:instrument withCVV:cvv];
            }
            
            [weakSelf refreshCardsList];
            
            NSMutableArray *allChangedIds = [NSMutableArray new];
            for ( GTLWalnutMPaymentTransactionGroup *transGrp in trans.groupDetails) {
                [allChangedIds addObject:transGrp.groupUuid];
            }
            
            [[WLTGroupsManager sharedManager] fetchEveryChangeOfGroupsIDS:allChangedIds completion:^(NSArray *grpArr, NSError *error) {
                
                for ( WLTGroup *grpChanged in grpArr ) {
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:NotifyGroupChanged object:grpChanged];
                }
            }];
        }
    }];
}

#pragma mark - WLTCardsScrollViewDelegate

-(void)changeRespondersForCurrentSelection {
    
    if (![amountField isFirstResponder]) {
        
        [entryView resetEnteredText];
        if (theScrollView.selectedCardView) {
            
            [entryView showCVVEntryPercent:1 animated:YES];
            [entryView->cvvEntryView becomeFirstResponder];
        }
        else {
            
            [entryView showCVVEntryPercent:0 animated:YES];
            [entryView->cardNumberField becomeFirstResponder];
        }
    }
}

-(void)cardsScrollView:(WLTCardsScrollView *)scrollView selectedIndexChanged:(NSInteger)newIndex {
    
    [self changeRespondersForCurrentSelection];
}

#pragma mark - WLTCardEntryAccessoryViewDelegate

-(void)cardEntryView:(WLTCardEntryAccessoryView *)view changedCardNumber:(NSString *)cardNum andExpDate:(NSString *)expDate {
    
    if (cardNum.length) {
        
        NSString *emptyPlaceholder = [NSString emptyCardPlaceHolder];
        NSString *cardNumStr = cardNum.formattedCreditCardNumber;
        
        theScrollView.emptyCardView->cardNumberLabel.text = [emptyPlaceholder stringByReplacingCharactersInRange:NSMakeRange(0, cardNumStr.length) withString:cardNumStr];
    }else {
        
        theScrollView.emptyCardView->cardNumberLabel.text = [NSString emptyCardPlaceHolder];
    }
    
    if (expDate.length) {
        
        theScrollView.emptyCardView->cardExpDateLabel.text = [@"XX/XX" stringByReplacingCharactersInRange:NSMakeRange(0, expDate.length) withString:expDate];
    }else {
        
        theScrollView.emptyCardView->cardExpDateLabel.text = @"XX/XX";
    }
    
    NSString *withoutSeperators = cardNum.creditCardNumber;
    CreditCardBrand brand = [withoutSeperators cardBrand];
    
    if ( brand != lastDetectedBrand ) {
        
        lastDetectedBrand = brand;
        if ( (lastDetectedBrand == CreditCardBrandInvalid) || (lastDetectedBrand == CreditCardBrandUnknown)) {
            
            theScrollView.emptyCardView->cardTypeImageView.image = nil;;
        }else {
            
            NSString *imageName = [NSString iconForCardType:lastDetectedBrand];
            theScrollView.emptyCardView->cardTypeImageView.image = [UIImage imageNamed:imageName];
        }
    }
}

-(void)cardEntryView:(WLTCardEntryAccessoryView *)view addedNewInstrumentOfID:(NSString *)instrID {
    // Harshad: Show the alert here! (for case 1)
    justAddedCardID = instrID;
    [self.addedInstrumentIDs addObject:instrID];
    
    [self refreshCardsList];
}

-(void)continuePaymentFromIntrument:(GTLWalnutMPaymentInstrument*)instrument andCVV:(NSString*)cvvText {
    
    [self initiateTransactionForInstrument:instrument withCVV:cvvText];
}

-(void)cardEntryView:(WLTCardEntryAccessoryView *)view payClickedWithCVV:(NSString *)cvvString {
    
    if (theScrollView.selectedCardView) {
        [self initiateTransactionForInstrument:theScrollView.selectedCardView.instrument withCVV:cvvString];
    }
}

#pragma mark UITextFieldDelegate

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
    entryView.hidden = NO;
    [self changeRespondersForCurrentSelection];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    entryView.hidden = YES;
    return YES;
}

-(void)amoutValueEdited:(UITextField*)field {
    
    WLTPaymentConfig *config = [[WLTDatabaseManager sharedManager] paymentConfig];
    
    if (field.text.floatValue < config.p2pMinLimit.doubleValue) {
        
        amoutErrorView.titleLabel.text = [NSString stringWithFormat:@"Amount cannot be less than %@", config.p2pMinLimit.displayCurrencyString];
        amoutErrorView.hidden = NO;
    } else if (field.text.floatValue > config.p2pMaxLimit.doubleValue) {
        amoutErrorView.titleLabel.text = [NSString stringWithFormat:@"Amount cannot be greater than %@", config.p2pMaxLimit.displayCurrencyString];
        amoutErrorView.hidden = NO;
    } else {
        
        amoutErrorView.hidden = YES;
    }
    
    editButton.enabled = amoutErrorView.hidden;
    self.transaction.currentAmount = field.text.floatValue;
}

@end
