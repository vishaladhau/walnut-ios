//
//  WLTNewBankAccountViewController.m
//  walnut
//
//  Created by Abhinav Singh on 16/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTNewBankAccountViewController.h"
#import "WLTIFSCSearchViewController.h"
#import "WLTInstrumentsManager.h"
#import "WLTIFSCResultsTableViewCell.h"

typedef NS_ENUM(NSInteger, VerificationState) {
    
    VerificationStateNone = 0,
    VerificationStateVerified,
    VerificationStateNotVerified
};

@interface WLTNewBankAccountViewController () <UITextFieldDelegate> {
    
}

@property(nonatomic, strong) GTLWalnutMIfscSearchObject *ifscBranch;
@property(nonatomic, assign) VerificationState verificationState;

@end

@implementation WLTNewBankAccountViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"Add a bank account";
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone
                                                                 target:self action:@selector(doneClicked:)];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    [self setEdgesForExtendedLayout:UIRectEdgeTop];
    
    widthConstraint.constant = [UIScreen mainScreen].bounds.size.width;
    
    theScrollView.alwaysBounceVertical = YES;
    [theScrollView setContentInset:UIEdgeInsetsMake(84, 0, 0, 0)];
    theScrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    [self addBackBarButtonWithSelector:@selector(backClicked:)];
    
    errorLabel.text = @"Account numbers don't match!";
    errorLabel.font = [UIFont sfUITextRegularOfSize:10];
    errorLabel.textColor = [UIColor pomegranateColor];
    errorLabel.hidden = YES;
    
    void (^ addImageToTextField)(UIImage *image, UITextField *field) = ^void (UIImage *image, UITextField *field){
        
        CGFloat width = image.size.width;
        width += 10;
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
        imageView.contentMode = UIViewContentModeLeft;
        imageView.width = width;
        imageView.tintColor = [UIColor cadetGrayColor];
        field.leftView = imageView;
        
        field.leftViewMode = UITextFieldViewModeAlways;
    };
    
    {
        addImageToTextField([UIImage imageNamed:@"BankIcon"], bankAcnoTextField);
        
        bankAcnoBackView.backgroundColor = [UIColor whiteColor];
        [bankAcnoBackView addBorderLineAtPosition:(BorderLinePositionTop|BorderLinePositionBottom)];
        
        bankAcnoTextField.font = [UIFont sfUITextRegularOfSize:16];
        bankAcnoTextField.placeholder = @"Enter account number";
        [bankAcnoTextField setSecureTextEntry:YES];
        bankAcnoTextField.textColor = [UIColor darkGrayTextColor];
    }
    
    {
        addImageToTextField([UIImage imageNamed:@"BankIcon"], bankAcnoConfirmTextField);
        
        bankAcnoConfirmBackView.backgroundColor = [UIColor whiteColor];
        [bankAcnoConfirmBackView addBorderLineAtPosition:BorderLinePositionBottom];
        
        bankAcnoConfirmTextField.font = [UIFont sfUITextRegularOfSize:16];
        bankAcnoConfirmTextField.placeholder = @"Repeat account number";
        bankAcnoConfirmTextField.textColor = [UIColor darkGrayTextColor];
    }
    
    {
        addImageToTextField([UIImage imageNamed:@"ActionIcon"], ifscCodeTextField);
        
        ifscCodeBackView.backgroundColor = [UIColor whiteColor];
        [ifscCodeBackView addBorderLineAtPosition:(BorderLinePositionTop|BorderLinePositionBottom)];
        
        ifscCodeTextField.font = [UIFont sfUITextRegularOfSize:16];
        ifscCodeTextField.delegate = self;
        ifscCodeTextField.placeholder = @"Enter IFSC Number";
        ifscCodeTextField.textColor = [UIColor darkGrayTextColor];
        
        ifscCodeButton.tintColor = [UIColor bleuDeFranceColor];
    }
    
    {
        addImageToTextField([UIImage imageNamed:@"BankIcon"], bankNameTextField);
        
        bankNameBackView.backgroundColor = [UIColor whiteColor];
        [bankNameBackView addBorderLineAtPosition:(BorderLinePositionBottom)];
        
        bankNameTextField.font = [UIFont sfUITextRegularOfSize:16];
        bankNameTextField.placeholder = @"Enter bank name";
        bankNameTextField.textColor = [UIColor darkGrayTextColor];
        bankNameTextField.delegate = self;
        
        bankNameBackView.hidden = YES;
    }
    
    {
        [bankAddressBackView addBorderLineAtPosition:BorderLinePositionBottom];
        
        bankAddressLabel.font = [UIFont sfUITextRegularOfSize:14];
        bankAddressLabel.textColor = [UIColor cadetColor];
        [bankAddressLabel setPreferredMaxLayoutWidth:([UIScreen mainScreen].bounds.size.width-20)];
        
        bankAddressBackView.hidden = YES;
    }
    
    _verificationState = -1;
    self.verificationState = VerificationStateNone;
}

-(void)setVerificationState:(VerificationState)verificationState {
    
    if (_verificationState != verificationState) {
        
        _verificationState = verificationState;
        
        if (verificationState == VerificationStateNone) {
            
            [ifscCodeButton setTitle:@"LOOKUP" forState:UIControlStateNormal];
            [ifscCodeButton setImage:nil forState:UIControlStateNormal];
        }
        else if (verificationState == VerificationStateVerified) {
            
            [ifscCodeButton setTitle:nil forState:UIControlStateNormal];
            [ifscCodeButton setImage:[[UIImage imageNamed:@"Checkmark"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
        }else {
            
            [ifscCodeButton setTitle:@"VERIFY" forState:UIControlStateNormal];
            [ifscCodeButton setImage:nil forState:UIControlStateNormal];
        }
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

-(void)backClicked:(UIBarButtonItem*)item {
    
    if (self.completion) {
        self.completion(nil);
    }
}

-(IBAction)ifscLookUpClicked:(id)sender {
    
    if (self.verificationState == VerificationStateNone) {
        
        WLTIFSCSearchViewController *cont = [self controllerWithIdentifier:@"WLTIFSCSearchViewController"];
        [self.navigationController pushViewController:cont animated:YES];
        
        __weak typeof(self) weakSelf = self;
        [cont setCompletion:^(GTLWalnutMIfscSearchObject *object){
            
            weakSelf.ifscBranch = object;
            weakSelf.verificationState = VerificationStateVerified;
            [weakSelf changeIFSCValue];
            
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }];
    }else if (self.verificationState == VerificationStateNotVerified) {
        
        //Make Search API call here.
        [self verifyCurrentIFSCString];
    }
}

-(void)doneClicked:(UIBarButtonItem*)item {
    
    NSString *bankName = nil;
    NSString *ifscCode = nil;
    
    if (self.ifscBranch) {
        
        bankName = self.ifscBranch.bank;
        ifscCode = self.ifscBranch.ifsc;
    }else {
        
        ifscCode = ifscCodeTextField.text;
        if(!bankNameBackView.hidden) {
            bankName = bankNameTextField.text;
        }
    }
    
    NSError *validationError = nil;
    if (!bankAcnoTextField.text.length || ![bankAcnoTextField.text isEqualToString:bankAcnoConfirmTextField.text]) {
        
        validationError = [NSError walnutErrorWithMessage:@"Please enter a valid bank account number."];
    }else if (!ifscCode.length) {
        
        validationError = [NSError walnutErrorWithMessage:@"Please enter a valid IFSC code."];
    }
    else if (!bankName.length) {
        
        if(bankNameBackView.hidden) {
            validationError = [NSError walnutErrorWithMessage:@"Please Verify IFSC Code."];
        }else {
            validationError = [NSError walnutErrorWithMessage:@"Please enter a valid Bank name."];
        }
    }
    
    if (!validationError) {
        
        [self.view endEditing:YES];
        
        WLTLoggedInUser *user = [WLTDatabaseManager sharedManager].currentUser;
        WLTDatabaseManager *dbManager = [WLTDatabaseManager sharedManager];
        
        GTLWalnutMPaymentInstrument *instrument = [[GTLWalnutMPaymentInstrument alloc] init];
        
        instrument.bankAccountNumber = bankAcnoTextField.text;
        instrument.cardName = user.name;
        
        instrument.deviceUuid = dbManager.appSettings.deviceIdentifier;
        instrument.instrumentUuid = [WLTNetworkManager generateUniqueIdentifier];
        instrument.isActive = @(YES);
        instrument.cardTokenNumber = @"";
        instrument.bankIfsc = ifscCode;
        instrument.expiryYear = @(0);
        instrument.expiryMonth = @(0);
        instrument.bank = bankName;
        
        instrument.subtype = @"Bank";
        instrument.type = InstrumentTypeBank;
        instrument.flags = @(0);
//        instrument.fastFundEnabled = @(YES);
        
        __weak typeof(self) weakSelf = self;
        [self startLoadingWithColor:[UIColor appleGreenColor]];
        
        [[WLTInstrumentsManager sharedManager] addInstrumentToWalnut:instrument withCompletion:^(id data) {
            
            if ([data isKindOfClass:[NSError class]]) {
                
                [weakSelf showAlertForError:data];
            }else {
                
                GTLWalnutMPaymentInstrument *added = instrument;
                if ([data isKindOfClass:[NSArray class]]) {
                    
                    for ( GTLWalnutMPaymentInstrument *instr in data ) {
                        
                        if([instr.instrumentUuid isEqualToString:instrument.instrumentUuid]) {
                            added = instr;
                            break;
                        }
                    }
                }
                
                if (weakSelf.completion) {
                    weakSelf.completion(added);
                }
            }
            
            [weakSelf endViewLoading];
        }];
    }else {
        
        [self showAlertForError:validationError];
    }
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if(bankAcnoTextField.text.length == 0) {
        [bankAcnoTextField becomeFirstResponder];
    }
}

-(void)changeIFSCValue {
    
    if (self.ifscBranch) {
        
        bankNameBackView.hidden = YES;
        bankAddressBackView.hidden = NO;
        
        ifscCodeTextField.text = self.ifscBranch.ifsc;
        
        NSString *bankName = [self.ifscBranch.bank uppercaseString];
        NSString *complete = [NSString stringWithFormat:@"%@\n\n%@",bankName, self.ifscBranch.address];
        NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:complete];
        
        [attributed addAttributes:@{NSForegroundColorAttributeName:[UIColor denimColor], NSFontAttributeName:[UIFont sfUITextRegularOfSize:14]} range:[complete rangeOfString:bankName]];
        [attributed addAttributes:@{NSFontAttributeName:[UIFont sfUITextRegularOfSize:5]} range:NSMakeRange([complete rangeOfString:bankName].length, 2)];
        
        bankAddressLabel.attributedText = attributed;
        bankAddressBackViewHeight.constant = ([bankAddressLabel intrinsicContentSize].height + 20);
    }
    
    if (ifscCodeTextField.text.length) {
        ifscCodeTextField.leftView.tintColor = [UIColor bleuDeFranceColor];
    }else {
        ifscCodeTextField.leftView.tintColor = [UIColor cadetGrayColor];
    }
    
    if (bankNameTextField.text.length) {
        bankNameTextField.leftView.tintColor = [UIColor bleuDeFranceColor];
    }else {
        bankNameTextField.leftView.tintColor = [UIColor cadetGrayColor];
    }
}

-(void)checkBankAccount:(NSString*)bankAccout andConfirmNumber:(NSString*)confirmed {
    
    errorLabel.hidden = YES;
    if (confirmed.length && bankAccout.length) {
        
        NSRange range = [bankAccout rangeOfString:confirmed];
        if (range.location != 0) {
            
            errorLabel.hidden = NO;
            bankAcnoConfirmTextField.leftView.tintColor = [UIColor flamePeaColor];
            bankAcnoTextField.leftView.tintColor = [UIColor flamePeaColor];
        }else if (range.length == bankAccout.length){
            
            bankAcnoConfirmTextField.leftView.tintColor = [UIColor appleGreenColor];
            bankAcnoTextField.leftView.tintColor = [UIColor appleGreenColor];
        }else {
            
            bankAcnoTextField.leftView.tintColor = [UIColor bleuDeFranceColor];
            bankAcnoConfirmTextField.leftView.tintColor = [UIColor bleuDeFranceColor];
        }
    }else {
        
        if (bankAccout.length) {
            bankAcnoTextField.leftView.tintColor = [UIColor bleuDeFranceColor];
        }else {
            bankAcnoTextField.leftView.tintColor = [UIColor cadetGrayColor];
        }
        
        if (confirmed.length) {
            bankAcnoConfirmTextField.leftView.tintColor = [UIColor bleuDeFranceColor];
        }else {
            bankAcnoConfirmTextField.leftView.tintColor = [UIColor cadetGrayColor];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL retValue = YES;
    if ( (textField == bankAcnoConfirmTextField) || (textField == bankAcnoConfirmTextField)) {
        
        NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if (newText.length) {
            textField.leftView.tintColor = [UIColor bleuDeFranceColor];
        }else {
            textField.leftView.tintColor = [UIColor cadetGrayColor];
        }
        
        if (textField == bankAcnoConfirmTextField) {
            
            [self checkBankAccount:bankAcnoTextField.text andConfirmNumber:newText];
        }
        else if (textField == bankAcnoTextField) {
            
            [self checkBankAccount:newText andConfirmNumber:bankAcnoConfirmTextField.text];
        }
    }
    else if (textField == ifscCodeTextField) {
        
        NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        ifscCodeTextField.text = [newText uppercaseString];
        
        self.ifscBranch = nil;
        if (!newText.length) {
            self.verificationState = VerificationStateNone;
        }else {
            self.verificationState = VerificationStateNotVerified;
        }
        
        if (newText.length) {
            ifscCodeTextField.leftView.tintColor = [UIColor bleuDeFranceColor];
        }else {
            ifscCodeTextField.leftView.tintColor = [UIColor cadetGrayColor];
        }
        
        retValue = NO;
    }
    else if (textField == bankNameTextField) {
        NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if (newText.length) {
            bankNameTextField.leftView.tintColor = [UIColor bleuDeFranceColor];
        }else {
            bankNameTextField.leftView.tintColor = [UIColor cadetGrayColor];
        }
    }
    
    return retValue;
}

-(void)verifyCurrentIFSCString{
    
    NSString *codeToSearch = ifscCodeTextField.text;
    if (codeToSearch.length) {
        
        GTLWalnutMIfscSearch *search = [[GTLWalnutMIfscSearch alloc] init];
        search.searchString = codeToSearch;
        
        GTLQueryWalnut *query = [GTLQueryWalnut queryForSearchIfscWithObject:search];
        
        __weak typeof(self) weakSelf = self;
        [self startLoading];
        [[WLTNetworkManager sharedManager].walnutService executeQuery:query completionHandler:^(GTLServiceTicket *ticket, GTLWalnutMIFSCSearchResults *object, NSError *error) {
            
            BOOL verified = NO;
            if (!error) {
                
                if (object.results.count == 1) {
                    
                    GTLWalnutMIfscSearchObject *objectSearch = [object.results firstObject];
                    if ([objectSearch.ifsc compare:codeToSearch options:NSCaseInsensitiveSearch] == NSOrderedSame) {
                        
                        weakSelf.ifscBranch = [object.results firstObject];
                        weakSelf.verificationState = VerificationStateVerified;
                        
                        [weakSelf changeIFSCValue];
                        verified = YES;
                    }
                }
            }
            
            if (!verified) {
                
                if (!error) {
                    error = [NSError walnutErrorWithMessage:@"We couldn't verify this IFSC code."];
                }
                
                bankNameBackView.hidden = NO;
                bankAddressBackView.hidden = YES;
                
                [weakSelf showAlertForError:error];
            }
            
            [weakSelf endViewLoading];
        }];
    }
}

@end
