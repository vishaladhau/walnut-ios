//
//  WLTPaymentTransactionsViewController.h
//  walnut
//
//  Created by Abhinav Singh on 25/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"

@interface WLTPaymentTransactionsViewController : WLTViewController {
    
    UITabBarItem *tabItem;
}

@end
