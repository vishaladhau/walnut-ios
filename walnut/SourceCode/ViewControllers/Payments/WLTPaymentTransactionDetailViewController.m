//
//  WLTPaymentTransactionDetailViewController.m
//  walnut
//
//  Created by Abhinav Singh on 23/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTPaymentTransactionDetailViewController.h"
#import "WLTContactsManager.h"
#import "WLTPaymentTransaction.h"
#import "WLTPaymentUser.h"
#import "WLTPaymentTransactionsTableViewCell.h"
#import "WLTServerCommentTableViewCell.h"
#import "WLTTableHeaderView.h"
#import "WLTKeyValueTableViewCell.h"
#import "WLTPaymentGroup.h"
#import "WLTPaymentInfoTableViewCell.h"
#import "WLTCardsListViewController.h"
#import "WLTPaymentTransationsManager.h"
#import "WLTRootViewController.h"
#import "WLTFirstPaymentAlertView.h"
#import "WLTGroup.h"
#import "WLTShareActivityProvider.h"

typedef NS_ENUM(NSInteger, ControllerSection) {
    
    ControllerSectionUnknown,
    ControllerSectionInfo,
    ControllerSectionGroups,
    ControllerSectionShare,
    ControllerSectionHelp,
};

typedef NS_ENUM(NSInteger, SharingOption) {
    
    SharingOptionNone = 0,
    SharingOptionAll,
    SharingOptionRemindTextAll,
    SharingOptionRemindOnlyWhatsApp,
    SharingOptionRemindOnlyOther,
};

@interface WLTPaymentTransactionDetailViewController () <UITableViewDelegate, UITableViewDataSource> {
    
    NSString *otherUserName;
}

@property(nonatomic, assign) SharingOption sharingOption;

-(void)loadCardsListViewController;

@end

@implementation WLTPaymentTransactionDetailViewController

- (instancetype)initWithCoder:(NSCoder *)coder {
    
    self = [super initWithCoder:coder];
    if (self) {
        
        _sharingOption = SharingOptionNone;
    }
    
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    __weak typeof(self) weakSelf = self;
    
    WLTPaymentUser *otherUser = nil;
    if (self.transaction.isPaid) {
        otherUser = self.transaction.receiver;
    }else {
        otherUser = self.transaction.sender;
    }
    
    otherUserName = [otherUser name];
    
    [[WLTContactsManager sharedManager] displayNameForUser:otherUser defaultIsMob:YES completion:^(NSString *data) {
        
        if (weakSelf) {
            
            if (weakSelf.transaction.isPaid) {
                [weakSelf setTitle:[NSString stringWithFormat:@"You paid %@", data]];
            }else {
                [weakSelf setTitle:[NSString stringWithFormat:@"%@ paid You", data]];
            }
            
            otherUserName = data;
        }
    }];
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectZero];
    theTableView.tableFooterView = footer;
    theTableView.separatorInset = UIEdgeInsetsZero;
    theTableView.rowHeight = UITableViewAutomaticDimension;
    theTableView.backgroundColor = self.view.backgroundColor;
    [theTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"DefaultTableCell"];
    [theTableView registerNib:[UINib nibWithNibName:@"WLTPaymentTransactionsTableViewCell" bundle:nil] forCellReuseIdentifier:@"WLTPaymentTransactionsTableViewCell"];
    [theTableView registerNib:[UINib nibWithNibName:@"WLTKeyValueTableViewCell" bundle:nil] forCellReuseIdentifier:@"WLTKeyValueTableViewCell"];
    [theTableView registerNib:[UINib nibWithNibName:@"WLTPaymentInfoTableViewCell" bundle:nil] forCellReuseIdentifier:@"WLTPaymentInfoTableViewCell"];
    
    if ( self.presentingViewController && [[self.navigationController rootViewController] isEqual:self] ) {
        
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                       style:UIBarButtonItemStyleDone
                                                                      target:self action:@selector(doneClicked:)];
        self.navigationItem.rightBarButtonItem = doneButton;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paymentsChanged:) name:PaymentTransactionsChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unreadBadgeCountChanged:) name:PaymentTransactionsCountChanged object:nil];
    
    [self reloadSharingOptions];
}

-(void)reloadSharingOptions {
    
    _sharingOption = SharingOptionNone;
    if (self.transaction.status == PaymentStatusPushSuccess) {
        
        _sharingOption = SharingOptionAll;
    }else if ( (self.transaction.status == PaymentStatusPushPending) && self.transaction.isPaid) {
        
        switch (self.transaction.subStatus) {
            case PaymentSubStatusReceiverNotHaveDefaultInstrument:
            case PaymentSubStatusReceiverInstrumentUUIDNotFound:
            case PaymentSubStatusReceiverNotOnWalnut: {
                
                NSString *whatsAppUrlString = [NSString stringWithFormat:@"whatsapp://send?text=12"];
                NSString *newString = [whatsAppUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURL *whatsAppUrl = [NSURL URLWithString:newString];
                
                BOOL canSendToWhatsApp = [[UIApplication sharedApplication] canOpenURL:whatsAppUrl];
                BOOL canSendToOthers = [MFMessageComposeViewController canSendText];
                if (!canSendToOthers) {
                    canSendToOthers = [MFMailComposeViewController canSendMail];
                }
                
                if (canSendToWhatsApp && canSendToOthers) {
                    
                    _sharingOption = SharingOptionRemindTextAll;
                }else if (canSendToWhatsApp) {
                    
                    _sharingOption = SharingOptionRemindOnlyWhatsApp;
                }else if (canSendToOthers) {
                    
                    _sharingOption = SharingOptionRemindOnlyOther;
                }
            }
            break;
            default:
                break;
        }
    }
}

-(void)unreadBadgeCountChanged:(NSNotification*)notify {
    
    if([self.navigationController.topViewController isEqual:self]) {
        
        [[WLTPaymentTransationsManager sharedManager] markTransactionIDAsViewed:self.transaction.transactionId];
    }
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[WLTPaymentTransationsManager sharedManager] markTransactionIDAsViewed:self.transaction.transactionId];
    
    [self refreshCurrentTransaction];
    
    if (self.transaction.groups.count) {
        
        NSArray *allIds = [self.transaction.groups valueForKeyPath:@"groupUUID"];
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"groupUUID IN %@", allIds];
        NSArray *allSavedGrps = [[WLTDatabaseManager sharedManager] allObjectsOfClass:@"WLTGroup" withPredicate:pred];
        
        NSMutableDictionary *nameMapping = [NSMutableDictionary new];
        for ( WLTGroup *grpOrig in allSavedGrps ) {
            nameMapping[grpOrig.groupUUID] = grpOrig.dName;
        }
        
        for ( WLTPaymentGroup *grp in self.transaction.groups ) {
            NSString *newName = nameMapping[grp.groupUUID];
            if (newName) {
                grp.name = newName;
            }
        }
        
        [[WLTDatabaseManager sharedManager] saveDataBase];
        [theTableView reloadData];
    }
}

-(void)shareActoinButtonClicked:(UIButton*)btn {
    
    NSString *whatsAppUrlString = [NSString stringWithFormat:@"whatsapp://send?text=%@", [WLTShareActivityProvider shareTextForActivityType:WLTShareActivityTypeTransactionRemind]];
    NSString *newString = [whatsAppUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *whatsAppUrl = [NSURL URLWithString:newString];
    
    if ([[UIApplication sharedApplication] canOpenURL:whatsAppUrl]) {
        [[UIApplication sharedApplication] openURL:whatsAppUrl];
    }
}

-(void)doneClicked:(UIBarButtonItem*)item {
    
    if (self.completion) {
        self.completion();
    }
}

-(ControllerSection)sectionTypeForIndex:(NSInteger)section {
  
    ControllerSection retSection = ControllerSectionUnknown;
    if (section == 0) {
        retSection = ControllerSectionInfo;
    }
    else if (section == 1) {
        
        if (self.sharingOption != SharingOptionNone) {
            
            retSection = ControllerSectionShare;
        }
        else if (self.transaction.groups.count) {
            
            retSection = ControllerSectionGroups;
        }
        else {
            
            retSection = ControllerSectionHelp;
        }
    }
    else if (section == 2) {
        
        if (self.sharingOption != SharingOptionNone) {
            
            if (self.transaction.groups.count) {
                
                retSection = ControllerSectionGroups;
            }else {
                
                retSection = ControllerSectionHelp;
            }
        }else {
            retSection = ControllerSectionHelp;
        }
    }
    else {
        
        retSection = ControllerSectionHelp;
    }
    
    return retSection;
}

-(void)addCardClicked:(LSButton*)button {
    
    if ([WLTFirstPaymentAlertView canShow]) {
        
        __weak typeof(self) weakSelf = self;
        WLTFirstPaymentAlertView *alert = [WLTFirstPaymentAlertView initWithDefaultXib];
        [alert setProceedBlock:^(BOOL success){
            
            if (success) {
                
                [weakSelf loadCardsListViewController];
            }
        }];
        [alert show];
    }else {
        
        [self loadCardsListViewController];
    }
}

- (void)loadCardsListViewController {
    
    WLTCardsListViewController *instruments = [self controllerWithIdentifier:@"WLTCardsListViewController"];
    instruments.flowType = WLTEntryFlowTypeReceive;
    instruments.showReceiveAlert = YES;
    instruments.transactionIDToShow = self.transaction.transactionId;
    instruments.navBarColor = self.navBarColor;
    [self.navigationController pushViewController:instruments animated:YES];
}

-(void)refreshCurrentTransaction {
    
    __weak typeof(self) weakSelf = self;
    [self startLoading];
    [[WLTPaymentTransationsManager sharedManager] fetchAllPaymentTransactionsCompletion:^(BOOL success, NSError *error) {
        [weakSelf endViewLoading];
    }];
}

#pragma mark - Payments Changed

-(void)paymentsChanged:(NSNotification*)notify {
    
    [self reloadSharingOptions];
    [theTableView reloadData];
}

#pragma mark - UITableViewDelegate & UITableViewDatasource

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ControllerSection retSection = [self sectionTypeForIndex:indexPath.section];
    
    UITableViewCell *tcell = nil;
    if (retSection == ControllerSectionInfo) {
        if (indexPath.row == 0) {
            
            WLTPaymentTransactionsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WLTPaymentTransactionsTableViewCell"];
            [cell showDetailsOfTransaction:self.transaction];
            
            tcell = cell;
        }else if (indexPath.row == 1) {
            
            WLTPaymentInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WLTPaymentInfoTableViewCell"];
            [cell->addCardButton addTarget:self action:@selector(addCardClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell showInfoOfTransaction:self.transaction];
            
            tcell = cell;
        }
    }
    else if (retSection == ControllerSectionHelp) {
        
        tcell = [theTableView dequeueReusableCellWithIdentifier:@"DefaultTableCell"];
        tcell.textLabel.font = [UIFont sfUITextRegularOfSize:16];
        tcell.textLabel.textColor = [UIColor cadetColor];
        
        tcell.textLabel.text = @"Email Us";
        tcell.imageView.image = nil;
    }
    else if (retSection == ControllerSectionShare) {
        
        tcell =  [theTableView dequeueReusableCellWithIdentifier:@"ShareTableCell"];
        if (!tcell) {
            
            UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ShareTableCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.textColor = [UIColor cadetColor];
            cell.textLabel.font = [UIFont sfUITextRegularOfSize:16];
            
            UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
            actionButton.tag = 111;
            actionButton.translatesAutoresizingMaskIntoConstraints = NO;
            [actionButton setImage:[UIImage imageNamed:@"WhatsappIcon"] forState:UIControlStateNormal];
            [actionButton addTarget:self action:@selector(shareActoinButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:actionButton];
            
            UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TextShareIcon"]];
            imgView.contentMode = UIViewContentModeCenter;
            imgView.tag = 112;
            imgView.translatesAutoresizingMaskIntoConstraints = NO;
            [cell.contentView addSubview:imgView];
            
            NSDictionary *dict = NSDictionaryOfVariableBindings(imgView, actionButton);
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[imgView(==45)]-5-|" options:0 metrics:nil views:dict]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[actionButton(==55)]-0-[imgView]-10-|" options:0 metrics:nil views:dict]];
            
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[actionButton]-0-|" options:0 metrics:nil views:dict]];
            
            tcell = cell;
        }
        
        UIButton *whatsAppButton = [tcell.contentView viewWithTag:111];
        
        if (self.sharingOption == SharingOptionAll) {
            
            whatsAppButton.hidden = YES;
            tcell.textLabel.text = @"Share link to Walnut";
        }else if (self.sharingOption != SharingOptionNone) {
            
            if ( (self.sharingOption == SharingOptionRemindTextAll) || (self.sharingOption == SharingOptionRemindOnlyWhatsApp)) {
                whatsAppButton.hidden = NO;
            }else {
                whatsAppButton.hidden = YES;
            }
            
            tcell.textLabel.text = [NSString stringWithFormat:@"Tell %@ to accept", otherUserName];
        }
    }
    else if (retSection == ControllerSectionGroups) {
        
        WLTKeyValueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WLTKeyValueTableViewCell"];
        cell->keyLabel.font = [UIFont sfUITextRegularOfSize:16];
        cell->keyLabel.textColor = [UIColor cadetColor];
        
        cell->valueLabel.font = [UIFont sfUITextRegularOfSize:18];
        
        WLTPaymentGroup *grp = self.transaction.groups.allObjects[indexPath.row];
        if (grp.isPaidAmount.boolValue) {
            cell->valueLabel.textColor = [UIColor oweColor];
        }else {
            cell->valueLabel.textColor = [UIColor getColor];
        }
		
		NSString *groupName = grp.name;
		if (!groupName) {
			groupName = @"Unknown Group";
		}
        [cell setKey:groupName forValue:grp.amount.displayCurrencyString];
        
        tcell = cell;
    }
    else {
        
        tcell =  [theTableView dequeueReusableCellWithIdentifier:@"DefaultTableCell"];
        tcell.textLabel.font = [UIFont sfUITextRegularOfSize:16];
        tcell.textLabel.textColor = [UIColor cadetColor];
        
        if (indexPath.row == 0) {
            tcell.textLabel.text = @"Received by WalnutPay";
            tcell.imageView.image = [UIImage imageNamed:@"StatusIconSent"];
        }
        else if (indexPath.row == 1) {
            
            if (self.transaction.status == PaymentStatusPushSuccess) {
                
                tcell.imageView.image = [UIImage imageNamed:@"StatusIconReceived"];
                
                WLTPaymentUser *otherUser = nil;
                if (self.transaction.isPaid) {
                    otherUser = self.transaction.receiver;
                }else {
                    otherUser = self.transaction.sender;
                }
                __weak typeof(self) weakSelf = self;
                [[WLTContactsManager sharedManager] displayNameForUser:otherUser defaultIsMob:YES completion:^(NSString *data) {
                    
                    if (weakSelf) {
                        
                        tcell.textLabel.text = [NSString stringWithFormat:@"Received by %@", data];
                    }
                }];
            }else if (self.transaction.status == PaymentStatusPushReversed) {
                
                tcell.imageView.image = [UIImage imageNamed:@"RevertPaymentIcon"];
                tcell.textLabel.text = @"Payment Revert Initiated";
            }
            else {
                
                tcell.imageView.image = [UIImage imageNamed:@"ErrorIcon"];
                tcell.textLabel.text = @"Payment Failed";
            }
        }
    }
    
    return tcell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger sectionCnt = 2;
    if (self.transaction.groups.count) {
        sectionCnt += 1;
    }
    if (self.sharingOption != SharingOptionNone) {
        sectionCnt += 1;
    }
    
    return sectionCnt;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger rowCnt = 0;
    ControllerSection retSection = [self sectionTypeForIndex:section];
    
    if (retSection == ControllerSectionInfo) {
        
        rowCnt = 1;
        if (self.transaction.statuMessage.length) {
            rowCnt += 1;
        }
    }
    else if (retSection == ControllerSectionHelp) {
        
        rowCnt = 1;
    }
    else if (retSection == ControllerSectionShare) {
        
        rowCnt = 1;
    }
    else if (retSection == ControllerSectionGroups) {
        
        rowCnt = self.transaction.groups.count;
    }
    
    return rowCnt;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 90;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    CGFloat height = 0.1;
    ControllerSection retSection = [self sectionTypeForIndex:section];
    if (retSection != ControllerSectionInfo) {
        height = 50;
    }
    
    return height;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *vHeader = nil;
    ControllerSection retSection = [self sectionTypeForIndex:section];
    if (retSection == ControllerSectionHelp) {
        
        WLTTableHeaderView *header = [[WLTTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
        header.label.text = @"NEED HELP?";
        
        vHeader = header;
    }
    else if (retSection == ControllerSectionGroups) {
        
        WLTTableHeaderView *header = [[WLTTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
        header.label.text = @"GROUP SETTLEMENTS";
        
        vHeader = header;
    }
    else if (retSection == ControllerSectionShare) {
        
        WLTTableHeaderView *header = [[WLTTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
        if (self.sharingOption == SharingOptionAll) {
            header.label.text = @"ENJOY USING WALNUT?";
        }
        else {
            header.label.text = @"";
        }
        vHeader = header;
    }
    return vHeader;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    ControllerSection retSection = [self sectionTypeForIndex:indexPath.section];
    if (retSection == ControllerSectionHelp) {
        
        [self sendSupportEmailForTransaction:self.transaction];
    }
    else if (retSection == ControllerSectionShare) {

        if (self.sharingOption == SharingOptionAll) {
            
            WLTShareActivityType type = WLTShareActivityTypeTransactionReceived;
            if ([self.transaction isPaid]) {
                type = WLTShareActivityTypeTransactionPaid;
            }
            
            NSArray *sharingItems = [WLTSharingHelper activityItemsForType:type];
            UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
            [activityController setExcludedActivityTypes:@[UIActivityTypeAssignToContact, UIActivityTypeCopyToPasteboard, UIActivityTypePrint,  UIActivityTypeSaveToCameraRoll, UIActivityTypePostToWeibo]];
            
            [activityController setCompletionWithItemsHandler:^(NSString * __nullable activityType, BOOL completed, NSArray * __nullable returnedItems, NSError * __nullable activityError){
                
            }];
            
            [self presentViewController:activityController animated:YES completion:nil];
        }else {
            
            NSArray *sharingItems = [WLTSharingHelper activityItemsForType:WLTShareActivityTypeTransactionRemind];
            UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
            [activityController setExcludedActivityTypes:@[UIActivityTypeAssignToContact, UIActivityTypeCopyToPasteboard, UIActivityTypePrint,  UIActivityTypeSaveToCameraRoll, UIActivityTypePostToWeibo]];
            
            [activityController setCompletionWithItemsHandler:^(NSString * __nullable activityType, BOOL completed, NSArray * __nullable returnedItems, NSError * __nullable activityError){
                
            }];
            
            [self presentViewController:activityController animated:YES completion:nil];
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
