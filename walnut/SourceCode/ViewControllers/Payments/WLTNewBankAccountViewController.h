//
//  WLTNewBankAccountViewController.h
//  walnut
//
//  Created by Abhinav Singh on 16/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"

typedef void (^ BankAddBlock)(GTLWalnutMPaymentInstrument *instrument);

@interface WLTNewBankAccountViewController : WLTViewController {
    
    __weak IBOutlet UILabel *errorLabel;
    
    __weak IBOutlet UIView *bankAcnoBackView;
    __weak IBOutlet UITextField *bankAcnoTextField;
    
    __weak IBOutlet UIView *bankAcnoConfirmBackView;
    __weak IBOutlet UITextField *bankAcnoConfirmTextField;
    
    __weak IBOutlet UIView *ifscCodeBackView;
    __weak IBOutlet UITextField *ifscCodeTextField;
    __weak IBOutlet UIButton *ifscCodeButton;
    
    __weak IBOutlet UIScrollView *theScrollView;
    __weak IBOutlet NSLayoutConstraint *widthConstraint;
    
    __weak IBOutlet UIView *bankNameBackView;
    __weak IBOutlet UITextField *bankNameTextField;
    
    __weak IBOutlet NSLayoutConstraint *bankAddressBackViewHeight;
    __weak IBOutlet UIView *bankAddressBackView;
    __weak IBOutlet UILabel *bankAddressLabel;
}

@property(nonatomic, strong) BankAddBlock completion;
@property (nonatomic, assign) BOOL showReceiveAlert;

@end
