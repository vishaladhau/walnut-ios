//
//  WLTIFSCSearchViewController.m
//  walnut
//
//  Created by Abhinav Singh on 17/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTIFSCSearchViewController.h"
#import "WLTSearchOperation.h"
#import "WLTIFSCResultsTableViewCell.h"
#import "WLTEmptyStateView.h"

static NSInteger MinimimSearchLength = 4;

@interface WLTIFSCSearchViewController () <UISearchBarDelegate, WLTEmptyStateViewDelegate>{
    
    NSInteger lastStringCount;
    GTLServiceTicket *currentRequest;
    
    WLTEmptyStateView *emptyStateView;
}

@property(nonatomic, strong) NSString *searchString;

@property(nonatomic, strong) NSMutableDictionary *mappingDictionary;
@property(nonatomic, strong) NSArray *bankNamesArray;
@property(nonatomic, strong) NSArray *bankNameIndexes;

@end

@implementation WLTIFSCSearchViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"Lookup your branch";
    
    self.mappingDictionary = [NSMutableDictionary new];
    self.bankNamesArray = [NSMutableArray new];
    
    [theSearchBar setPlaceholder:@"Search by bank city branch"];
    
    [self setEdgesForExtendedLayout:UIRectEdgeTop];
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectZero];
    theTableView.tableFooterView = footer;
    theTableView.separatorInset = UIEdgeInsetsZero;
    theTableView.rowHeight = UITableViewAutomaticDimension;
    theTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    [theTableView registerNib:[UINib nibWithNibName:@"WLTIFSCResultsTableViewCell" bundle:nil] forCellReuseIdentifier:@"WLTIFSCResultsTableViewCell"];
    
    WLTEmptyStateView *emptyView = [WLTEmptyStateView emptyStateViewWithStyle:WLTEmptyStateViewStyleDefault andDelegate:self];
    emptyView.userInteractionEnabled = NO;
    emptyView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:emptyView];
    
    emptyStateView = emptyView;
    [self changeErrorViewStatus];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(emptyView);
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[emptyView]-20-|" options:0 metrics:nil views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[emptyView]-0-|" options:0 metrics:nil views:dict]];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if (!self.mappingDictionary.count) {
        [theSearchBar becomeFirstResponder];
    }
}

-(void)changeErrorViewStatus {
    
    if ([self isLoading]) {
        emptyStateView.hidden = YES;
    }
    
    else if (!self.bankNamesArray.count) {
        
        NSMutableAttributedString *attributed = nil;
        if (self.searchString.length) {
            
            NSString *complete = [NSString stringWithFormat:@"Sorry!\nWe can't find any banks matching %@", self.searchString];
            attributed = [[NSMutableAttributedString alloc] initWithString:complete attributes:@{NSForegroundColorAttributeName:[UIColor darkGrayTextColor], NSFontAttributeName:[UIFont sfUITextRegularOfSize:14]}];
            [attributed addAttributes:@{NSFontAttributeName:[UIFont sfUITextMediumOfSize:14]} range:[complete rangeOfString:self.searchString]];
        }else {
            
            NSString *complete = @"Search by bank city branch\ne.g. Axis Pune Camp";
            attributed = [[NSMutableAttributedString alloc] initWithString:complete attributes:@{NSForegroundColorAttributeName:[UIColor darkGrayTextColor], NSFontAttributeName:[UIFont sfUITextRegularOfSize:14]}];
        }
        
        [emptyStateView showAttributedString:attributed];
        emptyStateView.hidden = NO;
    }else {
        
        emptyStateView.hidden = YES;
    }
}

#pragma mark - UITableViewDelegate & UITableViewDataSource

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *dataArray = self.mappingDictionary[self.bankNamesArray[indexPath.section]];
    GTLWalnutMIfscSearchObject *search = dataArray[indexPath.row];
    if (self.completion) {
        self.completion(search);
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIndentifier = @"WLTIFSCResultsTableViewCell";
    WLTIFSCResultsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    
    NSArray *dataArray = self.mappingDictionary[self.bankNamesArray[indexPath.section]];
    GTLWalnutMIfscSearchObject *search = dataArray[indexPath.row];
    [cell showDetailsOfSearchObject:search searchString:self.searchString];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 70;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray *dataArray = self.mappingDictionary[self.bankNamesArray[section]];
    return dataArray.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.bankNamesArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 35;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40)];
    header.backgroundColor = self.view.backgroundColor;
    
    NSString *bankName = self.bankNamesArray[section];
    NSArray *branches = self.mappingDictionary[bankName];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.adjustsFontSizeToFitWidth = YES;
    label.minimumScaleFactor = 0.4;
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.numberOfLines = 1;
    label.font = [UIFont sfUITextMediumOfSize:12];
    label.textColor = [UIColor darkGrayTextColor];
    label.text = [NSString stringWithFormat:@"%@ (%@)", bankName, @(branches.count).displayString];
    [header addSubview:label];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(label);
    [header addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[label]-0-|" options:0 metrics:nil views:dict]];
    [header addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[label]-5-|" options:0 metrics:nil views:dict]];
    
    return header;
}

#pragma mark - Search

-(void)downloadResultForQuery:(NSString*)queryString {
    
    GTLWalnutMIfscSearch *search = [[GTLWalnutMIfscSearch alloc] init];
    search.searchString = queryString;
    
    GTLQueryWalnut *query = [GTLQueryWalnut queryForSearchIfscWithObject:search];
    
    __weak WLTIFSCSearchViewController *weakSelf = self;
    if (currentRequest) {
        [currentRequest cancelTicket];
    }
    
    [self startLoading];
    [self changeErrorViewStatus];
    
    currentRequest = [[WLTNetworkManager sharedManager].walnutService executeQuery:query completionHandler:^(GTLServiceTicket *ticket, GTLWalnutMIFSCSearchResults *object, NSError *error) {
        
        if ([weakSelf.searchString isEqualToString:queryString]) {
            
            if (!error) {
                
                [weakSelf.mappingDictionary removeAllObjects];
                
                NSMutableArray *indexes = [NSMutableArray new];
                
                for ( GTLWalnutMIfscSearchObject *ifscBankBranch in object.results ) {
                    
                    NSString *bankName = ifscBankBranch.bank;
                    if (bankName.length) {
                        
                        NSMutableArray *branchs = weakSelf.mappingDictionary[bankName];
                        if (!branchs) {
                            
                            branchs = [NSMutableArray new];
                            weakSelf.mappingDictionary[bankName] = branchs;
                            
                            NSString *firstLetter = [[bankName substringToIndex:1] uppercaseString];
                            if (![indexes containsObject:firstLetter]) {
                                [indexes addObject:firstLetter];
                            }
                        }
                        
                        [branchs addObject:ifscBankBranch];
                    }
                }
                
                [indexes sortUsingSelector:@selector(compare:)];
                weakSelf.bankNamesArray = [[weakSelf.mappingDictionary allKeys] sortedArrayUsingSelector:@selector(compare:)];
                weakSelf.bankNameIndexes = indexes;
            }
            
            [theTableView reloadData];
            [weakSelf endViewLoading];
            [weakSelf changeErrorViewStatus];
        }
    }];
}

-(void)searchTextChanged {
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(searchTextChanged) object:nil];
    if (self.searchString.length) {
        [self downloadResultForQuery:self.searchString];
    }
}

-(void)resetSearch {
    
    lastStringCount = 0;
    self.searchString = nil;
    
//    [theSearchBar resignFirstResponder];
    theSearchBar.text = nil;
    
    [self.mappingDictionary removeAllObjects];
    self.bankNamesArray = nil;
    
    [theTableView reloadData];
    
    if (currentRequest) {
        [currentRequest cancelTicket];
        currentRequest = nil;
    }
    
    [self endViewLoading];
    
    [self changeErrorViewStatus];
}

#pragma mark - SearchBar

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [theSearchBar resignFirstResponder];
    NSString *searchText = searchBar.text;
    
    if (!searchText.length) {
        
        [self resetSearch];
    }else if (![searchText isEqualToString:self.searchString]) {
        
        self.searchString = searchText;
        [self searchTextChanged];
    }
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    NSString *newText = searchBar.text;
    if (!newText.length) {
        [self resetSearch];
    }
//    else if (newText.length < lastStringCount) {
//        //Do nothing
//    }
//    else if (newText.length >= MinimimSearchLength) {
//        
//        self.searchString = newText;
//        
//        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(searchTextChanged) object:nil];
//        [self performSelector:@selector(searchTextChanged) withObject:nil afterDelay:0.3];
//    }
    
//    lastStringCount = newText.length;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [theSearchBar resignFirstResponder];
}

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}

-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    
    [searchBar setShowsCancelButton:NO animated:YES];
    return YES;
}

#pragma mark - Empty State View

-(void)emptyStateViewActionButtonTapped:(WLTEmptyStateView *)view {
    
}

-(void)emptyStateViewReloadScreen:(WLTEmptyStateView *)view {
    
    if (self.searchString.length) {
        
        [self downloadResultForQuery:self.searchString];
    }
}

@end
