//
//  WLTPaymentEditorKeyboardAccessoryView.h
//  walnut
//
//  Created by Harshad on 20/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLTPaymentEditorKeyboardAccessoryView : UIView

+ (instancetype)instantiateAccessoryViewFromNib;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
