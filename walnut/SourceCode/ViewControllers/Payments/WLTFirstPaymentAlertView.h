//
//  WLTFirstPaymentAlertView.h
//  walnut
//
//  Created by Harshad on 19/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "PPTModalContentView.h"

@interface WLTFirstPaymentAlertView : PPTModalContentView {
    
    __weak IBOutlet UIButton *cancelButton;
    __weak IBOutlet UIButton *proceedButton;
    __weak IBOutlet UIButton *termsAndConditionsButton;
}

@property(nonatomic, strong) SuccessBlock proceedBlock;
+(BOOL)canShow;

@end
