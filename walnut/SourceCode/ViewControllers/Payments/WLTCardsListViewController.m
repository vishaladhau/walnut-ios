//
//  WLTCardsListViewController.m
//  walnut
//
//  Created by Abhinav Singh on 08/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTCardsListViewController.h"
#import "WLTCardEntryAccessoryView.h"
#import "WLTCardsScrollView.h"
#import "WLTEmptyCardView.h"
#import "WLTInstrumentsManager.h"
#import "WLTNewBankAccountViewController.h"
#import "LSButton.h"
#import "WLTCardView.h"
#import "WLTPaymentTransactionDetailViewController.h"
#import "WLTPaymentTransaction.h"
#import "WLTDatabaseManager.h"
#import "WLTPaymentTransationsManager.h"
#import "WLTRootViewController.h"

@interface WLTCardsListViewController () <WLTCardEntryAccessoryViewDelegate, WLTCardsScrollViewDelegate>{
    
    UIBarButtonItem *doneItem;
    
    CreditCardBrand lastDetectedBrand;
    
    __weak WLTCardEntryAccessoryView *entryView;
    __weak NSLayoutConstraint *bottomMargin;
}

@property(nonatomic, strong) NSArray *allIntruments;
@property(nonatomic, assign) BOOL redirectToTransactionAfterUpdate;

@property(nonatomic, strong) NSString *toShowCardID;
@property(nonatomic, strong) NSString *justAddedCardID;

@property(nonatomic, assign) PaymentInstrumentType instrumentsType;

-(void)loadNewBankAccountViewController;

@end

@implementation WLTCardsListViewController


-(void)keyboardWillShow:(NSNotification*)notify {
    
    if (!theScrollView.selectedCardView) {
        [UIView animateWithDuration:0.3 animations:^{
        
            newCardEntryBottomView.alpha = 0;
        }];
    }
}

-(void)keyboardWillHide:(NSNotification *)notification {
    
    if (!theScrollView.selectedCardView) {
        [UIView animateWithDuration:0.3 animations:^{
            
            newCardEntryBottomView.alpha = 1;
        }];
    }
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cardChanged:) name:NotifyCardChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cardAdded:) name:NotifyNewCardAdded object:nil];
    
    bottomBackViewHeight.constant = 250;
    
    self.redirectToTransactionAfterUpdate = NO;
    
    theScrollView.backgroundColor = [UIColor sanJuanColor];
    theScrollView.delegate = self;
    
    scrollViewWidth.constant = [UIScreen mainScreen].bounds.size.width;
    
    WLTCardEntryAccessoryView *inputView = [WLTCardEntryAccessoryView initWithDefaultXib];
    inputView.flowType = self.flowType;
    inputView.delegate = self;
    inputView.translatesAutoresizingMaskIntoConstraints = NO;
    [backScrollView addSubview:inputView];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(inputView, theScrollView);
    [backScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[inputView]-0-|" options:0 metrics:nil views:dict]];
    [backScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[inputView(==45)]" options:0 metrics:nil views:dict]];
    
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:inputView
                                                                  attribute:NSLayoutAttributeTop
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:theScrollView
                                                                  attribute:NSLayoutAttributeBottom
                                                                 multiplier:1 constant:0];
    [backScrollView addConstraint:constraint];
    [inputView layoutIfNeeded];
    
    bottomMargin = constraint;
    entryView = inputView;
    
    UIBarButtonItem *addNewCard = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"AddIcon"] style:UIBarButtonItemStyleDone
                                                                  target:self action:@selector(addNewCardClicked:)];
    self.navigationItem.rightBarButtonItem = addNewCard;
    doneItem = addNewCard;
    
    self.instrumentsType = PaymentInstrumentTypeBoth;
    if (self.transactionIDToShow) {
        self.instrumentsType = PaymentInstrumentTypeReceive;
    }
    
    [backScrollView bringSubviewToFront:theScrollView];
    
    {
        [entryView showCVVEntryPercent:0 animated:NO];
    }
    
    {
        bottomBackView.backgroundColor = [UIColor clearColor];
    }
    
    {
        orLabel.textColor = [UIColor cadetColor];
        orLabel.font = [UIFont sfUITextRegularOfSize:14];
        
        newCardEntryBottomView.backgroundColor = [UIColor clearColor];
        
        addNewBankButton.tintColor = [UIColor denimColor];
        [addNewBankButton setTitle:@"Add bank account" forState:UIControlStateNormal];
    }
    
    oldCardEntryBottomView.backgroundColor = [UIColor clearColor];
    {
        makeDefaultBackView.backgroundColor = [UIColor whiteColor];
        [makeDefaultBackView addBorderLineAtPosition:(BorderLinePositionTop|BorderLinePositionBottom)];
        
        [makeDefaultSwitch setOnTintColor:[UIColor appleGreenColor]];
        
        makeDefaultLabel.textColor = [UIColor cadetColor];
        makeDefaultLabel.font = [UIFont sfUITextRegularOfSize:16];
        makeDefaultLabel.text = @"Always use for receiving money";
    }
    
    {
        [addBankBackView addBorderLineAtPosition:(BorderLinePositionTop|BorderLinePositionBottom)];
        addBankBackView.backgroundColor = [UIColor whiteColor];
        addBankBackView.clipsToBounds = YES;
        
        [addBankButton addTarget:self action:@selector(addNewBankClicked:) forControlEvents:UIControlEventTouchUpInside];
        [addBankButton applyDefaultStyle];
        [addBankButton setTitle:@"Add bank account"];
    }
    
    {
        dontSupportBackView.backgroundColor = [UIColor whiteColor];
        [dontSupportBackView addBorderLineAtPosition:(BorderLinePositionTop|BorderLinePositionBottom)];
        
        dontSupportLabel.textColor = [UIColor cadetColor];
        dontSupportLabel.font = [UIFont sfUITextRegularOfSize:12];
        dontSupportLabel.numberOfLines = 2;
        dontSupportLabel.minimumScaleFactor = 0.5;
        dontSupportLabel.adjustsFontSizeToFitWidth = YES;
    }
    
    {
        removeCardBackView.backgroundColor = [UIColor whiteColor];
        [removeCardBackView addBorderLineAtPosition:(BorderLinePositionTop|BorderLinePositionBottom)];
        
        [removeCardButton setTitleColor:[UIColor pomegranateColor] forState:UIControlStateNormal];
        [removeCardButton.titleLabel setFont:[UIFont sfUITextRegularOfSize:16]];
        
        receiveUsingThisBackView.backgroundColor = [UIColor whiteColor];
        [receiveUsingThisBackView addBorderLineAtPosition:(BorderLinePositionTop|BorderLinePositionBottom)];
        
        [receiveUsingThisButton setTitleColor:[UIColor appleGreenColor] forState:UIControlStateNormal];
        [receiveUsingThisButton.titleLabel setFont:[UIFont sfUITextRegularOfSize:16]];
        
        if (self.instrumentsType == PaymentInstrumentTypeReceive) {
            
            receiveUsingThisBackView.hidden = NO;
            
            removeCardBackView.hidden = YES;
            makeDefaultBackView.hidden = YES;
            dontSupportBackView.hidden = YES;
            
            [entryView setUpForReceivingOnly];
        }else{
            
            receiveUsingThisBackView.hidden = YES;
            
            dontSupportBackView.hidden = NO;
            removeCardBackView.hidden = NO;
            makeDefaultBackView.hidden = NO;
        }
        
        fastFundInfoLabel.textColor = [UIColor cadetColor];
        fastFundInfoLabel.font = [UIFont sfUITextRegularOfSize:13];
        [fastFundInfoLabel setPreferredMaxLayoutWidth:([UIScreen mainScreen].bounds.size.width-70)];
    }
    
    [self changeDisplayedDataForInstrumentsArray:nil];
    [self refreshCardsList];
}

-(void)cardChanged:(NSNotification*)notify {
    
    [self refreshCardsList];
}

-(void)cardAdded:(NSNotification*)notify {
    
    if (![notify.object isEqual:entryView]) {
        
        [self refreshCardsList];
    }
}

-(BOOL)checkIfRedirectToPayments {
    
    BOOL redirecting  = NO;
    if (self.redirectToTransactionAfterUpdate && (self.instrumentsType == PaymentInstrumentTypeReceive)) {
        
        BOOL canContinue = NO;
        for (GTLWalnutMPaymentInstrument *instr in self.allIntruments) {
            if (instr.receiveDefault.boolValue) {
                canContinue = YES;
                break;
            }
        }
        
        GTLWalnutMPaymentInstrument *justAddedCard = nil;
        if (self.justAddedCardID) {
            
            for (GTLWalnutMPaymentInstrument *instr in self.allIntruments) {
                if ([instr.instrumentUuid isEqualToString:self.justAddedCardID]) {
                    justAddedCard = instr;
                    break;
                }
            }
        }
        
        if (canContinue) {
            
            redirecting = YES;
            
            [[UIApplication sharedApplication] setScheduledLocalNotifications:nil];
            
            __weak typeof(self) weakSelf = self;
            
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"transactionId == %@", self.transactionIDToShow];
            
            [self startLoadingWithColor:[UIColor appleGreenColor]];
            
            [[WLTPaymentTransationsManager sharedManager] coreDataPaymentTransactionForPredicate:pred completion:^(WLTPaymentTransaction *object, NSError *error) {
                
                if (object) {
                    
                    if (object.status == PaymentStatusPushPending) {
                        if (object.subStatus != PaymentSubStatusWaiting) {
                            
                            object.subStatusNum = @(PaymentSubStatusWaiting);
                            [[WLTDatabaseManager sharedManager] saveDataBase];
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:PaymentTransactionsChanged object:nil];
                        }
                    }
                    
                    UIViewController *prev = nil;
                    for ( UIViewController *cont in weakSelf.navigationController.viewControllers) {
                        if ([cont isKindOfClass:[WLTPaymentTransactionDetailViewController class]]) {
                            
                            WLTPaymentTransactionDetailViewController *payCont = (WLTPaymentTransactionDetailViewController*)cont;
                            if ([payCont.transaction.transactionId isEqualToString:weakSelf.transactionIDToShow]) {
                                prev = payCont;
                                break;
                            }
                        }
                    }
                    
                    NSMutableArray *nextCont = [NSMutableArray new];
                    if (prev) {
                        
                        NSArray *subArray = [weakSelf.navigationController allControllersUpTo:prev];
                        [nextCont addObjectsFromArray:subArray];
                    }else {
                        
                        NSArray *subArray = [weakSelf.navigationController allControllersUpTo:weakSelf];
                        [nextCont addObjectsFromArray:subArray];
                        [nextCont removeLastObject];
                        
                        WLTPaymentTransactionDetailViewController *controller = [weakSelf controllerWithIdentifier:@"WLTPaymentTransactionDetailViewController"];
                        controller.navBarColor = nil;
                        controller.transaction = object;
                        
                        [nextCont addObject:controller];
                    }
                    
                    [weakSelf.navigationController setViewControllers:nextCont animated:YES];
                    
                    if (justAddedCard) {
                        [[WLTInstrumentsManager sharedManager] showMakeNewDefaultPopUpForIntrument:justAddedCard completion:nil];
                    }
                }
                else {
                    
                    [weakSelf showAlertForError:error];
                }
                
                [weakSelf endViewLoading];
            }];
        }
        else {
            
            if (justAddedCard) {
                
                NSError *error = [NSError walnutErrorWithMessage:@"We cannot receive money in this card please try adding VISA debit card."];
                [self showAlertForError:error];
            }
        }
    }
    
    return redirecting;
}

-(void)refreshCardsList{
    
    __weak typeof(self) weakSelf = self;
    [self startLoadingWithColor:[UIColor appleGreenColor]];
    
    [[WLTInstrumentsManager sharedManager] validCardsForTransactionType:self.instrumentsType invalidateCache:NO withCompletion:^(id data) {
        
        [weakSelf endViewLoading];
        if ([data isKindOfClass:[NSError class]]) {
            
            [weakSelf showAlertForError:data];
        }else {
            
            [weakSelf changeDisplayedDataForInstrumentsArray:data];
        }
    }];
}

-(void)changeDisplayedDataForInstrumentsArray:(NSArray*)instruments {
    
    self.allIntruments = instruments;
    [theScrollView addSubviewsForCards:instruments];
    
    if (self.toShowCardID.length) {
        
        [theScrollView selectCardOfID:self.toShowCardID animated:NO];
    }
    else if (instruments.count) {
        
        GTLWalnutMPaymentInstrument *instr = [instruments firstObject];
        [theScrollView selectCardOfID:instr.instrumentUuid animated:NO];
    }
    
    [self changeRespondersForCurrentSelectionAnimated:NO];
    
    if (![self checkIfRedirectToPayments]) {
        
        if (self.justAddedCardID.length) {
            
            for (GTLWalnutMPaymentInstrument *instr in self.allIntruments) {
                
                if ([instr.instrumentUuid isEqualToString:self.justAddedCardID]) {
                    
                    __weak typeof(self) weakSelf = self;
                    [self startLoading];
                    
                    [[WLTInstrumentsManager sharedManager] showMakeNewDefaultPopUpForIntrument:instr completion:^(id data) {
                        
                        if ([data isKindOfClass:[NSError class]]) {
                            
                            [weakSelf showAlertForError:data];
                        }else if ([data isKindOfClass:[NSNumber class]]) {
                            
                            if ([data boolValue]) {
                                
                                weakSelf.toShowCardID = instr.instrumentUuid;
                                [weakSelf refreshCardsList];
                            }
                        }
                        
                        [weakSelf endViewLoading];
                    }];
                    
                    break;
                }
            }
        }
    }
    
    self.toShowCardID = nil;
    self.justAddedCardID = nil;
    
    self.redirectToTransactionAfterUpdate = NO;
}

#pragma mark - WLTCardsScrollViewDelegate

-(void)changeRespondersForCurrentSelectionAnimated:(BOOL)animate {
    
    [entryView resetEnteredText];
    
    CGFloat durat = 0;
    if (animate) {
        durat = 0.3;
    }
    if (theScrollView.selectedCardView) {
        
        GTLWalnutMPaymentInstrument *instr = theScrollView.selectedCardView.instrument;
        
        self.title = @"Saved Cards/Banks";
        
        [self.view endEditing:YES];
        
        BOOL showAddBank = NO;
        if (![[WLTInstrumentsManager sharedManager] defaultReceivingCard]) {
            if (!instr.enabledForMoneyreceive.boolValue) {
                showAddBank = YES;
            }
        }
        
        if (instr.enabledForMoneyreceive.boolValue) {
            
            if (instr.fastFundEnabled.boolValue) {
                
                fastFundInfoImageView.image = [[UIImage imageNamed:@"FastFundEnabled"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                fastFundInfoImageView.tintColor = [UIColor colorWithRed:1.00 green:0.76 blue:0.03 alpha:1.00];
                
                fastFundInfoLabel.text = @"Fast money transfer avaliable";
            }else {
                
                fastFundInfoImageView.tintColor = [UIColor cadetColor];
                fastFundInfoImageView.image = [[UIImage imageNamed:@"FastFundDisabled"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                fastFundInfoLabel.text = @"Fast money transfer not available. You can still receive money here but it will take about 2 business days to reach your bank.";
            }
        }else {
            
            fastFundInfoImageView.image = nil;
            fastFundInfoLabel.text = nil;
        }
        
        if (showAddBank) {
            
            addBankBackViewTopConstarint.constant = 25;
            addBankBackViewHeight.constant = 55;
        }else {
            
            addBankBackViewTopConstarint.constant = 0;
            addBankBackViewHeight.constant = 0;
        }
        
        if (bottomMargin.constant != -entryView.height) {
            bottomMargin.constant = -entryView.height;
        }
        
        if (self.instrumentsType != PaymentInstrumentTypeReceive) {
            
            makeDefaultBackView.hidden = YES;
            dontSupportBackView.hidden = YES;
            
            if (instr.enabledForMoneyreceive.boolValue) {
                
                makeDefaultBackView.hidden = NO;
            }else {
                
                if (![[WLTInstrumentsManager sharedManager] defaultReceivingCard]) {
                    dontSupportLabel.text = @"This card does not support receiving payments at present. Please provide bank details to receive.";
                }else {
                    dontSupportLabel.text = @"Currently this card does not support receiving money.";
                }
                
                dontSupportBackView.hidden = NO;
            }
        }
        
        if (!makeDefaultBackView.isHidden) {
            if (instr.receiveDefault.boolValue) {
                makeDefaultSwitch.on = YES;
            }else {
                makeDefaultSwitch.on = NO;
            }
        }
        
        if (!removeCardBackView.isHidden) {
            
            if ([instr.type isEqualToString:InstrumentTypeBank]) {
                
                [removeCardButton setTitle:@"Delete bank account" forState:UIControlStateNormal];
            }else {
                [removeCardButton setTitle:@"Delete card" forState:UIControlStateNormal];
            }
        }
        
        if (!receiveUsingThisBackView.isHidden) {
            
            if (instr.enabledForMoneyreceive.boolValue) {
                
                receiveUsingThisButton.enabled = YES;
                
                if ([instr.type isEqualToString:InstrumentTypeBank]) {
                    [receiveUsingThisButton setTitle:@"Receive in this bank" forState:UIControlStateNormal];
                }else {
                    [receiveUsingThisButton setTitle:@"Receive in This card." forState:UIControlStateNormal];
                }
            }else {
                
                receiveUsingThisButton.enabled = NO;
                
                if ([instr.type isEqualToString:InstrumentTypeBank]) {
                    [receiveUsingThisButton setTitle:@"Can't receive in this bank" forState:UIControlStateNormal];
                }else {
                    [receiveUsingThisButton setTitle:@"Can't receive in This card." forState:UIControlStateNormal];
                }
            }
        }
        
        [UIView animateWithDuration:durat animations:^{
            
            oldCardEntryBottomView.alpha = 1;
            newCardEntryBottomView.alpha = 0;
            
            [backScrollView layoutIfNeeded];
        }];
        
        if (!self.navigationItem.rightBarButtonItem) {
            
            self.navigationItem.rightBarButtonItem = doneItem;
        }
    }
    else {
        
        self.navigationItem.rightBarButtonItem = nil;
        self.title = @"Add Card";
        
        if (bottomMargin.constant != 0) {
            bottomMargin.constant = 0;
        }
        
        [UIView animateWithDuration:durat animations:^{
            
            oldCardEntryBottomView.alpha = 0;
            newCardEntryBottomView.alpha = 1;
            
            [backScrollView layoutIfNeeded];
        }];
    }
}

-(void)cardsScrollView:(WLTCardsScrollView *)scrollView selectedIndexChanged:(NSInteger)newIndex {
    
    [self changeRespondersForCurrentSelectionAnimated:YES];
}

#pragma mark - WLTCardEntryAccessoryViewDelegate

-(void)cardEntryView:(WLTCardEntryAccessoryView *)view changedCardNumber:(NSString *)cardNum andExpDate:(NSString *)expDate {
    
    if (cardNum.length) {
        
        NSString *emptyPlaceholder = [NSString emptyCardPlaceHolder];
        NSString *cardNumStr = cardNum.formattedCreditCardNumber;
        
        theScrollView.emptyCardView->cardNumberLabel.text = [emptyPlaceholder stringByReplacingCharactersInRange:NSMakeRange(0, cardNumStr.length) withString:cardNumStr];
    }else {
        theScrollView.emptyCardView->cardNumberLabel.text = [NSString emptyCardPlaceHolder];
    }
    
    if (expDate.length) {
        theScrollView.emptyCardView->cardExpDateLabel.text = [@"XX/XX" stringByReplacingCharactersInRange:NSMakeRange(0, expDate.length) withString:expDate];
    }else {
        theScrollView.emptyCardView->cardExpDateLabel.text = @"XX/XX";
    }
    
    NSString *withoutSeperators = cardNum.creditCardNumber;
    CreditCardBrand brand = [withoutSeperators cardBrand];
    
    if ( brand != lastDetectedBrand ) {
        
        lastDetectedBrand = brand;
        if ( (lastDetectedBrand == CreditCardBrandInvalid) || (lastDetectedBrand == CreditCardBrandUnknown)) {
            
            theScrollView.emptyCardView->cardTypeImageView.image = nil;;
        }else {
            
            NSString *imageName = [NSString iconForCardType:lastDetectedBrand];
            theScrollView.emptyCardView->cardTypeImageView.image = [UIImage imageNamed:imageName];
        }
    }
}

-(void)cardEntryView:(WLTCardEntryAccessoryView *)view addedNewInstrumentOfID:(NSString *)instrID {
    
    self.justAddedCardID = instrID;
    self.toShowCardID = instrID;
    
    self.redirectToTransactionAfterUpdate = YES;
    
    [self refreshCardsList];
}

-(void)cardEntryView:(WLTCardEntryAccessoryView *)view payClickedWithCVV:(NSString *)cvvString {
    
    if (theScrollView.selectedCardView) {
        
    }
}

-(BOOL)cardEntryViewCanDisplayAddCard {
    
    return YES;
}

-(void)cardEntryViewEditingStarted:(WLTCardEntryAccessoryView*)view {
    
    [self changeRespondersForCurrentSelectionAnimated:YES];
}

#pragma mark - Button Events

-(void)addNewCardClicked:(UIBarButtonItem*)item {
    
    [theScrollView selectCardOfID:nil animated:YES];
    [self changeRespondersForCurrentSelectionAnimated:YES];
}

-(IBAction)makeReceiveDefaultButtonClicked:(UIButton*)btn {
    
    __weak GTLWalnutMPaymentInstrument *weakInstr = theScrollView.selectedCardView.instrument;
    if (weakInstr) {
        
        weakInstr.receiveDefault = @(YES);
        __weak typeof(self) weakSelf = self;
        
        [weakSelf startLoadingWithColor:self.navBarColor];
        
        [[WLTInstrumentsManager sharedManager] addInstrumentToWalnut:weakInstr withCompletion:^(id data) {
            
            [weakSelf endViewLoading];
            
            if ([data isKindOfClass:[NSError class]]) {
                
                weakInstr.receiveDefault = @(NO);
                [weakSelf showAlertForError:data];
            }else {
                
                weakSelf.toShowCardID = weakInstr.instrumentUuid;
                weakSelf.redirectToTransactionAfterUpdate = YES;
                
                [weakSelf refreshCardsList];
            }
        }];
    }
}

-(IBAction)makeDefaultSwitchToggled:(UISwitch*)switchDefault {
    
    __weak GTLWalnutMPaymentInstrument *weakInstr = theScrollView.selectedCardView.instrument;
    if (weakInstr) {
        
        if (!weakInstr.receiveDefault.boolValue) {
            
            weakInstr.receiveDefault = @(YES);
            __weak typeof(self) weakSelf = self;
            
            [weakSelf startLoadingWithColor:self.navBarColor];
            
            [[WLTInstrumentsManager sharedManager] addInstrumentToWalnut:weakInstr withCompletion:^(id data) {
                
                [weakSelf endViewLoading];
                
                if ([data isKindOfClass:[NSError class]]) {
                    
                    [weakSelf showAlertForError:data];
                    
                    weakInstr.receiveDefault = @(NO);
                    switchDefault.on = !switchDefault.on;
                }else {
                    
                    weakSelf.toShowCardID = weakInstr.instrumentUuid;
                    weakSelf.redirectToTransactionAfterUpdate = YES;
                    
                    [weakSelf refreshCardsList];
                }
            }];
        }
        else {
            
            [switchDefault setOn:YES animated:YES];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"You need one card/bank to receive money." delegate:nil
                                                  cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

-(IBAction)deleteCardClicked:(UIButton*)btn {
    
    void (^ deleteCard)(GTLWalnutMPaymentInstrument *card) = ^void (GTLWalnutMPaymentInstrument *card){
        
        if (card) {
            
            __weak typeof(self) weakSelf = self;
            [self startLoading];
            
            [[WLTInstrumentsManager sharedManager] removeInstrumentFromWalnut:card withCompletion:^(id data) {
                
                [weakSelf endViewLoading];
                
                if ([data isKindOfClass:[NSError class]]) {
                    
                    [weakSelf showAlertForError:data];
                }else {
                    
                    [weakSelf refreshCardsList];
                }
            }];
        }
    };
    
    __weak GTLWalnutMPaymentInstrument *weakInstr = theScrollView.selectedCardView.instrument;
    if (weakInstr) {
        
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Please Confirm!" message:@"Are you sure you want to delete this card?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actionDelete = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 
                                                                 deleteCard(weakInstr);
                                                             }];
        
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                               style:UIAlertActionStyleDefault
                                                             handler:nil];
        
        [controller addAction:actionCancel];
        [controller addAction:actionDelete];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(IBAction)addNewBankClicked:(LSButton*)btn {
    [self loadNewBankAccountViewController];
}

-(void)loadNewBankAccountViewController {
    
    __weak typeof(self) weakSelf = self;
    WLTNewBankAccountViewController *newBank = [self controllerWithIdentifier:@"WLTNewBankAccountViewController"];
    newBank.showReceiveAlert = self.showReceiveAlert;
    [newBank setCompletion:^(GTLWalnutMPaymentInstrument *instr){
        
        [weakSelf dismissViewControllerAnimated:YES completion:^{
            
            if (instr) {
                if (weakSelf.showReceiveAlert) {
					
					UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations on receiving money!" message:[NSString stringWithFormat:@"You will receive money in account %@  ****%@", instr.bank, instr.last4Digits]
																   delegate:nil cancelButtonTitle:@"Got it" otherButtonTitles:nil, nil];
					[alert show];
                }
                
                weakSelf.justAddedCardID = instr.instrumentUuid;
                weakSelf.toShowCardID = instr.instrumentUuid;
                weakSelf.redirectToTransactionAfterUpdate = YES;
                
                [weakSelf refreshCardsList];
            }
        }];
    }];
    
    UINavigationController *navCont = [[UINavigationController alloc] initWithRootViewController:newBank];
    [navCont applyTranslusentStyle];
    
    [self presentViewController:navCont animated:YES completion:nil];
}

@end
