//
//  WLTHomeViewController.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 03/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTViewController.h"
#import "WLTHomeIconButton.h"
#import "WLTHomeHeaderButton.h"

@class WLTSettingsViewController;
@class WLTGroupListViewController;
@class WLTPaymentTransactionsViewController;

@interface WLTHomeViewController : WLTViewController <UINavigationControllerDelegate>{
    
}

@property(nonatomic, weak) WLTSettingsViewController *settingsController;
@property(nonatomic, weak) WLTGroupListViewController *groupListController;
@property(nonatomic, weak) WLTPaymentTransactionsViewController *paymentListController;

-(BOOL)isShowingDetailsOfGroup:(WLTGroup*)grp;
-(BOOL)isShowingGroupList;
-(WLTViewController*)visibleController;

@end
