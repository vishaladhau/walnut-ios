//
//  WLTTransactionsListViewController.m
//  walnut
//
//  Created by Abhinav Singh on 02/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTTransactionsListViewController.h"
#import "WLTGroup.h"
#import "WLTTransaction.h"
#import "WLTTransactionUser.h"

#import "WLTUserTransactionTableViewCell.h"
#import "WLTCombinedGroupPayments.h"
#import "WLTContactsManager.h"

@interface WLTTransactionsListViewController () <UITableViewDelegate, UITableViewDataSource> {
    
    NSMutableDictionary *mappingDictionary;
}

@end

@implementation WLTTransactionsListViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectZero];
    [theTableView registerNib:[UINib nibWithNibName:@"WLTUserTransactionTableViewCell" bundle:nil] forCellReuseIdentifier:@"WLTUserTransactionTableViewCell"];
    theTableView.rowHeight = UITableViewAutomaticDimension;
    theTableView.tableFooterView = footer;
    theTableView.backgroundColor = self.view.backgroundColor;
    theTableView.separatorInset = UIEdgeInsetsZero;
    
    NSString *currentUserMob = CURRENT_USER_MOB;
    NSString *userMob = [self.paymentGroup.user completeNumber];
    
    NSMutableDictionary *dictMaping = [NSMutableDictionary new];
    
    for ( WLTGroupPaymentData *grpsData in self.paymentGroup.groupsData ) {
        
        NSMutableArray *userGrpTrans = [NSMutableArray new];
        
        for ( WLTTransaction *trans in grpsData.group.transactions ) {
            if ([trans.type isEqualToString:TransactionObjectTypeSplit]) {
                
                NSString *ownersMobile = trans.owner.mobileString;
                
                BOOL isCurrentUserOwner = NO;
                BOOL isUserOwner = NO;
                
                if ([ownersMobile isEqualToString:userMob]) {
                    
                    isUserOwner = YES;
                }else if ([ownersMobile isEqualToString:currentUserMob]) {
                    
                    isCurrentUserOwner = YES;
                }
                
                if (isCurrentUserOwner || isUserOwner) {
                    
                    for ( WLTTransactionUser *usr in trans.splits ) {
                        
                        BOOL isRelavent = NO;
                        NSString *tUm = usr.mobileString;
                        if (isCurrentUserOwner) {
                            
                            if ([tUm isEqualToString:userMob]) {
                                isRelavent = YES;
                            }
                        }
                        else if (isUserOwner) {
                            
                            if ([tUm isEqualToString:currentUserMob]) {
                                isRelavent = YES;
                            }
                        }
                        
                        if (isRelavent) {
                            [userGrpTrans addObject:trans];
                            break;
                        }
                    }
                }
            }
        }
        
        [userGrpTrans sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"createdDate" ascending:NO]]];
        dictMaping[grpsData.group.groupUUID] = userGrpTrans;
    }
    
    mappingDictionary = dictMaping;
    
    __weak typeof(self) weakSelf = self;
    [[WLTContactsManager sharedManager] displayNameForUser:self.paymentGroup.user defaultIsMob:YES completion:^(NSString *data) {
		
		if (weakSelf) {
			
			if (weakSelf.paymentGroup.transactionState == UserTransactionStateOwe) {
				[weakSelf setTitle:[NSString stringWithFormat:@"You Owe %@", data] andSubtitle:@(weakSelf.paymentGroup.amount).displayCurrencyString];
			}else {
				[weakSelf setTitle:[NSString stringWithFormat:@"%@ Owe You", data] andSubtitle:@(weakSelf.paymentGroup.amount).displayCurrencyString];
			}
		}
    }];
}

#pragma mark - UITableViewDelegate & UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.paymentGroup.groupsData.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    WLTGroupPaymentData *grpsData = self.paymentGroup.groupsData[section];
    NSArray *dataArray = mappingDictionary[grpsData.group.groupUUID];
	NSInteger count = dataArray.count;
	
	if(count == 0) {
		count = 1;
	}
	
    return count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	BOOL isEmpty = NO;
	
	WLTGroupPaymentData *grpsData = self.paymentGroup.groupsData[indexPath.section];
	NSArray *dataArray = mappingDictionary[grpsData.group.groupUUID];
	if (dataArray.count == 0) {
		isEmpty = YES;
	}
	
	UITableViewCell *cell = nil;
	if (!isEmpty) {
		
		WLTUserTransactionTableViewCell *cellT = [tableView dequeueReusableCellWithIdentifier:@"WLTUserTransactionTableViewCell"];
		WLTTransaction *trans = dataArray[indexPath.row];
		[cellT showDetailsOfTransaction:trans fromUser:self.paymentGroup.user];
		
		cell = cellT;
	}else {
		
		cell = [tableView dequeueReusableCellWithIdentifier:@"DefaultTableCell"];
		if (!cell) {
			
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DefaultTableCell"];
			cell.selectionStyle = UITableViewCellSelectionStyleNone;
			cell.textLabel.font = [UIFont sfUITextRegularOfSize:12];
			cell.textLabel.textAlignment = NSTextAlignmentCenter;
			cell.textLabel.text = @"No Transactions found in this group!";
			cell.textLabel.textColor = [UIColor mediumGrayTextColor];
		}
	}
	
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    return 100;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 40;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    WLTGroupPaymentData *grpsData = self.paymentGroup.groupsData[section];
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.width, 40)];
    backView.backgroundColor = tableView.backgroundColor;
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.translatesAutoresizingMaskIntoConstraints = NO;
    headerLabel.textColor = [UIColor darkGrayTextColor];
    headerLabel.font = [UIFont sfUITextRegularOfSize:16];
    [headerLabel displayGroupName:grpsData.group checkDirect:NO];
    [backView addSubview:headerLabel];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(headerLabel);
    [backView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[headerLabel]-10-|" options:0 metrics:nil views:dict]];
    [backView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[headerLabel]-5-|" options:0 metrics:nil views:dict]];
	
    return backView;
}

@end
