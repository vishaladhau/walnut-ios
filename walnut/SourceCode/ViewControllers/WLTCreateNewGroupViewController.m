//
//  WLTCreateNewGroupViewController.m
//  walnut
//
//  Created by Abhinav Singh on 11/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTCreateNewGroupViewController.h"
#import "WLTAdminTableViewCell.h"
#import "WLTContact.h"
#import "WLTColorPalette.h"
#import "WLTTableHeaderView.h"
#import "WLTColorSelectionTableViewCell.h"
#import "WLTColorPickerViewController.h"
#import "WLTGroupTypeTableViewCell.h"
#import "WLTContactsManager.h"
#import "WLTGroupNameTableViewCell.h"
#import "WLTRootViewController.h"
#import "WLTUser.h"
#import "WLTGroupDetailViewController.h"
#import "WLTGroupsManager.h"
#import "WLTGroupContactsViewController.h"
#import "WLTEditableContactTableViewCell.h"
#import "WLTNewGroupValidator.h"

@interface WLTCreateNewGroupViewController () <WLTEditableContactTableViewCellDelegate>{
    
}

@end

@implementation WLTCreateNewGroupViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"Create Group";
    
    currentUserMobileNumber = CURRENT_USER_MOB;
    
    UITableView *table = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    table.delegate = self;
    table.dataSource = self;
    table.translatesAutoresizingMaskIntoConstraints = NO;
    table.rowHeight = UITableViewAutomaticDimension;
    table.separatorInset = UIEdgeInsetsZero;
    [self.view addSubview:table];
    
    [table registerClass:[UITableViewCell class] forCellReuseIdentifier:@"DefaultTableCell"];
    [table registerNib:[UINib nibWithNibName:@"WLTGroupNameTableViewCell" bundle:nil] forCellReuseIdentifier:@"WLTGroupNameTableViewCell"];
    [table registerNib:[UINib nibWithNibName:@"WLTGroupTypeTableViewCell" bundle:nil] forCellReuseIdentifier:@"WLTGroupTypeTableViewCell"];
    [table registerNib:[UINib nibWithNibName:@"WLTColorSelectionTableViewCell" bundle:nil] forCellReuseIdentifier:@"WLTColorSelectionTableViewCell"];
    [table registerClass:[WLTEditableContactTableViewCell class] forCellReuseIdentifier:@"WLTEditableContactTableViewCell"];
    [table registerClass:[WLTAdminTableViewCell class] forCellReuseIdentifier:@"WLTAdminTableViewCell"];
    
    {
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
        
        UILabel *labelTitle = [[UILabel alloc] initWithFrame:CGRectZero];
        labelTitle.text = @"How Smart Settlement Works";
        labelTitle.translatesAutoresizingMaskIntoConstraints = NO;
        labelTitle.textColor = [UIColor cadetColor];
        labelTitle.font = [UIFont sfUITextMediumOfSize:16];
        labelTitle.textAlignment = NSTextAlignmentCenter;
        labelTitle.numberOfLines = 0;
        [labelTitle setPreferredMaxLayoutWidth:(footerView.width-20)];
        [footerView addSubview:labelTitle];
        
        UIImageView *footer = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"GroupSettleInfo"]];
        footer.translatesAutoresizingMaskIntoConstraints = NO;
        footer.contentMode = UIViewContentModeCenter;
        [footerView addSubview:footer];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.translatesAutoresizingMaskIntoConstraints = NO;
        label.textColor = [UIColor cadetColor];
        label.font = [UIFont sfUITextRegularOfSize:12];
        label.textAlignment = NSTextAlignmentCenter;
        label.numberOfLines = 0;
        [label setPreferredMaxLayoutWidth:(footerView.width-20)];
        [footerView addSubview:label];
        
        NSString *textLineOne = @"Yellow paid ₹100 for Red, Blue paid ₹100 for Yellow.";
        NSString *textLineTwo = @"Smart Settlement: Red owes ₹100 to Blue.";
        
        NSString *complete = [NSString stringWithFormat:@"%@\n%@", textLineOne, textLineTwo];
        NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:complete];
        [attributed addAttributes:@{NSFontAttributeName:[UIFont sfUITextMediumOfSize:12]} range:[complete rangeOfString:textLineTwo]];
        label.attributedText = attributed;
        
        NSDictionary *dict = NSDictionaryOfVariableBindings(label, footer, labelTitle);
        [footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-30-[labelTitle]-5-[footer]-5-[label]-30-|" options:0 metrics:nil views:dict]];
        [footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[label]-0-|" options:0 metrics:nil views:dict]];
        [footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[labelTitle]-0-|" options:0 metrics:nil views:dict]];
        [footerView addConstraint:[NSLayoutConstraint constraintWithItem:footer attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:footerView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
        
        footerView.height = [labelTitle intrinsicContentSize].height + [footer intrinsicContentSize].height + [label intrinsicContentSize].height + 75;
        
        table.tableFooterView = footerView;
    }
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(table);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[table]-0-|" options:0 metrics:nil views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[table]-0-|" options:0 metrics:nil views:dict]];
    
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                 style:UIBarButtonItemStyleDone
                                                                target:self action:@selector(doneClicked:)];
    self.navigationItem.rightBarButtonItem = doneItem;
    
    theTableView = table;
    theScrollView = table;
    
    makePrivateGroup = NO;
}

-(void)checkAndAddOwnerOfGroup {
    
    WLTContact *owner = nil;
    NSInteger ownerIndex = NSNotFound;
    for ( WLTContact *cont in self.members ) {
        
        if ([cont.mobile.number isEqualToString:currentUserMobileNumber]) {
            ownerIndex = [self.members indexOfObject:cont];
            owner = cont;
            break;
        }
    }
    
    if (ownerIndex == NSNotFound) {
        
        WLTContact *cont = [[WLTContact alloc] initWithName:[WLTDatabaseManager sharedManager].currentUser.name andNumber:currentUserMobileNumber];
        [self.members insertObject:cont atIndex:0];
    }else if (ownerIndex != 0) {
        
        [self.members removeObject:owner];
        [self.members insertObject:owner atIndex:0];
    }
}

-(BOOL)userCanEditGroupInfo {
    return YES;
}

-(BOOL)userCanTakeActions {
    return NO;
}

-(BOOL)canShowGroupName {
    
    BOOL canShow = NO;
    if (self.members.count >= 2) {
        if (![WLTNewGroupValidator isStatusDirectWithMembers:self.members]) {
            canShow = YES;
        }
    }
    
    return canShow;
}

-(void)viewWillAppear:(BOOL)animated {
    
    if (!self.selectedColorID) {
        [self showNewColorID:[[WLTColorPalette sharedPalette] randomGroupColorID]];
    }
    
    [self checkAndAddOwnerOfGroup];
    [super viewWillAppear:animated];
}

-(void)showNewColorID:(NSString*)colorID {
    
    if (!self.selectedColorID || ![self.selectedColorID isEqualToString:colorID]) {
        
        self.selectedColorID = colorID;
        self.navBarColor = [[WLTColorPalette sharedPalette] colorForID:self.selectedColorID];
        
        for ( UIViewController *cont in self.navigationController.viewControllers ) {
            if ([cont isKindOfClass:[WLTGroupContactsViewController class]]) {
                [(WLTGroupContactsViewController*)cont setSelectedColorID:colorID];
            }
        }
        
        [UIView animateWithDuration:0.25 animations:^{
            [self.navigationController.navigationBar setBarTintColor:self.navBarColor];
        }];
        
        [theTableView reloadSections:[NSIndexSet indexSetWithIndex:[self sectionIndexForIdentifier:GroupControllerSectionTypeInfo]] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

-(void)doneClicked:(UIBarButtonItem*)item {
    
    if (self.isLoading) {
        return;
    }
    
    [self.view endEditing:YES];
    
    NSString *tempGrpName = nil;
    if (groupNameTableCell) {
        tempGrpName = [groupNameTableCell->nameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    BOOL isDirectGrp = [WLTNewGroupValidator isStatusDirectWithMembers:self.members];
    if (isDirectGrp) {
        tempGrpName = @"";
    }
    
    NSString *errorMessage = nil;
    UIAlertAction *action = nil;
    if (self.members.count < 2) {
        
        errorMessage = @"Please add some members to the group.";
    }
    else if (!tempGrpName.length && !isDirectGrp) {
        
        errorMessage = @"Please enter name of group.";
        action = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            
                                            [groupNameTableCell->nameTextField becomeFirstResponder];
                                        }];
        
        [theTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if (!self.selectedColorID.length) {
        
        errorMessage = @"Please select colour of your group.";
    }
    
    if (errorMessage.length) {
        
        if (!action) {
            
            action = [UIAlertAction actionWithTitle:@"Okay"
                                              style:UIAlertActionStyleDefault
                                            handler:nil];
        }
        
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Sorry!" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
        [controller addAction:action];
        [self presentViewController:controller animated:YES completion:nil];
    }
    else {
        
        WLTLoggedInUser *currentuser = [WLTDatabaseManager sharedManager].currentUser;
        NSString *currentUserM = CURRENT_USER_MOB;
        
        __weak typeof(self) weakSelf = self;
        
        [self startLoading];
        
        void (^ createNewGroup)() = ^void () {
            
            GTLWalnutMGroup *groupUpdated = [[GTLWalnutMGroup alloc] init];
            groupUpdated.privateProperty = @(makePrivateGroup);
            groupUpdated.deviceUuid = [WLTDatabaseManager sharedManager].appSettings.deviceIdentifier;
            groupUpdated.name = tempGrpName;
            groupUpdated.creationDate = @([NSDate currentTimeStamp] * 1000000);
            
            NSMutableArray *allMems = [NSMutableArray new];
            for ( WLTContact *cont in self.members ) {
                
                if (![cont.mobile.number isEqualToString:currentUserM]) {
                    
                    GTLWalnutMSplitUser *user = [[GTLWalnutMSplitUser alloc] init];
                    user.name = cont.fullName;
                    user.mobileNumber = cont.mobile.number;
                    
                    [allMems addObject:user];
                }
            }
            
            GTLWalnutMSplitUser *user = [[GTLWalnutMSplitUser alloc] init];
            user.name = currentuser.name;
            user.mobileNumber = currentUserM;
            
            [allMems addObject:user];
            
            groupUpdated.owner = user;
            
            groupUpdated.members = allMems;
            
            groupUpdated.uuid = [WLTNetworkManager generateUniqueIdentifier];
            
            GTLQueryWalnut *query = [GTLQueryWalnut queryForGroupCreateWithObject:groupUpdated];
            
            [[WLTNetworkManager sharedManager].walnutService executeQuery:query completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
                
                if (!error) {
                    
                    GTLWalnutMBSGenericObject *oj = object;
                    
                    WLTGroup *grpOne = [[WLTDatabaseManager sharedManager] objectOfClassString:@"WLTGroup"];
                    grpOne.colourID = self.selectedColorID;
                    
                    groupUpdated.uuid = oj.groupUuid;
                    
                    [grpOne updateGroupFromWLTGroup:groupUpdated];
                    
                    [[WLTDatabaseManager sharedManager] saveDataBase];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:NotifyGroupChanged object:grpOne];
                    
                    if (weakSelf.completion) {
                        weakSelf.completion(YES);
                    }
                    
                    if (groupUpdated.members.count == 2) {
                        [weakSelf trackEvent:GA_ACTION_PRIVATE_GROUP_ADDED label:weakSelf.title andValue:@(groupUpdated.members.count)];
                    }else {
                        [weakSelf trackEvent:GA_ACTION_GROUP_ADDED label:weakSelf.title andValue:@(groupUpdated.members.count)];
                    }
                }else {
                    
                    [weakSelf showAlertForError:error];
                    [weakSelf endViewLoading];
                }
            }];
        };
        
        void (^ redirectToGroup)(WLTGroup *grp) = ^void (WLTGroup *grp) {
            
            WLTGroupDetailViewController *details = [self.rootController controllerWithIdentifier:@"WLTGroupDetailViewController"];
            details.hidesBottomBarWhenPushed = YES;
            details.currentGroup = grp;
            details.navBarColor = [[WLTColorPalette sharedPalette] colorForGroup:grp];
            [self.navigationController setViewControllers:@[self.navigationController.rootViewController, details] animated:YES];
        };
        
        [WLTNewGroupValidator validateForMembers:self.members isPrivate:makePrivateGroup
                                        isDirect:isDirectGrp presenter:self completion:^(NewGroupValidatorAction action, id userInfo) {
                                            
                                            if (action == NewGroupValidatorActionCreateNewGroup) {
                                                
                                                createNewGroup();
                                            }
                                            else if (action == NewGroupValidatorActionRedirect) {
                                                
                                                redirectToGroup(userInfo);
                                                [weakSelf endViewLoading];
                                            }else {
                                                [weakSelf endViewLoading];
                                            }
                                        }];
    }
}

-(void)setUpPrivacyCell:(WLTGroupTypeTableViewCell*)tcell forRowIndex:(NSInteger)row {
    
    if (row == 0) {
        
        [tcell setUpForPrivate:NO];
        
        if (!makePrivateGroup) {
            tcell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else {
            tcell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    else if (row == 1) {
        
        [tcell setUpForPrivate:YES];
        
        if (makePrivateGroup) {
            tcell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else {
            tcell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
}

-(NSString*)cellIdentifierForContact:(WLTContact*)contact {
    
    NSString *cellIndentifier = @"WLTEditableContactTableViewCell";
    if ([contact.mobile.number isEqualToString:currentUserMobileNumber]) {
        cellIndentifier = @"WLTAdminTableViewCell";
    }
    
    return cellIndentifier;
}

#pragma mark - UITableViewDelegate & UITableViewDataSource

-(NSInteger)sectionIndexForIdentifier:(GroupControllerSectionType)identifier {
    
    NSInteger secIndex = NSNotFound;
    if (identifier == GroupControllerSectionTypeInfo) {
        secIndex = 0;
    }
    else if (identifier == GroupControllerSectionTypeName) {
        if ([self canShowGroupName]) {
            secIndex = 1;
        }
    }
    
    return secIndex;
}

-(GroupControllerSectionType)identifierForSectionIndex:(NSInteger)secIndex {
    
    GroupControllerSectionType secType = GroupControllerSectionTypeNone;
    if (secIndex == 0) {
        
        secType = GroupControllerSectionTypeInfo;
    }else if (secIndex == 1) {
        
        if ([self canShowGroupName]) {
            secType = GroupControllerSectionTypeName;
        }else {
            secType = GroupControllerSectionTypeGroupType;
        }
    }
    else if (secIndex == 2) {
        
        if ([self canShowGroupName]) {
            
            secType = GroupControllerSectionTypeGroupType;
        }else if(self.members.count){
            
            secType = GroupControllerSectionTypeMembers;
        }
        else if([self userCanTakeActions]){
            
            secType = GroupControllerSectionTypeEditAction;
        }
    }else if (secIndex == 3) {
        
        if ([self canShowGroupName] && self.members.count) {
            
            secType = GroupControllerSectionTypeMembers;
        }else if ([self userCanTakeActions]) {
            
            secType = GroupControllerSectionTypeEditAction;
        }
    }
    else if (secIndex == 4) {
        
        if ([self userCanTakeActions]) {
            
            secType = GroupControllerSectionTypeEditAction;
        }
    }
    
    return secType;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger sections = 2;
    if ([self canShowGroupName]) {
        sections += 1;
    }
    if (self.members.count) {
        sections += 1;
    }
    if([self userCanTakeActions]) {
        sections += 1;
    }
    
    return sections;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    GroupControllerSectionType secType = [self identifierForSectionIndex:section];;
    NSInteger rows = 0;
    if (secType == GroupControllerSectionTypeInfo) {
        rows = 1;
    }
    else if (secType == GroupControllerSectionTypeName) {
        rows = 1;
    }
    else if (secType == GroupControllerSectionTypeMembers) {
        
        rows = self.members.count;
    }
    else if (secType == GroupControllerSectionTypeGroupType) {
        rows = 2;
    }
    
    return rows;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    GroupControllerSectionType retSection = [self identifierForSectionIndex:indexPath.section];
    if (retSection == GroupControllerSectionTypeInfo) {
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"WLTColorSelectionTableViewCell"];
    }
    else if (retSection == GroupControllerSectionTypeGroupType) {
        
        WLTGroupTypeTableViewCell *tcell = [tableView dequeueReusableCellWithIdentifier:@"WLTGroupTypeTableViewCell"];
        [self setUpPrivacyCell:tcell forRowIndex:indexPath.row];
        
        cell = tcell;
    }
    else if (retSection == GroupControllerSectionTypeMembers) {
        
        WLTContact *cont = self.members[indexPath.row];
        NSString *cellIndentifier = [self cellIdentifierForContact:cont];
        
        WLTContactTableViewCell *userCell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
        [userCell showInformation:cont];
        
        if ([userCell isKindOfClass:[WLTEditableContactTableViewCell class]]) {
            
            [((WLTEditableContactTableViewCell*)userCell) setDelegate:self];
        }
        
        cell = userCell;
    }
    else if (retSection == GroupControllerSectionTypeName) {
        
        WLTGroupNameTableViewCell *tcell = [tableView dequeueReusableCellWithIdentifier:@"WLTGroupNameTableViewCell"];
        tcell->nameTextField.delegate = self;
        
        groupNameTableCell = tcell;
        cell = tcell;
    }
    
    if (!cell) {
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"DefaultTableCell"];
        cell.textLabel.text = @"Member's";
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GroupControllerSectionType retSection = [self identifierForSectionIndex:indexPath.section];
    
    if (retSection == GroupControllerSectionTypeInfo) {
        
        WLTColorPickerViewController *contr = [self.rootController controllerWithIdentifier:@"WLTColorPickerViewController"];
        contr.navBarColor = self.navBarColor;
        contr.selectedColorID = self.selectedColorID;
        [self.navigationController pushViewController:contr animated:YES];
        
        __weak typeof(self) weakSelf = self;
        [contr setCompletionBlock:^(NSString *data){
            
            if ([data isKindOfClass:[NSString class]]) {
                
                [weakSelf showNewColorID:data];
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }
        }];
    }
    else if (retSection == GroupControllerSectionTypeGroupType) {
        
        WLTGroupTypeTableViewCell *tCell = [tableView cellForRowAtIndexPath:indexPath];
        BOOL newValue = tCell.isPrivateGroup;
        
        if (makePrivateGroup != newValue) {
            
            makePrivateGroup = newValue;
            [theTableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell isKindOfClass:[WLTColorSelectionTableViewCell class]]) {
        
        WLTColorSelectionTableViewCell *cellTy = (WLTColorSelectionTableViewCell*)cell;
        cellTy->selectedColorView.backgroundColor = [[WLTColorPalette sharedPalette] colorForID:self.selectedColorID];
    }
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    return 45;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    CGFloat height = 10;
    
    return height;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    CGFloat height = 0.1;
    GroupControllerSectionType retSection = [self identifierForSectionIndex:section];
    if (retSection != GroupControllerSectionTypeInfo) {
        
        height = 25;
    }
    
    return height;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *vHeader = nil;
    GroupControllerSectionType retSection = [self identifierForSectionIndex:section];
    if (retSection == GroupControllerSectionTypeName) {
        
        WLTTableHeaderView *header = [[WLTTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
        header.label.text = @"GROUP NAME";
        
        vHeader = header;
    }
    else if (retSection == GroupControllerSectionTypeGroupType) {
        
        WLTTableHeaderView *header = [[WLTTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
        header.label.text = @"GROUP DETAILS";
        
        vHeader = header;
    }
    else if (retSection == GroupControllerSectionTypeMembers) {
        
        WLTTableHeaderView *header = [[WLTTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
        header.label.text = @"MEMBERS";
        
        vHeader = header;
    }
    
    return vHeader;
}

#pragma mark - UITextField

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - WLTEditableContactTableViewCellDelegate

-(void)removeContactClickedFromCell:(WLTEditableContactTableViewCell *)cell {
    
    NSIndexPath *indexPath = [theTableView indexPathForCell:cell];
    
    if (self.members.count > indexPath.row) {
        
        NSInteger prevGrpNameIndex = [self sectionIndexForIdentifier:GroupControllerSectionTypeName];
        WLTContact *cont = self.members[indexPath.row];
        
        if ([cont.mobile.number isEqualToString:[cell.displayedUser completeNumber]]) {
            
            [self.members removeObjectAtIndex:indexPath.row];
            
            BOOL canShowGrpName = [self canShowGroupName];;
            if (!canShowGrpName && (prevGrpNameIndex != NSNotFound)) {
                
                [theTableView beginUpdates];
                [theTableView deleteSections:[NSIndexSet indexSetWithIndex:prevGrpNameIndex] withRowAnimation:UITableViewRowAnimationAutomatic];
                [theTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                [theTableView endUpdates];
            }else {
                
                [theTableView beginUpdates];
                [theTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                [theTableView endUpdates];
            }
        }
    }
}

@end
