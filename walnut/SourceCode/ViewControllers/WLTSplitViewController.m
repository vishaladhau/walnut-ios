//
//  WLTSplitViewController.m
//  walnut
//
//  Created by Abhinav Singh on 22/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTSplitViewController.h"
#import "WLTSplitView.h"
#import "WLTColorPalette.h"
#import "WLTSplitFooterView.h"
#import "WLTGroupsManager.h"

@interface WLTSplitViewController () <UITextFieldDelegate, WLTSplitFooterViewDelegate>{
    
    NSMapTable *stepperViewMapping;
    NSMapTable *textFieldMapping;
    
    CGFloat totalToDistribute;
    CGFloat perShareValue;
    CGFloat remaningAmount;
    
    NSCharacterSet *notAllowedCharacters;
    WLTSplitFooterView *remaningSplitView;
}

@property(nonatomic, assign) BOOL isEquallyDistributed;

@end

@implementation WLTSplitUsers

@end

@implementation WLTSplitViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.isEquallyDistributed = YES;
    
    notAllowedCharacters = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789."] invertedSet];
    
    stepperViewMapping = [NSMapTable weakToWeakObjectsMapTable];
    textFieldMapping = [NSMapTable weakToWeakObjectsMapTable];
    
    totalLabel.text = @"Total";
    totalLabel.font = [UIFont sfUITextMediumOfSize:18];
    totalLabel.textColor = [UIColor darkGrayTextColor];
    
    amountLabel.font = [UIFont sfUITextRegularOfSize:18];
    amountLabel.textColor = [UIColor oweColor];
    amountLabel.text = self.transaction.amount.displayRoundedCurrencyString;
    
    [headerView addBorderLineAtPosition:BorderLinePositionBottom];
    headerView.backgroundColor = [UIColor whiteColor];
    
    theScrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    theScrollView.bounces = YES;
    theScrollView.alwaysBounceVertical = YES;
    [theScrollView setContentInset:UIEdgeInsetsMake(10, 0, 0, 0)];
	
    remaningAmount = 0;
	totalToDistribute = self.transaction.amount.floatValue;
	
	if (self.newTransaction) {
        
        CGFloat totShares = (self.allMembers.count + self.removedMembers.count);
        perShareValue = (totalToDistribute/totShares);
        
		for ( WLTSplitUsers *mem in self.allMembers ) {
			mem.amount = @(perShareValue);
		}
    }else {
        
        CGFloat minValue = NSIntegerMax;
        for ( WLTSplitUsers *mem in self.allMembers ) {
            
            CGFloat value = mem.amount.floatValue;
            if ( (value > 0) && (value < minValue) ) {
                minValue = value;
            }
        }
        
        if (minValue > 0) {
            perShareValue = minValue;
        }else {
            perShareValue = 1;
        }
    }
	
    [self setTitle:@"Split" andSubtitle:[NSString stringWithFormat:@"%d Members", (int)self.allMembers.count]];
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat cellHeight = 85;
    
    UIView *lastAdded = nil;
    for ( WLTSplitUsers *mem in self.allMembers ) {
        
        WLTSplitView *view = [WLTSplitView initWithDefaultXib];
        [view->amountTextField addTarget:self action:@selector(textValueChanged:) forControlEvents:UIControlEventEditingChanged];
        [view->stepper addTarget:self action:@selector(stepperValueChanged:) forControlEvents:UIControlEventValueChanged];
        view->amountTextField.delegate = self;
		
		CGFloat userShare = (mem.amount.floatValue/perShareValue);
        view->stepper.value = userShare;
        view->amountTextField.text = mem.amount.displayRoundedString;
        
        view.translatesAutoresizingMaskIntoConstraints = NO;
        [theScrollView addSubview:view];
        
        [stepperViewMapping setObject:view forKey:view->stepper];
        [textFieldMapping setObject:view forKey:view->amountTextField];
        
        [view showDetailsOfMember:mem isGroupMember:YES];
        
        if (lastAdded) {
            
            NSDictionary *dict = NSDictionaryOfVariableBindings(view, lastAdded);
            [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view(==lastAdded)]" options:0 metrics:nil views:dict]];
            [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lastAdded]-0-[view(==lastAdded)]" options:0 metrics:nil views:dict]];
        }else {
            
            [view addBorderLineAtPosition:BorderLinePositionTop];
            
            NSDictionary *dict = NSDictionaryOfVariableBindings(view);
            [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-0-[view(==%f)]-0-|", width] options:0 metrics:nil views:dict]];
            [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-0-[view(==%f)]", cellHeight] options:0 metrics:nil views:dict]];
        }
        
        lastAdded = view;
    }
    
    for ( WLTSplitUsers *mem in self.removedMembers ) {
        
        WLTSplitView *view = [WLTSplitView initWithDefaultXib];
        view->stepper.value = 0;
        view->amountTextField.text = @(0).stringValue;
        
        view.translatesAutoresizingMaskIntoConstraints = NO;
        [theScrollView addSubview:view];
        
        [view showDetailsOfMember:mem isGroupMember:NO];
        
        if (lastAdded) {
            
            NSDictionary *dict = NSDictionaryOfVariableBindings(view, lastAdded);
            [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view(==lastAdded)]" options:0 metrics:nil views:dict]];
            [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lastAdded]-0-[view(==lastAdded)]" options:0 metrics:nil views:dict]];
        }else {
            
            [view addBorderLineAtPosition:BorderLinePositionTop];
            
            NSDictionary *dict = NSDictionaryOfVariableBindings(view);
            [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-0-[view(==%f)]-0-|", width] options:0 metrics:nil views:dict]];
            [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-0-[view(==%f)]", cellHeight] options:0 metrics:nil views:dict]];
        }
        
        lastAdded = view;
    }
    
    WLTSplitFooterView *footer = [WLTSplitFooterView initWithDefaultXib];
    footer.delegate = self;
    footer.translatesAutoresizingMaskIntoConstraints = NO;
    [theScrollView addSubview:footer];
    
    remaningSplitView = footer;
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(lastAdded, footer);
    
    [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lastAdded]-10-[footer]-0-|" options:0 metrics:nil views:dict]];
    [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[footer]-0-|" options:0 metrics:nil views:dict]];
    
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                 style:UIBarButtonItemStyleDone
                                                                target:self action:@selector(doneClicked:)];
    self.navigationItem.rightBarButtonItem = doneItem;
    
    [self changeRemaningAmoutText];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL canChange = YES;
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([newString rangeOfCharacterFromSet:notAllowedCharacters].location != NSNotFound) {
        canChange = NO;
    }
    
    return canChange;
}

-(void)doneClicked:(UIBarButtonItem*)item {
    
    if ([self isLoading]) {
        return;
    }
    
    BOOL canCont = YES;
    if (canCont) {
        
        if (self.isLoading) {
            return;
        }
        
        [self.view endEditing:YES];
        
        CGFloat totalAmt = self.transaction.amount.floatValue;
        CGFloat splitedAmt = 0;
        
        NSMutableArray *splitmems = [NSMutableArray new];
        NSArray *allViews = stepperViewMapping.objectEnumerator.allObjects;
        
        for ( WLTSplitView *usrView in allViews ) {
            
            GTLWalnutMSplit *split = [[GTLWalnutMSplit alloc] init];
            split.mobileNumber = [usrView.displayedMember.member completeNumber];
            
            if ([usrView isEqual:[allViews lastObject]]) {
                
                CGFloat rema = (totalAmt-splitedAmt);
                if (rema < 0) {
                    
                    GTLWalnutMSplit *lastSplit = [splitmems lastObject];
                    lastSplit.amount = @(lastSplit.amount.floatValue + rema).twoPlaceDecimalNumber;
                    
                    rema = 0;
                }
                
                split.amount = @(rema).twoPlaceDecimalNumber;
            }else {
                
                split.amount = @(usrView->amountTextField.text.floatValue).twoPlaceDecimalNumber;
                splitedAmt += split.amount.floatValue;
            }
            
            [splitmems addObject:split];
        }
        
        self.transaction.splits = splitmems;
        [self startLoading];
        
        __weak WLTSplitViewController *weakSelf = self;
        
        GTLQueryWalnut *walN = [GTLQueryWalnut queryForTransactionAddWithObject:self.transaction];
		if (self.newTransaction) {
            
			walN = [GTLQueryWalnut queryForTransactionAddWithObject:self.transaction];
		}else {
            
			self.transaction.updatedAt = @([[NSDate date] timeStamp]);
			walN = [GTLQueryWalnut queryForTransactionUpdateWithObject:self.transaction];
		}
		
        [[WLTNetworkManager sharedManager].walnutService executeQuery:walN completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
            
            if (error) {
                
                [weakSelf showAlertForError:error];
                [weakSelf endViewLoading];
            }else {
                
                [[WLTGroupsManager sharedManager] fetchDetailsOfGroup:weakSelf.group completion:^(WLTGroup *cGrp, NSError *error) {
                    
                    if (error) {
                        
                        [weakSelf showAlertForError:error];
                    }else {
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:NotifyGroupChanged object:weakSelf.group];
                        
                        [weakSelf trackEvent:GA_ACTION_SPLIT_ADDED label:[weakSelf screenName] andValue:weakSelf.transaction.amount];
                        
                        if (weakSelf.isEquallyDistributed) {
                            [weakSelf trackEvent:GA_ACTION_SPLIT_STATUS label:GA_LABEL_SPLIT_STATUS_EQUALLY andValue:weakSelf.transaction.amount];
                        }else {
                            [weakSelf trackEvent:GA_ACTION_SPLIT_STATUS label:GA_LABEL_SPLIT_STATUS_UNEQUALLY andValue:weakSelf.transaction.amount];
                        }
                        if (weakSelf.completionBlock) {
                            weakSelf.completionBlock(YES);
                        }
                    }
                    
                    [weakSelf endViewLoading];
                }];
            }
        }];
    }
}

-(void)splitFooterViewDistributeClicked:(WLTSplitFooterView*)view {
    
    if (remaningAmount != 0) {
        
        CGFloat shares = 0;
        for ( WLTSplitView *view in stepperViewMapping.objectEnumerator) {
            shares += view->stepper.value;
        }

        if (shares == 0) {
            
            for ( WLTSplitView *view in stepperViewMapping.objectEnumerator) {
                view->stepper.value = 1;
            }
            shares = self.allMembers.count;
        }
        CGFloat perShareDistribution = (remaningAmount/shares);
        
        for ( WLTSplitView *view in stepperViewMapping.objectEnumerator) {
            
            view->amountTextField.text = @(view->amountTextField.text.floatValue + (perShareDistribution * view->stepper.value)).displayRoundedString;
        }
        
        [self changePerShareValue];
        [self changeRemaningAmoutText];
    }
}

#pragma mark - Distribution

-(void)changeRemaningAmoutText {
    
    CGFloat adjusted = 0;
    for ( WLTSplitView *view in stepperViewMapping.objectEnumerator) {
        CGFloat tVal = (perShareValue * view->stepper.value);
        adjusted += tVal;
    }
    
    remaningAmount = (int)(totalToDistribute - adjusted);
    
    if (remaningAmount != 0) {
        
        [remaningSplitView setRemamingAmount:remaningAmount];
        remaningSplitView.hidden = NO;
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }else {
        
        remaningSplitView.hidden = YES;
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
}

#pragma mark - On Text value change

//Change pershare value to the minimum value of any text field other that 0 in this case of zero pershare value is 1.
-(void)changePerShareValue {
    
    CGFloat minValue = NSIntegerMax;
    for (WLTSplitView *view in stepperViewMapping.objectEnumerator) {
        
        CGFloat value = view->amountTextField.text.doubleValue;
        if ( (value > 0) && (value < minValue) ) {
            minValue = value;
        }
    }
    
    if (minValue > 0) {
        perShareValue = minValue;
    }else {
        perShareValue = 1;
    }
}

-(void)textValueChanged:(UITextField*)field {
    
    [self changePerShareValue];
    [self changeAllSteppersValue];
    
    [self changeRemaningAmoutText];
}

-(void)changeAllSteppersValue {
    
    for (WLTSplitView *view in stepperViewMapping.objectEnumerator) {
        view->stepper.value = (view->amountTextField.text.floatValue/perShareValue);
    }
}

#pragma mark - On UIStepper Value Change

-(void)stepperValueChanged:(WLTStepper*)stepper {
    
    for ( WLTSplitView *view in stepperViewMapping.objectEnumerator) {
        
        CGFloat tVal = (perShareValue * view->stepper.value);
        view->amountTextField.text = @(tVal).displayRoundedString;
    }
    
    [self changeRemaningAmoutText];
}

@end
