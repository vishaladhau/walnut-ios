//
//  WLTGroupDetailViewController.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 05/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTGroupDetailViewController.h"
#import "WLTGroupSettingsViewController.h"
#import "WLTColorPalette.h"
#import "WLTTransactionUser.h"
#import "WLTTransaction.h"
#import "WLTSplitTransactionTableViewCell.h"
#import "WLTServerCommentTableViewCell.h"
#import "WLTSplitCommentTableViewCell.h"
#import "WLTExpenseViewController.h"
#import "WLTSplitSettleTableViewCell.h"
#import "WLTSettleGroupTableViewCell.h"
#import "WLTContactsManager.h"
#import "WLTMobileNumber.h"
#import "WLTTransactionsListViewController.h"
#import "WLTSplitTransactionDetailsViewController.h"
#import "WLTSplitComment.h"
#import "WLTCombinedGroupPayments.h"
#import "WLTDetailsHeaderButton.h"
#import "WLTGroupMemberDuesViewController.h"
#import "WLTKeyValueAmountViewController.h"
#import "WLTGroupsManager.h"
#import "WLTGroupContactsViewController.h"

@interface WLTSettlePayUser : NSObject <User> {
    
}

@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) WLTMobileNumber *mNumber;

@end

@implementation WLTSettlePayUser

-(NSString *)completeNumber{
    return self.mNumber.number;
}

@end

@interface WLTGroupDetailViewController () <WLTEmptyStateViewDelegate, WLTSplitCommentTableViewCellDelegate>{
    
    NSArray *allTransactions;
    NSArray *withOutCommentTransactions;
    
    NSString *lastTransactionObjectUid;
    UITapGestureRecognizer *tapGesture;
}

@property(nonatomic, strong) NSArray *transactionsToShow;
@property(nonatomic, strong) NSString *justPostedCommentID;

@property(nonatomic, strong) NSString *loggedInUserMobile;

-(void)showDetailsOfTransaction:(WLTTransaction*)trans;
-(void)showDetailsOfPaymentTransaction:(WLTTransaction*)trans;
-(void)markAllMessagesRead;

@end

@interface WLTTransactionTableViewDelegate () {
    
}

@end

@implementation WLTTransactionTableViewDelegate

-(void)configureTableView:(UITableView*)theTableView {
    
    _theTableView = theTableView;
    
    theTableView.delegate = self;
    theTableView.dataSource = self;
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
    headerView.backgroundColor = [UIColor clearColor];
    theTableView.tableHeaderView = headerView;
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectZero];
    theTableView.tableFooterView = footer;
    theTableView.separatorInset = UIEdgeInsetsZero;
    theTableView.rowHeight = UITableViewAutomaticDimension;
    theTableView.separatorColor = [UIColor clearColor];
    [theTableView setContentInset:UIEdgeInsetsMake(10, 0, 10, 0)];
    
    [theTableView registerNib:[UINib nibWithNibName:@"WLTSplitTransactionTableViewCellLeft" bundle:nil] forCellReuseIdentifier:@"WLTSplitTransactionTableViewCellLeft"];
    [theTableView registerNib:[UINib nibWithNibName:@"WLTSplitCommentTableViewCellLeft" bundle:nil] forCellReuseIdentifier:@"WLTSplitCommentTableViewCellLeft"];
    [theTableView registerNib:[UINib nibWithNibName:@"WLTSplitTransactionTableViewCellRight" bundle:nil] forCellReuseIdentifier:@"WLTSplitTransactionTableViewCellRight"];
    [theTableView registerNib:[UINib nibWithNibName:@"WLTSplitCommentTableViewCellRight" bundle:nil] forCellReuseIdentifier:@"WLTSplitCommentTableViewCellRight"];
    
    [theTableView registerNib:[UINib nibWithNibName:@"WLTSplitSettleTableViewCell" bundle:nil] forCellReuseIdentifier:@"WLTSplitSettleTableViewCell"];
    [theTableView registerNib:[UINib nibWithNibName:@"WLTSettleGroupTableViewCell" bundle:nil] forCellReuseIdentifier:@"WLTSettleGroupTableViewCell"];
    [theTableView registerNib:[UINib nibWithNibName:@"WLTServerCommentTableViewCell" bundle:nil] forCellReuseIdentifier:@"WLTServerCommentTableViewCell"];
    
    [theTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"DefaultCommentTableViewCell"];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    WLTTransaction *trans = self.owner.transactionsToShow[(indexPath.row)];
    if ([trans.type isEqualToString:TransactionObjectTypeSplit]) {
        
        WLTSplitTransactionTableViewCell *tcell = nil;
        BOOL isRightSide = NO;
        if ([trans.owner.mobileString isEqualToString:self.owner.loggedInUserMobile]) {
            
            tcell = (WLTSplitTransactionTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"WLTSplitTransactionTableViewCellRight"];
            isRightSide = YES;
        }else {
            tcell = (WLTSplitTransactionTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"WLTSplitTransactionTableViewCellLeft"];
        }
        
        [tcell showDetailsOfTransaction:trans isRightAlingedXIB:isRightSide];
        
        cell = tcell;
    }
    else if ([trans.type isEqualToString:TransactionObjectServerComment]) {
        
        WLTServerCommentTableViewCell *tcell = (WLTServerCommentTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"WLTServerCommentTableViewCell"];
        [tcell showComment:trans.notes fullWidth:NO];
        
        cell = tcell;
    }
    else if ([trans.type isEqualToString:TransactionObjectSplitComment]) {
        
        WLTSplitCommentTableViewCell *tcell = nil;
        if ([trans.owner.mobileString isEqualToString:self.owner.loggedInUserMobile]) {
            
            tcell = (WLTSplitCommentTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"WLTSplitCommentTableViewCellRight"];
        }else {
            tcell = (WLTSplitCommentTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"WLTSplitCommentTableViewCellLeft"];
        }
        
        tcell.delegate = self.owner;
        [tcell showDetailsOfTransaction:(WLTSplitComment*)trans];
        
        cell = tcell;
    }
    else if ([trans.type isEqualToString:TransactionObjectSplitSettle] || [trans.type isEqualToString:TransactionObjectTypeSplitSettleIncomplete]) {
        
        WLTSplitSettleTableViewCell *tcell = (WLTSplitSettleTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"WLTSplitSettleTableViewCell"];
        [tcell showDetailsOfTransaction:trans];
        
        cell = tcell;
    }
    else if ([trans.type isEqualToString:TransactionObjectGroupSettled]) {
        
        WLTSettleGroupTableViewCell *tcell = (WLTSettleGroupTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"WLTSettleGroupTableViewCell"];
        cell = tcell;
    }
    else {
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"DefaultCommentTableViewCell"];
    }
    
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger count = self.owner.transactionsToShow.count;
    return count;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 125;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    WLTTransaction *trans = self.owner.transactionsToShow[(indexPath.row)];
    if ([trans.type isEqualToString:TransactionObjectTypeSplit]) {
        
        [self.owner showDetailsOfTransaction:trans];
    }
    else if ([trans.type isEqualToString:TransactionObjectSplitSettle] || [trans.type isEqualToString:TransactionObjectTypeSplitSettleIncomplete]) {
        
        [self.owner showDetailsOfPaymentTransaction:trans];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat topOffsetToDisplayBottom = (_theTableView.contentSize.height - _theTableView.height);
    topOffsetToDisplayBottom -= 40;
    
    BOOL atBottom = YES;
    if (_theTableView.contentOffset.y < topOffsetToDisplayBottom) {
        atBottom = NO;
    }
    
    if (atBottom) {
        
        if (self.owner) {
            if (!self.owner.unreadCountView.hidden) {
                [self.owner markAllMessagesRead];
            }
        }
    }
}

@end

@implementation WLTGroupDetailViewController

- (void)dealloc {
    
    [transactionsTableView removeObserver:self forKeyPath:@"contentSize" context:nil];
}

-(void)viewDidLoad {
    
    [super viewDidLoad];
        
    self.loggedInUserMobile = CURRENT_USER_MOB;
    
    somethingChanged = NO;
    viewDidAppeared = NO;
    
    transactionsTableView.backgroundColor = self.view.backgroundColor;
    transactionTableDelegate = [[WLTTransactionTableViewDelegate alloc] init];
    transactionTableDelegate.owner = self;
    [transactionTableDelegate configureTableView:transactionsTableView];
    
    UITapGestureRecognizer *tGest = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeKeyBoardTapped:)];
    tGest.numberOfTapsRequired = 1;
    tGest.numberOfTouchesRequired = 1;
    [transactionsTableView addGestureRecognizer:tGest];
    
    tapGesture = tGest;
    tapGesture.enabled = NO;
    
    UIButton *settingIcon = [UIButton buttonWithType:UIButtonTypeCustom];
    settingIcon.tintColor = [UIColor whiteColor];
    [settingIcon setImage:[[UIImage imageNamed:@"SettingsIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [settingIcon addTarget:self action:@selector(settingsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [settingIcon sizeToFit];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:settingIcon];
    self.navigationItem.rightBarButtonItem = rightItem;
    settingsBarButton = settingIcon;
    
    [headerView addBorderLineAtPosition:BorderLinePositionBottom];
    headerHeight.constant = 90;
    
    [headerView setBackgroundColor:self.navBarColor];
    
    WLTEmptyStateView *noTransV = [WLTEmptyStateView emptyStateViewWithStyle:WLTEmptyStateViewStyleDefault andDelegate:self];
    noTransV.userInteractionEnabled = NO;
    [noTransV showTitle:@"Welcome!" andSubtitle:@"Add your first split expense to this group." imageName:@"NoChatImage"];
    noTransV.translatesAutoresizingMaskIntoConstraints = NO;
    [transactionsBackView addSubview:noTransV];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(noTransV);
    
    [transactionsBackView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[noTransV]-0-|" options:0 metrics:nil views:dict]];
    [transactionsBackView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[noTransV]-55-|" options:0 metrics:nil views:dict]];
    
    noTransactionsView = noTransV;
    
    showingComments = YES;
    
    [transactionsBackView bringSubviewToFront:commentView];
    
    WLTDetailsHeaderButton *btnOne = [WLTDetailsHeaderButton initWithDefaultXib];
    [btnOne addTarget:self action:@selector(showUserShares:) forControlEvents:UIControlEventTouchUpInside];
    [btnOne setNormalImage:@"YourShareNormal" highlighted:@"YourShareSelected"];
    btnOne.translatesAutoresizingMaskIntoConstraints = NO;
    [headerView addSubview:btnOne];
    
    WLTDetailsHeaderButton *btnTwo = [WLTDetailsHeaderButton initWithDefaultXib];
    [btnTwo addTarget:self action:@selector(showUserContributions:) forControlEvents:UIControlEventTouchUpInside];
    [btnTwo setNormalImage:@"ContributionButtonNormal" highlighted:@"ContributionButtonSelected"];
    btnTwo.translatesAutoresizingMaskIntoConstraints = NO;
    [headerView addSubview:btnTwo];
    
    WLTDetailsHeaderButton *btnThree = [WLTDetailsHeaderButton initWithDefaultXib];
    [btnThree addTarget:self action:@selector(showUserDues:) forControlEvents:UIControlEventTouchUpInside];
    [btnThree setNormalImage:@"YouGetButtonNormal" highlighted:@"YouGetButtonSelected"];
    btnThree.translatesAutoresizingMaskIntoConstraints = NO;
    [headerView addSubview:btnThree];
    
    NSDictionary *dict3 = NSDictionaryOfVariableBindings(btnOne, btnTwo, btnThree);
    NSDictionary *metrics = @{@"middleWidth":@([UIScreen mainScreen].bounds.size.width*0.4)};
    
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[btnOne(==btnThree)]-0-[btnTwo(==middleWidth)]-0-[btnThree(==btnOne)]-0-|" options:0 metrics:metrics views:dict3]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[btnOne]-0-|" options:0 metrics:nil views:dict3]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[btnTwo]-0-|" options:0 metrics:nil views:dict3]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[btnThree]-0-|" options:0 metrics:nil views:dict3]];
    
    yourContView = btnTwo;
    yourDuesView = btnThree;
    yourShareView = btnOne;
    
    [self refreshTransactionData];
    
    WLTTransaction *tranLast = [self.transactionsToShow lastObject];
    lastTransactionObjectUid = tranLast.objID;
    
    [self checkAndChangeAmounts];
    
    [self refreshGroupData:nil];
    
    if (!self.currentGroup.isPrivate.boolValue) {
        
        [commentView setPlaceHolderText:@"Add Comment..."];
        commentView->theTextView.userInteractionEnabled = YES;
    }else {
        [commentView setPlaceHolderText:@"Comments are disabled"];
        commentView->theTextView.userInteractionEnabled = NO;
    }
    
    [commentView setColor:self.navBarColor];
    [commentView->sendButton addTarget:self action:@selector(sendButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [commentView->commentButton addTarget:self action:@selector(commentButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [commentView->addButton addTarget:self action:@selector(addTrasactoinClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(groupChanged:) name:NotifyGroupChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(groupDeleted:) name:NotifyGroupDeleted object:nil];
    
    UIRefreshControl *control = [[UIRefreshControl alloc] initWithFrame:CGRectZero];
    [control sizeToFit];
    control.tintColor = self.navBarColor;
    [control addTarget:self action:@selector(refreshGroupData:) forControlEvents:UIControlEventValueChanged];
    [transactionsTableView addSubview:control];
    
    [transactionsTableView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
}

-(void)removeKeyBoardTapped:(UITapGestureRecognizer*)tapGest {
    
    [self.view endEditing:YES];
}

-(void)markAllMessagesRead {
    
    WLTTransaction *tranLast = [allTransactions lastObject];
    self.currentGroup.lastDisplayTime = @([tranLast.sortingDate timeStamp]);
    
    self.unreadCountView.hidden = YES;
    
    if (self.currentGroup.unreadMessagesCount.integerValue > 0) {
        
        self.currentGroup.unreadMessagesCount = @(0);
        [[WLTDatabaseManager sharedManager] saveDataBase];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NotifyGroupUnreadCountChanged object:self.currentGroup];
    }
}

-(IBAction)showUnreadMessagesClicked:(UIButton*)btn {
    
    if (!showingComments) {
        
        [self commentButtonClicked:nil];
        [self scrollTransactionTableToLastRow:YES];
    }else {
        [self scrollTransactionTableToLastRow:YES];
    }
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    //Control is not already reloading;
    if ([keyPath isEqualToString:@"contentSize"]) {
        
        [self adjustTransactionTableHeaderHeight];
    }
}

-(void)viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];
    
    if (!viewDidAppeared) {
        
        [self scrollTransactionTableToLastRow:NO];
    }
}

-(void)adjustTransactionTableHeaderHeight {
    
    UIView *newHeader = transactionsTableView.tableHeaderView;
    CGFloat remaning = (transactionsTableView.frame.size.height - (transactionsTableView.contentSize.height-(newHeader.height-20)));
    
    if (remaning > 0) {
        if (newHeader.height != remaning) {
            newHeader.height = remaning;
            transactionsTableView.tableHeaderView = newHeader;
        }
    }else if (remaning < -1) {
        
        if (newHeader.height != 0) {
            newHeader.height = 0;
            transactionsTableView.tableHeaderView = newHeader;
        }
    }
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        viewDidAppeared = YES;
    });
    
    if (somethingChanged) {
        [self scrollTransactionTableToLastRow:animated];
    }
    
    somethingChanged = NO;
}

-(void)backClicked:(UIButton*)brn {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)scrollTransactionTableToLastRow:(BOOL)animated {
    
    if (self.transactionsToShow.count > 0) {
        
        [transactionsTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:(self.transactionsToShow.count-1) inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:animated];
    }
    
    [self markAllMessagesRead];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.navBarColor = [[WLTColorPalette sharedPalette] colorForID:self.currentGroup.colourID];
    [UIView animateWithDuration:0.25 animations:^{
        
        [self.navigationController.navigationBar setBarTintColor:self.navBarColor];
        [headerView setBackgroundColor:self.navBarColor];
    }];
}

-(void)refreshGroupData:(UIRefreshControl*)control {
    
    if (control) {
        [control beginRefreshing];
    }
    
    __weak WLTGroupDetailViewController *weakSelf = self;
    __weak WLTGroup *weakGroup = self.currentGroup;
    
    [[WLTGroupsManager sharedManager] fetchEveryChangeOfGroup:self.currentGroup completion:^(WLTGroup *grp, NSError *error) {
        
        if (!error) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:NotifyGroupChanged object:weakGroup];
        }else {
            
            if (![NSObject checkAndShowToastIfNotGroupMember:error]) {
                
                if (control) {
                    
                    [weakSelf showAlertForError:error];
                }
            }
        }
        
        [control endRefreshing];
        [weakSelf endViewLoading];
    }];
}

-(void)groupDeleted:(NSNotification*)notify {
    
    __weak WLTGroupDetailViewController *weakSelf = self;
    if ([weakSelf.currentGroup isDeleted]) {
        
        [weakSelf.navigationController popToRootViewControllerAnimated:YES];
    }
}

-(void)groupChanged:(NSNotification*)notify {
    
    WLTGroup *grp = notify.object;
    if (grp && [grp.groupUUID isEqualToString:self.currentGroup.groupUUID]) {
        
        [self checkAndChangeAmounts];
        [self refreshTransactionData];
    }
}

-(void)checkAndReAdjustScrollViewWithTableIsAtBottom:(BOOL)tableIsAtBottom {
    
    NSString *nowLast = [[self.transactionsToShow lastObject] objID];
    if (!lastTransactionObjectUid || ![lastTransactionObjectUid isEqualToString:nowLast]) {
        
        lastTransactionObjectUid = nowLast;
        if ([lastTransactionObjectUid isEqualToString:self.justPostedCommentID]) {
            
            self.justPostedCommentID = nil;
            
            [self scrollTransactionTableToLastRow:YES];
        }else if ([self.navigationController.topViewController isEqual:self]) {
            
            if (tableIsAtBottom) {
                [self scrollTransactionTableToLastRow:YES];
            }else {
                if (lastTransactionObjectUid) {
                    self.unreadCountView.hidden = NO;
                }else {
                    self.unreadCountView.hidden = YES;
                }
            }
        }
        else {
            
            somethingChanged = YES;
        }
    }
}

-(void)checkAndChangeAmounts {
    
    [self setMembersTitleForGroup:self.currentGroup checkDirect:YES];
    
    if (self.currentGroup.currentUserShare.floatValue > 0) {
        [yourShareView setTitle:self.currentGroup.currentUserShare.displayRoundedCurrencyString andSubTitle:@"YOUR SHARE"];
    }else {
        [yourShareView setTitle:NilAmountPlaceholder andSubTitle:@"YOUR SHARE"];
    }
    
    if (self.currentGroup.currentUserActContr.floatValue > 0) {
        [yourContView setTitle:self.currentGroup.currentUserActContr.displayRoundedCurrencyString andSubTitle:@"YOUR CONTRIBUTION"];
    }else {
        [yourContView setTitle:NilAmountPlaceholder andSubTitle:@"YOUR CONTRIBUTION"];
    }
    
    NSString *typeString = nil;
    NSNumber *amount = nil;
    NSString *normalImageName = nil;
    NSString *selectedImageName = nil;
    
    if ([self.currentGroup userTransactionState] == UserTransactionStateOwe) {
        
        typeString = @"YOU OWE";
        normalImageName = @"YouOweButtonNormal";
        selectedImageName = @"YouOweButtonSelected";
        amount = self.currentGroup.currentUserOwe;
        
    }else if ([self.currentGroup userTransactionState] == UserTransactionStateGet) {

        typeString = @"YOU GET";
        normalImageName = @"YouGetButtonNormal";
        selectedImageName = @"YouGetButtonSelected";
        amount = self.currentGroup.currentUserGet;
        
    }
    if (typeString) {
        
        [yourDuesView setTitle:amount.displayRoundedCurrencyString andSubTitle:typeString];
        [yourDuesView setNormalImage:normalImageName highlighted:selectedImageName];
    }else {
        
        BOOL newGroup = YES;
        for ( WLTTransaction *trans in self.currentGroup.transactions ) {
            if ([trans.type isEqualToString:TransactionObjectTypeSplit]) {
                newGroup = NO;
                break;
            }
        }
        
        if (!newGroup) {
            typeString = @"DUES SETTLED";
            normalImageName = @"SettledButtonNormal";
            selectedImageName = @"SettledButtonSelected";
        }
        
        if(!typeString) {
            typeString = @"--";
            normalImageName = @"SettledButtonNormal";
            selectedImageName = @"SettledButtonSelected";
        }
        
        [yourDuesView setTitle:@"" andSubTitle:typeString];
        [yourDuesView setNormalImage:normalImageName highlighted:selectedImageName];

    }
}

-(void)commentButtonClicked:(WLTImageButton*)btnComment {
    
    NSArray *transToShow = nil;
    showingComments = !showingComments;
    
    if (!showingComments) {
        
        transToShow = withOutCommentTransactions;
        [commentView->commentButton setImage:[UIImage imageNamed:@"DisableCommentIcon"]];
    }else {
        
        transToShow = allTransactions;
        [commentView->commentButton setImage:[UIImage imageNamed:@"CommentIcon"]];
    }
    
    if (!showingComments) {
        
        NSMutableArray *toRemoveRight = [NSMutableArray new];
        NSMutableArray *toRemoveLeft = [NSMutableArray new];
        
        BOOL leftTurn = NO;
        for ( WLTTransaction *trans in allTransactions ) {
            if ([trans.type isEqualToString:TransactionObjectSplitComment] || [trans.type isEqualToString:TransactionObjectServerComment]) {
                
                NSInteger index = [allTransactions indexOfObject:trans];
                if (index != NSNotFound) {
                    if (leftTurn) {
                        [toRemoveLeft addObject:[NSIndexPath indexPathForRow:index inSection:0]];
                    }else {
                        [toRemoveRight addObject:[NSIndexPath indexPathForRow:index inSection:0]];
                    }
                    
                    leftTurn = !leftTurn;
                }
            }
        }
        
        self.transactionsToShow = transToShow;
        
        if (toRemoveRight.count || toRemoveLeft.count) {
            
            [transactionsTableView beginUpdates];
            if (toRemoveRight.count) {
                [transactionsTableView deleteRowsAtIndexPaths:toRemoveRight withRowAnimation:UITableViewRowAnimationRight];
            }
            if (toRemoveLeft.count) {
                [transactionsTableView deleteRowsAtIndexPaths:toRemoveLeft withRowAnimation:UITableViewRowAnimationLeft];
            }
            [transactionsTableView endUpdates];
        }
        else {
            
            [transactionsTableView reloadData];
        }
    }
    else {
        
        NSMutableArray *toAddLeft = [NSMutableArray new];
        NSMutableArray *toAddRight = [NSMutableArray new];
        
        BOOL leftTurn = NO;
        
        for ( WLTTransaction *trans in allTransactions ) {
            if ([trans.type isEqualToString:TransactionObjectSplitComment] || [trans.type isEqualToString:TransactionObjectServerComment]) {
                
                NSInteger index = [allTransactions indexOfObject:trans];
                if (index != NSNotFound) {
                    if (leftTurn) {
                        [toAddLeft addObject:[NSIndexPath indexPathForRow:index inSection:0]];
                    }else {
                        [toAddRight addObject:[NSIndexPath indexPathForRow:index inSection:0]];
                    }
                    
                    leftTurn = !leftTurn;
                }
            }
        }
        
        self.transactionsToShow = transToShow;
        
        if (toAddLeft.count || toAddRight.count) {
            [transactionsTableView beginUpdates];
            if (toAddLeft.count) {
                [transactionsTableView insertRowsAtIndexPaths:toAddLeft withRowAnimation:UITableViewRowAnimationLeft];
            }
            if (toAddRight.count) {
                [transactionsTableView insertRowsAtIndexPaths:toAddRight withRowAnimation:UITableViewRowAnimationRight];
            }
            
            [transactionsTableView endUpdates];
        }else {
            
            [transactionsTableView reloadData];
        }
    }
}

-(void)addTrasactoinClicked:(id)btn {
    
    [self.view endEditing:YES];
    
    WLTExpenseViewController *cont = [self controllerWithIdentifier:@"WLTExpenseViewController"];
    cont.navBarColor = self.navBarColor;
    cont.group = self.currentGroup;
    [self.navigationController pushViewController:cont animated:YES];
    
    __weak WLTGroupDetailViewController *weakCont = self;
    
    [cont setCompletion:^(BOOL success){
        
        if (success) {
            
            NSArray *allConts = [weakCont.navigationController allControllersUpTo:weakCont];
            [weakCont.navigationController setViewControllers:allConts animated:YES];
        }
    }];
}

#pragma mark - Keyboard Handling

-(void)keyboardShown:(NSNotification*)notify {
    
    tapGesture.enabled = YES;
    
    CGRect rectStart = [notify.userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGRect rectEnd = [notify.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    NSTimeInterval interval = [notify.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    NSInteger typeCurve = [notify.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    rectStart = [self.view.window convertRect:rectStart toView:self.view];
    rectEnd = [self.view.window convertRect:rectEnd toView:self.view];
    
    CGFloat gap = (self.view.height - rectEnd.origin.y);
    
    bottomConstraint.constant = gap;
    [self.view setNeedsLayout];
    
    CGPoint currentOff = transactionsTableView.contentOffset;
    
    [UIView animateWithDuration:interval delay:0 options:typeCurve animations:^{
        
        [transactionsTableView setContentOffset:CGPointMake(currentOff.x, currentOff.y+gap)];
        [self.view layoutIfNeeded];
    } completion:nil];
}

-(void)keyboardHide:(NSNotification*)notify {
    
    tapGesture.enabled = NO;
    
    NSTimeInterval interval = [notify.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    NSInteger typeCurve = [notify.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    bottomConstraint.constant = 0;
    
    [self.view setNeedsLayout];
    [UIView animateWithDuration:interval delay:0 options:typeCurve animations:^{
        
        [self.view layoutIfNeeded];
    } completion:nil];
}

#pragma mark -

-(void)refreshTransactionData {
    
    NSMutableArray *withoutComments = [NSMutableArray new];
    
    allTransactions = [self.currentGroup.transactions.allObjects sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"sortingDate" ascending:YES]]];
    
    for ( WLTTransaction *trans in allTransactions ) {
        if (![trans.type isEqualToString:TransactionObjectSplitComment] && ![trans.type isEqualToString:TransactionObjectServerComment]) {
            [withoutComments addObject:trans];
        }
    }
    withOutCommentTransactions = withoutComments;
    
    if (showingComments) {
        self.transactionsToShow = allTransactions;
    }else {
        self.transactionsToShow = withOutCommentTransactions;
    }
    
    if (!allTransactions.count) {
        noTransactionsView.hidden = NO;
    }else {
        noTransactionsView.hidden = YES;
    }
    
    CGFloat topOffsetToDisplayBottom = (transactionsTableView.contentSize.height - transactionsTableView.height);
    topOffsetToDisplayBottom -= 20;
    
    BOOL atBottom = YES;
    if (transactionsTableView.contentOffset.y < topOffsetToDisplayBottom) {
        atBottom = NO;
    }
    
    [transactionsTableView reloadData];
    [self checkAndReAdjustScrollViewWithTableIsAtBottom:atBottom];
}

-(void)settingsClicked:(UIBarButtonItem*)item {
    
    WLTGroupSettingsViewController *settings = [[WLTGroupSettingsViewController alloc] init];
    settings.toEdit = self.currentGroup;
    settings.navBarColor = self.navBarColor;
    [self.navigationController pushViewController:settings animated:YES];
    
    __weak WLTGroupDetailViewController *weakSelf = self;
    [settings setCompletion:^(BOOL success){
        
        if (success) {
            
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
//    
//    if ([self.currentGroup.owner.mobileString isEqualToString:CURRENT_USER_MOB]) {
//        
//        WLTGroupContactsViewController *cont = [[WLTGroupContactsViewController alloc] initWithNibName:@"WLTContactsViewController" bundle:nil];
////        cont.currentGroup = self.currentGroup;
//        cont.navBarColor = self.navBarColor;
//        [self.navigationController pushViewController:cont animated:YES];
//    }else {
//        
//        WLTGroupSettingsViewController *settings = [[WLTGroupSettingsViewController alloc] init];
//        settings.toEdit = self.currentGroup;
//        settings.navBarColor = self.navBarColor;
//        [self.navigationController pushViewController:settings animated:YES];
//        
//        __weak WLTGroupDetailViewController *weakSelf = self;
//        [settings setCompletion:^(BOOL success){
//            
//            if (success) {
//                
//                [weakSelf.navigationController popViewControllerAnimated:YES];
//            }
//        }];
//    }
}

-(void)showDetailsOfTransaction:(WLTTransaction*)trans {
    
    WLTSplitTransactionDetailsViewController *cont = [self controllerWithIdentifier:@"WLTSplitTransactionDetailsViewController"];
    cont.navBarColor = self.navBarColor;
    cont.group = self.currentGroup;
    cont.transaction = trans;
    [self.navigationController pushViewController:cont animated:YES];
    
    __weak typeof(self) weakSelf = self;
    [cont setCompletion:^(){
        
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
}

#pragma mark - WLTSplitCommentTableViewCellDelegate

-(void)splitCommentCellRetryClicked:(WLTSplitCommentTableViewCell*)cell {
    
    if (cell.displayedComment.state == WLTSplitCommentStateFailed) {
        
        [self postCommentForTransaction:nil orCoreDataTransaction:cell.displayedComment];
    }
}

-(void)splitCommentCellDeleteClicked:(WLTSplitCommentTableViewCell*)cell {
    
    if (cell.displayedComment.state == WLTSplitCommentStateFailed) {
        
        [self.currentGroup removeTransactionsObject:cell.displayedComment];
        [cell.displayedComment.managedObjectContext deleteObject:cell.displayedComment];
        
        [self refreshTransactionData];
    }
    else if ( (cell.displayedComment.state == WLTSplitCommentStatePosted) && ([cell.displayedComment.owner.mobileString isEqualToString:CURRENT_USER_MOB]) ) {
        
        [self removeTransaction:cell.displayedComment];
    }
}

#pragma mark - Comment Actions

-(WLTSplitComment*)addCoreDataTransactionForCommentTransaction:(GTLWalnutMTransaction*)trans {
    
    WLTSplitComment *transaction = [[WLTDatabaseManager sharedManager] objectOfClassString:@"WLTSplitComment"];
    transaction.notes = trans.notes;
    transaction.txnID = trans.txnUuid;
    transaction.type = TransactionObjectSplitComment;
    transaction.objID = trans.objUuid;
    
    transaction.createdDate = [NSDate dateWithTimeIntervalSince1970:trans.creationDate.doubleValue];
    transaction.sortingDate = transaction.createdDate;
    
    transaction.owner = [WLTUser userWithMobile:trans.owner.mobileNumber andName:trans.owner.name];
    transaction.stateNum = @(WLTSplitCommentStatePosting);
    
    [self.currentGroup addTransactionsObject:transaction];
    
    self.justPostedCommentID = transaction.objID;
    
    [self refreshTransactionData];
    
    return transaction;
}

-(void)sendButtonClicked:(WLTImageButton*)btn {
    
    NSError *validationError = nil;
    if (!commentView.text.length) {
        
        validationError = [NSObject walnutErrorWithMessage:@"Please enter a valid comment!"];
    }
    
    if (!validationError) {
        
        long long dateStamp = [NSDate currentTimeStamp];
        
        GTLWalnutMTransaction *trans = [[GTLWalnutMTransaction alloc] init];
        trans.groupUuid = self.currentGroup.groupUUID;
        trans.objUuid = [WLTNetworkManager generateUniqueIdentifier];
        trans.notes = commentView->theTextView.text;
        trans.creationDate = @(dateStamp);
        trans.deviceUuid = [WLTDatabaseManager sharedManager].appSettings.deviceIdentifier;
        
        GTLWalnutMSplitUser *owner = [[GTLWalnutMSplitUser alloc] init];
        owner.name = [WLTDatabaseManager sharedManager].currentUser.name;
        owner.mobileNumber = CURRENT_USER_MOB;
        
        trans.owner = owner;
        
        commentView.text = nil;
        [commentView changeHeightForCurrentText];
        
        [self postCommentForTransaction:trans orCoreDataTransaction:nil];
    }
    else {
        
        [self showAlertForError:validationError];
    }
}

-(void)postCommentForTransaction:(GTLWalnutMTransaction*)trans orCoreDataTransaction:(WLTSplitComment*)comment{
    
    if (trans || comment) {
        
        if (!comment) {
            
            comment = [self addCoreDataTransactionForCommentTransaction:trans];
        }else if (!trans) {
            
            GTLWalnutMTransaction *transaction = [[GTLWalnutMTransaction alloc] init];
            transaction.groupUuid = self.currentGroup.groupUUID;
            transaction.objUuid = comment.objID;
            transaction.notes = comment.notes;
            transaction.creationDate = @([comment.createdDate timeStamp]);
            transaction.deviceUuid = [WLTDatabaseManager sharedManager].appSettings.deviceIdentifier;
            
            GTLWalnutMSplitUser *owner = [[GTLWalnutMSplitUser alloc] init];
            owner.name = [WLTDatabaseManager sharedManager].currentUser.name;
            owner.mobileNumber = CURRENT_USER_MOB;
            
            transaction.owner = owner;
            
            trans = transaction;
            
            comment.stateNum = @(WLTSplitCommentStatePosting);
            self.justPostedCommentID = transaction.objUuid;
            
            NSInteger index = [self.transactionsToShow indexOfObject:comment];
            if (index != NSNotFound) {
                [transactionsTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        }
        
        __weak WLTGroupDetailViewController *weakSelf = self;
        __weak WLTGroup *weakGroup = self.currentGroup;
        
        GTLQueryWalnut *grpDetail = [GTLQueryWalnut queryForCommentAddWithObject:trans];
        [[WLTNetworkManager sharedManager].walnutService executeQuery:grpDetail completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
            
            if (!error) {
                
                comment.stateNum = @(WLTSplitCommentStatePosted);
                
                [[WLTGroupsManager sharedManager] fetchDetailsOfGroup:weakGroup completion:^(WLTGroup *grp, NSError *error) {
                    
                    if (error) {
                        
                        [transactionsTableView reloadData];
                    }else {
                        
                        NSInteger count = commentView.text.length;
                        [weakSelf trackEvent:GA_ACTION_COMMENT_ADDED label:[weakSelf screenName] andValue:@(count)];
                        
                        [weakSelf markAllMessagesRead];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:NotifyGroupChanged object:weakGroup];
                    }
                    
                    [weakSelf endViewLoading];
                }];
            }else {
                
                if ([NSObject checkAndShowToastIfNotGroupMember:error]) {
                    
                    [weakSelf.currentGroup removeTransactionsObject:comment];
                    [comment.managedObjectContext deleteObject:comment];
                    
                    [weakSelf refreshTransactionData];
                }else {
                    
                    comment.stateNum = @(WLTSplitCommentStateFailed);
                    [transactionsTableView reloadData];
                }
                
                [weakSelf endViewLoading];
            }
        }];
    }
}

-(void)removeTransaction:(WLTSplitComment*)comment {
    
    GTLWalnutMTransaction *ttrans = [[GTLWalnutMTransaction alloc] init];
    ttrans.deleted = @(YES);
    ttrans.deviceUuid = [WLTDatabaseManager sharedManager].appSettings.deviceIdentifier;
    ttrans.txnUuid = comment.txnID;
    ttrans.objUuid = comment.objID;
    ttrans.groupUuid = self.currentGroup.groupUUID;
    
    GTLWalnutMSplitUser *owner = [[GTLWalnutMSplitUser alloc] init];
    owner.name = comment.owner.name;
    owner.mobileNumber = comment.owner.mobileString;
    ttrans.owner = owner;
    
    GTLQueryWalnut *deleteCall = [GTLQueryWalnut queryForCommentUpdateWithObject:ttrans];
    [self startLoading];
    
    __weak WLTGroup *weakGroup = self.currentGroup;
    __weak typeof(self) weakSelf = self;
    [[WLTNetworkManager sharedManager].walnutService executeQuery:deleteCall completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
        
        if (!error) {
            
            [[WLTGroupsManager sharedManager] fetchDetailsOfGroup:weakGroup completion:^(WLTGroup *grp, NSError *error) {
                
                if (error) {
                    
                    [weakSelf showAlertForError:error];
                }else {
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:NotifyGroupChanged object:weakGroup];
                }
                
                [weakSelf endViewLoading];
            }];
        }else {
            
            [weakSelf showAlertForError:error];
            [weakSelf endViewLoading];
        }
    }];
}

#pragma mark - Header Buttons Click

-(void)showUserShares:(WLTDetailsHeaderButton*)button {
    
    WLTKeyValueAmountViewController *controller = [self controllerWithIdentifier:@"WLTKeyValueAmountViewController"];
    controller.title = @"Your Share";
    controller.navBarColor = self.navBarColor;
    controller.dataArray = @[@{@"key":@"Total spends in the group", @"value":self.currentGroup.groupTotal.displayRoundedCurrencyString},
                             @{@"key":@"Your share", @"value":self.currentGroup.currentUserShare.displayRoundedCurrencyString},
                             ];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)showUserContributions:(WLTDetailsHeaderButton*)button {
    
    WLTKeyValueAmountViewController *controller = [self controllerWithIdentifier:@"WLTKeyValueAmountViewController"];
    controller.title = @"Your Contribution";
    controller.navBarColor = self.navBarColor;
    
    controller.dataArray = @[@{@"key":@"You Paid in Group", @"value":self.currentGroup.currentUserContr.displayRoundedCurrencyString},
                             @{@"key":@"You Sent", @"value":self.currentGroup.currentUserSent.displayRoundedCurrencyString},
                             @{@"key":@"You Received", @"value":self.currentGroup.currentUserReceived.displayRoundedCurrencyString},
                             ];
    
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)showUserDues:(WLTDetailsHeaderButton*)button {
    
    WLTGroupMemberDuesViewController *controller = [self controllerWithIdentifier:@"WLTGroupMemberDuesViewController"];
    controller.navBarColor = self.navBarColor;
    controller.currentGroup = self.currentGroup;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)showDetailsOfPaymentTransaction:(WLTTransaction*)trans {
    
    if (trans.isWpaySettled.boolValue && PaymentsEnabled() && ([trans.receiver.mobileString isEqualToString:self.loggedInUserMobile] || [trans.owner.mobileString isEqualToString:self.loggedInUserMobile])) {
        
        [self showDetailsOfTransactionID:trans.txnID];
    }
}

#pragma mark - WLTEmptyStateViewDelegate

-(void)emptyStateViewReloadScreen:(WLTEmptyStateView *)view {
    
    [self refreshGroupData:nil];
}

-(void)emptyStateViewActionButtonTapped:(WLTEmptyStateView *)view {
    
}

@end

