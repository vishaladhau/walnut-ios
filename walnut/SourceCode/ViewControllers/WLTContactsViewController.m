//
//  WLTContactsViewController.m
//  walnut
//
//  Created by Abhinav Singh on 01/09/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTContactsViewController.h"
#import "WLTContactsManager.h"
#import "WLTContact.h"
#import "WLTDatabaseManager.h"
#import "WLTUser.h"
#import "WLTTableHeaderView.h"
#import "WLTNumberSelectionView.h"
#import "WLTCombinedGroupPayments.h"
#import "WLTSendMoneyRequestViewController.h"
#import "WLTContactTableViewCell.h"

NSString *const InvalidNamePrefix = @"#";

@implementation WLTContactsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"Contacts";
    
    indexMappingDict = [NSMutableDictionary new];
    allContactIndexes = [NSMutableArray new];
    searchUnknownContactsMapping = [NSMutableDictionary new];
    
    theSearchBar.placeholder = @"Enter any phone number or name";
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectZero];
    theTableView.rowHeight = UITableViewAutomaticDimension;
    theTableView.tableFooterView = footer;
    theTableView.separatorInset = UIEdgeInsetsZero;
    [theTableView registerClass:[WLTContactTableViewCell class] forCellReuseIdentifier:@"WLTContactTableViewCell"];
    [theTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"DefaultTableCell"];
    
    searchQueue = [[NSOperationQueue alloc] init];
    [searchQueue setMaxConcurrentOperationCount:2];
    
    [self startLoading];
    
    __weak typeof(self) weakSelf = self;
    [[WLTContactsManager sharedManager] fetchAllContacts:^(NSMutableArray *result) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [weakSelf addNewContacts:result];
            [weakSelf endViewLoading];
        });
    }];
}

-(void)addNewContacts:(NSMutableArray*)result {
    
    NSSortDescriptor *sorting = [[NSSortDescriptor alloc] initWithKey:@"fullName" ascending:YES];
    [result sortUsingDescriptors:@[sorting]];
    self.contactsArray = result;
    
    [self reloadIndexs];
    [self resetSearch];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if ([scrollView isDragging]) {
        
        if ([theSearchBar isFirstResponder]) {
            [theSearchBar resignFirstResponder];
        }
    }
}

-(void)resetSearch {
    
    theSearchBar.text = nil;
    self.currentSearchText = nil;
    
    showingSearchResults = NO;
    
    [theTableView reloadData];
}

-(void)userSelectedContact:(WLTContact*)contact atIndexPath:(NSIndexPath*)indexPath{
    
}

#pragma mark - Indexing

-(void)reloadIndexs {
    
    [indexMappingDict removeAllObjects];
    [allContactIndexes removeAllObjects];
    
    for ( WLTContact *contact in self.contactsArray ) {
        
        NSString *dStr = contact.fullName;
        if (!dStr.length) {
            dStr = InvalidNamePrefix;
        }
        
        NSString *first = [[dStr substringToIndex:1] uppercaseString];
        
        NSMutableArray *contArr = indexMappingDict[first];
        if (!contArr) {
            
            contArr = [NSMutableArray new];
            indexMappingDict[first] = contArr;
            [allContactIndexes addObject:first];
        }
        
        [contArr addObject:contact];
    }
    
    [allContactIndexes sortUsingSelector:@selector(compare:)];
    
    [theTableView reloadData];
}

-(WLTContact*)contactAtIndexPath:(NSIndexPath*)path {
    
    WLTContact *cont = nil;
    ContactSectionIndex identifier = [self identifierForIndex:path.section];
    
    if (identifier == ContactSectionIndexContactUsers) {
        
        cont = indexMappingDict[allContactIndexes[path.section]][path.row];
    }
    else if (identifier == ContactSectionIndexSearchResults) {
        
        cont = self.searchResults[path.row];
    }else if (identifier == ContactSectionIndexContactNotFound) {
        
        NSString *comString = [self.currentSearchText mobileNumberWithCountryCode];
        cont = searchUnknownContactsMapping[comString];
        if (!cont) {
            
            cont = [[WLTContact alloc] initWithName:nil andNumber:comString];
            searchUnknownContactsMapping[comString] = cont;
        }
    }
    
    return cont;
}

#pragma mark - UITableViewDelegate & UITableViewDataSource

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    if (showingSearchResults) {
        return nil;
    }
    
    return allContactIndexes;
}

-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    
    if (showingSearchResults) {
        return 0;
    }
    
    return [allContactIndexes indexOfObject:title];
}

-(ContactSectionIndex)identifierForIndex:(NSInteger)secIndex {
    
    ContactSectionIndex identifier = ContactSectionIndexNone;
    if (secIndex == 0) {
        
        if (showingSearchResults && self.currentSearchText.isValidIndianMobile && !self.searchResults.count) {
            
            identifier = ContactSectionIndexContactNotFound;
        }
        else if (showingSearchResults) {
            
            identifier = ContactSectionIndexSearchResults;
        }
        else  {
            identifier = ContactSectionIndexContactUsers;
        }
    }else {
        
        identifier = ContactSectionIndexContactUsers;
    }
    
    return identifier;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger secs = 0;
    if (showingSearchResults) {
        
        secs = 1;
    }else {
        
        secs = allContactIndexes.count;
    }
    
    return secs;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger rowsCount = 0;
    ContactSectionIndex identifier = [self identifierForIndex:section];
    if (identifier == ContactSectionIndexContactUsers) {
        
        rowsCount = [indexMappingDict[allContactIndexes[section]] count];
    }else if (identifier == ContactSectionIndexSearchResults) {
        
        rowsCount = self.searchResults.count;
    }
    else if (identifier == ContactSectionIndexContactNotFound) {
        
        rowsCount = 1;
    }
    
    return rowsCount;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 70;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    id <User>contact = [self contactAtIndexPath:indexPath];
    
    static NSString *cellIndentifier = @"WLTContactTableViewCell";
    WLTContactTableViewCell *userCell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    [userCell showInformation:contact];
    
    return userCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    CGFloat height = 0;
    ContactSectionIndex identifier = [self identifierForIndex:section];
    if ( identifier == ContactSectionIndexContactNotFound ) {
        height = 35;
    }
    
    return height;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *vHeader = nil;
    ContactSectionIndex identifier = [self identifierForIndex:section];
    if ( identifier == ContactSectionIndexContactNotFound ) {
        
        WLTTableHeaderView *header = [[WLTTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
        header.label.text = @"NOT IN CONTACTS";
        
        vHeader = header;
    }
    
    return vHeader;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    WLTContact *contact = [self contactAtIndexPath:indexPath];
    if (contact) {
        [self userSelectedContact:contact atIndexPath:indexPath];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - SearchBar

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}

-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    
    [searchBar setShowsCancelButton:NO animated:YES];
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    [searchQueue cancelAllOperations];
    if (searchText.length) {
        
        self.currentSearchText = searchText;
        
        if (self.contactsArray.count) {
            
            WLTSearchOperation *search = [[WLTSearchOperation alloc] initWithDataArray:self.contactsArray andSearchString:searchText];
            search.identifier = @"Other";
            search.delegate = self;
            [searchQueue addOperation:search];
        }
    }else {
        
        [self resetSearch];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [theSearchBar resignFirstResponder];
}

-(void)searchCompleted:(WLTSearchOperation*)search forString:(NSString*)string withResults:(NSMutableArray*)results {
    
    if ([string isEqualToString:self.currentSearchText]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if ([search.identifier isEqualToString:@"Other"]) {
                self.searchResults = results;
            }
            
            if (!showingSearchResults) {
                showingSearchResults = YES;
            }
            
            [theTableView reloadData];
        });
    }
}

@end
