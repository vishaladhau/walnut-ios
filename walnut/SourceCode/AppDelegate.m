//
//  AppDelegate.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 02/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "AppDelegate.h"
#import "WLTDatabaseManager.h"
#import "WLTHomeIconButton.h"
#import "WLTNetworkManager.h"
#import "WLTRootViewController.h"

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <Google/CloudMessaging.h>
#import <Google/Analytics.h>
#import "Apsalar.h"

#import "WLTContact.h"

@import WebKit;

NSString *const SubscriptionTopic = @"/topics/global";

@interface AppDelegate () <GGLInstanceIDDelegate, GCMReceiverDelegate> {
    
}

@property(nonatomic, strong) void (^registrationHandler) (NSString *registrationToken, NSError *error);

@property(nonatomic, readonly, strong) NSString *gcmSenderID;

@property(nonatomic, assign) BOOL connectedToGCM;
@property(nonatomic, strong) NSString* registrationToken;
@property(nonatomic, assign) BOOL subscribedToTopic;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
//    WLTContact *cont = [[WLTContact alloc] initWithName:@"Harish 88-@:)1&:);@[]{}#^*+=_<~$" andNumber:@"+919123456788"];
//    NSLog(@"Name:%@", [NSString validCardNameForUser:cont]);
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setFont:[UIFont sfUITextRegularOfSize:14]];
    
    UILocalNotification *notify = launchOptions[UIApplicationLaunchOptionsLocalNotificationKey];
    if (notify) {
        
        [self.rootController handleRemoteNotification:notify.userInfo fetchHandler:nil];
    }
    
    [Fabric with:@[[Crashlytics class]]];
    [self setUpGoogle];
    
    [WLTTargetSettings setup];
    
    return YES;
}

-(NSMutableArray*)convertAllObjectsToMutableForArray:(NSArray*)array {
    
    NSMutableArray *newArray = [NSMutableArray new];
    for ( id objs in array  ) {
        
        if ([objs isKindOfClass:[NSDictionary class]]) {
            
            NSMutableDictionary *dictNew = [self convertAllObjectsToMutableForDictionary:objs];
            [newArray addObject:dictNew];
        }
        else if ([objs isKindOfClass:[NSArray class]]) {
            
            NSMutableArray *newSubArray = [self convertAllObjectsToMutableForArray:objs];
            [newArray addObject:newSubArray];
        }else {
            [newArray addObject:objs];
        }
    }
    
    return newArray;
}

-(NSMutableDictionary*)convertAllObjectsToMutableForDictionary:(NSDictionary*)dict {
    
    NSMutableDictionary *newDict = [dict mutableCopy];
    NSArray *allKeys = dict.allKeys;
    
    for ( NSString *key in allKeys ) {
        
        id obj = dict[key];
        if ([obj isKindOfClass:[NSDictionary class]]) {
            
            NSMutableDictionary *dictNew = [self convertAllObjectsToMutableForDictionary:obj];
            newDict[key] = dictNew;
        }else if ([obj isKindOfClass:[NSArray class]]) {
            
            NSMutableArray *newArray = [self convertAllObjectsToMutableForArray:obj];
            newDict[key] = newArray;
        }
    }
    
    return newDict;
}

-(void)setUpGoogle {
    
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    
    [self setupGooglePush];
    [self setupGoogleAnalytics];
}

-(void)setupGoogleAnalytics {
    
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = NO;
    if ([WLTTargetSettings isDebugVersion]) {
        gai.logger.logLevel = kGAILogLevelNone;
    }else {
        gai.logger.logLevel = kGAILogLevelNone;
    }
}

#pragma mark - Google Push
// [START register_for_remote_notifications]
- (void)setupGooglePush {
    
    _gcmSenderID = [[[GGLContext sharedInstance] configuration] gcmSenderID];
    // Register for remote notifications
    
    GGLInstanceIDConfig *instanceIDConfig = [GGLInstanceIDConfig defaultConfig];
    if ([WLTTargetSettings isDebugVersion]) {
        [instanceIDConfig setLogLevel:kGGLInstanceIDLogLevelDebug];
    }
    instanceIDConfig.delegate = self;
    [[GGLInstanceID sharedInstance] startWithConfig:instanceIDConfig];
    
    UIUserNotificationType allNotificationTypes = (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    GCMConfig *gcmConfig = [GCMConfig defaultConfig];
    gcmConfig.receiverDelegate = self;
    [[GCMService sharedInstance] startWithConfig:gcmConfig];
    
    __weak typeof(self) weakSelf = self;
    _registrationHandler = ^(NSString *registrationToken, NSError *error){
        if (registrationToken != nil) {
            
            weakSelf.registrationToken = registrationToken;
            [weakSelf subscribeToTopic];
            
            [WLTDatabaseManager sharedManager].appSettings.pushToken = registrationToken;
            [[WLTDatabaseManager sharedManager] saveDataBase];
            [[WLTNetworkManager sharedManager] postPushNotificationAgain];
        }
    };
}

- (void)subscribeToTopic {
    
    // If the app has a registration token and is connected to GCM, proceed to subscribe to the
    // topic
    if (_registrationToken && _connectedToGCM) {
        
        [[GCMPubSub sharedInstance] subscribeWithToken:_registrationToken
                                                 topic:SubscriptionTopic
                                               options:nil
                                               handler:^(NSError *error) {
                                                   if (error) {
                                                       // Treat the "already subscribed" error more gently
                                                       if (error.code == 3001) {
                                                           NSLog(@"Already subscribed to %@",
                                                                 SubscriptionTopic);
                                                       } else {
                                                           NSLog(@"Subscription failed: %@",
                                                                 error.localizedDescription);
                                                       }
                                                   } else {
                                                       self.subscribedToTopic = true;
                                                       NSLog(@"Subscribed to %@", SubscriptionTopic);
                                                   }
                                               }];
    }
}

// [START connect_gcm_service]
- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    [application setApplicationIconBadgeNumber:0];
    [Apsalar startSession:AppSalarApiKey withKey:AppSalarApiSecrect];
    
    // Connect to the GCM server to receive non-APNS notifications
    [[GCMService sharedInstance] connectWithHandler:^(NSError *error) {
        if (error) {
            NSLog(@"Could not connect to GCM: %@", error.localizedDescription);
        } else {
            _connectedToGCM = true;
            // [START_EXCLUDE]
            [self subscribeToTopic];
            // [END_EXCLUDE]
        }
    }];
}
// [END connect_gcm_service]

// [START disconnect_gcm_service]
- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    [Apsalar endSession];
    
    [[GCMService sharedInstance] disconnect];
    _connectedToGCM = NO;
}

-(void)registerAPNToken {
    
    NSData *pushData = [[NSUserDefaults standardUserDefaults] objectForKey:@"PushTokenData"];
    if (pushData.length) {
        
        NSDictionary *registrationOptions = @{kGGLInstanceIDRegisterAPNSOption:pushData,
                                              kGGLInstanceIDAPNSServerTypeSandboxOption:([WLTTargetSettings isDebugVersion] ? @YES : @NO)};
        
        [[GGLInstanceID sharedInstance] tokenWithAuthorizedEntity:_gcmSenderID
                                                            scope:kGGLInstanceIDScopeGCM
                                                          options:registrationOptions
                                                          handler:_registrationHandler];
    }
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:@"PushTokenData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [Apsalar registerDeviceTokenForUninstall:deviceToken];
    
    [self registerAPNToken];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {

    [(WLTRootViewController*)self.window.rootViewController handleRemoteNotification:userInfo fetchHandler:nil];
    
    // This works only if the app started the GCM service
    [[GCMService sharedInstance] appDidReceiveMessage:userInfo];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))handler {
    
    [(WLTRootViewController*)self.window.rootViewController handleRemoteNotification:userInfo fetchHandler:handler];
    // This works only if the app started the GCM service
    [[GCMService sharedInstance] appDidReceiveMessage:userInfo];
}

// [END ack_message_reception]

// [START on_token_refresh]
- (void)onTokenRefresh {
    
    // A rotation of the registration tokens is happening, so the app needs to request a new token.
    
    [self registerAPNToken];
}

// [END on_token_refresh]

// [START upstream_callbacks]
- (void)willSendDataMessageWithID:(NSString *)messageID error:(NSError *)error {
    
    if (error) {
        // Failed to send the message.
    } else {
        // Will send message, you can save the messageID to track the message
    }
}

- (void)didSendDataMessageWithID:(NSString *)messageID {
    
    // Did successfully send message identified by messageID
}
// [END upstream_callbacks]

- (void)didDeleteMessagesOnServer {
    
    // Some messages sent to this device were deleted on the GCM server before reception, likely
    // because the TTL expired. The client should notify the app server of this, so that the app
    // server can resend those messages.
}

#pragma mark - UILocalNotifications

- (void)application:(UIApplication *)app didReceiveLocalNotification:(UILocalNotification *)notif {
 
    [self.rootController handleRemoteNotification:notif.userInfo fetchHandler:nil];
    
//    if([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
//        [self.rootController handleLocalNotification:notif.userInfo showAlert:YES];
//    }else {
//        [self.rootController handleLocalNotification:notif.userInfo showAlert:NO];
//    }
}


@end
  
