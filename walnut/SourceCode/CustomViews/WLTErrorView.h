//
//  WLTErrorView.h
//  walnut
//
//  Created by Abhinav Singh on 06/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLTErrorView : UIView {
    
}

-(void)setErrorString:(NSString*)string;

@end
