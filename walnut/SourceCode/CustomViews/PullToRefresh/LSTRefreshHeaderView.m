//
//  LSTRefreshHeaderView.m
//  TableRefresh
//
//  Created by Abhinav Singh on 19/05/14.
//  Copyright (c) 2014 Leftshift Technologies Pvt. Ltd. All rights reserved.
//

#import "LSTRefreshHeaderView.h"

@implementation LSTRefreshHeaderView

- (void)dealloc {
    
}

- (instancetype)initWithHeight:(NSInteger)height andPosition:(RefreshControlPosition)position {
	
    self = [super initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, height)];
    if (self) {
		
		_position = position;
		
		self.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin;
		
		[self loadUI];
		
		_state = RefreshControlStateUnknown;
		self.state = RefreshControlStateInitial;
		
		titleStrings = [NSMutableDictionary new];
		removingObserver = NO;
    }
	
    return self;
}

- (void)removeFromTableView:(UIView*)table {

	if (removingObserver) {
		return;
	}
	
	removingObserver = YES;
	self.state = RefreshControlStateInitial;
	
	[table removeObserver:self forKeyPath:@"contentInset" context:nil];
	[table removeObserver:self forKeyPath:@"contentOffset" context:nil];
	[table removeObserver:self forKeyPath:@"contentSize" context:nil];
	
	_scrollView = nil;
	removingObserver = NO;
}

-(void)willMoveToSuperview:(UIView *)newSuperview {
	
	if (self.superview) {
		[self removeFromTableView:self.superview];
	}
	
	[super willMoveToSuperview:newSuperview];
}

-(void)didMoveToSuperview {
	
	[super didMoveToSuperview];
	if (self.superview && [self.superview isKindOfClass:[UIScrollView class]]) {
		
		UIScrollView *tableView = (UIScrollView*)self.superview;
		
		myContentInset = self.frame.size.height;
		
		if (self.position == RefreshControlPositionTop) {
			self.frame = CGRectMake(0, -self.frame.size.height, tableView.frame.size.width, self.frame.size.height);
		}
		else {
			self.frame = CGRectMake(0, tableView.contentSize.height, tableView.frame.size.width, self.frame.size.height);
		}
		
		self.reloadOffset = self.frame.size.height;
		
		tableTopInset = tableView.contentInset.top;
		tableBottomInset = tableView.contentInset.bottom;
		tableContentSize = tableView.contentSize;
		
		[tableView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
		[tableView addObserver:self forKeyPath:@"contentInset" options:NSKeyValueObservingOptionNew context:nil];
		[tableView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
        
		_scrollView = tableView;
	}
}

-(void)setReloadOffset:(NSInteger)reloadOffset {
	
	if (self.position == RefreshControlPositionTop) {
		_reloadOffset = -reloadOffset;
	}
	else {
		_reloadOffset = reloadOffset;
	}
}

- (void)endRefreshing {
	self.state = RefreshControlStateInitial;
}

- (void)startRefreshing {
	self.state = RefreshControlStateStarted;
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	
	//Control is not already reloading;
	if ([keyPath isEqualToString:@"contentOffset"]) {
        if (self.state != RefreshControlStateStarted) {
            
            if ((self.state == RefreshControlStateReadyToStart) && !_scrollView.isTracking ) {
                
                self.state = RefreshControlStateStarted;
            }
            else if (self.scrollView.isTracking) {
                
                CGPoint nowPoint = [[change objectForKey:@"new"] CGPointValue];
                
                if (self.position == RefreshControlPositionTop) {
                    
                    CGFloat draggedPart = -(nowPoint.y+tableTopInset);
                    CGFloat progress = (draggedPart/self.frame.size.height);
                    self.progress = progress;
                }else {
                    
                    CGFloat draggedPart = (nowPoint.y+tableTopInset+_scrollView.frame.size.height);
                    int visiblePart = (draggedPart - (self.reloadOffset+tableContentSize.height+_scrollView.contentInset.top));
                    visiblePart += self.frame.size.height;
                    CGFloat progress = (visiblePart/self.frame.size.height);
                    
                    self.progress = progress;
                }
                
                if (self.position == RefreshControlPositionTop) {
                    
                    nowPoint.y += tableTopInset;
                    if (nowPoint.y < self.reloadOffset) {
                        self.state = RefreshControlStateReadyToStart;
                    }else {
                        self.state = RefreshControlStateInitial;
                    }
                }else {
                    
                    NSInteger tableHeight = _scrollView.frame.size.height;
                    nowPoint.y += (tableHeight+tableTopInset);
                    
                    NSInteger lastPoint = (self.reloadOffset+tableContentSize.height+_scrollView.contentInset.top);
                    
                    if ( (nowPoint.y > lastPoint) && (tableContentSize.height > 0)) {
                        self.state = RefreshControlStateReadyToStart;
                    }else {
                        self.state = RefreshControlStateInitial;
                    }
                }
            }
        }
	}
	else if ([keyPath isEqualToString:@"contentInset"]) {
		
		tableTopInset = _scrollView.contentInset.top;
		tableBottomInset = _scrollView.contentInset.bottom;
		
		if (self.state == RefreshControlStateStarted) {
			
			if (self.position == RefreshControlPositionTop) {
				tableTopInset -= myContentInset;
			}
			else {
				tableBottomInset -= myContentInset;
			}
		}
	}
	else if ([keyPath isEqualToString:@"contentSize"]) {
		if (self.position == RefreshControlPositionBottom) {
			
			CGSize size = [[change objectForKey:@"new"] CGSizeValue];
			tableContentSize = size;
            
			if ( self.state != RefreshControlStateStarted) {
                
				self.frame = CGRectMake(0, tableContentSize.height, tableContentSize.width, self.frame.size.height);
			}
		}
	}
}

-(void)setState:(RefreshControlState)state {
	
	if (self.state != state) {
		
		_state = state;
		
		[self refreshTitle];
		
		if (self.stateChangeBlock) {
			self.stateChangeBlock(state);
		}
		[self stateChangedTo:self.state];
	}
}

- (void)refreshTitle {

//	id title = [titleStrings objectForKey:@(self.state)];
//	if ([title isKindOfClass:[NSAttributedString class]]) {
//		self.titleLabel.attributedText = title;
//	}
//	else if ([title isKindOfClass:[NSString class]]) {
//		self.titleLabel.text = title;
//	}
}

-(void)setStateChangeBlock:(StateChangeBlock)stateChangeBlock {
	
	if (stateChangeBlock) {
		stateChangeBlock(self.state);
	}
	
	_stateChangeBlock = stateChangeBlock;
}

#pragma mark - Methods Which can be Override in Subclass.

-(void)setProgress:(CGFloat)progress {
	
	_progress = progress;
}

- (void)stateChangedTo:(RefreshControlState)state {
	
	switch (state) {
		case RefreshControlStateInitial: {
			[self.indicator stopAnimating];
			[UIView animateWithDuration:0.3 animations:^{
				if (self.position == RefreshControlPositionTop) {
					_scrollView.contentInset = UIEdgeInsetsMake( tableTopInset, 0, tableBottomInset, 0);
				}else {
					_scrollView.contentInset = UIEdgeInsetsMake( tableTopInset, 0, tableBottomInset, 0);
				}
			} completion:nil];
		}
		break;
		case RefreshControlStateStarted: {
			
			[self.indicator startAnimating];
			
			[UIView animateWithDuration:0.3 animations:^{
				if (self.position == RefreshControlPositionTop) {
					_scrollView.contentInset = UIEdgeInsetsMake( tableTopInset+myContentInset, 0, 0, 0);
				}else {
					_scrollView.contentInset = UIEdgeInsetsMake( tableTopInset, 0, tableBottomInset+myContentInset, 0);
				}
			} completion:nil];
		}
		break;
		case RefreshControlStateReadyToStart: {
			break;
		}
		default: {
			NSLog(@"This Shouldn't happen!!!!");
		}
		break;
	}
}

- (void)setTitle:(id)title forSate:(RefreshControlState)controlState {
	
	[titleStrings setObject:title forKey:@(controlState)];
	[self refreshTitle];
}

- (void)loadUI {
	
	self.translatesAutoresizingMaskIntoConstraints = YES;
	
	UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	indicator.color = [UIColor rhinoColor];
	[indicator setHidesWhenStopped:YES];
	indicator.translatesAutoresizingMaskIntoConstraints = NO;
	[self addSubview:indicator];
	
	_indicator = indicator;
	
//	UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
//	titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
//	titleLabel.numberOfLines = 0;
//	titleLabel.backgroundColor = [UIColor clearColor];
//	titleLabel.textAlignment = NSTextAlignmentCenter;
//	titleLabel.font = [UIFont boldSystemFontOfSize:16];
//	titleLabel.textColor = [UIColor blackColor];
//	[self addSubview:titleLabel];
//	
//	_titleLabel = titleLabel;
//	
//	NSLayoutConstraint *hxL = [NSLayoutConstraint constraintWithItem:self.titleLabel
//														   attribute:NSLayoutAttributeCenterX
//														   relatedBy:NSLayoutRelationEqual
//															  toItem:self
//														   attribute:NSLayoutAttributeCenterX
//														  multiplier:1
//															constant:0];
//	
//	NSLayoutConstraint *vyL = [NSLayoutConstraint constraintWithItem:self.titleLabel
//														   attribute:NSLayoutAttributeCenterY
//														   relatedBy:NSLayoutRelationEqual
//															  toItem:self
//														   attribute:NSLayoutAttributeCenterY
//														  multiplier:1
//															constant:0];
	
	NSLayoutConstraint *vyI = [NSLayoutConstraint constraintWithItem:self.indicator
														   attribute:NSLayoutAttributeCenterY
														   relatedBy:NSLayoutRelationEqual
															  toItem:self
														   attribute:NSLayoutAttributeCenterY
														  multiplier:1
															constant:0];
    
    NSLayoutConstraint *vyX = [NSLayoutConstraint constraintWithItem:self.indicator
                                                           attribute:NSLayoutAttributeCenterX
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:self
                                                           attribute:NSLayoutAttributeCenterX
                                                          multiplier:1
                                                            constant:0];
	
	[self addConstraints:@[vyX,vyI]];
	
//	NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(, indicator);
//	
//	NSArray *gapConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"[indicator]-10-[titleLabel]"
//																	  options:0 metrics:nil views:viewsDictionary];
//	
//	[self addConstraints:gapConstraints];
}

@end
