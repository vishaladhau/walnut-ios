//
//  LSTRefreshHeaderView.h
//  TableRefresh
//
//  Created by Abhinav Singh on 19/05/14.
//  Copyright (c) 2015 Leftshift Technologies Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM (NSInteger, RefreshControlState) {
    
	RefreshControlStateUnknown = -1,
	RefreshControlStateInitial = 0,
	RefreshControlStateReadyToStart,
	RefreshControlStateStarted,
};

typedef NS_ENUM (NSInteger, RefreshControlPosition) {
    
	RefreshControlPositionTop = 0,
	RefreshControlPositionBottom,
};

typedef void (^ StateChangeBlock)(RefreshControlState state);

@interface LSTRefreshHeaderView : UIView {
	
	NSInteger tableTopInset;
	NSInteger tableBottomInset;
	
	NSInteger myContentInset;
	CGSize tableContentSize;
	
	NSMutableDictionary *titleStrings;
	BOOL removingObserver;
}

//@property(readonly, weak) UILabel *titleLabel;
@property(readonly, weak) UIActivityIndicatorView *indicator;

@property(readonly, weak) UIScrollView *scrollView;
@property(readonly, assign) RefreshControlPosition position;
@property(nonatomic, assign) CGFloat progress;

@property(nonatomic, assign) NSInteger reloadOffset;
@property(nonatomic, strong) StateChangeBlock stateChangeBlock;

@property(readonly, assign) RefreshControlState state;

- (instancetype)initWithHeight:(NSInteger)height andPosition:(RefreshControlPosition)position;
- (void)endRefreshing;
- (void)startRefreshing;

//Methods to override in subclass.
//Customize UI of your Refresh Control Here.
- (void)loadUI;
//Callback of state change.
- (void)stateChangedTo:(RefreshControlState)state;
- (void)setTitle:(NSString*)title forSate:(RefreshControlState)controlState;

@end
