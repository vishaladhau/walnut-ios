//
//  WLTUnreadCountLabel.m
//  walnut
//
//  Created by Abhinav Singh on 27/08/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTUnreadCountLabel.h"

@implementation WLTUnreadCountLabel

-(void)awakeFromNib {
    
    [super awakeFromNib];
    [self initialSetup];
}

-(void)initialSetup {
	
	allowedWidth = 25;
	
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = (allowedWidth/2.0);
	self.layer.masksToBounds = YES;
	
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
	label.minimumScaleFactor = 0.5;
	label.adjustsFontSizeToFitWidth = YES;
    [label setPreferredMaxLayoutWidth:allowedWidth];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont sfUITextBoldOfSize:12];
    [self addSubview:label];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(label);
	[self addConstraint:[NSLayoutConstraint constraintWithItem:label
													 attribute:NSLayoutAttributeCenterX
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeCenterX
													multiplier:1 constant:0]];
	
	[self addConstraint:[NSLayoutConstraint constraintWithItem:label
													 attribute:NSLayoutAttributeWidth
													 relatedBy:NSLayoutRelationLessThanOrEqual
														toItem:nil
													 attribute:NSLayoutAttributeWidth
													multiplier:1 constant:allowedWidth]];
	
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[label]-0-|" options:0 metrics:nil views:dict]];
    
    unreadLabel = label;
}

-(void)setTextColor:(UIColor *)color {
    
    unreadLabel.textColor =  color;
}

-(void)setUnreadCount:(NSInteger)count {
    
    NSString *newText = nil;
    if (count > 0) {
        if (count > 99) {
            newText = @"99+";
        }else {
            newText = [NSString stringWithFormat:@"%d", (int)count];
        }
    }
    
    unreadLabel.text = newText;
    [self invalidateIntrinsicContentSize];
}

-(CGSize)intrinsicContentSize {
    
    CGSize size = CGSizeMake(0, allowedWidth);
    if (unreadLabel.text.length) {
        size.width = allowedWidth;
    }
    
    return size;
}

@end
