//
//  WLTUserIconButton.m
//  walnut
//
//  Created by Abhinav Singh on 4/1/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTUserIconButton.h"
#import "WLTColorPalette.h"
#import "WLTDatabaseManager.h"
#import "WLTContactsManager.h"
#import "WLTRootViewController.h"

@import AddressBook;
@import ContactsUI;

@interface WLTUserIconButton () <CNContactViewControllerDelegate>

@end

@implementation WLTUserIconButton

-(void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithFrame:(CGRect)frame {
	
	self = [super initWithFrame:frame];
	if (self) {
		
		[self initialSetup];
	}
	
	return self;
}

-(void)awakeFromNib {
	
	[super awakeFromNib];
	[self initialSetup];
}

-(void)initialSetup {
	
	self.backgroundColor = [UIColor clearColor];
    
    [self addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
	UIView *backView = [[UIView alloc] initWithFrame:CGRectZero];
    backView.userInteractionEnabled = NO;
	backView.translatesAutoresizingMaskIntoConstraints = NO;
	[self addSubview:backView];
	
	UILabel *nameLbl = [[UILabel alloc] initWithFrame:CGRectZero];
	nameLbl.translatesAutoresizingMaskIntoConstraints = NO;
	nameLbl.textColor = [UIColor whiteColor];
	nameLbl.textAlignment = NSTextAlignmentCenter;
	nameLbl.font = [UIFont sfUITextMediumOfSize:15];
	[self addSubview:nameLbl];
	
	nameLabel = nameLbl;
	backgroundView = backView;
	
	NSDictionary *dict = NSDictionaryOfVariableBindings(backView, nameLabel);
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[nameLabel]-0-|" options:0 metrics:nil views:dict]];
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[nameLabel]-0-|" options:0 metrics:nil views:dict]];
	
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[backView]-0-|" options:0 metrics:nil views:dict]];
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[backView]-0-|" options:0 metrics:nil views:dict]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeUserName:) name:NotifyContactListChanged object:nil];
    
    buttonStyle = UserIconButtonStyleDefault;
}

-(void)setViewStyle:(UserIconButtonStyle)style {
    
    buttonStyle = style;
}

-(void)layoutSubviews {
	
	[super layoutSubviews];
	backgroundView.layer.cornerRadius = (self.width/2);
}

-(void)changeUserName:(NSNotification*)notify {
    
    if (displayedUser) {
        [self displayUserIcon:displayedUser];
    }
}

-(BOOL)checkContactPermission {
    
    BOOL havePremission = YES;
    ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
    if ( (status == kABAuthorizationStatusDenied) || (status == kABAuthorizationStatusRestricted)) {
        
        havePremission = NO;
        
        if (self.rootController.presentedViewController) {
            [self.rootController dismissViewControllerAnimated:NO completion:nil];
        }
        
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Access!"
                                                                            message:@"You have not provided permission to access contacts to walnut.\nGoto Settings to allow us!"
                                                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Not Now"
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 
                                                             }];
        
        UIAlertAction *actionChange = [UIAlertAction actionWithTitle:@"Settings"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 
                                                                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                             }];
        
        [controller addAction:actionCancel];
        [controller addAction:actionChange];
        
        [self.rootController presentViewController:controller animated:YES completion:nil];
    }
    
    return havePremission;
}

-(void)buttonTapped:(WLTUserIconButton*)btn {
    
    if ([self checkContactPermission]) {
        
        NSString *userString = [displayedUser completeNumber];
        
        if ([[WLTContactsManager sharedManager] userExistenceInContacts:displayedUser] == UserInContactsStatusOut && userString.length && ![userString isEqualToString:CURRENT_USER_MOB]) {
            
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
            
            CNContactStore *store = [[CNContactStore alloc] init];
            
            CNMutableContact *contact = [[CNMutableContact alloc] init];
            CNLabeledValue *homePhone = [CNLabeledValue labeledValueWithLabel:CNLabelHome value:[CNPhoneNumber phoneNumberWithStringValue:[displayedUser completeNumber]]];
            contact.phoneNumbers = @[homePhone];
            
            CNContactViewController *controller = [CNContactViewController viewControllerForUnknownContact:contact];
            UINavigationController *navCont = [[UINavigationController alloc] initWithRootViewController:controller];
            controller.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone
                                                                                          target:self action:@selector(cancelClicked:)];
            
            controller.contactStore = store;
            controller.delegate = self;
            
            [self.rootController presentViewController:navCont animated:YES completion:nil];
        }
    }
}

-(void)cancelClicked:(UIBarButtonItem*)item {
    
    [self.rootController dismissViewControllerAnimated:YES completion:nil];
}

-(void)displayUserIcon:(id<User>)user {
	
    displayedUser = user;
	nameLabel.text = @"...";
	
    UIColor *color = [[WLTColorPalette sharedPalette] colorForUser:displayedUser];
    if (buttonStyle == UserIconButtonStyleBordered) {
        
        backgroundView.backgroundColor = [UIColor whiteColor];
        backgroundView.layer.borderColor = color.CGColor;
        backgroundView.layer.borderWidth = 1;
        
        nameLabel.textColor = color;
    }else {
        
        backgroundView.backgroundColor = color;
        backgroundView.layer.borderWidth = 0;
        nameLabel.textColor = [UIColor whiteColor];
    }
    
	if ([[user completeNumber] isEqualToString:CURRENT_USER_MOB]) {
		
		nameLabel.text = [WLTDatabaseManager sharedManager].currentUser.name.shortForm;
	}else {
		
		__weak typeof(self) weakSelf = self;
		[[WLTContactsManager sharedManager] displayNameForUser:user completion:^(NSString *data) {
			
			if (weakSelf) {
                
                if (data.length) {
                    nameLabel.text = data.shortForm;
                }
			}
		}];
	}
}

#pragma mark CNContactViewControllerDelegate

- (void)contactViewController:(CNContactViewController *)viewController didCompleteWithContact:(nullable CNContact *)contact {
    
    [self.rootController dismissViewControllerAnimated:YES completion:nil];
    
    if (contact.givenName || contact.familyName) {
		
        [[WLTContactsManager sharedManager] refreshContactsList];
    }
}

@end
