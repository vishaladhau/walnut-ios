//
//  WLTGroupCollectionViewCell.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 04/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTGroupCollectionViewCell.h"
#import "WLTGroup.h"
#import "WLTColorPalette.h"
#import "WLTNetworkManager.h"
#import "WLTUser.h"
#import "WLTDatabaseManager.h"
#import "WLTContactsManager.h"
#import "WLTTransaction.h"
#import "WLTUnreadCountLabel.h"

@implementation WLTGroupCollectionViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    [self initialSetup];
}

-(void)initialSetup {
    
    self.backgroundColor = [UIColor clearColor];
    
    [backgroundImageView setImage:[[UIImage imageNamed:@"ShadowBackground"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    
    memCountLabel.textColor = [UIColor whiteColor];
    memCountLabel.font = [UIFont sfUITextMediumOfSize:12];
    memCountLabel.adjustsFontSizeToFitWidth = 0.4;
    memCountLabel.numberOfLines = 1;
    
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont sfUITextLightOfSize:30];
    
    oweOrGetLabel.font = [UIFont sfUITextMediumOfSize:16];
    oweOrGetLabel.textColor = [UIColor whiteColor];
    
    [addButton addTarget:self action:@selector(addClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(groupChanged:) name:NotifyGroupChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(groupUnreadCountChanged:) name:NotifyGroupUnreadCountChanged object:nil];
}

-(void)groupUnreadCountChanged:(NSNotification*)notify {
    
    WLTGroup *grp = notify.object;
    if (grp && [grp.groupUUID isEqualToString:self.displayedGroup.groupUUID]) {
        
        [unreadCountView setUnreadCount:grp.unreadMessagesCount.integerValue];
    }
}

-(void)groupChanged:(NSNotification*)notify {
    
    WLTGroup *grp = notify.object;
    if (grp && [grp.groupUUID isEqualToString:self.displayedGroup.groupUUID]) {
        
        [self showInfoOfGroup:grp];
    }
}

-(void)showInfoOfGroup:(WLTGroup*)group {
    
    UIColor *color = [[WLTColorPalette sharedPalette] colorForGroup:group];
    _displayedGroup = group;
    
    void (^ changeOweAndGetStates)(WLTGroup *grp) = ^void (WLTGroup *grp){
        
        NSString *displayStr = nil;
        NSString *titleStr = nil;
        
        switch ([grp userTransactionState]) {
            case UserTransactionStateGet: {
                
                displayStr = grp.currentUserGet.displayRoundedCurrencyString;
                titleStr = @"YOU GET";
            }
            break;
            case UserTransactionStateOwe: {
                
                displayStr = grp.currentUserOwe.displayRoundedCurrencyString;
                titleStr = @"YOU OWE";
            }
            break;
            default: {
            }
            break;
        }
        
        if (displayStr.length && titleStr.length) {
            
            NSString *completeOwe = [NSString stringWithFormat:@"%@\n%@",titleStr, displayStr];
            
            NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:completeOwe attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont sfUITextMediumOfSize:24]}];
            [attributed addAttributes:@{NSFontAttributeName:[UIFont sfUITextRegularOfSize:10]} range:[completeOwe rangeOfString:titleStr]];
            oweOrGetLabel.attributedText = attributed;
        }else {
            
            oweOrGetLabel.text = nil;
        }
    };
    
    backgroundImageView.tintColor = color;
    [unreadCountView setUnreadCount:group.unreadMessagesCount.integerValue];
    [unreadCountView setTextColor:color];
    
    if (group.unreadMessagesCount.integerValue) {
        [titleLabel setPreferredMaxLayoutWidth:([UIScreen mainScreen].bounds.size.width-(38+16+[unreadCountView intrinsicContentSize].width))];
    }else {
        [titleLabel setPreferredMaxLayoutWidth:([UIScreen mainScreen].bounds.size.width-(38+16))];
    }
    
    if (group.members.count == 2) {
        
        memCountLabel.text = @" ";
    }else {
        
        NSString *memCount = nil;
        if (group.members.count > 0) {
            
            if (group.members.count == 1) {
                memCount = [NSString stringWithFormat:@"%d Member", (int)group.members.count];
            }else {
                memCount = [NSString stringWithFormat:@"%d Members",(int)group.members.count];
            }
        }
        
        memCountLabel.text = memCount;
    }
    
    [titleLabel displayGroupName:group checkDirect:NO];
    if ( (group.currentUserGet.floatValue > 0) || (group.currentUserOwe.floatValue > 0) ) {
        
        changeOweAndGetStates(_displayedGroup);
    }else {
        
        BOOL newGroup = YES;
        for ( WLTTransaction *trans in group.transactions ) {
            if ([trans.type isEqualToString:TransactionObjectTypeSplit]) {
                newGroup = NO;
                break;
            }
        }
        
        if (!newGroup) {
            oweOrGetLabel.text = @"Dues Settled";
        }else {
            oweOrGetLabel.text = nil;
        }
    }
}

-(void)addClicked:(id)sender {
    
    [self.delegate addButtonClickedFromCell:self];
}

-(void)payClicked:(id)sender {
    
    [self.delegate payButtonClickedFromCell:self];
}

-(void)remindClicked:(id)sender {
    
    [self.delegate reminderButtonClickedFromCell:self];
}

@end
