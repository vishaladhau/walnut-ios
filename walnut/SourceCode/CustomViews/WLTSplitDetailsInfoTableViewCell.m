//
//  WLTSplitDetailsInfoTableViewCell.m
//  walnut
//
//  Created by Abhinav Singh on 03/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTSplitDetailsInfoTableViewCell.h"
#import "WLTTransaction.h"
#import "WLTContactsManager.h"
#import "WLTUser.h"

@implementation WLTSplitDetailsInfoTableViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    topBackView.backgroundColor = [UIColor whiteColor];
    [topBackView addBorderLineAtPosition:BorderLinePositionBottom];
    
    bottomBackView.backgroundColor = [UIColor fullMoonColor];
    
    paidByLabel.font = [UIFont sfUITextMediumOfSize:12];
    paidByLabel.textColor = [UIColor denimColor];
    
    splitNameLabel.textColor = [UIColor cadetColor];
    splitNameLabel.font = [UIFont sfUITextRegularOfSize:16];
    
    amountLabel.textColor = [UIColor getColor];
    amountLabel.font = [UIFont sfUITextRegularOfSize:16];
    
    addedByLabel.textColor = [UIColor cadetColor];
    addedByLabel.font = [UIFont sfUITextRegularOfSize:12];
    
    dateLabel.textColor = [UIColor cadetColor];
    dateLabel.font = [UIFont sfUITextRegularOfSize:10];
}

-(void)showDetailsOfTransaction:(WLTTransaction*)trans {
    
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    
    amountLabel.text = trans.amount.displayCurrencyString;
    [splitNameLabel setPreferredMaxLayoutWidth:(screenWidth - ([amountLabel intrinsicContentSize].width + 30))];
    
    splitNameLabel.text = trans.placeName;
    if (!trans.placeName.length) {
        splitNameLabel.text = trans.notes;
    }
    
    [[WLTContactsManager sharedManager] displayNameForUser:trans.owner defaultIsMob:YES completion:^(NSString *name) {
        paidByLabel.text = [NSString stringWithFormat:@"%@ Paid", name];
    }];
    
    [[WLTContactsManager sharedManager] displayNameForUser:trans.addedBy defaultIsMob:YES completion:^(NSString *name) {
        addedByLabel.text = [NSString stringWithFormat:@"Added By %@", name];
    }];
    
    dateLabel.text = trans.sortingDate.displayString;
}

@end
