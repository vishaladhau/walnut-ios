//
//  WLTBouncingButton.m
//  walnut
//
//  Created by Abhinav Singh on 02/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTBouncingButton.h"

@implementation WLTBouncingButton

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self initialSetup];
}

-(void)initialSetup {
    
    self.backgroundColor = [UIColor appleGreenColor];
    
    self.layer.shadowOpacity = 0.3;
    self.layer.shadowRadius = 2;
    self.layer.shadowOffset = CGSizeMake(0, 2);
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont sfUITextBoldOfSize:12];
    label.text = @"PAY";
    label.frame = self.bounds;
    label.translatesAutoresizingMaskIntoConstraints = YES;
    label.autoresizingMask = (UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight);
    [self addSubview:label];
    
    textLabel = label;
}

-(void)bounce {
    
    [UIView animateWithDuration:0.1 animations:^{
        
        self.transform = CGAffineTransformMakeScale(1.1, 1.1);
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.15 animations:^{
            
            self.transform = CGAffineTransformMakeScale(0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.18 animations:^{
                
                self.transform = CGAffineTransformIdentity;
            } completion:^(BOOL finished) {
                
            }];
        }];
    }];
}

@end
