//
//  WLTHomeHeaderButton.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 05/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTHomeHeaderButton.h"

@interface WLTHomeHeaderButton ()

@property(nonatomic, assign) UserTransactionState currentState;

@end

@implementation WLTHomeHeaderButton

-(void)initialSetup {
    
    LSStateColor *normalClr = [[LSStateColor alloc] initWithBack:[UIColor clearColor]
                                                    contentColor:nil
                                                    andBorderClr:nil];
    
    [self addColors:normalClr forState:LSButtonStateNormal];
    
    [super initialSetup];
}

-(void)addAllSubViews {
    
    [super addAllSubViews];
    
    titleLabel.numberOfLines = 0;
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.minimumScaleFactor = 0.5;
	
	self.layer.borderWidth = 0;
    self.layer.cornerRadius = DefaultCornerRadius;
}

-(void)setValue:(NSNumber*)value withState:(UserTransactionState)tstate {
    
    _currentState = tstate;
    
    NSString *valueString = nil;
    if (value.floatValue > 0) {
        valueString = value.displayRoundedCurrencyString;
    }else {
        valueString = @"--";
    }
    
    NSString *titleStr = @"UNKNOWN";
    UIColor *textColor = nil;
    
    if (tstate == UserTransactionStateOwe) {
        titleStr = @"YOU OWE";
        textColor = [UIColor oweColor];
    }
    else if (tstate == UserTransactionStateGet) {
        titleStr = @"YOU GET";
        textColor = [UIColor getColor];
    }
    
    NSString *complete = [NSString stringWithFormat:@"%@\n%@", titleStr, valueString];
    NSMutableAttributedString *attrbuted = [[NSMutableAttributedString alloc] initWithString:complete attributes:@{NSForegroundColorAttributeName:textColor, NSFontAttributeName:[UIFont sfUITextLightOfSize:24]}];
    [attrbuted addAttributes:@{NSForegroundColorAttributeName:[UIColor cadetColor], NSFontAttributeName:[UIFont sfUITextMediumOfSize:12]} range:[complete rangeOfString:titleStr]];
    
    titleLabel.attributedText = attrbuted;
}

- (void)changeDisplayForCurrentState {
    
    switch (state) {
        case LSButtonStateNormal: {
            self.backgroundColor = [UIColor clearColor];
        }
        break;
        case LSButtonStateHighlighted: {
			
            self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.1];
        }
            break;
        default:
            break;
    }
}

@end
