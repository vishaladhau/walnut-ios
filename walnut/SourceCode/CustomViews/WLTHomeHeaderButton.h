//
//  WLTHomeHeaderButton.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 05/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "LSButton.h"
#import "WLTGroup.h"

@interface WLTHomeHeaderButton : LSButton {
    
}

-(void)setValue:(NSNumber*)value withState:(UserTransactionState)state;

@end
