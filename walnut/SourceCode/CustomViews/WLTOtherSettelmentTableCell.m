//
//  WLTOtherSettelmentTableCell.m
//  walnut
//
//  Created by Abhinav Singh on 02/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTOtherSettelmentTableCell.h"
#import "WLTOtherSettlement.h"
#import "WLTUser.h"
#import "WLTContactsManager.h"
#import "WLTUserIconButton.h"

@implementation WLTOtherSettelmentTableCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self initialSetup];
    }
    
    return self;
}

-(void)initialSetup {
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    titleLabel.textColor = [UIColor cadetColor];
    titleLabel.font = [UIFont sfUITextRegularOfSize:16];
    titleLabel.numberOfLines = 0;
    [self.contentView addSubview:titleLabel];
    
    UILabel *subtitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    subtitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    subtitleLabel.textColor = [UIColor oweColor];
    subtitleLabel.font = [UIFont sfUITextRegularOfSize:19];
    subtitleLabel.adjustsFontSizeToFitWidth = YES;
    subtitleLabel.minimumScaleFactor = 0.5;
    subtitleLabel.numberOfLines = 1;
    [self.contentView addSubview:subtitleLabel];
    
    WLTUserIconButton *circle = [[WLTUserIconButton alloc] initWithFrame:CGRectZero];
    circle.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:circle];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(titleLabel, subtitleLabel, circle);
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[circle(==40)]-10-[titleLabel]-10-|" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[circle]-10-[subtitleLabel]-10-|" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[circle(==40)]" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[titleLabel]-0-[subtitleLabel]-10-|" options:0 metrics:nil views:dict]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:circle attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual
                                                                    toItem:titleLabel attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
    
    circleView = circle;
    _titleLabel = titleLabel;
    _subtitleLabel = subtitleLabel;
}

-(void)showDetailsOfOtherSettelment:(WLTOtherSettlement*)com{
    
    _displayedSettlement = com;
    [circleView displayUserIcon:com.fromUser];
    _subtitleLabel.text = com.amount.displayCurrencyString;
    
    __weak typeof(self) weakSelf = self;
    void (^ checkAndDisplay)( NSString *firstName, NSString *secondName ) = ^void (NSString *firstName, NSString *secondName){
        
        if (firstName.length && secondName.length) {
            
            if (com == weakSelf.displayedSettlement) {
                _titleLabel.text = [NSString stringWithFormat:@"%@ owes %@", firstName, secondName];
            }
        }
    };
    
    __block NSString *fName = nil;
    __block NSString *sName = nil;
    
    [[WLTContactsManager sharedManager] displayNameForUser:com.fromUser defaultIsMob:YES completion:^(NSString *name) {
        fName = name;
        checkAndDisplay(fName, sName);
    }];
    
    [[WLTContactsManager sharedManager] displayNameForUser:com.toUser defaultIsMob:YES completion:^(NSString *name) {
        sName = name;
        checkAndDisplay(fName, sName);
    }];
}

@end
