//
//  WLTSettleGroupTableViewCell.h
//  walnut
//
//  Created by Abhinav Singh on 09/03/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLTSettleGroupTableViewCell : UITableViewCell {
    
    __weak IBOutlet UILabel *infoLabel;
    __weak IBOutlet UIView *backView;
}

@end
