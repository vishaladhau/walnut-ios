//
//  WLTEditableContactTableViewCell.h
//  walnut
//
//  Created by Abhinav Singh on 03/09/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTContactTableViewCell.h"

@class WLTEditableContactTableViewCell;

@protocol WLTEditableContactTableViewCellDelegate <NSObject>

-(void)removeContactClickedFromCell:(WLTEditableContactTableViewCell*)cell;

@end

@interface WLTEditableContactTableViewCell : WLTContactTableViewCell {
    
    __weak UIButton *removeButton;
}

@property(nonatomic, weak) id <WLTEditableContactTableViewCellDelegate> delegate;

@end
