//
//  WLTSplitView.h
//  walnut
//
//  Created by Abhinav Singh on 22/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WLTStepper.h"

@interface WLTUnderlinedField: UITextField {
    
}

@end

@class WLTSplitUsers, WLTSplitView;

@interface WLTSplitView : UIControl {
    
    __weak IBOutlet UILabel *nameLabel;
    __weak IBOutlet UILabel *shareLabel;
    
    @public
    __weak IBOutlet WLTStepper *stepper;
    __weak IBOutlet UITextField *amountTextField;
}

@property(readonly, strong) WLTSplitUsers *displayedMember;

-(void)showDetailsOfMember:(WLTSplitUsers*)member isGroupMember:(BOOL)isMember;

@end
