//
//  WLTGroupMemberDuesTableViewCell.m
//  walnut
//
//  Created by Abhinav Singh on 30/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTGroupMemberDuesTableViewCell.h"
#import "WLTCombinedGroupPayments.h"
#import "WLTImageButton.h"
#import "WLTDatabaseManager.h"

@implementation WLTGroupMemberDuesTableViewCell

-(void)addAllSubviews {
    
    [super addAllSubviews];
    
    UIView *buttonContainer = [[UIView alloc] initWithFrame:CGRectZero];
    buttonContainer.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:buttonContainer];
    
    WLTImageButton *action = [[WLTImageButton alloc] initWithFrame:CGRectZero];
    [action addTarget:self action:@selector(actionClicked:) forControlEvents:UIControlEventTouchUpInside];
    action.translatesAutoresizingMaskIntoConstraints = NO;
    [buttonContainer addSubview:action];
    
    WLTImageButton *remindAction = [[WLTImageButton alloc] initWithFrame:CGRectZero];
    [remindAction addTarget:self action:@selector(remindClicked:) forControlEvents:UIControlEventTouchUpInside];
    remindAction.translatesAutoresizingMaskIntoConstraints = NO;
    [remindAction setWalnutRemindStyle];
    [buttonContainer addSubview:remindAction];
    
    remindButton = remindAction;
    actionButton = action;
    _accessory = buttonContainer;
    
    _titleTextLabel.font = [UIFont sfUITextRegularOfSize:16];
    _titleTextLabel.textColor = [UIColor cadetColor];
    
    _numberTextLabel.font = [UIFont sfUITextRegularOfSize:19];
}

-(void)addLayoutConstraints {
    
    [super addLayoutConstraints];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(actionButton, remindButton, _accessory);
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[remindButton(==40)]-10-[actionButton(==40)]-0-|" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[actionButton]-0-|" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[remindButton]-0-|" options:0 metrics:nil views:dict]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_accessory
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1 constant:0]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_accessory
                                                                 attribute:NSLayoutAttributeHeight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:nil
                                                                 attribute:NSLayoutAttributeNotAnAttribute
                                                                multiplier:1 constant:40]];
    
    NSLayoutConstraint *widthConstarint = [NSLayoutConstraint constraintWithItem:_accessory
                                                                       attribute:NSLayoutAttributeWidth
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:nil
                                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                                      multiplier:1 constant:90];
    [self.contentView addConstraint:widthConstarint];
    accessoryWidthConstraint = widthConstarint;
}

-(void)showDetailsOfPayment:(WLTCombinedGroupPayments*)com{
    
    _displayedTransaction = com;
    [self showInformation:com.user];
    
    _numberTextLabel.text = @(com.amount).displayCurrencyString;
    
    CGFloat sceenWidth = [[UIScreen mainScreen] bounds].size.width;
    
    if (com.transactionState == UserTransactionStateOwe) {
        
        remindButton.hidden = YES;
        if (PaymentsEnabled()) {
            
            accessoryWidthConstraint.constant = 40;
            [actionButton setWalnutPayStyle];
            actionButton.hidden = NO;
        }else {
            
            accessoryWidthConstraint.constant = 0;
            actionButton.hidden = YES;
        }
        
        _numberTextLabel.textColor = [UIColor oweColor];
    }else {
        
        
        actionButton.hidden = NO;
        
        if ([[WLTDatabaseManager sharedManager] doesCurrentUserHaveMultipleGroupTransactionsWithUser:com.user]) {
            
            accessoryWidthConstraint.constant = 40;
            remindButton.hidden = YES;
        }else {
            
            accessoryWidthConstraint.constant = 90;
            remindButton.hidden = NO;
        }
        
        actionButton.backgroundColor = [UIColor greenColor];
        [actionButton setWalnutSettleStyle];
        
        _numberTextLabel.textColor = [UIColor getColor];
    }
    
    [_titleTextLabel setPreferredMaxLayoutWidth:(sceenWidth-(64+accessoryWidthConstraint.constant))];
}

-(void)actionClicked:(WLTImageButton*)imgBtn {
    
    [self.delegate groupMemberDuesCellClickedActionButton:self];
}

-(void)remindClicked:(WLTImageButton*)imgBtn {
    
    [self.delegate groupMemberDuesCellClickedRemindButton:self];
}

@end
