//
//  WLTGroupTypeTableViewCell.m
//  walnut
//
//  Created by Abhinav Singh on 26/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTGroupTypeTableViewCell.h"

@implementation WLTGroupTypeTableViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    titleTextLabel.textColor = [UIColor cadetColor];
    titleTextLabel.font = [UIFont sfUITextRegularOfSize:16];
    
    subTitleTextLabel.textColor = [UIColor cadetColor];
    subTitleTextLabel.font = [UIFont sfUITextRegularOfSize:12];
    
    self.tintColor = [UIColor denimColor];
}

-(void)setUpForPrivate:(BOOL)privateGrp {
    
    _isPrivateGroup = privateGrp;
    if (privateGrp) {
        
        titleTextLabel.attributedText = [@"Private Group" attributtedStringAddingPrivateIconOfHeight:16];
        subTitleTextLabel.text = @"Split expenses privately, group member won’t see this group";
    }else {
        
        titleTextLabel.text = @"Shared Group";
        subTitleTextLabel.text = @"Group members can see this group";
    }
    
}

@end
