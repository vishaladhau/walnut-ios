//
//  WLTSplitTransactionTableViewCell.h
//  walnut
//
//  Created by Abhinav Singh on 01/03/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WLTUserIconButton.h"

@class WLTTransaction;

@interface WLTSplitTransactionTableViewCell : UITableViewCell {
    
    __weak IBOutlet UILabel *nameLabel;
    __weak IBOutlet UILabel *dateLabel;
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UILabel *totalLabel;
    __weak IBOutlet UILabel *myShareLabel;
    __weak IBOutlet UILabel *addedByLabel;
    
    __weak IBOutlet UIImageView *dottedImageView;
    
    __weak IBOutlet WLTUserIconButton *userNameIconView;
    
    __weak IBOutlet UIView *billBackView;
    
    __weak IBOutlet NSLayoutConstraint *leadingSpaceConstraint;
    __weak IBOutlet NSLayoutConstraint *tralingSpaceConstraint;
    __weak IBOutlet NSLayoutConstraint *topSpaceConstraint;
    __weak IBOutlet NSLayoutConstraint *bottomSpaceConstraint;
}

@property(nonatomic, readonly) WLTTransaction *transaction;

-(void)showDetailsOfTransaction:(WLTTransaction*)trans isRightAlingedXIB:(BOOL)rightSide;

@end
