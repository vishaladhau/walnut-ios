//
//  WLTOTPDotView.m
//  walnut
//
//  Created by Abhinav Singh on 03/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTOTPDotView.h"

@import CoreText;

@implementation WLTOTPDotView

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.layer.borderColor = [UIColor appleGreenColor].CGColor;
        self.layer.borderWidth = 2;
        
        contentColor = [UIColor appleGreenColor];
        
        awatingToFillup = NO;
        
        shapeLayer = [CAShapeLayer layer];
        shapeLayer.geometryFlipped = YES;
        shapeLayer.fillColor = contentColor.CGColor;
        [self.layer addSublayer:shapeLayer];
    }
    
    return self;
}

-(void)layoutSubviews {
    
    [super layoutSubviews];
    
    self.layer.cornerRadius = (self.width/2.0);
    shapeLayer.frame = self.layer.bounds;
}

-(void)animateFromEmptyToFilledUp {
    
    awatingToFillup = NO;
    
    UIBezierPath *fromPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake((self.width/2.0f)-1, (self.width/2.0f)-1, 2, 2)];
    
    CGPathRef toPath = [self pathFromSingleCharacter:self.text];
    CGRect rect = CGPathGetPathBoundingBox(toPath);
    rect.size.width += 1;
    
    CGAffineTransform tt = CGAffineTransformMakeTranslation(((self.width-(rect.size.width))/2.0)-1, ((self.height-rect.size.height)/2.0));
    
    toPath = CGPathCreateCopyByTransformingPath(toPath, &tt);
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"path"];
    animation.delegate = self;
    animation.fromValue = (__bridge id _Nullable)(fromPath.CGPath);
    animation.toValue = (__bridge id _Nullable)(toPath);
    animation.duration = 0.3;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    animation.fillMode = kCAFillModeForwards;
    [animation setValue:@"EmptyToFilledUp" forKey:@"AnimationIdentifier"];
    
    shapeLayer.path = toPath;
    
    [shapeLayer addAnimation:animation forKey:@"EmptyToFilledUp"];
}

-(void)animateFromFilledUpToEmpty {
    
    awatingToFillup = NO;
    
    UIBezierPath *toPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake((self.width/2.0f)-1, (self.width/2.0f)-1, 2, 2)];
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"path"];
    animation.delegate = self;
    animation.toValue = (__bridge id _Nullable)(toPath.CGPath);
    animation.fromValue = (__bridge id _Nullable)(shapeLayer.path);
    animation.duration = 0.3;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.fillMode = kCAFillModeForwards;
    
    shapeLayer.path = nil;
    
    [shapeLayer addAnimation:animation forKey:@"FilledUpToEmpty"];
}

- (void)animationDidStop:(CAAnimation *)animation finished:(BOOL)finished {
    
    NSString *identifier = [animation valueForKey:@"AnimationIdentifier"];
    if ([identifier isEqualToString:@"EmptyToFilledUp"]) {
        
        awatingToFillup = YES;
        [self performSelector:@selector(doFinalStepOfFilledUpAnimation) withObject:nil afterDelay:1];
    }
}

-(void)doFinalStepOfFilledUpAnimation {
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    if (awatingToFillup) {
        
        awatingToFillup = NO;
        
        UIBezierPath *toPath = [UIBezierPath bezierPathWithOvalInRect:self.bounds];
        shapeLayer.path = toPath.CGPath;
    }
}

//Refrence:https://github.com/aderussell/string-to-CGPathRef
-(CGPathRef)pathFromSingleCharacter:(NSString*)string {
    
    NSAttributedString *attributed = [[NSAttributedString alloc] initWithString:[string substringToIndex:1] attributes:@{NSFontAttributeName:[UIFont sfUITextBoldOfSize:16], NSForegroundColorAttributeName:contentColor}];
    
    CTLineRef line = CTLineCreateWithAttributedString((__bridge CFAttributedStringRef)attributed);
    
    CFArrayRef runArray = CTLineGetGlyphRuns(line);
    
    // Get FONT for this run
    CTRunRef run = (CTRunRef)CFArrayGetValueAtIndex(runArray, 0);
    CTFontRef runFont = CFDictionaryGetValue(CTRunGetAttributes(run), kCTFontAttributeName);
    
    // get Glyph & Glyph-data
    CFRange thisGlyphRange = CFRangeMake(0, 1);
    CGGlyph glyph;
    CGPoint position;
    CTRunGetGlyphs(run, thisGlyphRange, &glyph);
    CTRunGetPositions(run, thisGlyphRange, &position);
    
    CGPathRef letter = CTFontCreatePathForGlyph(runFont, glyph, NULL);
    
    return letter;
}

@end
