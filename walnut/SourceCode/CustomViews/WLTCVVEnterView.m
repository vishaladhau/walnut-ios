//
//  WLTCVVEnterView.m
//  walnut
//
//  Created by Abhinav Singh on 03/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTCVVEnterView.h"

@implementation WLTCVVEnterView

-(void)awakeFromNib {
    
    [super awakeFromNib];
    [self initialSetup];
}

-(void)initialSetup {
    
    self.backgroundColor = [UIColor clearColor];
    
    cvvLabelField.userInteractionEnabled = NO;
    cvvLabelField.text = @"XXX";
    cvvLabelField.font = [UIFont ocrFontOfSize:12];
    cvvLabelField.textColor = [UIColor colorWithRed:0.91 green:0.28 blue:0.36 alpha:1.00];
    
    identifierLabel.textColor = [UIColor mediumGrayTextColor];
    identifierLabel.font = [UIFont ocrFontOfSize:12];
}

@end
