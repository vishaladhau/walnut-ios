//
//  WLTCommentView.m
//  walnut
//
//  Created by Abhinav Singh on 01/03/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTCommentView.h"
#import "WLTImageButton.h"

@interface WLTCommentView () {
   
    NSCharacterSet *trimCharacters;
}

@end

@implementation WLTCommentView

-(void)initialSetup {
    
    [super initialSetup];
    
    trimCharacters = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    WLTImageButton *cmntButton = [[WLTImageButton alloc] initWithFrame:CGRectZero];
    [cmntButton setImage:[UIImage imageNamed:@"CommentIcon"]];
    [self addSubview:cmntButton];
    
    WLTImageButton *sendBtn = [[WLTImageButton alloc] initWithFrame:CGRectZero];
    [sendBtn setImage:[UIImage imageNamed:@"PostCommentIcon"]];
    [sendBtn setColorWithBackground:[UIColor deepSkyBlueColor]];
    [self addSubview:sendBtn];
    
    WLTImageButton *addBtn = [[WLTImageButton alloc] initWithFrame:CGRectZero];
    [addBtn setImage:[UIImage imageNamed:@"AddIcon"]];
    [self addSubview:addBtn];
    
    addButton = addBtn;
    sendButton = sendBtn;
    commentButton = cmntButton;
    
    theTextView.backgroundColor = [UIColor lightBackgroundColor];
    theTextView.layer.cornerRadius = DefaultCornerRadius;
    
    self.backgroundColor = [UIColor whiteColor];
    placeHolderLabel.font = theTextView.font;
    
    prevHeight = 0;
    [self addBorderLineAtPosition:BorderLinePositionTop];
    
    sendButton.hidden = YES;
}

-(void)setText:(NSString*)text {
    
    NSString *newText = [text stringByTrimmingCharactersInSet:trimCharacters];
    [super setText:newText];
    if (newText.length > 0) {
        
        sendButton.hidden = NO;
        addButton.hidden = YES;
    }else {
        
        sendButton.hidden = YES;
        addButton.hidden = NO;
    }
}

-(NSString *)text {
    
    NSString *txt = [super text];
    txt = [txt stringByTrimmingCharactersInSet:trimCharacters];
    return txt;
}

-(void)setColor:(UIColor*)colour {
    
    [commentButton setColorWithBackground:colour];
    [addButton setColorWithBackground:colour];
}

-(void)layoutSubviews {
    
    [super layoutSubviews];
    
    commentButton.frame = CGRectMake(10, (self.height-32)/2, 32, 32);
    sendButton.frame = CGRectMake(self.width-42, commentButton.originY, 32, 32);
    addButton.frame = sendButton.frame;
    
    theTextView.frame = CGRectMake(52, 10, self.width-104, self.height-20);
    placeHolderLabel.frame = CGRectMake(58, 15, self.width-110, self.height-30);
}

-(void)setPlaceHolderText:(NSString *)placeHolderText {
    
    [super setPlaceHolderText:placeHolderText];
    [self invalidateIntrinsicContentSize];
}

-(CGSize)intrinsicContentSize {
    
    CGSize size = [theTextView sizeThatFits:CGSizeMake(theTextView.width, 100)];
    size.height += 20;
    
    if (size.height < 52) {
        size.height = 52;
    }
    else if (size.height > 120) {
        size.height = 120;
    }
    
    prevHeight = size.height;
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, size.height);
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    BOOL canChange = [super textView:textView shouldChangeTextInRange:range replacementText:text];
    if (canChange) {
        
        NSString *newText = [textView.text stringByReplacingCharactersInRange:range withString:text];
        newText = [newText stringByTrimmingCharactersInSet:trimCharacters];
        if (newText.length > 0) {
            
            sendButton.hidden = NO;
            addButton.hidden = YES;
        }else {
            
            sendButton.hidden = YES;
            addButton.hidden = NO;
        }
    }
    
    return canChange;
}

-(void)textViewDidChange:(UITextView *)textView {
	
	dispatch_async(dispatch_get_main_queue(), ^{
		
		[self changeHeightForCurrentText];
		[theTextView scrollRectToVisible:CGRectMake(0, theTextView.height, theTextView.width, 10) animated:NO];
	});
}

-(void)changeHeightForCurrentText {
    
    CGSize newSize = [theTextView sizeThatFits:CGSizeMake(theTextView.width, 100)];
    if(newSize.height != prevHeight) {
        
        [self invalidateIntrinsicContentSize];
        [self.superview layoutIfNeeded];
    }
}

@end
