//
//  WLTHomeFooterButton.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 07/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTHomeFooterButton.h"

@implementation WLTHomeFooterButton

-(void)initialSetup {
    
    LSStateColor *normalClr = [[LSStateColor alloc] initWithBack:[UIColor appleGreenColor]
                                                    contentColor:[UIColor whiteColor]
                                                    andBorderClr:nil];
    [stateColors setObject:normalClr forKey:@(LSButtonStateNormal)];
    
    LSStateColor *highClr = [[LSStateColor alloc] initWithBack:[UIColor whiteColor]
                                                  contentColor:[UIColor appleGreenColor]
                                                  andBorderClr:[UIColor appleGreenColor]];
    [stateColors setObject:highClr forKey:@(LSButtonStateHighlighted)];
    
    [super initialSetup];
    
    [self setTitle:@"ADD GROUP"];
    titleLabel.font = [UIFont sfUITextMediumOfSize:16];
}

@end
