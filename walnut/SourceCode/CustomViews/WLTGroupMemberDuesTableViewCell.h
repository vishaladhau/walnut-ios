//
//  WLTGroupMemberDuesTableViewCell.h
//  walnut
//
//  Created by Abhinav Singh on 30/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTContactTableViewCell.h"

@class WLTTransactionUser, WLTCombinedGroupPayments, WLTImageButton, WLTGroupMemberDuesTableViewCell;

@protocol WLTGroupMemberDuesTableViewCellDelegate

-(void)groupMemberDuesCellClickedActionButton:(WLTGroupMemberDuesTableViewCell*)cell;
-(void)groupMemberDuesCellClickedRemindButton:(WLTGroupMemberDuesTableViewCell*)cell;

@end

@interface WLTGroupMemberDuesTableViewCell : WLTContactTableViewCell {
    
    __weak NSLayoutConstraint *accessoryWidthConstraint;
    
    __weak WLTImageButton *actionButton;
    __weak WLTImageButton *remindButton;
}

@property(readonly, strong) WLTCombinedGroupPayments *displayedTransaction;
@property(nonatomic, weak) id <WLTGroupMemberDuesTableViewCellDelegate> delegate;

-(void)showDetailsOfPayment:(WLTCombinedGroupPayments*)com;

@end