//
//  WLTPaymentInfoTableViewCell.m
//  walnut
//
//  Created by Abhinav Singh on 09/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTPaymentInfoTableViewCell.h"
#import "WLTPaymentTransaction.h"
#import "WLTInstrumentsManager.h"
#import "GTLWalnutMPaymentInstrument.h"

@implementation WLTPaymentInfoTableViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    infoLabel.textColor = [UIColor cadetColor];
    infoLabel.font = [UIFont sfUITextRegularOfSize:12];
    [infoLabel setPreferredMaxLayoutWidth:([UIScreen mainScreen].bounds.size.width-74)];
    
    backView.backgroundColor = [UIColor fullMoonColor];
    
    [addCardButton setTitle:@"ADD DEBIT CARD"];
    [addCardButton applyDefaultStyle];
    
    statusImageView.tintColor = [UIColor denimColor];
}

-(void)showInfoOfTransaction:(WLTPaymentTransaction*)trans {
    
    NSString *imageName = nil;
    BOOL changeColor = NO;
    
    switch (trans.status) {
        case PaymentStatusPullSuccess:
        case PaymentStatusPushPending: {
            imageName = @"StatusIconSent";
            changeColor = YES;
        }
        break;
        case PaymentStatusPushSuccess: {
            imageName = @"StatusIconReceived";
            changeColor = YES;
        }
        break;
        case PaymentStatusPushReversed: {
            imageName = @"RevertPaymentIcon";
            changeColor = YES;
        }
        break;
        default: {
            imageName = @"ErrorIcon";
        }
        break;
    }
    
    BOOL showAddButton = NO;
    if ( (trans.status == PaymentStatusPushPending) && !trans.isPaid) {
        
        if ( (trans.subStatus == PaymentSubStatusReceiverNotHaveDefaultInstrument) || (trans.subStatus == PaymentSubStatusReceiverInstrumentUUIDNotFound) || (trans.subStatus == PaymentSubStatusRetryAddInstrument) || (trans.subStatus == PaymentSubStatusRetry)) {
            showAddButton = YES;
        }
    }
    
    if ( (trans.status == PaymentStatusPushPending) && (trans.subStatus == PaymentSubStatusWaiting)) {
        
        infoLabel.text = @"Payment in progress";
    }
    else {
        
        if (trans.subStatusMessage.length) {
            
            NSString *complete = [NSString stringWithFormat:@"%@\n\n%@", trans.statuMessage, trans.subStatusMessage];
            NSRange subRange = [complete rangeOfString:trans.subStatusMessage];
            
            
            NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:complete];
            [attributed addAttribute:NSFontAttributeName value:[UIFont sfUITextRegularItallicOfSize:12] range:subRange];
            [attributed addAttribute:NSFontAttributeName value:[UIFont sfUITextRegularItallicOfSize:5] range:NSMakeRange(subRange.location-1, 1)];
            
            infoLabel.attributedText = attributed;
        }else {
            
            infoLabel.text = trans.statuMessage;
        }
    }
    
    [addCardButton setTitle:@"ADD DEBIT CARD"];
    if (showAddButton) {
        
        if ( (trans.subStatus == PaymentSubStatusRetry) || (trans.subStatus == PaymentSubStatusRetryAddInstrument)) {
            
            imageName = @"ErrorIcon";
            changeColor = NO;
            [addCardButton setTitle:@"ADD BANK ACCOUNT"];
        }
    }
    
    if (changeColor) {
        
        statusImageView.image = [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }else {
        
        statusImageView.image = [UIImage imageNamed:imageName];
    }
    
    CGFloat height = [infoLabel intrinsicContentSize].height;
    height += 30;
    
    if (showAddButton) {
        
        addCardButton.hidden = NO;
        
        height += 60;
    }else {
        
        addCardButton.hidden = YES;
    }
    
    backViewHeight.constant = height;
}

@end
