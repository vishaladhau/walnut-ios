//
//  WLTCardView.h
//  walnut
//
//  Created by Abhinav Singh on 02/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GTLWalnutMPaymentInstrument;

@interface WLTCardView : UIView {
    
    __weak IBOutlet UIImageView *tintImageView;
    __weak IBOutlet UIImageView *fastFundImageView;
    __weak IBOutlet UIImageView *cardTypeImageView;
    __weak IBOutlet UILabel *cardNameLabel;
	
    @public
    __weak IBOutlet UILabel *cardNumberLabel;
}

-(void)showDetailsOfInstrument:(GTLWalnutMPaymentInstrument*)instrument;
@property(nonatomic, strong) GTLWalnutMPaymentInstrument *instrument;

@end
