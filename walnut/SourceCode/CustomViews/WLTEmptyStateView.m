//
//  WLTEmptyStateView.m
//  walnut
//
//  Created by Abhinav Singh on 11/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTEmptyStateView.h"
#import "LSButton.h"
#import "WLTNetworkManager.h"

@interface WLTEmptyStateView ()

@property(nonatomic, weak) id <WLTEmptyStateViewDelegate> delegate;
@property(nonatomic, assign) WLTNetworkState networkState;

@end

@implementation WLTEmptyStateView

+(WLTEmptyStateView*)emptyStateViewWithStyle:(WLTEmptyStateViewStyle)style andDelegate:(id<WLTEmptyStateViewDelegate>)deleg{
    
    WLTEmptyStateView *empty = [[[self class] alloc] initWithViewStyle:style];
    empty.delegate = deleg;
    
    return empty;
}

-(instancetype)initWithViewStyle:(WLTEmptyStateViewStyle)style {
    
    self = [super initWithFrame:CGRectZero];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
        
        NSDictionary *metrics = @{@"vGap":@(20)};
        
        {
            UIView *emptyStateV = [[UIView alloc] initWithFrame:CGRectZero];
            emptyStateV.translatesAutoresizingMaskIntoConstraints = NO;
            [self addSubview:emptyStateV];
            
            UIView *topView = [[UIView alloc] initWithFrame:CGRectZero];
            topView.translatesAutoresizingMaskIntoConstraints = NO;
            topView.userInteractionEnabled = NO;
            [emptyStateV addSubview:topView];
            
            UIView *bottomView = [[UIView alloc] initWithFrame:CGRectZero];
            bottomView.translatesAutoresizingMaskIntoConstraints = NO;
            [emptyStateV addSubview:bottomView];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
            [label setPreferredMaxLayoutWidth:([UIScreen mainScreen].bounds.size.width - 60)];
            label.numberOfLines = 0;
            label.textAlignment = NSTextAlignmentCenter;
            label.translatesAutoresizingMaskIntoConstraints = NO;
            [emptyStateV addSubview:label];
            
            UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectZero];
            imageV.contentMode = UIViewContentModeScaleAspectFit;
            imageV.translatesAutoresizingMaskIntoConstraints = NO;
            [emptyStateV addSubview:imageV];
            
            if (style == WLTEmptyStateViewStyleWithAction) {
                
                LSButton *btn = [[LSButton alloc] initWithFrame:CGRectZero];
                [btn addTarget:self action:@selector(actionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                btn.translatesAutoresizingMaskIntoConstraints = NO;
                [btn applyDefaultStyle];
                [emptyStateV addSubview:btn];
                
                actionButton = btn;
            }
            
            NSDictionary *dict = nil;
            if (actionButton) {
                dict = NSDictionaryOfVariableBindings(actionButton, label, topView, bottomView, imageV, emptyStateV);
            }else {
                dict = NSDictionaryOfVariableBindings(label, topView, bottomView, imageV, emptyStateV);
            }
            
            [emptyStateV addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-30-[label]-30-|" options:0 metrics:nil views:dict]];
            [emptyStateV addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[topView]-0-|" options:0 metrics:nil views:dict]];
            [emptyStateV addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[bottomView]-0-|" options:0 metrics:nil views:dict]];
            
            [emptyStateV addConstraint:[NSLayoutConstraint constraintWithItem:imageV
                                                                    attribute:NSLayoutAttributeCenterX
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:emptyStateV
                                                                    attribute:NSLayoutAttributeCenterX
                                                                   multiplier:1 constant:0]];
            
            if (actionButton) {
                
                [emptyStateV addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[actionButton]-20-|" options:0 metrics:nil views:dict]];
                [emptyStateV addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[topView(==bottomView)]-0-[label]-vGap-[imageV]-vGap-[actionButton(==45)]-0-[bottomView(==topView)]-0-|" options:0 metrics:metrics views:dict]];
            }else {
                [emptyStateV addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[topView(==bottomView)]-0-[label]-vGap-[imageV]-vGap-[bottomView(==topView)]-0-|" options:0 metrics:metrics views:dict]];
            }
            
            errorLabel = label;
            emptyStateView = emptyStateV;
            emptyStateImageView = imageV;
        }
        
        {
            UIView *netErrorView = [[UIView alloc] initWithFrame:CGRectZero];
            netErrorView.translatesAutoresizingMaskIntoConstraints = NO;
            [self addSubview:netErrorView];
            
            UIView *topView = [[UIView alloc] initWithFrame:CGRectZero];
            topView.translatesAutoresizingMaskIntoConstraints = NO;
            topView.userInteractionEnabled = NO;
            [netErrorView addSubview:topView];
            
            UIView *bottomView = [[UIView alloc] initWithFrame:CGRectZero];
            bottomView.translatesAutoresizingMaskIntoConstraints = NO;
            [netErrorView addSubview:bottomView];
            
            UILabel *netErrorlabel = [[UILabel alloc] initWithFrame:CGRectZero];
            netErrorlabel.numberOfLines = 0;
            [netErrorlabel setPreferredMaxLayoutWidth:([UIScreen mainScreen].bounds.size.width - 60)];
            netErrorlabel.textAlignment = NSTextAlignmentCenter;
            netErrorlabel.translatesAutoresizingMaskIntoConstraints = NO;
            [netErrorView addSubview:netErrorlabel];
            
            NSString *title = @"No can do!";
            NSString *subtitle = @"We can’t reach our servers.\nPlease check your internet connection";
            NSString *networkErrorString = [NSString stringWithFormat:@"%@\n%@", title, subtitle];
            NSMutableAttributedString *attributtedError = [[NSMutableAttributedString alloc] initWithString:networkErrorString attributes:@{NSForegroundColorAttributeName:[UIColor cadetColor], NSFontAttributeName:[UIFont sfUITextMediumOfSize:17]}];
            [attributtedError addAttributes:@{NSFontAttributeName:[UIFont sfUITextRegularOfSize:14]} range:[networkErrorString rangeOfString:subtitle]];
            netErrorlabel.attributedText = attributtedError;
            
            UIImage *image = [UIImage imageNamed:@"NoNetworkImage"];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
            imageView.translatesAutoresizingMaskIntoConstraints = NO;
            [netErrorView addSubview:imageView];
            
            [netErrorView addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:netErrorView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
            
            NSDictionary *dict = NSDictionaryOfVariableBindings(topView, bottomView, netErrorlabel, imageView);
            
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-30-[netErrorlabel]-30-|" options:0 metrics:nil views:dict]];
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[topView(==bottomView)]-0-[netErrorlabel]-vGap-[imageView]-0-[bottomView(==topView)]-0-|" options:0 metrics:metrics views:dict]];
            
            noNetworkStateView = netErrorView;
        }
        
        NSDictionary *dict = NSDictionaryOfVariableBindings(noNetworkStateView, emptyStateView);
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[emptyStateView]-0-|" options:0 metrics:nil views:dict]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[emptyStateView]-0-|" options:0 metrics:nil views:dict]];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[noNetworkStateView]-0-|" options:0 metrics:nil views:dict]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[noNetworkStateView]-0-|" options:0 metrics:nil views:dict]];
        
        self.networkState = -1;
        [self checkNetworkStatus];
    }
    
    return self;
}

-(void)showTitle:(NSString*)title andSubtitle:(NSString*)info {
    
    if (!title) {
        title = @"Test Title String";
    }
    if (!info) {
        info = @"Test Information String";
    }
    
    NSString *complete = [NSString stringWithFormat:@"%@\n%@", title, info];
    
    NSMutableAttributedString *attributted = [[NSMutableAttributedString alloc] initWithString:complete attributes:@{NSForegroundColorAttributeName:[UIColor cadetColor], NSFontAttributeName:[UIFont sfUITextMediumOfSize:17]}];
    [attributted addAttributes:@{NSFontAttributeName:[UIFont sfUITextRegularOfSize:14]} range:[complete rangeOfString:info]];
    
    errorLabel.attributedText = attributted;
    
    [self invalidateIntrinsicContentSize];
}

-(void)showImage:(NSString*)imageName {
    
    emptyStateImageView.image = [UIImage imageNamed:imageName];
    [self invalidateIntrinsicContentSize];
}

-(void)showTitle:(NSString*)title andSubtitle:(NSString*)info imageName:(NSString*)imageName {
    
    [self showTitle:title andSubtitle:info];
    [self showImage:imageName];
}

-(void)showAttributedString:(NSAttributedString*)string {
    
    errorLabel.attributedText = string;
}

-(void)setActionButtonTitle:(NSString*)title{
    
    [actionButton setTitle:title];
}

-(void)actionButtonClicked:(LSButton*)btn {
    
    [self.delegate emptyStateViewActionButtonTapped:self];
}

#pragma mark - Reachability

-(void)setNetworkState:(WLTNetworkState)networkState {
    
    if (networkState != self.networkState) {
        
        if((self.networkState == WLTNetworkStateNotReachable) && (networkState == WLTNetworkStateReachable)) {
            [self.delegate emptyStateViewReloadScreen:self];
        }
        
        if (networkState == WLTNetworkStateNotReachable) {
            [self showNetworkErrorState:YES];
        }else {
            [self showNetworkErrorState:NO];
        }
        
        _networkState = networkState;
    }
}

-(void)checkNetworkStatus {
    
    NetworkStatus networkStatus = [[WLTNetworkManager sharedManager].internetReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        self.networkState = WLTNetworkStateNotReachable;
    } else {
        self.networkState = WLTNetworkStateReachable;
    }
}

- (void)reachabilityChanged:(NSNotification *)note {
    
    [self checkNetworkStatus];
}

-(void)showNetworkErrorState:(BOOL)show {
    
    emptyStateView.hidden = show;
    noNetworkStateView.hidden = !show;
}

@end
