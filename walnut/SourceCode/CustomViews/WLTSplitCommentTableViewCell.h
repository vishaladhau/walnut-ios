//
//  WLTSplitCommentTableViewCell.h
//  walnut
//
//  Created by Abhinav Singh on 01/03/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WLTUserIconButton.h"

@class WLTSplitComment;
@class WLTDotsLoadingView;
@class WLTSplitCommentTableViewCell;

@protocol WLTSplitCommentTableViewCellDelegate <NSObject>

-(void)splitCommentCellRetryClicked:(WLTSplitCommentTableViewCell*)cell;
-(void)splitCommentCellDeleteClicked:(WLTSplitCommentTableViewCell*)cell;

@end

@interface WLTSplitCommentTableViewCell : UITableViewCell {
    
    __weak IBOutlet UILabel *nameLabel;
    __weak IBOutlet UILabel *dateLabel;
    __weak IBOutlet UILabel *titleLabel;
    
    __weak IBOutlet WLTUserIconButton *userNameIconView;
    __weak IBOutlet WLTDotsLoadingView *dotsLoadingView;
    
    __weak IBOutlet UIImageView *failedStatusImageView;
}

@property(nonatomic, weak) id <WLTSplitCommentTableViewCellDelegate>delegate;
@property(readonly, strong) WLTSplitComment *displayedComment;

-(void)showDetailsOfTransaction:(WLTSplitComment*)transaction;

@end
