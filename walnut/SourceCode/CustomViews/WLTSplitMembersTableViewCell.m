//
//  WLTSplitMembersTableViewCell.m
//  walnut
//
//  Created by Abhinav Singh on 03/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTSplitMembersTableViewCell.h"
#import "WLTTransactionUser.h"
#import "WLTUserIconButton.h"

@implementation WLTSplitMembersTableViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    userNameLabel.textColor = [UIColor cadetColor];
    userNameLabel.font = [UIFont sfUITextMediumOfSize:16];
    
    amountLabel.textColor = [UIColor oweColor];
    amountLabel.font = [UIFont sfUITextRegularOfSize:16];
}

-(void)showDetailsOfTransactionUser:(WLTTransactionUser*)user {
    
    [userNameLabel displayUserNameOrNumberOfUser:user];
    
    amountLabel.text = user.amount.displayCurrencyString;
    [userButton displayUserIcon:user];
}

@end
