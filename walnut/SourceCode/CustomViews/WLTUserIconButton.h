//
//  WLTUserIconButton.h
//  walnut
//
//  Created by Abhinav Singh on 4/1/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, UserIconButtonStyle) {
    
    UserIconButtonStyleDefault = 0,
    UserIconButtonStyleBordered,
};

@interface WLTUserIconButton : UIControl {
	
	__weak UIView *backgroundView;
    id<User> displayedUser;
    BOOL isAddedInContacts;
    
    UserIconButtonStyle buttonStyle;
    @public
    __weak UILabel *nameLabel;
}

-(void)setViewStyle:(UserIconButtonStyle)style;
-(void)displayUserIcon:(id<User>)user;

@end
