//
//  WLTErrorView.m
//  walnut
//
//  Created by Abhinav Singh on 06/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTErrorView.h"

@interface WLTErrorView () {
    
    __weak UILabel *infoLabel;
    __weak UIImageView *imageView;
}

@end

@implementation WLTErrorView

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initialSetup];
    }
    
    return self;
}

-(void)initialSetup {
    
    UIImageView *imagView = [[UIImageView alloc] initWithFrame:CGRectZero];
    imagView.translatesAutoresizingMaskIntoConstraints = NO;
    imagView.image = [UIImage imageNamed:@"ErrorIcon"];
    [self addSubview:imagView];
    
    UILabel *infLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    infLabel.textAlignment = NSTextAlignmentCenter;
    [infLabel setPreferredMaxLayoutWidth:([UIScreen mainScreen].bounds.size.width-60)];
    infLabel.translatesAutoresizingMaskIntoConstraints = NO;
    infLabel.textColor = [UIColor cadetColor];
    infLabel.font = [UIFont sfUITextRegularOfSize:14];
    infLabel.numberOfLines = 0;
    [self addSubview:infLabel];
    
    imageView = imagView;
    infoLabel = infLabel;
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(imageView, infoLabel);
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-15-[imageView]-10-[infoLabel]-15-|" options:0 metrics:nil views:dict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[infoLabel]-0-|" options:0 metrics:nil views:dict]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self attribute:NSLayoutAttributeCenterX
                                                    multiplier:1 constant:0]];
}

-(void)setErrorString:(NSString*)string {
    
    infoLabel.text = string;
    [self invalidateIntrinsicContentSize];
}

-(CGSize)intrinsicContentSize {
    
    CGSize size = CGSizeMake(infoLabel.preferredMaxLayoutWidth, 0);
    size.height += 15;
    size.height += [imageView intrinsicContentSize].height;
    size.height += 10;
    size.height += [infoLabel intrinsicContentSize].height;
    size.height += 15;
    
    return size;
}

@end
