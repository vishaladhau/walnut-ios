//
//  WLTEmptyStateView.h
//  walnut
//
//  Created by Abhinav Singh on 11/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSButton.h"
#import "Reachability.h"

@class WLTEmptyStateView;

typedef NS_ENUM(NSInteger, WLTNetworkState) {
    
    WLTNetworkStateUnknown = 0,
    WLTNetworkStateReachable,
    WLTNetworkStateNotReachable,
};

typedef NS_ENUM(NSInteger, WLTEmptyStateViewStyle) {
    
    WLTEmptyStateViewStyleDefault,
    WLTEmptyStateViewStyleWithAction,
};

@protocol WLTEmptyStateViewDelegate <NSObject>

-(void)emptyStateViewReloadScreen:(WLTEmptyStateView*)view;
-(void)emptyStateViewActionButtonTapped:(WLTEmptyStateView*)view;

@end

@interface WLTEmptyStateView : UIView {
    
    __weak UILabel *errorLabel;
    __weak LSButton *actionButton;
    __weak UIView *emptyStateView;
    __weak UIImageView *emptyStateImageView;
    
    __weak UIView *noNetworkStateView;
}

+(WLTEmptyStateView*)emptyStateViewWithStyle:(WLTEmptyStateViewStyle)style andDelegate:(id<WLTEmptyStateViewDelegate>)deleg;
-(void)showAttributedString:(NSAttributedString*)string;

-(void)showImage:(NSString*)image;

-(void)showTitle:(NSString*)title andSubtitle:(NSString*)info;

-(void)showTitle:(NSString*)title
     andSubtitle:(NSString*)info
       imageName:(NSString*)imageName;

-(void)setActionButtonTitle:(NSString*)title;

@end
