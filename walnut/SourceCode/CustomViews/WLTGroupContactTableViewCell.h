//
//  WLTGroupContactTableViewCell.h
//  walnut
//
//  Created by Abhinav Singh on 02/09/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTContactTableViewCell.h"

@interface WLTGroupContactTableViewCell : WLTContactTableViewCell {
    
    __weak UIImageView *selectionImageView;
}

-(void)showSelectedContact:(BOOL)selected;
-(void)setAccessoryColor:(UIColor*)color;

@end
