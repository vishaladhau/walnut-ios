//
//  WLTKeyValueTableViewCell.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 07/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTKeyValueTableViewCell.h"

@implementation WLTKeyValueTableViewCell

- (void)awakeFromNib {
    
    // Initialization code
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor whiteColor];
    
    self.accessoryType = UITableViewCellAccessoryNone;
    
    keyLabel.font = [UIFont sfUITextRegularOfSize:12];
    keyLabel.textColor = [UIColor mediumGrayTextColor];
    
    valueLabel.font = [UIFont sfUITextMediumOfSize:12];
    valueLabel.textColor = [UIColor mediumGrayTextColor];
    
    [valueLabel setPreferredMaxLayoutWidth:250];
}

-(void)setKey:(NSString*)keyString forValue:(NSString*)valueString {
    
    valueLabel.text = valueString;
    
    CGFloat width = [valueLabel intrinsicContentSize].width;
    CGFloat avaliable = [UIScreen mainScreen].bounds.size.width - (width + 10 + 10 + 10);
    [keyLabel setPreferredMaxLayoutWidth:avaliable];
    
    keyLabel.text = [keyString capitalizedString];
}

@end
