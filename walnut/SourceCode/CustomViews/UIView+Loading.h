//
//  UIView+Loading.h
//  IIFL
//
//  Created by Abhinav Singh on 19/10/14.
//  Copyright (c) 2014 LeftShift. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ViewLoadingProtocol <NSObject>

-(void)showLoadingAtNavbar:(BOOL)navBar;
-(void)removeLoadingFromNavbar:(BOOL)navBar;

@end

@interface UIView (Loading)

-(void)startLoadingWithColor:(UIColor *)color;
-(void)startLoadingWithColor:(UIColor*)color andStyle:(UIActivityIndicatorViewStyle)style;
-(void)startLoading;
-(void)endLoading;
-(BOOL)haveLoadingView;

@end
