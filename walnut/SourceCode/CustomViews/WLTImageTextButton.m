//
//  WLTImageTextButton.m
//  walnut
//
//  Created by Abhinav Singh on 25/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTImageTextButton.h"

@implementation WLTImageTextButton

- (void)initialSetup {
    
    [super initialSetup];
    self.layer.cornerRadius = DefaultCornerRadius;
}

-(void)addAllSubViews {
    
    [super addAllSubViews];
    
    UIImageView *imagev = [[UIImageView alloc] initWithFrame:CGRectZero];
    [imagev setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    imagev.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:imagev];
    
    if (buttonImage) {
        imagev.image = buttonImage;
    }
    
    imageView = imagev;
}

-(void)setColor:(UIColor*)color {
    
    LSStateColor *normalClr = [[LSStateColor alloc] initWithBack:color
                                                    contentColor:[UIColor whiteColor]
                                                    andBorderClr:nil];
    
    LSStateColor *highClr = [[LSStateColor alloc] initWithBack:[UIColor whiteColor]
                                                  contentColor:color
                                                  andBorderClr:color];
    
    [self addColors:normalClr forState:LSButtonStateNormal];
    [self addColors:highClr forState:LSButtonStateHighlighted];
}

-(void)setImage:(UIImage*)img {
    
    buttonImage = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    imageView.image = buttonImage;
}

-(void)addAllConstraints {
    
    UIView *leftPadding = [[UIView alloc] initWithFrame:CGRectZero];
    leftPadding.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:leftPadding];
    
    UIView *rightPadding = [[UIView alloc] initWithFrame:CGRectZero];
    rightPadding.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:rightPadding];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(rightPadding, leftPadding, imageView, titleLabel);
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[leftPadding(==rightPadding)]-0-[imageView]-10-[titleLabel]-0-[rightPadding(==leftPadding)]-0-|" options:0 metrics:nil views:dict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[leftPadding]-0-|" options:0 metrics:nil views:dict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[rightPadding]-0-|" options:0 metrics:nil views:dict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[titleLabel]-0-|" options:0 metrics:nil views:dict]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:imageView
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY
                                                    multiplier:1 constant:0]];
}

- (void)changeDisplayForCurrentState {
    
    LSStateColor *clrsData = [stateColors objectForKey:@(state)];
    
    if (clrsData.borderColor) {
        
        self.layer.borderWidth = 1;
        self.layer.borderColor = [clrsData.borderColor CGColor];
    }else {
        self.layer.borderWidth = 0.0f;
        self.layer.borderColor = nil;
    }
    
    imageView.tintColor = clrsData.contentColor;
    titleLabel.textColor = clrsData.contentColor;
    
    self.backgroundColor = clrsData.backColor;
}

@end
