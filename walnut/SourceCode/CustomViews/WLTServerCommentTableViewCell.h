//
//  WLTServerCommentTableViewCell.h
//  walnut
//
//  Created by Abhinav Singh on 01/03/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLTServerCommentTableViewCell : UITableViewCell {
    
    __weak IBOutlet UIView *commentBackView;
    __weak IBOutlet UILabel *commentLabel;
}

-(void)showComment:(NSString*)comment fullWidth:(BOOL)fullWidth;

@end
