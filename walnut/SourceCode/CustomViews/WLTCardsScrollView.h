//
//  WLTCardsScrollView.h
//  walnut
//
//  Created by Abhinav Singh on 08/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WLTCardsScrollView;
@class WLTCardView;
@class WLTEmptyCardView;

@protocol WLTCardsScrollViewDelegate <NSObject>

-(void)cardsScrollView:(WLTCardsScrollView*)scrollView selectedIndexChanged:(NSInteger)newIndex;

@end

@interface WLTCardsScrollView : UIView {
    
    __weak NSLayoutConstraint *contentWidthConstraint;
    __weak NSLayoutConstraint *contentLeadingConstraint;
    
    __weak UIView *contentView;
    
    CGPoint initialPoint;
    CGFloat initialLeadingX;
}

@property(nonatomic, weak) id <WLTCardsScrollViewDelegate>delegate;
@property(readonly, assign) WLTCardView *selectedCardView;
@property(readonly, assign) WLTEmptyCardView *emptyCardView;

@property(readonly, strong) NSArray *cardViews;
-(void)addSubviewsForCards:(NSArray*)cards;

-(void)selectCardOfID:(NSString*)cardID animated:(BOOL)animate;

@end
