//
//  WLTContactTableViewCell.h
//  walnut
//
//  Created by Abhinav Singh on 26/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTContactTableViewCell.h"

@class WLTUserIconButton;

@interface WLTContactTableViewCell : UITableViewCell {
    
    __weak WLTUserIconButton *_iconButton;
    __weak UILabel *_titleTextLabel;
    __weak UILabel *_numberTextLabel;
    
    __weak UIView *_accessory;
}

@property(readonly, strong) id <User> displayedUser;

-(void)showInformation:(id<User>)usr;
-(void)addAllSubviews;

-(void)addLayoutConstraints;
-(void)addDefaultConstraints;

@end
