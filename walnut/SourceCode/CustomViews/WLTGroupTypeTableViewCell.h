//
//  WLTGroupTypeTableViewCell.h
//  walnut
//
//  Created by Abhinav Singh on 26/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLTGroupTypeTableViewCell : UITableViewCell {
    
    __weak IBOutlet UILabel *titleTextLabel;
    __weak IBOutlet UILabel *subTitleTextLabel;
}

-(void)setUpForPrivate:(BOOL)privateGrp;
@property(nonatomic, assign) BOOL isPrivateGroup;

@end
