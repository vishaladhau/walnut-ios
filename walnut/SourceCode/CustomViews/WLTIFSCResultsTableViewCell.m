//
//  WLTIFSCResultsTableViewCell.m
//  walnut
//
//  Created by Abhinav Singh on 08/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTIFSCResultsTableViewCell.h"
#import "GTLWalnutMIfscSearchObject.h"

@implementation WLTIFSCResultsTableViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    addressLabel.font = [UIFont sfUITextRegularOfSize:14];
    addressLabel.textColor = [UIColor cadetColor];
    [addressLabel setPreferredMaxLayoutWidth:([UIScreen mainScreen].bounds.size.width-20)];
    
    codeLabel.font = [UIFont sfUITextMediumOfSize:14];
    codeLabel.textColor = [UIColor cadetColor];
    [codeLabel setPreferredMaxLayoutWidth:addressLabel.preferredMaxLayoutWidth];
}

-(void)showDetailsOfSearchObject:(GTLWalnutMIfscSearchObject*)search searchString:(NSString*)searchString{
    
    codeLabel.text = search.ifsc;
    
    NSRange searchRange = NSMakeRange(NSNotFound, 0);
    if (searchString.length) {
        searchRange = [search.address rangeOfString:searchString options:NSCaseInsensitiveSearch];
    }
    
    if (searchRange.location != NSNotFound) {
        
        NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:search.address];
        [attributed addAttributes:@{NSFontAttributeName:[UIFont sfUITextMediumOfSize:15], NSForegroundColorAttributeName:[UIColor denimColor]} range:searchRange];
        
        addressLabel.attributedText = attributed;
    }else {
        
        addressLabel.text = search.address;
    }
}

-(CGSize)intrinsicContentSize {
    
    CGSize size = CGSizeMake(UIViewNoIntrinsicMetric, 0);
    size.height += 35;
    size.height += [codeLabel intrinsicContentSize].height;
    size.height += [addressLabel intrinsicContentSize].height;
    
    return size;
}

@end
