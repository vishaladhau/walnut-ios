//
//  WLTUserTransactionTableViewCell.m
//  walnut
//
//  Created by Abhinav Singh on 02/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTUserTransactionTableViewCell.h"
#import "WLTTransaction.h"
#import "WLTUser.h"
#import "WLTDatabaseManager.h"
#import "WLTTransactionUser.h"

@implementation WLTUserTransactionTableViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    dateLabel.font = [UIFont sfUITextRegularOfSize:14];
    dateLabel.textColor = [UIColor mediumGrayTextColor];
    
    userNameLabel.textColor = [UIColor darkGrayTextColor];
    userNameLabel.font = [UIFont sfUITextRegularOfSize:16];
    
    amountLabel.font = [UIFont sfUITextRegularOfSize:16];
}

-(void)showDetailsOfTransaction:(WLTTransaction*)trans fromUser:(id<User>)userOther{
    
    userNameLabel.text = trans.placeName;
    dateLabel.text = trans.createdDate.displayString;
    
    NSString *ownersMobile = trans.owner.mobileString;
    NSString *currentUserMob = CURRENT_USER_MOB;
    NSString *userMob = [userOther completeNumber];
    
    BOOL isCurrentUserOwner = NO;
    BOOL isUserOwner = NO;
    
    if ([ownersMobile isEqualToString:userMob]) {
        
        isUserOwner = YES;
    }else if ([ownersMobile isEqualToString:currentUserMob]) {
        
        isCurrentUserOwner = YES;
    }
    
    NSNumber *userAmt = nil;
    BOOL isUserPaying = NO;
    
    for ( WLTTransactionUser *usr in trans.splits ) {
        
        NSString *tUm = usr.mobileString;
        if (isCurrentUserOwner) {
            
            if ([tUm isEqualToString:userMob]) {
                userAmt = usr.amount;
                isUserPaying = NO;
                break;
            }
        }
        else if ([tUm isEqualToString:currentUserMob]) {
            userAmt = usr.amount;
            isUserPaying = YES;
            break;
        }
    }
    
    amountLabel.text = userAmt.displayCurrencyString;
    if (isUserPaying) {
        amountLabel.textColor = [UIColor oweColor];
    }else {
        amountLabel.textColor = [UIColor getColor];
    }
}

@end
