//
//  WLTSendRequestMoneyButton.m
//  walnut
//
//  Created by Abhinav Singh on 12/08/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTSendRequestMoneyButton.h"

@implementation WLTSendRequestMoneyButton

-(void)addAllSubViews {
    
    [super addAllSubViews];
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectZero];
    imgView.contentMode = UIViewContentModeCenter;
    imgView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:imgView];
    
    imageView = imgView;
}

-(void)addAllConstraints {
    
    self.backgroundColor = [UIColor clearColor];
    self.layer.borderWidth = 0;
    
    titleLabel.textColor = [UIColor cadetColor];
    titleLabel.font = [UIFont sfUITextMediumOfSize:12];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(imageView, titleLabel);
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[titleLabel]-5-[imageView]-15-|" options:0 metrics:nil views:dict]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:imageView
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1 constant:0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1 constant:0]];
}

-(void)setImage:(UIImage *)image andColor:(UIColor *)color {
    
    imageView.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    imageColor = color;
    
    [self changeDisplayForCurrentState];
}

- (void)changeDisplayForCurrentState {
    
    if ( self.state == LSButtonStateHighlighted ) {
        imageView.tintColor = [UIColor cadetColor];
    }else {
        imageView.tintColor = imageColor;
    }
}

-(CGSize)intrinsicContentSize {
    
    CGFloat height = [titleLabel intrinsicContentSize].height;
    CGFloat width = [titleLabel intrinsicContentSize].width;
    if (width < 88) {
        width = 88;
    }
    
    height += 5;
    height += 88;
    
    return CGSizeMake(width, height);
}

@end
