//
//  LSToastMessage.h
//  MobiTVConnect
//
//  Created by Abhinav Singh on 26/06/15.
//  Copyright (c) 2015 Leftshift Technologies Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

FOUNDATION_EXPORT NSString *const SlowNetwork;
FOUNDATION_EXPORT NSString *const NotAGroupMember;
FOUNDATION_EXPORT NSString *const MoreThanP2PLimit;
FOUNDATION_EXPORT NSString *const LessThanP2PLimit;

@interface LSToastMessageHandler : NSObject {
    
}

+(void)removeAllMesages;
+(void)removeAllMesageOfID:(NSString*)messageID;

+(void)showMessage:(NSAttributedString*)string
            withID:(NSString*)messageID
        andPriorty:(NSOperationQueuePriority)priorty;

+(void)showMessage:(NSAttributedString*)string withID:(NSString*)messageID;

@end

@interface LSToastMessage : UIView {
    
}

@end