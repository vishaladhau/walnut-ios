//
//  WLTProfilesSelectionView.m
//  walnut
//
//  Created by Abhinav Singh on 21/03/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTProfilesSelectionView.h"
#import "GTLWalnutMUserProfile.h"

@implementation WLTProfilesSelectionView

-(void)initialSetup {
    
    [super initialSetup];
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMM yyyy"];
}

-(NSString*)cancelButtonTitle {
    
    return @"Setup New Profile";
}

-(NSAttributedString*)titleForObject:(GTLWalnutMUserProfile*)prof {
    
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.alignment = NSTextAlignmentCenter;
    
    NSString *complete = [NSString stringWithFormat:@"%@\n(%@ Entries) %@", prof.deviceName, prof.statementCount.displayString, [formatter stringFromDate:prof.syncTime.date]];
    NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:complete attributes:@{NSParagraphStyleAttributeName:style, NSFontAttributeName:[UIFont sfUITextLightOfSize:12], NSForegroundColorAttributeName:[UIColor darkGrayTextColor]}];
    [attributed addAttributes:@{NSForegroundColorAttributeName:[self tintColor],NSFontAttributeName:[UIFont sfUITextRegularOfSize:15]} range:[complete rangeOfString:prof.deviceName]];
    
    return attributed;
}

-(BOOL)canDismissOnAlphaViewTap {
    return NO;
}

@end
