//
//  WLTAdminTableViewCell.h
//  walnut
//
//  Created by Abhinav Singh on 22/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTContactTableViewCell.h"

@interface WLTAdminTableViewCell : WLTContactTableViewCell {
    
    __weak UILabel *adminLabel;
}

@end
