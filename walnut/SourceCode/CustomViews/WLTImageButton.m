//
//  WLTImageButton.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 04/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTImageButton.h"

@implementation WLTImageButton

-(void)addAllSubViews {
    
    UIImageView *imagev = [[UIImageView alloc] initWithFrame:CGRectZero];
    [imagev setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    imagev.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:imagev];
    
    if (buttonImage) {
        imagev.image = buttonImage;
    }
    
    imageView = imagev;
}

-(void)setWalnutPayStyle {
    
    buttonImage = [UIImage imageNamed:@"WalnutPayIconPay"];
    imageView.image = buttonImage;
    
    LSStateColor *highClr = [[LSStateColor alloc] initWithBack:nil
                                                  contentColor:nil
                                                  andBorderClr:[UIColor lightGrayTextColor]];
    
    LSStateColor *normalClr = [[LSStateColor alloc] initWithBack:nil
                                                    contentColor:nil
                                                    andBorderClr:[UIColor lightGrayTextColor]];
    
    [self addColors:normalClr forState:LSButtonStateNormal];
    [self addColors:highClr forState:LSButtonStateHighlighted];
}

-(void)setWalnutRemindStyle {
    
    [self setImage:[UIImage imageNamed:@"AddReminderIcon"]];
    
    LSStateColor *normalClr = [[LSStateColor alloc] initWithBack:[UIColor athensGray]
                                                    contentColor:[UIColor cadetColor]
                                                    andBorderClr:nil];
    
    LSStateColor *highClr = [[LSStateColor alloc] initWithBack:[UIColor cadetColor]
                                                  contentColor:[UIColor athensGray]
                                                  andBorderClr:nil];
    
    [self addColors:normalClr forState:LSButtonStateNormal];
    [self addColors:highClr forState:LSButtonStateHighlighted];
}

-(void)setWalnutSettleStyle {
    
    [self setImage:[UIImage imageNamed:@"SettleSymbolIcon"]];
    
    UIColor *actionColor = [UIColor getColor];
    
    LSStateColor *normalClr = [[LSStateColor alloc] initWithBack:[UIColor whiteColor]
                                                    contentColor:actionColor
                                                    andBorderClr:nil];
    
    LSStateColor *highClr = [[LSStateColor alloc] initWithBack:actionColor
                                                  contentColor:[UIColor whiteColor]
                                                  andBorderClr:actionColor];
    
    [self addColors:normalClr forState:LSButtonStateNormal];
    [self addColors:highClr forState:LSButtonStateHighlighted];
}

-(void)setColor:(UIColor*)color {
    
    LSStateColor *normalClr = [[LSStateColor alloc] initWithBack:[UIColor whiteColor]
                                                    contentColor:color
                                                    andBorderClr:nil];
    
    LSStateColor *highClr = [[LSStateColor alloc] initWithBack:color
                                                  contentColor:[UIColor whiteColor]
                                                  andBorderClr:[UIColor whiteColor]];
    
    [self addColors:normalClr forState:LSButtonStateNormal];
    [self addColors:highClr forState:LSButtonStateHighlighted];
}

-(void)setColorWithBackground:(UIColor*)color {
    
    LSStateColor *normalClr = [[LSStateColor alloc] initWithBack:color
                                                    contentColor:[UIColor whiteColor]
                                                    andBorderClr:nil];
    
    LSStateColor *highClr = [[LSStateColor alloc] initWithBack:[UIColor whiteColor]
                                                  contentColor:color
                                                  andBorderClr:color];
    
    LSStateColor *disableClr = [[LSStateColor alloc] initWithBack:[UIColor grayColor]
                                                  contentColor:[UIColor whiteColor]
                                                  andBorderClr:nil];
    
    [self addColors:disableClr forState:LSButtonStateDisabled];
    [self addColors:normalClr forState:LSButtonStateNormal];
    [self addColors:highClr forState:LSButtonStateHighlighted];
}


-(void)setImage:(UIImage*)img {
    
    buttonImage = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    imageView.image = buttonImage;
}

-(void)addAllConstraints {
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:imageView
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1 constant:0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:imageView
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1 constant:0]];
}

-(void)layoutSubviews {
    
    [super layoutSubviews];
    self.layer.cornerRadius = (self.height/2);
}

- (void)changeDisplayForCurrentState {
    
    LSStateColor *clrsData = [stateColors objectForKey:@(state)];
    
    if (clrsData.borderColor) {
        
        self.layer.borderWidth = 1;
        self.layer.borderColor = [clrsData.borderColor CGColor];
    }else {
        self.layer.borderWidth = 0.0f;
        self.layer.borderColor = nil;
    }
    
    imageView.tintColor = clrsData.contentColor;
    self.backgroundColor = clrsData.backColor;
}

@end
