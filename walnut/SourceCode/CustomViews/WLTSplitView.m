//
//  WLTSplitView.m
//  walnut
//
//  Created by Abhinav Singh on 22/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTSplitView.h"
#import "WLTContactsManager.h"
#import "WLTDatabaseManager.h"
#import "WLTSplitViewController.h"

@implementation WLTUnderlinedField

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    [[UIColor colorWithRed:0.85 green:0.85 blue:0.85 alpha:1.00] setStroke];
    
    CGContextSetLineWidth(context, 2.0);
    
    CGFloat lengths[] = {4.0f, 4.0};
    
    CGContextSetLineDash(context, 0, lengths, 2);
    
    CGContextMoveToPoint(context, 0, self.bounds.size.height - 2.0);
    CGContextAddLineToPoint(context, self.bounds.size.width, self.bounds.size.height - 2.0);
    
    CGContextDrawPath(context, kCGPathStroke);
}


@end


@implementation WLTSplitView

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self addBorderLineAtPosition:BorderLinePositionBottom];
    
    nameLabel.textColor = [UIColor darkGrayTextColor];
    nameLabel.font = [UIFont sfUITextRegularOfSize:16];
    nameLabel.adjustsFontSizeToFitWidth = YES;
    nameLabel.minimumScaleFactor = 0.5;
    nameLabel.numberOfLines = 1;
    
    amountTextField.textColor = [UIColor oweColor];
    amountTextField.font = [UIFont sfUITextRegularOfSize:16];
    amountTextField.keyboardType = UIKeyboardTypeDecimalPad;
    
    shareLabel.textColor = [UIColor darkGrayTextColor];
    shareLabel.font = [UIFont sfUITextRegularOfSize:14];
    shareLabel.text = @"shares";
    
    stepper.value = 1;
}

-(void)showDetailsOfMember:(WLTSplitUsers*)member isGroupMember:(BOOL)isMember{
    
    _displayedMember = member;
    
    if (isMember) {
        
        amountTextField.textColor = [UIColor oweColor];
        nameLabel.textColor = [UIColor darkGrayTextColor];
        shareLabel.textColor = [UIColor darkGrayTextColor];
        
        amountTextField.enabled = YES;
        stepper.enabled = YES;
        
        [nameLabel displayUserNameOrNumberOfUser:member.member];
    }else {
        
        stepper.enabled = NO;
        amountTextField.enabled = NO;
        
        amountTextField.textColor = [UIColor mediumGrayTextColor];
        shareLabel.textColor = [UIColor mediumGrayTextColor];
        nameLabel.textColor = [UIColor mediumGrayTextColor];
        
        nameLabel.text = @"...";
        __weak typeof(self) weakSelf = self;
        [[WLTContactsManager sharedManager] displayNameForUser:member.member defaultIsMob:YES completion:^(NSString *data) {
            
            if (weakSelf) {
                
                nameLabel.text = [NSString stringWithFormat:@"%@ (Not a group Member)", data];
            }
        }];
    }
}

-(BOOL)isFirstResponder {
    
    return [amountTextField isFirstResponder];
}

@end
