//
//  LSKeyboardDismissalView.m
//  walnut
//
//  Created by Abhinav Singh on 03/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "LSKeyboardDismissalView.h"

@implementation LSKeyboardDismissalView

+(UIView*)firstResponderSubViewOfView:(UIView*)supView {
    
    UIView *firstRes = nil;
    for ( UIView *subs in supView.subviews ) {
        if ([subs isFirstResponder]) {
            firstRes = subs;
        }else {
            firstRes = [LSKeyboardDismissalView firstResponderSubViewOfView:subs];
        }
        
        if (firstRes) {
            break;
        }
    }
    
    return firstRes;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    BOOL hideKeyboard = NO;
    UIView *vi = [LSKeyboardDismissalView firstResponderSubViewOfView:self];
    if (vi) {
        
        hideKeyboard = YES;
        for ( UITouch *tch in touches ) {
            if ([tch.view isEqual:vi]) {
                
                hideKeyboard = NO;
                break;
            }
        }
    }
    
    if (hideKeyboard) {
        
        [self endEditing:YES];
    }else {
        
        [super touchesBegan:touches withEvent:event];
    }
}

@end
