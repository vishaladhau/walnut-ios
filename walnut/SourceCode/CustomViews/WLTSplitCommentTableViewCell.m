//
//  WLTSplitCommentTableViewCell.m
//  walnut
//
//  Created by Abhinav Singh on 01/03/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTSplitCommentTableViewCell.h"
#import "WLTContactsManager.h"
#import "WLTColorPalette.h"
#import "WLTSplitComment.h"
#import "WLTUser.h"
#import "WLTDotsLoadingView.h"
#import "WLTDatabaseManager.h"

@implementation WLTSplitCommentTableViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
	
    nameLabel.font = [UIFont sfUITextMediumOfSize:10];
    nameLabel.textColor = [UIColor denimColor];
    
    dateLabel.textColor = [UIColor mediumGrayTextColor];
    dateLabel.font = [UIFont sfUITextLightOfSize:10];
    
    titleLabel.font = [UIFont sfUITextRegularOfSize:16];
    titleLabel.textColor = [UIColor mediumGrayTextColor];
    titleLabel.numberOfLines = 0;
    
    userNameIconView.hidden = NO;
    failedStatusImageView.hidden = YES;
    
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(longPressGestureRecognized:)];
    longPressGestureRecognizer.minimumPressDuration = 0.3;
    [self addGestureRecognizer:longPressGestureRecognizer];
}

-(void)showDetailsOfTransaction:(WLTSplitComment*)transaction {
    
    _displayedComment = transaction;
    
    [nameLabel displayUserName:transaction.owner];
    [userNameIconView displayUserIcon:transaction.owner];
    
    titleLabel.text = transaction.notes;
    
    [dotsLoadingView stopAnimating];
    
    WLTSplitCommentState state = [transaction state];
    
    if ( state == WLTSplitCommentStatePosting) {
        
        dateLabel.text = nil;
        [dotsLoadingView startAnimating];
    }
    else if ( state == WLTSplitCommentStatePosted) {
        
        dateLabel.text = transaction.sortingDate.displayString;
        dateLabel.textColor = [UIColor mediumGrayTextColor];
        
        userNameIconView.hidden = NO;
        failedStatusImageView.hidden = YES;
    }
    else if ( state == WLTSplitCommentStateFailed) {
        
        dateLabel.text = @"Sending Failed";
        dateLabel.textColor = [UIColor mediumCarmine];
        
        userNameIconView.hidden = YES;
        failedStatusImageView.hidden = NO;
    }
    
    [self invalidateIntrinsicContentSize];
}

#pragma mark - Callbacks

- (void)longPressGestureRecognized:(UILongPressGestureRecognizer *)gestureRecognizer {
    
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        
        [self becomeFirstResponder];
        
        UIMenuController *copyMenu = [UIMenuController sharedMenuController];
        if ( self.displayedComment.state == WLTSplitCommentStateFailed) {
            
            UIMenuItem *itemRetry = [[UIMenuItem alloc] initWithTitle:@"Retry" action:@selector(retry:)];
            copyMenu.menuItems = @[itemRetry];
        }
        
        CGRect bounds = self.bounds;
        bounds.origin.y = titleLabel.originY;
        
        [copyMenu setTargetRect:bounds inView:self];
        copyMenu.arrowDirection = UIMenuControllerArrowDefault;
        [copyMenu setMenuVisible:YES animated:YES];
    }
}

#pragma mark - UIResponder

- (BOOL)canBecomeFirstResponder {
    
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    
    BOOL retValue = NO;
    
    if (action == @selector(copy:)) {
        
        retValue = YES;
    }
    else if (action == @selector(delete:)) {
        
        if ( (self.displayedComment.state == WLTSplitCommentStateFailed) ) {
            retValue = YES;
        }
        else if ( (self.displayedComment.state == WLTSplitCommentStatePosted) && ([self.displayedComment.owner.mobileString isEqualToString:CURRENT_USER_MOB]) ) {
            retValue = YES;
        }
    }
    else if (action == @selector(retry:)) {
        
        if ( self.displayedComment.state == WLTSplitCommentStateFailed) {
            retValue = YES;
        }
    }
    
    return retValue;
}

-(void)copy:(id)sender {
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    NSString *stringToCopy = titleLabel.text;
    [pasteboard setString:stringToCopy];
}

-(void)delete:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(splitCommentCellDeleteClicked:)]) {
        [self.delegate splitCommentCellDeleteClicked:self];
    }
}

-(void)retry:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(splitCommentCellRetryClicked:)]) {
        [self.delegate splitCommentCellRetryClicked:self];
    }
}

@end
