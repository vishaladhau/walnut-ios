//
//  WLTSplitFooterView.h
//  walnut
//
//  Created by Abhinav Singh on 01/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSButton.h"

@class WLTSplitFooterView;

@protocol WLTSplitFooterViewDelegate <NSObject>

-(void)splitFooterViewDistributeClicked:(WLTSplitFooterView*)view;

@end

@interface WLTSplitFooterView : UIView {
    
    __weak IBOutlet UILabel *remTextLabel;
    __weak IBOutlet UILabel *remAmountLabel;
    
    __weak IBOutlet LSButton *distrubuteButton;
}

-(void)setRemamingAmount:(CGFloat)remaning;
@property(nonatomic, weak) id <WLTSplitFooterViewDelegate> delegate;

@end
