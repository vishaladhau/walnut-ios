//
//  WLTSplitTransactionTableViewCell.m
//  walnut
//
//  Created by Abhinav Singh on 01/03/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTSplitTransactionTableViewCell.h"
#import "WLTTransaction.h"
#import "WLTContactsManager.h"
#import "WLTUser.h"
#import "WLTTransactionUser.h"
#import "WLTDatabaseManager.h"
#import "WLTColorPalette.h"

@implementation WLTSplitTransactionTableViewCell

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
	
    billBackView.layer.cornerRadius = DefaultCornerRadius;
    billBackView.backgroundColor = [UIColor lightBackgroundColor];
    
    nameLabel.font = [UIFont sfUITextMediumOfSize:10];
    nameLabel.textColor = [UIColor denimColor];
    
    dateLabel.textColor = [UIColor mediumGrayTextColor];
    dateLabel.font = [UIFont sfUITextLightOfSize:10];
    
    titleLabel.font = [UIFont sfUITextRegularOfSize:16];
    titleLabel.textColor = [UIColor mediumGrayTextColor];
    
    totalLabel.textColor = [UIColor mediumGrayTextColor];
    totalLabel.font = [UIFont sfUITextRegularOfSize:16];
    
    myShareLabel.textColor = [UIColor oweColor];
    myShareLabel.font = [UIFont sfUITextRegularOfSize:16];
    
    
    addedByLabel.textColor = [UIColor mediumGrayTextColor];
    addedByLabel.font = [UIFont sfUITextMediumOfSize:10];
    
    userNameIconView.backgroundColor = [UIColor denimColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeUserName) name:NotifyContactListChanged object:nil];
}

-(void)changeUserName {
   
    [nameLabel displayUserName:_transaction.owner];
}

-(void)showDetailsOfTransaction:(WLTTransaction*)trans isRightAlingedXIB:(BOOL)rightSide{
    
    if ([trans.owner.mobileString isEqualToString:trans.addedBy.mobileString]) {
        
        if (rightSide) {
            
            leadingSpaceConstraint.constant = 8;
            tralingSpaceConstraint.constant = -15;
        }else {
            
            leadingSpaceConstraint.constant = 15;
            tralingSpaceConstraint.constant = -8;
        }
        
        addedByLabel.text = nil;
        dottedImageView.hidden = YES;
        topSpaceConstraint.constant = 8;
        bottomSpaceConstraint.constant = -8;
    }else {
        
        if (rightSide) {
            
            leadingSpaceConstraint.constant = 15;
            tralingSpaceConstraint.constant = -23;
        }else {
            leadingSpaceConstraint.constant = 23;
            tralingSpaceConstraint.constant = -16;
        }
        
        dottedImageView.hidden = NO;
        
        topSpaceConstraint.constant = 40;
        bottomSpaceConstraint.constant = -16;
        
        [[WLTContactsManager sharedManager] displayNameForUser:trans.addedBy defaultIsMob:YES completion:^(NSString *name) {
            
            addedByLabel.text = [NSString stringWithFormat:@"Added by %@", name];
        }];
    }
    _transaction = trans;
    
    [self changeUserName];
    [userNameIconView displayUserIcon:trans.owner];
    
    titleLabel.text = trans.placeName;
    if (!trans.placeName.length) {
        titleLabel.text = trans.notes;
    }
    
    if ([trans.updatedDate compare:trans.createdDate] == NSOrderedDescending) {
        dateLabel.text = [NSString stringWithFormat:@"(Edited) %@", trans.sortingDate.displayString];
    }else {
        dateLabel.text = trans.sortingDate.displayString;
    }
    
    NSString *completeTotal = [NSString stringWithFormat:@"Total Bill\n%@", trans.amount.displayRoundedCurrencyString];
    NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:completeTotal attributes:@{NSForegroundColorAttributeName:[UIColor mediumGrayTextColor], NSFontAttributeName:[UIFont sfUITextRegularOfSize:16]}];
    [attributed addAttributes:@{NSFontAttributeName:[UIFont sfUITextRegularOfSize:10]} range:[completeTotal rangeOfString:@"Total Bill"]];
    
    totalLabel.attributedText = attributed;
    
    NSString *currentUserMobile = CURRENT_USER_MOB;
    
    NSString *usrAmtstring = nil;
    for ( WLTTransactionUser *usr in trans.splits ) {
        if ([usr.mobileString isEqualToString:currentUserMobile]) {
            usrAmtstring = usr.amount.displayRoundedCurrencyString;
            break;
        }
    }
    
    if (!usrAmtstring.length) {
        usrAmtstring = NilAmountPlaceholder;
    }
    
    NSString *completeShare = [NSString stringWithFormat:@"Your Share\n%@", usrAmtstring];
    attributed = [[NSMutableAttributedString alloc] initWithString:completeShare attributes:@{NSForegroundColorAttributeName:[UIColor oweColor], NSFontAttributeName:[UIFont sfUITextRegularOfSize:16]}];
    [attributed addAttributes:@{NSFontAttributeName:[UIFont sfUITextRegularOfSize:10], NSForegroundColorAttributeName:[UIColor mediumGrayTextColor]} range:[completeShare rangeOfString:@"Your Share"]];
    
    myShareLabel.attributedText = attributed;
    
    [self invalidateIntrinsicContentSize];
}

@end
