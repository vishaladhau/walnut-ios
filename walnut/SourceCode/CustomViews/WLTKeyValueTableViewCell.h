//
//  WLTKeyValueTableViewCell.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 07/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLTKeyValueTableViewCell : UITableViewCell {
    
    @public
    __weak IBOutlet UILabel *keyLabel;
    __weak IBOutlet UILabel *valueLabel;
}

-(void)setKey:(NSString*)keyString forValue:(NSString*)valueString;

@end
