//
//  WLTImageButton.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 04/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "LSButton.h"

@interface WLTImageButton : LSButton {
    
    UIImage *buttonImage;
    __weak UIImageView *imageView;
}

-(void)setImage:(UIImage*)img;
-(void)setColorWithBackground:(UIColor*)color;

-(void)setWalnutRemindStyle;
-(void)setWalnutPayStyle;
-(void)setWalnutSettleStyle;

@end
