//
//  WLTNumberSelectionView.h
//  walnut
//
//  Created by Abhinav Singh on 05/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "PPTModalContentView.h"
#import "WLTActionSheet.h"

@interface WLTNumberSelectionView : WLTActionSheet {
    
    CompletionBlock removeBlock;
}

-(void)showObjects:(NSArray*)objects removeContactBlock:(CompletionBlock)block;

@end
