//
//  WLTActionSheet.m
//  walnut
//
//  Created by Abhinav Singh on 05/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTActionSheet.h"

@implementation WLTActionSheet

-(void)initialSetup {
    
    [super initialSetup];
    
    objectViewMapping = [NSMapTable weakToStrongObjectsMapTable];
    
    _alertContentView.backgroundColor = [UIColor clearColor];
    _alertContentView.layer.borderColor = nil;
    _alertContentView.layer.borderWidth = 0;
    _alertContentView.layer.cornerRadius = 0;
    
    UIButton *canBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [canBtn addTarget:self action:@selector(cancelClicked:) forControlEvents:UIControlEventTouchUpInside];
    [canBtn setTitle:[self cancelButtonTitle] forState:UIControlStateNormal];
    [canBtn.titleLabel setFont:[UIFont sfUITextRegularOfSize:17]];
    [canBtn setTitleColor:canBtn.tintColor forState:UIControlStateNormal];
    canBtn.backgroundColor = [UIColor whiteColor];
    canBtn.translatesAutoresizingMaskIntoConstraints  = NO;
    canBtn.layer.cornerRadius = 12.5;
    canBtn.clipsToBounds = YES;
    [_alertContentView addSubview:canBtn];
    
    UIScrollView *theScrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
    theScrollView.backgroundColor = [UIColor whiteColor];
    theScrollView.layer.cornerRadius = 12.5;
    theScrollView.clipsToBounds = YES;
    theScrollView.translatesAutoresizingMaskIntoConstraints = NO;
    [_alertContentView addSubview:theScrollView];
    
    UIView *content = [[UIView alloc] initWithFrame:CGRectZero];
    content.backgroundColor = [UIColor whiteColor];
    content.translatesAutoresizingMaskIntoConstraints = NO;
    [theScrollView addSubview:content];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(theScrollView, canBtn, content);
    [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-0-[content(==%f)]-0-|", ([UIScreen mainScreen].bounds.size.width-20)] options:0 metrics:nil views:dict]];
    [theScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[content]-0-|" options:0 metrics:nil views:dict]];
    
    NSLayoutConstraint *constratintHeight = [NSLayoutConstraint constraintWithItem:content
                                                                         attribute:NSLayoutAttributeHeight
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:nil attribute:NSLayoutAttributeNotAnAttribute
                                                                        multiplier:1 constant:0];
    
    [theScrollView addConstraint:constratintHeight];
    
    [_alertContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[theScrollView]-10-|" options:0 metrics:nil views:dict]];
    [_alertContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[canBtn]-10-|" options:0 metrics:nil views:dict]];
    [_alertContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-0-[theScrollView]-7-[canBtn(==%f)]-10-|", [self cellContentHeight]] options:0 metrics:nil views:dict]];
    
    scrollView = theScrollView;
    cancelButton = canBtn;
    
    contentView = content;
    contentHeight = constratintHeight;
}

-(NSAttributedString*)titleForObject:(id)object {
    
    return nil;
}

-(void)show {
    
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    if (window) {
        
        self.frame = window.bounds;
        [window addSubview:self];
        alertContentOriginY.constant = self.height;
        
        [self layoutIfNeeded];
        
        alertContentOriginY.constant = (self.height - alertContentHeight.constant);
        _alphaView.alpha = 0;
        
        [UIView animateWithDuration:0.25 delay:0.0f options:UIViewAnimationOptionCurveEaseInOut  animations:^{
            
            _alphaView.alpha = 0.8;
            [self layoutIfNeeded];
        } completion:^(BOOL finished) {
            
            [scrollView scrollRectToVisible:CGRectMake(0, contentHeight.constant-2, 3, 2) animated:YES];
        }];
    }
}

-(CGFloat)alertContentViewWidth {
    
    return [UIScreen mainScreen].bounds.size.width;
}

-(CGFloat)cellContentHeight {
    return 50;
}

-(NSString*)cancelButtonTitle {
    
    return @"Cancel";
}

-(void)objectSelected:(UIButton*)button {
    
    if (self.completionBlock) {
        self.completionBlock([objectViewMapping objectForKey:button]);
    }
    
    [self dismiss];
}

-(void)showObjects:(NSArray*)objects{
    
    __block UIView *lastAdded = nil;
    void (^ addView)(UIView *view) = ^void (UIView *view){
        
        view.translatesAutoresizingMaskIntoConstraints = NO;
        [contentView addSubview:view];
        
        NSDictionary *dictInte = NSDictionaryOfVariableBindings(view);
        [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:dictInte]];
        
        if (lastAdded) {
            
            NSDictionary *dict = NSDictionaryOfVariableBindings(lastAdded, view);
            [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lastAdded(==view)]-0-[view(==lastAdded)]" options:0 metrics:nil views:dict]];
            
            [view addBorderLineAtPosition:BorderLinePositionTop];
        }else {
            
            [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]" options:0 metrics:nil views:dictInte]];
        }
        
        lastAdded = view;
    };
    
    for ( id obj in objects ) {
        
        UIButton *cardView = [UIButton buttonWithType:UIButtonTypeCustom];
        [cardView.titleLabel setNumberOfLines:0];
        [cardView setTitleColor:cardView.tintColor forState:UIControlStateNormal];
        [cardView addTarget:self action:@selector(objectSelected:) forControlEvents:UIControlEventTouchUpInside];
        
        [cardView setAttributedTitle:[self titleForObject:obj] forState:UIControlStateNormal];
        
        addView(cardView);
        
        [objectViewMapping setObject:obj forKey:cardView];
    }
    
    [self addLastView:lastAdded totalView:objects.count];
    
    [self show];
}

-(void)addLastView:(UIView*)lastView totalView:(NSInteger)count{
    
    if (lastView) {
        
        NSDictionary *dict = NSDictionaryOfVariableBindings(lastView);
        [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lastView]-0-|" options:0 metrics:nil views:dict]];
    }
    
    contentHeight.constant = (count * [self cellContentHeight]);
    
    CGFloat alertHeight = (contentHeight.constant + [self cellContentHeight] + 7 + 10);
    
    if (alertHeight > ([UIScreen mainScreen].bounds.size.height-100)) {
        alertHeight = ([UIScreen mainScreen].bounds.size.height-100);
    }
    
    alertContentHeight.constant = alertHeight;
}

-(void)cancelClicked:(UIButton*)btn {
    
    if (self.completionBlock) {
        self.completionBlock(nil);
    }
    [self dismiss];
}

@end
