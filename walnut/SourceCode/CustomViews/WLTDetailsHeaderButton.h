//
//  WLTDetailsHeaderButton.h
//  walnut
//
//  Created by Abhinav Singh on 30/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLTDetailsHeaderButton : UIControl {
    
    UIImage *normalImage;
    UIImage *highlightedImage;
    
    __weak IBOutlet UIImageView *stateImageView;
@public
    
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UILabel *subtitleLabel;
}

-(void)setNormalImage:(NSString*)imageNormal highlighted:(NSString*)highImage;
-(void)setTitle:(NSString*)title andSubTitle:(NSString*)subTitle;

@end