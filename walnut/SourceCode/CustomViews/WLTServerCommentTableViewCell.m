//
//  WLTServerCommentTableViewCell.m
//  walnut
//
//  Created by Abhinav Singh on 01/03/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTServerCommentTableViewCell.h"
#import "WLTTransaction.h"

@implementation WLTServerCommentTableViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    commentLabel.textColor = [UIColor cadetColor];
    commentLabel.font = [UIFont sfUITextRegularOfSize:12];
    
    commentBackView.backgroundColor = [UIColor fullMoonColor];
    commentBackView.layer.borderWidth = 0.5;
    commentBackView.layer.borderColor = [UIColor placeHolderTextColor].CGColor;
    commentBackView.layer.cornerRadius = DefaultCornerRadius;
}

-(void)showComment:(NSString*)comment fullWidth:(BOOL)fullWidth{
    
    commentLabel.text = comment;
    
    if (fullWidth) {
        self.backgroundColor = [UIColor fullMoonColor];
        commentBackView.hidden = YES;
    }else{
        
        self.backgroundColor = [UIColor clearColor];
        commentBackView.hidden = NO;
    }
    
    [self invalidateIntrinsicContentSize];
}

@end
