//
//  WLTStepper.h
//  walnut
//
//  Created by Abhinav Singh on 01/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLTStepper : UIControl {
    
    __weak UIButton *reduceButton;
    __weak UIButton *increaseButton;
    __weak UILabel *amountLabel;
}

@property(nonatomic, assign) CGFloat value;

@end
