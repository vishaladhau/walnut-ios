//
//  WLTGroupContactTableViewCell.m
//  walnut
//
//  Created by Abhinav Singh on 02/09/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTGroupContactTableViewCell.h"

@implementation WLTGroupContactTableViewCell

-(void)addAllSubviews {
    
    [super addAllSubviews];
    
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectZero];
    [imageV setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    imageV.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:imageV];
    
    selectionImageView = imageV;
    _accessory = selectionImageView;
}

-(void)addLayoutConstraints {
    
    [super addLayoutConstraints];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:selectionImageView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView attribute:NSLayoutAttributeCenterY
                                                                multiplier:1 constant:0]];
}

-(void)setAccessoryColor:(UIColor*)color {
    
    selectionImageView.tintColor = color;
}

-(void)showSelectedContact:(BOOL)selected {
    
    if (selected) {
        
        [selectionImageView setImage:[[UIImage imageNamed:@"Checkmark"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    }else {
        [selectionImageView setImage:[UIImage imageNamed:@"EmptyCircle"]];
    }
}

@end
