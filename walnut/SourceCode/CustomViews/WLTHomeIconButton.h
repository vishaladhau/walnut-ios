//
//  WLTHomeIconButton.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 05/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "LSButton.h"

@interface WLTHomeIconButton : UIControl {
    
    @public
    __weak UIImageView *logoImageView;
}

-(void)applyHomeStyleAnimated:(BOOL)animate;
-(void)applySettingsStyleAnimated:(BOOL)animate;

@end
