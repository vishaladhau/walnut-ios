//
//  LSButton.m
//
//  Created by Abhinav Singh on 22/05/15.
//  Copyright (c) 2015 Leftshift Technologies Pvt. Ltd. All rights reserved.
//

#import "LSButton.h"

@implementation LSStateColor

- (instancetype)initWithBack:(UIColor*)bckClr
                contentColor:(UIColor*)contClr
                andBorderClr:(UIColor*)brdrClr {
    
    self = [super init];
    if (self) {
        
        _backColor = bckClr;
        _borderColor = brdrClr;
        _contentColor = contClr;
    }
    
    return self;
}

@end

@implementation LSButton

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        alingment = NSTextAlignmentCenter;
        
        stateColors = [NSMutableDictionary new];
        [self initialSetup];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    
    self = [super initWithCoder:coder];
    if (self) {
        
        alingment = NSTextAlignmentCenter;
        stateColors = [NSMutableDictionary new];
    }
    
    return self;
}

-(void)applyDefaultStyle {
    
    self.layer.cornerRadius = DefaultCornerRadius;
    
    LSStateColor *normalClr = [[LSStateColor alloc] initWithBack:[UIColor appleGreenColor]
                                                    contentColor:[UIColor whiteColor]
                                                    andBorderClr:nil];
    
    LSStateColor *highClr = [[LSStateColor alloc] initWithBack:[UIColor whiteColor]
                                                  contentColor:[UIColor appleGreenColor]
                                                  andBorderClr:[UIColor appleGreenColor]];
    
    [self addColors:highClr forState:LSButtonStateHighlighted];
    [self addColors:normalClr forState:LSButtonStateNormal];
}

-(void)awakeFromNib {
    
    [super awakeFromNib];
    [self initialSetup];
}

- (void)initialSetup {
    
    state = LSButtonStateUnknown;
    
    self.layer.masksToBounds = YES;
    self.layer.borderWidth = 1.0f;
    
    [self addAllSubViews];
    [self addAllConstraints];
    
    if (!stateColors[@(LSButtonStateNormal)]) {
        
        LSStateColor *normalClr = [[LSStateColor alloc] initWithBack:[UIColor whiteColor]
                                                        contentColor:[UIColor dogerBlueColor]
                                                        andBorderClr:nil];
        
        [stateColors setObject:normalClr forKey:@(LSButtonStateNormal)];
    }
    
    if (!stateColors[@(LSButtonStateHighlighted)]) {
        
        LSStateColor *highClr = [[LSStateColor alloc] initWithBack:[UIColor dogerBlueColor]
                                                        contentColor:[UIColor whiteColor]
                                                        andBorderClr:nil];
        
        [stateColors setObject:highClr forKey:@(LSButtonStateHighlighted)];
    }
    
    if (!stateColors[@(LSButtonStateDisabled)]) {
        
        LSStateColor *disabledClr = [[LSStateColor alloc] initWithBack:[UIColor hokiColor]
                                                      contentColor:[UIColor whiteColor]
                                                      andBorderClr:nil];
        
        [stateColors setObject:disabledClr forKey:@(LSButtonStateDisabled)];
    }
    
    [self changeStateTo:LSButtonStateNormal];
}

-(void)addAllSubViews {
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    titleLabel.font = [UIFont sfUITextRegularOfSize:16];
    titleLabel.textAlignment = alingment;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = titleText;
    [self addSubview:titleLabel];
}

-(void)addAllConstraints {
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(titleLabel);
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[titleLabel]-5-|" options:0 metrics:nil views:dict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[titleLabel]-0-|" options:0 metrics:nil views:dict]];
}

-(void)setTitle:(NSString*)title {
    
    titleText = title;
    titleLabel.text = title;
}

-(void)setTextAlingment:(NSTextAlignment)aling {
    
    alingment = aling;
    titleLabel.textAlignment = aling;
}

- (void)changeStateTo:(LSButtonState)newState {
    
    if (newState != state) {
        
        state = newState;
        [self changeDisplayForCurrentState];
    }
}

- (void)changeDisplayForCurrentState {
    
    LSStateColor *clrsData = [stateColors objectForKey:@(state)];
    
    if (clrsData.borderColor) {
        
        self.layer.borderWidth = 0.5f;
        self.layer.borderColor = [clrsData.borderColor CGColor];
    }else {
        self.layer.borderWidth = 0.0f;
        self.layer.borderColor = nil;
    }
    
    titleLabel.textColor = clrsData.contentColor;
    self.backgroundColor = clrsData.backColor;
}

-(void)addColors:(LSStateColor*)clrs forState:(LSButtonState)forState {
    
    [stateColors setObject:clrs forKey:@(forState)];
    
    if (forState == state) {
        [self changeDisplayForCurrentState];
    }
}

-(void)setHighlighted:(BOOL)highlighted {
    
    [super setHighlighted:highlighted];
    
    if (self.isEnabled) {
        
        if (self.isHighlighted || self.isSelected) {
            [self changeStateTo:LSButtonStateHighlighted];
        }else {
            [self changeStateTo:LSButtonStateNormal];
        }
    }
}

-(void)setSelected:(BOOL)selected {
    
    [super setSelected:selected];
    
    if (self.isEnabled) {
        if (self.isHighlighted || self.isSelected) {
            [self changeStateTo:LSButtonStateHighlighted];
        }else {
            [self changeStateTo:LSButtonStateNormal];
        }
    }
}

-(void)setEnabled:(BOOL)enabled {
    
    [super setEnabled:enabled];
    
    if (!self.enabled) {
        [self changeStateTo:LSButtonStateDisabled];
    }else {
        if (self.isHighlighted || self.isSelected) {
            [self changeStateTo:LSButtonStateHighlighted];
        }else {
            [self changeStateTo:LSButtonStateNormal];
        }
    }
}

@end
