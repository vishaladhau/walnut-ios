//
//  WLTSplitFooterView.m
//  walnut
//
//  Created by Abhinav Singh on 01/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTSplitFooterView.h"

@implementation WLTSplitFooterView

-(void)awakeFromNib {
    
    [super awakeFromNib];
    [self initialSetup];
}

-(void)initialSetup {
    
    [self addBorderLineAtPosition:(BorderLinePositionBottom|BorderLinePositionTop)];
    
    remTextLabel.font = [UIFont sfUITextMediumOfSize:14];
    remTextLabel.textColor = [UIColor cadetColor];
    remTextLabel.text = @"Remaining amount";
    
    remAmountLabel.font = [UIFont sfUITextMediumOfSize:14];
    remAmountLabel.textColor = [UIColor cadetColor];
    
    LSStateColor *nrmlClr = [[LSStateColor alloc] initWithBack:[UIColor whiteColor]
                                                  contentColor:[UIColor appleGreenColor]
                                                  andBorderClr:[UIColor cadetColor]];
    
    LSStateColor *highClr = [[LSStateColor alloc] initWithBack:[UIColor appleGreenColor]
                                                  contentColor:[UIColor whiteColor]
                                                  andBorderClr:nil];
    
    [distrubuteButton addColors:nrmlClr forState:LSButtonStateNormal];
    [distrubuteButton addColors:highClr forState:LSButtonStateHighlighted];
    distrubuteButton.layer.cornerRadius = DefaultCornerRadius;
    [distrubuteButton addTarget:self action:@selector(distributeClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [distrubuteButton setTitle:@"DISTRIBUTE PROPORTIONALLY"];
}

-(void)distributeClicked:(LSButton*)btn {
    
    [self.delegate splitFooterViewDistributeClicked:self];
}

-(void)setRemamingAmount:(CGFloat)remaning {
    
    if(remaning < 0) {
        remAmountLabel.textColor = [UIColor oweColor];
    }else {
        remAmountLabel.textColor = [UIColor cadetColor];
    }
    remAmountLabel.text = @(remaning).displayCurrencyString;
}

-(CGSize)intrinsicContentSize {
    return CGSizeMake(UIViewNoIntrinsicMetric, 100);
}

@end
