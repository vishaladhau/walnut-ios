//
//  WLTGroupNameTableViewCell.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 07/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTGroupNameTableViewCell.h"

@implementation WLTGroupNameTableViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.accessoryType = UITableViewCellAccessoryNone;
    
    nameTextField.font = [UIFont sfUITextRegularOfSize:16];
    nameTextField.textColor = [UIColor cadetColor];
    nameTextField.clearButtonMode = UITextFieldViewModeAlways;
}

@end
