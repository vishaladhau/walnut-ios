//
//  WLTSendRequestMoneyButton.h
//  walnut
//
//  Created by Abhinav Singh on 12/08/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "LSButton.h"

@interface WLTSendRequestMoneyButton : LSButton {
    
    UIColor *imageColor;
    __weak UIImageView *imageView;
}

-(void)setImage:(UIImage *)image andColor:(UIColor *)color;

@end
