//
//  WLTAdminTableViewCell.m
//  walnut
//
//  Created by Abhinav Singh on 22/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTAdminTableViewCell.h"

@implementation WLTAdminTableViewCell

-(void)addAllSubviews {
    
    [super addAllSubviews];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    [label setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.font = [UIFont sfUITextRegularOfSize:12];
    label.textColor = [UIColor whiteColor];
    label.clipsToBounds = YES;
    label.backgroundColor = [UIColor appleGreenColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"Owner";
    label.layer.cornerRadius = DefaultCornerRadius;
    [self.contentView addSubview:label];
    
    adminLabel = label;
    _accessory = adminLabel;
}

-(void)addLayoutConstraints {
    
    [super addLayoutConstraints];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(adminLabel);
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[adminLabel(==20)]" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[adminLabel(==45)]" options:0 metrics:nil views:dict]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:adminLabel
                                                                 attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual
                                                                    toItem:_titleTextLabel attribute:NSLayoutAttributeTop
                                                                multiplier:1 constant:0]];
}

@end
