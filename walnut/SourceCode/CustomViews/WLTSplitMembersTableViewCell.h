//
//  WLTSplitMembersTableViewCell.h
//  walnut
//
//  Created by Abhinav Singh on 03/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WLTUserIconButton;
@class WLTTransactionUser;

@interface WLTSplitMembersTableViewCell : UITableViewCell {
    
    __weak IBOutlet WLTUserIconButton *userButton;
    __weak IBOutlet UILabel *amountLabel;
    __weak IBOutlet UILabel *userNameLabel;
}

-(void)showDetailsOfTransactionUser:(WLTTransactionUser*)user;

@end
