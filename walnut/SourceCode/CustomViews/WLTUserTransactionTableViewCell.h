//
//  WLTUserTransactionTableViewCell.h
//  walnut
//
//  Created by Abhinav Singh on 02/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WLTTransaction;

@interface WLTUserTransactionTableViewCell : UITableViewCell {
    
    __weak IBOutlet UILabel *userNameLabel;
    __weak IBOutlet UILabel *amountLabel;
    __weak IBOutlet UILabel *dateLabel;
}

-(void)showDetailsOfTransaction:(WLTTransaction*)trans
                       fromUser:(id<User>)userOther;

@end
