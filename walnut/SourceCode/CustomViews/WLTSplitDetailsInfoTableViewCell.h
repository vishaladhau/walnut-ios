//
//  WLTSplitDetailsInfoTableViewCell.h
//  walnut
//
//  Created by Abhinav Singh on 03/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WLTTransaction;

@interface WLTSplitDetailsInfoTableViewCell : UITableViewCell {
    
    __weak IBOutlet UIView *topBackView;
    __weak IBOutlet UIView *bottomBackView;
    
    __weak IBOutlet UILabel *paidByLabel;
    __weak IBOutlet UILabel *splitNameLabel;
    __weak IBOutlet UILabel *amountLabel;
    __weak IBOutlet UILabel *addedByLabel;
    __weak IBOutlet UILabel *dateLabel;
}

-(void)showDetailsOfTransaction:(WLTTransaction*)trans;

@end
