//
//  WLTIFSCResultsTableViewCell.h
//  walnut
//
//  Created by Abhinav Singh on 08/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GTLWalnutMIfscSearchObject;

@interface WLTIFSCResultsTableViewCell : UITableViewCell {
    
    __weak IBOutlet UILabel *codeLabel;
    __weak IBOutlet UILabel *addressLabel;
}

-(void)showDetailsOfSearchObject:(GTLWalnutMIfscSearchObject*)search searchString:(NSString*)searchString;

@end
