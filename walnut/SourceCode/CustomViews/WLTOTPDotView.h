//
//  WLTOTPDotView.h
//  walnut
//
//  Created by Abhinav Singh on 03/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTOTPView.h"

@interface WLTOTPDotView : WLTDotView {
    
    CAShapeLayer *shapeLayer;
    UIColor *contentColor;
    
    BOOL awatingToFillup;
}

@end
