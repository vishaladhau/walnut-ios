//
//  WLTCombinedGroupPaymentsView.m
//  walnut
//
//  Created by Abhinav Singh on 02/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTCombinedGroupPaymentsView.h"
#import "WLTColorPalette.h"
#import "WLTDatabaseManager.h"

@implementation WLTCombinedGroupPaymentsView

-(void)awakeFromNib {
    
    [super awakeFromNib];
    [self initialSetup];
}

-(void)initialSetup {
    
    [self addBorderLineAtPosition:BorderLinePositionBottom];
    
    viewDataMapping = [NSMapTable weakToWeakObjectsMapTable];
    
    UITapGestureRecognizer *tapGest = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mainViewTapped:)];
    tapGest.delegate = self;
    tapGest.numberOfTapsRequired = 1;
    tapGest.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tapGest];
    
    tapGest = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(groupViewTapped:)];
    tapGest.numberOfTapsRequired = 1;
    tapGest.numberOfTouchesRequired = 1;
    [multipleGroupsBackView addGestureRecognizer:tapGest];
    
    userNameLabel.textColor = [UIColor darkGrayTextColor];
    userNameLabel.font = [UIFont sfUITextRegularOfSize:16];
    
    groupNameLabel.textColor = [UIColor darkGrayTextColor];
    groupNameLabel.font = [UIFont sfUITextRegularOfSize:12];
    
    [reminderButton setWalnutRemindStyle];
}

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    
    CGPoint point = [gestureRecognizer locationInView:self];
    if (CGRectContainsPoint(actionButton.frame, point) || CGRectContainsPoint(reminderButton.frame, point) || CGRectContainsPoint(userIconButton.frame, point)) {
        return NO;
    }
    
    return YES;
}

-(void)mainViewTapped:(UITapGestureRecognizer*)tapGest {
    
    if (_paymentGroup.groupsData.count == 1) {
        
        WLTGroupPaymentData *dataGrp = [self.paymentGroup.groupsData firstObject];
        if (dataGrp) {
            [self.delegate combinedPayCell:self selectedGroup:dataGrp.group];
        }
    }
}

-(void)groupViewTapped:(UITapGestureRecognizer*)tapGest {
    
    CGPoint point = [tapGest locationInView:multipleGroupsBackView];
    for ( UIView *view in multipleGroupsBackView.subviews ) {
        
        if (CGRectContainsPoint(view.frame, point)) {
            WLTGroupPaymentData *dataGrp = [viewDataMapping objectForKey:view];
            if (dataGrp) {
                
                [self.delegate combinedPayCell:self selectedGroup:dataGrp.group];
                break;
            }
        }
    }
}

-(IBAction)reminderButtonClicked:(id)sender {
    
    [self.delegate reminderButtonClickedFromCell:self];
}

-(IBAction)actionButtonClicked:(id)sender {
    
    [self.delegate actionButtonClickedFromCell:self];
}

-(void)showDetailsOfData:(WLTCombinedGroupPayments*)data {
    
    _paymentGroup = data;
    
    CGFloat sceenWidth = [[UIScreen mainScreen] bounds].size.width;
    
    [userIconButton displayUserIcon:data.user];
    [userNameLabel displayUserNameOrNumberOfUser:data.user];
    
    UIColor *actionColor = nil;
    if ( (data.transactionState == UserTransactionStateOwe) || (data.transactionState == UserTransactionStateGet)) {
        
        userAmountLabel.font = [UIFont sfUITextRegularOfSize:15];
        
        NSString *prefix = nil;
        if (data.transactionState == UserTransactionStateOwe) {
            
            prefix = @"You Owe";
            
            if (PaymentsEnabled()) {
                actionButton.hidden = NO;
            }else {
                actionButton.hidden = YES;
            }
            
            reminderButton.hidden = YES;
            
            actionColor = [UIColor walnutIconColor];
            userNameGapConstraints.constant = 10;
            [actionButton setWalnutPayStyle];
        }
        else {
            
            prefix = @"You Get";
            
            actionButton.hidden = NO;
            reminderButton.hidden = NO;
            
            WLTGroupPaymentData *firstGrp = [data.groupsData firstObject];
            if (firstGrp.group.isPrivate.boolValue) {
                
                reminderButton.hidden = YES;
                userNameGapConstraints.constant = 60;
            }else {
                
                userNameGapConstraints.constant = 110;
            }
            
            actionColor = [UIColor getColor];
            [actionButton setWalnutSettleStyle];
        }
        
        userAmountLabel.textColor = actionColor;
        [userNameLabel setPreferredMaxLayoutWidth:(sceenWidth-(64+userNameGapConstraints.constant))];
        
        NSString *complete = [NSString stringWithFormat:@"%@ %@", prefix, @(data.amount).displayCurrencyString];
        NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:complete];
        [attributed addAttributes:@{NSForegroundColorAttributeName:[UIColor cadetColor], NSFontAttributeName:[UIFont sfUITextRegularOfSize:12]} range:[complete rangeOfString:prefix]];
        
        userAmountLabel.attributedText = attributed;
        
        NSMutableArray *toShowHere = [NSMutableArray new];
        for ( WLTGroupPaymentData *dataGrp in data.groupsData ) {
            
            if ( (dataGrp.transactionState == UserTransactionStateOwe) || (dataGrp.transactionState == UserTransactionStateGet) ) {
                [toShowHere addObject:dataGrp];
            }
        }
        
        if (toShowHere.count == 1) {
            
            WLTGroupPaymentData *dataGrp = [data.groupsData firstObject];
            [groupNameLabel displayGroupName:dataGrp.group checkDirect:YES];
            
            heightConstraint.constant = 0;
        }else {
            
            groupNameLabel.text = nil;
            CGFloat heigthEach = 45;
            CGFloat vGap = 5;
            
            NSDictionary *dictMetrics = @{@"height":@(heigthEach), @"vGap":@(vGap)};
            
            UIView *lastAdded = nil;
            for ( WLTGroupPaymentData *dataGrp in toShowHere ) {
                
                UIView *viewBack = [[UIView alloc] initWithFrame:CGRectZero];
                viewBack.layer.cornerRadius = 3;
                viewBack.backgroundColor = [UIColor lightBackgroundColor];
                viewBack.translatesAutoresizingMaskIntoConstraints = NO;
                [multipleGroupsBackView addSubview:viewBack];
                
                UILabel *grpName = [[UILabel alloc] initWithFrame:CGRectZero];
                grpName.translatesAutoresizingMaskIntoConstraints = NO;
                grpName.textColor = [UIColor darkGrayTextColor];
                grpName.font = [UIFont sfUITextRegularOfSize:16];
                grpName.textAlignment = NSTextAlignmentLeft;
                [grpName displayGroupName:dataGrp.group checkDirect:YES];
                [viewBack addSubview:grpName];
                
                NSString *prefix2 = @"";
                if (dataGrp.transactionState == UserTransactionStateGet) {
                    prefix2 = @"You Get";
                }
                else if (dataGrp.transactionState == UserTransactionStateOwe) {
                    prefix2 = @"You Owe";
                }
                
                NSString *complete2 = [NSString stringWithFormat:@"%@ %@", prefix2, @(dataGrp.amount).displayCurrencyString];
                NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:complete2];
                [attributed addAttributes:@{NSForegroundColorAttributeName:[UIColor cadetColor], NSFontAttributeName:[UIFont sfUITextRegularOfSize:12]} range:[complete2 rangeOfString:prefix2]];
                
                UILabel *amtLabel = [[UILabel alloc] initWithFrame:CGRectZero];
                amtLabel.translatesAutoresizingMaskIntoConstraints = NO;
                [amtLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
                amtLabel.font = [UIFont sfUITextRegularOfSize:16];
                amtLabel.textAlignment = NSTextAlignmentLeft;
                [viewBack addSubview:amtLabel];
                
                if ([dataGrp.group userTransactionState] == UserTransactionStateOwe) {
                    amtLabel.textColor = [UIColor oweColor];
                }else {
                    amtLabel.textColor = [UIColor getColor];
                }
                amtLabel.attributedText = attributed;
                
                NSDictionary *dict = NSDictionaryOfVariableBindings(viewBack, grpName, amtLabel);
                [multipleGroupsBackView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[viewBack]-0-|" options:0 metrics:dictMetrics views:dict]];
                [viewBack addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[grpName]-10-[amtLabel]-10-|" options:0 metrics:nil views:dict]];
                [viewBack addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[grpName]-5-|" options:0 metrics:nil views:dict]];
                [viewBack addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[amtLabel]-5-|" options:0 metrics:nil views:dict]];
                
                if (lastAdded) {
                    
                    dict = NSDictionaryOfVariableBindings(viewBack, lastAdded);
                    [multipleGroupsBackView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lastAdded]-vGap-[viewBack(==height)]" options:0 metrics:dictMetrics views:dict]];
                }else {
                    
                    [multipleGroupsBackView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-vGap-[viewBack(==height)]" options:0 metrics:dictMetrics views:dict]];
                }
                
                lastAdded = viewBack;
                
                [viewDataMapping setObject:dataGrp forKey:viewBack];
            }
            
            heightConstraint.constant = ((heigthEach+vGap)*toShowHere.count);
        }
    }
    else {
        
        actionButton.hidden = YES;
        reminderButton.hidden = YES;
        
        groupNameLabel.text = nil;
        heightConstraint.constant = 0;
        
        userNameGapConstraints.constant = 10;
        
        userAmountLabel.font = [UIFont sfUITextMediumOfSize:15];
        userAmountLabel.textColor = [UIColor cadetColor];
        userAmountLabel.text = @"Settled";
        
        [userNameLabel setPreferredMaxLayoutWidth:(sceenWidth-(64+userNameGapConstraints.constant))];
    }
}

-(CGSize)intrinsicContentSize {
    
    CGSize size = CGSizeMake(UIViewNoIntrinsicMetric, 0);
    size.height += 10;
    size.height += [userNameLabel intrinsicContentSize].height;
    size.height += [userAmountLabel intrinsicContentSize].height;
    
    if (groupNameLabel.text.length) {
        
        size.height += [groupNameLabel intrinsicContentSize].height;
        if (size.height < 54) {
            size.height = 54;
        }
    }else {
        
        if (size.height < 54) {
            size.height = 54;
        }
        size.height += heightConstraint.constant;
    }
    
    size.height += 10;
    return size;
}

@end