//
//  WLTOTPView.h
//  walnut
//
//  Created by Abhinav Singh on 15/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLTDotView : UIView {
    
}

- (instancetype)initWithFrame:(CGRect)frame;

@property(nonatomic, strong) NSString *text;

-(void)animateFromEmptyToFilledUp;
-(void)animateFromFilledUpToEmpty;
-(void)doFinalStepOfFilledUpAnimation;

@end

@interface WLTOTPView : UIControl <UIKeyInput> {
    
    NSMutableArray *dotsArray;
    UIView *contentView;
}

@property(readonly, assign) NSInteger enteredDigits;

@property(nonatomic, assign) CGFloat dotsSize;
@property(nonatomic, assign) CGFloat dotsGap;

-(NSString*)text;
-(void)reset;

-(void)setupForOTPCount:(int)cnt andDotViewClass:(Class)dotViewClass;

@end
