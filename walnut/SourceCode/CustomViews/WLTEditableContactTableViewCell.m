//
//  WLTEditableContactTableViewCell.m
//  walnut
//
//  Created by Abhinav Singh on 03/09/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTEditableContactTableViewCell.h"

@implementation WLTEditableContactTableViewCell

-(void)addAllSubviews {
    
    [super addAllSubviews];
    
    UIButton *rButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [rButton addTarget:self action:@selector(removeClicked:) forControlEvents:UIControlEventTouchUpInside];
    [rButton setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    rButton.translatesAutoresizingMaskIntoConstraints = NO;
    [rButton.titleLabel setFont:[UIFont sfUITextRegularOfSize:12]];
    [rButton setTitle:@"REMOVE" forState:UIControlStateNormal];
    [rButton setTintColor:[UIColor flamePeaColor]];
    [self.contentView addSubview:rButton];
    
    removeButton = rButton;
    _accessory = rButton;
}

-(void)removeClicked:(UIButton*)btn {
    
    [self.delegate removeContactClickedFromCell:self];
}

-(void)addLayoutConstraints {
    
    [super addLayoutConstraints];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:removeButton
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1 constant:0]];
}

@end
