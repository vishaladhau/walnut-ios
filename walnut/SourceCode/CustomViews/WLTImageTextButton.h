//
//  WLTImageTextButton.h
//  walnut
//
//  Created by Abhinav Singh on 25/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "LSButton.h"

@interface WLTImageTextButton : LSButton {
    
    UIImage *buttonImage;
    __weak UIImageView *imageView;
}

-(void)setColor:(UIColor*)color;
-(void)setImage:(UIImage*)img;

@end
