//
//  WLTDetailsHeaderButton.m
//  walnut
//
//  Created by Abhinav Singh on 30/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTDetailsHeaderButton.h"

@implementation WLTDetailsHeaderButton

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont sfUITextLightOfSize:16];
    
    subtitleLabel.textColor = [UIColor whiteColor];
    subtitleLabel.font = [UIFont sfUITextRegularOfSize:10];
}

-(void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    if ([self isHighlighted]) {
        stateImageView.image = highlightedImage;
    }else {
        stateImageView.image = normalImage;
    }
}

-(void)setNormalImage:(NSString*)imageNormal highlighted:(NSString*)highImage {
    
    normalImage = [UIImage imageNamed:imageNormal];
    highlightedImage = [UIImage imageNamed:highImage];
    
    [self setHighlighted:[self isHighlighted]];
}

-(void)setTitle:(NSString*)title andSubTitle:(NSString*)subTitle {
    
    titleLabel.text = title;
    subtitleLabel.text = subTitle;
}

@end