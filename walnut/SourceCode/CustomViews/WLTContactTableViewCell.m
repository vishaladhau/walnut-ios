//
//  WLTContactTableViewCell.m
//  walnut
//
//  Created by Abhinav Singh on 26/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTContactTableViewCell.h"
#import "WLTContactsManager.h"
#import "WLTUserIconButton.h"

@implementation WLTContactTableViewCell

-(void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self initialSetup];
    }
    
    return self;
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    [self initialSetup];
}

-(void)initialSetup {
    
    self.accessoryType = UITableViewCellAccessoryNone;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeUserName:) name:NotifyContactListChanged object:nil];
    
    [self addAllSubviews];
    [self addLayoutConstraints];
}

-(void)changeUserName:(NSNotification*)notify {
    
    if (self.displayedUser) {
        [self showInformation:self.displayedUser];
    }
}

-(UIEdgeInsets)layoutMargins {
    return UIEdgeInsetsZero;
}

-(void)addAllSubviews {
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    titleLabel.textColor = [UIColor cadetColor];
    titleLabel.font = [UIFont sfUITextRegularOfSize:16];
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.minimumScaleFactor = 0.5;
    titleLabel.numberOfLines = 1;
    [self.contentView addSubview:titleLabel];
    
    UILabel *numberTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    numberTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    numberTitleLabel.textColor = [UIColor cadetColor];
    numberTitleLabel.font = [UIFont sfUITextLightOfSize:12];
    numberTitleLabel.adjustsFontSizeToFitWidth = YES;
    numberTitleLabel.minimumScaleFactor = 0.5;
    numberTitleLabel.numberOfLines = 1;
    [self.contentView addSubview:numberTitleLabel];
    
    WLTUserIconButton *iconButton = [[WLTUserIconButton alloc] initWithFrame:CGRectZero];
    [iconButton setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    [iconButton setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    iconButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:iconButton];
    
    _numberTextLabel = numberTitleLabel;
    _iconButton = iconButton;
    _titleTextLabel = titleLabel;
}

-(void)addLayoutConstraints {
    
    [self addDefaultConstraints];
    
    if (_accessory) {
        
        NSDictionary *dict = NSDictionaryOfVariableBindings(_titleTextLabel, _numberTextLabel, _accessory);
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_titleTextLabel]-5-[_accessory]-10-|" options:0 metrics:nil views:dict]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_numberTextLabel]-5-[_accessory]-10-|" options:0 metrics:nil views:dict]];
    }else {
        
        NSDictionary *dict = NSDictionaryOfVariableBindings(_titleTextLabel, _numberTextLabel);
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_titleTextLabel]-10-|" options:0 metrics:nil views:dict]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_numberTextLabel]-10-|" options:0 metrics:nil views:dict]];
    }
}

-(void)addDefaultConstraints {
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(_iconButton, _titleTextLabel, _numberTextLabel);
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[_iconButton(==37)]-5-[_titleTextLabel]" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[_iconButton(==37)]-5-[_numberTextLabel]" options:0 metrics:nil views:dict]];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_iconButton(==37)]" options:0 metrics:nil views:dict]];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[_titleTextLabel]-0-[_numberTextLabel]-10-|" options:0 metrics:nil views:dict]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_iconButton
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1 constant:0]];
}

-(void)showInformation:(id<User>)usr {
    
    _displayedUser = usr;
    
    _numberTextLabel.text = [usr completeNumber];
    [_iconButton displayUserIcon:usr];
    
    _titleTextLabel.text = @"...";
    __weak typeof(self) weakSelf = self;
    [[WLTContactsManager sharedManager] displayNameForUser:usr defaultIsMob:YES completion:^(NSString *name) {
        
        if (weakSelf) {
            if (!name) {
                name = @"Unknown";
            }
            _titleTextLabel.text = name;
        }
    }];
}

@end
