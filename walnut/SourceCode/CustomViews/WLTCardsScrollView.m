//
//  WLTCardsScrollView.m
//  walnut
//
//  Created by Abhinav Singh on 08/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTCardsScrollView.h"
#import "WLTEmptyCardView.h"
#import "WLTCardView.h"
#import "GTLWalnutMPaymentInstrument.h"

static CGFloat GapBetweenCards = 0;
static CGFloat StartCardGaps = 15;

#define SWIPE_UP_THRESHOLD -1000.0f
#define SWIPE_DOWN_THRESHOLD 1000.0f
#define SWIPE_LEFT_THRESHOLD -1000.0f
#define SWIPE_RIGHT_THRESHOLD 1000.0f

@interface WLTCardsScrollView ()

@property(readonly, assign) NSInteger selectedIndex;
-(void)setSelectedIndex:(NSInteger)selectedIndex animated:(BOOL)animated;

@end

@implementation WLTCardsScrollView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initialSetup];
    }
    
    return self;
}

-(void)awakeFromNib {
    
    [super awakeFromNib];
    [self initialSetup];
}

-(void)initialSetup {
    
    self.clipsToBounds = YES;
    
    UIPanGestureRecognizer *panGest = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanSwipe:)];
    panGest.minimumNumberOfTouches = 1;
    panGest.maximumNumberOfTouches = 1;
    [self addGestureRecognizer:panGest];
    
    UIView *cView = [[UIView alloc] initWithFrame:CGRectZero];
    cView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:cView];
    
    contentView = cView;
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(cView);
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[cView]-0-|" options:0 metrics:nil views:dict]];
    
    NSLayoutConstraint *leadingC = [NSLayoutConstraint constraintWithItem:cView attribute:NSLayoutAttributeLeading
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:self attribute:NSLayoutAttributeLeading
                                multiplier:1 constant:0];
    
    NSLayoutConstraint *widthC = [NSLayoutConstraint constraintWithItem:cView attribute:NSLayoutAttributeWidth
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:nil attribute:NSLayoutAttributeNotAnAttribute
                                                               multiplier:1 constant:0];
    [self addConstraint:leadingC];
    [self addConstraint:widthC];
    
    contentWidthConstraint = widthC;
    contentLeadingConstraint = leadingC;
    
    _selectedIndex = -1;
}

-(void)selectCardOfID:(NSString*)cardID animated:(BOOL)animate {
    
    if (cardID.length) {
        
        for ( WLTCardView *cardV in self.cardViews ) {
            
            if ([cardV isKindOfClass:[WLTCardView class]]) {
                
                if ([cardV.instrument.instrumentUuid isEqualToString:cardID]) {
                    
                    NSInteger index = [self.cardViews indexOfObject:cardV];
                    [self setSelectedIndex:index animated:animate];
                    break;
                }
            }
        }
    }else {
        
        [self setSelectedIndex:0 animated:animate];
    }
    
}

- (void)handlePanSwipe:(UIPanGestureRecognizer*)recognizer {
    
    CGPoint currentPoint = [recognizer locationInView:self];
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        
        //Touches Just Started
        initialPoint = currentPoint;
        initialLeadingX = contentLeadingConstraint.constant;
    }
    else if (recognizer.state == UIGestureRecognizerStateChanged) {
        
        //Touches moved
        CGFloat changed = ( currentPoint.x - initialPoint.x);
        contentLeadingConstraint.constant = (changed + initialLeadingX);
    }
    else if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        CGPoint vel = [recognizer velocityInView:recognizer.view];
        if (vel.x < SWIPE_LEFT_THRESHOLD) {
            
            [self showNextView];
        }
        else if (vel.x > SWIPE_RIGHT_THRESHOLD) {
            
            [self showPreviousView];
        }
        else {
            
            [self adjustToClosestView];
        }
    }else {
        
        [self adjustToClosestView];
    }
}

-(void)adjustToClosestView {
    
    UIView *closestView = nil;
    CGFloat prevMax = 0;
    for ( UIView *cardV in self.cardViews ) {
        
        CGRect overMe = [self convertRect:cardV.frame fromView:contentView];
        CGRect intersection = CGRectIntersection(self.bounds, overMe);
        
        if (intersection.size.width > 0) {
            if (intersection.size.width > prevMax) {
                
                prevMax = intersection.size.width;
                closestView = cardV;
            }
        }
    }
    
    if (closestView) {
        
        NSInteger index = [self.cardViews indexOfObject:closestView];
        if ((index != NSNotFound) && (index != self.selectedIndex)) {
            
            NSInteger prev = self.selectedIndex;
            [self setSelectedIndex:index animated:YES];
            
            if (prev != self.selectedIndex) {
                
                [self.delegate cardsScrollView:self selectedIndexChanged:self.selectedIndex];
            }
        }else {
            
            [self changeCenterOffsetForSelectedindexAnimated:YES];
        }
    }
}

-(void)showNextView {
    
    NSInteger prev = self.selectedIndex;
    [self setSelectedIndex:(prev+1) animated:YES];
    
    if (prev != self.selectedIndex) {
        [self.delegate cardsScrollView:self selectedIndexChanged:self.selectedIndex];
    }else {
        [self changeCenterOffsetForSelectedindexAnimated:YES];
    }
}

-(void)showPreviousView {
    
    NSInteger prev = self.selectedIndex;
    [self setSelectedIndex:(prev-1) animated:YES];
    
    if (prev != self.selectedIndex) {
        
        [self.delegate cardsScrollView:self selectedIndexChanged:self.selectedIndex];
    }else {
        
        [self changeCenterOffsetForSelectedindexAnimated:YES];
    }
}

-(void)addSubviewsForCards:(NSArray*)instruments {
    
    [self.cardViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    NSMutableArray *allCardsView = [NSMutableArray new];
    CGFloat cardHeight = 150;
    CGFloat width = (screenWidth - (StartCardGaps * 2.0f));
    
    NSDictionary *metrics = @{@"hStartGap":@(StartCardGaps), @"hbGap":@(GapBetweenCards), @"cHeight":@(cardHeight), @"cWidth":@(width)};
    
    WLTEmptyCardView *viewEmpty = [WLTEmptyCardView initWithDefaultXib];
    viewEmpty.translatesAutoresizingMaskIntoConstraints = NO;
    [contentView addSubview:viewEmpty];
    
    _emptyCardView = viewEmpty;
    
    UIView *lastAddedView = viewEmpty;
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(viewEmpty);
    [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[viewEmpty(==cHeight)]" options:0 metrics:metrics views:dict]];
    [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-hStartGap-[viewEmpty(==cWidth)]" options:0 metrics:metrics views:dict]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:viewEmpty attribute:NSLayoutAttributeCenterY
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:contentView attribute:NSLayoutAttributeCenterY
                                                           multiplier:1 constant:0]];
    
    [allCardsView addObject:viewEmpty];
    
    for ( GTLWalnutMPaymentInstrument *instr in instruments  ) {
        
        WLTCardView *view = [WLTCardView initWithDefaultXib];
        [view showDetailsOfInstrument:instr];
        view.translatesAutoresizingMaskIntoConstraints = NO;
        [contentView addSubview:view];
        
        dict = NSDictionaryOfVariableBindings(view, lastAddedView);
        [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(==cHeight)]" options:0 metrics:metrics views:dict]];
        [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[lastAddedView]-hbGap-[view(==cWidth)]" options:0 metrics:metrics views:dict]];
        [contentView addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeCenterY
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:contentView attribute:NSLayoutAttributeCenterY
                                                               multiplier:1 constant:0]];
        lastAddedView = view;
        [allCardsView addObject:view];
    }
    
    _cardViews = allCardsView;
    
    NSInteger viewsCount = allCardsView.count;
    contentWidthConstraint.constant = ((width * viewsCount) + (GapBetweenCards * (viewsCount-1)) + (StartCardGaps * 2));
    
    [self setSelectedIndex:0 animated:NO];
}

-(void)setSelectedIndex:(NSInteger)selectedIndex animated:(BOOL)animated {
    
    if (_selectedIndex != selectedIndex) {
        
        if ((selectedIndex < self.cardViews.count) && (selectedIndex >= 0)) {
            
            _selectedIndex = selectedIndex;
            if (_selectedIndex == 0) {
                _selectedCardView = nil;
            }else {
                _selectedCardView = self.cardViews[_selectedIndex];
            }
            
            [self changeCenterOffsetForSelectedindexAnimated:animated];
        }
    }

}

-(void)changeCenterOffsetForSelectedindexAnimated:(BOOL)animate {
    
    [self layoutIfNeeded];
    
    UIView *view = self.cardViews[self.selectedIndex];
    CGPoint viewCenter = view.center;
    
    contentLeadingConstraint.constant = -(viewCenter.x - (self.width/2));
    
    if (animate) {
        [UIView animateWithDuration:0.3 animations:^{
            
            [self layoutIfNeeded];
        }];
    }else {
        
        [self layoutIfNeeded];
    }
}

@end
