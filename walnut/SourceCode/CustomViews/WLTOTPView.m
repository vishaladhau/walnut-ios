//
//  WLTOTPView.m
//  walnut
//
//  Created by Abhinav Singh on 15/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTOTPView.h"

@implementation WLTDotView

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.userInteractionEnabled = NO;
    }
    
    return self;
}

-(void)doFinalStepOfFilledUpAnimation {}

-(void)animateFromEmptyToFilledUp {}

-(void)animateFromFilledUpToEmpty {}

-(void)setText:(NSString*)text {
    
    if (!_text.length && text.length) {
        
        _text = text;
        [self animateFromEmptyToFilledUp];
    }else if (_text.length && !text.length){
        
        _text = text;
        [self animateFromFilledUpToEmpty];
    }else {
        _text = text;
    }
}

@end

@implementation WLTOTPView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initialSetup];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    
    self = [super initWithCoder:coder];
    if (self) {
        
        [self initialSetup];
    }
    
    return self;
}

-(UITextAutocorrectionType)autocorrectionType {
    return UITextAutocorrectionTypeNo;
}

-(UITextAutocapitalizationType)autocapitalizationType {
    return UITextAutocapitalizationTypeNone;
}

-(UITextSpellCheckingType)spellCheckingType {
    return UITextSpellCheckingTypeNo;
}

-(UIKeyboardType)keyboardType {
    return UIKeyboardTypeNumberPad;
}

-(UIKeyboardAppearance)keyboardAppearance {
    return UIKeyboardAppearanceDefault;
}

-(UIReturnKeyType)returnKeyType {
    return UIReturnKeyDefault;
}

-(BOOL)enablesReturnKeyAutomatically {
    return NO;
}

-(BOOL)isSecureTextEntry {
    return NO;
}

-(void)initialSetup {
    
    contentView = [[UIView alloc] initWithFrame:CGRectZero];
    contentView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:contentView];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(contentView);
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[contentView]-0-|" options:0 metrics:nil views:dict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[contentView]-0-|" options:0 metrics:nil views:dict]];
    
    self.dotsGap = 10;
    self.dotsSize = 30;
    
    dotsArray = [NSMutableArray new];
    
    self.backgroundColor = [UIColor whiteColor];
}

-(BOOL)canBecomeFirstResponder {
    
    return YES;
}

-(BOOL)canResignFirstResponder {
    return YES;
}

-(BOOL)hasText {
    return NO;
}

-(void)insertText:(NSString *)text {
    
    WLTDotView *toFillNext = nil;
    WLTDotView *lastFilled = nil;
    
    for ( WLTDotView *dtView in dotsArray ) {
        if (!dtView.text.length) {
            toFillNext = dtView;
            break;
        }else {
            lastFilled = dtView;
        }
    }
    
    if (toFillNext) {
        
        _enteredDigits += 1;
        [toFillNext setText:text];
        [self sendActionsForControlEvents:UIControlEventValueChanged];
        
        if (lastFilled) {
            [lastFilled doFinalStepOfFilledUpAnimation];
        }
    }
}

-(void)deleteBackward {
    
    WLTDotView *toEmptyNext = nil;
    for ( WLTDotView *dtView in dotsArray.reverseObjectEnumerator ) {
        if (dtView.text.length) {
            toEmptyNext = dtView;
            break;
        }
    }
    
    if (toEmptyNext) {
        
        _enteredDigits -= 1;
        [toEmptyNext setText:nil];
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
}

-(void)setupForOTPCount:(int)cnt andDotViewClass:(Class)dotViewClass {
    
    if (cnt >= 3) {
        
        [contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [contentView removeConstraints:contentView.constraints];
        
        UIView *leftPadding = [[UIView alloc] initWithFrame:CGRectZero];
        leftPadding.userInteractionEnabled = NO;
        leftPadding.translatesAutoresizingMaskIntoConstraints = NO;
        [contentView addSubview:leftPadding];
        
        UIView *rightPadding = [[UIView alloc] initWithFrame:CGRectZero];
        rightPadding.userInteractionEnabled = NO;
        rightPadding.translatesAutoresizingMaskIntoConstraints = NO;
        [contentView addSubview:rightPadding];
        
        NSDictionary *dictMetrics = @{@"hGap":@(self.dotsGap), @"size":@(self.dotsSize)};
        
        WLTDotView *lastAdded = nil;
        for ( int i = 0; i < cnt ; i++ ) {
            
            WLTDotView *dotView = [[dotViewClass alloc] initWithFrame:CGRectZero];
            dotView.translatesAutoresizingMaskIntoConstraints = NO;
            [contentView addSubview:dotView];
            [dotsArray addObject:dotView];
            
            NSDictionary *dict = NSDictionaryOfVariableBindings(dotView);
            
            [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[dotView(==size)]" options:0 metrics:dictMetrics views:dict]];
            [contentView addConstraint:[NSLayoutConstraint constraintWithItem:dotView
                                                                    attribute:NSLayoutAttributeCenterY
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:contentView
                                                                    attribute:NSLayoutAttributeCenterY
                                                                   multiplier:1 constant:0]];
            if (lastAdded) {
                
                dict = NSDictionaryOfVariableBindings(lastAdded, dotView);
                [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[lastAdded]-hGap-[dotView(==size)]" options:0 metrics:dictMetrics views:dict]];
            }else {
                
                dict = NSDictionaryOfVariableBindings(leftPadding, dotView, rightPadding);
                [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[leftPadding(==rightPadding)]-0-[dotView(==size)]" options:0 metrics:dictMetrics views:dict]];
            }
            
            lastAdded = dotView;
        }
        
        NSDictionary *dict = NSDictionaryOfVariableBindings(rightPadding, lastAdded, leftPadding);
        [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[lastAdded]-0-[rightPadding(==leftPadding)]-0-|" options:0 metrics:dictMetrics views:dict]];
        [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[rightPadding]-0-|" options:0 metrics:dictMetrics views:dict]];
        [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[leftPadding]-0-|" options:0 metrics:dictMetrics views:dict]];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [super touchesBegan:touches withEvent:event];
    
    if (![self isFirstResponder]) {
        [self becomeFirstResponder];
    }
}

-(NSString*)text {
    
    NSMutableString *retText = nil;
    for ( WLTDotView *dtView in dotsArray ) {
        if (dtView.text.length) {
            
            if (!retText) {
                retText = [@"" mutableCopy];
            }
            
            [retText appendString:dtView.text];
        }else {
            break;
        }
    }
    
    return retText;
}

-(void)reset {
    
    for ( WLTDotView *dtView in dotsArray.reverseObjectEnumerator ) {
        [dtView setText:nil];
    }
    _enteredDigits = 0;
}

@end
