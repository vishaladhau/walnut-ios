//
//  WLTPaymentInfoTableViewCell.h
//  walnut
//
//  Created by Abhinav Singh on 09/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSButton.h"

@class WLTPaymentTransaction;

@interface WLTPaymentInfoTableViewCell : UITableViewCell {
    
    __weak IBOutlet NSLayoutConstraint *backViewHeight;
    __weak IBOutlet UILabel *infoLabel;
    __weak IBOutlet UIImageView *statusImageView;
    
    __weak IBOutlet UIView *backView;
    
    @public
    __weak IBOutlet LSButton *addCardButton;
}

-(void)showInfoOfTransaction:(WLTPaymentTransaction*)trans;

@end
