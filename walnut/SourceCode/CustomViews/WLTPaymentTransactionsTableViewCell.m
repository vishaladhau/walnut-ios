//
//  WLTPaymentTransactionsTableViewCell.m
//  walnut
//
//  Created by Abhinav Singh on 11/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTPaymentTransactionsTableViewCell.h"
#import "WLTPaymentTransaction.h"
#import "WLTPaymentUser.h"
#import "WLTContactsManager.h"
#import "WLTImageButton.h"

@implementation WLTPaymentTransactionsTableViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    statusImageView.tintColor = [UIColor denimColor];
    
    iconButton.layer.cornerRadius = 22;
    
    userNameLabel.font = [UIFont sfUITextMediumOfSize:16];
    userNameLabel.textColor = [UIColor darkGrayTextColor];
    
    dateLabel.font = [UIFont sfUITextRegularOfSize:10];
    dateLabel.textColor = [UIColor mediumGrayTextColor];
    
    amountLabel.font = [UIFont sfUITextRegularOfSize:19];
    amountLabel.textColor = [UIColor oweColor];
    
    bankLabel.font = [UIFont sfUITextRegularOfSize:12];
    bankLabel.textColor = [UIColor mediumGrayTextColor];
    
    clickToAcceptLabel.font = [UIFont sfUITextRegularOfSize:10];
    clickToAcceptLabel.textColor = [UIColor mediumGrayTextColor];
}

-(void)showMessagesForTransaction:(WLTPaymentTransaction*)trans withFullSize:(BOOL)fullSize {
    
    CGFloat height = 0;
    
    NSString *receiverMessage = trans.userMessage;
    NSString *senderMessage = trans.receiverMessage;//this is acctually reply Message
    
    BOOL canShowUsers = YES;
    if ( (trans.type == PaymentTypePull) || (trans.type == PaymentTypePush)) {
        if (!senderMessage.length) {
            canShowUsers = NO;
        }
    }
    
    if(!fullSize) {
        
        receiverMessage = receiverMessage.stringByRemovingExtraSpacesAndNewLine;
        senderMessage = senderMessage.stringByRemovingExtraSpacesAndNewLine;
    }
    
    if (receiverMessage.length || senderMessage.length) {
        
        CGFloat uMessageHeight = 0;
        CGFloat rMessageHeight = 0;
        
        if (receiverMessage.length) {
            
            UIView *labelBack = [[UIView alloc] initWithFrame:CGRectZero];
            labelBack.translatesAutoresizingMaskIntoConstraints = NO;
            labelBack.layer.cornerRadius = 3;
            labelBack.clipsToBounds = YES;
            labelBack.backgroundColor = [UIColor lightBackgroundColor];
            [infoBackView addSubview:labelBack];
            
            UILabel *uMessageLabel = [[UILabel alloc] initWithFrame:CGRectZero];
            uMessageLabel.translatesAutoresizingMaskIntoConstraints = NO;
            uMessageLabel.textColor = [UIColor cadetColor];
            uMessageLabel.font = [UIFont sfUITextRegularOfSize:14];
            [uMessageLabel setPreferredMaxLayoutWidth:([UIScreen mainScreen].bounds.size.width-(85))];
            if (fullSize) {
                uMessageLabel.numberOfLines = 0;
            }else {
                uMessageLabel.numberOfLines = 4;
            }
            
            if (canShowUsers) {
                
                uMessageLabel.text = [NSString stringWithFormat:@"--\n%@", receiverMessage];
                [[WLTContactsManager sharedManager] displayNameForUser:trans.receiver defaultIsMob:YES completion:^(NSString *name) {
                    
                    NSString *complete = [NSString stringWithFormat:@"%@\n\n%@",name, receiverMessage];
                    NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:complete];
                    [attributed addAttributes:@{NSFontAttributeName:[UIFont sfUITextRegularOfSize:10], NSForegroundColorAttributeName:[UIColor denimColor]} range:[complete rangeOfString:name]];
                    [attributed addAttributes:@{NSFontAttributeName:[UIFont sfUITextRegularOfSize:3]} range:NSMakeRange([complete rangeOfString:name].length, 2)];
                    
                    uMessageLabel.attributedText = attributed;
                }];
            }else {
                uMessageLabel.text = receiverMessage;
            }
            
            [labelBack addSubview:uMessageLabel];
            
            CGFloat labelHeight = [uMessageLabel intrinsicContentSize].height;
            
            NSDictionary *dict = NSDictionaryOfVariableBindings(labelBack, uMessageLabel);
            
            [infoBackView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-10-[labelBack(==%f)]", (labelHeight+10)] options:0 metrics:nil views:dict]];
            [infoBackView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[labelBack]-0-|" options:0 metrics:nil views:dict]];
            
            [labelBack addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[uMessageLabel]-5-|" options:0 metrics:nil views:dict]];
            [labelBack addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[uMessageLabel]" options:0 metrics:nil views:dict]];
            
            uMessageHeight = (20+labelHeight);
        }
        
        if (senderMessage.length) {
            
            UIView *labelBack = [[UIView alloc] initWithFrame:CGRectZero];
            labelBack.translatesAutoresizingMaskIntoConstraints = NO;
            labelBack.layer.cornerRadius = 3;
            labelBack.clipsToBounds = YES;
            labelBack.backgroundColor = [UIColor lightBackgroundColor];
            [infoBackView addSubview:labelBack];
            
            UILabel *rMessageLabel = [[UILabel alloc] initWithFrame:CGRectZero];
            rMessageLabel.translatesAutoresizingMaskIntoConstraints = NO;
            rMessageLabel.textColor = [UIColor cadetColor];
            rMessageLabel.font = [UIFont sfUITextRegularOfSize:14];
            [rMessageLabel setPreferredMaxLayoutWidth:([UIScreen mainScreen].bounds.size.width-(85))];
            if (fullSize) {
                rMessageLabel.numberOfLines = 0;
            }else {
                rMessageLabel.numberOfLines = 4;
            }
            rMessageLabel.text = [NSString stringWithFormat:@"--\n%@", senderMessage];
            [[WLTContactsManager sharedManager] displayNameForUser:trans.sender defaultIsMob:YES completion:^(NSString *name) {
                
                NSString *complete = [NSString stringWithFormat:@"%@\n\n%@",name, senderMessage];
                NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:complete];
                [attributed addAttributes:@{NSFontAttributeName:[UIFont sfUITextRegularOfSize:10], NSForegroundColorAttributeName:[UIColor denimColor]} range:[complete rangeOfString:name]];
                [attributed addAttributes:@{NSFontAttributeName:[UIFont sfUITextRegularOfSize:3]} range:NSMakeRange([complete rangeOfString:name].length, 2)];
                
                rMessageLabel.attributedText = attributed;
            }];
            
            [labelBack addSubview:rMessageLabel];
            
            CGFloat labelHeight = [rMessageLabel intrinsicContentSize].height;
            
            NSDictionary *dict = NSDictionaryOfVariableBindings(labelBack, rMessageLabel);
            
            [infoBackView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[labelBack(==%f)]-10-|", (labelHeight+10)] options:0 metrics:nil views:dict]];
            [infoBackView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[labelBack]-0-|" options:0 metrics:nil views:dict]];
            
            [labelBack addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[rMessageLabel]-5-|" options:0 metrics:nil views:dict]];
            [labelBack addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[rMessageLabel]" options:0 metrics:nil views:dict]];
            
            rMessageHeight = (20+labelHeight);
        }
        
        if (uMessageHeight && rMessageHeight) {
            
            height = (uMessageHeight + 5 + rMessageHeight);
        }else if(uMessageHeight) {
            
            height = (uMessageHeight + 10);
        }
        else if(rMessageHeight) {
            
            height = (rMessageHeight + 10);
        }
    }
    
    if (!height) {
        height = 11;
    }
    
    messagesHeightConstraint.constant = height;
}

-(void)commonSetupForTransaction:(WLTPaymentTransaction*)trans {
    
    amountLabel.text = trans.amount.fixedCurrencyString;
    dateLabel.text = trans.updateDate.displayString;
    
    [infoBackView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    messagesHeightConstraint.constant = 10;
    
    BOOL isPullPush = [trans isTransactionPullPush];
    
    NSString *prefix = nil;
    id <User> userToShow = nil;
    if (trans.isPaid) {
        
        amountLabel.textColor = [UIColor oweColor];
        userToShow = trans.receiver;
        if (isPullPush) {
            prefix = @"To";
        }else {
            prefix = @"Request from";
        }
    }else {
        
        amountLabel.textColor = [UIColor getColor];
        userToShow = trans.sender;
        if (isPullPush) {
            prefix = @"From";
        }else {
            prefix = @"Request to";
        }
    }
    
    if (isPullPush) {
        [iconButton setViewStyle:UserIconButtonStyleDefault];
    }else {
        [iconButton setViewStyle:UserIconButtonStyleBordered];
    }
    
    __weak typeof(self) weakSelf = self;
    [iconButton displayUserIcon:userToShow];
    [[WLTContactsManager sharedManager] displayNameForUser:userToShow defaultIsMob:YES completion:^(NSString *data) {
        
        if (weakSelf) {
            
            NSString *complete = [NSString stringWithFormat:@"%@ %@", prefix, data];
            NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:complete];
            [attributed addAttributes:@{NSFontAttributeName:[UIFont sfUITextRegularOfSize:16], NSForegroundColorAttributeName:[UIColor mediumGrayTextColor]} range:[complete rangeOfString:prefix]];
            
            userNameLabel.attributedText = attributed;
        }
    }];
}

-(void)showDetailsOfTransactionFromListPage:(WLTPaymentTransaction*)trans{
    
    [self commonSetupForTransaction:trans];
    
    bankLabel.text = nil;
    bankLabelGapConstraint.constant = 0;
    
    clickToAcceptLabel.text = nil;
    clickToAcceptTopGap.constant = 0;
    
    BOOL isShowingActionButton = NO;
    
    if ([trans isPaymentRequired]) {
        
        actionButton.hidden = NO;
        nameLabelTralingGap.constant = 60;
        [actionButton setWalnutPayStyle];
    }else if ([trans isActionRequired]) {
        
        actionButton.hidden = NO;
        
        [actionButton setImage:[UIImage imageNamed:@"ArrowForward"]];
        UIColor *actionColor = [UIColor walnutIconColor];
        LSStateColor *highClr = [[LSStateColor alloc] initWithBack:[UIColor whiteColor]
                                                      contentColor:actionColor
                                                      andBorderClr:actionColor];
        
        LSStateColor *normalClr = [[LSStateColor alloc] initWithBack:actionColor
                                                        contentColor:[UIColor whiteColor]
                                                        andBorderClr:actionColor];
        
        [actionButton addColors:normalClr forState:LSButtonStateNormal];
        [actionButton addColors:highClr forState:LSButtonStateHighlighted];
        
        nameLabelTralingGap.constant = 60;
        
        isShowingActionButton = YES;
        
        clickToAcceptLabel.text = @"Click here to accept";
        clickToAcceptTopGap.constant = 4;
    }else {
        
        actionButton.hidden = YES;
        nameLabelTralingGap.constant = 10;
    }
    
    BOOL isPullPush = [trans isTransactionPullPush];
    
    if (isPullPush && !isShowingActionButton ) {
        
        NSString *imageName = nil;
        BOOL changeColor = NO;
        
        switch (trans.status) {
            case PaymentStatusPullSuccess:
            case PaymentStatusPushPending: {
                imageName = @"StatusIconSent";
                changeColor = YES;
            }
                break;
            case PaymentStatusPushSuccess: {
                imageName = @"StatusIconReceived";
                changeColor = YES;
            }
                break;
            case PaymentStatusPushReversed:
                imageName = @"RevertPaymentIcon";
                changeColor = YES;
                break;
            default:
                imageName = @"ErrorIcon";
                break;
        }
        
        if (changeColor) {
            
            statusImageView.image = [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        }else {
            
            statusImageView.image = [UIImage imageNamed:imageName];
        }
    }
    else {
        
        statusImageView.image = nil;
    }
    
    [self showMessagesForTransaction:trans withFullSize:NO];
    [self invalidateIntrinsicContentSize];
}

-(void)showDetailsOfTransaction:(WLTPaymentTransaction*)trans{
    
    [self commonSetupForTransaction:trans];
    
    clickToAcceptLabel.text = nil;
    clickToAcceptTopGap.constant = 0;
    
    statusImageView.image = nil;
    nameLabelTralingGap.constant = 10;
    actionButton.hidden = YES;
    
    BOOL isPullPush = [trans isTransactionPullPush];
    if (isPullPush) {
        
        NSString *accNumString = nil;
        NSString *senderName = trans.senderBankName;
        if (trans.isPaid && trans.senderLast4Digits.length) {
            
            accNumString = trans.senderLast4Digits;
            senderName = [senderName stringByAppendingFormat:@" %@", trans.senderLast4Digits];
        }
        
        if (trans.receiverBankName.length) {
            
            NSString *receiverName = trans.receiverBankName;
            if (!trans.isPaid && trans.receiverLast4Digits.length) {
                
                accNumString = trans.receiverLast4Digits;
                receiverName = [receiverName stringByAppendingFormat:@" %@", trans.receiverLast4Digits];
            }
            
            NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:senderName];
            
            NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
            UIImage *image = [UIImage imageNamed:@"BankNameSeperator"];
            textAttachment.image = image;
            textAttachment.bounds = CGRectMake( 0, 0, image.size.width, image.size.height );
            
            NSAttributedString *attributedAtt = [NSAttributedString attributedStringWithAttachment:textAttachment];
            [attributed appendAttributedString:attributedAtt];
            
            NSAttributedString *attributedRec = [[NSAttributedString alloc] initWithString:receiverName];
            [attributed appendAttributedString:attributedRec];
            
            if (accNumString.length) {
                
                NSRange range = [attributed.string rangeOfString:accNumString];
                [attributed addAttributes:@{NSFontAttributeName:[UIFont sfUITextMediumOfSize:12], NSForegroundColorAttributeName:[UIColor denimColor]} range:range];
            }
            
            bankLabel.attributedText = attributed;
        }
        else if(senderName){
            
            NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:senderName];
            NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
            UIImage *image = [UIImage imageNamed:@"BankNameSeperator"];
            textAttachment.image = image;
            textAttachment.bounds = CGRectMake( 0, 0, image.size.width, image.size.height );
            
            NSAttributedString *attributedAtt = [NSAttributedString attributedStringWithAttachment:textAttachment];
            [attributed appendAttributedString:attributedAtt];
            
            if (accNumString.length) {
                
                NSRange range = [attributed.string rangeOfString:accNumString];
                [attributed addAttributes:@{NSFontAttributeName:[UIFont sfUITextMediumOfSize:12], NSForegroundColorAttributeName:[UIColor denimColor]} range:range];
            }
            
            bankLabel.attributedText = attributed;
        }
        else {
            
            bankLabel.text = nil;
            bankLabel.attributedText = nil;
            bankLabelGapConstraint.constant = 0;
        }
    }
    else {
        
        bankLabel.attributedText = nil;
        bankLabel.text = nil;
        bankLabelGapConstraint.constant = 0;
    }
    
    [self showMessagesForTransaction:trans withFullSize:YES];
    [self invalidateIntrinsicContentSize];
}

-(IBAction)actionButtonTapped:(id)sender {
    
    [self.delegate actionButtonClickedFromCell:self];
}

@end
