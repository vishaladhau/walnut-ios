//
//  WLTTableHeaderView.h
//  walnut
//
//  Created by Abhinav Singh on 01/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLTTableHeaderView : UIView

@property(nonatomic, weak) UILabel *label;
+(CGFloat)defaultHeight;

@end