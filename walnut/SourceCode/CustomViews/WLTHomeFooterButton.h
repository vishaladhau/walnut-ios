//
//  WLTHomeFooterButton.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 07/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "LSButton.h"

@interface WLTHomeFooterButton : LSButton

@end
