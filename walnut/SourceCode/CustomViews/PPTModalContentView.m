//
//  PPTModalContentView.m
//  PepperTap
//
//  Created by Abhinav Singh on 19/12/15.
//  Copyright © 2015 Leftshift Technologies Pvt. Ltd. All rights reserved.
//

#import "PPTModalContentView.h"

@implementation PPTModalContentView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initialSetup];
    }
    
    return self;
}

-(void)awakeFromNib {
    
    [super awakeFromNib];
    [self initialSetup];
}

-(CGFloat)alertContentViewWidth {
    return 275;
}

-(void)initialSetup {
    
    self.backgroundColor = [UIColor clearColor];
    
    if (!_alphaView) {
        
        UIView *alphaView = [[UIView alloc] initWithFrame:CGRectZero];
        alphaView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:alphaView];
        [self sendSubviewToBack:alphaView];
        
        NSDictionary *dict = NSDictionaryOfVariableBindings(alphaView);
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[alphaView]-0-|" options:0 metrics:nil views:dict]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[alphaView]-0-|" options:0 metrics:nil views:dict]];
        
        _alphaView = alphaView;
    }
    
    if (!_alertContentView) {
        
        UIView *alertContentView = [[UIView alloc] initWithFrame:CGRectZero];
        alertContentView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:alertContentView];
        
        _alertContentView = alertContentView;
        
        NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:alertContentView
                                                                   attribute:NSLayoutAttributeCenterX
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self
                                                                   attribute:NSLayoutAttributeCenterX
                                                                  multiplier:1 constant:0];
        
        NSLayoutConstraint *topY = [NSLayoutConstraint constraintWithItem:alertContentView
                                                                   attribute:NSLayoutAttributeTop
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self
                                                                   attribute:NSLayoutAttributeTop
                                                                  multiplier:1 constant:0];
        
        NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:alertContentView
                                                                attribute:NSLayoutAttributeHeight
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:nil
                                                                attribute:NSLayoutAttributeNotAnAttribute
                                                               multiplier:1 constant:275];

        
        alertContentOriginY = topY;
        alertContentHeight = height;
        
        [self addConstraints:@[centerX, topY, height]];
        
        NSDictionary *dict = NSDictionaryOfVariableBindings(alertContentView);
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[alertContentView(==%f)]", [self alertContentViewWidth]] options:0 metrics:nil views:dict]];
    }
    
    _alphaView.backgroundColor = [UIColor blackColor];
    
    if ([self canDismissOnAlphaViewTap]) {
        
        UITapGestureRecognizer *tapGest = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(alphaViewTapped:)];
        tapGest.numberOfTapsRequired = 1;
        tapGest.numberOfTouchesRequired = 1;
        [_alphaView addGestureRecognizer:tapGest];
    }
    
    
    _alertContentView.layer.borderColor = [UIColor placeHolderTextColor].CGColor;
    _alertContentView.layer.borderWidth = 0.5;
    _alertContentView.clipsToBounds = YES;
    _alertContentView.layer.cornerRadius = DefaultCornerRadius;
    
    [self bringSubviewToFront:_alertContentView];
}

-(BOOL)canDismissOnAlphaViewTap {
    return YES;
}

-(void)alphaViewTapped:(UIGestureRecognizer*)gest {
    
    [self dismiss];
}

#pragma mark - Public Methods

-(void)dismiss {
    
    alertContentOriginY.constant = self.height;
    [UIView animateWithDuration:0.25 animations:^{
        
        _alphaView.alpha = 0;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        [self removeFromSuperview];
    }];
}

-(void)show {
    
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    if (window) {
        
        self.frame = window.bounds;
        [window addSubview:self];
        alertContentOriginY.constant = self.height;
        
        [self layoutIfNeeded];
        
        alertContentOriginY.constant = (self.height - alertContentHeight.constant)/2;
        _alphaView.alpha = 0;
        
        [UIView animateWithDuration:0.25 delay:0.0f options:UIViewAnimationOptionCurveEaseOut  animations:^{
            
            _alphaView.alpha = ModalViewMaximumAlpha;
            [self layoutIfNeeded];
        } completion:nil];
    }
}

@end
