//
//  WLTCardInfoTableViewCell.m
//  walnut
//
//  Created by Abhinav Singh on 6/7/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTCardInfoTableViewCell.h"
#import "GTLWalnutMPaymentInstrument.h"

@implementation WLTCardInfoTableViewCell

- (void)awakeFromNib {
    // Initialization code
	[super awakeFromNib];
	
	infoLabel.textColor = [UIColor rhinoColor];
	infoLabel.font = [UIFont sfUITextRegularOfSize:14];
	
	last4DigitsLabel.textColor = [UIColor rhinoColor];
	last4DigitsLabel.font = [UIFont sfUITextRegularOfSize:12];
}

-(void)showDetailsOfInstrument:(GTLWalnutMPaymentInstrument*)intrument isReceiveDefault:(BOOL)receive {
	
	CreditCardBrand brand = [NSString cardTypeForDisplayString:intrument.network];
	cardTypeImageView.image = [UIImage imageNamed:[NSString imageNameForCardType:brand]];
	
	last4DigitsLabel.text = [NSString stringWithFormat:@"XXXX %@", intrument.last4Digits];
	if (receive) {
		infoLabel.text = @"Receive money using";
	}else {
		infoLabel.text = @"Send money using";
	}
}

@end
