//
//  WLTColorCollectionViewCell.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 07/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTColorCollectionViewCell.h"

@implementation WLTColorCollectionViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    selectionView.image = [[UIImage imageNamed:@"Checkmark"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    selectionView.tintColor = [UIColor whiteColor];
    
    [backgroundImageView setImage:[[UIImage imageNamed:@"ShadowBackground"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
}

-(void)setSelected:(BOOL)selected {
    
    [super setSelected:selected];
    if (selected) {
        selectionView.hidden = NO;
    }else {
        selectionView.hidden = YES;
    }
}

@end
