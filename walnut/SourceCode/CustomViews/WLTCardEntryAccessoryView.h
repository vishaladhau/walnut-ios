//
//  WLTCardEntryAccessoryView.h
//  walnut
//
//  Created by Abhinav Singh on 23/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WLTFlowConstants.h"

FOUNDATION_EXPORT NSString *const NotifyNewCardAdded;

static int AllowedCVVLength = 3;

typedef NS_ENUM(NSInteger, WLTCardEntryState) {
    
    WLTCardEntryStateUnknown = 0,
    WLTCardEntryStateCVV,
    WLTCardEntryStateNewCard,
};

@class WLTOTPView;
@class LSButton;
@class WLTViewController;
@class WLTCardEntryAccessoryView;
@class GTLWalnutMPaymentInstrument;

@protocol WLTCardEntryAccessoryViewDelegate <NSObject>

-(void)cardEntryView:(WLTCardEntryAccessoryView*)view changedCardNumber:(NSString*)cardNum andExpDate:(NSString*)expDate;
-(void)cardEntryView:(WLTCardEntryAccessoryView*)view payClickedWithCVV:(NSString*)cvvString;

@optional

-(void)cardEntryViewEditingStarted:(WLTCardEntryAccessoryView*)view;
-(void)cardEntryView:(WLTCardEntryAccessoryView*)view addedNewInstrumentOfID:(NSString*)instrID;
-(void)continuePaymentFromIntrument:(GTLWalnutMPaymentInstrument*)instrument andCVV:(NSString*)cvvText;

@end

@interface WLTCardEntryAccessoryView : UIView {
    
    NSCharacterSet *otherCharacterSet;
    NSDateFormatter *formatter;
    NSDate *cardExpiryDate;
    
    __weak IBOutlet UIButton *payButton;
    
    __weak IBOutlet UIView *otpEntryBackView;
    __weak IBOutlet UILabel *otpEntryLabel;
    
    __weak IBOutlet UIView *cardEntryBackView;
    __weak IBOutlet UILabel *cardErrorLabel;
    
    __weak IBOutlet UILabel *cardExpiryErrorLabel;
    
    __weak IBOutlet UIView *addCardBackView;
    __weak IBOutlet UIButton *addCardButton;
    __weak IBOutlet UIButton *editCardButton;
    
    CGFloat currentAlphaValue;
    WLTCardEntryState currentState;
    
    NSString *cardErrorDefaultText;
    
    __weak IBOutlet NSLayoutConstraint *expDateTralingSpace;
    
    @public
    __weak IBOutlet UITextField *cardNumberField;
    __weak IBOutlet UITextField *cardCVVNumberField;
    __weak IBOutlet UITextField *cardExpDateField;
    NSString *enteredCVVText;
    
    __weak WLTOTPView *cvvEntryView;
    NSArray *allowedCards;
}

@property(nonatomic, weak) WLTViewController <WLTCardEntryAccessoryViewDelegate> *delegate;
@property (nonatomic, assign) WLTEntryFlowType flowType;

-(void)showCVVEntryPercent:(CGFloat)percent animated:(BOOL)animate;
-(void)setUpForReceivingOnly;

-(void)resetEnteredText;
-(void)resetNewCardText;

@end
