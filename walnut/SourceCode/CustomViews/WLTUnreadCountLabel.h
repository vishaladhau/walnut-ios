//
//  WLTUnreadCountLabel.h
//  walnut
//
//  Created by Abhinav Singh on 27/08/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLTUnreadCountLabel : UIView {
	
	CGFloat allowedWidth;
    __weak UILabel *unreadLabel;
}

-(void)setTextColor:(UIColor *)color;
-(void)setUnreadCount:(NSInteger)count;

@end
