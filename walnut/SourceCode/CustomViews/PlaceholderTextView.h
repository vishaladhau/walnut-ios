//
//  PlaceholderTextView.h
//  Practo
//
//  Created by Abhinav Singh on 04/10/15.
//  Copyright (c) 2015 Practo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PlaceholderTextView;

@protocol PlaceholderTextViewDelegate <NSObject>

@optional
-(void)textEditingCompletedForView:(PlaceholderTextView*)view;
-(void)textDidChangedForView:(PlaceholderTextView*)view;

@end

@interface PlaceholderTextView : UIView <UITextViewDelegate>{
	
    __weak UILabel *placeHolderLabel;
    
    @public
	__weak UITextView *theTextView;
}

@property(nonatomic, strong) id placeHolderText;
@property(nonatomic, weak) id <PlaceholderTextViewDelegate> delegate;

-(void)initialSetup;
-(BOOL)canChangeTextTo:(NSString*)newText;

@property(nonatomic, weak) NSString *text;
-(void)setFont:(UIFont *)font;
-(void)setTextColor:(UIColor *)textColor;

@end
