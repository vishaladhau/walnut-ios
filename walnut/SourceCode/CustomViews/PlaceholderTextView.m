//
//  PlaceholderTextView.m
//  Practo
//
//  Created by Abhinav Singh on 04/10/15.
//  Copyright (c) 2015 Practo. All rights reserved.
//

#import "PlaceholderTextView.h"

@interface PlaceholderTextView () {
	
}

@end

@implementation PlaceholderTextView

- (instancetype)initWithCoder:(NSCoder *)coder {
    
    self = [super initWithCoder:coder];
    if (self) {
        
        [self initialSetup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initialSetup];
    }
    return self;
}

-(void)initialSetup {
    
    self.clipsToBounds = YES;
    self.backgroundColor = [UIColor clearColor];
    
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectZero];
    textView.autoresizingMask = UIViewAutoresizingNone;
    textView.delegate = self;
    textView.font = [UIFont sfUITextRegularOfSize:14];
    textView.textColor = [UIColor darkGrayTextColor];
    [self addSubview:textView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.alpha = 0.5;
    label.numberOfLines = 0;
    label.font = textView.font;
    label.autoresizingMask = UIViewAutoresizingNone;
    label.textColor = [UIColor placeHolderTextColor];
    [self addSubview:label];
    
    placeHolderLabel = label;
    theTextView = textView;
}

-(void)layoutSubviews {
    
    [super layoutSubviews];
    
    theTextView.frame = self.bounds;
    
    CGSize size = [placeHolderLabel sizeThatFits:CGSizeMake(self.width-10, self.height-16)];
    placeHolderLabel.frame = CGRectMake(5, 8, size.width, size.height);
}

-(BOOL)canChangeTextTo:(NSString*)newText {
	
	if (newText.length > 0) {
		[self hidePlaceHolderLabel:YES];
	}else {
		[self hidePlaceHolderLabel:NO];
	}
    
    if ([self.delegate respondsToSelector:@selector(textDidChangedForView:)]) {
        [self.delegate textDidChangedForView:self];
    }
    
	return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
	
	NSString *newText = [textView.text stringByReplacingCharactersInRange:range withString:text];
	BOOL canEdit = [self canChangeTextTo:newText];
    
	return canEdit;
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    
    if ([self.delegate respondsToSelector:@selector(textEditingCompletedForView:)]) {
        
        [self.delegate textEditingCompletedForView:self];
    }
}

-(void)setFont:(UIFont *)font {
    
    theTextView.font = font;
    placeHolderLabel.font = font;
}

-(void)setTextColor:(UIColor *)textColor {
    
    theTextView.textColor = textColor;
    placeHolderLabel.textColor = textColor;
}

-(void)setText:(NSString*)text {
    
    theTextView.text = text;
    if (theTextView.text.length) {
        [self hidePlaceHolderLabel:YES];
    }else {
        [self hidePlaceHolderLabel:NO];
    }
}
-(NSString *)text {
    return theTextView.text;
}
-(BOOL)resignFirstResponder {
    return [theTextView resignFirstResponder];
}

-(BOOL)becomeFirstResponder {
    return [theTextView becomeFirstResponder];
}

-(BOOL)isFirstResponder {
    return [theTextView isFirstResponder];
}

-(void)hidePlaceHolderLabel:(BOOL)hide {
    
    if (placeHolderLabel.hidden != hide) {
        placeHolderLabel.hidden = hide;
    }
}

-(void)setPlaceHolderText:(id )placeHolderText {
	
    if ([placeHolderText isKindOfClass:[NSString class]]) {
        
        _placeHolderText = placeHolderText;
        placeHolderLabel.text = placeHolderText;
    }else if ([placeHolderText isKindOfClass:[NSAttributedString class]]) {
        
        _placeHolderText = placeHolderText;
        placeHolderLabel.attributedText = placeHolderText;
    }
}

@end
