//
//  WLTCVVDotView.h
//  walnut
//
//  Created by Abhinav Singh on 03/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTOTPView.h"

@interface WLTCVVDotView : WLTDotView {
    
    __weak UILabel *digitLabel;
    __weak UIView *borderView;
    __weak UIView *backView;
}

@end
