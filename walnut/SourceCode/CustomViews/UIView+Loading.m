//
//  UIView+Loading.m
//
//  Created by Abhinav Singh on 2/25/15.
//  Copyright (c) 2015 Leftshift Technologies Pvt. Ltd. All rights reserved.
//

#import "UIView+Loading.h"
#import <objc/runtime.h>

@implementation UIView (Loading)

-(UIView*)getLoadingView {
    
    UIView *view = objc_getAssociatedObject(self, @"LoadingView");
    return view;
}

-(void)startLoadingWithColor:(UIColor*)color andStyle:(UIActivityIndicatorViewStyle)style {
    
    UIView *viPrev = [self getLoadingView];
    if (!viPrev) {
        
        UIView *backView = [[UIView alloc] initWithFrame:CGRectZero];
        backView.backgroundColor = [UIColor clearColor];
        [self addSubview:backView];
        
        UIView *blur = [UIView lightBlurView];
        blur.layer.cornerRadius = 7;
        blur.layer.masksToBounds = YES;
        [backView addSubview:blur];
        
        UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:style];
        activity.tag = 11990;
        if (!color) {
            activity.color = [UIColor blackColor];
        }else{
            activity.color = color;
        }
        
        [backView addSubview:activity];
        
        if (self.constraints.count) {
            
            backView.translatesAutoresizingMaskIntoConstraints = NO;
            activity.translatesAutoresizingMaskIntoConstraints = NO;
            blur.translatesAutoresizingMaskIntoConstraints = NO;
            
            [backView addConstraint:[NSLayoutConstraint constraintWithItem:activity attribute:NSLayoutAttributeCenterX
                                                                 relatedBy:NSLayoutRelationEqual toItem:backView
                                                                 attribute:NSLayoutAttributeCenterX
                                                                multiplier:1 constant:0]];
            
            [backView addConstraint:[NSLayoutConstraint constraintWithItem:activity attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual toItem:backView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1 constant:0]];
            
            [backView addConstraint:[NSLayoutConstraint constraintWithItem:blur attribute:NSLayoutAttributeCenterX
                                                                 relatedBy:NSLayoutRelationEqual toItem:backView
                                                                 attribute:NSLayoutAttributeCenterX
                                                                multiplier:1 constant:0]];
            
            [backView addConstraint:[NSLayoutConstraint constraintWithItem:blur attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual toItem:backView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1 constant:0]];
            
            
            NSDictionary *dict = NSDictionaryOfVariableBindings(backView, blur);
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[backView]-0-|" options:0 metrics:nil views:dict]];
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[backView]-0-|" options:0 metrics:nil views:dict]];
            
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[blur(==70)]" options:0 metrics:nil views:dict]];
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[blur(==70)]" options:0 metrics:nil views:dict]];
        }else {
            
            backView.frame = self.bounds;
            backView.autoresizingMask = (UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth);
            
            blur.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin);
            blur.frame = CGRectMake((backView.width-70)/2, (backView.height-70)/2, 70, 70);
            
            activity.center = backView.centerPoint;
            activity.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin);
        }
        
        [activity startAnimating];
        objc_setAssociatedObject(self, @"LoadingView", backView, OBJC_ASSOCIATION_ASSIGN);
    }
}

-(void)startLoadingWithColor:(UIColor *)color {
    
    [self startLoadingWithColor:color andStyle:UIActivityIndicatorViewStyleWhiteLarge];
}

-(void)startLoading {
    
    [self startLoadingWithColor:nil andStyle:UIActivityIndicatorViewStyleWhiteLarge];
}

- (void)endLoading {
    
    UIView *loading = [self getLoadingView];
    objc_setAssociatedObject(self, @"LoadingView", nil, OBJC_ASSOCIATION_ASSIGN);
    [loading removeFromSuperview];
}

-(BOOL)haveLoadingView {
    
    UIView *loading = [self getLoadingView];
    if (loading) {
        return YES;
    }
    
    return NO;
}
@end
