//
//  LSToastMessage.m
//  MobiTVConnect
//
//  Created by Abhinav Singh on 26/06/15.
//  Copyright (c) 2015 Leftshift Technologies Pvt. Ltd. All rights reserved.
//

#import "LSToastMessage.h"

NSString *const NotAGroupMember = @"NotAGroupMember";
NSString *const SlowNetwork = @"SlowNetwork";

NSString *const MoreThanP2PLimit = @"MoreThanP2PLimit";
NSString *const LessThanP2PLimit = @"LessThanP2PLimit";

@interface LSToastMessage () {
    
    UILabel *newMessagelabel;
}

@property(readonly, strong) NSString *messageID;
@property(readonly, assign) NSOperationQueuePriority priority;

@end

@implementation LSToastMessage

-(instancetype)initWithMesasge:(NSAttributedString*)message operationPriority:(NSOperationQueuePriority)pri andID:(NSString*)msgID{
    
    if (!msgID.length) {
        return nil;
    }
    
    self = [super initWithFrame:CGRectZero];
    if (self) {
        
        _messageID = msgID;
        _priority = pri;
        
        self.layer.cornerRadius = 0;
        
        self.layer.masksToBounds = YES;
        self.backgroundColor = [UIColor blackColor];
        self.userInteractionEnabled = NO;
        
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        
        newMessagelabel = [[UILabel alloc] initWithFrame:CGRectZero];
        newMessagelabel.translatesAutoresizingMaskIntoConstraints = NO;
        [newMessagelabel setPreferredMaxLayoutWidth:(width-40)];
        newMessagelabel.numberOfLines = 0;
        newMessagelabel.textAlignment = NSTextAlignmentCenter;
        newMessagelabel.textColor = [UIColor whiteColor];
        newMessagelabel.font = [UIFont sfUITextMediumOfSize:14];
        newMessagelabel.attributedText = message;
        [self addSubview:newMessagelabel];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:newMessagelabel attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual toItem:self
                                                         attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:newMessagelabel attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual toItem:self
                                                         attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        
        [self sizeToFit];
    }
    
    return self;
}

-(void)sizeToFit {
    
    CGSize labelSize = [newMessagelabel intrinsicContentSize];
    self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, (labelSize.height + 30));
}

@end

@interface LSToastMessageHandler () {
    
}

@property(nonatomic, assign) BOOL messageDisplayed;
@property(nonatomic, weak) UIWindow *delegateWindow;
@property(nonatomic, strong) NSMutableArray *messagesQueue;

@end

@implementation LSToastMessageHandler

+(void)removeAllMesages {
    
    LSToastMessageHandler *instance = [LSToastMessageHandler sharedInstance];
    [instance.messagesQueue removeAllObjects];
}

+(void)removeAllMesageOfID:(NSString*)messageID {
    
    LSToastMessageHandler *instance = [LSToastMessageHandler sharedInstance];
    
    LSToastMessage *toRemove = nil;
    
    for ( LSToastMessage *scheduled in instance.messagesQueue ) {
        if ([scheduled.messageID isEqualToString:messageID]) {
            toRemove = scheduled;
            break;
        }
    }
    
    if (toRemove) {
        [instance.messagesQueue removeObject:toRemove];
    }
}

+ (LSToastMessageHandler *)sharedInstance {
    
    static dispatch_once_t once;
    static LSToastMessageHandler *sharedInstance;
    dispatch_once(&once, ^{
        
        sharedInstance = [[LSToastMessageHandler alloc] init];
    });
    
    return sharedInstance;
}

+(void)showMessage:(NSAttributedString*)string withID:(NSString*)messageID andPriorty:(NSOperationQueuePriority)priorty {
    
    if (!messageID.length) {
        return;
    }
    
    BOOL scheduledAlready = NO;
    LSToastMessageHandler *instance = [LSToastMessageHandler sharedInstance];
    
    NSInteger toReplaceIndex = -1;
    LSToastMessage *withReplace = nil;
    
    for ( LSToastMessage *scheduled in instance.messagesQueue ) {
        
        if ([scheduled.messageID isEqualToString:messageID]) {
            
            //Check if already scheduled message has less priorty and not already visible.
            if ((scheduled.priority < priorty)) {
                if (!scheduled.superview) {
                    
                    toReplaceIndex = [instance.messagesQueue indexOfObject:scheduled];
                    LSToastMessage *newView = [[LSToastMessage alloc] initWithMesasge:string operationPriority:priorty andID:messageID];
                    withReplace = newView;
                }
            }
            
            scheduledAlready = YES;
            break;
        }
    }
    
    if ( (toReplaceIndex >= 0) && withReplace) {
        [instance.messagesQueue replaceObjectAtIndex:toReplaceIndex withObject:withReplace];
    }
    
    if (!scheduledAlready) {
        
        LSToastMessage *newView = [[LSToastMessage alloc] initWithMesasge:string operationPriority:priorty andID:messageID];
        [instance.messagesQueue addObject:newView];
        
        [instance performSelector:@selector(checkAndShowNewMessage) withObject:nil afterDelay:0.5];
    }
}

+(void)showMessage:(NSAttributedString*)string withID:(NSString*)messageID {
    
    [self showMessage:string withID:messageID andPriorty:NSOperationQueuePriorityLow];
}

- (instancetype)init {
    
    self = [super init];
    if (self) {
        
        self.messagesQueue = [NSMutableArray new];
        self.messageDisplayed = NO;
    }
    
    return self;
}

-(void)checkAndShowNewMessage {
    
    LSToastMessage *newView = [self.messagesQueue lastObject];
    if (newView && !self.messageDisplayed) {
        
        if (!self.delegateWindow) {
            self.delegateWindow = [[[UIApplication sharedApplication] delegate] window];
        }
        
        [self.delegateWindow addSubview:newView];
        newView.originY = self.delegateWindow.frame.size.height;
        
        [self performSelector:@selector(removeMessage:) withObject:newView afterDelay:3];
        
        __weak LSToastMessageHandler *weakSelf = self;
        
        self.messageDisplayed = YES;
        
        [UIView animateWithDuration:0.3 animations:^{
            
            CGRect frame = newView.frame;
            frame.origin.y = (weakSelf.delegateWindow.frame.size.height - newView.frame.size.height);
            newView.frame = frame;
        }];
    }
}

-(void)removeMessage:(LSToastMessage*)messageView {
    
    if (messageView) {
        
        [self.messagesQueue removeObject:messageView];
        
        [UIView animateWithDuration:0.3 animations:^{
            
            CGRect frame = messageView.frame;
            frame.origin.y = messageView.superview.frame.size.height;
            messageView.frame = frame;
        } completion:^(BOOL finished) {
            
            [messageView removeFromSuperview];
        }];
    }
    
    self.messageDisplayed = NO;
    [self checkAndShowNewMessage];
}

@end
