//
//  WLTBouncingButton.h
//  walnut
//
//  Created by Abhinav Singh on 02/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLTBouncingButton : UIControl {
    
    __weak UILabel *textLabel;
}

-(void)bounce;

@end
