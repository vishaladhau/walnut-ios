//
//  WLTTableHeaderView.m
//  walnut
//
//  Created by Abhinav Singh on 01/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTTableHeaderView.h"

@implementation WLTTableHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initialSetup];
    }
    
    return self;
}

+(CGFloat)defaultHeight {
    
    return 40;
}

-(void)initialSetup {
    
    self.backgroundColor = [UIColor lightBackgroundColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.numberOfLines = 1;
    headerLabel.translatesAutoresizingMaskIntoConstraints = NO;
    headerLabel.textColor = [UIColor cadetGrayColor];
    headerLabel.font = [UIFont sfUITextRegularOfSize:12];
    [self addSubview:headerLabel];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(headerLabel);
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[headerLabel]-10-|" options:0 metrics:nil views:dict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[headerLabel]-5-|" options:0 metrics:nil views:dict]];
    
    _label = headerLabel;
}

@end