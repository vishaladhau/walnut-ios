//
//  WLTSplitSettleTableViewCell.h
//  walnut
//
//  Created by Abhinav Singh on 08/03/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//
//#import "WLTSplitSettleView.h"

@import UIKit;
@class WLTTransaction;

@interface WLTSplitSettleTableViewCell : UITableViewCell {
    
    __weak IBOutlet UILabel *payTypeLabel;
    __weak IBOutlet UILabel *payDateLabel;
    __weak IBOutlet UILabel *amountLabel;
    __weak IBOutlet UILabel *infoLabel;
    
    __weak IBOutlet UILabel *bankNameLabel;
    __weak IBOutlet NSLayoutConstraint *bankNameLabelTopConstraint;
    
    __weak IBOutlet UIView *amountBackView;
    
    __weak IBOutlet UIImageView *backImageView;
    __weak IBOutlet UIImageView *payTypeImageView;
    __weak IBOutlet UIImageView *paymentStatusImageView;
    
    __weak IBOutlet UIImageView *dottedImageView;
    
    __weak IBOutlet UILabel *crossSettleLabel;
    
    __weak IBOutlet NSLayoutConstraint *leadingGapConstraint;
    __weak IBOutlet NSLayoutConstraint *topGapConstraint;
    __weak IBOutlet NSLayoutConstraint *tralingGapConstraint;
    __weak IBOutlet NSLayoutConstraint *bottomGapConstraint;
}

-(void)showDetailsOfTransaction:(WLTTransaction*)trans;

@end
