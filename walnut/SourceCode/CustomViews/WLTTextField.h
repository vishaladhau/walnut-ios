//
//  WLTTextField.h
//  walnut
//
//  Created by Abhinav Singh on 15/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WLTTextField;

@protocol WLTTextFieldDelegate <NSObject>

-(BOOL)textField:(WLTTextField*)field canChangeTextTo:(NSString*)newTextString;

@end

@interface WLTTextField : UIView <UITextFieldDelegate>{
    
    __weak UILabel *placeHolderLabel;
    BOOL emptyStringState;
    
    NSLayoutConstraint *labelBottomConstraint;
    
    @public
    __weak UITextField *theTextField;
}

-(void)setText:(id)text;
-(NSString*)text;

@property(nonatomic, strong) NSString *placeholderText;
@property(nonatomic, strong) UIColor *valueColor;
@property(nonatomic, weak) id <WLTTextFieldDelegate> delegate;

@end
