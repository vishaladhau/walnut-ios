//
//  WLTHomeIconButton.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 05/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTHomeIconButton.h"

@implementation WLTHomeIconButton

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initialSetup];
    }
    
    return self;
}

-(void)initialSetup {
    
    self.backgroundColor = [UIColor appleGreenColor];
    
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = 0.3;
    self.layer.shadowRadius = 2;
    self.layer.shadowOffset = CGSizeMake(0, 2);
    
    UIImageView *imgV = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"WalnutWIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    imgV.tintColor = [UIColor whiteColor];
    imgV.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:imgV];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:imgV attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:imgV
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
    
    logoImageView = imgV;
}

-(void)awakeFromNib {
    
    [super awakeFromNib];
    [self initialSetup];
}

-(void)applySettingsStyleAnimated:(BOOL)animate {
    
    if (animate) {
        
        [UIView animateWithDuration:0.3 animations:^{
            
            [self setBackgroundColor:[UIColor whiteColor]];
        } completion:^(BOOL finished) {
            
            logoImageView.tintColor = [UIColor appleGreenColor];
        }];
    }else {
        [self setBackgroundColor:[UIColor whiteColor]];
        logoImageView.tintColor = [UIColor appleGreenColor];
    }
}

-(void)applyHomeStyleAnimated:(BOOL)animate {
    
    if (animate) {
        
        [UIView animateWithDuration:0.3 animations:^{
            
            [self setBackgroundColor:[UIColor appleGreenColor]];
        } completion:^(BOOL finished) {
            
            logoImageView.tintColor = [UIColor whiteColor];
        }];
    }else {
        [self setBackgroundColor:[UIColor appleGreenColor]];
        logoImageView.tintColor = [UIColor whiteColor];
    }
    
}

-(void)layoutSubviews {
    
    [super layoutSubviews];
    self.layer.cornerRadius = (self.width/2);
}

@end
