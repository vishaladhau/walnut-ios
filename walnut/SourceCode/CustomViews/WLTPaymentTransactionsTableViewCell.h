//
//  WLTPaymentTransactionsTableViewCell.h
//  walnut
//
//  Created by Abhinav Singh on 11/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WLTUserIconButton.h"

@class WLTPaymentTransactionsTableViewCell;
@class WLTPaymentTransaction;
@class WLTImageButton;

@protocol WLTPaymentTransTableCellDelegate <NSObject>

-(void)actionButtonClickedFromCell:(WLTPaymentTransactionsTableViewCell*)cell;

@end

@interface WLTPaymentTransactionsTableViewCell : UITableViewCell {

    __weak IBOutlet WLTUserIconButton *iconButton;
    
    __weak IBOutlet UILabel *userNameLabel;
    __weak IBOutlet UILabel *dateLabel;
    __weak IBOutlet UILabel *clickToAcceptLabel;
    __weak IBOutlet UILabel *amountLabel;
    __weak IBOutlet UILabel *bankLabel;
    __weak IBOutlet NSLayoutConstraint *bankLabelGapConstraint;
    __weak IBOutlet UIView *infoBackView;
    __weak IBOutlet NSLayoutConstraint *messagesHeightConstraint;
    __weak IBOutlet NSLayoutConstraint *nameLabelTralingGap;
    
    __weak IBOutlet NSLayoutConstraint *clickToAcceptTopGap;
    __weak IBOutlet WLTImageButton *actionButton;
    
    @public
    __weak IBOutlet UIImageView *statusImageView;
}

@property(nonatomic, weak) id <WLTPaymentTransTableCellDelegate> delegate;

-(void)showDetailsOfTransaction:(WLTPaymentTransaction*)trans;
-(void)showDetailsOfTransactionFromListPage:(WLTPaymentTransaction*)trans;

@end
