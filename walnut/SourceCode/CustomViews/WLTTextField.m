//
//  WLTTextField.m
//  walnut
//
//  Created by Abhinav Singh on 15/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTTextField.h"

@implementation WLTTextField

-(void)awakeFromNib {
    
    [super awakeFromNib];
    [self initialSetup];
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initialSetup];
    }
    return self;
}

-(void)initialSetup {
    
    UITextField *field = [[UITextField alloc] initWithFrame:CGRectZero];
    field.delegate = self;
    field.font = [UIFont sfUITextRegularOfSize:16];
    field.textColor = [UIColor darkGrayTextColor];
    field.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:field];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.userInteractionEnabled = NO;
    label.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:label];
    
    [self addBorderLineAtPosition:BorderLinePositionBottom];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(label, field);
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[field]-10-|" options:0 metrics:nil views:dict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[label]-10-|" options:0 metrics:nil views:dict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[field]-0-|" options:0 metrics:nil views:dict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[label]" options:0 metrics:nil views:dict]];
    
    labelBottomConstraint = [NSLayoutConstraint constraintWithItem:label
                                                         attribute:NSLayoutAttributeBottom
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeBottom
                                                        multiplier:1 constant:0];
    labelBottomConstraint.constant = 0;
    [self addConstraint:labelBottomConstraint];
    
    placeHolderLabel = label;
    theTextField = field;
    
    emptyStringState = YES;
}

-(void)setPlaceholderText:(NSString *)placeholderText {
    
    _placeholderText = placeholderText;
    placeHolderLabel.text = placeholderText;
}

-(void)setValueColor:(UIColor *)valueColor {
    
    _valueColor = valueColor;
    
    emptyStringState = !emptyStringState;
    [self changeStringStateToEmpty:!emptyStringState];
}

-(NSString *)text {
    
    if (theTextField.text.length) {
        return theTextField.text;
    }else {
        return theTextField.attributedText.string;
    }
    
    return nil;
}

-(void)setText:(id)text {
    
    BOOL empty = YES;
    if ([text isKindOfClass:[NSAttributedString class]]) {
        
        theTextField.attributedText = text;
        
        if (theTextField.attributedText.length) {
            empty = NO;
        }
    }else {
        
        theTextField.text = text;
        if (theTextField.text.length) {
            empty = NO;
        }
    }
    
    [self changeStringStateToEmpty:empty];
}

-(void)changeStringStateToEmpty:(BOOL)empty {
    
    if (theTextField.leftView) {
        empty = NO;
        emptyStringState = !empty;
    }
    
    if (emptyStringState != empty) {
        
        emptyStringState = empty;
        
        if (empty) {
            
            placeHolderLabel.font = theTextField.font;
            placeHolderLabel.textColor = [UIColor placeHolderTextColor];
            
            if (![self.constraints containsObject:labelBottomConstraint]) {
                [self addConstraint:labelBottomConstraint];
            }
        }else {
            
            placeHolderLabel.font = [UIFont sfUITextRegularOfSize:9];
            placeHolderLabel.textColor = self.valueColor;
            
            if ([self.constraints containsObject:labelBottomConstraint]) {
                [self removeConstraint:labelBottomConstraint];
            }
        }
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    BOOL retValue = YES;
    if ([self.delegate respondsToSelector:@selector(textField:canChangeTextTo:)]) {
        retValue = [self.delegate textField:self canChangeTextTo:newString];
    }
    
    if (retValue) {
        if (newString.length) {
            [self changeStringStateToEmpty:NO];
        }else {
            [self changeStringStateToEmpty:YES];
        }
    }
    
    return retValue;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return NO;
}

@end
