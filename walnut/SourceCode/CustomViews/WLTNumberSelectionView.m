//
//  WLTNumberSelectionView.m
//  walnut
//
//  Created by Abhinav Singh on 05/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTNumberSelectionView.h"
#import "WLTMobileNumber.h"

@implementation WLTNumberSelectionView

-(void)showObjects:(NSArray*)objects removeContactBlock:(CompletionBlock)block {
    
    removeBlock = block;
    [super showObjects:objects];
}

-(void)addLastView:(UIView*)lastView totalView:(NSInteger)count{
    
    if (removeBlock) {
        
        UIButton *cardView = [UIButton buttonWithType:UIButtonTypeCustom];
        [cardView.titleLabel setNumberOfLines:0];
        [cardView setTitleColor:cardView.tintColor forState:UIControlStateNormal];
        [cardView addTarget:self action:@selector(removeContactClicked:) forControlEvents:UIControlEventTouchUpInside];
        cardView.translatesAutoresizingMaskIntoConstraints = NO;
        [cardView addBorderLineAtPosition:BorderLinePositionTop];
        
        NSAttributedString *attributed = [[NSAttributedString alloc] initWithString:@"Remove Contact" attributes:@{NSForegroundColorAttributeName:[UIColor flamePeaColor], NSFontAttributeName:[UIFont sfUITextRegularOfSize:17]}];
        [cardView setAttributedTitle:attributed forState:UIControlStateNormal];
        [contentView addSubview:cardView];
        
        NSDictionary *dictInte = NSDictionaryOfVariableBindings(cardView, lastView);
        [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[cardView]-0-|" options:0 metrics:nil views:dictInte]];
        [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lastView(==cardView)]-0-[cardView(==lastView)]-0-|" options:0 metrics:nil views:dictInte]];
        
        contentHeight.constant = ( (count+1) * [self cellContentHeight]);
        
        CGFloat alertHeight = (contentHeight.constant + [self cellContentHeight] + 7 + 10);
        
        if (alertHeight > ([UIScreen mainScreen].bounds.size.height-100)) {
            alertHeight = ([UIScreen mainScreen].bounds.size.height-100);
        }
        
        alertContentHeight.constant = alertHeight;
    }else {
        
        [super addLastView:lastView totalView:count];
    }
}

-(void)removeContactClicked:(UIButton*)btn {
    
    if (removeBlock) {
        removeBlock();
    }
    
    [self dismiss];
}

-(NSAttributedString*)titleForObject:(WLTMobileNumber*)mob {
    
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.alignment = NSTextAlignmentCenter;
    
    NSString *complete = [NSString stringWithFormat:@"%@ %@", [mob.label capitalizedString], mob.number];
    
    NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:complete attributes:@{NSParagraphStyleAttributeName:style, NSFontAttributeName:[UIFont sfUITextMediumOfSize:17], NSForegroundColorAttributeName:[self tintColor]}];
    [attributed addAttributes:@{NSForegroundColorAttributeName:[UIColor darkGrayTextColor],NSFontAttributeName:[UIFont sfUITextRegularOfSize:15]} range:[complete rangeOfString:[mob.label capitalizedString]]];
    
    return attributed;
}


@end
