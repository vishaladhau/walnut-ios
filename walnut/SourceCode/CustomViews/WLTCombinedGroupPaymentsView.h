//
//  WLTCombinedGroupPaymentsView.h
//  walnut
//
//  Created by Abhinav Singh on 02/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WLTImageButton.h"
#import "WLTUserIconButton.h"
#import "WLTCombinedGroupPayments.h"

@class WLTCombinedGroupPaymentsView;

@protocol WLTCombinedGroupPaymentsViewDelegate <NSObject>

-(void)reminderButtonClickedFromCell:(WLTCombinedGroupPaymentsView*)cell;
-(void)actionButtonClickedFromCell:(WLTCombinedGroupPaymentsView*)cell;
-(void)combinedPayCell:(WLTCombinedGroupPaymentsView*)cell selectedGroup:(WLTGroup*)grp;

@end

@interface WLTCombinedGroupPaymentsView : UIView <UIGestureRecognizerDelegate>{
    
    __weak IBOutlet UILabel *userNameLabel;
    __weak IBOutlet WLTUserIconButton *userIconButton;
    __weak IBOutlet UILabel *groupNameLabel;
    
    __weak IBOutlet UILabel *userAmountLabel;
    
    __weak IBOutlet WLTImageButton *reminderButton;
    __weak IBOutlet WLTImageButton *actionButton;
    
    __weak IBOutlet UIView *multipleGroupsBackView;
    
    __weak IBOutlet NSLayoutConstraint *userNameGapConstraints;
    __weak IBOutlet NSLayoutConstraint *heightConstraint;
    
    NSMapTable *viewDataMapping;
}

@property(nonatomic, weak) id <WLTCombinedGroupPaymentsViewDelegate> delegate;

-(void)showDetailsOfData:(WLTCombinedGroupPayments*)data;
@property(readonly, strong) WLTCombinedGroupPayments *paymentGroup;

@end
