//
//  WLTColorSelectionTableViewCell.m
//  Walnut_ios
//
//  Created by Abhinav Singh on 07/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTColorSelectionTableViewCell.h"

@implementation WLTColorSelectionTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    selectedColorView.layer.cornerRadius = DefaultCornerRadius;
    selectedColorView.layer.masksToBounds = YES;
    
    nameLabel.font = [UIFont sfUITextRegularOfSize:16];
    nameLabel.text = @"Group Colour";
    nameLabel.textColor = [UIColor cadetColor];
}

@end
