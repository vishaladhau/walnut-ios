//
//  WLTOtherSettelmentTableCell.h
//  walnut
//
//  Created by Abhinav Singh on 02/07/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

@import UIKit;

@class WLTOtherSettlement, WLTUserIconButton;

@interface WLTOtherSettelmentTableCell : UITableViewCell {
    
    __weak WLTUserIconButton *circleView;
    __weak UILabel *_titleLabel;
    __weak UILabel *_subtitleLabel;
}

@property(readonly, strong) WLTOtherSettlement *displayedSettlement;

-(void)showDetailsOfOtherSettelment:(WLTOtherSettlement*)com;

@end
