//
//  WLTStepper.m
//  walnut
//
//  Created by Abhinav Singh on 01/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTStepper.h"

@implementation WLTStepper

-(void)awakeFromNib {
    
    [super awakeFromNib];
    [self initialSetup];
}

-(void)initialSetup {
    
    self.layer.cornerRadius = DefaultCornerRadius;
    self.layer.masksToBounds = YES;
    
    self.backgroundColor = [UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1.00];
    
    UIButton *reduce = [UIButton buttonWithType:UIButtonTypeCustom];
    reduce.translatesAutoresizingMaskIntoConstraints = NO;
    reduce.tintColor = [UIColor appleGreenColor];
    [reduce setImage:[[UIImage imageNamed:@"ReduceIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [reduce addTarget:self action:@selector(reduceClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:reduce];
    
    UIButton *addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    addBtn.translatesAutoresizingMaskIntoConstraints = NO;
    addBtn.tintColor = [UIColor appleGreenColor];
    [addBtn setImage:[[UIImage imageNamed:@"IncreaseIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [addBtn addTarget:self action:@selector(addClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:addBtn];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor darkGrayTextColor];
    label.text = @(_value).displayRoundedString;
    label.font = [UIFont sfUITextRegularOfSize:16];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:label];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(reduce, label, addBtn);
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[reduce(==35)]-0-[label]-0-[addBtn(==reduce)]-0-|" options:0 metrics:nil views:dict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[reduce]-0-|" options:0 metrics:nil views:dict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[addBtn]-0-|" options:0 metrics:nil views:dict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[label]-0-|" options:0 metrics:nil views:dict]];
    
    amountLabel = label;
    reduceButton = reduce;
    increaseButton = addBtn;
}

-(void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];
    if ([self isEnabled]) {
        
        reduceButton.tintColor = [UIColor appleGreenColor];
        increaseButton.tintColor = [UIColor appleGreenColor];
        
        amountLabel.textColor = [UIColor darkGrayTextColor];
    }else {
        
        reduceButton.tintColor = [UIColor mediumGrayTextColor];
        increaseButton.tintColor = [UIColor mediumGrayTextColor];
        
        amountLabel.textColor = [UIColor mediumGrayTextColor];
    }
}

-(void)reduceClicked:(UIButton*)rBtn {
    
    CGFloat newValue = 0;
    CGFloat lower = floor(self.value);
    
    if ( self.value == lower) {
        newValue += roundf((self.value-1));
    }else {
        newValue = floor(self.value);
    }
    
    CGFloat oldVal = self.value;
    self.value = newValue;
    if (oldVal != self.value) {
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
}

-(void)addClicked:(UIButton*)aBtn {
    
    CGFloat newValue = 0;
    CGFloat lower = floor(self.value);
    
    if ( self.value == lower) {
        newValue += roundf((self.value+1));
    }else {
        newValue = ceil(self.value);
    }
    
    CGFloat oldVal = self.value;
    self.value = newValue;
    if (oldVal != self.value) {
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
}

-(void)setValue:(CGFloat)value {
    
    if ( (_value != value) && (value >= 0)) {
        _value = value;
        amountLabel.text = @(_value).displayRoundedString;
    }
}

@end
