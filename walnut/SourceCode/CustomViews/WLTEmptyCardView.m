//
//  WLTEmptyCardView.m
//  walnut
//
//  Created by Abhinav Singh on 23/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTEmptyCardView.h"
#import "WLTColorPalette.h"

@implementation WLTEmptyCardView

-(void)awakeFromNib {
    
    [super awakeFromNib];
    [self initialSetup];
}

-(void)initialSetup {
    
    cardNumberLabel.textColor = [UIColor whiteColor];
    cardNumberLabel.font = [UIFont ocrFontOfSize:21];
    cardNumberLabel.shadowColor = [UIColor colorWithWhite:0 alpha:0.5];
    cardNumberLabel.shadowOffset = CGSizeMake(0, 1);
    cardNumberLabel.text = [NSString emptyCardPlaceHolder];
    
    cardExpDateLabel.textColor = [UIColor whiteColor];
    cardExpDateLabel.font = [UIFont ocrFontOfSize:16];
    cardExpDateLabel.text = @"XX/XX";
    cardExpDateLabel.shadowColor = [UIColor colorWithWhite:0 alpha:0.5];
    cardExpDateLabel.shadowOffset = CGSizeMake(0, 1);
    
    tintImageView.image = [tintImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    tintImageView.tintColor = [[WLTColorPalette sharedPalette] colorForInstrumentID:nil];
    
    cardExpDateHeaderLabel.font = [UIFont sfUITextRegularOfSize:8];
    cardExpDateHeaderLabel.text = @"EXPIRES";
    cardExpDateHeaderLabel.textColor = [UIColor whiteColor];
    
    cardNumberHeaderLabel.font = [UIFont sfUITextRegularOfSize:8];
    cardNumberHeaderLabel.textColor = [UIColor whiteColor];
    cardNumberHeaderLabel.text = @"CARD NUMBER";
    
    
    
    cardTypeImageView.image = nil;
    
    self.backgroundColor = [UIColor clearColor];
}

@end
