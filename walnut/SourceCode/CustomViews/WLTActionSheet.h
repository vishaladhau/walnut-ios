//
//  WLTActionSheet.h
//  walnut
//
//  Created by Abhinav Singh on 05/04/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "PPTModalContentView.h"

@interface WLTActionSheet : PPTModalContentView {
    
    NSMapTable *objectViewMapping;
    
    __weak UIButton *cancelButton;
    __weak UIScrollView *scrollView;
    
    __weak UIView *contentView;
    __weak NSLayoutConstraint *contentHeight;
}

@property(nonatomic, strong) DataCompletionBlock completionBlock;

-(CGFloat)cellContentHeight;

-(void)showObjects:(NSArray*)objects;
-(void)addLastView:(UIView*)lastView totalView:(NSInteger)count;

@end
