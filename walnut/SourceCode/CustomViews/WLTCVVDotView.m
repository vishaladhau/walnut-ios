//
//  WLTCVVDotView.m
//  walnut
//
//  Created by Abhinav Singh on 03/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTCVVDotView.h"

@implementation WLTCVVDotView

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        
        UIView *brdrView = [[UIView alloc] initWithFrame:CGRectZero];
        brdrView.layer.borderColor = [UIColor appleGreenColor].CGColor;
        brdrView.layer.borderWidth = 2;
        brdrView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:brdrView];
        
        UIView *bView = [[UIView alloc] initWithFrame:CGRectZero];
        bView.backgroundColor = [UIColor appleGreenColor];
        bView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:bView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont sfUITextRegularOfSize:14];
        label.textColor = [UIColor whiteColor];
        label.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:label];
        
        NSDictionary *dict = NSDictionaryOfVariableBindings(label, bView, brdrView);
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[label]-0-|" options:0 metrics:nil views:dict]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[label]-0-|" options:0 metrics:nil views:dict]];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[bView]-0-|" options:0 metrics:nil views:dict]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[bView]-0-|" options:0 metrics:nil views:dict]];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[brdrView]-0-|" options:0 metrics:nil views:dict]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[brdrView]-0-|" options:0 metrics:nil views:dict]];
        
        borderView = brdrView;
        backView = bView;
        digitLabel = label;
        
        backView.hidden = YES;
    }
    
    return self;
}

-(void)layoutSubviews {
    
    [super layoutSubviews];
    
    CGFloat crnrRadi = (self.width/2.0);
    
    self.layer.cornerRadius = crnrRadi;
    borderView.layer.cornerRadius = crnrRadi;
    backView.layer.cornerRadius = crnrRadi;
}

-(void)animateFromEmptyToFilledUp {
    
    digitLabel.text = self.text;
    backView.hidden = NO;
}

-(void)animateFromFilledUpToEmpty {
    
    digitLabel.text = nil;
    backView.hidden = YES;
}

-(void)doFinalStepOfFilledUpAnimation {
    
}

@end
