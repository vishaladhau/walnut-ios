//
//  LSButton.h
//
//  Created by Abhinav Singh on 22/05/15.
//  Copyright (c) 2015 Leftshift Technologies Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LSStateColor : NSObject {
    
}

- (instancetype)initWithBack:(UIColor*)bckClr contentColor:(UIColor*)contClr
                andBorderClr:(UIColor*)brdrClr;

@property(readonly, strong) UIColor *backColor;
@property(readonly, strong) UIColor *borderColor;
@property(readonly, strong) UIColor *contentColor;

@end

typedef NS_ENUM(NSInteger, LSButtonState) {
    
    LSButtonStateUnknown = -1,
    LSButtonStateNormal = 0,
    LSButtonStateHighlighted,
    LSButtonStateDisabled,
};

@interface LSButton : UIControl {
    
    NSString *titleText;
    LSButtonState state;
    NSMutableDictionary *stateColors;
    
    NSTextAlignment alingment;
    
    @public
    UILabel *titleLabel;
}

-(void)changeDisplayForCurrentState;
-(void)setTitle:(NSString*)title;
-(void)setTextAlingment:(NSTextAlignment)aling;
-(void)initialSetup;
-(void)addColors:(LSStateColor*)clrs forState:(LSButtonState)forState;

-(void)addAllConstraints;
-(void)addAllSubViews;

-(void)applyDefaultStyle;

@end
