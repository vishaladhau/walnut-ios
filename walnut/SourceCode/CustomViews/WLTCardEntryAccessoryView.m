//
//  WLTCardEntryAccessoryView.m
//  walnut
//
//  Created by Abhinav Singh on 23/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTCardEntryAccessoryView.h"
#import "WLTOTPView.h"
#import "WLTCVVDotView.h"
#import "LSButton.h"
#import "WLTPaymentConfig.h"
#import "WLTNetworkManager.h"
#import "WLTInstrumentsManager.h"
#import "WLTDatabaseManager.h"
#import "WLTViewController.h"
#import "WLTRootViewController.h"

NSString *const NotifyNewCardAdded = @"NotifyNewCardAdded";

@interface WLTCardEntryAccessoryView() {
    
    NSCharacterSet *onlyNumbersCharSet;
    NSString *previousTextFieldContent;
    UITextRange *previousSelection;
}

@end

@implementation WLTCardEntryAccessoryView

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self bringSubviewToFront:addCardBackView];
    
    {
        addCardBackView.hidden = YES;
        addCardBackView.backgroundColor = [UIColor appleGreenColor];
        
        [addCardButton.titleLabel setFont:[UIFont sfUITextMediumOfSize:16]];
        [addCardButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [editCardButton addTarget:self action:@selector(editCardClicked:) forControlEvents:UIControlEventTouchUpInside];
        [addCardButton addTarget:self action:@selector(addCardClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    otherCharacterSet = [[NSCharacterSet characterSetWithCharactersInString:@" 0123456789"] invertedSet];
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/yyyy"];
    
    allowedCards = @[@(CreditCardBrandVisa), @(CreditCardBrandMasterCard)];
    cardErrorDefaultText = @"Only Visa and MaterCard Supported";
    cardErrorLabel.text = cardErrorDefaultText;
    cardErrorLabel.font = [UIFont sfUITextRegularOfSize:10];
    cardErrorLabel.textColor = [UIColor pomegranateColor];
    cardErrorLabel.hidden = YES;
    
    cardExpiryErrorLabel.text = @"Invalid expiry date";
    [cardExpiryErrorLabel setPreferredMaxLayoutWidth:100];
    cardExpiryErrorLabel.font = [UIFont sfUITextRegularOfSize:10];
    cardExpiryErrorLabel.textColor = [UIColor pomegranateColor];
    cardExpiryErrorLabel.hidden = YES;
    
    cardNumberField.textColor = [UIColor darkGrayTextColor];
    cardNumberField.font = [UIFont sfUITextRegularOfSize:14];
    [cardNumberField addTarget:self action:@selector(reformatAsCardNumber:) forControlEvents:UIControlEventEditingChanged];
    
    cardExpDateField.textColor = [UIColor darkGrayTextColor];
    cardExpDateField.font = [UIFont sfUITextRegularOfSize:14];
    [cardExpDateField addTarget:self action:@selector(expDateTextChanged:) forControlEvents:UIControlEventEditingChanged];
    
    cardCVVNumberField.textColor = [UIColor darkGrayTextColor];
    cardCVVNumberField.font = [UIFont sfUITextRegularOfSize:14];
    [cardCVVNumberField addTarget:self action:@selector(cvvTextChanged:) forControlEvents:UIControlEventEditingChanged];
    
    otpEntryLabel.textColor = [UIColor mediumGrayTextColor];
    otpEntryLabel.font = [UIFont sfUITextRegularOfSize:14];
    
    WLTOTPView *otpView = [[WLTOTPView alloc] initWithFrame:CGRectZero];
    otpView.translatesAutoresizingMaskIntoConstraints = NO;
    otpView.backgroundColor = [UIColor clearColor];
    otpView.dotsSize = 25;
    otpView.dotsGap = 10;
    [otpEntryBackView addSubview:otpView];
    [otpView setupForOTPCount:AllowedCVVLength andDotViewClass:[WLTCVVDotView class]];
    
    cvvEntryView = otpView;

    NSDictionary *dict = NSDictionaryOfVariableBindings(otpView);
    [otpEntryBackView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[otpView]-0-|"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:dict]];
    
    [otpEntryBackView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[otpView(==130)]"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:dict]];
    
    [otpEntryBackView addConstraint:[NSLayoutConstraint constraintWithItem:otpView
                                 attribute:NSLayoutAttributeCenterX
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:otpEntryBackView
                                 attribute:NSLayoutAttributeCenterX
                                multiplier:1
                                  constant:0]];
    
    [self addBorderLineAtPosition:(BorderLinePositionTop|BorderLinePositionBottom)];
    
    currentAlphaValue = -1;
    [self showCVVEntryPercent:0 animated:NO];
}

-(void)setFlowType:(WLTEntryFlowType)flowType {
    
    _flowType = flowType;
    
    if (_flowType == WLTEntryFlowTypePay) {
        
        cardCVVNumberField.hidden = NO;
        
        expDateTralingSpace.constant = 54;
        [cardExpDateField setTextAlignment:NSTextAlignmentRight];
        [addCardButton setTitle:@"SAVE & PAY" forState:UIControlStateNormal];
    }else {
        
        cardCVVNumberField.hidden = YES;
        expDateTralingSpace.constant = 5;
        [cardExpDateField setTextAlignment:NSTextAlignmentLeft];
        [addCardButton setTitle:@"SAVE" forState:UIControlStateNormal];
    }
}

-(void)setUpForReceivingOnly {
    
    allowedCards = @[@(CreditCardBrandVisa)];
    
    cardErrorDefaultText = @"Only VISA cards are supported";
    cardErrorLabel.text = cardErrorDefaultText;
}

-(void)editCardClicked:(UIButton*)btn {
    
    [UIView animateWithDuration:0.3 animations:^{
        addCardBackView.alpha = 0;
    } completion:^(BOOL finished) {
        addCardBackView.hidden = YES;
    }];
    
    [cardNumberField becomeFirstResponder];
}

-(void)addCardClicked:(UIButton*)btn {
    
    if (self.flowType == WLTEntryFlowTypePay) {
        enteredCVVText = cardCVVNumberField.text;
    }
    
    [self payClicked:nil];
}

-(void)showCVVEntryPercent:(CGFloat)percent animated:(BOOL)animate {
    
    if (percent != currentAlphaValue) {
        
        currentAlphaValue = percent;
        currentState = WLTCardEntryStateUnknown;
        
        if (currentAlphaValue >= 1) {
            
            currentState = WLTCardEntryStateCVV;
        }else if (currentAlphaValue <= 0) {
            
            currentState = WLTCardEntryStateNewCard;
        }
        
        otpEntryBackView.alpha = percent;
        cardEntryBackView.alpha = (1-percent);
    }
}

-(void)resetNewCardText {
    
    [UIView animateWithDuration:0.3 animations:^{
        addCardBackView.alpha = 0;
    } completion:^(BOOL finished) {
        addCardBackView.hidden = YES;
    }];
    
    [cardNumberField setText:nil];
    [cardExpDateField setText:nil];
    [cardCVVNumberField setText:nil];
    
    cardExpiryDate = nil;
    
    [self.delegate cardEntryView:self changedCardNumber:nil andExpDate:nil];
}

-(void)resetEnteredText {
    
    [UIView animateWithDuration:0.3 animations:^{
        
        addCardBackView.alpha = 0;
    } completion:^(BOOL finished) {
        
        addCardBackView.hidden = YES;
    }];
    
    [cvvEntryView reset];
}

-(BOOL)isFirstResponder {
    
    BOOL responder = [cardNumberField isFirstResponder];
    if (!responder) {
        responder = [cardExpDateField isFirstResponder];
    }
    if (!responder) {
        responder = [cvvEntryView isFirstResponder];
    }
    if (!responder) {
        responder = [cardCVVNumberField isFirstResponder];
    }
    return responder;
}

-(IBAction)payClicked:(UIBarButtonItem*)item {
    
    if (currentState == WLTCardEntryStateNewCard) {
        
        NSString *savedCardNumber = cardNumberField.text.creditCardNumber;
        NSString *errorString = nil;
        
        if (savedCardNumber.length != MaxCardLengthAllowed) {
            
            errorString = @"Please enter a valid card number.";
        }
        else if (![savedCardNumber isValidCreditCard]) {
            
            errorString = @"Please enter a valid card number.";
        }
        else if (!cardExpiryDate) {
            
            errorString = @"Please enter a valid card expiry date.";
        }
        
        if (errorString) {
            
            NSError *error = [NSObject walnutErrorWithMessage:errorString];
            [self showAlertForError:error];
            [self editCardClicked:nil];
        }else {
            
            [self addCardToPaynimoForCardNumber:savedCardNumber];
        }
    }else if (currentState == WLTCardEntryStateCVV){
        
        if (cvvEntryView.enteredDigits == AllowedCVVLength) {
            
            [self.delegate cardEntryView:self payClickedWithCVV:cvvEntryView.text];
        }else {
            
            NSError *error = [NSObject walnutErrorWithMessage:@"Please enter a valid CVV."];
            [self showAlertForError:error];
        }
    }
}

-(void)addCardToPaynimoForCardNumber:(NSString*)savedCardNumber {
    
    WLTDatabaseManager *manager = [WLTDatabaseManager sharedManager];
    WLTPaymentConfig *dbConfig = [[manager allObjectsOfClass:@"WLTPaymentConfig"] firstObject];
    
    if (!PaymentsEnabled()) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Payments Unavaliable!" message:@"Payments are not enabled for this account"
                                                       delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        
        [self editCardClicked:nil];
    }
    else if(dbConfig.isSuspended.boolValue) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Payments Unavaliable!" message:[WLTDatabaseManager sharedManager].paymentConfig.suspensionMessage
                                                       delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        
        [self editCardClicked:nil];
    }
    else {
        
        WLTLoggedInUser *user = manager.currentUser;
        
        NSString *custAliasID = dbConfig.aliasId;
        NSString *paynimoCustID = dbConfig.userId;
        
        NSString *cardRegPGMID = dbConfig.cardRegPgmId;
        NSString *txnPGMID = dbConfig.txnPgmId;
        
        NSString *merchantIdent = dbConfig.identifierProperty;
        NSString *custName = [NSString stringWithFormat:@"WPay %@", [NSString validCardNameForUser:user]];;
        NSString *custEmailID = user.email;
        NSString *requestType = @"TWD";
        NSString *action = @"Create";
        
        NSString *cardNumber = savedCardNumber;
        
        NSDateFormatter *apiFormatter = [[NSDateFormatter alloc] init];
        [apiFormatter setDateFormat:@"MM"];
        NSString *expMonth = [apiFormatter stringFromDate:cardExpiryDate];
        
        [apiFormatter setDateFormat:@"yyyy"];
        NSString *expYear = [apiFormatter stringFromDate:cardExpiryDate];
        
        [apiFormatter setDateFormat:@"dd-MM-yyyy"];
        NSString *todaysDate = [apiFormatter stringFromDate:[NSDate date]];
        
        NSMutableDictionary *parameters = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"DefaultJsonParameters" ofType:@"data"]];
        
        NSMutableDictionary *consumer = parameters[@"consumer"];
        NSMutableDictionary *consAlias = [consumer[@"consumerAlias"] firstObject];
        consAlias[@"custAliasId"] = custAliasID;
        
        consumer[@"emailID"] = custEmailID;
        consumer[@"identifier"] = custAliasID;
        
        //    consumer[@"mobileNumber"] = mobileNumber;
        consumer[@"name"] = custName;
        consumer[@"paynimoCustRegId"] = paynimoCustID;
        
        NSMutableDictionary *merchant = parameters[@"merchant"];
        merchant[@"cardRegPGMID"] = cardRegPGMID;
        merchant[@"identifier"] = merchantIdent;
        merchant[@"txnPGMID"] = txnPGMID;
        
        NSMutableDictionary *instrument = parameters[@"payment"][@"instrument"];
        instrument[@"alias"] = custName;
        instrument[@"identifier"] = cardNumber;
        
        NSMutableDictionary *expiry = instrument[@"expiry"];
        expiry[@"month"] = expMonth;
        expiry[@"year"] = expYear;
        
        NSMutableDictionary *holder = instrument[@"holder"];
        holder[@"name"] = custName;
        
        NSMutableDictionary *transaction = parameters[@"transaction"];
        transaction[@"dateTime"] = todaysDate;
        transaction[@"action"] = action;
        transaction[@"requestType"] = requestType;
        
        NSMutableDictionary *paySender = transaction[@"payment"][@"sender"];
        paySender[@"paynimoSenderAliasToken"] = custAliasID;
        paySender[@"paynimoSenderName"] = custName;
        
        NSString *cardSaveUrl = [WLTDatabaseManager sharedManager].paymentConfig.url;
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:cardSaveUrl]];
        [request setValue:@"application/json charset=utf-8" forHTTPHeaderField:@"content-type"];
        
        [request setHTTPMethod:@"POST"];
        
        NSData *bodyData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        [request setHTTPBody:bodyData];
        
        __weak typeof(self) weakSelf = self;
        [self.delegate startLoadingWithColor:[UIColor appleGreenColor]];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
            
            NSDictionary *dictResponse = nil;
            if (!connectionError) {
                dictResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            }
            
            [weakSelf.delegate endViewLoading];
            
            if (connectionError) {
                
                [weakSelf showAlertForError:connectionError];
                [weakSelf editCardClicked:nil];
            }else {
                
                [weakSelf addCardToWalnutServerForDictionary:dictResponse];
            }
        }];
    }
}

-(void)addCardToWalnutServerForDictionary:(NSDictionary*)dict {
    
    NSDictionary *instrumentDict = dict[@"payment"][@"instrument"];
    GTLWalnutMPaymentInstrument *instrument = nil;
    
    if (instrumentDict && instrumentDict.allKeys.count) {
        
        NSString *token = instrumentDict[@"token"];
        if ([token isEqualToString:@"NA"]) {
            token = nil;
        }
        
        NSString *paynimoAlias = instrumentDict[@"alias"];
        
        if (token.length && paynimoAlias.length) {
            
            NSString *type = instrumentDict[@"type"];
            NSString *subType = instrumentDict[@"subType"];
            NSString *netName = instrumentDict[@"issuer"];
            
            NSArray *allObjs = [paynimoAlias componentsSeparatedByString:@"_"];
            
            NSString *cardComNum = allObjs[3];
            NSString *lat4Digits = nil;
            if (cardComNum.length >= 4) {
                
                lat4Digits = [cardComNum substringWithRange:NSMakeRange(cardComNum.length-4, 4)];
            }
            
            NSString *bankName = allObjs[5];
            
            if (token.length && lat4Digits.length && paynimoAlias.length && type.length && subType.length && netName.length && bankName.length) {
                
                WLTLoggedInUser *user = [WLTDatabaseManager sharedManager].currentUser;
                WLTDatabaseManager *dbManager = [WLTDatabaseManager sharedManager];
                
                NSString *cName = [NSString stringWithFormat:@"WPay %@", [NSString validCardNameForUser:user]];
                
                instrument = [[GTLWalnutMPaymentInstrument alloc] init];
                instrument.cardName = cName;
                instrument.cardTokenNumber = token;
                instrument.deviceUuid = dbManager.appSettings.deviceIdentifier;
                instrument.instrumentUuid = [WLTNetworkManager generateUniqueIdentifier];
                instrument.last4Digits = lat4Digits;
                instrument.isActive = @(YES);
                instrument.paynimoAliasId = paynimoAlias;
                instrument.subtype = subType;
                instrument.type = type;
                instrument.bank = bankName;
                instrument.network = netName;
                
                NSData *dataJson = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
                instrument.paynimoResponse = [[NSString alloc] initWithData:dataJson encoding:NSUTF8StringEncoding];
            }
        }
    }
    
    if (instrument) {
        
        __weak typeof(self) weakSelf = self;
        [self.delegate startLoadingWithColor:[UIColor appleGreenColor]];
        
        [[WLTInstrumentsManager sharedManager] addInstrumentToWalnut:instrument withCompletion:^(id data) {
            
            if ([data isKindOfClass:[NSError class]]) {
                
                [weakSelf showAlertForError:data];
                [weakSelf editCardClicked:nil];
            }else {
                
                GTLWalnutMPaymentInstrument *added = instrument;
                if ([data isKindOfClass:[NSArray class]]) {
                    
                    for ( GTLWalnutMPaymentInstrument *instr in data ) {
                        
                        if([instr.instrumentUuid isEqualToString:instrument.instrumentUuid]) {
                            added = instr;
                            break;
                        }
                    }
                }
                
                if ( (weakSelf.flowType == WLTEntryFlowTypePay) && (enteredCVVText.length == AllowedCVVLength) && added.enabledForMoneysend.boolValue) {
                    
                    if ([weakSelf.delegate respondsToSelector:@selector(continuePaymentFromIntrument:andCVV:)]) {
                        [weakSelf.delegate continuePaymentFromIntrument:added andCVV:enteredCVVText];
                    }
                }else {
                    
                    if ([weakSelf.delegate respondsToSelector:@selector(cardEntryView:addedNewInstrumentOfID:)]) {
                        [weakSelf.delegate cardEntryView:weakSelf addedNewInstrumentOfID:added.instrumentUuid];
                    }
                }
                
                if (weakSelf.flowType == WLTEntryFlowTypeReceive) {
                    [[WLTInstrumentsManager sharedManager] showNewDefaultPopUpForIntrument:added flowType:weakSelf.flowType];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:NotifyNewCardAdded object:self];
            }
        }];
    }else {
        
        NSString *errorString = nil;
        
        NSDictionary *trans = dict[@"transaction"];
        if (trans && [trans isKindOfClass:[NSDictionary class]]) {
            
            NSString *errorStr = trans[@"txnErrorDesc"];
            NSArray *compo = [errorStr componentsSeparatedByString:@"|"];
            errorString = [compo lastObject];
        }
        
        NSError *error = [NSError walnutErrorWithMessage:errorString];
        [self showAlertForError:error];
        [self editCardClicked:nil];
    }
}

#pragma mark UITextField

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    BOOL canChange = YES;
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([textField isEqual:cardNumberField]) {
        
        canChange = NO;
        NSString *withoutSeperators = newText.creditCardNumber;
        CreditCardBrand brand = [withoutSeperators cardBrand];
        
        if ( (brand == CreditCardBrandUnknown) || [allowedCards containsObject:@(brand)]) {
            
            cardErrorLabel.hidden = YES;
            if ( (withoutSeperators.length <= MaxCardLengthAllowed) && ([string rangeOfCharacterFromSet:otherCharacterSet].location == NSNotFound) ) {
                
                canChange = YES;
                
                previousTextFieldContent = textField.text;
                previousSelection = textField.selectedTextRange;
            }
        }else {
            
            cardErrorLabel.text = cardErrorDefaultText;
            cardErrorLabel.hidden = NO;
        }
        
        if (canChange) {
            
            if (withoutSeperators.length >= 6) {
                
                NSArray *blocked = [WLTDatabaseManager sharedManager].blockedBins;
                for ( NSString *blockedBin in blocked ) {
                    
                    NSRange range = [withoutSeperators rangeOfString:blockedBin];
                    if ((range.location == 0) && (range.length == blockedBin.length) ) {
                        
                        cardErrorLabel.hidden = NO;
                        cardErrorLabel.text = @"This card is not yet supported";
                        
                        canChange = NO;
                        break;
                    }
                }
            }
            
            [self.delegate cardEntryView:self changedCardNumber:withoutSeperators andExpDate:cardExpDateField.text];
        }
    }
    else if ([textField isEqual:cardExpDateField]) {
        
        if (newText.length <= 5) {
            
            if (newText.length == 1) {
                if (newText.intValue > 1) {
                    textField.text = [NSString stringWithFormat:@"0%d/", newText.intValue];
                    canChange = NO;
                }
            }
            else if (newText.length == 2) {
                if (textField.text.length < 2) {
                    if (newText.intValue <= 12) {
                        textField.text = [NSString stringWithFormat:@"%@/", newText];
                    }
                    
                    canChange = NO;
                }
            }
            else if (newText.length == 3) {
                
                NSRange seperatorRange = [newText rangeOfString:@"/"];
                if (seperatorRange.location == NSNotFound) {
                    
                    textField.text = [NSString stringWithFormat:@"%@/%@", [newText substringToIndex:2], [newText substringFromIndex:2]];
                    canChange = NO;
                }
            }
        }
        else {
            canChange = NO;
        }
        
        if (canChange) {
            
            [self refreshExpiryDateForText:newText];
            [self.delegate cardEntryView:self changedCardNumber:cardNumberField.text.creditCardNumber andExpDate:newText];
        }else {
            
            [self refreshExpiryDateForText:textField.text];
            [self.delegate cardEntryView:self changedCardNumber:cardNumberField.text.creditCardNumber andExpDate:textField.text];
        }
        
        [self checkAndShowAddCardView];
    }
    
    return canChange;
}

-(void)checkAndShowAddCardView {
    
    BOOL canAdd = NO;
    if (cardExpiryDate) {
        
        NSString *cardNum = [cardNumberField.text creditCardNumber];
        if ([cardNum isValidCreditCard] && (cardNum.length == MaxCardLengthAllowed)) {
            
            canAdd = YES;
            if (self.flowType == WLTEntryFlowTypePay) {
                
                canAdd = NO;
                if (cardCVVNumberField.text.length == AllowedCVVLength) {
                    canAdd = YES;
                }
            }
        }
    }
    
    if (canAdd) {
        
        addCardBackView.hidden = NO;
        addCardBackView.alpha = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            
            addCardBackView.alpha = 1;
        }];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
    if ([textField isEqual:cardNumberField]) {
        if (textField.text.length) {
            
            if (![[textField.text creditCardNumber] isValidCreditCard]) {
                
                textField.textColor = [UIColor flamePeaColor];
            }
        }
    }
    else if ([textField isEqual:cardExpDateField]) {
        
        if (textField.text.length) {
            
            if (!cardExpiryDate) {
                
                textField.textColor = [UIColor flamePeaColor];
            }
        }
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if ([textField isEqual:cardNumberField]) {
        
        textField.textColor = [UIColor darkGrayTextColor];
    }
    else if ([textField isEqual:cardExpDateField]) {
        
        textField.textColor = [UIColor darkGrayTextColor];
    }
}

-(void)refreshExpiryDateForText:(NSString*)text {
    
    cardExpiryDate = nil;
    cardExpiryErrorLabel.hidden = YES;
    
    NSArray *components = [text componentsSeparatedByString:@"/"];
    if (components.count == 2) {
        
        NSString *year = [components lastObject];
        NSString *month = [components firstObject];
        
        if ((year.length == 2) && (month.length == 2)) {
            
            NSDate *date = [formatter dateFromString:[NSString stringWithFormat:@"%@/20%@", month, year]];
            if (date && ([[NSDate date] timeIntervalSinceDate:date] < 0)) {
                
                cardExpiryDate = date;
                cardExpiryErrorLabel.hidden = YES;
            }else {
                cardExpiryErrorLabel.hidden = NO;
            }
        }
    }
}

#pragma mark - TextField Card Formatting Handling

//http://stackoverflow.com/questions/12083605/formatting-a-uitextfield-for-credit-card-input-like-xxxx-xxxx-xxxx-xxxx
// Version 1.2
// Source and explanation: http://stackoverflow.com/a/19161529/1709587
-(void)reformatAsCardNumber:(UITextField *)textField {
    
    // In order to make the cursor end up positioned correctly, we need to
    // explicitly reposition it after we inject spaces into the text.
    // targetCursorPosition keeps track of where the cursor needs to end up as
    // we modify the string, and at the end we set the cursor position to it.
    NSUInteger targetCursorPosition = [textField offsetFromPosition:textField.beginningOfDocument toPosition:textField.selectedTextRange.start];
    
    NSString *cardNumberWithoutSpaces = [self removeNonDigits:textField.text andPreserveCursorPosition:&targetCursorPosition];
    
    if (cardNumberWithoutSpaces.length == MaxCardLengthAllowed) {
        
        [cardExpDateField becomeFirstResponder];
    }
    else if ([cardNumberWithoutSpaces length] > MaxCardLengthAllowed) {
        
        [textField setText:previousTextFieldContent];
        textField.selectedTextRange = previousSelection;
        return;
    }
    
    [self checkAndShowAddCardView];
    NSString *cardNumberWithSpaces = [self insertSpacesEveryFourDigitsIntoString:cardNumberWithoutSpaces andPreserveCursorPosition:&targetCursorPosition];
    
    textField.text = cardNumberWithSpaces;
    UITextPosition *targetPosition = [textField positionFromPosition:[textField beginningOfDocument] offset:targetCursorPosition];
    [textField setSelectedTextRange: [textField textRangeFromPosition:targetPosition toPosition:targetPosition]];
}

/*
 Removes non-digits from the string, decrementing `cursorPosition` as
 appropriate so that, for instance, if we pass in `@"1111 1123 1111"`
 and a cursor position of `8`, the cursor position will be changed to
 `7` (keeping it between the '2' and the '3' after the spaces are removed).
 */
- (NSString *)removeNonDigits:(NSString *)string andPreserveCursorPosition:(NSUInteger *)cursorPosition {
    
    NSUInteger originalCursorPosition = *cursorPosition;
    NSMutableString *digitsOnlyString = [NSMutableString new];
    
    for (NSUInteger i=0; i<[string length]; i++) {
        
        unichar characterToAdd = [string characterAtIndex:i];
        
        if (isdigit(characterToAdd)) {
            
            NSString *stringToAdd = [NSString stringWithCharacters:&characterToAdd length:1];
            [digitsOnlyString appendString:stringToAdd];
        }
        else {
            
            if (i < originalCursorPosition) {
                
                (*cursorPosition)--;
            }
        }
    }
    
    return digitsOnlyString;
}

- (NSString *)insertSpacesEveryFourDigitsIntoString:(NSString *)string andPreserveCursorPosition:(NSUInteger *)cursorPosition {
    
    NSMutableString *stringWithAddedSpaces = [NSMutableString new];
    NSUInteger cursorPositionInSpacelessString = *cursorPosition;
    for (NSUInteger i=0; i<[string length]; i++) {
        
        if ((i>0) && ((i % 4) == 0)) {
            
            [stringWithAddedSpaces appendString:@" "];
            
            if (i < cursorPositionInSpacelessString) {
                
                (*cursorPosition)++;
            }
        }
        
        unichar characterToAdd = [string characterAtIndex:i];
        NSString *stringToAdd = [NSString stringWithCharacters:&characterToAdd length:1];
        [stringWithAddedSpaces appendString:stringToAdd];
    }
    
    return stringWithAddedSpaces;
}

#pragma mark -

-(void)expDateTextChanged:(UITextField*)field {
    
    if (self.flowType == WLTEntryFlowTypePay) {
        
        if (cardExpiryDate) {
            [cardCVVNumberField becomeFirstResponder];
        }
    }
}

-(void)cvvTextChanged:(UITextField*)field {
    
    [self checkAndShowAddCardView];
}

@end
