//
//  WLTCardView.m
//  walnut
//
//  Created by Abhinav Singh on 02/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTCardView.h"
#import "GTLWalnutMPaymentInstrument.h"
#import "WLTColorPalette.h"
#import "WLTInstrumentsManager.h"

@implementation WLTCardView

-(void)awakeFromNib {
    
    [super awakeFromNib];
    [self initialSetup];
}

-(void)initialSetup {
    
    cardNumberLabel.textColor = [UIColor whiteColor];
    cardNumberLabel.font = [UIFont ocrFontOfSize:21];
    cardNumberLabel.shadowColor = [UIColor colorWithWhite:0 alpha:0.5];
    cardNumberLabel.shadowOffset = CGSizeMake(0, 1);
    
    cardNameLabel.textColor = [UIColor whiteColor];
    cardNameLabel.font = [UIFont ocrFontOfSize:16];
    cardNameLabel.shadowColor = [UIColor colorWithWhite:0 alpha:0.5];
    cardNameLabel.shadowOffset = CGSizeMake(0, 1);
    
    tintImageView.image = [tintImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    fastFundImageView.tintColor = [UIColor whiteColor];
    
    self.backgroundColor = [UIColor clearColor];
}

-(void)showDetailsOfInstrument:(GTLWalnutMPaymentInstrument*)instrument {
    
    self.instrument = instrument;
    
    if ([instrument.type isEqualToString:InstrumentTypeBank]) {
        
        cardTypeImageView.image = [[UIImage imageNamed:@"BankIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cardTypeImageView.tintColor = [UIColor whiteColor];
        
        cardNumberLabel.text = [NSString stringWithFormat:@"XX%@", instrument.last4Digits];
    }
    else {
       
        CreditCardBrand brand = [NSString cardTypeForDisplayString:instrument.network];
        NSString *imageName = [NSString iconForCardType:brand];
        cardTypeImageView.image = [UIImage imageNamed:imageName];
        
        cardNumberLabel.text = instrument.last4Digits.cardNumberForLast4Digits;
    }
    
    tintImageView.tintColor = [[WLTColorPalette sharedPalette] colorForInstrumentID:instrument.instrumentUuid];
    
    cardNameLabel.text = instrument.bank;
    if(!cardNameLabel.text.length) {
        cardNameLabel.text = instrument.cardName;
    }
    
    if (instrument.enabledForMoneyreceive.boolValue) {
        
        if (instrument.fastFundEnabled.boolValue) {
            fastFundImageView.image = [[UIImage imageNamed:@"FastFundEnabled"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        }else {
            fastFundImageView.image = [[UIImage imageNamed:@"FastFundDisabled"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        }
    }else {
        fastFundImageView.image = nil;
    }
}

@end
