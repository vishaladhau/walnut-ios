//
//  WLTSettleGroupTableViewCell.m
//  walnut
//
//  Created by Abhinav Singh on 09/03/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTSettleGroupTableViewCell.h"

@implementation WLTSettleGroupTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self setBackgroundColor:[UIColor clearColor]];
    [backView setBackgroundColor:[UIColor whiteColor]];
    
    [backView addBorderLineAtPosition:(BorderLinePositionBottom|BorderLinePositionTop)];
    
    infoLabel.font = [UIFont sfUITextMediumOfSize:13];
    infoLabel.textColor = [UIColor darkGrayTextColor];
    infoLabel.text = @"GROUP SETTLED";
}

@end
