//
//  WLTCommentView.h
//  walnut
//
//  Created by Abhinav Singh on 01/03/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "PlaceholderTextView.h"
#import "WLTImageButton.h"

@interface WLTCommentView : PlaceholderTextView {
    
    CGFloat prevHeight;
    
    @public
    __weak WLTImageButton *commentButton;
    __weak WLTImageButton *sendButton;
    __weak WLTImageButton *addButton;
}

-(void)setColor:(UIColor*)colour;
-(void)changeHeightForCurrentText;

@end
