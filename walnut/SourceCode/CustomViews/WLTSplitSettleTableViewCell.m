//
//  WLTSplitSettleTableViewCell.m
//  walnut
//
//  Created by Abhinav Singh on 08/03/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import "WLTSplitSettleTableViewCell.h"
#import "WLTTransaction.h"
#import "WLTContactsManager.h"
#import "WLTDatabaseManager.h"
#import "WLTPaymentTransationsManager.h"
#import "WLTPaymentTransaction.h"

@implementation WLTSplitSettleTableViewCell {
    NSString *_transactinID;
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    amountBackView.layer.cornerRadius = DefaultCornerRadius;
    amountBackView.backgroundColor = [UIColor lightBackgroundColor];
    
    payTypeImageView.layer.cornerRadius = 20;
    payTypeImageView.backgroundColor = [UIColor whiteColor];
    payTypeImageView.tintColor = [UIColor getColor];
    
    payDateLabel.font = [UIFont sfUITextRegularOfSize:9];
    payDateLabel.textColor = [UIColor mediumGrayTextColor];
    
    payTypeLabel.font = [UIFont sfUITextRegularOfSize:9];
    payTypeLabel.textColor = [UIColor denimColor];
    
    infoLabel.textColor = [UIColor mediumGrayTextColor];
    infoLabel.font = [UIFont sfUITextRegularOfSize:16];
    
    bankNameLabel.textColor = [UIColor mediumGrayTextColor];
    bankNameLabel.font = [UIFont sfUITextRegularOfSize:10];
    bankNameLabel.numberOfLines = 0;
    
    amountLabel.textColor = [UIColor mediumGrayTextColor];
    amountLabel.font = [UIFont sfUITextRegularOfSize:16];
    amountLabel.textAlignment = NSTextAlignmentLeft;
    
    crossSettleLabel.textColor = [UIColor mediumGrayTextColor];
    crossSettleLabel.font = [UIFont sfUITextMediumOfSize:10];
}

-(void)nameForUser:(id<User>)user compeltion:(DataCompletionBlock)block{
    
    __weak typeof(self) weakSelf = self;
    [[WLTContactsManager sharedManager] displayNameForUser:user defaultIsMob:YES completion:^(NSString *data) {
        
        if (weakSelf) {
            block(data);
        }
    }];
}

-(void)showDetailsOfTransaction:(WLTTransaction*)trans{
    
    BOOL showCrossGrp = NO;
    if (trans.isCrossGroupSettlement.boolValue) {
        
        NSString *currentUserMob = CURRENT_USER_MOB;
        if ([trans.owner.completeNumber isEqualToString:currentUserMob] || [trans.receiver.completeNumber isEqualToString:currentUserMob]) {
            showCrossGrp = YES;
        }
    }
    
    if (showCrossGrp) {
        
        dottedImageView.hidden = NO;
        crossSettleLabel.hidden = NO;
        
        leadingGapConstraint.constant = 15;
        tralingGapConstraint.constant = -21;
        topGapConstraint.constant = 35;
        bottomGapConstraint.constant = -15;
    }else {
        
        dottedImageView.hidden = YES;
        crossSettleLabel.hidden = YES;
        
        leadingGapConstraint.constant = 8;
        tralingGapConstraint.constant = -15;
        topGapConstraint.constant = 8;
        bottomGapConstraint.constant = -8;
    }
    
    crossSettleLabel.text = @"";
    
    NSString *completeTotal = [NSString stringWithFormat:@"Amount\n%@", trans.amount.displayRoundedCurrencyString];
    NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:completeTotal attributes:@{NSForegroundColorAttributeName:[UIColor mediumGrayTextColor], NSFontAttributeName:[UIFont sfUITextRegularOfSize:16]}];
    [attributed addAttributes:@{NSFontAttributeName:[UIFont sfUITextRegularOfSize:10]} range:[completeTotal rangeOfString:@"Amount"]];
    
    amountLabel.attributedText = attributed;
    if (trans.createdDate) {
        payDateLabel.text = trans.createdDate.displayString;
    }else {
        payDateLabel.text = @"";
    }
    
    __weak typeof(self) wSelf = self;
    infoLabel.text = @"";
    [self nameForUser:trans.receiver compeltion:^(NSString *getName) {
        
        [wSelf nameForUser:trans.owner compeltion:^(NSString *payName) {
            
            infoLabel.text = [NSString stringWithFormat:@"%@ paid %@", payName, getName];
        }];
    }];
    
    if (trans.senderBank.length) {
        
        bankNameLabelTopConstraint.constant = 5;
        
        NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:trans.senderBank];
        
        NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
        UIImage *image = [UIImage imageNamed:@"BankNameSeperator"];
        textAttachment.image = image;
        textAttachment.bounds = CGRectMake( 0, 0, image.size.width, image.size.height );
        
        NSAttributedString *attributedAtt = [NSAttributedString attributedStringWithAttachment:textAttachment];
        [attributed appendAttributedString:attributedAtt];
        
        if (trans.receiverBank.length) {
            NSAttributedString *recStr = [[NSAttributedString alloc] initWithString:trans.receiverBank];
            [attributed appendAttributedString:recStr];
        }
        
        bankNameLabel.attributedText = attributed;
    }
    else {
        bankNameLabelTopConstraint.constant = 0;
        bankNameLabel.text = nil;
    }
    
    paymentStatusImageView.image = nil;
    _transactinID = trans.txnID;
    
    __block NSString *newCrossString = @"Cross Group Settlement";
    
    if (trans.isWpaySettled.boolValue) {
        
        payTypeImageView.image = [[UIImage imageNamed:@"WalnutPayIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        payTypeLabel.text = @"WalnutPay";
        
        NSString *identifierForFetch = [_transactinID copy];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY groups.splitTransactionID == %@", _transactinID];
        
        
        [[WLTPaymentTransationsManager sharedManager] coreDataPaymentTransactionForPredicate:predicate completion:^(WLTPaymentTransaction *object, NSError *error) {
			
            if (wSelf == nil) {
                return;
            }
            
            typeof(self) sSelf = wSelf;
            if ([identifierForFetch isEqualToString:sSelf->_transactinID]) {
                
                if(object) {
                    
                    NSString *imageName = nil;
                    switch (object.status) {
                        case PaymentStatusPullSuccess:
                        case PaymentStatusPushPending: {
                            imageName = @"StatusIconSent";
                        }
                            break;
                        case PaymentStatusPushSuccess: {
                            imageName = @"StatusIconReceived";
                        }
                            break;
                        case PaymentStatusPushReversed:
                            imageName = @"RevertPaymentIcon";
                            break;
                        default:
                            break;
                    }
                    sSelf->paymentStatusImageView.image = [UIImage imageNamed:imageName];
                    
                    if (object.groups.count) {
                        newCrossString = [NSString stringWithFormat:@"Settled across %@ groups", @(object.groups.count).displayString];
                    }
                }
            }
            
            if (showCrossGrp) {
                sSelf->crossSettleLabel.text = newCrossString;
            }
        }];
    }else {
        
        if (showCrossGrp) {
            crossSettleLabel.text = newCrossString;
        }
        
        payTypeImageView.image = [[UIImage imageNamed:@"RupeesSymbolIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        payTypeLabel.text = @"Cash";
    }
}

@end
