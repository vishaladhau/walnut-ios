//
//  WLTGroupCollectionViewCell.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 04/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WLTImageButton.h"

@class WLTGroupCollectionViewCell;
@class WLTGroup;
@class WLTUnreadCountLabel;

@protocol WLTGroupCollectionViewCellDelegate <NSObject>

-(void)payButtonClickedFromCell:(WLTGroupCollectionViewCell*)cell;
-(void)reminderButtonClickedFromCell:(WLTGroupCollectionViewCell*)cell;
-(void)addButtonClickedFromCell:(WLTGroupCollectionViewCell*)cell;

@end

@interface WLTGroupCollectionViewCell : UIControl {
    
    @public
    
    __weak IBOutlet WLTUnreadCountLabel *unreadCountView;
    
    __weak IBOutlet UIImageView *backgroundImageView;
    __weak IBOutlet UILabel *oweOrGetLabel;
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UILabel *memCountLabel;
    
    __weak IBOutlet UIButton *addButton;
    
    __weak IBOutlet NSLayoutConstraint *amountLeadingConstraint;
}

@property(readonly, strong) WLTGroup *displayedGroup;
@property(nonatomic, weak) id <WLTGroupCollectionViewCellDelegate> delegate;

-(void)showInfoOfGroup:(WLTGroup*)group;

@end
