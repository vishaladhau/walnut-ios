//
//  WLTEmptyCardView.h
//  walnut
//
//  Created by Abhinav Singh on 23/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLTEmptyCardView : UIView {
    
    __weak IBOutlet UIImageView *tintImageView;
    
    __weak IBOutlet UILabel *cardExpDateHeaderLabel;
    __weak IBOutlet UILabel *cardNumberHeaderLabel;
    
@public
    __weak IBOutlet UILabel *cardNumberLabel;
    __weak IBOutlet UILabel *cardExpDateLabel;
    __weak IBOutlet UIImageView *cardTypeImageView;
}

@end
