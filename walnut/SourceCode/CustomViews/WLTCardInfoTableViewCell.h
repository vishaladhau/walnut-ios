//
//  WLTCardInfoTableViewCell.h
//  walnut
//
//  Created by Abhinav Singh on 6/7/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GTLWalnutMPaymentInstrument;

@interface WLTCardInfoTableViewCell : UITableViewCell {
	
	__weak IBOutlet UIImageView *cardTypeImageView;
	__weak IBOutlet UILabel *infoLabel;
	__weak IBOutlet UILabel *last4DigitsLabel;
}

-(void)showDetailsOfInstrument:(GTLWalnutMPaymentInstrument*)intrument isReceiveDefault:(BOOL)receive;

@end
