//
//  WLTCVVEnterView.h
//  walnut
//
//  Created by Abhinav Singh on 03/05/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLTCVVEnterView : UIView {
    
    @public
    __weak IBOutlet UILabel *identifierLabel;
    __weak IBOutlet UITextField *cvvLabelField;
}

@end
