//
//  WLTColorCollectionViewCell.h
//  Walnut_ios
//
//  Created by Abhinav Singh on 07/02/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLTColorCollectionViewCell : UICollectionViewCell {
    
    __weak IBOutlet UIImageView *selectionView;
    @public
    __weak IBOutlet UIImageView *backgroundImageView;
}

@end
