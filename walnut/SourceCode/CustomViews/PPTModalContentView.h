//
//  PPTModalContentView.h
//  PepperTap
//
//  Created by Abhinav Singh on 19/12/15.
//  Copyright © 2015 Leftshift Technologies Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

static CGFloat ModalViewMaximumAlpha = 0.8;

@interface PPTModalContentView : UIView {
    
    __weak IBOutlet UIView *_alertContentView;
    __weak IBOutlet UIView *_alphaView;
    
    __weak IBOutlet NSLayoutConstraint *alertContentOriginY;
    __weak IBOutlet NSLayoutConstraint *alertContentHeight;
}

-(void)show;
-(void)dismiss;

-(void)initialSetup;
-(CGFloat)alertContentViewWidth;

@end
