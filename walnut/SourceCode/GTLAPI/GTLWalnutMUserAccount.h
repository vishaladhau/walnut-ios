/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2016 Google Inc.
 */

//
//  GTLWalnutMUserAccount.h
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   walnut/v1
// Description:
//   This is an API
// Classes:
//   GTLWalnutMUserAccount (0 custom class methods, 19 custom properties)

#if GTL_BUILT_AS_FRAMEWORK
  #import "GTL/GTLObject.h"
#else
  #import "GTLObject.h"
#endif

// ----------------------------------------------------------------------------
//
//   GTLWalnutMUserAccount
//

@interface GTLWalnutMUserAccount : GTLObject
@property (nonatomic, retain) NSNumber *accountColor;  // longLongValue
@property (nonatomic, retain) NSNumber *acctType;  // longLongValue
@property (nonatomic, retain) NSNumber *balLastSyncTime;  // longLongValue
@property (nonatomic, retain) NSNumber *balance;  // doubleValue
@property (nonatomic, copy) NSString *cardIssuer;
@property (nonatomic, copy) NSString *displayName;
@property (nonatomic, copy) NSString *displayPan;
@property (nonatomic, retain) NSNumber *enabled;  // boolValue
@property (nonatomic, retain) NSNumber *endDate;  // longLongValue
@property (nonatomic, retain) NSNumber *flags;  // longLongValue
@property (nonatomic, copy) NSString *mergeUuid;
@property (nonatomic, retain) NSNumber *modifyCount;  // longLongValue
@property (nonatomic, copy) NSString *name;
@property (nonatomic, retain) NSNumber *obalLastSyncTime;  // longLongValue
@property (nonatomic, retain) NSNumber *outstandingBalance;  // doubleValue
@property (nonatomic, copy) NSString *pan;
@property (nonatomic, retain) NSNumber *startDate;  // longLongValue
@property (nonatomic, retain) NSNumber *updatedTime;  // longLongValue
@property (nonatomic, copy) NSString *uuid;
@end
