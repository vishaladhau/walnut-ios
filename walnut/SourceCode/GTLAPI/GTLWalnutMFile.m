/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2016 Google Inc.
 */

//
//  GTLWalnutMFile.m
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   walnut/v1
// Description:
//   This is an API
// Classes:
//   GTLWalnutMFile (0 custom class methods, 5 custom properties)

#import "GTLWalnutMFile.h"

// ----------------------------------------------------------------------------
//
//   GTLWalnutMFile
//

@implementation GTLWalnutMFile
@dynamic contentType, data, fileName, identifier, type;

+ (NSDictionary *)propertyToJSONKeyMap {
  NSDictionary *map = @{
    @"contentType" : @"content_type",
    @"fileName" : @"file_name",
    @"identifier" : @"id_",
    @"type" : @"type_"
  };
  return map;
}

@end
