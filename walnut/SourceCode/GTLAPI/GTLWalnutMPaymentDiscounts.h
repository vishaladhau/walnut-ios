/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2016 Google Inc.
 */

//
//  GTLWalnutMPaymentDiscounts.h
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   walnut/v1
// Description:
//   This is an API
// Classes:
//   GTLWalnutMPaymentDiscounts (0 custom class methods, 1 custom properties)

#if GTL_BUILT_AS_FRAMEWORK
  #import "GTL/GTLObject.h"
#else
  #import "GTLObject.h"
#endif

@class GTLWalnutMPaymentDiscount;

// ----------------------------------------------------------------------------
//
//   GTLWalnutMPaymentDiscounts
//

@interface GTLWalnutMPaymentDiscounts : GTLObject
@property (nonatomic, retain) NSArray *discounts;  // of GTLWalnutMPaymentDiscount
@end
