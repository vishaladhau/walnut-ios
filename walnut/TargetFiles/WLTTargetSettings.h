//
//  WLTTargetSettings.h
//  walnut
//
//  Created by Abhinav Singh on 01/06/16.
//  Copyright © 2016 Leftshift Technologies Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const AppSalarApiKey;
FOUNDATION_EXPORT NSString *const AppSalarApiSecrect;

FOUNDATION_EXPORT NSString *const WalnutClientID;
FOUNDATION_EXPORT NSString *const WalnutAPIKey;
FOUNDATION_EXPORT NSString *const RegisterAPIUrl;
FOUNDATION_EXPORT NSString *const ValidateOTPUrl;

@interface WLTTargetSettings : NSObject

+(void)setup;
+(BOOL)isDebugVersion;

@end
